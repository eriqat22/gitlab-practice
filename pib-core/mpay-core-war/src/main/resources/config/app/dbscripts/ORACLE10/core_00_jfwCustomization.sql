UPDATE JFW_VALUE_PROVIDERS SET PROVIDER='com.progressoft.jfw.model.bussinessobject.FiltersWidgetTypeProvider' WHERE id='1088';
-- Issue in excel
update JFW_PROPERTIES set VALID_FOR_GRPNG = 0;

 --update orgs
update jfw_orgs set z_org_id =id;
update JFW_GENERIC_CONFIGURATION set config_value='(MPAY).*' where config_key='APP.KEY';

-- PIN_MQ_HEADERS
update JFW_PORTLETS set name = id where name is null;

-- threre is no record having NOTIFY_QUEUE='mpay.registration.notification' ...
-- update JFW_WF_ACTION set JFW_WF_ACTION.HEADERS_PROVIDER='com.progressoft.mpay.registration.customers.RegNotificationHeaderProvider' where name='SVC_Approve' and action_code='24' and NOTIFY_QUEUE='mpay.registration.notification';
UPDATE JFW_PROPERTIES SET MAXLENGTH = 0 WHERE ID = (SELECT ID FROM JFW_PROPERTIES WHERE NAME = 'schemeViews' AND ENTITY_ID = (SELECT ID FROM JFW_ENTITIES WHERE NAME= 'com.progressoft.jfw.model.bussinessobject.core.JfwViewsScheme'));
UPDATE JFW_VIEWS SET LOCKING_MODE = 1 WHERE NAME = 'OSWorkflow';
update jfw_properties set width =20 where name ='statusId';
update jfw_generic_configuration set config_value='false' where config_key='SHOW_USER_LOGIN_INFO';

------------------------------Was post import ----------------------
INSERT INTO MPAY_BulkPayment (ID , Z_ORG_ID, Z_TENANT_ID, DESCRIPTION, FILENAME) VALUES (1, 0, 'SYSTEM', 'Test','Test');
INSERT INTO MPAY_BulkRegistration (ID , Z_ORG_ID, Z_TENANT_ID, DESCRIPTION, FILENAME) VALUES (1, 0, 'SYSTEM', 'Test','Test');
UPDATE JFW_PROPERTIES SET REQUIRED = '1' WHERE NAME = 'kycTemplate';
update JFW_TABS set FILE_EXTENSIONS = 'pdf,xls,xlsx,png,jpeg,gif,bmp,doc,docx,jpg' where PORTLET_ID in (select id from JFW_PORTLETS where NAME = 'Attachments');

------------------- Update Portlets Order on New and Edit Portals in CorporateKYC View to appear last -------------------
-- Attachment Types and Activity:
UPDATE JFW_PORTLETS SET ORDERVALUE = '1000' WHERE name = 'KYC Attachment Types';
UPDATE JFW_PORTLETS SET ORDERVALUE = '1001' WHERE name = 'Activity';
CREATE SEQUENCE MPAY_MESSAGEID_SEQ START WITH 2921218077343  MAXVALUE 9999999999999999999999999999  MINVALUE 1  NOCYCLE  CACHE 20  NOORDER;
------------------------------Was post import ----------------------
