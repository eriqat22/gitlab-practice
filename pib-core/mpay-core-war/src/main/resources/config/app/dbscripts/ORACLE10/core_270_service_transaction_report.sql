DROP VIEW serviceinfo;

DROP TABLE serviceinfo;

CREATE VIEW serviceinfo AS
    ( SELECT DISTINCT
        name   servicename,
        FIRST_VALUE(t.z_creation_date) OVER(
            PARTITION BY name
            ORDER BY
                t.z_creation_date DESC
        ) servicelasttrn
    FROM
        mpay_transactions t,
        mpay_corpoarteservices s
    WHERE
        s.id = t.senderserviceid
        OR s.id = t.receiverserviceid
    );

DROP VIEW mpay_service_trans_rep;

DROP TABLE mpay_service_trans_rep;

CREATE OR REPLACE FORCE VIEW mpay_service_trans_rep AS
    SELECT
        (
            SELECT
                SYSDATE
            FROM
                dual
        ) extractstamp,
        c.name                            corporatename,
        s.name                            servicename,
        (
            SELECT
                status.description
            FROM
                jfw_wf_status status
            WHERE
                status.id = s.z_status_id
        ) servicestatus,
        (
            SELECT
                name
            FROM
                mpay_banks bank
            WHERE
                s.bankid = bank.id
        ) servicebankname,
        (
            SELECT
                id
            FROM
                mpay_banks bank
            WHERE
                s.bankid = bank.id
        ) servicebanknumber,
        s.iban                            serviceiban,
        account.accnumber                 servicempayaccount,
        mpaytransaction.z_creation_date   trndate,
        mpaytransaction.z_creation_date   trntime,
        CASE mpaytransaction.reftypeid
            WHEN 1   THEN 'CR'
            WHEN 2   THEN 'DR'
        END trnside,
        CASE mpaytransaction.reftypeid
            WHEN 1   THEN 'CR'
            WHEN 2   THEN 'DR'
        END trntype,
        mpaytransaction.originalamount    trnamount,
        mpaytransaction.totalcharge       trncharges,
        mpaytransaction.totalcharge       trnvat,
        mpaytransaction.totalamount       trnnetamount,
        mpaytransaction.reference         trnrefernceno,
        mpaytransaction.reasondesc        trndescription,
        jv.balancebefore                  servicebalancebeforetrn,
        jv.balanceafter                   servicebalanceaftertrn,
        mobile.mobilenumber               otherpartymobileno,
        NULL otherpartyamount,
        s.z_creation_date                 createddate,
        s.z_created_by                    createduser,
        s.z_updating_date                 modifieddate,
        s.z_updated_by                    modifieduser,
        s.z_deleted_on                    deleteddate,
        s.z_deleted_by                    deleteduser,
        s.id                              id,
        s.z_archive_on,
        s.z_assigned_user,
        s.z_archive_queued,
        s.z_archive_status,
        s.z_assigned_group,
        s.z_created_by,
        s.z_creation_date,
        s.z_deleted_by,
        s.z_deleted_flag,
        s.z_deleted_on,
        s.z_editable,
        s.z_locked_by,
        s.z_locked_until,
        s.z_org_id,
        s.z_tenant_id,
        s.z_updated_by,
        s.z_updating_date,
        s.z_workflow_id,
        s.z_ws_token,
        s.z_draft_status,
        s.z_draft_id,
        s.z_status_id
    FROM
        mpay_corpoarteservices s
        JOIN mpay_corporates c ON c.id = s.refcorporateid
        JOIN mpay_serviceaccounts serviceacc ON serviceacc.serviceid = s.id
        JOIN mpay_accounts account ON account.id = serviceacc.refaccountid
        LEFT JOIN mpay_transactions mpaytransaction ON mpaytransaction.receiverserviceid = s.id
                                                       OR mpaytransaction.senderserviceid = s.id
        LEFT JOIN mpay_jvdetails jv ON jv.reftrsansactionid = mpaytransaction.id
                                       AND jv.refaccountid = account.id
        LEFT JOIN mpay_customermobiles mobile ON mobile.id = mpaytransaction.sendermobileid
    WHERE
        c.clienttypeid = 9
        AND mpaytransaction.z_creation_date IN (
            SELECT
                servicelasttrn
            FROM
                serviceinfo s
            WHERE
                s.servicename = s.name
        );