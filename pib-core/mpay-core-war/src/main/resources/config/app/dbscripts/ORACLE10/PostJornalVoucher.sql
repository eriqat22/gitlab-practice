CREATE OR REPLACE PROCEDURE PostJournalVoucher (
   accountId           MPAY_ACCOUNTS.ID%TYPE,
   amount              MPAY_ACCOUNTS.BALANCE%TYPE,
   jvId                MPAY_JVDETAILS.ID%TYPE,
   vRowsCount          OUT NUMBER)
AS
    vType NUMBER;
    vBalanceAfter NUMBER;
    vBalanceBefore NUMBER;
BEGIN
    SELECT BalanceTypeID, Balance
    INTO vType, vBalanceBefore
    FROM MPAY_Accounts
    WHERE ID = accountId;
    vBalanceAfter := vBalanceBefore + amount;
IF vType = 1 THEN --Debit
    UPDATE MPAY_Accounts SET balance = vBalanceAfter
    WHERE id = accountId AND vBalanceAfter >= 0;
ELSE IF vType = 2 THEN --Credit
    UPDATE MPAY_Accounts SET balance = vBalanceAfter
    WHERE id = accountId; -- AND vBalanceAfter <= 0;
ELSE
    UPDATE MPAY_Accounts SET balance = vBalanceAfter
    WHERE id = accountId;
END IF;
END IF;

vRowsCount := sql%rowcount;

IF vRowsCount = 0 THEN
    UPDATE MPAY_JVDETAILS
    SET BALANCEBEFORE = vBalanceBefore,
    BALANCEAFTER = vBalanceBefore
    WHERE ID = jvId;
ELSE
    UPDATE MPAY_JVDETAILS
    SET BALANCEBEFORE = vBalanceBefore,
    BALANCEAFTER = vBalanceAfter,
    ISPOSTED = 1
    WHERE ID = jvId;
END IF;
END PostJournalVoucher;