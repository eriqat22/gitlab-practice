DROP VIEW serviceinfo;

DROP TABLE serviceinfo;

CREATE VIEW serviceinfo AS
    ( SELECT DISTINCT
        name   servicename,
        FIRST_VALUE(t.z_creation_date) OVER(
            PARTITION BY name
            ORDER BY
                t.z_creation_date DESC
        ) servicelasttrn
    FROM
        mpay_transactions t,
        mpay_corpoarteservices s
    WHERE
        s.id = t.senderserviceid
        OR s.id = t.receiverserviceid
    );
DROP VIEW mpay_service_detail_rep;
DROP TABLE mpay_service_detail_rep;
CREATE OR REPLACE FORCE VIEW mpay_service_detail_rep AS
    SELECT
        (
            SELECT
                SYSDATE
            FROM
                dual
        ) extractstamp,
        c.name                            corporatename,
        s.name                            servicename,
        (
            SELECT
                status.description
            FROM
                jfw_wf_status status
            WHERE
                status.id = s.z_status_id
        ) status,
        s.description                     servicedescription,
        s.alias                           servicealias,
        CASE
            WHEN s.isblocked = 1 THEN 'Yes'
            ELSE 'No'
        END blocked,
        (
            SELECT
                name
            FROM
                mpay_banks bank
            WHERE
                s.bankid = bank.id
        ) servicebankname,
        c.externalacc                     servierbanknumber,
        s.iban                            serviceiban,
        account.accnumber                 servicempayaccount,
        account.balance                   servicebalance,
        CASE
            WHEN s.wallettype = 1 THEN 'Individual'
            ELSE 'Corporate'
        END wallettype,
        (
            SELECT
                name
            FROM
                mpay_messagetypes mt
            WHERE
                mt.id = s.paymenttypeid
        ) paymenttype,
        CASE
            WHEN s.isregistered = 1 THEN 'Yes'
            ELSE 'No'
        END registered,
        CASE
            WHEN s.isactive = 1 THEN 'Yes'
            ELSE 'No'
        END active,
        (
            SELECT
                name
            FROM
                mpay_notificationchannels channel
            WHERE
                channel.id = s.notificationchannelid
        ) notificationchannel,
        s.notificationreceiver,
        (
            SELECT
                name
            FROM
                mpay_profiles
            WHERE
                id = s.refprofileid
        ) profile,
        CASE
            WHEN s.chargeduration = 1 THEN 'Once'
            ELSE 'Monthly'
        END chargeduration,
        (
            SELECT
                name
            FROM
                mpay_servicescategories
            WHERE
                id = s.servicecategoryid
        ) servicecategory,
        (
            SELECT
                description
            FROM
                mpay_servicetypes
            WHERE
                id = s.servicetypeid
        ) servicetype,
        s.pinlastchanged                  pinlastchanges,
        s.retrycount,
        mpaytransaction.z_creation_date   lasttransactiondate,
        CASE mpaytransaction.reftypeid
            WHEN 1   THEN 'CR'
            WHEN 2   THEN 'DR'
        END lasttransactiontype,
        mpaytransaction.originalamount    lasttransactionamount,
        s.z_creation_date                 createddate,
        s.z_created_by                    createduser,
        s.z_updating_date                 modifieddate,
        s.z_updated_by                    modifieduser,
        s.z_deleted_on                    deleteddate,
        s.z_deleted_by                    deleteduser,
        s.id                              id,
        s.z_archive_on,
        s.z_assigned_user,
        s.z_archive_queued,
        s.z_archive_status,
        s.z_assigned_group,
        s.z_created_by,
        s.z_creation_date,
        s.z_deleted_by,
        s.z_deleted_flag,
        s.z_deleted_on,
        s.z_editable,
        s.z_locked_by,
        s.z_locked_until,
        s.z_org_id,
        s.z_tenant_id,
        s.z_updated_by,
        s.z_updating_date,
        s.z_workflow_id,
        s.z_ws_token,
        s.z_draft_status,
        s.z_draft_id,
        s.z_status_id
    FROM
        mpay_corpoarteservices s
        JOIN mpay_corporates c ON c.id = s.refcorporateid
        JOIN mpay_accounts account ON account.id IN (
            SELECT
                refaccountid
            FROM
                mpay_serviceaccounts serviceacc
            WHERE
                serviceacc.serviceid = s.id
        )
        LEFT JOIN mpay_transactions mpaytransaction ON mpaytransaction.receiverserviceid = s.id
                                                       OR mpaytransaction.senderserviceid = s.id
    WHERE
        c.clienttypeid = 9  AND mpaytransaction.z_creation_date IN (
            SELECT
                servicelasttrn
            FROM
                serviceinfo s
            WHERE
                s.servicename = s.name
        );
