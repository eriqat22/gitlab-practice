CREATE OR REPLACE PROCEDURE UPDATE_CREDIT_CLIENTS_LIMITS
(
    pACCOUNTID MPAY_CLIENTSLIMITS.ACCOUNTID%TYPE,
    pMESSAGETYPEID MPAY_CLIENTSLIMITS.MESSAGETYPEID%TYPE,
    pAMOUNT MPAY_CLIENTSLIMITS.DAILYCREDITAMOUNT%TYPE,
    pCOUNT MPAY_CLIENTSLIMITS.DAILYCOUNT%TYPE,
    pDATE MPAY_CLIENTSLIMITS.DAILYDATE%TYPE,
    rROWCOUNT OUT NUMBER)
    AS
    rID MPAY_CLIENTSLIMITS.ID%TYPE;
    vCount NUMBER;
BEGIN
SELECT COUNT(*) INTO vCount FROM MPAY_CLIENTSLIMITS WHERE ACCOUNTID = pACCOUNTID AND MESSAGETYPEID = pMESSAGETYPEID;
IF vCount = 0 THEN
    SELECT MPAY_CLIENTSLIMITS_SEQ.NEXTVAL INTO rID FROM DUAL;
    INSERT INTO MPAY_CLIENTSLIMITS (
       ID,
       ACCOUNTID,
       MESSAGETYPEID,
       DAILYDATE,
       WEEKNUMBER,
       MONTH,
       YEAR,
       DAILYCOUNT,
       DAILYAMOUNT,
       WEEKLYCOUNT,
       WEEKLYAMOUNT,
       MONTHLYCOUNT,
       MONTHLYAMOUNT,
       YEARLYCOUNT,
       YEARLYAMOUNT,
       DAILYCREDITAMOUNT,
       WEEKLYCREDITAMOUNT,
       MONTHLYCREDITAMOUNT,
       YEARLYCREDITAMOUNT)
    VALUES (rID,
     pACCOUNTID,
     pMESSAGETYPEID,
     TRUNC(pDATE),
     to_number(to_char(pDATE, 'WW')),
     EXTRACT(MONTH FROM pDATE),
     EXTRACT(YEAR FROM pDATE),
     0,
     0,
     0,
     0,
     0,
     0,
     0,
     0,
     pAMOUNT,
     pAMOUNT,
     pAMOUNT,
     pAMOUNT);

ELSE
DECLARE
    vYEAR MPAY_CLIENTSLIMITS.YEAR%TYPE;
    vDAILYDATE MPAY_CLIENTSLIMITS.DAILYDATE%TYPE;
    vWEEKNUMBER MPAY_CLIENTSLIMITS.WEEKNUMBER%TYPE;
    vMONTH MPAY_CLIENTSLIMITS.MONTH%TYPE;
    vDAILYCREDITAMOUNT MPAY_CLIENTSLIMITS.DAILYCREDITAMOUNT%TYPE;
    vWEEKLYCREDITAMOUNT MPAY_CLIENTSLIMITS.WEEKLYCREDITAMOUNT%TYPE;
    vMONTHLYCREDITAMOUNT MPAY_CLIENTSLIMITS.MONTHLYCREDITAMOUNT%TYPE;
    vYEARLYCREDITAMOUNT MPAY_CLIENTSLIMITS.YEARLYCREDITAMOUNT%TYPE;

BEGIN
SELECT YEAR, DAILYDATE, WEEKNUMBER, MONTH, DAILYCREDITAMOUNT,WEEKLYCREDITAMOUNT, MONTHLYCREDITAMOUNT, YEARLYCREDITAMOUNT
INTO vYEAR, vDAILYDATE, vWEEKNUMBER, vMONTH, vDAILYCREDITAMOUNT, vWEEKLYCREDITAMOUNT, vMONTHLYCREDITAMOUNT, vYEARLYCREDITAMOUNT
FROM MPAY_CLIENTSLIMITS WHERE ACCOUNTID = pACCOUNTID AND MESSAGETYPEID = pMESSAGETYPEID;
    IF vYEAR <> EXTRACT(YEAR FROM pDATE) THEN
        vYEAR := EXTRACT(YEAR FROM pDATE);
        vDAILYDATE := TRUNC(pDATE);
        vWEEKNUMBER := to_number(to_char(pDATE, 'WW'));
        vMONTH := EXTRACT(MONTH FROM pDATE);

        vDAILYCREDITAMOUNT := pAMOUNT;
        vWEEKLYCREDITAMOUNT := pAMOUNT;
        vMONTHLYCREDITAMOUNT := pAMOUNT;
        vYEARLYCREDITAMOUNT := pAMOUNT;
    ELSE
        vYEARLYCREDITAMOUNT := vYEARLYCREDITAMOUNT + pAMOUNT;
        IF vDAILYDATE <> TRUNC(pDATE) THEN
            vDAILYDATE := TRUNC(pDATE);
            vDAILYCREDITAMOUNT := pAMOUNT;
        ELSE
            vDAILYCREDITAMOUNT := vDAILYCREDITAMOUNT + pAMOUNT;
        END IF;

        IF vWEEKNUMBER <> to_number(to_char(pDATE, 'WW')) THEN
            vWEEKNUMBER := to_number(to_char(pDATE, 'WW'));
            vWEEKLYCREDITAMOUNT := pAMOUNT;
        ELSE
            vWEEKLYCREDITAMOUNT := vWEEKLYCREDITAMOUNT + pAMOUNT;
        END IF;

        IF vMONTH <> EXTRACT(MONTH FROM pDATE) THEN
            vMONTH := EXTRACT(MONTH FROM pDATE);
            vMONTHLYCREDITAMOUNT := pAMOUNT;
         ELSE
            vMONTHLYCREDITAMOUNT := vMONTHLYCREDITAMOUNT + pAMOUNT;
         END IF;
    END IF;

    IF vDAILYCREDITAMOUNT < 0 THEN
        vDAILYCREDITAMOUNT := 0;
    END IF;

    IF vWEEKLYCREDITAMOUNT < 0 THEN
        vWEEKLYCREDITAMOUNT := 0;
    END IF;

    IF vMONTHLYCREDITAMOUNT < 0 THEN
        vMONTHLYCREDITAMOUNT := 0;
    END IF;

    IF vYEARLYCREDITAMOUNT < 0 THEN
        vYEARLYCREDITAMOUNT := 0;
    END IF;

    UPDATE MPAY_CLIENTSLIMITS
    SET
        DAILYDATE = vDAILYDATE,
        WEEKNUMBER = vWEEKNUMBER,
        MONTH = vMONTH,
        YEAR = vYEAR,
        DAILYCREDITAMOUNT = vDAILYCREDITAMOUNT,
        WEEKLYCREDITAMOUNT = vWEEKLYCREDITAMOUNT,
        MONTHLYCREDITAMOUNT = vMONTHLYCREDITAMOUNT,
        YEARLYCREDITAMOUNT = vYEARLYCREDITAMOUNT
    WHERE ACCOUNTID = pACCOUNTID AND  MESSAGETYPEID = pMESSAGETYPEID;
END;
END IF;
rROWCOUNT := SQL%ROWCOUNT;
END;
