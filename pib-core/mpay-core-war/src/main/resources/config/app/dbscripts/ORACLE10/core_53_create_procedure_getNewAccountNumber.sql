create or replace PROCEDURE GetNewAccountNumber(
rNewSeqValue out NUMBER
)
AS
BEGIN
      SELECT ACCOUNT_NUMBER_SEQ.NEXTVAL INTO rNewSeqValue FROM DUAL;
END;