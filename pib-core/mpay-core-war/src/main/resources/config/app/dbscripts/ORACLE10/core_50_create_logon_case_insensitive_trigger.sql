CREATE OR REPLACE TRIGGER LOGON_CASEINSENS
AFTER LOGON
ON SCHEMA
begin
EXECUTE IMMEDIATE 'alter session set NLS_COMP=LINGUISTIC';
EXECUTE IMMEDIATE 'alter session set nls_sort=binary_ci';
end;