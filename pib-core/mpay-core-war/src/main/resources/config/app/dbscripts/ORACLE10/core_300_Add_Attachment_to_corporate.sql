BEGIN
UPDATE JFW_FORMS SET SHOWPREVIEW = 1 , SHOWATTACHMENTFIELD = 1 WHERE ID =  (SELECT FORM_ID FROM JFW_TABS
WHERE PORTLET_ID =  (SELECT id FROM JFW_PORTLETS WHERE PORTAL_ID = (SELECT ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_Corporates.NewPortal') AND NAME = 'Attachment'));

update JFW_TABS  set file_extensions ='.jpg,.png,.jpeg'
WHERE PORTLET_ID =  (SELECT id FROM JFW_PORTLETS WHERE PORTAL_ID = (SELECT ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_Corporates.NewPortal') AND NAME = 'Attachment');

UPDATE JFW_FORMS SET SHOWPREVIEW = 1 , SHOWATTACHMENTFIELD = 1 WHERE ID =  (SELECT FORM_ID FROM JFW_TABS
WHERE PORTLET_ID =  (SELECT id FROM JFW_PORTLETS WHERE PORTAL_ID = (SELECT ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_Corporates.EditPortal') AND NAME = 'Attachment'));

update JFW_TABS  set file_extensions ='.jpg,.png,.jpeg'
WHERE PORTLET_ID =  (SELECT id FROM JFW_PORTLETS WHERE PORTAL_ID = (SELECT ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_Corporates.EditPortal') AND NAME = 'Attachment');
END;