DROP VIEW MPAY_CUSTOMER_TRANS_REP;
DROP table MPAY_CUSTOMER_TRANS_REP;
/* Formatted on 12/23/2018 5:20:02 PM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW MPAY_CUSTOMER_TRANS_REP

AS
   SELECT mpaytransaction.id id,
          (SELECT SYSDATE FROM DUAL) EXTRACTSTAMP,
          mpaytransaction.reference transactionreference,
          mpaytransaction.z_creation_date trndate,
          mpaytransaction.z_creation_date trntime,
          (SELECT name
             FROM mpay_processingstatuses ps
            WHERE ps.id = mpaytransaction.processingstatusid)
             trnstatus,
          mpaytransaction.z_creation_date trnsactionsource,
          CASE mpaytransaction.reftypeid
             WHEN 1 THEN 'CR'
             WHEN 2 THEN 'DR'
          END
             trntype,
          (SELECT description
             FROM jfw_currencies
            WHERE id IN (SELECT currencyid FROM mpay_transactions))
             trnccy,
          mpaytransaction.originalamount TRNAMOUNT,
          NVL (senderservice.iban, sendermobile.iban) senderiban,
          NVL (sendercorporate.registrationid, sendercustomer.idnum)
             sendernin,
          NVL (senderservice.name, sendercustomer.fullname) sendername,
          NVL (senderservice.externalacc, sendermobile.externalacc)
             senderbankaccountnumber,
          NVL (senderserviceaccount.accnumber, sendermobileaccount.accnumber)
             senderaccountnumber,
          NVL (sendercorporate.mobilenumber, sendermobile.mobilenumber)
             sendermobilenumber,
          mpaytransaction.sendercharge senderchargesamount,
          NVL (senderservicejv.amount, sendermobilejv.amount) senderamount,
          NVL (senderservicejv.balancebefore, sendermobilejv.balancebefore)
             senderbalancebefore,
          NVL (senderservicejv.balanceafter, sendermobilejv.balanceafter)
             senderbalanceafter,
          CASE
             WHEN mpaytransaction.sendermobileid IS NOT NULL
             THEN
                (SELECT name
                   FROM mpay_banks bank
                  WHERE sendermobile.bankid = bank.id)
             ELSE
                (SELECT name
                   FROM mpay_banks bank
                  WHERE senderservice.bankid = bank.id)
          END
             senderbank,
          mpaytransaction.sendercharge sendervatamount,
          NVL (receiverservice.iban, receivermobile.iban) receiveriban,
          NVL (receivercorporate.registrationid, receivercustomer.idnum)
             receivernin,
          NVL (receiverservice.name, receivercustomer.fullname) receivername,
          NVL (receiverservice.externalacc, receivermobile.externalacc)
             receiverbankaccountnumber,
          NVL (receiverserviceaccount.accnumber,
               receivermobileaccount.accnumber)
             receiveraccountnumber,
          NVL (receivercorporate.mobilenumber, receivermobile.mobilenumber)
             receivermobilenumber,
          mpaytransaction.receivercharge receiverchargesamount,
          NVL (receiverservicejv.amount, receivermobilejv.amount)
             receivernetamount,
          NVL (receiverservicejv.balancebefore,
               receivermobilejv.balancebefore)
             RECEIVERBALANCEBEFORE,
          NVL (receiverservicejv.balanceafter, receivermobilejv.balanceafter)
             receiverbalanceafter,
          CASE
             WHEN mpaytransaction.receivermobileid IS NOT NULL
             THEN
                (SELECT name
                   FROM mpay_banks bank
                  WHERE receivermobile.bankid = bank.id)
             ELSE
                (SELECT name
                   FROM mpay_banks bank
                  WHERE receiverservice.bankid = bank.id)
          END
             receiverbank,
          mpaytransaction.receivercharge receivervatamount,
          NVL (sendercorporate.name, receivercorporate.name) merchantname,
          mpaytransaction.z_archive_on,
          mpaytransaction.z_assigned_user,
          mpaytransaction.z_archive_queued,
          mpaytransaction.z_archive_status,
          mpaytransaction.z_assigned_group,
          mpaytransaction.z_created_by,
          mpaytransaction.z_creation_date,
          mpaytransaction.z_deleted_by,
          mpaytransaction.z_deleted_flag,
          mpaytransaction.z_deleted_on,
          mpaytransaction.z_editable,
          mpaytransaction.z_locked_by,
          mpaytransaction.z_locked_until,
          mpaytransaction.z_org_id,
          mpaytransaction.z_tenant_id,
          mpaytransaction.z_updated_by,
          mpaytransaction.z_updating_date,
          mpaytransaction.z_workflow_id,
          mpaytransaction.z_ws_token,
          mpaytransaction.z_draft_status,
          mpaytransaction.z_draft_id,
          mpaytransaction.z_status_id
     FROM mpay_transactions mpaytransaction
          LEFT JOIN mpay_customermobiles sendermobile
             ON (sendermobile.id = mpaytransaction.sendermobileid)
          LEFT JOIN mpay_customermobiles receivermobile
             ON (receivermobile.id = mpaytransaction.receivermobileid)
          LEFT JOIN mpay_corpoarteservices senderservice
             ON (senderservice.id = mpaytransaction.senderserviceid)
          LEFT JOIN mpay_corpoarteservices receiverservice
             ON (receiverservice.id = mpaytransaction.receiverserviceid)
          LEFT JOIN mpay_accounts sendermobileaccount
             ON sendermobileaccount.id IN (SELECT refaccountid
                                             FROM mpay_mobileaccounts
                                            WHERE mobileid = sendermobile.id)
          LEFT JOIN mpay_accounts receivermobileaccount
             ON receivermobileaccount.id IN
                   (SELECT refaccountid
                      FROM mpay_mobileaccounts
                     WHERE mobileid = receivermobile.id)
          LEFT JOIN mpay_accounts senderserviceaccount
             ON senderserviceaccount.id IN
                   (SELECT refaccountid
                      FROM mpay_serviceaccounts
                     WHERE serviceid = senderservice.id)
          LEFT JOIN mpay_accounts receiverserviceaccount
             ON receiverserviceaccount.id IN
                   (SELECT refaccountid
                      FROM mpay_serviceaccounts
                     WHERE serviceid = receiverservice.id)
          LEFT JOIN mpay_customers sendercustomer
             ON (sendercustomer.id = sendermobile.refcustomerid)
          LEFT JOIN mpay_customers receivercustomer
             ON (receivercustomer.id = receivermobile.refcustomerid)
          LEFT JOIN mpay_corporates sendercorporate
             ON (sendercorporate.id = senderservice.refcorporateid)
          LEFT JOIN mpay_corporates receivercorporate
             ON (receivercorporate.id = receiverservice.refcorporateid)
          LEFT JOIN mpay_jvdetails sendermobilejv
             ON sendermobilejv.reftrsansactionid = mpaytransaction.id
                AND sendermobilejv.refaccountid = sendermobileaccount.id
          LEFT JOIN mpay_jvdetails receivermobilejv
             ON receivermobilejv.reftrsansactionid = mpaytransaction.id
                AND receivermobilejv.refaccountid = receivermobileaccount.id
          LEFT JOIN mpay_jvdetails senderservicejv
             ON senderservicejv.reftrsansactionid = mpaytransaction.id
                AND senderservicejv.refaccountid = senderserviceaccount.id
          LEFT JOIN mpay_jvdetails receiverservicejv
             ON receiverservicejv.reftrsansactionid = mpaytransaction.id
                AND receiverservicejv.refaccountid =
                       receiverserviceaccount.id;

