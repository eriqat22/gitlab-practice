BEGIN
INSERT INTO JFW_WF_STATUS(ID,Z_TENANT_ID,z_org_id,code,description,active) values (HIBERNATE_SEQUENCE.NEXTVAL,'SYSTEM',0,'SUCCESS','Success',1);
INSERT INTO JFW_WF_STATUS(ID,Z_TENANT_ID,z_org_id,code,description,active) values (HIBERNATE_SEQUENCE.NEXTVAL,'SYSTEM',0,'FAILED','Failed',1);
INSERT INTO JFW_WF_STATUS(ID,Z_TENANT_ID,z_org_id,code,description,active) values (HIBERNATE_SEQUENCE.NEXTVAL,'SYSTEM',0,'PARTIALY_SUCCESS','Partialy Success',1);

UPDATE JFW_FORMS SET SHOWPREVIEW = 1 , SHOWATTACHMENTFIELD = 1 WHERE ID = (SELECT FORM_ID FROM JFW_TABS WHERE PORTLET_ID = (SELECT ID FROM JFW_PORTLETS WHERE PORTAL_ID = (SELECT ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_BulkPayment.NewPortal') AND NAME = 'Bulk Payment'));

UPDATE JFW_FORMS SET SHOWPREVIEW = 1 , SHOWATTACHMENTFIELD = 1 WHERE ID = (SELECT FORM_ID FROM JFW_TABS WHERE PORTLET_ID = (SELECT ID FROM JFW_PORTLETS WHERE PORTAL_ID = (SELECT  ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_BulkRegistration.NewPortal') AND NAME = 'Bulk Registration'));

UPDATE JFW_PORTLETS SET PORTAL_ID = NULL WHERE ID = (SELECT ID FROM JFW_PORTLETS WHERE PORTAL_ID = (SELECT ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_BulkRegistration.NewPortal') AND NAME = 'Customers Management');
update JFW_ATTACHMNT_DICTIONARIES set MAXATTACHMENTS = 1 , MINATTACHMENTS = 1 where ENTITYNAME = 'com.progressoft.mpay.entities.MPAY_BulkPaymentAtt';
update JFW_VIEWS set SORTBY = 'creationDate' , SORTDIR = 'DESC' where name = 'MPAY_BulkPayment.View';
update JFW_ATTACHMNT_DICTIONARIES set MAXATTACHMENTS = 1 , MINATTACHMENTS = 1 where ENTITYNAME = 'com.progressoft.mpay.entities.MPAY_BulkRegistrationAtt';
update JFW_VIEWS set SORTBY = 'creationDate' , SORTDIR = 'DESC' where name = 'MPAY_BulkRegistration.View';
UPDATE  JFW_TABS set file_extensions ='.txt,.csv'  WHERE PORTLET_ID =  (SELECT ID FROM JFW_PORTLETS WHERE PORTAL_ID = (SELECT ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_BulkRegistration.NewPortal') and name ='Bulk Registration');
UPDATE  JFW_TABS set file_extensions ='.txt,.csv'  WHERE PORTLET_ID = (SELECT ID FROM JFW_PORTLETS WHERE PORTAL_ID =(SELECT ID FROM JFW_PORTALS WHERE DESCRIPTION = 'MPAY_BulkPayment.NewPortal') AND NAME = 'Bulk Payment');
END;