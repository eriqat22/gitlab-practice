alter sequence HIBERNATE_SEQUENCE increment by 1000000;
alter sequence JFW_OSCURRENTSTEP_SEQ increment by 10000000;
alter sequence JFW_OSHISTORYSTEP_SEQ increment by 10000000;
alter sequence JFW_OSWFENTRY_SEQ increment by 10000000;
alter sequence ORG_PREF_SEQ increment by 10000000;
alter sequence ORG_TYPE_SEQ increment by 10000000;
alter sequence USER_TYPE_SEQ increment by 10000000;
