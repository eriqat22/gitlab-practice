-- Delete view given its name ...
-- Still not completed:
--  - delete unused workflow and its its dependencies (e.g. actions)
--  - delete unused changle handlers on properties and portals.
--  - delete unused value-provides.


-- for enabling DBMS_OUTPUT.PUT_LINE() ...
set serveroutput ON ;

declare

  viewName varchar(100):= UPPER('set the view name here !!');
  viewExist number;
  viewID  varchar(100);
  workflowID varchar(100);
  entityId varchar(100);
  newPortalId varchar(100);
  editPortalId varchar(100);
  dummy varchar(100);
  BEGIN

  SELECT COUNT(*) into viewExist FROM JFW_VIEWS where UPPER(name) = viewName;
  IF viewExist > 0 THEN

    ------------------
    select id, Z_WORKFLOW_ID, ENTITY_ID, NEWITEM_PORTAL_ID, PORTAL_ID
      into viewId, workflowId, entityId, newPortalId, editPortalId
      from JFW_VIEWS where UPPER(name) = viewName;

    ------------------
    delete from JFW_FORM_FIELDS where PROPERTY_ID in (select id from JFW_PROPERTIES where ENTITY_ID = entityId);
            select count(*) into dummy from JFW_FORM_FIELDS where PROPERTY_ID in (select id from JFW_PROPERTIES where ENTITY_ID = entityId);

    ------------------
    delete from JFW_PROPERTIES where ENTITY_ID = entityId ;

    ------------------
    delete from JFW_FORMS where id  in
      (
        select FORM_ID from JFW_TABS where FORM_ID is not null and  PORTLET_ID in
          (
            select id from JFW_PORTLETS where PORTAL_ID in (newPortalId,editPortalId)
          )
      );

     ------------------

     delete from JFW_TABS where PORTLET_ID in
     (
        select id from JFW_PORTLETS where PORTAL_ID in (newPortalId,editPortalId)
     ) ;

     ------------------

     delete from  JFW_VIEWS where UPPER(name) = viewName;

     ------------------

    ------------------
     delete from JFW_PORTLETS where PORTAL_ID in (newPortalId,editPortalId);
     ------------------
     delete from JFW_PORTALS where id in (newPortalId,editPortalId);
     ------------------

    delete from JFW_ENTITIES where id = entityId;

  -------------------------------------


    -- to be rmoved later on once done !!
    ROLLBACK;

      --------
      select count(*) into dummy from JFW_FORM_FIELDS where PROPERTY_ID in (select id from JFW_PROPERTIES where ENTITY_ID = entityId);
      DBMS_OUTPUT.PUT_LINE(' JFW_FORM_FIELDS '|| dummy );

    -----------
     select count(*) into dummy from JFW_PROPERTIES where ENTITY_ID = entityId ;
      DBMS_OUTPUT.PUT_LINE(' JFW_PROPERTIES '|| dummy );

      --------------
       select count(id) into dummy from JFW_FORMS where id  in
      (
        select FORM_ID from JFW_TABS where FORM_ID is not null and  PORTLET_ID in
          (
            select id from JFW_PORTLETS where PORTAL_ID in (newPortalId,editPortalId)
          )
      );
          DBMS_OUTPUT.PUT_LINE(' JFW_FORMS '|| dummy );
      ----------------

      select count(*) into dummy from JFW_TABS where PORTLET_ID in
     (
        select id from JFW_PORTLETS where PORTAL_ID in (newPortalId,editPortalId)
     ) ;

       DBMS_OUTPUT.PUT_LINE(' JFW_TABS '|| dummy );
      --------------------

      select count(*) into dummy from  JFW_VIEWS where UPPER(name) = viewName;
      DBMS_OUTPUT.PUT_LINE(' JFW_VIEWS '|| dummy );

      ---------------------

    select count(*) into dummy from JFW_PORTLETS where PORTAL_ID in (newPortalId,editPortalId);
     DBMS_OUTPUT.PUT_LINE(' JFW_PORTLETS '|| dummy );
     -----------------------
     select count(*) into dummy from JFW_PORTALS where id in (newPortalId,editPortalId);
     DBMS_OUTPUT.PUT_LINE(' JFW_PORTALS '|| dummy );
     -----------------------
     select count(*) into dummy from JFW_ENTITIES where id = entityId;
     DBMS_OUTPUT.PUT_LINE(' JFW_ENTITIES '|| dummy );
     ---------------------
  END IF;
END;