DROP VIEW MPAY_P2P_TRANS_REP;
Drop table   MPAY_P2P_TRANS_REP;

/* Formatted on 12/23/2018 5:20:33 PM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW MPAY_P2P_TRANS_REP
as
SELECT
        (
            SELECT
                SYSDATE
            FROM
                dual
        ) EXTRACTDATE,
        mpaytransaction.z_creation_date   TRNDATE,
        mpaytransaction.z_creation_date   trntime,
        (
            SELECT
                name
            FROM
                mpay_processingstatuses ps
            WHERE
                ps.id = mpaytransaction.processingstatusid
        ) trnstatus,
        mpaytransaction.reference         TRNREFERNCENO,
        mpaytransaction.originalamount    trnamount,
        mpaytransaction.reasondesc        trndescription,
        sendercustomer.iban               senderiban,
        sendercustomer.idnum              sendernin,
        sendercustomer.fullname           sendername,
        sendercustomer.externalacc        senderbankaccountnumber,
        senderaccount.accnumber           senderaccountnumber,
        sendercustomer.mobilenumber       sendermobilenumber,
        mpaytransaction.sendercharge      senderchargesamount,
        senderjv.amount                   senderamount,
        senderjv.balancebefore            senderbalancebefore,
        senderjv.balanceafter             senderbalanceafter,
        (
            SELECT
                name
            FROM
                mpay_banks b
            WHERE
                b.id = sendercustomer.bankid
        ) senderbank,
        mpaytransaction.totalcharge       sendervatamount,
        receivercustomer.iban             RECEIVERIBAN,
        receivercustomer.idnum            RECEIVERNIN,
        receivercustomer.fullname         RECEIVERNAME,
        receivercustomer.externalacc      RECEIVERBANKACCOUNTNUMBER,
        receiveraccount.accnumber         RECEIVERaccountnumber,
        receivercustomer.mobilenumber     RECEIVERMOBILENUMBER,
        mpaytransaction.sendercharge      RECEIVERCHARGESAMOUNT,
        receiverjv.amount                 RECEIVERNETAMOUNT,
        receiverjv.balancebefore          RECEIVERBALANCEBEFORE,
        receiverjv.balanceafter           RECEIVERbalanceafter,
        (
            SELECT
                name
            FROM
                mpay_banks b
            WHERE
                b.id = sendercustomer.bankid
        ) RECEIVERbank,
        mpaytransaction.totalcharge       RECEIVERVATAMOUNT,
        mpaytransaction.z_archive_on,
        mpaytransaction.z_assigned_user,
        mpaytransaction.z_archive_queued,
        mpaytransaction.z_archive_status,
        mpaytransaction.z_assigned_group,
        mpaytransaction.z_created_by,
        mpaytransaction.z_creation_date,
        mpaytransaction.z_deleted_by,
        mpaytransaction.z_deleted_flag,
        mpaytransaction.z_deleted_on,
        mpaytransaction.z_editable,
        mpaytransaction.z_locked_by,
        mpaytransaction.z_locked_until,
        mpaytransaction.z_org_id,
        mpaytransaction.z_tenant_id,
        mpaytransaction.z_updated_by,
        mpaytransaction.z_updating_date,
        mpaytransaction.z_workflow_id,
        mpaytransaction.z_ws_token,
        mpaytransaction.z_draft_status,
        mpaytransaction.z_draft_id,
        mpaytransaction.z_status_id,
        mpaytransaction.id id

    FROM
        mpay_transactions mpaytransaction
        LEFT JOIN mpay_customermobiles sendermobile ON ( sendermobile.id = mpaytransaction.sendermobileid )
        LEFT JOIN mpay_customermobiles recivermobile ON ( recivermobile.id = mpaytransaction.receivermobileid )
        LEFT JOIN mpay_accounts senderaccount ON senderaccount.id IN (
            SELECT
                refaccountid
            FROM
                mpay_mobileaccounts
            WHERE
                mobileid = sendermobile.id
        )
        LEFT JOIN mpay_accounts receiveraccount ON receiveraccount.id IN (
            SELECT
                refaccountid
            FROM
                mpay_mobileaccounts
            WHERE
                mobileid = recivermobile.id
        )
        LEFT JOIN mpay_customers sendercustomer ON ( sendercustomer.id = sendermobile.refcustomerid )
        LEFT JOIN mpay_customers receivercustomer ON ( receivercustomer.id = recivermobile.refcustomerid )
        LEFT JOIN mpay_jvdetails senderjv ON senderjv.reftrsansactionid = mpaytransaction.id
                                             AND senderjv.refaccountid = senderaccount.id
        LEFT JOIN mpay_jvdetails receiverjv ON receiverjv.reftrsansactionid = mpaytransaction.id
                                               AND receiverjv.refaccountid = receiveraccount.id
    WHERE
        refoperationid = 1;
