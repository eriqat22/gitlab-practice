BEGIN
-- Hide the additional fields requested by Dinarak...

UPDATE JFW_FORM_FIELDS set VISIBLE=0 where PROPERTY_ID in 
(
  SELECT ID from JFW_PROPERTIES WHERE ENTITY_ID in 
  (
    select ID from JFW_ENTITIES WHERE NAME = 'com.progressoft.mpay.entities.MPAY_Customer'    
  ) 
  AND JFW_PROPERTIES.NAME in ('phoneThree','profession','monthlyIncome','beneficiaryNationalID')
);

update JFW_PORTLETS set VISIBLE=0 where PORTAL_ID in 
(
  select ID from JFW_PORTALS where id in 
  (
     select PORTAL_ID from JFW_VIEWS WHERE ENTITY_ID in ( select ID from JFW_ENTITIES WHERE NAME = 'com.progressoft.mpay.entities.MPAY_Customer' )
     Union
     select NEWITEM_PORTAL_ID from JFW_VIEWS  WHERE ENTITY_ID in ( select ID from JFW_ENTITIES WHERE NAME = 'com.progressoft.mpay.entities.MPAY_Customer' )
  )
)
AND NAME in
(
  'Additional Info',
  'Know Your Customer'
);

--update JFW_PROPERTIES set SHOWASRESULTCOLUMN=0  WHERE ENTITY_ID in
--(
--   select ID from JFW_ENTITIES WHERE NAME = 'com.progressoft.mpay.entities.MPAY_Customer'
--)
--AND JFW_PROPERTIES.NAME in
--(
--    'phoneThree',
--    'profession',
--    'monthlyIncome',
--    'beneficiaryNationalID',
--    'firstTextField',
--    'secondTextField',
--    'thirdTextField',
--    'forthTextField',
--    'fifthTextField',
--    'sixthTextField',
--    'firstCheckbox',
--    'secondCheckbox',
--    'firstNumberField',
--    'secondNumberField',
--    'firstDaterField',
--    'secondDaterField',
--    'kycid',
--    'kycCompleted'
--);
END;