CREATE USER EFIN IDENTIFIED BY efin
DEFAULT TABLESPACE MPAY_TAB01 
TEMPORARY TABLESPACE TEMP

GRANT ALL ON DBMS_CRYPTO TO EFIN

GRANT CREATE SESSION TO EFIN

GRANT QUERY REWRITE TO EFIN

GRANT UNLIMITED TABLESPACE TO EFIN

GRANT RESOURCE TO EFIN

GRANT CONNECT TO EFIN

GRANT SELECT_CATALOG_ROLE TO EFIN

GRANT SELECT ANY DICTIONARY TO EFIN

GRANT CREATE ANY SNAPSHOT TO EFIN

GRANT CREATE ANY SYNONYM TO EFIN

GRANT CREATE DATABASE LINK TO EFIN
