create or replace PROCEDURE CheckBalance (
   accountId           MPAY_ACCOUNTS.ID%TYPE,
   amount              MPAY_ACCOUNTS.BALANCE%TYPE,
   vBalanceAvailable   OUT NUMBER)
AS
    vType NUMBER;
    vBalanceAfter NUMBER;
    vBalanceBefore NUMBER;
    vIsAvailable BOOLEAN;
BEGIN
    SELECT BalanceTypeID, Balance
    INTO vType, vBalanceBefore
    FROM MPAY_Accounts
    WHERE ID = accountId;
    vIsAvailable := (vBalanceBefore + amount) > 0;
    IF vIsAvailable = TRUE OR vType = 2 THEN
        vBalanceAvailable := 1;
    ELSE
        vBalanceAvailable := 0;
    END IF;
END ;