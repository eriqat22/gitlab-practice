create or replace PROCEDURE UpdateBalances (
accountId MPAY_ACCOUNTS.ID%TYPE,
amount MPAY_ACCOUNTS.BALANCE%TYPE,
vRowsCount OUT NUMBER,
vBalanceBefore OUT MPAY_ACCOUNTS.BALANCE%TYPE,
vBalanceAfter OUT MPAY_ACCOUNTS.BALANCE%TYPE)
AS
vType NUMBER;
BEGIN
SELECT BalanceTypeID, Balance
INTO vType, vBalanceBefore
FROM MPAY_Accounts
WHERE ID = accountId for update;
vBalanceAfter := vBalanceBefore + amount;
IF vType = 1 THEN --Debit
UPDATE MPAY_Accounts SET balance = vBalanceAfter
WHERE id = accountId AND vBalanceAfter >= 0;
vRowsCount := sql%rowcount;
--COMMIT;
ELSE IF vType = 2 THEN
UPDATE MPAY_Accounts SET balance = vBalanceAfter
WHERE id = accountId AND vBalanceAfter <= 0;
vRowsCount := sql%rowcount;
--COMMIT;
ELSE
UPDATE MPAY_Accounts SET balance = vBalanceAfter
WHERE id = accountId;
vRowsCount := sql%rowcount;
--COMMIT;
END IF;
END IF;
END UpdateBalances;
