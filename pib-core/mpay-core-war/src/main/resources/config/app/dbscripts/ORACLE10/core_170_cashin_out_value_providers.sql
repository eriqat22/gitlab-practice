DECLARE 
  cashInReAccountValueProvider NUMBER;
  cashOutReAccountValueProvider NUMBER;
  cashInEntity NUMBER;
  cashOutEntity NUMBER;
  refAccInProp NUMBER;
  refAccOutProp NUMBER;

  serviceCashInValueProvider NUMBER;
  serviceCashOutValueProvider NUMBER;
  serviceCashInEntity NUMBER;
  serviceCashOutEntity NUMBER;
  refSrvAccInProp NUMBER;
  refSrvAccOutProp NUMBER;
BEGIN
  SELECT HIBERNATE_SEQUENCE.NEXTVAL INTO cashInReAccountValueProvider FROM DUAL;
  SELECT HIBERNATE_SEQUENCE.NEXTVAL INTO cashOutReAccountValueProvider FROM DUAL;
  SELECT id INTO cashInEntity FROM JFW_ENTITIES WHERE name = 'com.progressoft.mpay.entities.MPAY_CashIn';
  SELECT id INTO cashOutEntity FROM JFW_ENTITIES WHERE name = 'com.progressoft.mpay.entities.MPAY_CashOut';
  SELECT id INTO refAccInProp FROM JFW_PROPERTIES WHERE ENTITY_ID = cashInEntity and NAME = 'refMobileAccount';
  SELECT id INTO refAccOutProp FROM JFW_PROPERTIES WHERE ENTITY_ID = cashOutEntity and NAME = 'refMobileAccount';

  SELECT HIBERNATE_SEQUENCE.NEXTVAL INTO serviceCashInValueProvider FROM DUAL;
  SELECT HIBERNATE_SEQUENCE.NEXTVAL INTO serviceCashOutValueProvider FROM DUAL;
  SELECT id INTO serviceCashInEntity FROM JFW_ENTITIES WHERE name = 'com.progressoft.mpay.entities.MPAY_ServiceCashIn';
  SELECT id INTO serviceCashOutEntity FROM JFW_ENTITIES WHERE name = 'com.progressoft.mpay.entities.MPAY_ServiceCashOut';
  SELECT id INTO refSrvAccInProp FROM JFW_PROPERTIES WHERE ENTITY_ID = serviceCashInEntity and NAME = 'refServiceAccount';
  SELECT id INTO refSrvAccOutProp FROM JFW_PROPERTIES WHERE ENTITY_ID = serviceCashOutEntity and NAME = 'refServiceAccount';
  
  INSERT INTO JFW_VALUE_PROVIDERS(id,z_org_id,z_tenant_id,method,provider,type,description,JOINEXPRESSION,MASTER_DRAFT_MODE,KEYPROPERTY,VALUEPROPERTY) VALUES (cashInReAccountValueProvider,0,'SYSTEM','getMobileAccounts','MobileAccountCashInValueProvider','SpringBean','MobileAccountCashInValueProvider','${refCustMob.id}','NORMAL','id','refMobileAccount');
  INSERT INTO JFW_VALUE_PROVIDERS(id,z_org_id,z_tenant_id,method,provider,type,description,JOINEXPRESSION,MASTER_DRAFT_MODE,KEYPROPERTY,VALUEPROPERTY) VALUES (cashOutReAccountValueProvider,0,'SYSTEM','getMobileAccounts','MobileAccountCashOutValueProvider','SpringBean','MobileAccountCashOutValueProvider','${refCustMob.id}','NORMAL','id','refMobileAccount');
  UPDATE JFW_PROPERTIES SET TYPE_ID = 4,VALUES_ID = cashInReAccountValueProvider WHERE id = refAccInProp;
  UPDATE JFW_PROPERTIES SET TYPE_ID = 4,VALUES_ID = cashOutReAccountValueProvider WHERE id = refAccOutProp;

  INSERT INTO JFW_VALUE_PROVIDERS(id,z_org_id,z_tenant_id,method,provider,type,description,JOINEXPRESSION,MASTER_DRAFT_MODE,KEYPROPERTY,VALUEPROPERTY) VALUES (serviceCashInValueProvider,0,'SYSTEM','getServiceAccounts','ServiceAccountCashValueProvider','SpringBean','ServiceAccountCashValueProvider','${refCorporateService.id}','NORMAL','id','refServiceAccount');
  INSERT INTO JFW_VALUE_PROVIDERS(id,z_org_id,z_tenant_id,method,provider,type,description,JOINEXPRESSION,MASTER_DRAFT_MODE,KEYPROPERTY,VALUEPROPERTY) VALUES (serviceCashOutValueProvider,0,'SYSTEM','getServiceAccounts','ServiceAccountCashValueProvider','SpringBean','ServiceAccountCashValueProvider','${refCorporateService.id}','NORMAL','id','refServiceAccount');
  UPDATE JFW_PROPERTIES SET TYPE_ID = 4,VALUES_ID = serviceCashInValueProvider WHERE id = refSrvAccInProp;
  UPDATE JFW_PROPERTIES SET TYPE_ID = 4,VALUES_ID = serviceCashOutValueProvider WHERE id = refSrvAccOutProp;
END;