DROP VIEW MPAY_Corporate_Detail_Rep;
DROP table MPAY_Corporate_Detail_Rep;
/* Formatted on 12/23/2018 5:19:32 PM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW MPAY_Corporate_Detail_Rep

AS
   SELECT (SELECT SYSDATE FROM DUAL) extratstamp,
          c.id id,
          c.name shortname,
          c.description fullname,
          c.registrationdate,
          (SELECT name
             FROM mpay_idtypes TYPE
            WHERE TYPE.id = c.idtypeid)
             idtype,
          c.registrationid,
          (SELECT s.description
             FROM jfw_wf_status s
            WHERE s.id = c.z_status_id)
             status,
          CASE WHEN c.isregistered = 1 THEN 'Yes' ELSE 'No' END registered,
          CASE WHEN c.isactive = 1 THEN 'Yes' ELSE 'No' END active,
          (SELECT name
             FROM mpay_languages l
            WHERE c.preflangid = l.id)
             notificationslanguage,
          c.pobox,
          c.zipcode,
          c.streetname street,
          (SELECT name
             FROM mpay_cities city
            WHERE city.id = c.cityid)
             city,
          (SELECT country.country_name
             FROM jfw_countries country
            WHERE c.refcountryid = country.id)
             country,
          c.phoneone PHONE1,
          c.mobilenumber mobileno,
          c.externalacc BANKACCOUNTNUMBER,
          c.iban,
          (SELECT name
             FROM mpay_messagetypes mt
            WHERE mt.id = c.paymenttypeid)
             paymenttype,
          (SELECT COUNT (*)
             FROM mpay_corpoarteservices
            WHERE refcorporateid = c.id)
             noofservices,
          c.z_created_by addedby,
          c.z_deleted_by deletedby,
          c.approvalnote,
          c.rejectionnote,
          c.z_creation_date createddate,
          c.z_created_by createduser,
          c.z_updating_date modifieddate,
          c.z_updated_by modifieduser,
          c.z_deleted_on deleteddate,
          c.z_deleted_by deleteduser,
          c.z_archive_on,
          c.z_assigned_user,
          c.z_archive_queued,
          c.z_archive_status,
          c.z_assigned_group,
          c.z_created_by,
          c.z_creation_date,
          c.z_deleted_by,
          c.z_deleted_flag,
          c.z_deleted_on,
          c.z_editable,
          c.z_locked_by,
          c.z_locked_until,
          c.z_org_id,
          c.z_tenant_id,
          c.z_updated_by,
          c.z_updating_date,
          c.z_workflow_id,
          c.z_ws_token,
          c.z_draft_status,
          c.z_draft_id,
          c.z_status_id
     FROM mpay_corporates c
    WHERE c.clienttypeid = 9;
    
    
    


