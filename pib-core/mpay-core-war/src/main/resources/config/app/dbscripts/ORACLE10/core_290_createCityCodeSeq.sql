declare
  sequenceName varchar(100):= UPPER('CITY_CODE_SEQ');
  sequenceExist number;
BEGIN  
  SELECT COUNT(*) into sequenceExist FROM all_sequences where upper(sequence_name) = sequenceName;
  IF sequenceExist > 0 THEN
    EXECUTE IMMEDIATE 'DROP SEQUENCE ' || sequenceName;
  END IF;
  EXECUTE IMMEDIATE 'CREATE SEQUENCE '|| sequenceName || ' START WITH 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 NOCYCLE CACHE 20 NOORDER';
END;