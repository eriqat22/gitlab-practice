set escape on
update jfw_value_providers set JOINEXPRESSION = 'statusId.code = ''100005'' AND isActive = 1' where description = 'MPAY_CustomerMobiles.refCustomer.MPAY_Customers';
update jfw_value_providers set JOINEXPRESSION = 'refCustomer.statusId.code = ''100005'' AND statusId.code = ''100105'' AND isActive = 1' where description = 'MPAY_MobileAccounts.mobile.MPAY_CustomerMobiles';
update jfw_value_providers set JOINEXPRESSION = 'statusId.code = ''200005'' AND isActive = 1' where description = 'MPAY_CorpoarteServices.refCorporate.MPAY_Corporates';
update jfw_value_providers set JOINEXPRESSION = 'refCorporate.statusId.code = ''200005'' AND statusId.code = ''200105'' AND isActive = 1' where description = 'MPAY_ServiceAccounts.service.MPAY_CorpoarteServices';