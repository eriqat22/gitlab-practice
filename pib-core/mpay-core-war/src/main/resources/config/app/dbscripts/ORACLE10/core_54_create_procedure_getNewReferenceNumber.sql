create or replace PROCEDURE GetNewReferenceNumber(
rNewSeqValue out NUMBER
)
AS
BEGIN
      SELECT REFERENCE_NUMBER_SEQ.NEXTVAL INTO rNewSeqValue FROM DUAL;
END;