DROP VIEW mobile_trn_info;

CREATE VIEW mobile_trn_info AS
    SELECT DISTINCT
        m.id   mobileid,
        FIRST_VALUE(t.z_creation_date) OVER(
            PARTITION BY m.id
            ORDER BY
                t.z_creation_date DESC
        ) mobilelasttrn,
        m.mobilenumber
    FROM
        mpay_customermobiles m,
        mpay_transactions t
    WHERE
        m.refcustomerid IN (
            SELECT
                id
            FROM
                mpay_customers
        )
        AND t.sendermobileid = m.id
        OR t.receivermobileid = m.id;

DROP VIEW mpay_customer_rep;

DROP TABLE mpay_customer_rep;

CREATE OR REPLACE FORCE VIEW mpay_customer_rep AS
    SELECT
        (
            SELECT
                SYSDATE
            FROM
                dual
        ) extractstamp,
        c.fullname                       customername,
        CASE
            WHEN c.externalacc IS NULL THEN 'NON SAIB'
            ELSE 'SAIB'
        END customertype,
        c.z_status_id,
        (
            SELECT
                description
            FROM
                jfw_wf_status
            WHERE
                id = c.z_status_id
        ) status,
        CASE
            WHEN c.isactive = 1 THEN 'Yes'
            ELSE 'No'
        END active,
        c.idnum                          nin,
        (
            SELECT
                country_name
            FROM
                jfw_countries
            WHERE
                id = c.nationalityid
        ) nationality,
        c.idexpirydate,
        (
            SELECT
                name
            FROM
                mpay_idtypes
            WHERE
                mpay_idtypes.id = c.idtypeid
        ) idtype,
        c.dob,
        m.mobilenumber                   customermobile,
        c.iban,
        mpayaccount.accnumber            accountnumber,
        c.externalacc                    bankaccnumber,
        mpayaccount.balance              balances,
        CASE
            WHEN c.preflangid = 1 THEN 'en'
            ELSE 'Ar'
        END languageindicator,
        (
            SELECT
                name
            FROM
                mpay_notificationchannels
            WHERE
                id = c.notificationshowtype
        ) notificationchannel,
        cd.deviceid                      deviceid,
        cd.devicename                    devicename,
        (
            SELECT
                description
            FROM
                jfw_wf_status
            WHERE
                id = cd.z_status_id
        ) devicestatus,
        CASE
            WHEN cd.activedevice = 1 THEN 'Yes'
            ELSE 'No'
        END deviceactive,
        CASE
            WHEN cd.isstolen = 1 THEN 'Yes'
            ELSE 'No'
        END devicestolen,
        cd.retrycount                    deviceretry,
        c.maxnumberofdevices             maximumnumberofdevices,
        cd.lastlogintime                 lastlogondate,
        c.address                        customeralladdressdetails,
        CASE
            WHEN c.gender = 1 THEN 'Male '
            ELSE 'Female'
        END gender,
        c.email                          emailaddress,
        'Active' dormancyflag,
        ma.lasttransactiondate           lasttransactiondate,
        CASE
            WHEN mpaytransaction.reftypeid = 1 THEN 'CR'
            ELSE 'DR'
        END lasttransactiontype,
        mpaytransaction.originalamount   lasttransactionamount,
        c.z_created_by                   createduser,
        c.z_creation_date                createddate,
        c.z_updated_by                   modifieduser,
        c.z_updating_date                modifieddate,
        c.z_deleted_on                   deleteddate,
        c.z_deleted_by                   deleteduser,
        c.id,
        c.z_updating_date,
        c.z_archive_on,
        c.z_assigned_user,
        c.z_archive_queued,
        c.z_archive_status,
        c.z_assigned_group,
        c.z_created_by,
        c.z_creation_date,
        c.z_deleted_by,
        c.z_deleted_flag,
        c.z_deleted_on,
        c.z_editable,
        c.z_locked_by,
        c.z_locked_until,
        c.z_org_id,
        c.z_tenant_id,
        c.z_workflow_id,
        c.z_ws_token,
        c.z_draft_status,
        c.z_draft_id,
        c.z_updated_by
    FROM
        mpay_customers c
        JOIN mpay_customermobiles m ON m.refcustomerid = c.id
        JOIN mpay_mobileaccounts ma ON ma.mobileid = m.id
        JOIN mpay_accounts mpayaccount ON ma.refaccountid = mpayaccount.id
        LEFT JOIN mpay_transactions mpaytransaction ON mpaytransaction.receivermobileid = m.id
                             OR mpaytransaction.sendermobileid = m.id
        LEFT JOIN mpay_customerdevices cd ON cd.refcustomermobileid = m.id
        where  mpaytransaction.z_creation_date IN (
            SELECT
                mobilelasttrn
            FROM
                mobile_trn_info mobiletrn
            WHERE
                mobiletrn.mobileid = m.id
        );


