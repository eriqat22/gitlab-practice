CREATE OR REPLACE PROCEDURE UpdateRegFileRecordsCount (
   fileid    IN     NUMBER,
   vrowscount      OUT NUMBER,
   vRecordsCount   OUT NUMBER)
AS

BEGIN

    update MPAY_Intg_Reg_Files
    set REGFILEPROCESSEDRECORDS = (REGFILEPROCESSEDRECORDS + 1)
    where ID=fileid;
    vrowscount := sql%rowcount;
   COMMIT;
    SELECT REGFILEPROCESSEDRECORDS  INTO vRecordsCount  FROM MPAY_Intg_Reg_Files where ID=fileid;

END   UpdateRegFileRecordsCount;
