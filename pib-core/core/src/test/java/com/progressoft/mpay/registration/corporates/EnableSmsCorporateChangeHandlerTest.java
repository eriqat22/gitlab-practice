package com.progressoft.mpay.registration.corporates;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.UIFieldAttributes;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Corporate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class EnableSmsCorporateChangeHandlerTest {

    private EnableSmsCorporateChangeHandler handler;
    private WfChangeHandlerContext<MPAY_Corporate> handlerContext;
    private ChangeHandlerContext context;

    @BeforeEach
    public void setUp(){
        handler = new EnableSmsCorporateChangeHandler();
        context = new ChangeHandlerContext();
        handlerContext = new WfChangeHandlerContext<>(context);
    }


    @Test
    public void givenCorporateWithEnableSmsTrue_ThenHandlerShouldBeChangeFieldToRequiredTrue(){
        context.setEntity(getCorporate(true));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_Corporate.MOBILE_NUMBER, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertTrue(uiAttribute.isRequired());
    }

    @Test
    public void givenCorporateWithEnableSmsFalse_ThenHandlerShouldBeChangeFieldToRequiredFalse(){
        context.setEntity(getCorporate(false));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_Corporate.MOBILE_NUMBER, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertFalse(uiAttribute.isRequired());
    }

    private MPAY_Corporate getCorporate(boolean enableSms) {
        MPAY_Corporate service = new MPAY_Corporate();
        service.setEnableSMS(enableSms);
        return service;
    }

    private UIFieldAttributes getUiAttribute() {
        UIFieldAttributes attributes = new UIFieldAttributes();
        attributes.setRequired(false);
        return attributes;
    }

}