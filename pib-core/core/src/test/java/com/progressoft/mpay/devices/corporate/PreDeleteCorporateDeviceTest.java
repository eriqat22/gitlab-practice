package com.progressoft.mpay.devices.corporate;

import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.test.FakeItemDao;
import com.progressoft.jfw.workflow.WfContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PreDeleteCorporateDeviceTest {
    private PreDeleteCorporateDevice deleteCorporateDevice;
    private WfContext<MPAY_CorporateDevice> context ;
    private Map<String,Object> vars;
    private FakeItemDao itemDao;

    @BeforeEach
    public void setUp(){
        vars = new HashMap<>();
        itemDao = new FakeItemDao();
        context= new WfContext<>(vars,null,null);
        deleteCorporateDevice = new PreDeleteCorporateDevice();
        deleteCorporateDevice.setItemDao(itemDao);
    }

    @Test
    public void givenFourDevicesForOneService_WhenDeleteDefaultDevice_ThenFunctionShouldBeSetOldestDeviceAsDefaultDevice(){
        itemDao.filterItems();
        initData();
        MPAY_CorporateDevice device = itemDao.getItem(MPAY_CorporateDevice.class, 1L);
        vars.put(WorkflowService.WF_ARG_BEAN,device);
        context = new WfContext<>(vars,null,null);
        deleteCorporateDevice.execute(context);
        assertFalse(device.getIsDefault());
        assertTrue(itemDao.getItem(MPAY_CorporateDevice.class,2L).getIsDefault());
        assertFalse(itemDao.getItem(MPAY_CorporateDevice.class,3L).getIsDefault());
        assertFalse(itemDao.getItem(MPAY_CorporateDevice.class,4L).getIsDefault());
    }

    private void initData() {
        MPAY_CorpoarteService service = getService();
        itemDao.add(getDevice(1L,new Timestamp(getFixedTime()),true,service));
        itemDao.add(getDevice(2L,new Timestamp(getFixedTime()-2000),false,service));
        itemDao.add(getDevice(3L,new Timestamp(getFixedTime()-1500),false,service));
        itemDao.add(getDevice(4L,new Timestamp(getFixedTime()-1000),false,service));
    }

    private MPAY_CorpoarteService getService(){
        MPAY_CorpoarteService service = new MPAY_CorpoarteService();
        service.setId(1L);
        return service;
    }

    private MPAY_CorporateDevice getDevice(Long id, Timestamp creationDate,boolean isDefault,MPAY_CorpoarteService service){
        MPAY_CorporateDevice device = new MPAY_CorporateDevice();
        device.setId(id);
        device.setIsDefault(isDefault);
        device.setRefCorporateService(service);
        device.setCreationDate(creationDate);
        return device;
    }

    private Long getFixedTime(){
        return 1600850225294L;
    }
}