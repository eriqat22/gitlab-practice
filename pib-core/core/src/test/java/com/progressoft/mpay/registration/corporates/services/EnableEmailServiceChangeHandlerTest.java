package com.progressoft.mpay.registration.corporates.services;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.UIFieldAttributes;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class EnableEmailServiceChangeHandlerTest {

    private EnableEmailServiceChangeHandler handler;
    private WfChangeHandlerContext<MPAY_CorpoarteService> handlerContext;
    private ChangeHandlerContext context;

    @BeforeEach
    public void setUp(){
        handler = new EnableEmailServiceChangeHandler();
        context = new ChangeHandlerContext();
        handlerContext = new WfChangeHandlerContext<>(context);
    }

    @Test
    public void givenServiceWithEnableEmailTrue_ThenHandlerShouldBeChangeEmailFieldToRequiredTrue(){
        context.setEntity(getCorporateService(true));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_CorpoarteService.EMAIL, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertTrue(uiAttribute.isRequired());
    }

    @Test
    public void givenServiceWithEnableEmailFalse_ThenHandlerShouldBeChangeEmailFieldToRequiredFalse(){
        context.setEntity(getCorporateService(false));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_CorpoarteService.EMAIL, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertFalse(uiAttribute.isRequired());
    }

    private MPAY_CorpoarteService getCorporateService(boolean enableEmail) {
        MPAY_CorpoarteService service = new MPAY_CorpoarteService();
        service.setEnableEmail(enableEmail);
        return service;
    }

    private UIFieldAttributes getUiAttribute() {
        UIFieldAttributes attributes = new UIFieldAttributes();
        attributes.setRequired(false);
        return attributes;
    }

}