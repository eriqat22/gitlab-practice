package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.AppException;
import com.progressoft.jfw.AppValidationException;
import com.progressoft.jfw.workflow.WfContext;
import com.progressoft.mpay.entities.MPAY_Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CustomerEnableChannelsValidatorTest {

    private CustomerEnableChannelsValidator validator;
    private WfContext<MPAY_Customer> context;
    private Map<String, Object> vars;

    @BeforeEach
    public void setUp() {
        validator = new CustomerEnableChannelsValidator();
        vars = new HashMap<>();
        context = new WfContext<>(vars, null, null);
    }

    @Test
    public void givenEnabledSmsAndEmptyMobileNumber_ThenValidatorShouldBeThrownException() {
        vars.put("bean", getCustomer(true, false, null, null));
        context = new WfContext<>(vars, null, null);
        AppValidationException appException = assertThrows(AppValidationException.class, () -> validator.validate(context));
        assertEquals("Please enter mobile number",appException.getMessage());
    }

    @Test
    public void givenEnabledEmailAndEmptyEmail_ThenValidatorShouldBeThrownException() {
        vars.put("bean", getCustomer(false, true, null, null));
        context = new WfContext<>(vars, null, null);
        AppValidationException appException = assertThrows(AppValidationException.class, () -> validator.validate(context));
        assertEquals("Please enter email address",appException.getMessage());
    }

    @Test
    public void givenEnabledEmailAndFilledEmail_ThenValidatorShouldNotThrownException() {
        vars.put("bean", getCustomer(false, true, null, "email@email.com"));
        context = new WfContext<>(vars, null, null);
        assertDoesNotThrow(() -> validator.validate(context));
    }

    @Test
    public void givenEnabledSmsAndFilledMobileNumber_ThenValidatorShouldNotThrownException() {
        vars.put("bean", getCustomer(true, false, "0788888888", null));
        context = new WfContext<>(vars, null, null);
        assertDoesNotThrow(() -> validator.validate(context));
    }



    private MPAY_Customer getCustomer(boolean isEnableSms, boolean isEnableEmail, String mobileNumber, String email) {
        MPAY_Customer customer = new MPAY_Customer();
        customer.setEnableSMS(isEnableSms);
        customer.setEnableEmail(isEnableEmail);
        customer.setEmail(email);
        customer.setMobileNumber(mobileNumber);
        return customer;
    }

}