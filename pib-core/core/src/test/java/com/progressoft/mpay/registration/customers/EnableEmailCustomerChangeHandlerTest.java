package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.UIFieldAttributes;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class EnableEmailCustomerChangeHandlerTest {
    private EnableEmailCustomerChangeHandler handler;
    private WfChangeHandlerContext<MPAY_Customer> handlerContext;
    private ChangeHandlerContext context;

    @BeforeEach
    public void setUp(){
        handler = new EnableEmailCustomerChangeHandler();
        context = new ChangeHandlerContext();
        handlerContext = new WfChangeHandlerContext<>(context);
    }

    @Test
    public void givenCustomerWithEnableEmailTrue_ThenHandlerShouldBeChangeEmailFieldToRequiredTrue(){
        context.setEntity(getCustomer(true));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_Customer.EMAIL, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertTrue(uiAttribute.isRequired());
    }

    @Test
    public void givenCustomerWithEnableEmailFalse_ThenHandlerShouldBeChangeEmailFieldToRequiredFalse(){
        context.setEntity(getCustomer(false));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_Customer.EMAIL, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertFalse(uiAttribute.isRequired());
    }

    private MPAY_Customer getCustomer(boolean enableEmail) {
        MPAY_Customer mobile = new MPAY_Customer();
        mobile.setEnableEmail(enableEmail);
        return mobile;
    }

    private UIFieldAttributes getUiAttribute() {
        UIFieldAttributes attributes = new UIFieldAttributes();
        attributes.setRequired(false);
        return attributes;
    }

}