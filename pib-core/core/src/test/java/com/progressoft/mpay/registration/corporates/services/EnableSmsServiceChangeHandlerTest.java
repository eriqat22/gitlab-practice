package com.progressoft.mpay.registration.corporates.services;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.UIFieldAttributes;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class EnableSmsServiceChangeHandlerTest  {
    private EnableSmsServiceChangeHandler handler;
    private WfChangeHandlerContext<MPAY_CorpoarteService> handlerContext;
    private ChangeHandlerContext context;

    @BeforeEach
    public void setUp(){
        handler = new EnableSmsServiceChangeHandler();
        context = new ChangeHandlerContext();
        handlerContext = new WfChangeHandlerContext<>(context);
    }


    @Test
    public void givenServiceWithEnableSmsTrue_ThenHandlerShouldBeChangeFieldToRequiredTrue(){
        context.setEntity(getCorporateService(true));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_CorpoarteService.MOBILE_NUMBER, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertTrue(uiAttribute.isRequired());
    }

    @Test
    public void givenServiceWithEnableSmsFalse_ThenHandlerShouldBeChangeFieldToRequiredFalse(){
        context.setEntity(getCorporateService(false));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_CorpoarteService.MOBILE_NUMBER, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertFalse(uiAttribute.isRequired());
    }

    private MPAY_CorpoarteService getCorporateService(boolean enableSms) {
        MPAY_CorpoarteService service = new MPAY_CorpoarteService();
        service.setEnableSMS(enableSms);
        return service;
    }

    private UIFieldAttributes getUiAttribute() {
        UIFieldAttributes attributes = new UIFieldAttributes();
        attributes.setRequired(false);
        return attributes;
    }

}