package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.UIFieldAttributes;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class EnableEmailMobileChangeHandlerTest {

    private EnableEmailMobileChangeHandler handler;
    private WfChangeHandlerContext<MPAY_CustomerMobile> handlerContext;
    private ChangeHandlerContext context;

    @BeforeEach
    public void setUp(){
        handler = new EnableEmailMobileChangeHandler();
        context = new ChangeHandlerContext();
        handlerContext = new WfChangeHandlerContext<>(context);
    }

    @Test
    public void givenMobileWithEnableEmailTrue_ThenHandlerShouldBeChangeEmailFieldToRequiredTrue(){
        context.setEntity(getCustomerMobile(true));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_CustomerMobile.EMAIL, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertTrue(uiAttribute.isRequired());
    }

    @Test
    public void givenMobileWithEnableEmailFalse_ThenHandlerShouldBeChangeEmailFieldToRequiredFalse(){
        context.setEntity(getCustomerMobile(false));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_CustomerMobile.EMAIL, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertFalse(uiAttribute.isRequired());
    }

    private MPAY_CustomerMobile getCustomerMobile(boolean enableEmail) {
        MPAY_CustomerMobile mobile = new MPAY_CustomerMobile();
        mobile.setEnableEmail(enableEmail);
        return mobile;
    }

    private UIFieldAttributes getUiAttribute() {
        UIFieldAttributes attributes = new UIFieldAttributes();
        attributes.setRequired(false);
        return attributes;
    }
}