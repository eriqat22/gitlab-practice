package com.progressoft.mpay.registration.corporates;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.UIFieldAttributes;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Corporate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EnableEmailCorporateChangeHandlerTest {
    private EnableEmailCorporateChangeHandler handler;
    private WfChangeHandlerContext<MPAY_Corporate> handlerContext;
    private ChangeHandlerContext context;

    @BeforeEach
    public void setUp(){
        handler = new EnableEmailCorporateChangeHandler();
        context = new ChangeHandlerContext();
        handlerContext = new WfChangeHandlerContext<>(context);
    }

    @Test
    public void givenCorporateWithEnableEmailTrue_ThenHandlerShouldBeChangeEmailFieldToRequiredTrue(){
        context.setEntity(getCorporate(true));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_Corporate.EMAIL, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertTrue(uiAttribute.isRequired());
    }

    @Test
    public void givenCorporateWithEnableEmailFalse_ThenHandlerShouldBeChangeEmailFieldToRequiredFalse(){
        context.setEntity(getCorporate(false));
        UIFieldAttributes uiAttribute = getUiAttribute();
        context.setFieldsAttributes(Collections.singletonMap(MPAY_Corporate.EMAIL, uiAttribute));
        handlerContext = new WfChangeHandlerContext<>(context);
        assertFalse(uiAttribute.isRequired());
        handler.handle(handlerContext);
        assertFalse(uiAttribute.isRequired());
    }

    private MPAY_Corporate getCorporate(boolean enableEmail) {
        MPAY_Corporate service = new MPAY_Corporate();
        service.setEnableEmail(enableEmail);
        return service;
    }

    private UIFieldAttributes getUiAttribute() {
        UIFieldAttributes attributes = new UIFieldAttributes();
        attributes.setRequired(false);
        return attributes;
    }

}