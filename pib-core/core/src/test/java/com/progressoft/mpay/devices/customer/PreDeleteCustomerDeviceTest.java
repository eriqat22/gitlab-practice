package com.progressoft.mpay.devices.customer;

import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.test.FakeItemDao;
import com.progressoft.jfw.workflow.WfContext;
import com.progressoft.mpay.entities.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PreDeleteCustomerDeviceTest {
    private PreDeleteCustomerDevice deleteCustomerDevice;
    private WfContext<MPAY_CustomerDevice> context ;
    private Map<String,Object> vars;
    private FakeItemDao itemDao;

    @BeforeEach
    public void setUp(){
        vars = new HashMap<>();
        itemDao = new FakeItemDao();
        context= new WfContext<>(vars,null,null);
        deleteCustomerDevice = new PreDeleteCustomerDevice();
        deleteCustomerDevice.setItemDao(itemDao);
    }

    @Test
    public void givenFourDevicesForOneService_WhenDeleteDefaultDevice_ThenFunctionShouldBeSetOldestDeviceAsDefaultDevice(){
        itemDao.filterItems();
        initData();
        MPAY_CustomerDevice device = itemDao.getItem(MPAY_CustomerDevice.class, 1L);
        vars.put(WorkflowService.WF_ARG_BEAN,device);
        context = new WfContext<>(vars,null,null);
        deleteCustomerDevice.execute(context);
        assertFalse(device.getIsDefault());
        assertTrue(itemDao.getItem(MPAY_CustomerDevice.class,2L).getIsDefault());
        assertFalse(itemDao.getItem(MPAY_CustomerDevice.class,3L).getIsDefault());
        assertFalse(itemDao.getItem(MPAY_CustomerDevice.class,4L).getIsDefault());
    }

    private void initData() {
        MPAY_CustomerMobile mobile = getMobile();
        itemDao.add(getDevice(1L,new Timestamp(getFixedTime()),true,mobile));
        itemDao.add(getDevice(2L,new Timestamp(getFixedTime()-2000),false,mobile));
        itemDao.add(getDevice(3L,new Timestamp(getFixedTime()-1500),false,mobile));
        itemDao.add(getDevice(4L,new Timestamp(getFixedTime()-1000),false,mobile));
    }

    private MPAY_CustomerMobile getMobile(){
        MPAY_CustomerMobile mobile = new MPAY_CustomerMobile();
        mobile.setId(1L);
        return mobile;
    }

    private MPAY_CustomerDevice getDevice(Long id, Timestamp creationDate,boolean isDefault,MPAY_CustomerMobile mobile){
        MPAY_CustomerDevice device = new MPAY_CustomerDevice();
        device.setId(id);
        device.setIsDefault(isDefault);
        device.setRefCustomerMobile(mobile);
        device.setCreationDate(creationDate);
        return device;
    }

    private Long getFixedTime(){
        return 1600850225294L;
    }

}