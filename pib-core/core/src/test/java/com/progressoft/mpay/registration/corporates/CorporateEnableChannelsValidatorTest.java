package com.progressoft.mpay.registration.corporates;

import com.progressoft.jfw.AppException;
import com.progressoft.jfw.AppValidationException;
import com.progressoft.jfw.workflow.WfContext;
import com.progressoft.mpay.entities.MPAY_Corporate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CorporateEnableChannelsValidatorTest {
    private CorporateEnableChannelsValidator validator;
    private WfContext<MPAY_Corporate> context;
    private Map<String, Object> vars;

    @BeforeEach
    public void setUp() {
        validator = new CorporateEnableChannelsValidator();
        vars = new HashMap<>();
        context = new WfContext<>(vars, null, null);
    }

    @Test
    public void givenEnabledSmsAndEmptyMobileNumber_ThenValidatorShouldBeThrownException() {
        vars.put("bean", getCorporate(true, false, null, null));
        context = new WfContext<>(vars, null, null);
        AppValidationException appException = assertThrows(AppValidationException.class, () -> validator.validate(context));
        assertEquals("Please enter mobile number",appException.getMessage());
    }

    @Test
    public void givenEnabledEmailAndEmptyEmail_ThenValidatorShouldBeThrownException() {
        vars.put("bean", getCorporate(false, true, null, null));
        context = new WfContext<>(vars, null, null);
        AppValidationException appException = assertThrows(AppValidationException.class, () -> validator.validate(context));
        assertEquals("Please enter email address",appException.getMessage());
    }

    @Test
    public void givenEnabledEmailAndFilledEmail_ThenValidatorShouldNotThrownException() {
        vars.put("bean", getCorporate(false, true, null, "email@email.com"));
        context = new WfContext<>(vars, null, null);
        assertDoesNotThrow(() -> validator.validate(context));
    }

    @Test
    public void givenEnabledSmsAndFilledMobileNumber_ThenValidatorShouldNotThrownException() {
        vars.put("bean", getCorporate(true, false, "0788888888", null));
        context = new WfContext<>(vars, null, null);
        assertDoesNotThrow(() -> validator.validate(context));
    }



    private MPAY_Corporate getCorporate(boolean isEnableSms, boolean isEnableEmail, String mobileNumber, String email) {
        MPAY_Corporate corporate = new MPAY_Corporate();
        corporate.setEnableSMS(isEnableSms);
        corporate.setEnableEmail(isEnableEmail);
        corporate.setEmail(email);
        corporate.setMobileNumber(mobileNumber);
        return corporate;
    }

}