package com.progressoft.notification;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.notifications.INotificationService;

public class NotificationServiceRepository {
	private static final Logger logger = LoggerFactory.getLogger(NotificationServiceRepository.class);
	private static HashMap<String, INotificationService> processors = new HashMap<>();

	private NotificationServiceRepository() {

	}

	public static INotificationService getService(String processor) {
		logger.debug("Inside getService....");
		logger.debug("processor = " + processor);
		try {
			if (!processors.containsKey(processor)) {
				Class<?> cls = Class.forName(processor);
				processors.put(processor, (INotificationService) cls.newInstance());
			}
			return processors.get(processor);
		} catch (Exception e) {
			logger.error("Error in GetProcessor", e);
			return null;
		}
	}
}
