package com.progressoft.notification;

import java.io.StringReader;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.camel.Body;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_NotificationService;
import com.progressoft.mpay.exceptions.NotificationException;
import com.progressoft.mpay.notifications.INotificationService;
import com.progressoft.mpay.notifications.NotificationServiceContext;
import com.progressoft.mpay.sms.NotificationRequest;

public class PushNotificationSender {

	private static final Logger logger = LoggerFactory.getLogger(PushNotificationSender.class);
	HttpsURLConnection connection;

	public void sendPushNotificationMessage(@Body String body) {
		logger.debug("Inside sendPushNotificationMessage ....");
		NotificationRequest notificationRequest = getNotificationRequestFromBody(body);
		MPAY_NotificationService notificationService = MPayContext.getInstance().getLookupsLoader().getNotifictionService(MPayContext.getInstance().getSystemParameters().getPushNotificationService());
		INotificationService service = NotificationServiceRepository.getService(notificationService.getProcessor());
		NotificationServiceContext context = new NotificationServiceContext();
		context.setNotificationService(notificationService);
		context.setRequest(notificationRequest);
		service.sendNotification(context);
//		FCMPushMessagePayload payload = preparePayload(notificationRequest);
//		connection = preparePushNotificationConnection();
//		getOutputStream(payload);
	}

	private NotificationRequest getNotificationRequestFromBody(String body) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { NotificationRequest.class });
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			return (NotificationRequest) unmarshaller.unmarshal(new StringReader(body));
		} catch (Exception e) {
			throw new NotificationException("Error while getNotificationRequestFromBody", e);
		}
	}

//	private FCMPushMessagePayload preparePayload(NotificationRequest notificationRequest) {
//		logger.debug("Inside preparePayload ...." + notificationRequest);
//
//		FCMPushMessagePayload payload = new FCMPushMessagePayload();
//		DataObject data = new DataObject();
//		data.setMessage(notificationRequest.getContent());
//		payload.setData(data);
//		NotificationObject notification = new NotificationObject();
//		notification.setTitle(notificationRequest.getSender());
//		notification.setBody(notificationRequest.getContent());
//		payload.setNotification(notification);
//		payload.setRecipient(notificationRequest.getExtra1());
//		return payload;
//	}
//
//	private HttpsURLConnection preparePushNotificationConnection() {
//		try {
//			connection = (HttpsURLConnection) getURL().openConnection();
//			connection.setRequestMethod("POST");
//			connection.setRequestProperty("Content-Type", "application/json");
//			connection.addRequestProperty("Authorization", "key=" + getAPIKeyConfiguration());
//			connection.setDoOutput(true);
//			return connection;
//		} catch (Exception e) {
//			throw new NotificationException("Error while preparePushNotificationConnection", e);
//		}
//	}

//	private URL getURL() throws MalformedURLException {
//		return new URL(getPushNotificationURLConfiguration());
//	}
//
//	private String getPushNotificationURLConfiguration() {
//		return SystemParameters.getInstance().getPushNotificationURL();
//	}
//
//	private String getAPIKeyConfiguration() {
//		return SystemParameters.getInstance().getAPIKey();
//	}
//
//	private void getOutputStream(FCMPushMessagePayload payload) {
//		logger.debug("Inside getOutputStream ...." + payload);
//		try {
//			OutputStream output = connection.getOutputStream();
//			DataOutputStream dataOutputStream = new DataOutputStream(output);
//			dataOutputStream.writeBytes(payload.toJson());
//			dataOutputStream.flush();
//			dataOutputStream.close();
//		} catch (Exception e) {
//			throw new NotificationException("Error while getOutputStream", e);
//		}
//	}
}
