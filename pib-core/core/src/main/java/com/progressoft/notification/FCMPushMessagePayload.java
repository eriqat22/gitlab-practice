package com.progressoft.notification;

import com.google.gson.Gson;

public class FCMPushMessagePayload {
	private String to;
	private DataObject data;
	private NotificationObject notification;

	public String getRecipient() {
		return to;
	}

	public void setRecipient(String to) {
		this.to = to;
	}

	public DataObject getData() {
		return data;
	}

	public void setData(DataObject data) {
		this.data = data;
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public NotificationObject getNotification() {
		return notification;
	}

	public void setNotification(NotificationObject notification) {
		this.notification = notification;
	}
}