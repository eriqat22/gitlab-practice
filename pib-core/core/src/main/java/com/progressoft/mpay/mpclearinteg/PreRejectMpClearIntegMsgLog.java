package com.progressoft.mpay.mpclearinteg;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegRjctReason;
import com.progressoft.mpay.entity.MpClearIntegRjctReasonCodes;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.solab.iso8583.IsoMessage;

public class PreRejectMpClearIntegMsgLog implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PreRejectMpClearIntegMsgLog.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside PreRejectMpClearIntegMsgLog--------------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());
		MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).updateResponseReceived(isomsg.getField(116).toString());
		if (msg.getReason() == null) {
			MPAY_MpClearIntegRjctReason reason = LookupsLoader.getInstance().getMpClearIntegrationReasons(MpClearIntegRjctReasonCodes.UNSUPPORTED_MESSAGE_TYPE);
			msg.setReason(reason);
		}
	}
}
