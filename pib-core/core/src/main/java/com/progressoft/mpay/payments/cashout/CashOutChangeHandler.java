package com.progressoft.mpay.payments.cashout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CashOut;

public class CashOutChangeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(CashOutChangeHandler.class);
	private static final String CALCULATION_STEP = "100606";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("***inside CashOutChangeHandler : " + changeHandlerContext.getSource());

		MPAY_CashOut cashOut = (MPAY_CashOut) changeHandlerContext.getEntity();

		cashOut.setCurrency(SystemParameters.getInstance().getDefaultCurrency());
		changeHandlerContext.getFieldsAttributes().get("refCustMob").setVisible(true);
		changeHandlerContext.getFieldsAttributes().get("refMobileAccount").setVisible(true);
		if (cashOut.getStatusId() != null && CALCULATION_STEP.equals(cashOut.getStatusId().getCode())) {
			changeHandlerContext.getFieldsAttributes().get("refCustMob").setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get("refMobileAccount").setEnabled(false);
		}
	}
}