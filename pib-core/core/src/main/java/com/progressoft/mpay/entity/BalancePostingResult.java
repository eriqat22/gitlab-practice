package com.progressoft.mpay.entity;

import java.math.BigDecimal;

public class BalancePostingResult {
	boolean isSuccess;
	BigDecimal balanceBefore;
	BigDecimal balanceAfter;

	public BalancePostingResult(boolean isSuccess, BigDecimal balanceBefore, BigDecimal balanceAfter)
	{
		this.isSuccess = isSuccess;
		this.balanceBefore = balanceBefore;
		this.balanceAfter = balanceAfter;
	}

	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public BigDecimal getBalanceBefore() {
		return balanceBefore;
	}
	public void setBalanceBefore(BigDecimal balanceBefore) {
		this.balanceBefore = balanceBefore;
	}
	public BigDecimal getBalanceAfter() {
		return balanceAfter;
	}
	public void setBalanceAfter(BigDecimal balanceAfter) {
		this.balanceAfter = balanceAfter;
	}


}
