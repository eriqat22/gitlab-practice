package com.progressoft.mpay.devices.corporate;

import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.bussinessobject.core.AbstractJFWEntity;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.jfw.workflow.WfContext;
import com.progressoft.jfw.workflow.WfFunction;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Component
public class PreDeleteCorporateDevice extends WfFunction<MPAY_CorporateDevice> {

    @Override
    public void execute(WfContext<MPAY_CorporateDevice> wfContext) {
        MPAY_CorporateDevice entity = wfContext.getEntity();
        if (entity.getIsDefault()) {
            entity.setIsDefault(false);
            List<MPAY_CorporateDevice> devices = itemDao.getItems(MPAY_CorporateDevice.class, null, getFilterList(entity), null, null);
            devices.sort(Comparator.comparing(AbstractJFWEntity::getCreationDate));
            devices.stream().findFirst().ifPresent(c -> c.setIsDefault(true));
        }
    }

    private List<Filter> getFilterList(MPAY_CorporateDevice entity) {
        return Collections.singletonList(Filter.get(MPAY_CorporateDevice.REF_CORPORATE_SERVICE + ".id", Operator.EQ, entity.getRefCorporateService().getId()));
    }
}
