package com.progressoft.mpay.registration.corporates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;

public class CorporateServiceNotificationChannelsHandler implements ChangeHandler {

	private static final String SERVICE_NOTIFICATION_RECEIVER = "serviceNotificationReceiver";
	private static final String NOTIFICATION_RECEIVER = "notificationReceiver";
	private static final Logger logger = LoggerFactory.getLogger(CorporateServiceNotificationChannelsHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handle ------------");

		Object entity = changeHandlerContext.getEntity();
		String notificationChannelCode;
		if (entity instanceof MPAY_CorpoarteService) {
			MPAY_CorpoarteService service = (MPAY_CorpoarteService) changeHandlerContext.getEntity();
//			notificationChannelCode = service.getNotificationChannel().getCode();
//			if (notificationChannelCode.equals(NotificationChannelsCode.SMS))
//				changeHandlerContext.getFieldsAttributes().get(NOTIFICATION_RECEIVER).setRegex(SystemParameters.getInstance().getMobileNumberRegex());
//			else if (notificationChannelCode.equals(NotificationChannelsCode.EMAIL))
//				changeHandlerContext.getFieldsAttributes().get(NOTIFICATION_RECEIVER).setRegex(SystemParameters.getInstance().getEmailRegex());

//			if (notificationChannelCode.equals(NotificationChannelsCode.PUSH_NOTIFICATION))
//				changeHandlerContext.getFieldsAttributes().get(NOTIFICATION_RECEIVER).setRequired(false);
//			else
//				changeHandlerContext.getFieldsAttributes().get(NOTIFICATION_RECEIVER).setRequired(true);
		} else {
			MPAY_Corporate corporate = (MPAY_Corporate) changeHandlerContext.getEntity();
//			notificationChannelCode = corporate.getServiceNotificationChannel().getCode();
//			if (notificationChannelCode.equals(NotificationChannelsCode.SMS))
//				changeHandlerContext.getFieldsAttributes().get(SERVICE_NOTIFICATION_RECEIVER).setRegex(SystemParameters.getInstance().getMobileNumberRegex());
//			else if (notificationChannelCode.equals(NotificationChannelsCode.EMAIL))
//				changeHandlerContext.getFieldsAttributes().get(SERVICE_NOTIFICATION_RECEIVER).setRegex(SystemParameters.getInstance().getEmailRegex());
//
//			if (notificationChannelCode.equals(NotificationChannelsCode.PUSH_NOTIFICATION))
//				changeHandlerContext.getFieldsAttributes().get(SERVICE_NOTIFICATION_RECEIVER).setRequired(false);
//			else
//				changeHandlerContext.getFieldsAttributes().get(SERVICE_NOTIFICATION_RECEIVER).setRequired(true);
		}
	}
}
