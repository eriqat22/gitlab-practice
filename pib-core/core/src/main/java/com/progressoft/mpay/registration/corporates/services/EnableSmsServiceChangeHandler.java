package com.progressoft.mpay.registration.corporates.services;

import com.progressoft.jfw.workflow.WfChangeHandler;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import org.springframework.stereotype.Component;

@Component
public class EnableSmsServiceChangeHandler extends WfChangeHandler<MPAY_CorpoarteService> {

    @Override
    public void handle(WfChangeHandlerContext<MPAY_CorpoarteService> wfChangeHandlerContext) {
        wfChangeHandlerContext.field(MPAY_CorpoarteService.MOBILE_NUMBER)
                .setRequired(wfChangeHandlerContext.getEntity().getEnableSMS());
    }
}
