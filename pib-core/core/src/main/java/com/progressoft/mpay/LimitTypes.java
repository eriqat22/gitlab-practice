package com.progressoft.mpay;

public class LimitTypes {
    public static final long DAILY = 1;
    public static final long WEEKLY = 2;
    public static final long MONTHLY = 3;
    public static final long YEARLY = 4;
    private LimitTypes() {
    }
}
