package com.progressoft.mpay.entity;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_JvType;


public enum JvDetailsTypes {

    TRANSFER("1"), CHARGES("2"), REVERSAL("3"), COMMISSIONS("4"), TAX("5");

    private String value;

    JvDetailsTypes(String value) {
        this.value = value;
    }

    public boolean compare(MPAY_JvType msgType) {
        if (msgType == null)
            throw new NullArgumentException("msgType");
        return msgType.getCode().equals(this.value);
    }

    @Override
    public String toString() {
        return value;
    }
}
