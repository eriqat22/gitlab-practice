//
//
//package com.progressoft.mpay.webclient.security;
//
//import android.content.Context;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.util.zip.CRC32;
//import java.util.zip.CheckedInputStream;
//
//public class CalculateCheckSum {
//    public CalculateCheckSum() {
//    }
//
//    public static long calculate() {
//        String apkPath = context.getPackageCodePath();
//        System.out.println(apkPath);
//        FileInputStream fis = null;
//        Long checksum = null;
//
//        try {
//            fis = new FileInputStream(new File(apkPath));
//            CRC32 chk = new CRC32();
//            CheckedInputStream cis = new CheckedInputStream(fis, chk);
//            byte[] buff = new byte[80];
//
//            while(true) {
//                if (cis.read(buff) < 0) {
//                    checksum = chk.getValue();
//                    break;
//                }
//            }
//        } catch (Exception var15) {
//            var15.printStackTrace();
//        } finally {
//            if (fis != null) {
//                try {
//                    fis.close();
//                } catch (IOException var14) {
//                    var14.printStackTrace();
//                }
//            }
//
//        }
//
//        System.out.println(checksum);
//        return checksum;
//    }
//}
