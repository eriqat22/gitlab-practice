package com.progressoft.mpay;

public class ValidationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(String message, Exception ex) {
		super(message, ex);
	}
}
