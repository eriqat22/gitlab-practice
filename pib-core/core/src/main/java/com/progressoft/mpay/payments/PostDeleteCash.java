package com.progressoft.mpay.payments;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;

public class PostDeleteCash implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostDeleteCash.class);

	@Autowired
	private ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside execute ===================");
		itemDao.removeItem(arg0.get(WorkflowService.WF_ARG_BEAN));
	}
}