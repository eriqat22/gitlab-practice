package com.progressoft.mpay.entity;

public class AccountTypes {
    public static final String WALLET = "1";
    public static final String CHARGES = "2";
    public static final String CLEARING = "3";
    public static final String DIFFERENCES = "4";
    public static final String COMMISSION = "5";
    private AccountTypes() {
    }
}