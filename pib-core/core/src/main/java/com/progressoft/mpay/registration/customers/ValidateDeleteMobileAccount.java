package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.registration.ValidateAccountDeletion;

public class ValidateDeleteMobileAccount implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteMobileAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside validate ==========");
		InvalidInputException iie = new InvalidInputException();

		MPAY_MobileAccount account = (MPAY_MobileAccount) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (account.getIsDefault() || account.getIsSwitchDefault()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.ACCOUNT_DELETE_DEFAULT));
			throw iie;
		}
		ValidateAccountDeletion.validateWallet(account.getRefAccount());
	}
}
