package com.progressoft.mpay.migs.mvc.controller;

import java.util.HashMap;
import java.util.Map;

public class MigsRequestObjectFactory {

    static final String DEFAULT = "DEFAULT";
    static Map<String, MigsRequestBuiler> types = new HashMap<>();

    private MigsRequestObjectFactory() {

    }

    public static MigsRequestBuiler getMigsRequestObjectBuilder(MigsContext context) {
        initializeTypes(context);
        if (types.containsKey(DEFAULT))
            return types.get(DEFAULT);
        else
            throw new MigsException("Not Supported Message");
    }

    private static void initializeTypes(MigsContext context) {
        if (types.isEmpty())
            types.put(DEFAULT, new MigsRequestBuilerImp(context));
    }

}
