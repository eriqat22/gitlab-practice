package com.progressoft.mpay.migs.request;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.progressoft.mpay.entities.MPAY_ServiceType;
import com.progressoft.mpay.migs.mvc.controller.MigsContext;

@XmlRootElement(name = "MigsRequest")
public class MigsRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vpcAmount;
	private String command;
	private String locale;
	private String merchTxnRef;
	private String orderInfo;
	private String returnURL;
	private String version;
	private String secureHash;
	private String mobileNumber;
	private String newVersion;
	private String tenantId;
	private String deviceId;
	private String json;
	private MPAY_ServiceType serviceType;

	private String serviceTypeCode;

	@XmlElement(name = "vpc_Amount")
	public String getVpcAmount() {
		return vpcAmount;
	}

	public String getVpcCommand() {
		return command;
	}

	public String getVpcLocale() {
		return locale;
	}

	public String getVpcMerchTxnRef() {
		return merchTxnRef;
	}

	public String getVpcOrderInfo() {
		return orderInfo;
	}

	public String getVpcReturnURL() {
		return returnURL;
	}

	public String getVpcVersion() {
		return version;
	}

	public String getVpcSecureHash() {
		return secureHash;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getNewVersion() {
		return newVersion;
	}

	public String getTenantId() {
		return tenantId;
	}

	public String getJson() {
		return json;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public MPAY_ServiceType getServiceType() {
		return serviceType;
	}

	public static class Builder {
		private MigsRequest result;
		private MigsContext context;

		public Builder(MigsContext context) {
			result = new MigsRequest();
			this.context = context;
		}

		public Builder withAmount(String amount) {
			result.vpcAmount = amount;
			return this;
		}

		public Builder withCommand(String command) {
			result.command = command;
			return this;
		}

		public Builder withLocal(String local) {
			result.locale = local;
			return this;
		}

		public Builder withMerchTxnRef(String merchTxnRef) {
			result.merchTxnRef = merchTxnRef;
			return this;
		}

		public Builder withOrderInfo(String orderInfo) {
			result.orderInfo = orderInfo;
			return this;
		}

		public Builder withReturnURL(String returnUrl) {
			result.returnURL = returnUrl;
			return this;
		}

		public Builder withVersion(String version) {
			result.version = version;
			return this;
		}

		public Builder withMobileNumber(String mobileNumber) {
			result.mobileNumber = mobileNumber;
			return this;
		}

		public Builder withNewVersion(String newVersion) {
			result.newVersion = newVersion;
			return this;
		}

		public Builder withJson(String json) {
			result.json = json;
			return this;
		}

		public Builder withServiceTypeCode(String serviceTypeCode) {
			result.serviceTypeCode = serviceTypeCode;
			result.serviceType = context.getItemDao().getItem(MPAY_ServiceType.class, "code", result.serviceTypeCode);
			return this;
		}

		public Builder withDeviceId(String deviceId) {
			result.deviceId = deviceId;
			return this;
		}

		public Builder withHashing() {
			String secureHashing = MigsHAshingBuilder.getMigsHAshing(context).generateHasing(result);
			result.secureHash = secureHashing;
			return this;
		}

		public MigsRequest make() {
			return result;
		}
	}
}