package com.progressoft.mpay.plugins.validators;

import com.progressoft.mpay.AccountLimits;
import com.progressoft.mpay.LimitTypes;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;

import static ch.lambdaj.Lambda.*;
import static com.progressoft.mpay.LimitTypes.*;

public class LimitsValidator {
    private static final Logger logger = LoggerFactory.getLogger(LimitsValidator.class);

    private LimitsValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate...");
        if (context == null)
            throw new NullArgumentException("context");

        ProcessingContextSide side;
        if (context.getTransactionNature() == null || context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_CREDIT))
            side = context.getSender();

        else
            side = context.getReceiver();

        return validate(side, context.getAmount(), context.getOperation().getMessageType());
    }

    public static ValidationResult validate(ProcessingContextSide side, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside Validate ...");
        if (side == null)
            throw new NullArgumentException("side");

        MPAY_Profile profile = side.getProfile();
        if (profile == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        MPAY_LimitsScheme scheme = profile.getLimitsScheme();
        if (scheme == null || !scheme.getIsActive())
            return new ValidationResult(ReasonCodes.VALID, null, true);

        if (side.getLimits() == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);

        ValidationResult normalLimitsResult = validateLimits(side.getLimits(), amount.add(side.getCharge()), scheme, messageType);
        if (!normalLimitsResult.isValid())
            return normalLimitsResult;
        return validateTotalLimits(side.getLimits(), amount, scheme);
    }

    private static ValidationResult validateLimits(AccountLimits limits, BigDecimal amount, MPAY_LimitsScheme scheme, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateLimits ...");

        for (MPAY_Limit limit : scheme.getRefSchemeLimits()) {
            if (!limit.getIsActive())
                continue;
            MPAY_LimitsDetail detail = getActiveLimitDetail(limit, messageType);
            if (detail != null) {
                long limitTypeId = limit.getRefType().getId();
                ValidationResult result = validateLimitDetails(limits, amount, messageType, detail, limitTypeId);
                if (!result.isValid())
                    return result;
            }
        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateLimitDetails(AccountLimits limits, BigDecimal amount, MPAY_MessageType messageType, MPAY_LimitsDetail detail, long limitTypeId) {
        ValidationResult result;
        if (limitTypeId == DAILY) {
            result = validateDailyLimit(limits, detail, amount, messageType);
            if (!result.isValid())
                return result;
        } else if (limitTypeId == WEEKLY) {
            result = validateWeeklyLimit(limits, detail, amount, messageType);
            if (!result.isValid())
                return result;
        } else if (limitTypeId == LimitTypes.MONTHLY) {
            result = validateMonthlyLimit(limits, detail, amount, messageType);
            if (!result.isValid())
                return result;
        } else if (limitTypeId == LimitTypes.YEARLY) {
            result = validateYearlyLimit(limits, detail, amount, messageType);
            if (!result.isValid())
                return result;
        } else
            throw new InvalidArgumentException("Limit type not supported: " + limitTypeId);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateYearlyLimit(AccountLimits limits, MPAY_LimitsDetail detail, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateYearlyLimit ...");
        if (limits.getYear() != LocalDate.now().getYear() && amount.compareTo(detail.getTxAmountLimit()) < 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (detail.getTxCountLimit() > 0 && limits.getYearlyCount() >= detail.getTxCountLimit())
            return new ValidationResult(ReasonCodes.EXCEEDED_YEARLY_COUNT_LIMIT, null, false);
        if (!messageType.getIsFinancial() || amount == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (limits.getYearlyAmount().add(amount).compareTo(detail.getTxAmountLimit()) > 0)
            return new ValidationResult(ReasonCodes.EXCEEDED_YEARLY_AMOUNT_LIMIT, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateMonthlyLimit(AccountLimits limits, MPAY_LimitsDetail detail, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateMonthlyLimit ...");
        if (limits.getMonth() != LocalDate.now().getMonthValue() && amount.compareTo(detail.getTxAmountLimit()) < 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (detail.getTxCountLimit() > 0 && limits.getMonthlyCount() >= detail.getTxCountLimit())
            return new ValidationResult(ReasonCodes.EXCEEDED_MONTHLY_COUNT_LIMIT, null, false);
        if (!messageType.getIsFinancial() || amount == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (limits.getMonthlyAmount().add(amount).compareTo(detail.getTxAmountLimit()) > 0)
            return new ValidationResult(ReasonCodes.EXCEEDED_MONTHLY_AMOUNT_LIMIT, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateWeeklyLimit(AccountLimits limits, MPAY_LimitsDetail detail, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateWeeklyLimit ...");
        if (limits.getWeekNumber() != Calendar.getInstance().get(Calendar.WEEK_OF_YEAR) && amount.compareTo(detail.getTxAmountLimit()) < 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (detail.getTxCountLimit() > 0 && limits.getWeeklyCount() >= detail.getTxCountLimit())
            return new ValidationResult(ReasonCodes.EXCEEDED_WEEKLY_COUNT_LIMIT, null, false);
        if (!messageType.getIsFinancial() || amount == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (limits.getWeeklyAmount().add(amount).compareTo(detail.getTxAmountLimit()) > 0)
            return new ValidationResult(ReasonCodes.EXCEEDED_WEEKLY_AMOUNT_LIMIT, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateDailyLimit(AccountLimits limits, MPAY_LimitsDetail detail, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateDailyLimit ...");
        if (!limits.getDailyDate().equals(SystemHelper.getCurrentDateWithoutTime()) && amount.compareTo(detail.getTxAmountLimit()) < 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (detail.getTxCountLimit() > 0 && limits.getDailyCount() >= detail.getTxCountLimit())
            return new ValidationResult(ReasonCodes.EXCEEDED_DAILY_COUNT_LIMIT, null, false);
        if (!messageType.getIsFinancial() || amount == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (limits.getDailyAmount().add(amount).compareTo(detail.getTxAmountLimit()) > 0)
            return new ValidationResult(ReasonCodes.EXCEEDED_DAILY_AMOUNT_LIMIT, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static MPAY_LimitsDetail getActiveLimitDetail(MPAY_Limit limit, MPAY_MessageType messageType) {
        logger.debug("Inside GetActiveLimitDetail ...");
        return selectFirst(limit.getRefLimitLimitsDetails(),
                having(on(MPAY_LimitsDetail.class).getMsgType().getId(), Matchers.equalTo(messageType.getId())).and(having(on(MPAY_LimitsDetail.class).getIsActive(), Matchers.equalTo(true))));
    }

    private static ValidationResult validateTotalLimits(AccountLimits limits, BigDecimal amount, MPAY_LimitsScheme scheme) {
        for (MPAY_Limit limit : scheme.getRefSchemeLimits()) {
            if (limit.getRefType().getId().equals(DAILY)) {
                return validateTotals(amount, limits.getTotalDailyAmount(), limit.getTotalAmount(), limits.getTotalDailyCount(), limit.getTotalCount(), ReasonCodes.EXCEEDED_TOTAL_DAILY_AMOUNT_LIMIT, ReasonCodes.EXCEEDED_TOTAL_DAILY_COUNT_LIMIT);
            } else if (limit.getRefType().getId().equals(WEEKLY)) {
                return validateTotals(amount, limits.getTotalWeeklyAmount(), limit.getTotalAmount(), limits.getTotalWeeklyCount(), limit.getTotalCount(), ReasonCodes.EXCEEDED_TOTAL_WEEKLY_AMOUNT_LIMIT, ReasonCodes.EXCEEDED_TOTAL_WEEKLY_COUNT_LIMIT);
            } else if (limit.getRefType().getId().equals(MONTHLY)) {
                return validateTotals(amount, limits.getTotalMonthlyAmount(), limit.getTotalAmount(), limits.getTotalMonthlyCount(), limit.getTotalCount(), ReasonCodes.EXCEEDED_TOTAL_MONTHLY_AMOUNT_LIMIT, ReasonCodes.EXCEEDED_TOTAL_MONTHLY_COUNT_LIMIT);
            } else if (limit.getRefType().getId().equals(YEARLY)) {
                return validateTotals(amount, limits.getTotalYearlyAmount(), limit.getTotalAmount(), limits.getTotalYearlyCount(), limit.getTotalCount(), ReasonCodes.EXCEEDED_TOTAL_YEARLY_AMOUNT_LIMIT, ReasonCodes.EXCEEDED_TOTAL_YEARLY_COUNT_LIMIT);
            } else {
                throw new InvalidArgumentException("Limit type not supported: " + limit.getRefType().getId().toString());
            }
        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateTotals(BigDecimal amount, BigDecimal periodAmount, BigDecimal totalPeriodAmount, long periodCount, long totalPeriodCount, String amountReason, String countReason) {
        if (totalPeriodAmount.compareTo(BigDecimal.ZERO) > 0 && periodAmount.add(amount).compareTo(totalPeriodAmount) > 0)
            return new ValidationResult(amountReason.toString(), null, false);
        if (totalPeriodCount > 0 && periodCount > totalPeriodCount)
            return new ValidationResult(countReason.toString(), null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

}
