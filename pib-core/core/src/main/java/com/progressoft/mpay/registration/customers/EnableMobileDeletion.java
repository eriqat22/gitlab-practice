package com.progressoft.mpay.registration.customers;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class EnableMobileDeletion implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(EnableMobileDeletion.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVar.get(WorkflowService.WF_ARG_BEAN);
		boolean isEnabled = checkIfLastMobile(mobile);
		if(!isEnabled)
			return false;
		EnableMobileActions enableMobileActions = new EnableMobileActions();
		return enableMobileActions.passesCondition(transientVar, args, ps);
	}

	private boolean checkIfLastMobile(MPAY_CustomerMobile mobile){
		logger.debug("inside checkIfLastMobile ------------");
		List<MPAY_CustomerMobile> mobiles = select(
				mobile.getRefCustomer().getRefCustomerCustomerMobiles(),
				(having(on(MPAY_CustomerMobile.class).getDeletedFlag(),
						Matchers.nullValue()).or(having(on(MPAY_CustomerMobile.class).getDeletedFlag(),
						Matchers.equalTo(false)))).and(having(on(MPAY_CustomerMobile.class).getId(),
						Matchers.not(mobile.getId())).and(having(on(MPAY_CustomerMobile.class).getIsRegistered(),Matchers.equalTo(true)))));
		return !mobiles.isEmpty();
	}
}