package com.progressoft.mpay.entity;

import java.sql.Timestamp;

public class TransactionDetailsFilter {
	private String txSenderMobile;
	private String txReceiverMobile;
	private String txSenderService;
	private String txReceiverService;
	private String txType;
	private String txDirection;
	private String txOperation;
	private String txReference;
	private Timestamp fromTime;
	private Timestamp toTime;

	public String getTxSenderMobile() {
		return txSenderMobile;
	}
	public void setTxSenderMobile(String txSenderMobile) {
		this.txSenderMobile = txSenderMobile;
	}
	public String getTxReceiverMobile() {
		return txReceiverMobile;
	}
	public void setTxReceiverMobile(String txReceiverMobile) {
		this.txReceiverMobile = txReceiverMobile;
	}
	public String getTxSenderService() {
		return txSenderService;
	}
	public void setTxSenderService(String txSenderService) {
		this.txSenderService = txSenderService;
	}
	public String getTxReceiverService() {
		return txReceiverService;
	}
	public void setTxReceiverService(String txReceiverService) {
		this.txReceiverService = txReceiverService;
	}
	public String getTxType() {
		return txType;
	}
	public void setTxType(String txType) {
		this.txType = txType;
	}
	public String getTxDirection() {
		return txDirection;
	}
	public void setTxDirection(String txDirection) {
		this.txDirection = txDirection;
	}
	public String getTxOperation() {
		return txOperation;
	}
	public void setTxOperation(String txOperation) {
		this.txOperation = txOperation;
	}
	public String getTxReference() {
		return txReference;
	}
	public void setTxReference(String txReference) {
		this.txReference = txReference;
	}
	public Timestamp getFromTime() {
		return fromTime;
	}
	public void setFromTime(Timestamp fromTime) {
		this.fromTime = fromTime;
	}
	public Timestamp getToTime() {
		return toTime;
	}
	public void setToTime(Timestamp toTime) {
		this.toTime = toTime;
	}
}
