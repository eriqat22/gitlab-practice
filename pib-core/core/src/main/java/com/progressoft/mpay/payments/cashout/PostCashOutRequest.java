package com.progressoft.mpay.payments.cashout;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.mpclear.MPayCommunicatorHelper;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.payments.CashMessage;
import com.progressoft.mpay.payments.CashRequestHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.psp.PSP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.UUID;

public class PostCashOutRequest implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCashOutRequest.class);

    @SuppressWarnings({"rawtypes"})
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("***inside PostCashInRequest");
        MPAY_CashOut cashOut = (MPAY_CashOut) arg0.get(WorkflowService.WF_ARG_BEAN);

        if (!SystemNetworkStatus.isSystemOnline(DataProvider.instance())) {
            throw new WorkflowException(
                    LookupsLoader.getInstance().getReason(ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE).getReasonsNLS().get(SystemParameters.getInstance().getSystemLanguage().getId()).getDescription());
        }
        try {
            MPayRequest request = new MPayRequest("cashout", cashOut.getRefCustMob().getMobileNumber(), ReceiverInfoType.MOBILE, null,
                    LanguageMapper.getInstance().getLanguageLongByLocaleCode(SystemParameters.getInstance().getSystemLanguage().getCode()), UUID.randomUUID().toString(), cashOut.getTenantId());
            request.getExtraData().add(new ExtraData(CashMessage.AMOUNT_KEY, cashOut.getAmount().toString()));
            request.getExtraData().add(new ExtraData(CashMessage.CHARGE_AMOUNT_KEY, cashOut.getChargeAmount().toString()));
            request.getExtraData().add(new ExtraData(CashMessage.RECEIVER_KEY, getPSPCashOutService(cashOut.getRefCustMob()).getName()));
            request.getExtraData().add(new ExtraData(CashMessage.RECEIVER_TYPE_KEY, ReceiverInfoType.CORPORATE));
            request.getExtraData().add(new ExtraData(CashMessage.SENDER_ACCOUNT_KEY, prepareRefMobileAccount(cashOut.getRefMobileAccount())));
            request.getExtraData().add(new ExtraData(CashMessage.NOTES_KEY, cashOut.getComments()));
            MessageProcessingContext context = new MessageProcessingContext(DataProvider.instance(), LookupsLoader.getInstance(), SystemParameters.getInstance(), new MPayCryptographer(),
                    NotificationHelper.getInstance());
            context.setRequest(request);
            context.setOriginalRequest(request.toString());
            context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
            context.setOperation(LookupsLoader.getInstance().getEndPointOperation(request.getOperation()));
            MessageProcessor processor = MessageProcessorHelper.getProcessor(context.getOperation());
            if (processor == null)
                throw new WorkflowException("Cash Out processor not available");

            MPAY_BulkPayment bulkPayment = context.getDataProvider().getBulkPayment(context.getSystemParameters().getBulkPaymentDefaultId());
            if (bulkPayment == null)
                throw new WorkflowException("Default Bulk Payment ID Not Found");
            MPAY_MPayMessage message = MessageProcessorHelper.createMessage(new ServiceUserManager(), context, ProcessingStatusCodes.PENDING, null, null, bulkPayment);
            context.getExtraData().put("cashOut", cashOut);
            cashOut.setRefMessage(message);
            DataProvider.instance().persistMPayMessage(message);
            context.setMessage(message);
            MessageProcessingResult result = processor.processMessage(context);
            cashOut.setProcessingStatus(result.getMessage().getProcessingStatus());
            cashOut.setReason(result.getMessage().getReason());
            cashOut.setReasonDesc(result.getMessage().getReasonDesc());
            String response = CashRequestHelper.generateResponse(Long.parseLong(result.getMessage().getReference()), result.getMessage().getProcessingStatus().getCode(),
                    result.getMessage().getReason(), SystemParameters.getInstance().getSystemLanguage());
            result.getMessage().setResponseContent(response);
            DataProvider.instance().persistProcessingResult(result);
            if (result.getStatus().getCode().equals(ProcessingStatusCodes.REJECTED) || result.getStatus().getCode().equals(ProcessingStatusCodes.FAILED)) {
                JfwHelper.executeActionWithServiceUser(MPAYView.CASH_OUT, cashOut.getId(), "SVC_Reject");
                return;
            }
            if (result.getMpClearMessage() != null && !result.isHandleMPClearOffline()) {
                cashOut.setRefLastMsgLog(result.getMpClearMessage());
                MPClearHelper.sendToActiveMQ(result.getMpClearMessage(), MPayCommunicatorHelper.getCommunicator());
                DataProvider.instance().mergeCashOut(cashOut);
            }
        } catch (Exception e) {

            throw new WorkflowException(e);
        }

    }

    MPAY_CorpoarteService getPSPCashOutService(MPAY_CustomerMobile oMobile) throws WorkflowException {
        MPAY_CorpoarteService pspCashInService = new MPAY_CorpoarteService();
        MPAY_Corporate psp = PSP.get();
        for (MPAY_CorpoarteService oService : psp.getRefCorporateCorpoarteServices()) {
            if (MessageCodes.CO.toString().equals(oService.getPaymentType().getCode()) && oService.getBank().getId() == oMobile.getBank().getId()) {
                pspCashInService = oService;
            }
        }

        return pspCashInService;
    }

    private String prepareRefMobileAccount(String refMobileAccount) {
        if (refMobileAccount.contains("key") || refMobileAccount.contains("value")) {
            String[] split = refMobileAccount.split("=");
            return split[1].replace(",", "").replace("key", "");
        }
        return refMobileAccount;
    }
}