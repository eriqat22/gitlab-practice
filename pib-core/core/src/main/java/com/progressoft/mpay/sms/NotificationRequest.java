package com.progressoft.mpay.sms;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author u445
 */
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content",
    "reciever",
    "sender",
    "extra1",
    "extra2",
    "extra3"
})
@XmlRootElement(name = "NotificationRequest")
public class NotificationRequest implements Serializable {

	@XmlElement(name = "Content")
	private String content;
	@XmlElement(name = "Reciever")
	private String reciever;
	@XmlElement(name = "Sender")
	private String sender;
	@XmlElement(name = "Extra1")
	private String extra1;
	@XmlElement(name = "Extra2")
	private String extra2;
	@XmlElement(name = "Extra3")
	private String extra3;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReciever() {
		return reciever;
	}

	public void setReciever(String reciever) {
		this.reciever = reciever;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getExtra1() {
		return extra1;
	}

	public void setExtra1(String extra1) {
		this.extra1 = extra1;
	}

	public String getExtra2() {
		return extra2;
	}

	public void setExtra2(String extra2) {
		this.extra2 = extra2;
	}

	public String getExtra3() {
		return extra3;
	}

	public void setExtra3(String extra3) {
		this.extra3 = extra3;
	}

}