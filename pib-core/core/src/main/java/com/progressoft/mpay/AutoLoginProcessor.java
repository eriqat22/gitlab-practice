package com.progressoft.mpay;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_NetworkManagement;

public class AutoLoginProcessor implements Processor {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutoLoginProcessor.class);
	private static final String TRUE = "TRUE";
	private static final String FALSE = "FALSE";

	@Override
	public void process(Exchange exchange) throws Exception {
		MPAY_NetworkManagement networkManagement = getTenantNetworkManagement();
		LOGGER.debug("Auto login route for tenant " + networkManagement.getTenantId());
		exchange.getIn().setHeader("executeAction", canExecuteAutoLoginAction(networkManagement));
	}

	private MPAY_NetworkManagement getTenantNetworkManagement() {
		return DataProvider.instance().getLastNetworkManagement();
	}

	private String canExecuteAutoLoginAction(MPAY_NetworkManagement networkManagement) {
		if (networkManagement.getIsAutoLogin())
			return TRUE;
		LOGGER.debug("Auto Login is false");
		return FALSE;
	}
}