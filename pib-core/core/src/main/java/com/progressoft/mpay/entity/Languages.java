package com.progressoft.mpay.entity;

public class Languages {
	public static final long ENGLISH = 1;
    public static final long ARBIC = 2;

    private Languages() {
    }
}
