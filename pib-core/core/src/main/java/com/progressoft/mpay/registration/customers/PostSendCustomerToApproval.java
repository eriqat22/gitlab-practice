package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostSendCustomerToApproval implements FunctionProvider {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(PostSendCustomerToApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =====");
		MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
		handleMobiles(customer);
	}

	private void handleMobiles(MPAY_Customer customer) throws WorkflowException {
		logger.debug("Inside HandleMobiles =====");
		List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId());
		if (mobiles == null || mobiles.isEmpty())
			return;
		Iterator<MPAY_CustomerMobile> mobilesIterator = mobiles.listIterator();
		do {
			MPAY_CustomerMobile mobile = mobilesIterator.next();
			if (mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.MODIFIED) || mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.REJECTED))
				JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, CustomerMobileWorkflowServiceActions.SEND_APPROVAL, new WorkflowException());
			else if (mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.MODIFICATION)) {
				InvalidInputException iie = new InvalidInputException();
				iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.MOBILE_UNDER_MODIFICATION));
				throw iie;
			}
			handleAccounts(mobile.getId());

		} while (mobilesIterator.hasNext());
	}

	private void handleAccounts(long customerMobileId) throws WorkflowException {
		logger.debug("Inside HandleAccounts ================");
		List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(customerMobileId);
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_MobileAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_MobileAccount mobileAccount = accountsIterator.next();
			if (mobileAccount.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.MODIFIED.toString()) || mobileAccount.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.REJECTED.toString()))
				JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, mobileAccount, MobileAccountWorkflowServiceActions.SEND_APPROVAL, new WorkflowException());
			else if (mobileAccount.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.MODIFICATION.toString())) {
				InvalidInputException ex = new InvalidInputException();
				ex.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.MOBILE_ACCOUNT_UNDER_MODIFICATION));
				throw ex;
			}
		} while (accountsIterator.hasNext());
	}
}
