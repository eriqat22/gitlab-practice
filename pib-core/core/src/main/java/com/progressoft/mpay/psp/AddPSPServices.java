package com.progressoft.mpay.psp;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.*;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.profiles.Profile;
import com.progressoft.mpay.registration.AccountsHelper;
import com.progressoft.mpay.registration.ClientManagement;
import com.progressoft.mpay.registration.corporates.ServiceAccountWorkflowServiceActions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

public class AddPSPServices implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(AddPSPServices.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug(".... Inside AddPSPServices /Add CachIn/CachOut service for PSP");

        MPAY_Bank bank = (MPAY_Bank) arg0.get(WorkflowService.WF_ARG_BEAN);

        if (!bank.getSettlementParticipant().equals(BankType.SETTLEMENT))
            return;

        addPSPService(bank, MessageCodes.CI.toString(), BalanceTypes.CREDIT, null);
        addPSPService(bank, MessageCodes.CO.toString(), BalanceTypes.DEBIT, null);
        addPSPService(bank, MessageCodes.FEES.toString(), BalanceTypes.DEBIT, null);
        addPSPService(bank, MessageCodes.CLEARING.toString(), BalanceTypes.NORMAL, null);
        addPSPService(bank, MessageCodes.GENERIC.toString(), BalanceTypes.NORMAL, Constants.PSP_ACCOUNTS_SERVICE_NAME);
        addPSPService(bank, MessageCodes.COMMISSIONS.toString(), BalanceTypes.NORMAL, null);
        addPSPService(bank, MessageCodes.INCOME_VAT.toString(), BalanceTypes.DEBIT, null);
        addPSPService(bank, MessageCodes.EXPENSES_VAT.toString(), BalanceTypes.DEBIT, null);
    }

    @SuppressWarnings("unchecked")
    private void addPSPService(MPAY_Bank bank, String serviceType, String balanceType, String name)
            throws WorkflowException {
        PSPCache pspCache = MPayContext.getInstance().getLookupsLoader().getPSP();
        MPAY_CorpoarteService pspService = new MPAY_CorpoarteService();
        MPAY_Corporate psp = new MPAY_Corporate();
        MPAY_ServicesCategory serviceCategory = MPayContext.getInstance().getLookupsLoader().getServiceCategory("0");
        MPAY_ServiceType sType = MPayContext.getInstance().getLookupsLoader().getServiceType("0");
        psp.setId(pspCache.getId());
        psp.setCategory(getAccountCategory());
        MPAY_Corporate corporate = DataProvider.instance().getCorporate(psp.getId());
        pspService.setRefCorporate(corporate);
        if (name == null)
            pspService.setName(pspCache.getName() + serviceType);
        else
            pspService.setName(pspCache.getName() + name);
        pspService.setCategory(getAccountCategory());
        pspService.setAlias(bank.getTellerCode());
        pspService.setMpclearAlias(bank.getTellerCode());
        pspService.setBankedUnbanked(BankType.SETTLEMENT);
        pspService.setBank(bank);
        pspService.setPaymentType(LookupsLoader.getInstance().getMessageTypes(serviceType));
        pspService.setIsRegistered(Boolean.TRUE);
        pspService.setIsActive(Boolean.TRUE);
        pspService.setMas(1L);
        pspService.setExternalAcc("placeholder");
        pspService.setRetryCount(0L);
        pspService.setEmail(corporate.getEmail());
        pspService.setMobileNumber(corporate.getMobileNumber());
//		pspService.setNotificationChannel(
//				LookupsLoader.getInstance().getNotificationChannel(NotificationChannelsCode.EMAIL));
//		pspService.setNotificationReceiver(pspService.getName());
        pspService.setIsBlocked(false);

        pspService.setServiceCategory(serviceCategory);
        pspService.setServiceType(sType);
        pspService.setMaxNumberOfDevices(0L);
        pspService.setNotificationChannel(MPayContext.getInstance().getLookupsLoader().getNotificationChannel(NotificationChannelsCode.SMS));
        pspService.setPaymentType(MPayContext.getInstance().getLookupsLoader().getMessageTypes(MessageCodes.CI.toString()));
        try {
            pspService.setRefProfile(Profile.getDefault());
            JfwHelper.createEntityWithServiceUser(MPAYView.PSP_SERVICES, pspService);
            MPAY_ServiceAccount account = createServiceAccount(pspService, balanceType);
            JfwHelper.createEntityWithServiceUser(MPAYView.SERVICE_ACCOUNTS, account);
            JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account,
                    ServiceAccountWorkflowServiceActions.DIRECT_APPROVE, new WorkflowException());
        } catch (InvalidInputException e) {
            logger.error("error... change me later", e);
            throw e;
        } catch (BusinessException | InvalidActionException ex1) {
            logger.error("error... change me later", ex1);
            InvalidInputException iie = new InvalidInputException();
            iie.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS, "System error ,Unable to define new service");
            throw iie;
        } catch (Exception ex) {
            logger.error("Error while create PSP Service", ex);
            throw new WorkflowException(ex);
        }
    }

    private MPAY_Account addNewAccount(String accName, String balanceTypeCode) throws WorkflowException {
        MPAY_BalanceType balanceType = MPayContext.getInstance().getLookupsLoader().getBalanceType(balanceTypeCode);
        MPAY_AccountType refAccountType = LookupsLoader.getInstance().getAccountTypes(AccountTypes.WALLET);
        MPAY_Account refAccount = AccountsHelper.createAccount(accName,
                SystemParameters.getInstance().getDefaultCurrency(), new BigDecimal(0), refAccountType, null,
                balanceType, false);
        DataProvider.instance().persistAccount(refAccount);
        return refAccount;
    }

    private MPAY_ServiceAccount createServiceAccount(MPAY_CorpoarteService service, String balanceType) {
        MPAY_ServiceAccount account = new MPAY_ServiceAccount();
        account.setBankedUnbanked(service.getBankedUnbanked());
        account.setBank(service.getBank());
        account.setExternalAcc(service.getExternalAcc());
        account.setMas(service.getMas());
        account.setIsActive(service.getIsActive());
        account.setIsRegistered(service.getIsRegistered());
        account.setRefProfile(service.getRefProfile());
        account.setIsDefault(true);
        account.setIsSwitchDefault(true);
        account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
        account.setCategory(getAccountCategory());
        try {
            account.setRefAccount(
                    addNewAccount(service.getName() + " " + ClientManagement.getRegistrationId(account), balanceType));
        } catch (WorkflowException e) {
            throw new InvalidOperationException("Error while createServiceAccount", e);
        }
        account.setService(service);
        return account;
    }

    private MPAY_AccountCategory getAccountCategory() {
        MPAY_SysConfig serviceCategoryId = MPayContext.getInstance().getLookupsLoader().getSystemConfigurations(SysConfigKeys.SERVICE_CATEGORY_KEY);
        return MPayContext.getInstance().getLookupsLoader().getAccountCategory(Long.valueOf(serviceCategoryId.getConfigValue()));
    }

}
