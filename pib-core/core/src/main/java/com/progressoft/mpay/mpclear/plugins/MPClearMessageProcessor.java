package com.progressoft.mpay.mpclear.plugins;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.exceptions.InvalidArgumentException;

public abstract class MPClearMessageProcessor {
	public abstract MPClearProcessingResult processMessage(MPClearProcessingContext context);

	protected void validateContext(MPClearProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");
		if (context.getMessageType() == null)
			throw new InvalidArgumentException("context.getMessageType() == null");
		if (context.getMessage() == null)
			throw new InvalidArgumentException("context.getMessage() == null");
		if (context.getIsoMessage() == null)
			throw new InvalidArgumentException("context.getIsoMessage() == null");
	}
}
