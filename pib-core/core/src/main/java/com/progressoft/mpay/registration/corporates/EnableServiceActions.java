package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class EnableServiceActions implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(EnableServiceActions.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVar.get(WorkflowService.WF_ARG_BEAN);
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
		if (accounts.size() == 1)
			return true;

		MPAY_ServiceAccount newAccount = selectFirst(service.getServiceServiceAccounts(), having(on(MPAY_ServiceAccount.class).getIsRegistered(), Matchers.equalTo(false)));
		return newAccount == null;
	}
}
