package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.CustomerMobileMetaData;


public class PostEditCustomerMobile implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostEditCustomerMobile.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("execute************");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN);
		if (!mobile.getIsRegistered())
			return;
		MPAY_CustomerMobile originalMobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN_ORIGINAL);
		mobile.setNewMobileNumber(mobile.getMobileNumber());
		mobile.setNewAlias(mobile.getAlias());
		mobile.setMobileNumber(originalMobile.getMobileNumber());
		mobile.setAlias(originalMobile.getAlias());
		mobile.setApprovedData(new CustomerMobileMetaData(originalMobile).toString());
		DataProvider.instance().mergeCustomerMobile(mobile);
	}

	private String getOperator(String customerOperatorValue) {
		try {
			JSONObject json = new JSONObject(customerOperatorValue);
			if (json.isNull("key"))
				return customerOperatorValue;
			return json.getString("key");
		} catch (Exception e) {
			return customerOperatorValue;
		}
	}
}
