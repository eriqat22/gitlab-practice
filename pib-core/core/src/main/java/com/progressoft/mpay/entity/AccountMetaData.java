package com.progressoft.mpay.entity;

import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.exceptions.NullArgumentException;

import flexjson.JSONSerializer;

public class AccountMetaData {
	protected String externalAcc;
    protected long profileId;

	public void fillServiceAccountFromMetaData(MPAY_ServiceAccount account) {
        if (account == null)
            throw new NullArgumentException("account");

        account.setExternalAcc(this.externalAcc);
    }

    public String getExternalAcc() {
        return externalAcc;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }

    @Override
    public String toString() {
        JSONSerializer serializer = new JSONSerializer().exclude("*.class");
        return serializer.serialize(this);
    }
}