package com.progressoft.mpay.registration.customers;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_MobileAccount;

public class CheckIfLastMobileAccount implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfLastMobileAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_MobileAccount account = (MPAY_MobileAccount) transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (account.getIsDefault() || account.getIsSwitchDefault())
			return false;
		List<MPAY_MobileAccount> accounts = select(
				account.getMobile().getMobileMobileAccounts(),
				having(on(MPAY_MobileAccount.class).getDeletedFlag(), Matchers.nullValue()).or(having(on(MPAY_MobileAccount.class).getDeletedFlag(), Matchers.equalTo(false))).and(
						having(on(MPAY_MobileAccount.class).getId(), Matchers.not(account.getId()))));
		return !accounts.isEmpty();
	}
}