package com.progressoft.mpay.registration.bulk;

public class CustomerRegistraionLine extends BulkRegistrationLine {

    private String firstName;
    private String middleName;
    private String lastName;
    private String idType;
    private String idNum;
    private String birthDate;
    private String mobileNumber;
    private String operator;
    private String nfcSerial;
    private String notificationShowType;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getNfcSerial() {
        return nfcSerial;
    }

    public void setNfcSerial(String nfcSerial) {
        this.nfcSerial = nfcSerial;
    }

    public String getNotificationShowType() {
        return notificationShowType;
    }

    public void setNotificationShowType(String notificationShowType) {
        this.notificationShowType = notificationShowType;
    }
}
