package com.progressoft.mpay.pushnotificationservices.onesignal;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PushNotificationBody {
   private Notification notification;
   private String to ;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

}
