package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.*;
import com.progressoft.mpay.MPayCryptographer.DefaultCryptographer;
import com.progressoft.mpay.common.DateUtils;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.*;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.registration.AccountsHelper;
import com.progressoft.mpay.registration.ClientManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Map;

import static java.util.Objects.nonNull;

public class PostMobileAccountRegistrationApproval implements FunctionProvider {

    private static final String REFERRAL_KEY_LENGTH = "Referral Code Length";
    private static final String LIGHT_WALLET_PROFILE = "Light Wallet Profile Id";
    private static final String AUTO_REGISTERED_PROFILE = "Auto Registered Wallet Profile Id";
    private static final Logger logger = LoggerFactory.getLogger(PostMobileAccountRegistrationApproval.class);
    private Long lightWallet;
    private Long autoRegistration;

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute--------------------------");
        try {
            MPAY_MobileAccount account = (MPAY_MobileAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
            MPAY_CustomerMobile mobile = account.getMobile();
            MPAY_Customer customer = mobile.getRefCustomer();

            if (account.getApprovedData() != null)
                applyApprovedData(account, customer);
            if (nonNull(account.getIsRegistered()) && account.getIsRegistered())
                return;
            account.setIsRegistered(true);
            createWalletAccount(mobile, account);

            ILookupsLoader lookupsLoader = LookupsLoader.getInstance();
            Long customerCategoryId = Long.valueOf(
                    lookupsLoader.getSystemConfigurations(SysConfigKeys.CUSTOMER_CATEGORY_KEY).getConfigValue());
            lookupsLoader.getAccountCategory(customerCategoryId);
            if (account.getCategory() != null && account.getCategory().getId().equals(customerCategoryId))
                notifyCustomer(account);
        } catch (Exception e) {
            logger.error("Error while trying to notify customer", e);
            throw new WorkflowException(e);
        }
    }

    private void applyApprovedData(MPAY_MobileAccount account, MPAY_Customer customer) throws Exception {

        lightWallet = Long.valueOf(LookupsLoader.getInstance().getSystemConfigurations(LIGHT_WALLET_PROFILE).getConfigValue());
        autoRegistration = Long.valueOf(LookupsLoader.getInstance().getSystemConfigurations(AUTO_REGISTERED_PROFILE).getConfigValue());


        //if (isFromLightOrAuto(account) && !isToLightOrAuto(account)) {

        String customerId = String.valueOf(customer.getId());
        customer.setIsLight(false);
        customer.setIsAutoRegistered(false);
        customer.setExpiryDate(null);
        customer.setReferralkey(fillReferralKey(customerId));
        //}

        if (account.getNewProfile() != null) {
            account.setRefProfile(account.getNewProfile());
            account.setNewProfile(null);
        }

        account.setExternalAcc(account.getNewExternalAcc());
        account.setNewExternalAcc(null);
        account.setApprovedData(null);
    }

//    private boolean isFromLightOrAuto(MPAY_MobileAccount account) {
//        return lightWallet.compareTo(account.getRefProfile().getId()) == 0 || autoRegistration.compareTo(account.getRefProfile().getId()) == 0;
//    }
//
//    private boolean isToLightOrAuto(MPAY_MobileAccount account) {
//        return lightWallet.compareTo(account.getNewProfile().getId()) == 0 || autoRegistration.compareTo(account.getNewProfile().getId()) == 0;
//    }

    private void createWalletAccount(MPAY_CustomerMobile mobile, MPAY_MobileAccount mobileAccount)
            throws WorkflowException {
        String accountTypeCode = mobileAccount.getCategory().getId().toString().equals(AccountCategory.COMMISSION) ?
                AccountTypes.COMMISSION : AccountTypes.WALLET;
        MPAY_AccountType accountType = LookupsLoader.getInstance().getAccountTypes(accountTypeCode);
        MPAY_Account account = AccountsHelper.createAccount(
                mobile.getMobileNumber() + " " + ClientManagement.getRegistrationId(mobileAccount),
                SystemParameters.getInstance().getDefaultCurrency(), new BigDecimal(0), accountType,
                mobileAccount.getExternalAcc(),
                MPayContext.getInstance().getLookupsLoader().getBalanceType(BalanceTypes.DEBIT),
                mobileAccount.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));

        DataProvider.instance().persistAccount(account);
        mobileAccount.setRefAccount(account);

        logger.debug("\n\r===Saving Account....");
        DataProvider.instance().mergeCustomerMobile(mobile);
    }

    public void notifyCustomer(MPAY_MobileAccount account) throws WorkflowException {
        try {
            MPAY_CustomerMobile mobile = account.getMobile();
            boolean sendActivationCodeOrNot = SystemParameters.getInstance().getSendActivationCodeUponCustomerMobileRegistration();
            boolean sendPinCode = SystemParameters.getInstance().getSendPinCodeUponRegistration();
            MPAY_EndPointOperation operation = LookupsLoader.getInstance().getEndPointOperation("cust");
            CustomerRegistrationNotificationContext context = new CustomerRegistrationNotificationContext();
            MPAY_Language language = mobile.getRefCustomer().getPrefLang();

            if (language.getCode().equals(LanguageCode.ENGLISH))
                context.setAppName(SystemParameters.getInstance().getApplicationNameEn());
            else
                context.setAppName(SystemParameters.getInstance().getApplicationNameAr());
            context.setReference(mobile.getRefCustomer().getClientRef());
            context.setAccountNumber(ClientManagement.getRegistrationId(account));
            if (sendActivationCodeOrNot) {
                int activationCodeLength = SystemParameters.getInstance().getActivationCodeLength();
                String activationCode = OTPGenerator.generateCode(activationCodeLength);
                context.setHasActivationCode(true);
                context.setActivationCode(activationCode);
                mobile.setActivationCode(HashingAlgorithm.hash(SystemParameters.getInstance(), activationCode));
                mobile.setActivationCodeValidy(
                        DateUtils.getValidityTime(SystemParameters.getInstance().getActivationCodeValidity()));
                context.setExtraData3(getValidityTime(mobile.getActivationCodeValidy()));
            }

            NotificationHelper.getInstance().createRegistrationNotification(null, context,
                    NotificationType.ACCEPTED_SENDER, operation.getId(), language.getId(), mobile.getMobileNumber(),
                    NotificationChannelsCode.SMS);

            if (sendPinCode) {
                int length = SystemParameters.getInstance().getPinCodeLength();
                String pinCode = OTPGenerator.generateCode(length);
                String hashedPinCode = HashingAlgorithm.hash(SystemParameters.getInstance(), pinCode);
                mobile.setPin(hashedPinCode);
                mobile.setPinLastChanged(SystemHelper.getSystemTimestamp());
                String encryptedOTP = DefaultCryptographer.INSTANCE.get().encrypt(pinCode, false);
                context.setPinCode("\"" + encryptedOTP + "\"");
                NotificationHelper.getInstance().createRegistrationNotification(null, context,
                        NotificationType.ACCEPTED_RECEIVER, operation.getId(), language.getId(),
                        mobile.getMobileNumber(), NotificationChannelsCode.SMS);
            }

        } catch (Exception e) {
            logger.error("error create notification for activation code when register new customer mobile number....", e);
            throw new WorkflowException(e);
        }
    }

    private String getValidityTime(Timestamp activationCodeValidy) {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(activationCodeValidy);
    }

    private String fillReferralKey(String customerId) throws NoSuchAlgorithmException {
        String configValue = LookupsLoader.getInstance().getSystemConfigurations(REFERRAL_KEY_LENGTH).getConfigValue();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(customerId.getBytes(StandardCharsets.UTF_8));
        String base64String = new BigInteger(1, hash).toString(36);

        return base64String.substring(0, Integer.parseInt(configValue));

    }
}
