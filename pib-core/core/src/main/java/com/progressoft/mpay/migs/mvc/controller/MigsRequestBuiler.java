package com.progressoft.mpay.migs.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import com.progressoft.mpay.migs.request.MigsRequest;

public abstract class MigsRequestBuiler {

	protected MigsContext context;

	public MigsRequestBuiler(MigsContext context) {
		this.context = context;
	}

	public abstract MigsRequest buildRequestObject(HttpServletRequest request);
}