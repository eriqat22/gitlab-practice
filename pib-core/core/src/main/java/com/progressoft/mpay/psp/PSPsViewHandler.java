package com.progressoft.mpay.psp;

import com.progressoft.mpay.MPayContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.ClientTypes;

public class PSPsViewHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(PSPsViewHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("***inside Agents View Handler: " + changeHandlerContext.getSource());
		try {
			MPAY_Corporate psp = (MPAY_Corporate) changeHandlerContext.getEntity();
			MPAY_ClientType type = LookupsLoader.getInstance().getClientType(ClientTypes.SERVICE_PROVIDER_CODE);
			psp.setClientType(type);
			changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.CLIENT_TYPE).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.MOBILE_NUMBER).setRegex(MPayContext.getInstance().getSystemParameters().getMobileNumberRegex());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new BusinessException("An error occured in the PSPs View Handler", ex);
		}
	}

}
