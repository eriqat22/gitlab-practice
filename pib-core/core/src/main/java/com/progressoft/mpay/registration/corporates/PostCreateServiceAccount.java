package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class PostCreateServiceAccount implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCreateServiceAccount.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute PostCreateServiceAccount--------------------------");
        MPAY_ServiceAccount account = (MPAY_ServiceAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
        account.setIsActive(true);
    }
}
