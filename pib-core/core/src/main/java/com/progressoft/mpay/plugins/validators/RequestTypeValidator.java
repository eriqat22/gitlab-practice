package com.progressoft.mpay.plugins.validators;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_RequestTypesDetail;
import com.progressoft.mpay.entities.MPAY_RequestTypesScheme;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;

public class RequestTypeValidator {
	private static final Logger logger = LoggerFactory.getLogger(RequestTypeValidator.class);

	private RequestTypeValidator() {

	}

	public static ValidationResult validate(MPAY_Profile profile, MPAY_MessageType messageType) {
		logger.debug("Inside Validate ....");
		if (profile == null || messageType == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);

		if (!checkIfRequestEnabled(messageType.getCode(), profile.getRequestTypesScheme()))
			return new ValidationResult(ReasonCodes.UNSUPPORTED_REQUEST, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static boolean checkIfRequestEnabled(String messageTypeCode, MPAY_RequestTypesScheme scheme) {
		MPAY_RequestTypesDetail detail = selectFirst(scheme.getRefSchemeRequestTypesDetails(), having(on(MPAY_RequestTypesDetail.class).getMsgType().getCode(), Matchers.equalTo(messageTypeCode)));
		if (detail == null)
			return false;
		return detail.getIsActive();

	}
}
