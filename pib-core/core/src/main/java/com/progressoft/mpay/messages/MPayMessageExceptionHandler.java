package com.progressoft.mpay.messages;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.ReasonCodes;

public class MPayMessageExceptionHandler implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(MPayMessageExceptionHandler.class);

	@Override
	public void process(Exchange exchange) throws Exception {

		logger.debug("inside MPayMessageExceptionHandler ...");
		Exception cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
		logger.error("Error occured while process message", cause);
		exchange.getOut().setBody(rejectMessage(ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, "Internal System Error"));
	}

	private String rejectMessage(String processingStatus, String reasonCode, String reasonDescription) {
		logger.debug("Invalid Message Received.");
		logger.debug("Receiving Date: " + SystemHelper.getCurrentDateTime().toString());
		MPayResponse response = new MPayResponse();
		response.setErrorCd(reasonCode);
		response.setStatusCode(processingStatus);
		response.setDesc(reasonDescription);
		return response.toString();
	}
}
