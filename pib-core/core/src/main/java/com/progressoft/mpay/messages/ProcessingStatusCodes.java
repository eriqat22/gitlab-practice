package com.progressoft.mpay.messages;

public class ProcessingStatusCodes {
	public static final String ACCEPTED = "1";
	public static final String PARTIALLY_ACCEPTED = "2";
	public static final String REJECTED = "3";
	public static final String PENDING = "4";
	public static final String FAILED = "5";

	private ProcessingStatusCodes() {

	}
}
