package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.registration.customers.CustomerMobileWorkflowStatuses;

public class MobileValidator {
	private static final Logger logger = LoggerFactory.getLogger(MobileValidator.class);

	private MobileValidator() {

	}

	public static ValidationResult validate(MPAY_CustomerMobile mobile, boolean isSender) {
		logger.debug("Inside Validate ...");
		if (mobile == null || !mobile.getIsRegistered()) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_MOBILE_NOT_REGISTERED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_MOBILE_NOT_REGISTERED, null, false);
		}
		if (!mobile.getIsActive()) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_MOBILE_NOT_ACTIVE, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_MOBILE_NOT_ACTIVE, null, false);
		}

		if (mobile.getIsBlocked()) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_MOBILE_BLOCKED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_MOBILE_BLOCKED, null, false);
		}
		return validateSuspension(mobile, isSender);
	}

	private static ValidationResult validateSuspension(MPAY_CustomerMobile mobile, boolean isSender) {
		if (mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.SUSPENDED)) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_MOBILE_SUSPENDED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_MOBILE_SUSPENDED, null, false);
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
