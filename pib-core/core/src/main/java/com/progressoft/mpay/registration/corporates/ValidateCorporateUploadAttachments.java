package com.progressoft.mpay.registration.corporates;

import static com.progressoft.mpay.MPayErrorMessages.NO_FILE_UPLOADED;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_CorporateAtt;

@Component
@SuppressWarnings({ "rawtypes", "deprecation" })
public class ValidateCorporateUploadAttachments implements Validator {

    @Autowired
    ItemDao itemDao;

    @Override
    public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        MPAY_Corporate customer = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
        String where = "recordId=" + customer.getId();
        Long itemCount = itemDao.getItemCount(MPAY_CorporateAtt.class, null, where);

        if (itemCount == 0) {
            InvalidInputException iie = new InvalidInputException();
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(NO_FILE_UPLOADED));
            throw iie;
        }
    }
}
