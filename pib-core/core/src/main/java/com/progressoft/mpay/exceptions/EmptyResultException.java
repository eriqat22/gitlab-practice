package com.progressoft.mpay.exceptions;

public class EmptyResultException extends RuntimeException {
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public EmptyResultException(String s) {
        super(s);
    }

    public EmptyResultException(Exception e) {
        super(e);
    }
}
