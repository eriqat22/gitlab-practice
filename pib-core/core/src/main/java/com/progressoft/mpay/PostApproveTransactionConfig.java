package com.progressoft.mpay;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;

public class PostApproveTransactionConfig implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostApproveTransactionConfig.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("\n\r===inside PostApproveTransactionConfig....");
		try {

			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_INFO_MSGS,
					AppContext.getMsg("TransactionConfig.info.PleaseConfigurethesenderandreciver"));

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new WorkflowException("An error occured while operator account.", ex);
		}

	}
}