package com.progressoft.mpay.plugins;

public class TransactionDirection {
	public static final long NONE = 0;
	public static final long ONUS = 1;
	public static final long OUTWARD = 2;
	public static final long INWARD = 3;

	private TransactionDirection() {

	}
}
