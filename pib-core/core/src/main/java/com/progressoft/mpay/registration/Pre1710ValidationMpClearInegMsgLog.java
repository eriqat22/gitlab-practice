package com.progressoft.mpay.registration;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CustIntegMessage;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegRjctReason;
import com.progressoft.mpay.entities.MPAY_MpClearResponseCode;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.MpClearIntegRjctReasonCodes;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Pre1710ValidationMpClearInegMsgLog implements FunctionProvider {

	@Autowired
	private ItemDao itemDao;
	private static final Logger logger = LoggerFactory.getLogger(Pre1710ValidationMpClearInegMsgLog.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {

		logger.debug("inside Pre1710ValidationMpClearInegMsgLog---------------------------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());

		String reasonCode = technicalValidation(msg, isomsg);
		if (null == reasonCode) {
			transientVars.put(Result.VALIDATION_RESULT, Result.SUCCESSFUL);
		} else {
			transientVars.put(Result.VALIDATION_RESULT, Result.FAILED);
			MPAY_MpClearIntegRjctReason rsn = itemDao.getItem(MPAY_MpClearIntegRjctReason.class, "code", reasonCode);
			msg.setReason(rsn);
		}
	}

	private String technicalValidation(MPAY_MpClearIntegMsgLog msg, IsoMessage isomsg) {
		if (!msg.getSource().equalsIgnoreCase(IntegMessagesSource.MP_CLEAR)) {
			return MpClearIntegRjctReasonCodes.INVALID_SOURCE;
		}
		MPAY_MpClearResponseCode code = LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(isomsg.getField(44).toString());
		if (null == code) {
			return MpClearIntegRjctReasonCodes.INVALID_RESPONSE_CODE;
		}
		MPAY_CustIntegMessage orgMessage = getOrigionalMessageByMessageID(isomsg.getField(116).toString());
		if (orgMessage == null) {
			return MpClearIntegRjctReasonCodes.INVALID_ORIGINAL_MESSAGE_ID;
		}

		if (!orgMessage.getIntegMsgType().getCode().equalsIgnoreCase(IntegMessageTypes.TYPE_1700)) {
			return MpClearIntegRjctReasonCodes.INVALID_ORIGINAL_MESSAGE_ID;
		}

		return null;
	}

	private MPAY_CustIntegMessage getOrigionalMessageByMessageID(String messageId) {
		MPAY_CustIntegMessage value = null;

		List<MPAY_CustIntegMessage> items = itemDao.getItems(MPAY_CustIntegMessage.class, 0, 1, null, null,
				"refMessage='" + messageId + "'", null);
		if (items != null && !items.isEmpty())
			value = items.get(0);
		return value;
	}
}
