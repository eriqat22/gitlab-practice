package com.progressoft.mpay.limits;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;
import com.progressoft.mpay.entities.MPAY_Profile;

public class ValidateDeleteLimitsScheme implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteLimitsScheme.class);

	@Autowired
	private ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside ValidateDeleteLimitsScheme --------------------- ");

		InvalidInputException iie = new InvalidInputException();
		MPAY_LimitsScheme scheme = (MPAY_LimitsScheme) arg0.get(WorkflowService.WF_ARG_BEAN);

		List<Filter> filters = new ArrayList<>();
		Filter profileFilter = new Filter();
		profileFilter.setProperty("limitsScheme.id");
		profileFilter.setOperator(Operator.EQ);
		profileFilter.setFirstOperand(scheme.getId());
		filters.add(profileFilter);

		Long linkedProfiles = itemDao.getItemCount(MPAY_Profile.class, filters, null);

		if (linkedProfiles != 0) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("limit.invalid.percentage"));
			throw iie;
		}
	}

}
