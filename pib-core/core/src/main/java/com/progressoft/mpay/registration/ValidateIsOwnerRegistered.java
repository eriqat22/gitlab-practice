package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class ValidateIsOwnerRegistered implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(ValidateIsOwnerRegistered.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object sender = transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (sender instanceof MPAY_MobileAccount)
			return ((MPAY_MobileAccount) sender).getMobile().getIsRegistered();
		else if (sender instanceof MPAY_ServiceAccount)
			return ((MPAY_ServiceAccount) sender).getService().getIsRegistered();
		else if (sender instanceof MPAY_CustomerMobile)
			return ((MPAY_CustomerMobile) sender).getIsRegistered();
		else if (sender instanceof MPAY_CorpoarteService)
			return ((MPAY_CorpoarteService) sender).getIsRegistered();
		else
			throw new WorkflowException("Invalid Object Received");
	}
}