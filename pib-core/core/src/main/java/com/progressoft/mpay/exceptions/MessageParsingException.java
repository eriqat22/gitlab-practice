package com.progressoft.mpay.exceptions;

public class MessageParsingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MessageParsingException(String message){
		super(message);
	}

	public MessageParsingException(String message, Exception causedBy){
		super(message, causedBy);
	}
}
