package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;

public class PreReactivateCorporateDevice implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PreReactivateCorporateDevice.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_CorporateDevice device = (MPAY_CorporateDevice) transientVars.get(WorkflowService.WF_ARG_BEAN);
		device.setIsStolen(false);
	}
}