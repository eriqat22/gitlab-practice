package com.progressoft.mpay.registration.corporates;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;

import java.util.List;

public class EmptyChildCorproatesValidator {
    public static void validate(MPAY_Corporate corporate) throws InvalidInputException {

        List<MPAY_CorpoarteService> childs = DataProvider.instance().getCorporatesByParentCorporateId(corporate.getId());
        if (!childs.isEmpty()) {
            InvalidInputException iie = new InvalidInputException();
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DELETE_PARENT_CORPORATE_NOT_ALLOWED));
            throw iie;
        }

    }
}
