package com.progressoft.mpay.registration.bulk;

import com.progressoft.mpay.entity.ClientMetaData;

public abstract class BulkRegistrationLine extends ClientMetaData {

	private String city;
	private String nationality;
	private String clientRef;
	private String prefLang;
	private String bankShortName;
	private String mobileAccSelector;
	private String externalAcc;
	private String alias;
	private String line;
	private int lineNumber;
	private String resolution;
	private String errorDescription;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getClientRef() {
		return clientRef;
	}

	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	public String getPrefLang() {
		return prefLang;
	}

	public void setPrefLang(String prefLang) {
		this.prefLang = prefLang;
	}

	public String getBankShortName() {
		return bankShortName;
	}

	public void setBankShortName(String bankShortName) {
		this.bankShortName = bankShortName;
	}

	public String getMobileAccSelector() {
		return mobileAccSelector;
	}

	public void setMobileAccSelector(String mobileAccSelector) {
		this.mobileAccSelector = mobileAccSelector;
	}

	public String getExternalAcc() {
		return externalAcc;
	}

	public void setExternalAcc(String externalAcc) {
		this.externalAcc = externalAcc;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
}
