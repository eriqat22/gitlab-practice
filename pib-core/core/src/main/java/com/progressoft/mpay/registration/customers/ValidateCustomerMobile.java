package com.progressoft.mpay.registration.customers;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class ValidateCustomerMobile implements Validator {
	private static final Logger logger = LoggerFactory.getLogger(ValidateCustomerMobile.class);

	@Autowired
	ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("ValidateCustomerMobile ==========");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN);
		validateMaxNumberOfMobiles(mobile);
		validateMobileMetaData(mobile);
	}

	private void validateMaxNumberOfMobiles(MPAY_CustomerMobile mobile) throws InvalidInputException {
		List<MPAY_CustomerMobile> undeletedMobiles = select(
				mobile.getRefCustomer().getRefCustomerCustomerMobiles(),
				(having(on(MPAY_CustomerMobile.class).getDeletedFlag(), Matchers.nullValue()).or(having(on(MPAY_CustomerMobile.class).getDeletedFlag(), Matchers.equalTo(false)))).and(having(
						on(MPAY_CustomerMobile.class).getMobileNumber(), Matchers.not(mobile.getMobileNumber()))));
		MPAY_ClientType type = LookupsLoader.getInstance().getCustomerClientType();
		if(type.getMaxNumberOfOwners() == 0)
			return;
		if (!undeletedMobiles.isEmpty()&&undeletedMobiles.size() >= type.getMaxNumberOfOwners()) {
			InvalidInputException cause = new InvalidInputException();
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.MOBILE_MAX_REACHED));
			throw cause;
		}
	}

	private void validateMobileMetaData(MPAY_CustomerMobile mobile) throws InvalidInputException {
		InvalidInputException cause = Unique.validate(itemDao, mobile, "mobileNumber", mobile.getMobileNumber(), MPayErrorMessages.MOBILE_INVALID_NUMBER_IS_USED);
		if (cause.getErrors().size() > 0)
			throw cause;

		cause = Unique.validate(itemDao, mobile, "alias", mobile.getAlias(), MPayErrorMessages.MOBILE_INVALID_ALIAS_IS_USED);
		if (cause.getErrors().size() > 0)
			throw cause;
	}
}
