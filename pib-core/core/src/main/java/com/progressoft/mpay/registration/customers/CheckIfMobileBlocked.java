package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class CheckIfMobileBlocked implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfMobileBlocked.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVar.get(WorkflowService.WF_ARG_BEAN);
		return mobile.getIsBlocked();
	}
}