package com.progressoft.mpay.entity;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_NetworkManActionType;

public enum NetworkManActionTypes {
	LOGIN("LOGIN"), LOGOUT("LOGOUT");

	private String value;

	NetworkManActionTypes(String value) {
		this.value = value;
	}

	public boolean compare(MPAY_NetworkManActionType oNetworkManActionType) {
		if(oNetworkManActionType == null)
			throw new NullArgumentException("oNetworkManActionType");
		return oNetworkManActionType.getCode().equals(this.value);
	}

	@Override
	public String toString() {
		return value;
	}
}