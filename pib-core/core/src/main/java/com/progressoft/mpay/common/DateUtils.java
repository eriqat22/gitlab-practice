package com.progressoft.mpay.common;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.SystemHelper;

public class DateUtils {

    private DateUtils() {
    }

    public static LocalDateTime localDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static LocalDate localDate(Date date) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault()).toLocalDate();
    }

    public static Timestamp timestamp(LocalDateTime date) {
        return Timestamp.from(date.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date date(LocalDate date) {
        return Date.from(LocalDateTime.from(date.atStartOfDay()).atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalTime localTime(Date date) {
        return localDateTime(date).toLocalTime().truncatedTo(ChronoUnit.MINUTES);
    }

    public static Timestamp plusMinutes(int minutes, Timestamp timestamp) {
        return DateUtils.timestamp(DateUtils.localDateTime(timestamp).plus(minutes, ChronoUnit.MINUTES));
    }

    public static Timestamp plusHours(int hours, Timestamp timestamp) {
        return DateUtils.timestamp(DateUtils.localDateTime(timestamp).plus(hours, ChronoUnit.HOURS));
    }

    public static Timestamp minusMinutes(int minutes, Timestamp timestamp) {
        return DateUtils.timestamp(DateUtils.localDateTime(timestamp).minus(minutes, ChronoUnit.MINUTES));
    }

    public static Timestamp minusHours(int hours, Timestamp timestamp) {
        return DateUtils.timestamp(DateUtils.localDateTime(timestamp).minus(hours, ChronoUnit.HOURS));
    }

    public static String toXmlDate(LocalDateTime date) {
        return toXml(DateUtils.timestamp(date)).toXMLFormat();
    }

    public static String toNormalizedXmlDate(LocalDateTime date) {
        return toXml(DateUtils.timestamp(date)).normalize().toXMLFormat();
    }

    public static XMLGregorianCalendar toXMLGregorianDateTime(LocalDateTime time) {
        return toXml(DateUtils.timestamp(time));
    }

    public static XMLGregorianCalendar toXMLGregorianDateWithoutTimeZone(LocalDate date) {
        XMLGregorianCalendar result = toXml(DateUtils.date(date));
        result.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        return result;
    }

    public static XMLGregorianCalendar toNormalizedCalendar(LocalDateTime time) {
        return toXml(DateUtils.timestamp(time)).normalize();
    }

    public static XMLGregorianCalendar toXMLGregorianDate(LocalDate date) {
        return toXml(DateUtils.date(date));
    }

    public static XMLGregorianCalendar toXMLGregorianDate(Date date) {
        return toXml(DateUtils.date(localDate(date)));
    }

    public static XMLGregorianCalendar toXml(Timestamp timestamp) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(timestamp.getTime());
        return toXml(calendar);
    }

    public static XMLGregorianCalendar toXml(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return toXml(calendar);
    }

    public static XMLGregorianCalendar toXml(GregorianCalendar calendar) {
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException e) {
            throw new DateConversionException("failed to process gregorian calendar", e);
        }
    }

    public static Date toDate(XMLGregorianCalendar calendar) {
        return toDate(calendar.toGregorianCalendar());
    }

    public static Date toDate(GregorianCalendar calendar) {
        return calendar.getTime();
    }

    private static class DateConversionException extends RuntimeException {
        private static final long serialVersionUID = 1L;
		public DateConversionException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static Timestamp replaceTime(Date date, Timestamp timespan) {
        return timestamp(localDateTime(date).with(localTime(timespan)));
    }

    public static Timestamp getValidityTime(long mins) {
		long minsInMellSec = mins * 60 * 1000;
		Timestamp validityTime = SystemHelper.getSystemTimestamp();
		validityTime.setTime(validityTime.getTime() + minsInMellSec);
		return validityTime;
	}
    
    public static Timestamp getValidityTimeForOtp(long mins , Timestamp validityTime) {
		long minsInMellSec = mins * 60 * 1000;
		Timestamp otpTime = new Timestamp(validityTime.getTime());
		otpTime.setTime(validityTime.getTime() + minsInMellSec);
		return otpTime;
	}
    
    public static String format(Date date, String format) {
    	SimpleDateFormat dateFormat = new SimpleDateFormat(format);
    	return dateFormat.format(date);
    }
}
