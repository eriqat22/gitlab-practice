package com.progressoft.mpay.common;

import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.mpay.ICryptographer;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.notifications.INotificationHelper;

public class RequestProcessor {
	protected IDataProvider dataProvider;
	protected ISystemParameters systemParameters;
	protected ILookupsLoader lookupsLoader;
	protected ICryptographer cryptographer;
	protected INotificationHelper notificationHelper;
	protected Communicator communicator;

	public RequestProcessor(IDataProvider dataProvider, ISystemParameters systemParameters, ILookupsLoader lookupsLoader, ICryptographer cryptographer, INotificationHelper notificationHelper, Communicator communicator) {
		if (dataProvider == null)
			throw new NullArgumentException("dataProvider");
		if (systemParameters == null)
			throw new NullArgumentException("systemParameters");
		if (lookupsLoader == null)
			throw new NullArgumentException("lookupsLoader");
		if (cryptographer == null)
			throw new NullArgumentException("cryptographer");
		if (notificationHelper == null)
			throw new NullArgumentException("notificationHelper");
		if(communicator == null)
			throw new NullArgumentException("communicator");

		this.dataProvider = dataProvider;
		this.systemParameters = systemParameters;
		this.lookupsLoader = lookupsLoader;
		this.cryptographer = cryptographer;
		this.notificationHelper = notificationHelper;
		this.communicator = communicator;
	}
}
