package com.progressoft.mpay.registration.customers;

import com.progressoft.mpay.plugins.NotificationContext;

public class CustomerRegistrationNotificationContext extends NotificationContext {
	private String appName;
	private String activationCode;
	private boolean hasActivationCode;
	private String accountNumber;
	private String pinCode;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public boolean isHasActivationCode() {
		return hasActivationCode;
	}

	public void setHasActivationCode(boolean hasActivationCode) {
		this.hasActivationCode = hasActivationCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
}
