package com.progressoft.mpay.registration;

public final class RegistrationConstants {
	public static final String WF_REG_INSTRS_STTS_NEW = "New";
	public static final String WF_REG_INSTRS_STTS_WAITING = "Waiting Response";
	public static final String WF_REG_INSTRS_STTS_PROCECCED = "Processed";

	public static final String REG_INSTRS_STTS_NES = "NEW";
	public static final String REG_INSTRS_STTS_SUCCESS = "SUCS";
	public static final String REG_INSTRS_STTS_REJECTED = "RJCT";

	public static final String REG_FILE_STTS_NEW = "NEW";
	public static final String REG_FILE_STTS_ACCEPTED = "ACPT";
	public static final String REG_FILE_STTS_REJECTED = "RJCT";
	public static final String REG_FILE_STTS_PARTIALLY = "PART";

	public static final String REG_INTEG_FILE_TYPE = "csv";

	private RegistrationConstants() {

	}
}
