package com.progressoft.mpay.registration.customers;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CustomerCityChangeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(CustomerCityChangeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside CustomerCityChangeHandler ------------");
		MPAY_Customer customer = (MPAY_Customer)changeHandlerContext.getEntity();
		customer.setArea(null);
	}
}
