package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class PostChangeCustomerMobileAlias implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostChangeCustomerMobileAlias.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("PostApproveNewCustomerMobile************");
        MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN);
        if (!mobile.getIsRegistered())
            return;
        MPAY_CustomerMobile originalMobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN_ORIGINAL);
        mobile.setNewAlias(mobile.getAlias());
        mobile.setAlias(originalMobile.getAlias());
        DataProvider.instance().mergeCustomerMobile(mobile);
    }


}
