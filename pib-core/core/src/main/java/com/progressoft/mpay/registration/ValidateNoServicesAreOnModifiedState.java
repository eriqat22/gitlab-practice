package com.progressoft.mpay.registration;

import java.util.List;

import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;

public class ValidateNoServicesAreOnModifiedState  {
	public static void  validate(MPAY_Corporate corporate) throws WorkflowException {
		InvalidInputException iie = new InvalidInputException();
		List<MPAY_CorpoarteService> services = corporate.getRefCorporateCorpoarteServices();
		for (MPAY_CorpoarteService service : services) {
			if (service.getStatusId().getDescription().equals("Modified")
					|| service.getStatusId().getDescription().equals("Under Modification")) {
				iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("service.under.modification"));
				throw iie;
			}
		}
	}
}
