package com.progressoft.mpay.charges;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Commission;
import com.progressoft.mpay.entities.MPAY_CommissionSlice;
import com.progressoft.mpay.entities.WF_CommissionSlices;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

@Component
public class PostCancelCommissionDetails implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCancelCommissionDetails.class);
    private static final String SLICE_DELETED_STATUS = WF_CommissionSlices.STEP_Deleted;
    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PostApproveCommissionDetails ----------------");
        MPAY_Commission details = (MPAY_Commission) arg0.get(WorkflowService.WF_ARG_BEAN);
        if(details.getRefCommissionCommissionSlices() == null)
        	return;
        for (MPAY_CommissionSlice slice : details.getRefCommissionCommissionSlices()) {
            if (!SLICE_DELETED_STATUS.equals(slice.getStatusId().getCode())) {
                Map<Option, Object> options = new EnumMap<>(Option.class);
                options.put(Option.ACTION_NAME, "SVC_Cancel");
                JfwHelper.executeAction(MPAYView.COMMISSIONS_SLICES, slice, options);
            }
        }
    }

}
