package com.progressoft.mpay.registration.corporates;

import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.MPayContext;
import org.springframework.beans.factory.annotation.Autowired;

public class ServiceAccountsViewChangeHandler implements ChangeHandler {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ServiceAccountsViewChangeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside ServiceAccountsViewChangeHandler*******************-----------------");
		try {
			MPAY_ServiceAccount account = (MPAY_ServiceAccount) changeHandlerContext.getEntity();
			MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "code", MPayContext.getInstance().getSystemParameters().getDefaultBankCode());
			account.setBank(defaultBank);

			CorporatesChangeHandler handler = (CorporatesChangeHandler) Class.forName(MPayContext.getInstance().getSystemParameters().getCorporatesChangeHandler()).newInstance();
			handler.handleServiceAccount(changeHandlerContext);
		} catch (Exception e) {
			logger.error("Failed to create change handler instance", e);
			throw new InvalidInputException(e.getMessage());
		}
	}
}
