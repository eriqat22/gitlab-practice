package com.progressoft.mpay;

public enum SmsNotificationType {

	GENERAL("mpay.notification.notifications") , REGISTRATION("mpay.registration.notification") ;

	private String queue;

	SmsNotificationType(String queue) {
		this.queue = queue;
	}

	public String getQueueName() {
		return queue;
	}
}
