package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.MPAYView;

public class HandleCorporateCancel implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(HandleCorporateCancel.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute ====================");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (corporate.getIsRegistered())
			JfwHelper.executeAction(MPAYView.CORPORATE, corporate, CorporateWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.CORPORATE, corporate, CorporateWorkflowServiceActions.REJECT, new WorkflowException());
	}
}
