package com.progressoft.mpay.mpclear.plugins;

import java.util.List;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingResult;

public class MPClearProcessingResult {

	private MPAY_MPayMessage mpayMessage;
	private MPAY_Transaction transaction;
	private String reasonCode;
	private String reasonDescription;
	private MPAY_MpClearIntegMsgLog response;
	private MPAY_MpClearIntegMsgLog mpClearMessage;
	private List<MPAY_Notification> notifications;

	public MPClearProcessingResult() {
		//Default
	}

	public MPClearProcessingResult(IntegrationProcessingResult integrationResult) {
		if (integrationResult == null)
			throw new NullArgumentException("integrationResult");
		this.mpayMessage = integrationResult.getMpayMessage();
		this.transaction = integrationResult.getTransaction();
		if (integrationResult.getReason() != null)
			this.reasonCode = integrationResult.getReason().getCode();
		this.reasonDescription = integrationResult.getReasonDescription();
		this.mpClearMessage = integrationResult.getMpClearMessage();
		this.response = integrationResult.getMpClearOutMessage();
		this.notifications = integrationResult.getNotifications();
	}

	public MPClearProcessingResult(MessageProcessingResult processingResult) {
		if (processingResult == null)
			throw new NullArgumentException("processingResult");
		this.mpayMessage = processingResult.getMessage();
		this.transaction = processingResult.getTransaction();
		this.reasonCode = processingResult.getReasonCode();
		this.notifications = processingResult.getNotifications();
	}

	public MPAY_MPayMessage getMpayMessage() {
		return mpayMessage;
	}

	public void setMpayMessage(MPAY_MPayMessage mpayMessage) {
		this.mpayMessage = mpayMessage;
	}

	public MPAY_Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(MPAY_Transaction transaction) {
		this.transaction = transaction;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReasonDescription() {
		return reasonDescription;
	}

	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}

	public MPAY_MpClearIntegMsgLog getResponse() {
		return response;
	}

	public void setResponse(MPAY_MpClearIntegMsgLog response) {
		this.response = response;
	}

	public MPAY_MpClearIntegMsgLog getMpClearMessage() {
		return mpClearMessage;
	}

	public void setMpClearMessage(MPAY_MpClearIntegMsgLog mpClearMessage) {
		this.mpClearMessage = mpClearMessage;
	}

	public List<MPAY_Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<MPAY_Notification> notifications) {
		this.notifications = notifications;
	}
}
