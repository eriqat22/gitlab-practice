package com.progressoft.mpay.registration.corporates;

import com.progressoft.jfw.AppException;
import com.progressoft.jfw.AppValidationException;
import com.progressoft.jfw.workflow.WfContext;
import com.progressoft.jfw.workflow.WfValidator;
import com.progressoft.mpay.entities.MPAY_Corporate;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CorporateEnableChannelsValidator extends WfValidator<MPAY_Corporate> {
    @Override
    public void validate(WfContext<MPAY_Corporate> wfContext) {
        MPAY_Corporate corporate = wfContext.getEntity();
        if(isInvalidChannelReceiver(corporate.getEnableSMS(), corporate.getMobileNumber()))
            throw new AppValidationException("Please enter mobile number");
        if(isInvalidChannelReceiver(corporate.getEnableEmail(), corporate.getEmail()))
            throw new AppValidationException("Please enter email address");

    }

    private boolean isInvalidChannelReceiver(Boolean isEnabled, String receiver) {
        return isEnabled && Objects.isNull(receiver);
    }
}
