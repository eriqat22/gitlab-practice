package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class ValidateEditCustomerMobile implements Validator {
	private static final Logger logger = LoggerFactory.getLogger(ValidateCustomerMobile.class);

	@Autowired
	ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("ValidateCustomerMobile ==========");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN);
		InvalidInputException cause = Unique.validate(itemDao, mobile, "mobileNumber", mobile.getMobileNumber(), MPayErrorMessages.MOBILE_INVALID_NUMBER_IS_USED);
		if (cause.getErrors().size() > 0)
			throw cause;
	}
}
