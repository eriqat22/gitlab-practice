package com.progressoft.mpay.notifications;

import com.progressoft.mpay.SmsNotificationType;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.NotificationContext;

public interface INotificationHelper {
	MPAY_Notification createNotification(MPAY_MPayMessage message, NotificationContext notificationContext, long typeId, long operationId, long languageId, String receiver, String channelCode);
	void sendToActiveMQ(MPAY_Notification notification, SmsNotificationType smsNotificationType);
	void sendNotifications(MessageProcessingResult result);
	void sendNotifications(MPAY_Notification notification);
}
