package com.progressoft.mpay.messages;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.ArrayList;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MPayResponse extends MPayMessage {
	private static final Logger logger = LoggerFactory.getLogger(MPayResponse.class);

	private String errorCd;
	private String desc;
	private long ref;
	private String statusCode;

	public MPayResponse() {
		this.extraData = new ArrayList<>();
	}

	public String getErrorCd() {
		return errorCd;
	}

	public void setErrorCd(String errorCd) {
		this.errorCd = errorCd;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public long getRef() {
		return ref;
	}

	public void setRef(long ref) {
		this.ref = ref;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public static MPayResponse fromJson(String json) {
		try {
			Gson gson = new GsonBuilder().create();
			return gson.fromJson(json, MPayResponse.class);
		} catch (Exception ex) {
			logger.debug("Error while fromJson", ex);
			return null;
		}
	}

	public String toValues() {
		StringBuilder sb = new StringBuilder();
		sb.append(getStringValue(getDesc()));
		sb.append(getStringValue(getErrorCd()));
		sb.append(getRef());
		sb.append(getStatusCode());
		writeExtraDataValues(sb);
		return sb.toString();
	}

	public String getValue(String key) {
		ExtraData data = selectFirst(getExtraData(), having(on(ExtraData.class).getKey(), Matchers.equalTo(key)));
		if (data == null)
			return null;
		return data.getValue();
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}
}
