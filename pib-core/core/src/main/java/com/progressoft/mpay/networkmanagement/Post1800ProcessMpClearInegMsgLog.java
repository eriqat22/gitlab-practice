package com.progressoft.mpay.networkmanagement;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgType;
import com.progressoft.mpay.entities.MPAY_NetworkManActionType;
import com.progressoft.mpay.entities.MPAY_NetworkManIntegMsg;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Post1800ProcessMpClearInegMsgLog implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(Post1800ProcessMpClearInegMsgLog.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("insode Post1800ProcessMpClearInegMsgLog---------------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());
		MPAY_NetworkManIntegMsg networkMessage = new MPAY_NetworkManIntegMsg();
		networkMessage.setRefMsgLog(msg);
		MPAY_MpClearIntegMsgType msgType = LookupsLoader.getInstance().getMpClearIntegrationMessageTypes(IntegMessageTypes.TYPE_1800);

		networkMessage.setIntegMsgType(msgType);

		MPAY_NetworkManActionType type = LookupsLoader.getInstance().getNetworkManagementActionTypes(isomsg.getField(48).toString());
		networkMessage.setActionType(type);
		networkMessage.setRefMessage(isomsg.getField(115).toString());
		MPAY_NetworkManagement networkManagement = DataProvider.instance().getNetworkManagement(Long.valueOf(msg.getHint()));
		networkManagement.setRefLastMsgLog(msg);
		try {
			DataProvider.instance().persistNetowkrIntegMessage(networkMessage);
			DataProvider.instance().updateNetworkManagement(networkManagement);
			JfwHelper.sendToActiveMQ(networkMessage, networkMessage.getTenantId(), MPAYView.NETWORK_MAN_1800_MSG.viewName, "mpay.mpclear.network");
		} catch (Exception e) {

			throw new WorkflowException(e);
		}
	}
}