package com.progressoft.mpay.devices.customer;

import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.bussinessobject.core.AbstractJFWEntity;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.jfw.workflow.WfContext;
import com.progressoft.jfw.workflow.WfFunction;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Component
public class PreDeleteCustomerDevice extends WfFunction<MPAY_CustomerDevice> {

    @Override
    public void execute(WfContext<MPAY_CustomerDevice> wfContext) {
        MPAY_CustomerDevice entity = wfContext.getEntity();
        if (entity.getIsDefault()) {
            entity.setIsDefault(false);
            List<MPAY_CustomerDevice> devices = itemDao.getItems(MPAY_CustomerDevice.class, null, getFilterList(entity), null, null);
            devices.sort(Comparator.comparing(AbstractJFWEntity::getCreationDate));
            devices.stream().findFirst().ifPresent(c-> c.setIsDefault(true));
        }
    }

    private List<Filter> getFilterList(MPAY_CustomerDevice entity) {
        return Collections.singletonList(Filter.get(MPAY_CustomerDevice.REF_CUSTOMER_MOBILE + ".id", Operator.EQ, entity.getRefCustomerMobile().getId()));
    }
}
