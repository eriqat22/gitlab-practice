package com.progressoft.mpay;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;

public class CheckIfTransactionReversed implements Condition {

    private static final Logger logger = LoggerFactory.getLogger(CheckIfTransactionReversed.class);

    @SuppressWarnings("rawtypes")
    @Override
    public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside passesCondition ------------");
        MPAY_Transaction transaction = (MPAY_Transaction) transientVar.get(WorkflowService.WF_ARG_BEAN);
        return (!transaction.getIsReversed() && transaction.getReversable() && isAccepted(transaction));
    }

    private boolean isAccepted(MPAY_Transaction transaction) {
        return transaction.getProcessingStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED);
    }
}