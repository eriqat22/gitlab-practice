package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;

public class PostMobileAccountApproveDefaultApproval implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostMobileAccountApproveDefaultApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_MobileAccount account = (MPAY_MobileAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
		handleAccounts(account.getMobile(), account.getId());
	}

	private void handleAccounts(MPAY_CustomerMobile mobile, long currentAccountId) throws WorkflowException {
		logger.debug("inside HandleAccounts =========");
		List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_MobileAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_MobileAccount account = accountsIterator.next();
			if (account.getId() == currentAccountId)
				continue;
			account.setIsDefault(false);
			DataProvider.instance().updateMobileAccount(account);
		} while (accountsIterator.hasNext());
	}
}
