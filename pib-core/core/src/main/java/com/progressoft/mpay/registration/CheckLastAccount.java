package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class CheckLastAccount implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckLastAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object account = transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (account instanceof MPAY_MobileAccount) {
			MPAY_MobileAccount mobileAccount = (MPAY_MobileAccount) account;
			if (mobileAccount.getMobile().getMobileMobileAccounts() == null)
				return false;
			boolean isValid = mobileAccount.getMobile().getMobileMobileAccounts().size() > 1;
			if (!isValid)
				return isValid;
			return !(mobileAccount.getIsDefault() || mobileAccount.getIsSwitchDefault());
		} else {
			MPAY_ServiceAccount serviceAccount = (MPAY_ServiceAccount) account;
			if (serviceAccount.getService().getServiceServiceAccounts() == null)
				return false;
			boolean isValid = serviceAccount.getService().getServiceServiceAccounts().size() > 1;
			if (!isValid)
				return isValid;
			return !(serviceAccount.getIsDefault() || serviceAccount.getIsSwitchDefault());
		}
	}
}