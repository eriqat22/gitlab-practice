package com.progressoft.mpay.payments.cashin;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.tax.TaxCalculator;

public class CashInIsChargeIncluded implements ChangeHandler {

	private static final String REF_MOBILE_ACCOUNT = "refMobileAccount";
	private static final Logger logger = LoggerFactory.getLogger(CashInIsChargeIncluded.class);
	public static final String RECACULATIONSTEP = "100506";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("****inside handle : " + changeHandlerContext.getSource());

		MPAY_CashIn cashIn = (MPAY_CashIn) changeHandlerContext.getEntity();
		if (cashIn.getAmount() != null && cashIn.getAmount().doubleValue() > 0 && cashIn.getRefCustMob() != null) {
			MPAY_MobileAccount account = SystemHelper.getMobileAccount(MPayContext.getInstance().getSystemParameters(), cashIn.getRefCustMob(), cashIn.getRefMobileAccount());
			MPAY_Profile profile = account.getRefProfile();
			BigDecimal chargeAmount = ChargesCalculator.calculateCharge(MPayContext.getInstance().getDataProvider(), cashIn.getAmount(), MessageCodes.CI.toString(), profile, false);
			BigDecimal taxAmount = TaxCalculator.calculateTax(MPayContext.getInstance().getDataProvider(), chargeAmount, MessageCodes.CI.toString(), profile, MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
			cashIn.setRefMobileAccount(prepareRefMobileAccount(cashIn.getRefMobileAccount()));
			cashIn.setTransAmount(cashIn.getAmount());
			cashIn.setChargeAmount(chargeAmount);
			cashIn.setTaxAmount(taxAmount);
			cashIn.setTotalAmount(cashIn.getAmount());
		}

		if (cashIn.getRefCustMob() != null && cashIn.getStatusId() !=null &&cashIn.getStatusId().getCode().equals(RECACULATIONSTEP) ) {
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setRequired(false);
		}else if (cashIn.getRefCustMob() != null) {
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setEnabled(true);
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setRequired(true);
		}
		else {
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setRequired(false);
		}
}

	private String prepareRefMobileAccount(String refMobileAccount) {
		if(refMobileAccount ==null)
			return null ;
		if(refMobileAccount.contains("{value={value=")){
			String s = refMobileAccount.split(",")[0];
			return  s.replace("{value={value=","");
		}
		if (refMobileAccount.contains("key") || refMobileAccount.contains("value")) {
			String[] split = refMobileAccount.split("=");
			return split[1].replace(",", "").replace("key", "");
		}
		return refMobileAccount;
	}
}