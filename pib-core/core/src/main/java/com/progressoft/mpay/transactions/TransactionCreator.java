package com.progressoft.mpay.transactions;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.JournalVoucherFlags;
import com.progressoft.mpay.entity.JvDetailsTypes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.TransactionDirection;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;

public abstract class TransactionCreator {

    private static final Logger logger = LoggerFactory.getLogger(TransactionCreator.class);

    private static MPAY_Transaction createTransactionEntity(ProcessingContext context, String typeCode,
                                                            BigDecimal originalAmount, BigDecimal externalFees, String comments) {
        logger.debug("Inside CreateTransactionEntity ...");
        MPAY_TransactionType type = context.getLookupsLoader().getTransactionType(typeCode);
        if (type == null)
            throw new InvalidArgumentException("typeCode = " + typeCode);

        BigDecimal deductedCharge = context.getSender().getCharge();
        BigDecimal deductedTax = context.getSender().getTax();
        if (context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_DEBIT)) {
            deductedCharge = context.getReceiver().getCharge();
            deductedTax = context.getReceiver().getTax();
        }
        MPAY_Transaction transaction = new MPAY_Transaction();
        transaction.setComments(comments);
        transaction.setSenderInfo(context.getSender().getInfo());
        transaction.setSenderMobile(context.getSender().getMobile());
        transaction.setSenderMobileAccount(context.getSender().getMobileAccount());
        transaction.setSenderService(context.getSender().getService());
        transaction.setSenderServiceAccount(context.getSender().getServiceAccount());
        transaction.setReceiverInfo(context.getReceiver().getInfo());
        transaction.setReceiverMobile(context.getReceiver().getMobile());
        transaction.setReceiverMobileAccount(context.getReceiver().getMobileAccount());
        transaction.setReceiverService(context.getReceiver().getService());
        transaction.setReceiverServiceAccount(context.getReceiver().getServiceAccount());
        transaction.setSenderBank(context.getSender().getBank());
        transaction.setReceiverBank(context.getReceiver().getBank());
        transaction.setOriginalAmount(originalAmount);
        transaction.setTotalAmount(originalAmount.add(deductedCharge.add(deductedTax)));
        transaction.setTotalCharge(context.getSender().getCharge().add(context.getReceiver().getCharge()));
        transaction.setSenderCharge(context.getSender().getCharge());
        transaction.setReceiverCharge(context.getReceiver().getCharge());
        transaction.setExternalFees(BigDecimal.ZERO);
        transaction.setCurrency(context.getLookupsLoader().getDefaultCurrency());
        transaction.setExternalFees(externalFees);
        transaction.setSenderTax(context.getSender().getTax());
        transaction.setReceiverTax(context.getReceiver().getTax());

        transaction.setDirection(context.getLookupsLoader().getTransactionDirection(context.getDirection()));
        transaction.setRefType(type);
        transaction.setRefOperation(context.getOperation());

        if (context.getMessage() != null) {
            transaction.setRefMessage(context.getMessage());
            transaction.setTransDate(context.getMessage().getProcessingStamp());
            transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
            transaction.setReason(context.getMessage().getReason());
            transaction.setReasonDesc(context.getMessage().getReasonDesc());
            transaction.setReference(context.getMessage().getReference());
            transaction.setRequestedId(context.getMessage().getRequestedId());
            transaction.setShopId(context.getMessage().getShopId());
            transaction.setChannelId(context.getMessage().getChannelId());
            transaction.setWalletId(context.getMessage().getWalletId());
            transaction.setExternalReference(getExternalReference(context.getMessage().getReference()));
        } else {
            transaction.setTransDate(SystemHelper.getSystemTimestamp());
            transaction
                    .setProcessingStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
            transaction.setReason(context.getLookupsLoader().getReason(ReasonCodes.VALID));
            transaction.setReference(String.valueOf(context.getDataProvider().getNextReferenceNumberSequence()));
            transaction.setExternalReference(
                    getExternalReference(String.valueOf(context.getDataProvider().getNextReferenceNumberSequence())));
        }
        transaction.setRefTrsansactionJvDetails(new ArrayList<>());
        transaction.setReversable(context.getTransactionConfig().getReversable());
        if (context.getDirection() == TransactionDirection.INWARD) {
            IntegrationProcessingContext integrationProcessingContext = (IntegrationProcessingContext) context;
            MPAY_MpClearIntegMsgLog mpClearMessage = integrationProcessingContext.getMpClearMessage();
            if (mpClearMessage != null)
                transaction.setInwardMessage(mpClearMessage);
        }
        return transaction;
    }

    private static String getExternalReference(String reference) {
        String externalReference;
        if (reference.length() < 6) {
            externalReference = StringUtils.leftPad(reference, 6, "0");
        } else
            externalReference = reference.substring(reference.length() - 6, reference.length());
        return externalReference;
    }

    public static MPAY_JvDetail createJVDetail(MPAY_Transaction transaction, String debitCreditType,
                                               MPAY_Account refAccount, BigDecimal amount, MPAY_JvType jvType, String description) {
        logger.debug("Inside createJVDetail ...");
        String direction;
        String actualDescription = description;
        if (Objects.equals(debitCreditType, JournalVoucherFlags.CREDIT))
            direction = "Credit";
        else if (Objects.equals(debitCreditType, JournalVoucherFlags.DEBIT))
            direction = "Debit";
        else
            throw new InvalidArgumentException("debitCreditType == " + debitCreditType);
        if (actualDescription == null)
            actualDescription = direction + " " + refAccount.getAccNumber() + " for " + jvType.getName();

        MPAY_JvDetail detail = new MPAY_JvDetail();
        detail.setRefTrsansaction(transaction);
        detail.setSerial((long) transaction.getRefTrsansactionJvDetails().size() + 1);
        detail.setDescription(actualDescription);
        detail.setDebitCreditFlag(debitCreditType);
        detail.setRefAccount(refAccount);
        detail.setCurrency(transaction.getCurrency());
        detail.setAmount(amount);
        detail.setJvType(jvType);
        detail.setIsPosted(false);
        detail.setTenantId(transaction.getTenantId());
        detail.setOrgId(transaction.getOrgId());
        detail.setCreatedBy(transaction.getCreatedBy());
        detail.setCreationDate(SystemHelper.getSystemTimestamp());
        detail.setUpdatedBy(transaction.getCreatedBy());
        detail.setUpdatingDate(transaction.getCreationDate());
        if (transaction.getRefTrsansactionJvDetails() == null)
            transaction.setRefTrsansactionJvDetails(new ArrayList<>());
        transaction.getRefTrsansactionJvDetails().add(detail);
        return detail;
    }

    protected abstract void createOnusJVs(ProcessingContext context, MPAY_Transaction transaction);

    protected abstract void createOutwardJVs(ProcessingContext context, MPAY_Transaction transaction);

    protected abstract void createInwardJVs(ProcessingContext context, MPAY_Transaction transaction);

    public MPAY_Transaction createTransaction(ProcessingContext context, String typeCode, BigDecimal externalFees,
                                              String comments) {
        logger.debug("Inside CreateTransaction for MessageProcessor ...");

        if (context == null)
            throw new NullArgumentException("context");
        MPAY_Transaction transaction = createTransactionEntity(context, typeCode, context.getAmount(), externalFees,
                comments);
        transaction.setIsReversed(false);
        createJVDetails(context, transaction);
        createChargesJVs(context, transaction);
        createTaxesJVs(context, transaction);
        return transaction;
    }

    private void createJVDetails(ProcessingContext context, MPAY_Transaction transaction) {
        if (context.getDirection() == TransactionDirection.ONUS)
            createOnusJVs(context, transaction);
        else if (context.getDirection() == TransactionDirection.OUTWARD)
            createOutwardJVs(context, transaction);
        else if (context.getDirection() == TransactionDirection.INWARD)
            createInwardJVs(context, transaction);
        else
            throw new InvalidArgumentException(
                    "context.getDirection() is not supported, value: " + context.getDirection());
    }

    private void createChargesJVs(ProcessingContext context, MPAY_Transaction transaction) {
        if (context.getSender().getCharge().compareTo(BigDecimal.ZERO) > 0)
            createSenderCharges(context, transaction);
        if (context.getReceiver().getCharge().compareTo(BigDecimal.ZERO) > 0)
            createReceiverCharges(context, transaction);
    }

    private void createSenderCharges(ProcessingContext context, MPAY_Transaction transaction) {
        if (context.getSender().getAccount() == null)
            return;
        createJVDetail(transaction, JournalVoucherFlags.DEBIT, context.getSender().getAccount(),
                context.getSender().getCharge(),
                context.getLookupsLoader().getJvTypes(JvDetailsTypes.CHARGES.toString()), null);
        createJVDetail(transaction, JournalVoucherFlags.CREDIT,
                context.getLookupsLoader().getPSP().getPspChargeAccount(), context.getSender().getCharge(),
                context.getLookupsLoader().getJvTypes(JvDetailsTypes.CHARGES.toString()), null);
    }

    private void createReceiverCharges(ProcessingContext context, MPAY_Transaction transaction) {
        if (context.getReceiver().getAccount() == null)
            return;
        createJVDetail(transaction, JournalVoucherFlags.DEBIT, context.getReceiver().getAccount(),
                context.getReceiver().getCharge(),
                context.getLookupsLoader().getJvTypes(JvDetailsTypes.CHARGES.toString()), null);
        createJVDetail(transaction, JournalVoucherFlags.CREDIT,
                context.getLookupsLoader().getPSP().getPspChargeAccount(), context.getReceiver().getCharge(),
                context.getLookupsLoader().getJvTypes(JvDetailsTypes.CHARGES.toString()), null);
    }

    private void createTaxesJVs(ProcessingContext context, MPAY_Transaction transaction) {
        createTaxJV(context, transaction, context.getSender().getTax(), context.getSender().getAccount());
        createTaxJV(context, transaction, context.getReceiver().getTax(), context.getReceiver().getAccount());
    }

    private void createTaxJV(ProcessingContext context, MPAY_Transaction transaction, BigDecimal amount,
                             MPAY_Account debtorAccount) {
        if (amount.compareTo(BigDecimal.ZERO) == 0)
            return;
        MPAY_Account taxesAccount = context.getLookupsLoader().getPSP().getPspTaxesAccount();
        createJVDetail(transaction, JournalVoucherFlags.DEBIT, debtorAccount, amount,
                context.getLookupsLoader().getJvTypes(JvDetailsTypes.TAX.toString()), null);
        createJVDetail(transaction, JournalVoucherFlags.CREDIT, taxesAccount, amount,
                context.getLookupsLoader().getJvTypes(JvDetailsTypes.TAX.toString()), null);
    }
}