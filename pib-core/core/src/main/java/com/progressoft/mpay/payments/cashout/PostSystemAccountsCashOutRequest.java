package com.progressoft.mpay.payments.cashout;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_JvDetail;
import com.progressoft.mpay.entities.MPAY_SystemAccountsCashOut;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.BalanceTypes;
import com.progressoft.mpay.entity.JournalVoucherFlags;
import com.progressoft.mpay.entity.JvDetailsTypes;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.transactions.AccountingHelper;

public class PostSystemAccountsCashOutRequest implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostSystemAccountsCashOutRequest.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {

		logger.debug("***inside execute");
		MPAY_SystemAccountsCashOut cashOut = (MPAY_SystemAccountsCashOut) arg0.get(WorkflowService.WF_ARG_BEAN);
		MPAY_CorpoarteService sourceService = cashOut.getSourceService();
		MPAY_CorpoarteService destinationService = cashOut.getDestinationService();

		MPAY_Transaction transaction = new MPAY_Transaction();
		transaction.setComments(cashOut.getComments());
		transaction.setSenderInfo(sourceService.getName());
		transaction.setReceiverInfo(destinationService.getName());
		transaction.setDirection(MPayContext.getInstance().getLookupsLoader().getTransactionDirection(TransactionDirection.ONUS));
		transaction.setReference(String.valueOf(MPayContext.getInstance().getDataProvider().getNextReferenceNumberSequence()));
		transaction.setSenderService(sourceService);
		transaction.setReceiverService(destinationService);
		transaction.setSenderServiceAccount(sourceService.getServiceServiceAccounts().get(0));
		transaction.setReceiverServiceAccount(destinationService.getServiceServiceAccounts().get(0));
		transaction.setRefType(MPayContext.getInstance().getLookupsLoader().getTransactionType(TransactionTypeCodes.DIRECT_CREDIT));
		transaction.setSenderBank(transaction.getSenderServiceAccount().getBank());
		transaction.setReceiverBank(transaction.getReceiverServiceAccount().getBank());
		transaction.setTransDate(SystemHelper.getSystemTimestamp());
		transaction.setCurrency(cashOut.getCurrency());
		transaction.setTotalCharge(BigDecimal.ZERO);
		transaction.setOriginalAmount(cashOut.getAmount());
		transaction.setTotalAmount(cashOut.getAmount());
		transaction.setSenderCharge(BigDecimal.ZERO);
		transaction.setReceiverCharge(BigDecimal.ZERO);
		transaction.setSenderTax(BigDecimal.ZERO);
		transaction.setReceiverTax(BigDecimal.ZERO);
		transaction.setProcessingStatus(MPayContext.getInstance().getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED));
		transaction.setReason(MPayContext.getInstance().getLookupsLoader().getReason(ReasonCodes.VALID));
		transaction.setIsReversed(false);
		transaction.setExternalFees(BigDecimal.ZERO);
		transaction.setReference(String.valueOf(MPayContext.getInstance().getDataProvider().getNextReferenceNumberSequence()));

		appendTransactionJVs(transaction);

		transaction.setRefTransactionSystemAccountsCashOut(new ArrayList<>());
		transaction.getRefTransactionSystemAccountsCashOut().add(cashOut);
		MPayContext.getInstance().getDataProvider().persistTransaction(transaction);
		cashOut.setRefTransaction(MPayContext.getInstance().getDataProvider().getTransactionByTransactionRef(transaction.getReference()));
		try {
			JVPostingResult result = AccountingHelper.postAccounts(MPayContext.getInstance().getDataProvider(), MPayContext.getInstance().getLookupsLoader(), transaction);
			if (result.isSuccess()) {
				transaction.setProcessingStatus(MPayContext.getInstance().getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED));
				cashOut.setProcessingStatus(transaction.getProcessingStatus());
				cashOut.setReason(transaction.getReason());
				JfwHelper.executeAction(MPAYView.SYSTEM_ACCOUNTS_CASHOUT, cashOut, "SVC_Accept", new WorkflowException());
			} else {
				transaction.setReason(MPayContext.getInstance().getLookupsLoader().getReason(result.getReason()));
				transaction.setProcessingStatus(MPayContext.getInstance().getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED));
				cashOut.setProcessingStatus(transaction.getProcessingStatus());
				cashOut.setReason(transaction.getReason());
				JfwHelper.executeAction(MPAYView.SYSTEM_ACCOUNTS_CASHOUT, cashOut, "SVC_Reject", new WorkflowException());
			}
		} catch (Exception e) {
			logger.error("Error while posting JVs", e);
			transaction.setReason(MPayContext.getInstance().getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR));
			transaction.setProcessingStatus(MPayContext.getInstance().getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED));
			cashOut.setProcessingStatus(transaction.getProcessingStatus());
			cashOut.setReason(transaction.getReason());
			JfwHelper.executeAction(MPAYView.SYSTEM_ACCOUNTS_CASHOUT, cashOut, "SVC_Reject", new WorkflowException());
		}
	}

	private void appendTransactionJVs(MPAY_Transaction transaction) {
		transaction.setRefTrsansactionJvDetails(new ArrayList<>());
		MPAY_Account debitorAccount = transaction.getSenderServiceAccount().getRefAccount();
		MPAY_Account creditorAccount = transaction.getReceiverServiceAccount().getRefAccount();
		if (debitorAccount.getBalanceType().getCode().equals(BalanceTypes.CREDIT)) {
			debitorAccount = transaction.getReceiverServiceAccount().getRefAccount();
			creditorAccount = transaction.getSenderServiceAccount().getRefAccount();
		}
		transaction.getRefTrsansactionJvDetails().add(createJVDetails(transaction, debitorAccount, JournalVoucherFlags.DEBIT));
		transaction.getRefTrsansactionJvDetails().add(createJVDetails(transaction, creditorAccount, JournalVoucherFlags.CREDIT));
	}

	private MPAY_JvDetail createJVDetails(MPAY_Transaction transaction, MPAY_Account account, String flag) {
		String description;
		BigDecimal balanceAfter;
		if (flag.equals(JournalVoucherFlags.CREDIT)) {
			description = "Credit " + account.getAccNumber();
			balanceAfter = account.getBalance().add(transaction.getOriginalAmount());
		} else {
			description = "Debit " + account.getAccNumber();
			balanceAfter = account.getBalance().subtract(transaction.getOriginalAmount());
		}
		MPAY_JvDetail jv = new MPAY_JvDetail();
		jv.setRefTrsansaction(transaction);
		jv.setSerial(1L);
		jv.setDescription(description);
		jv.setJvType(MPayContext.getInstance().getLookupsLoader().getJvTypes(JvDetailsTypes.TRANSFER.toString()));
		jv.setDebitCreditFlag(flag);
		jv.setRefAccount(account);
		jv.setCurrency(transaction.getCurrency());
		jv.setAmount(transaction.getOriginalAmount());
		jv.setBalanceBefore(account.getBalance());
		jv.setBalanceAfter(balanceAfter);
		jv.setIsPosted(false);
		return jv;
	}
}
