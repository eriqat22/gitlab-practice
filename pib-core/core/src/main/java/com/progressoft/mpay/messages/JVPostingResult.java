package com.progressoft.mpay.messages;

import com.progressoft.mpay.entity.ReasonCodes;

public class JVPostingResult {
	private boolean isSuccess;
	private String reason;

	public JVPostingResult() {
		//Default
	}

	public JVPostingResult(boolean isSuccess, String reason) {
		this.isSuccess = isSuccess;
		this.reason = reason;
	}

	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public static JVPostingResult success() {
		JVPostingResult result = new JVPostingResult();
		result.setSuccess(true);
		result.setReason(ReasonCodes.VALID);
		return result;
	}
}
