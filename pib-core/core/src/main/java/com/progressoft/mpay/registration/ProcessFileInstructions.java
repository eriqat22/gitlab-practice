package com.progressoft.mpay.registration;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.jfw.integration.JfwFacade;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.ValidationException;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_Intg_Cust_Reg_Instr;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.exceptions.InvalidRecordException;
import com.progressoft.mpay.exceptions.WorkflowExecutionException;

/**
 * @author u163
 */
public class ProcessFileInstructions implements Processor {
    private static final Logger logger = LoggerFactory.getLogger(ProcessFileInstructions.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        logger.debug("inside process file instruction file registration processor...");
        String body = (String) exchange.getIn().getBody();
        logger.debug("body=" + body);
        boolean enabledByMe = IntegrationUtils.enableServiceUser((String) exchange.getProperty("tenantId"));
        ItemDao itemDao = (ItemDao) AppContext.getApplicationContext().getBean("itemDao");
        List<MPAY_Intg_Cust_Reg_Instr> instrs = itemDao.getItems(MPAY_Intg_Cust_Reg_Instr.class, null, prepareFilter(), null, null);
        logger.debug("Retreived Instructions = " + instrs.size());
        processInstructions(instrs, itemDao);
        if (enabledByMe) {
            IntegrationUtils.disableServiceUser();
        }
    }


    private void processInstructions(List<MPAY_Intg_Cust_Reg_Instr> instrs, ItemDao itemDao) {
        logger.debug("***********inside processInstructions function***********");
        MPAY_Profile profile = null;
        if (!instrs.isEmpty()) {
            String defaultProfileSysConfigValue = SystemParameters.getInstance().getDefaultProfile();
            profile = itemDao.getItem(MPAY_Profile.class, "code", defaultProfileSysConfigValue);
            if (profile == null)
                throw new UnsupportedOperationException("Default profile is not defined");
        }

        for (MPAY_Intg_Cust_Reg_Instr inst : instrs) {
            logger.debug("Going to register instruction with record id at mpclear = " + inst.getRegInstrRecordId());
            MPAY_Customer customer = new MPAY_Customer();
            validateCustomerInstruction(inst, itemDao);
            setCustomerInformation(profile, inst, customer);
            MPAY_CustomerMobile mobile = new MPAY_CustomerMobile();
            setMobileInformation(profile, inst, customer, mobile);
            persistCustomerAndExecuteWF(customer, inst);
            executeInstructionWF(inst);
        }
    }


    private void validateCustomerInstruction(MPAY_Intg_Cust_Reg_Instr inst, ItemDao itemDao) {
        if (inst.getRegInstrRecordId() == null || "-1".equals(inst.getRegInstrRecordId())) {
            throw new InvalidRecordException("Invalid Record ID");
        }

        validateCustomerInformation(inst);
        validateMobileInformation(inst, itemDao);
    }


    private void executeInstructionWF(MPAY_Intg_Cust_Reg_Instr inst) {
        try {
            JfwHelper.executeAction(MPAYView.FILE_INSTRUCTIONS, inst.getId(), "SVC_Advance");
        } catch (WorkflowException e) {
            throw new WorkflowExecutionException(e);
        }
    }


    private void persistCustomerAndExecuteWF(MPAY_Customer customer, MPAY_Intg_Cust_Reg_Instr inst) {
        logger.debug("Persisting Customer/Mobile into database and execute workflow actions (create and approve).");
        try {
            customer.setInstId(inst);

            Map<Option, Object> nestedOptions = new EnumMap<>(Option.class);
            nestedOptions.put(Option.ORG_SHORT_NAME, "SUPER");

            Map<String, Object> transientVar = new HashMap<>();
            transientVar.put("dontCheckMaxService", "1");

            JfwFacade jfwFacade = AppContext.getApplicationContext().getBean("defaultJfwFacade", JfwFacade.class);
            jfwFacade.createEntity(MPAYView.CUSTOMER.viewName, customer, transientVar, nestedOptions);

            MPAY_Customer customerEntity = AppContext.getServiceLocator().getItemManagmentService().getItem(customer.getClass().getName(), String.valueOf(customer.getId()));

            logger.debug("Executing Approve action on the customer entity");

            Map<Option, Object> options = new EnumMap<>(Option.class);
            options.put(Option.ACTION_NAME, "Approve");

            jfwFacade.executeAction(MPAYView.CUSTOMER.viewName, null, customerEntity, transientVar, options);
        } catch (Exception ex) {
            throw new WorkflowExecutionException(ex);
        }
    }


    private void setMobileInformation(MPAY_Profile profile, MPAY_Intg_Cust_Reg_Instr inst, MPAY_Customer customer, MPAY_CustomerMobile mobile) {
        mobile.setMobileNumber(inst.getMobileNumber());
        mobile.setRefCustomer(customer);
        mobile.setAlias(inst.getAlias());
        mobile.setNfcSerial(inst.getNfcSerial());
        mobile.setBankedUnbanked("2");
        mobile.setBank(LookupsLoader.getInstance().getBank(inst.getBankShortName()));
        if (inst.getMobileAccSelector() != null) {
            mobile.setMas(Long.parseLong(inst.getMobileAccSelector()));
        }
        mobile.setRefProfile(profile);
        mobile.setNotificationShowType(inst.getNotificationShowType());
        mobile.setEmail(inst.getEmail());
        mobile.setIsActive(true);
    }

    private void validateMobileInformation(MPAY_Intg_Cust_Reg_Instr inst, ItemDao itemDao) {
        MPAY_CustomerMobile mobile = DataProvider.instance().getCustomerMobile(inst.getMobileNumber());
        Pattern pattern = Pattern.compile("[0-9]+");

        if (mobile != null)
            throw new ValidationException("Mobile number is already registered");

        mobile = itemDao.getItem(MPAY_CustomerMobile.class, "alias", inst.getAlias());
        if (mobile != null)
            throw new ValidationException("Alias is already registered");


        if (LookupsLoader.getInstance().getBank(inst.getBankShortName()) == null)
            throw new ValidationException("Bank is not defined in system");

        if (!pattern.matcher(inst.getMobileAccSelector()).matches())
            throw new ValidationException("Invalid account selector");


        if (!"1".equals(inst.getNotificationShowType()) && !"2".equals(inst.getNotificationShowType()))
            throw new ValidationException("Invalid notification type");
    }

    private void setCustomerInformation(MPAY_Profile profile, MPAY_Intg_Cust_Reg_Instr inst, MPAY_Customer customer) {
        customer.setFirstName(inst.getFirstName());
        customer.setMiddleName(inst.getMiddleName());
        customer.setLastName(inst.getLastName());
        customer.setIsActive(true);
        customer.setNationality(LookupsLoader.getInstance().getCountry(inst.getNationality()));
        customer.setIdType(LookupsLoader.getInstance().getIDType(inst.getIdType()));
        customer.setIdNum(inst.getIdNum());
        try {
            customer.setDob(SystemHelper.parseDate(inst.getDob(), "yyyy-MM-dd"));
        } catch (Exception exp) {

            throw new ValidationException("Invalid Date of Birth Format.", exp);
        }
        String personCode = "150";
        customer.setClientType(LookupsLoader.getInstance().getClientType(personCode));
        customer.setClientRef(inst.getClientRef());
        customer.setPhoneOne(inst.getPhoneOne());
        customer.setPhoneTwo(inst.getPhoneTwo());
        customer.setCity(LookupsLoader.getInstance().getCity(inst.getCity()));
        customer.setPrefLang(LookupsLoader.getInstance().getLanguageByCode(inst.getPrefLang()));
        customer.setEmail(inst.getEmail());
        customer.setPobox(inst.getPobox());
        customer.setZipCode(inst.getZipCode());
        customer.setBuildingNum(inst.getBuildingNum());
        customer.setStreetName(inst.getStreetName());
        customer.setNote(inst.getNote());
        customer.setAlias(inst.getAlias());
        customer.setNfcSerial(inst.getNfcSerial());

        customer.setRefProfile(profile);
        customer.setMobileNumber(inst.getMobileNumber());
        customer.setBankedUnbanked("2");
        customer.setBank(LookupsLoader.getInstance().getBank(inst.getBankShortName()));

        customer.setNotificationShowType(inst.getNotificationShowType());

        customer.setExternalAcc(inst.getExternalAcc());
    }

    private void validateCustomerInformation(MPAY_Intg_Cust_Reg_Instr inst) {
        validateCustomerName(inst);
        validateCustomerAddress(inst);
        validateCustomerId(inst);
        validateCustomerDOB(inst);
        validateCustomerLangCode(inst);
    }

    private void validateCustomerLangCode(MPAY_Intg_Cust_Reg_Instr inst) {
        if (LookupsLoader.getInstance().getLanguageByCode(inst.getPrefLang()) == null)
            throw new ValidationException("Invalid language id");
    }

    private void validateCustomerDOB(MPAY_Intg_Cust_Reg_Instr inst) {
        try {
            SystemHelper.parseDate(inst.getDob(), "yyyy-MM-dd");
        } catch (Exception exp) {
            throw new ValidationException("Invalid Date of Birth Format.", exp);
        }
    }

    private void validateCustomerId(MPAY_Intg_Cust_Reg_Instr inst) {
        if (LookupsLoader.getInstance().getIDType(inst.getIdType()) == null) {
            throw new ValidationException("Invalid customer id type");
        }
        if (inst.getIdNum() == null || "".equals(inst.getIdNum().trim())) {
            throw new ValidationException("Invalid customer id");
        }
    }

    private void validateCustomerAddress(MPAY_Intg_Cust_Reg_Instr inst) {
        if (LookupsLoader.getInstance().getCountry(inst.getNationality()) == null)
            throw new ValidationException("Invalid country code");
        if (LookupsLoader.getInstance().getCity(inst.getCity()) == null)
            throw new ValidationException("Invalid city code");
    }

    private void validateCustomerName(MPAY_Intg_Cust_Reg_Instr inst) {
        if (inst.getFirstName() == null || "".equals(inst.getFirstName().trim()))
            throw new ValidationException("First name is required");

        if (inst.getMiddleName() == null || "".equals(inst.getMiddleName().trim()))
            throw new ValidationException("Middle name is required");

        if (inst.getLastName() == null || "".equals(inst.getLastName().trim()))
            throw new ValidationException("Last name is required");
    }


    private List<Filter> prepareFilter() {
        List<Filter> filters = new ArrayList<>();
        Filter stts = Filter.get("statusId.name", Operator.LIKE, RegistrationConstants.WF_REG_INSTRS_STTS_NEW);
        filters.add(stts);
        stts = Filter.get("regInstrProcessingStts.code", Operator.LIKE, RegistrationConstants.REG_INSTRS_STTS_NES);
        filters.add(stts);
        return filters;
    }

}
