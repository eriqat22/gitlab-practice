package com.progressoft.mpay.mpclear;

public class ClientInfo {
	private long clientTypeId;
	private boolean isBanked;

	public long getClientTypeId() {
		return clientTypeId;
	}
	public void setClientTypeId(long clientTypeId) {
		this.clientTypeId = clientTypeId;
	}
	public boolean isBanked() {
		return isBanked;
	}
	public void setBanked(boolean isBanked) {
		this.isBanked = isBanked;
	}
}
