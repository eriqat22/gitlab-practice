package com.progressoft.mpay.plugins;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.ICryptographer;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_ServiceIntegMessage;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entities.MPAY_TransactionConfig;
import com.progressoft.mpay.notifications.INotificationHelper;

public class ProcessingContext {
	private MPAY_TransactionConfig transactionConfig;
	private MPAY_EndPointOperation operation;
	private MPAY_Transaction transaction;
	private MPAY_MPayMessage message;
	private ProcessingContextSide sender;
	private ProcessingContextSide receiver;
	private BigDecimal amount;
	private long direction;
	private MPAY_ServiceIntegMessage serviceIntegMessage;
	private MPAY_ServiceIntegMessage originalServiceIntegMessage;
	private String transactionNature;
	private IDataProvider dataProvider;
	private ILookupsLoader lookupsLoader;
	private ISystemParameters systemParameters;
	private HashMap<String, Object> extraData;
	private ICryptographer cryptographer;
	private INotificationHelper notificationHelper;
	private boolean isResetPassword;
	private String senderNotificationToken;
	private String receiverNotificationToken;
	private List<String> receiverDeviceTokens;
	private List<String> senderDeviceTokens;

	public ProcessingContext(IDataProvider dataProvider, ILookupsLoader lookupsLoader,
			ISystemParameters systemParameters, ICryptographer cryptographer, INotificationHelper notificationHelper) {
		if (dataProvider == null)
			throw new NullArgumentException("dataProvider");
		if (lookupsLoader == null)
			throw new NullArgumentException("lookupsLoader");
		if (systemParameters == null)
			throw new NullArgumentException("systemParameters");
		if (cryptographer == null)
			throw new NullArgumentException("cryptographer");
		if (notificationHelper == null)
			throw new NullArgumentException("notificationHelper");

		this.dataProvider = dataProvider;
		this.lookupsLoader = lookupsLoader;
		this.systemParameters = systemParameters;
		this.cryptographer = cryptographer;
		this.extraData = new HashMap<>();
		this.amount = BigDecimal.ZERO;
		this.isResetPassword = false;
		this.setNotificationHelper(notificationHelper);
	}

	public Map<String, Object> getExtraData() {
		return extraData;
	}

	public void setExtraData(Map<String, Object> extraData) {
		this.extraData = (HashMap<String, Object>) extraData;
	}

	public MPAY_TransactionConfig getTransactionConfig() {
		return transactionConfig;
	}

	public void setTransactionConfig(MPAY_TransactionConfig transactionConfig) {
		this.transactionConfig = transactionConfig;
	}

	public MPAY_EndPointOperation getOperation() {
		return operation;
	}

	public void setOperation(MPAY_EndPointOperation operation) {
		this.operation = operation;
	}

	public MPAY_Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(MPAY_Transaction transaction) {
		this.transaction = transaction;
	}

	public MPAY_MPayMessage getMessage() {
		return message;
	}

	public void setMessage(MPAY_MPayMessage message) {
		this.message = message;
	}

	public ProcessingContextSide getSender() {
		return sender;
	}

	public void setSender(ProcessingContextSide sender) {
		this.sender = sender;
	}

	public ProcessingContextSide getReceiver() {
		return receiver;
	}

	public void setReceiver(ProcessingContextSide receiver) {
		this.receiver = receiver;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public long getDirection() {
		return direction;
	}

	public void setDirection(long direction) {
		this.direction = direction;
	}

	public MPAY_ServiceIntegMessage getServiceIntegMessage() {
		return serviceIntegMessage;
	}

	public void setServiceIntegMessage(MPAY_ServiceIntegMessage serviceIntegMessage) {
		this.serviceIntegMessage = serviceIntegMessage;
	}

	public MPAY_ServiceIntegMessage getOriginalServiceIntegMessage() {
		return originalServiceIntegMessage;
	}

	public void setOriginalServiceIntegMessage(MPAY_ServiceIntegMessage originalServiceIntegMessage) {
		this.originalServiceIntegMessage = originalServiceIntegMessage;
	}

	public String getTransactionNature() {
		return transactionNature;
	}

	public void setTransactionNature(String transactionNature) {
		this.transactionNature = transactionNature;
	}

	public IDataProvider getDataProvider() {
		return this.dataProvider;
	}

	public ILookupsLoader getLookupsLoader() {
		return this.lookupsLoader;
	}

	public ISystemParameters getSystemParameters() {
		return this.systemParameters;
	}

	public ICryptographer getCryptographer() {
		return cryptographer;
	}

	public void setCryptographer(ICryptographer cryptographer) {
		this.cryptographer = cryptographer;
	}

	public INotificationHelper getNotificationHelper() {
		return notificationHelper;
	}

	public void setNotificationHelper(INotificationHelper notificationHelper) {
		this.notificationHelper = notificationHelper;
	}

	public boolean isResetPassword() {
		return isResetPassword;
	}

	public void setResetPassword(boolean isResetPassword) {
		this.isResetPassword = isResetPassword;
	}

	public String getSenderNotificationToken() {
		return senderNotificationToken;
	}

	public void setSenderNotificationToken(String senderNotificationToken) {
		this.senderNotificationToken = senderNotificationToken;
	}

	public String getReceiverNotificationToken() {
		return receiverNotificationToken;
	}

	public void setReceiverNotificationToken(String receiverNotificationToken) {
		this.receiverNotificationToken = receiverNotificationToken;
	}

	public List<String> getReceiverDeviceTokens() {
		return receiverDeviceTokens;
	}

	public void setReceiverDeviceTokens(List<String> receiverDeviceTokens) {
		this.receiverDeviceTokens = receiverDeviceTokens;
	}

	public List<String> getSenderDeviceTokens() {
		return senderDeviceTokens;
	}

	public void setSenderDeviceTokens(List<String> senderDeviceTokens) {
		this.senderDeviceTokens = senderDeviceTokens;
	}
}
