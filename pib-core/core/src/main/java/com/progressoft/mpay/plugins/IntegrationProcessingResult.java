package com.progressoft.mpay.plugins;


import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Transaction;


public class IntegrationProcessingResult {
    private MPAY_MPayMessage mpayMessage;
    private MPAY_Transaction transaction;
    private MPAY_MpClearIntegMsgLog mpClearMessage;
    private MPAY_MpClearIntegMsgLog originalMPClearMessage;
    private MPAY_MpClearIntegMsgLog mpClearOutMessage;
    private MPAY_Reason reason;
    private MPAY_ProcessingStatus processingStatus;
    private String reasonDescription;
    private List<MPAY_Notification> notifications;
    private boolean isRequest;

    public IntegrationProcessingResult() {
        notifications = new ArrayList<>();
    }

    public static IntegrationProcessingResult create(IntegrationProcessingContext context, String processingStatus, String reasonCode, String reasonDescription,
                                                     MPAY_MpClearIntegMsgLog mpClearResponse, boolean isRequest) {
        IntegrationProcessingResult result = new IntegrationProcessingResult();
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);

        if (context.getMessage() != null) {
            context.getMessage().setProcessingStatus(status);
            context.getMessage().setReason(reason);
            context.getMessage().setReasonDesc(reasonDescription);
        }
        if (context.getTransaction() != null) {
            context.getTransaction().setProcessingStatus(status);
            context.getTransaction().setReason(reason);
            context.getTransaction().setReasonDesc(reasonDescription);
        }
        context.getMpClearMessage().setRefMessage(context.getMessage());
        result.setMpayMessage(context.getMessage());
        result.setTransaction(context.getTransaction());
        result.setMpClearMessage(context.getMpClearMessage());
        result.setOriginalMPClearMessage(context.getOriginalMPClearMessage());
        result.setProcessingStatus(status);
        result.setReason(reason);
        result.setReasonDescription(reasonDescription);
        result.setMpClearOutMessage(mpClearResponse);
        result.setRequest(isRequest);
        return result;
    }

    public MPAY_MPayMessage getMpayMessage() {
        return mpayMessage;
    }

    public void setMpayMessage(MPAY_MPayMessage mpayMessage) {
        this.mpayMessage = mpayMessage;
    }

    public MPAY_Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(MPAY_Transaction transaction) {
        this.transaction = transaction;
    }

    public MPAY_MpClearIntegMsgLog getMpClearMessage() {
        return mpClearMessage;
    }

    public void setMpClearMessage(MPAY_MpClearIntegMsgLog mpClearMessage) {
        this.mpClearMessage = mpClearMessage;
    }

    public MPAY_MpClearIntegMsgLog getOriginalMPClearMessage() {
        return originalMPClearMessage;
    }

    public void setOriginalMPClearMessage(MPAY_MpClearIntegMsgLog originalMPClearMessage) {
        this.originalMPClearMessage = originalMPClearMessage;
    }

    public MPAY_MpClearIntegMsgLog getMpClearOutMessage() {
        return mpClearOutMessage;
    }

    public void setMpClearOutMessage(MPAY_MpClearIntegMsgLog mpClearOutMessage) {
        this.mpClearOutMessage = mpClearOutMessage;
    }

    public MPAY_Reason getReason() {
        return reason;
    }

    public void setReason(MPAY_Reason reason) {
        this.reason = reason;
    }

    public MPAY_ProcessingStatus getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(MPAY_ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public List<MPAY_Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<MPAY_Notification> notifications) {
        this.notifications = notifications;
    }

    public boolean isRequest() {
        return isRequest;
    }

    public void setRequest(boolean isRequest) {
        this.isRequest = isRequest;
    }
}
