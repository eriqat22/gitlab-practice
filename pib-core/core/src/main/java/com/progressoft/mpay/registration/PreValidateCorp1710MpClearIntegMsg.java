package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.Result;

public class PreValidateCorp1710MpClearIntegMsg implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PreValidate1710MpClearIntegMsg.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Map transientVars, Map arg1, PropertySet ps) throws WorkflowException {

		logger.debug("inside PreValidateCorp1710MpClearIntegMsg function .");
		transientVars.put(Result.VALIDATION_RESULT, Result.SUCCESSFUL);
	}
}
