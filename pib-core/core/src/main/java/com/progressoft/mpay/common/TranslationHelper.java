package com.progressoft.mpay.common;

import com.progressoft.jfw.model.service.utils.AppContext;

public class TranslationHelper {
	private TranslationHelper() {

	}

	public static String getTranslation(String key) {
		return AppContext.getMsg(key);
	}
}
