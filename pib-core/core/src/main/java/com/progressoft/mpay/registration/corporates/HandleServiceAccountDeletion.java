package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class HandleServiceAccountDeletion implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(HandleServiceAccountDeletion.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute ====================");

		MPAY_ServiceAccount account = (MPAY_ServiceAccount) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (account.getIsRegistered())
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.DELETE_APPROVAL, new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.DELETE, new WorkflowException());
	}
}