package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.registration.corporates.CorporateServiceWorkflowStatuses;
import com.progressoft.mpay.registration.corporates.CorporateWorkflowStatuses;
import com.progressoft.mpay.registration.customers.CheckIfLastMobileAccount;
import com.progressoft.mpay.registration.customers.CustomerMobileWorkflowStatuses;
import com.progressoft.mpay.registration.customers.CustomerWorkflowStatuses;

public class EnableSendToApproval implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfLastMobileAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object sender = transientVar.get(WorkflowService.WF_ARG_BEAN);

		if (sender instanceof MPAY_MobileAccount)
			return checkMobileAccount((MPAY_MobileAccount) sender);
		else if (sender instanceof MPAY_CustomerMobile)
			return checkMobile((MPAY_CustomerMobile) sender);
		if (sender instanceof MPAY_ServiceAccount)
			return checkServiceAccount((MPAY_ServiceAccount) sender);
		else if (sender instanceof MPAY_CorpoarteService)
			return checkService((MPAY_CorpoarteService) sender);

		return false;
	}

	private boolean checkService(MPAY_CorpoarteService service) {
		logger.debug("inside checkMobile ------------");
		if (checkCorporateModified(service.getRefCorporate()))
			return false;
		return true;
	}

	private boolean checkServiceAccount(MPAY_ServiceAccount account) {
		logger.debug("inside checkServiceAccount ------------");
		if (checkServiceModified(account.getService()))
			return false;
		if (checkCorporateModified(account.getService().getRefCorporate()))
			return false;

		return true;
	}

	private boolean checkCorporateModified(MPAY_Corporate corporate) {
		logger.debug("inside checkCorporateModified ------------");
		return !corporate.getStatusId().getCode().equals(CorporateWorkflowStatuses.APPROVED);
	}

	private boolean checkMobileAccount(MPAY_MobileAccount account) {
		logger.debug("inside checkMobileAccount ------------");
		if (checkMobileModified(account.getMobile()))
			return false;
		if (checkCustomerModified(account.getMobile().getRefCustomer()))
			return false;

		return true;
	}

	private boolean checkServiceModified(MPAY_CorpoarteService service) {
		logger.debug("inside checkServiceModified ------------");
		return !service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.APPROVED.toString());
	}

	private boolean checkMobileModified(MPAY_CustomerMobile mobile) {
		logger.debug("inside checkMobileModified ------------");
		return !mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.APPROVED);
	}

	private boolean checkCustomerModified(MPAY_Customer customer) {
		logger.debug("inside checkCustomerModified ------------");
		return !customer.getStatusId().getCode().equals(CustomerWorkflowStatuses.APPROVED);
	}

	private boolean checkMobile(MPAY_CustomerMobile mobile) {
		logger.debug("inside checkMobile ------------");
		if (checkCustomerModified(mobile.getRefCustomer()))
			return false;
		return true;
	}
}