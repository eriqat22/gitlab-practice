package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;

public class ServiceValidator {
	private static final Logger logger = LoggerFactory.getLogger(ServiceValidator.class);

	private ServiceValidator() {

	}

	public static ValidationResult validate(MPAY_CorpoarteService service, boolean isSender) {
		logger.debug("Inside Validate ...");
		logger.debug("isSender: " + isSender);
		if (service == null || !service.getIsRegistered()) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_SERVICE_NOT_REGISTERED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_SERVICE_NOT_REGISTERED, null, false);
		}
		if (!service.getIsActive()) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_SERVICE_NOT_ACTIVE, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_SERVICE_NOT_ACTIVE, null, false);
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
