package com.progressoft.mpay;

import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.mpay.common.DateUtils;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AmountType;
import org.apache.commons.codec.binary.Base64;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ch.lambdaj.Lambda.*;

public class SystemHelper {

	private static final String ZERO_LEFT_PADDING = "0";
	private static final Logger LOG = LoggerFactory.getLogger(SystemHelper.class);

	private SystemHelper() {
	}

	public static Timestamp getSystemTimestamp() {
		return new Timestamp(Calendar.getInstance().getTimeInMillis());
	}

	public static Calendar getCalendar() {
		return Calendar.getInstance();
	}

	public static String formatDate(Date date, String format) {
		if (null == date)
			return null;
		SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
		return dateFormatter.format(date);
	}

	public static String getIsoDate(Date date) {
		if (null == date)
			return null;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ");
		return dateFormatter.format(date);
	}

	static boolean toPremitiveBoolean(Boolean reference) {
		return reference != null && (boolean) reference;

	}

	private static Date getDateWithOutTime(Date targetDate) {
		Calendar newDate = Calendar.getInstance();
		newDate.setLenient(false);
		newDate.setTime(targetDate);
		newDate.set(Calendar.HOUR_OF_DAY, 0);
		newDate.set(Calendar.MINUTE, 0);
		newDate.set(Calendar.SECOND, 0);
		newDate.set(Calendar.MILLISECOND, 0);

		return newDate.getTime();
	}

	public static java.sql.Date getCurrentDateWithoutTime() {
		return new java.sql.Date(SystemHelper.getDateWithOutTime(SystemHelper.getSystemTimestamp()).getTime());
	}

	public static java.sql.Date getCurrentDateTime() {

		Calendar newDate = Calendar.getInstance();
		return new java.sql.Date(newDate.getTime().getTime());

	}

	static byte[] decodeString(String data, String encoding) throws UnsupportedEncodingException {
		return data.getBytes(encoding);
	}

	static String encodeString(byte[] data, String encoding) throws UnsupportedEncodingException {
		return new String(data, encoding);
	}

	public static byte[] decodeString(ISystemParameters systemParameters, String data)
			throws UnsupportedEncodingException {
		return decodeString(data, systemParameters.getDefaultEncoding());
	}

	static String encodeBase64(byte[] binaryData) {
		return Base64.encodeBase64String(binaryData);
	}

	static String formatHex(String hex) {
		StringBuilder result = new StringBuilder();
		char[] hexChars = hex.toCharArray();

		for (int i = 0; i < hexChars.length; i++) {
			result.append(hexChars[i]);
			if (i % 2 != 0) {
				result.append(" ");
			}
		}

		return result.toString().trim();
	}

	static byte[] decodeBase64(String data) {
		return Base64.decodeBase64(data);
	}

	public static boolean isNullOrEmpty(String data) {
		return data == null || "".equals(data);
	}

	public static Date parseDate(String date, String format) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(date);
	}

	public static String formatISOAmount(ISystemParameters systemParameters, BigDecimal value, int type) {
		String stringVal = null;
		if (value != null) {
			String format = (type == AmountType.AMOUNT) ? systemParameters.getAmountFormat()
					: systemParameters.getChargesAmountFormat();
			stringVal = new DecimalFormat(format).format(value).replace(".", "");
		}
		return stringVal;
	}

	public static String formatMobileNumber(ISystemParameters systemParameters, String mobileNum) {
		String countryCode = systemParameters.getCountryCode();
		String internationalNumber = "00" + countryCode;
		if (mobileNum.startsWith(internationalNumber)) {
			return mobileNum;
		} else if (mobileNum.startsWith("+" + countryCode))
			return "00" + mobileNum.substring(1);
		else if (mobileNum.startsWith(countryCode))
			return "00" + mobileNum;
		else if (!mobileNum.startsWith(ZERO_LEFT_PADDING))
			return internationalNumber + mobileNum;
		return internationalNumber + mobileNum.substring(1);
	}

	public static boolean validateMobielNumber(ILookupsLoader lookupsLoader, String mobileNumber) {
		String mobilePattern = lookupsLoader.getSystemConfigurations("Mobile Number Regex").getConfigValue();
		Pattern pattern = Pattern.compile(mobilePattern);
		Matcher matcher = pattern.matcher(mobileNumber);

		return matcher.matches();
	}

	public static XMLGregorianCalendar generateXmlDate() {
		return DateUtils.toXMLGregorianDate(getCurrentDateTime());
	}

	public static XMLGregorianCalendar generateXmlDateFromDate(Date date) {
		return DateUtils.toXMLGregorianDate(date);
	}
	public static XMLGregorianCalendar getGregorianCalendar() {
		GregorianCalendar gcal = new GregorianCalendar();
		XMLGregorianCalendar xmlDateTime = null;
		try {
			xmlDateTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
		} catch (DatatypeConfigurationException e) {
			LOG.error("Error... change me later", e);
		}
		return xmlDateTime;
	}

	public static boolean isDeleted(JFWEntity entity) {
		return entity.getDeletedFlag() != null && entity.getDeletedFlag();
	}

	public static String formatAmount(ISystemParameters systemParameters, BigDecimal amount) {
		StringBuilder amountFormat;
		if (amount.equals(BigDecimal.ZERO))
			amountFormat = new StringBuilder("0.");
		else
			amountFormat = new StringBuilder("##.");
		int decimalPlaces = systemParameters.getDecimalCount();

		for (int i = 0; i < decimalPlaces; i++)
			amountFormat.append(ZERO_LEFT_PADDING);

		DecimalFormat format = new DecimalFormat(amountFormat.toString());

		String finalAmount = format.format(amount);
		if (amount.doubleValue() < 1)
			finalAmount = ZERO_LEFT_PADDING + finalAmount;
		return finalAmount;
	}

	private static MPAY_MobileAccount getDefaultMobileAccount(MPAY_CustomerMobile mobile) {
		return selectFirst(mobile.getMobileMobileAccounts(),
				having(on(MPAY_MobileAccount.class).getIsDefault(), Matchers.equalTo(true)));
	}

	public static MPAY_MobileAccount getMobileAccount(ISystemParameters systemParameters, MPAY_CustomerMobile mobile,
			String registrationId) {

		if (registrationId == null)
			return getDefaultMobileAccount(mobile);
		if(registrationId.startsWith("{value="))
		registrationId = registrationId.substring(registrationId.indexOf("{value=")+7,registrationId.indexOf(","));

		String routeCode = registrationId.substring(0, systemParameters.getRoutingCodeLength());
		int mas;
		try {
			mas = Integer.parseInt(registrationId.substring(routeCode.length(),
					routeCode.length() + systemParameters.getMobileAccountSelectorLength()));
		} catch (Exception ex) {
			LOG.debug("getMobileAccount--> ", ex);
			return null;
		}
		List<MPAY_MobileAccount> accounts = mobile.getMobileMobileAccounts();
		for (MPAY_MobileAccount account : accounts) {
			if (account.getBank().getCode().equals(routeCode) && account.getMas() == mas)
				return account;
		}
		return null;
	}

	public static MPAY_Account getWalletAccount(ISystemParameters systemParameters, MPAY_CustomerMobile mobile,
			String registrationId) {
		MPAY_MobileAccount account = getMobileAccount(systemParameters, mobile, registrationId);
		if (account == null)
			return null;
		return account.getRefAccount();
	}

	public static MPAY_Account getDefaultWalletAccount(MPAY_CustomerMobile mobile) {
		MPAY_MobileAccount account = getDefaultMobileAccount(mobile);
		if (account == null)
			return null;
		return account.getRefAccount();
	}

	public static MPAY_ServiceAccount getServiceAccount(ISystemParameters systemParameters,
			MPAY_CorpoarteService service, String registrationId) {
		if (registrationId == null)
			return getDefaultServiceAccount(service);
		if(registrationId.startsWith("{value="))
			registrationId = registrationId.substring(registrationId.indexOf("{value=")+7,registrationId.indexOf(","));
		String routeCode = registrationId.substring(0, systemParameters.getRoutingCodeLength());
		int mas;
		try {
			mas = Integer.parseInt(registrationId.substring(routeCode.length(),
					routeCode.length() + systemParameters.getMobileAccountSelectorLength()));
		} catch (Exception ex) {
			LOG.debug("getServiceAccount--> ", ex);
			return null;
		}
		List<MPAY_ServiceAccount> accounts = service.getServiceServiceAccounts();
		for (MPAY_ServiceAccount account : accounts) {
			if ((account.getBank().getCode().equals(routeCode)) && (account.getMas() == mas))
				return account;
		}
		return null;
	}

	public static MPAY_Account getDefaultWalletAccount(MPAY_CorpoarteService service) {
		MPAY_ServiceAccount account = getDefaultServiceAccount(service);
		if (account == null)
			return null;
		return account.getRefAccount();
	}
	
	public static String maskMobileNumber(ISystemParameters systemParameters, String mobileNumber) {
		if (!systemParameters.getMaskMobileNumberInNotification())
			return mobileNumber;
		int lastIndex = mobileNumber.length() - 3;
		int startIndex = lastIndex - 4;
		return mobileNumber.substring(0, startIndex) + "****"
				+ mobileNumber.substring(lastIndex, mobileNumber.length());
	}

	private static MPAY_ServiceAccount getDefaultServiceAccount(MPAY_CorpoarteService service) {
		return selectFirst(service.getServiceServiceAccounts(),
				having(on(MPAY_ServiceAccount.class).getIsDefault(), Matchers.equalTo(true)));
	}
}