package com.progressoft.mpay.registration.customers;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.bussinessobject.core.IJFWEntity;
import com.progressoft.mpay.entity.BeneficiaryFlag;

public class BeneficiaryChangeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(BeneficiaryChangeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside BeneficiaryChangeHandler ------------");

		IJFWEntity entity = changeHandlerContext.getEntity();
		String value;
		try {
			value = (String) PropertyUtils.getProperty(entity, "isBeneficiary");
			if (value == null)
				return;

			if (BeneficiaryFlag.Beneficiary.equals(value)) {
				changeHandlerContext.getFieldsAttributes().get("beneficiaryName").setRequired(false);
				changeHandlerContext.getFieldsAttributes().get("beneficiaryId").setRequired(false);
				changeHandlerContext.getFieldsAttributes().get("beneficiaryIdType").setRequired(false);
				changeHandlerContext.getFieldsAttributes().get("beneficiaryEmail").setRequired(false);
				changeHandlerContext.getFieldsAttributes().get("guardian").setRequired(false);
				changeHandlerContext.getFieldsAttributes().get("guardianEmail").setRequired(false);
			}
			else if (BeneficiaryFlag.NonBeneficiary.equals(value))
			{
				changeHandlerContext.getFieldsAttributes().get("beneficiaryName").setRequired(true);
				changeHandlerContext.getFieldsAttributes().get("beneficiaryId").setRequired(true);
				changeHandlerContext.getFieldsAttributes().get("beneficiaryIdType").setRequired(true);
				changeHandlerContext.getFieldsAttributes().get("beneficiaryEmail").setRequired(true);
				changeHandlerContext.getFieldsAttributes().get("guardian").setRequired(true);
				changeHandlerContext.getFieldsAttributes().get("guardianEmail").setRequired(true);
			}
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			logger.error("error while changing the radio group values beneficiary or nonbeneficiary", e);
		}
	}
}
