
package com.progressoft.mpay.webclient.security;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class KeyPinStore {
    private static KeyPinStore instance = null;
    private static String cerFileName = "";
    private SSLContext sslContext = SSLContext.getInstance("TLS");
    private PublicKey publicKey = null;

    private KeyPinStore() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = new BufferedInputStream(new FileInputStream(cerFileName));
        Certificate ca = null;

        try {
            ca = cf.generateCertificate(caInput);
            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
        } catch (Exception var10) {
            var10.printStackTrace();
        } finally {
            caInput.close();
        }

        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);
        this.publicKey = ca.getPublicKey();
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);
        this.sslContext.init(null, tmf.getTrustManagers(), null);
    }

    public static synchronized KeyPinStore getInstance(String fileName) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        if (instance == null || !cerFileName.equals(fileName)) {
            cerFileName = fileName;
            instance = new KeyPinStore();
        }

        return instance;
    }

    public SSLContext getContext() {
        return this.sslContext;
    }

    public PublicKey getPublicKey() {
        return this.publicKey;
    }
}
