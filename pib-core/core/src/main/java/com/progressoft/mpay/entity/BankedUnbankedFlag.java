package com.progressoft.mpay.entity;

public final class BankedUnbankedFlag {
	public static final String UNBANKED = "1";
	public static final String BANKED = "2";

	private BankedUnbankedFlag() {

	}
}
