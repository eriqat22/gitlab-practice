package com.progressoft.mpay.charges;

import java.math.BigDecimal;
import java.util.Map;

import com.progressoft.jfw.shared.UIFieldAttributes;
import com.progressoft.mpay.entities.MPAY_ChargesSlice;
import com.progressoft.mpay.entity.ChargeTypes;

public final class ChargeHandlersHelper {

    private static final String CHARGE_AMOUNT = "chargeAmount";
    private static final String CHARGE_PERCENT = "chargePercent";
    private static final String MIN_AMOUNT = "minAmount";
    private static final String MAX_AMOUNT = "maxAmount";

    private ChargeHandlersHelper() {
    }

    public static void handleChargeType(Map<String, UIFieldAttributes> fieldsAttributes, MPAY_ChargesSlice slice) {
        if (slice.getChargeType() == null || slice.getChargeType().equalsIgnoreCase(ChargeTypes.FIXED)) {
            fieldsAttributes.get(CHARGE_AMOUNT).setEnabled(true);
            fieldsAttributes.get(CHARGE_AMOUNT).setRequired(true);

            fieldsAttributes.get(CHARGE_PERCENT).setEnabled(false);
            fieldsAttributes.get(MIN_AMOUNT).setEnabled(false);
            fieldsAttributes.get(MAX_AMOUNT).setEnabled(false);

            fieldsAttributes.get(CHARGE_PERCENT).setRequired(false);
            fieldsAttributes.get(MIN_AMOUNT).setRequired(false);
            fieldsAttributes.get(MAX_AMOUNT).setRequired(false);

            slice.setChargePercent(BigDecimal.ZERO);
            slice.setMinAmount(null);
            slice.setMaxAmount(null);
        } else {
            fieldsAttributes.get(CHARGE_AMOUNT).setEnabled(false);
            fieldsAttributes.get(CHARGE_AMOUNT).setRequired(false);

            fieldsAttributes.get(CHARGE_PERCENT).setEnabled(true);
            fieldsAttributes.get(MIN_AMOUNT).setEnabled(true);
            fieldsAttributes.get(MAX_AMOUNT).setEnabled(true);

            fieldsAttributes.get(CHARGE_PERCENT).setRequired(true);
            fieldsAttributes.get(MIN_AMOUNT).setRequired(true);
            fieldsAttributes.get(MAX_AMOUNT).setRequired(true);

            slice.setChargeAmount(null);
        }
    }
}