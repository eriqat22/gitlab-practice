package com.progressoft.mpay.registration.customers;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;

public class ValidateMobileAccount implements Validator {
	private static final Logger logger = LoggerFactory.getLogger(ValidateMobileAccount.class);

	@Autowired
	ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("validate =======");

		MPAY_MobileAccount mobileAccount = (MPAY_MobileAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
		if (mobileAccount.getStatusId() != null)
			return;
		validateMaxAccounts(mobileAccount);
		validateMasLength(mobileAccount);
		MPAY_CustomerMobile mobile = mobileAccount.getMobile();
		List<MPAY_MobileAccount> allAccounts = DataProvider.instance().listMobileAccounts(mobile.getId());
		if (allAccounts == null || allAccounts.isEmpty())
			return;
		Iterator<MPAY_MobileAccount> accounts = allAccounts.iterator();
		do {
			MPAY_MobileAccount account = accounts.next();
			if (account.getId().equals(mobileAccount.getId()) )
				continue;
			validateAccount(account, mobileAccount);

		} while (accounts.hasNext());
	}

	private void validateAccount(MPAY_MobileAccount account, MPAY_MobileAccount currentAccount) throws InvalidInputException {
		InvalidInputException iie = new InvalidInputException();
		String storedRegistrationId = account.getBank().getCode() + String.valueOf(account.getMas());
		if (storedRegistrationId.equals(currentAccount.getBank().getCode() + String.valueOf(currentAccount.getMas()))) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_MAS));
			throw iie;
		}
		if (currentAccount.getExternalAcc() != null && currentAccount.getExternalAcc().equals(account.getExternalAcc())) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_EXTERNAL_ACCOUNT));
			throw iie;
		}
		if (currentAccount.getIsDefault() && account.getIsDefault()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_ISDEFUALT));
			throw iie;
		}
	}

	private void validateMaxAccounts(MPAY_MobileAccount mobileAccount) throws InvalidInputException {
		if (!mobileAccount.getMobile().getIsRegistered())
			return;
		List<MPAY_MobileAccount> undeletedMobileAccounts = select(mobileAccount.getMobile().getMobileMobileAccounts(),
				having(on(MPAY_MobileAccount.class).getDeletedFlag(), Matchers.nullValue()).or(having(on(MPAY_MobileAccount.class).getDeletedFlag(), Matchers.equalTo(false))));
		MPAY_ClientType type = LookupsLoader.getInstance().getCustomerClientType();
		if (type.getMaxNumberOfAccount() == 0)
			return;
		if (undeletedMobileAccounts.size() >= type.getMaxNumberOfAccount()) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.ACCOUNT_MAX_REACHED));
			throw iie;
		}
	}

	private void validateMasLength(MPAY_MobileAccount mobileAccount) throws InvalidInputException {
		if (String.valueOf(mobileAccount.getMas()).length() > SystemParameters.getInstance().getMobileAccountSelectorLength()) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg(MPayErrorMessages.INVALID_MAS_LENGTH), SystemParameters.getInstance().getMobileAccountSelectorLength()));
			throw iie;
		}
	}
}