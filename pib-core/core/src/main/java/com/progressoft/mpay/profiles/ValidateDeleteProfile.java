package com.progressoft.mpay.profiles;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_Profile;

public class ValidateDeleteProfile implements Validator {

	@Autowired
	private ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteProfile.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("\n\r===inside ValidateDeleteProfile....");

		InvalidInputException iie = new InvalidInputException();
		MPAY_Profile profile = (MPAY_Profile) arg0.get(WorkflowService.WF_ARG_BEAN);

		MPAY_CustomerMobile oCustomerMobile = itemDao.getItem(MPAY_CustomerMobile.class, "refProfile.id",
				profile.getId());

		if (oCustomerMobile != null) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("profile.invalid.mobileAttached"));
			throw iie;
		}

	}

}
