package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.mpclear.MPClearClientHelper;

public class PostApproveDeleteCustomer implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostApproveDeleteCustomer.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("Inside execute =====");
        MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);

        if (customer.getIsRegistered()) {
        	MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).removeCustomer(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), customer);
        } else
            throw new WorkflowException("customer is not Registered");

    }
}