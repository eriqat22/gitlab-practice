package com.progressoft.mpay.registration.corporates.services;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class PostCorporateServiceChangeAlias implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCorporateServiceChangeAlias.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_CorpoarteService originalService = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN_ORIGINAL);
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN);
		if (!service.getIsRegistered())
			return;
		service.setNewAlias(service.getAlias());
		service.setAlias(originalService.getAlias());
		DataProvider.instance().mergeCorporateService(service);
	}
}