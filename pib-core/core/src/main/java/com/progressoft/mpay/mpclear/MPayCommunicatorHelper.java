package com.progressoft.mpay.mpclear;

import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.jfw.model.service.utils.AppContext;

public class MPayCommunicatorHelper {

	private static Communicator communicator;

	private MPayCommunicatorHelper() {

	}

	public static Communicator getCommunicator() {
		if (communicator == null) {
			communicator = AppContext.getApplicationContext().getBean("jfwCommunicator", Communicator.class);
		}
		return communicator;
	}
}
