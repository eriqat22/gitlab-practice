package com.progressoft.mpay.networkmanagement;

import java.sql.Timestamp;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;

public class CalculateLoginExpirationTime implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(CalculateLoginExpirationTime.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside CalculateLoginExpirationTime ====");
		MPAY_NetworkManagement oNetworkManagement = (MPAY_NetworkManagement) arg0.get(WorkflowService.WF_ARG_BEAN);

		int loginValidtyHours = SystemParameters.getInstance().getMpClearLoginValidtyHours();
		oNetworkManagement.setLoginExpiration(new Timestamp(SystemHelper.getCalendar().getTimeInMillis()
				+ (1000 * 60 * 60 * loginValidtyHours)));

	}
}
