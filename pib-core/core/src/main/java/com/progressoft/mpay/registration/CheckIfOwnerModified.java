package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class CheckIfOwnerModified implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfOwnerModified.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object entity = transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (entity instanceof MPAY_CustomerMobile)
			return checkCustomer((MPAY_CustomerMobile) entity);
		else if (entity instanceof MPAY_MobileAccount)
			return checkMobile((MPAY_MobileAccount) entity);
		else if (entity instanceof MPAY_CorpoarteService)
			return checkCorporate((MPAY_CorpoarteService) entity);
		else if (entity instanceof MPAY_ServiceAccount)
			return checkService((MPAY_ServiceAccount) entity);
		else
			throw new WorkflowException("Invalid type received " + entity.getClass().getName());
	}

	private boolean checkService(MPAY_ServiceAccount account) {
		return account.getApprovedData() != null && account.getService().getApprovedData() == null;
	}

	private boolean checkCorporate(MPAY_CorpoarteService service) {
		return service.getApprovedData() != null && service.getRefCorporate().getApprovedData() == null;
	}

	private boolean checkMobile(MPAY_MobileAccount account) {
		return account.getApprovedData() != null && account.getMobile().getApprovedData() == null;
	}

	private boolean checkCustomer(MPAY_CustomerMobile mobile) {
		return mobile.getApprovedData() != null && mobile.getRefCustomer().getApprovedData() == null;
	}
}