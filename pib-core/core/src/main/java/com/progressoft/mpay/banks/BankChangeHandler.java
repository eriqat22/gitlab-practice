package com.progressoft.mpay.banks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entity.BankType;

public class BankChangeHandler implements ChangeHandler {

    private static final String TELLER_CODE = "tellerCode";
    private static final String PROCESSOR = "processor";
    private static final Logger logger = LoggerFactory.getLogger(BankChangeHandler.class);

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("***inside BankChangeHandler : " + changeHandlerContext.getSource());
        MPAY_Bank bank = (MPAY_Bank) changeHandlerContext.getEntity();

        if (bank.getSettlementParticipant() == null || bank.getSettlementParticipant().equals(BankType.SETTLEMENT)) {
            changeHandlerContext.getFieldsAttributes().get(TELLER_CODE).setVisible(true);
            changeHandlerContext.getFieldsAttributes().get(TELLER_CODE).setRequired(true);
            changeHandlerContext.getFieldsAttributes().get(PROCESSOR).setVisible(false);
            changeHandlerContext.getFieldsAttributes().get(PROCESSOR).setRequired(false);
        } else {
            bank.setTellerCode(null);
            changeHandlerContext.getFieldsAttributes().get(TELLER_CODE).setVisible(false);
            changeHandlerContext.getFieldsAttributes().get(TELLER_CODE).setRequired(false);
            changeHandlerContext.getFieldsAttributes().get(PROCESSOR).setVisible(true);
            changeHandlerContext.getFieldsAttributes().get(PROCESSOR).setRequired(true);
        }



    }
}
