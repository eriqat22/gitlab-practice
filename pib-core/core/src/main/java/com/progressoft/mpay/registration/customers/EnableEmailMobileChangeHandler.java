package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.workflow.WfChangeHandler;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import org.springframework.stereotype.Component;

@Component
public class EnableEmailMobileChangeHandler extends WfChangeHandler<MPAY_CustomerMobile> {

    @Override
    public void handle(WfChangeHandlerContext<MPAY_CustomerMobile> wfChangeHandlerContext) {
        wfChangeHandlerContext.field(MPAY_CustomerMobile.EMAIL)
                .setRequired(wfChangeHandlerContext.getEntity().getEnableEmail());
    }
}
