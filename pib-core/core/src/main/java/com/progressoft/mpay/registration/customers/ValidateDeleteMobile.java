package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.registration.ValidateAccountDeletion;

public class ValidateDeleteMobile implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteMobile.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside validate =======================");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) arg0.get(WorkflowService.WF_ARG_BEAN);
		ValidateAccountDeletion.validate(mobile);
	}
}