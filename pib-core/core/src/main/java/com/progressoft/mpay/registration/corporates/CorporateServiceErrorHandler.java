package com.progressoft.mpay.registration.corporates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.ErrorHandler;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.util.simpleview.SimpleView;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class CorporateServiceErrorHandler extends ErrorHandler {

	@Autowired
	private ItemDao itemDao;

	@Override
	public InvalidInputException handleError(Exception ex, SimpleView simpleView, Object entity, int actionKey) {
		InvalidInputException cause = null;

		if (ex instanceof DataIntegrityViolationException || ex instanceof TransactionSystemException
				|| ex.getCause() instanceof DataIntegrityViolationException
				|| ex.getCause() instanceof TransactionSystemException) {

			cause = Unique.validate(itemDao, (MPAY_CorpoarteService) entity, "name",
					((MPAY_CorpoarteService) entity).getName(), "duplicat.corporate.service.item.name");

			if (cause == null) {
				cause = Unique.validate(itemDao, (MPAY_CorpoarteService) entity, "alias",
						((MPAY_CorpoarteService) entity).getAlias(), "duplicat.corporate.service.item.alias");

			}

		}

		if (simpleView != null && cause == null) {
			cause = new InvalidInputException();
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS,
					AppContext.getMsg("duplicat.corporate.service.item.name"));
			return cause;
		}

		return cause;
	}
}
