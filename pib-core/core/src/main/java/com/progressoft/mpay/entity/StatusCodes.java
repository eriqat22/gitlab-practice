package com.progressoft.mpay.entity;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;

public enum StatusCodes {
	REJECTED("999990"), ACCEPTED("999991"), PENDING_REPLY_ONLINE("999992"), PENDING_REPLY_OFFLINE("999993"), MOBILE_NEW(
			"100101"), PROCESSED("100705"), REPLIED("103104"), NETWORK_MANAGEMENT_PENDING_REPLY("102802"), ORIGINAL_MESSAGE_PENDING_REPLY(
			"102903"), LOGGED_IN("102803"), LOGGED_OUT("102802"), SUSPENDED("100122"), SERVICE_SUSPENDED("1070122");

	private String value;

	StatusCodes(String value) {
		this.value = value;
	}

	public boolean compare(WorkflowStatus status) {
		return status == null ? false : status.getCode().equals(this.value);
	}

	@Override
	public String toString() {
		return value;
	}
}
