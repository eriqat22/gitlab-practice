package com.progressoft.mpay.plugins.validators;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ClientsOTP;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.PluginsCommonConstants;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;

public class PinCodeValidator {
    private static final Logger logger = LoggerFactory.getLogger(PinCodeValidator.class);

    private PinCodeValidator() {

    }

    public static ValidationResult validate(ProcessingContext context, MPAY_CustomerMobile mobile, String pinCode) {
        logger.debug("Inside Validate ....");
        if (context == null)
            throw new NullArgumentException("context");
        if (mobile == null || context.getOperation() == null || context.getOperation().getMessageType() == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);

        ValidationResult result = validatePinless(context);
        if (result != null)
            return result;

        if (StringUtils.isEmpty(pinCode)) {
            handleInvalidCustomerPin(context, mobile, false);
            return new ValidationResult(ReasonCodes.INVALID_PIN_CODE, null, false);
        }

        if (context.getOperation().getMessageType().getIsOTPEnabled())
            result = validateOTP(context, mobile);
        else
            result = validatePinCode(mobile, pinCode, context);

        handleInvalidCustomerPin(context, mobile, result.isValid());
        return result;
    }

    private static ValidationResult validatePinless(ProcessingContext context) {
        if (!context.getOperation().getMessageType().getIsFinancial())
            return null;
        BigDecimal amount = context.getAmount();
        BigDecimal pinLessAmount;
        if (TransactionTypeCodes.DIRECT_CREDIT.equalsIgnoreCase(context.getTransactionNature()))
            pinLessAmount = context.getSender().getProfile().getLimitsScheme().getPinlessAmount();
        else if (TransactionTypeCodes.DIRECT_DEBIT.equalsIgnoreCase(context.getTransactionNature()))
            pinLessAmount = context.getReceiver().getProfile().getLimitsScheme().getPinlessAmount();
        else
            throw new NotSupportedException("context.getTransactionNature(): " + context.getTransactionNature() + " Not supported.");


        if (context.getDirection() == TransactionDirection.INWARD) {
            if (context.getTransactionConfig() != null && context.getTransactionConfig().getMessageType().getIsPinlessEnabled() && pinLessAmount.compareTo(BigDecimal.ZERO) > 0 && pinLessAmount.compareTo(amount) > 0)
                return new ValidationResult(ReasonCodes.VALID, null, true);
        } else if (context.getMessage().getMessageType().getIsPinlessEnabled() && pinLessAmount.compareTo(BigDecimal.ZERO) > 0 && pinLessAmount.compareTo(amount) > 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        return null;
    }

    private static ValidationResult validatePinCode(MPAY_CustomerMobile mobile, String pinCode, ProcessingContext context) {
        if (!pinCode.equals(mobile.getPin()))
            return new ValidationResult(ReasonCodes.INVALID_PIN_CODE, null, false);
        if (!context.isResetPassword() && SystemHelper.getSystemTimestamp().after(calculatePinExpiredDate(context, mobile.getPinLastChanged())))
            return new ValidationResult(ReasonCodes.PIN_EXPIRED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateOTP(ProcessingContext context, MPAY_CustomerMobile mobile) {
        return OTPValidator.validate(context.getSystemParameters(), (MPAY_ClientsOTP) context.getExtraData().get(PluginsCommonConstants.OTP_KEY), mobile, context.getAmount(), context.getOperation().getMessageType());
    }

    public static ValidationResult validate(ProcessingContext context, MPAY_CorpoarteService service, String pinCode) {
        logger.debug("Inside Validate ...");
        if (context == null)
            throw new NullArgumentException("context");
        if (service == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);

        if (service.getIsBlocked())
            return new ValidationResult(ReasonCodes.SENDER_MOBILE_BLOCKED, null, false);
        if (pinCode == null || pinCode.isEmpty()) {
            handleInvalidCorporatePin(context, service, false);
            return new ValidationResult(ReasonCodes.INVALID_PIN_CODE, null, false);
        }
        if (service.getPin() == null || !service.getPin().equals(pinCode)) {
            handleInvalidCorporatePin(context, service, false);
            return new ValidationResult(ReasonCodes.INVALID_PIN_CODE, null, false);
        }

        if (SystemHelper.getCurrentDateTime().after(calculatePinExpiredDate(context, service.getPinLastChanged())))
            return new ValidationResult(ReasonCodes.PIN_EXPIRED, null, false);
        handleInvalidCorporatePin(context, service, true);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static void handleInvalidCustomerPin(ProcessingContext context, MPAY_CustomerMobile mobile,
                                                 boolean isValid) {
        logger.debug("Inside HandleInvalidPin ...");
        logger.debug("isValid: ", isValid);
        if (isValid)
            mobile.setRetryCount(0L);
        else {
            mobile.setRetryCount(mobile.getRetryCount() + 1);
            if (mobile.getRetryCount() >= context.getSystemParameters().getPinCodeMaxRetryCount())
                mobile.setIsBlocked(true);
        }
        context.getDataProvider().mergeCustomerMobile(mobile);
    }

    private static void handleInvalidCorporatePin(ProcessingContext context, MPAY_CorpoarteService service,
                                                  boolean isValid) {
        logger.debug("Inside HandleInvalidPin ...");
        logger.debug("isValid: " + isValid);
        if (isValid)
            service.setRetryCount(0L);
        else {
            service.setRetryCount(service.getRetryCount() + 1);
            if (service.getRetryCount() >= context.getSystemParameters().getPinCodeMaxRetryCount())
                service.setIsBlocked(true);
        }
        context.getDataProvider().mergeCorporateService(service);
    }

    private static Timestamp calculatePinExpiredDate(ProcessingContext context, Timestamp timestamp) {
        logger.debug("Inside calculatePinExpiredDate ...");
        String pinExpiryTimeInHours = context.getSystemParameters().getPinExpiryTimeInHours();
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(timestamp.getTime());
        instance.set(Calendar.HOUR, Integer.parseInt(pinExpiryTimeInHours) + instance.get(Calendar.HOUR));
        return new Timestamp(instance.getTimeInMillis());
    }
}
