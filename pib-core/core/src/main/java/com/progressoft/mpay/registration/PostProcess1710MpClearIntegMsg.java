package com.progressoft.mpay.registration;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.nio.charset.Charset;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.NotImplementedException;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.SmsNotificationType;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CustIntegMessage;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_MpClearResponseCode;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entity.CustomerManagementActionTypes;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.registration.customers.CustomerMobileWorkflowServiceActions;
import com.progressoft.mpay.registration.customers.CustomerMobileWorkflowStatuses;
import com.progressoft.mpay.registration.customers.CustomerWorkflowServiceActions;
import com.progressoft.mpay.registration.customers.CustomerWorkflowStatuses;
import com.progressoft.mpay.registration.customers.MobileAccountWorkflowServiceActions;
import com.solab.iso8583.IsoMessage;

public class PostProcess1710MpClearIntegMsg implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostProcess1710MpClearIntegMsg.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map arg1, PropertySet ps) throws WorkflowException {

		logger.debug("inside PostProcess1710MpClearIntegMsg function .");
		MPAY_CustIntegMessage response = (MPAY_CustIntegMessage) transientVars.get(WorkflowService.WF_ARG_BEAN);
		long reason = Long.parseLong(response.getResponse().getCode());
		try {
			if (reason == 0) {
				handleMPClearResponse(response, true);
			} else if (reason > 0 && reason < 99) {
				handleMPClearResponse(response, false);
			}
		} catch (JAXBException exception) {
			logger.error("Error while processing registration response ", exception);
		}
	}

	private void handleMPClearResponse(MPAY_CustIntegMessage response, boolean isAccepted)
			throws WorkflowException, JAXBException {
		if (CustomerManagementActionTypes.ADD_CLIENT.isEqual(response.getRefOriginalMsg().getActionType())) {
			handleAddClient(response, isAccepted);
		} else if (CustomerManagementActionTypes.UPDATE_CLIENT.isEqual(response.getRefOriginalMsg().getActionType())) {
			handleUpdateClient(response, isAccepted);
		} else if (CustomerManagementActionTypes.REMOVE_CLIENT.isEqual(response.getRefOriginalMsg().getActionType())) {
			handleRemoveClient(response, isAccepted);
		} else if (CustomerManagementActionTypes.ADD_ACCOUNT.isEqual(response.getRefOriginalMsg().getActionType())) {
			handleAddAccount(response, isAccepted);
		} else if (CustomerManagementActionTypes.REMOVE_ACCOUNT.isEqual(response.getRefOriginalMsg().getActionType())) {
			handleRemoveAccount(response, isAccepted);
		} else if (CustomerManagementActionTypes.CHANGE_ALIAS.isEqual(response.getRefOriginalMsg().getActionType())) {
			handleChangeAlias(response, isAccepted);
		} else if (CustomerManagementActionTypes.SET_DEFAULT_ACCOUNT
				.isEqual(response.getRefOriginalMsg().getActionType())) {
			handleSetDefaultAccount(response, isAccepted);
		} else {
			throw new NotImplementedException("Customer integration action code: '"
					+ response.getRefOriginalMsg().getActionType().getCode() + "' is not implemented");
		}
	}

	private void handleAddAccount(MPAY_CustIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_MobileAccount account = getAccount(response);
		MPAYView view;
		JFWEntity entity;
		if (!account.getMobile().getRefCustomer().getIsRegistered()) {
			view = MPAYView.CUSTOMER;
			entity = account.getMobile().getRefCustomer();
		} else if (!account.getMobile().getIsRegistered()) {
			view = MPAYView.CUSTOMER_MOBILE_VIEWS;
			entity = account.getMobile();
		} else {
			view = MPAYView.MOBILE_ACCOUNTS_VIEW;
			entity = account;
		}

		if (isAccepted)
			JfwHelper.executeAction(view, entity, CustomerWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(view, entity, CustomerWorkflowServiceActions.REJECT, new WorkflowException());
	}

	private void handleAddClient(MPAY_CustIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_Customer customer = response.getRefCustomer();
		if (isAccepted)
			JfwHelper.executeAction(MPAYView.CUSTOMER, customer, CustomerWorkflowServiceActions.APPROVE,
					new WorkflowException());
		else {
			MPAY_MpClearResponseCode reason = LookupsLoader.getInstance()
					.getMpClearIntegrationResponseCodes(response.getResponse().getCode());
			customer.setRejectionNote(reason.getCode() + " - " + reason.getName());
			DataProvider.instance().updateCustomer(customer);
			JfwHelper.executeAction(MPAYView.CUSTOMER, customer, CustomerWorkflowServiceActions.REJECT,
					new WorkflowException());
		}
	}

	private void handleUpdateClient(MPAY_CustIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_Customer customer = response.getRefCustomer();
		MPAY_CustomerMobile mobile = response.getRefMobile();
		MPAYView view = null;
		JFWEntity entity = null;
		if (customer.getStatusId().getCode().equals(CustomerWorkflowStatuses.INTEGRATION)) {
			view = MPAYView.CUSTOMER;
			entity = customer;
		} else if (mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.INTEGRATION)) {
			view = MPAYView.CUSTOMER_MOBILE_VIEWS;
			entity = mobile;
		}
		if (isAccepted)
			JfwHelper.executeAction(view, entity, CustomerWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(view, entity, CustomerWorkflowServiceActions.REJECT, new WorkflowException());
	}

	private void handleRemoveClient(MPAY_CustIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_Customer customer = response.getRefCustomer();
		if (isAccepted)
			JfwHelper.executeAction(MPAYView.CUSTOMER, customer, CustomerWorkflowServiceActions.DELETE,
					new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.CUSTOMER, customer, CustomerWorkflowServiceActions.REJECT,
					new WorkflowException());
	}

	private void handleRemoveAccount(MPAY_CustIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_MobileAccount account = getAccount(response);
		MPAY_CustomerMobile mobile = response.getRefMobile();
		MPAYView view;
		JFWEntity entity;
		if (mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.INTEGRATION)) {
			view = MPAYView.CUSTOMER_MOBILE_VIEWS;
			entity = mobile;
		} else {
			view = MPAYView.MOBILE_ACCOUNTS_VIEW;
			entity = account;
		}
		if (isAccepted)
			JfwHelper.executeAction(view, entity, CustomerWorkflowServiceActions.DELETE, new WorkflowException());
		else
			JfwHelper.executeAction(view, entity, CustomerWorkflowServiceActions.REJECT, new WorkflowException());
	}

	private void handleSetDefaultAccount(MPAY_CustIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_CustomerMobile mobile = response.getRefMobile();
		MPAY_MobileAccount account = getAccount(response);

		if (isAccepted) {
			JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.APPROVE,
					new WorkflowException());
			for (MPAY_MobileAccount acc : mobile.getMobileMobileAccounts()) {
				if (acc.getId() != account.getId() && acc.getIsSwitchDefault()) {
					acc.setIsSwitchDefault(false);
					DataProvider.instance().updateMobileAccount(acc);
				}
			}
		} else {
			JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account,
					MobileAccountWorkflowServiceActions.DEFAULT_REJECT, new WorkflowException());
		}
	}

	private MPAY_MobileAccount getAccount(MPAY_CustIntegMessage response) throws WorkflowException {
		MPAY_CustIntegMessage originalMessage = response.getRefOriginalMsg();
		IsoMessage originalIsoMessage = ISO8583Parser
				.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath())
				.parseMessage(originalMessage.getRefMsgLog().getContent());
		String registrationId = originalIsoMessage.getField(62).getValue().toString();
		byte[] bytes = Base64.decodeBase64(registrationId);
		registrationId = new String(bytes, Charset.forName("UTF-16LE"));
		String routeCode = registrationId.substring(0, SystemParameters.getInstance().getRoutingCodeLength());
		long mas = Long.parseLong(registrationId.substring(SystemParameters.getInstance().getRoutingCodeLength(),
				SystemParameters.getInstance().getRoutingCodeLength()
						+ SystemParameters.getInstance().getMobileAccountSelectorLength()));
		MPAY_CustomerMobile mobile = response.getRefMobile();
		return selectFirst(mobile.getMobileMobileAccounts(),
				having(on(MPAY_MobileAccount.class).getBank().getCode(), Matchers.equalTo(routeCode))
						.and(having(on(MPAY_MobileAccount.class).getMas(), Matchers.equalTo(mas))));
	}

	private void handleChangeAlias(MPAY_CustIntegMessage response, boolean isAccepted)
			throws WorkflowException, JAXBException {
		MPAY_EndPointOperation aliasOperation;
		if (response.getRefOriginalMsg().getRefMsgLog().getRefMessage() == null)
			aliasOperation = LookupsLoader.getInstance()
					.getEndPointOperation(SystemParameters.getInstance().getAliasChangeEndPointOperation());
		else
			aliasOperation = response.getRefOriginalMsg().getRefMsgLog().getRefMessage().getRefOperation();
		MessageProcessor processor = MessageProcessorHelper.getProcessor(aliasOperation);
		if (processor == null)
			throw new WorkflowException("Alias Change Processor with code = "
					+ SystemParameters.getInstance().getAliasChangeEndPointOperation()
					+ " not found, make sure to change it in system configuration");
		MPClearProcessingContext context = new MPClearProcessingContext(
				new ProcessingContext(DataProvider.instance(), LookupsLoader.getInstance(),
						SystemParameters.getInstance(), new MPayCryptographer(), NotificationHelper.getInstance()));
		context.setMessage(response.getRefMsgLog());
		context.setIsoMessage(ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath())
				.parseMessage(response.getRefMsgLog().getContent()));
		context.setOperation(aliasOperation);
		context.setOriginalMessage(response.getRefOriginalMsg().getRefMsgLog());
		context.setMessageType(LookupsLoader.getInstance().getMpClearIntegrationMessageTypes("1710"));
		IntegrationProcessingContext integContext = new IntegrationProcessingContext(context);
		IntegrationProcessingResult result = processor.processIntegration(integContext);
		if (result != null) {
			for (MPAY_Notification notification : result.getNotifications()) {
				DataProvider.instance().persistNotification(notification);
				sendNotificationToMQ(notification);
			}
		}
//		if (!response.getRefMobile().getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.INTEGRATION))
//			return;
		if (isAccepted)
			JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, response.getRefMobile(),
					CustomerMobileWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, response.getRefMobile(),
					CustomerMobileWorkflowServiceActions.REJECT_ALIAS, new WorkflowException());
	}

	private void sendNotificationToMQ(MPAY_Notification notification) throws WorkflowException, JAXBException {
		JfwHelper.sendToActiveMQ(notification, notification.getTenantId(), MPAYView.NOTIFICATIONS.viewName,
				SmsNotificationType.GENERAL.getQueueName());
	}
}