package com.progressoft.mpay.payments.cashin;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entities.MPAY_ServiceCashIn;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.payments.CashRequestHelper;
import com.progressoft.mpay.tax.TaxCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Map;

public class PostServiceCashInCalcuateCharge implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostServiceCashInCalcuateCharge.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("***inside execute");
        MPAY_ServiceCashIn serviceCashIn = (MPAY_ServiceCashIn) arg0.get(WorkflowService.WF_ARG_BEAN);
        if (serviceCashIn.getAmount() != null && serviceCashIn.getAmount().doubleValue() > 0 && serviceCashIn.getRefCorporateService() != null) {
            String refAccount = CashRequestHelper.prepareRefMobileAccount(serviceCashIn.getRefServiceAccount());
            serviceCashIn.setRefServiceAccount(refAccount);
            MPAY_ServiceAccount account = SystemHelper.getServiceAccount(MPayContext.getInstance().getSystemParameters(), serviceCashIn.getRefCorporateService(), refAccount);
            if (account == null)
                return;
            MPAY_Profile profile = account.getRefProfile();
            BigDecimal chargeAmount = ChargesCalculator.calculateCharge(MPayContext.getInstance().getDataProvider(), serviceCashIn.getAmount(), MessageCodes.CI.toString(), profile, false);
            BigDecimal taxAmount = TaxCalculator.calculateTax(MPayContext.getInstance().getDataProvider(), chargeAmount, MessageCodes.CI.toString(), profile, MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
            serviceCashIn.setTransAmount(serviceCashIn.getAmount());
            serviceCashIn.setChargeAmount(chargeAmount);
            serviceCashIn.setTaxAmount(taxAmount);
            serviceCashIn.setTotalAmount(serviceCashIn.getAmount().add(chargeAmount).add(taxAmount));
        }
    }
}