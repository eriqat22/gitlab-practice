package com.progressoft.mpay.mpclear;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgType;
import com.progressoft.mpay.mpclear.plugins.MPClearMessageProcessor;

public class MPClearMessageProcessorHelper {
	private static final Logger logger = LoggerFactory.getLogger(MPClearMessageProcessorHelper.class);
	private static HashMap<String, MPClearMessageProcessor> processors = new HashMap<>();

	private MPClearMessageProcessorHelper() {

	}

	public static MPClearMessageProcessor getProcessor(MPAY_MpClearIntegMsgType type) {
		logger.debug("Inside GetProcessor ...");
		if (type == null)
			return null;

		try {
			if (!processors.containsKey(type.getCode())) {
				Class<?> cls = Class.forName(type.getProcessor());
				processors.put(type.getCode(), (MPClearMessageProcessor) cls.newInstance());
			}
			return processors.get(type.getCode());
		} catch (Exception e) {
			logger.error("Error in GetProcessor", e);
			return null;
		}
	}

	public static void flush() {
		processors.clear();
	}
}
