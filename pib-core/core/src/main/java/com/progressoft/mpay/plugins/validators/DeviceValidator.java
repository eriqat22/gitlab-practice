package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;

public class DeviceValidator {
    private static final Logger logger = LoggerFactory.getLogger(DeviceValidator.class);

    private DeviceValidator() {

    }

    public static ValidationResult validate(MPAY_CustomerDevice device) {
        logger.debug("Inside Validate ...");
        if (device == null)
            return new ValidationResult(ReasonCodes.DEVICE_NOT_FOUND, null, false);
        if (isNotSaibApplication()) {
            if (device.getIsBlocked())
                return new ValidationResult(ReasonCodes.DEVICE_BLOCKED, null, false);
        }
        if (device.getIsStolen())
            return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static boolean isNotSaibApplication() {
        logger.info("----------------------- the tenant name is "  + AppContext.getCurrentTenant() + " --------------- ");
        return !"SAIB".equals(AppContext.getCurrentTenant().trim());
    }

    public static ValidationResult validate(MPAY_CorporateDevice device) {
        logger.debug("Inside Validate ...");
        if (device == null)
            return new ValidationResult(ReasonCodes.DEVICE_NOT_FOUND, null, false);
        if (device.getIsBlocked())
            return new ValidationResult(ReasonCodes.DEVICE_BLOCKED, null, false);
        if (device.getIsStolen())
            return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
