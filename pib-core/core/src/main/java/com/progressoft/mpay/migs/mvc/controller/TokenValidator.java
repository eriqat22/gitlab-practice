package com.progressoft.mpay.migs.mvc.controller;

import javax.servlet.http.HttpServletRequest;

public abstract class TokenValidator {

	protected MigsContext context;

	public TokenValidator(MigsContext context) {
		this.context = context;
	}

	public abstract boolean isValidToken(HttpServletRequest request);
}
