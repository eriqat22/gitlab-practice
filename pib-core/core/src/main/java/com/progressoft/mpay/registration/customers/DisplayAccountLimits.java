package com.progressoft.mpay.registration.customers;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hamcrest.Matchers;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.DynamicField;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.AccountLimits;
import com.progressoft.mpay.LimitTypes;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class DisplayAccountLimits implements ChangeHandler {

    public static final String LIMITS = "Limits";

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
        for (UIPortletAttributes ui : portletsAttributes) {
            ui.setVisible(true);
        }
        Object account = changeHandlerContext.getEntity();
        if (account instanceof MPAY_MobileAccount)
            handleMobileAccount((MPAY_MobileAccount) account, changeHandlerContext);
        else if (account instanceof MPAY_ServiceAccount)
            handleServiceAccount((MPAY_ServiceAccount) account, changeHandlerContext);
    }

    private void handleServiceAccount(MPAY_ServiceAccount account, ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        try {
            if (account.getRefAccount() == null)
                return;
            AccountLimits limits = MPayContext.getInstance().getDataProvider().getAccountLimits(account.getRefAccount().getId(), account.getMessageType().getId());
            renderFields(changeHandlerContext, limits, account.getRefProfile(), account.getMessageType().getId());
        } catch (Exception e) {
            throw new InvalidInputException(e);
        }
    }

    private void handleMobileAccount(MPAY_MobileAccount account, ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        try {
            if (account.getRefAccount() == null || account.getMessageType() == null)
                return;
            AccountLimits limits = MPayContext.getInstance().getDataProvider().getAccountLimits(account.getRefAccount().getId(), account.getMessageType().getId());
            renderFields(changeHandlerContext, limits, account.getRefProfile(), account.getMessageType().getId());
        } catch (Exception e) {
            throw new InvalidInputException(e);
        }
    }

    private void renderFields(ChangeHandlerContext changeHandlerContext, AccountLimits limits, MPAY_Profile profile, long messageTypeId) {
        List<DynamicField> dynamicFields = new ArrayList<>();
        List<MPAY_Limit> schemeLimits = profile.getLimitsScheme().getRefSchemeLimits();
        MPAY_LimitsDetail limitDetails = getLimitDetails(schemeLimits, LimitTypes.DAILY, messageTypeId);
        boolean isActive = limitDetails.getIsActive() && limitDetails.getRefLimit().getIsActive() && limitDetails.getRefLimit().getRefScheme().getIsActive();

        DynamicField field = new DynamicField();
        field.setLabel("Limit Type");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel("Current Limit");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel("Max Limit");
        dynamicFields.add(field);

        setDailyLimits(limits, dynamicFields, limitDetails, isActive);
        setWeeklyLimits(limits, messageTypeId, dynamicFields, schemeLimits);
        setMonthlyLimits(limits, messageTypeId, dynamicFields, schemeLimits);
        setYearlyLimits(limits, messageTypeId, dynamicFields, schemeLimits);

        changeHandlerContext.getPortletsAttributes().get(LIMITS).setVisible(true);
        changeHandlerContext.getPortletsAttributes().get(LIMITS).setDynamicPortlet(true);
        changeHandlerContext.getPortletsAttributes().get(LIMITS).setDynamicFields(dynamicFields);
    }

	private void setYearlyLimits(AccountLimits limits, long messageTypeId, List<DynamicField> dynamicFields, List<MPAY_Limit> schemeLimits) {
		DynamicField field;
		MPAY_LimitsDetail limitDetails = getLimitDetails(schemeLimits, LimitTypes.YEARLY, messageTypeId);
		boolean isActive = limitDetails.getIsActive() && limitDetails.getRefLimit().getIsActive() && limitDetails.getRefLimit().getRefScheme().getIsActive();
        field = new DynamicField();
        field.setLabel("Yearly Count");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(String.valueOf(limits.getYearlyCount()));
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(isActive ? String.valueOf(limitDetails.getTxCountLimit()) : "-");
        field.setColor("red");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel("Yearly Amount");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(SystemHelper.formatAmount(MPayContext.getInstance().getSystemParameters(), limits.getYearlyAmount()));
        field.setColor("red");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(isActive ? SystemHelper.formatAmount(MPayContext.getInstance().getSystemParameters(), limitDetails.getTxAmountLimit()) : "-");
        dynamicFields.add(field);
	}

	private void setMonthlyLimits(AccountLimits limits, long messageTypeId, List<DynamicField> dynamicFields, List<MPAY_Limit> schemeLimits) {
		DynamicField field;
		MPAY_LimitsDetail limitDetails = getLimitDetails(schemeLimits, LimitTypes.MONTHLY, messageTypeId);
		boolean isActive = limitDetails.getIsActive() && limitDetails.getRefLimit().getIsActive() && limitDetails.getRefLimit().getRefScheme().getIsActive();
        field = new DynamicField();
        field.setLabel("Monthly Count");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(String.valueOf(limits.getMonthlyCount()));
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(isActive ? String.valueOf(limitDetails.getTxCountLimit()) : "-");
        field.setColor("red");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel("Monthly Amount");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(SystemHelper.formatAmount(MPayContext.getInstance().getSystemParameters(), limits.getMonthlyAmount()));
        field.setColor("red");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(isActive ? SystemHelper.formatAmount(MPayContext.getInstance().getSystemParameters(), limitDetails.getTxAmountLimit()) : "-");
        dynamicFields.add(field);
	}

	private void setWeeklyLimits(AccountLimits limits, long messageTypeId, List<DynamicField> dynamicFields, List<MPAY_Limit> schemeLimits) {
		DynamicField field;
		MPAY_LimitsDetail limitDetails = getLimitDetails(schemeLimits, LimitTypes.WEEKLY, messageTypeId);
		boolean isActive = limitDetails.getIsActive() && limitDetails.getRefLimit().getIsActive() && limitDetails.getRefLimit().getRefScheme().getIsActive();
        field = new DynamicField();
        field.setLabel("Weekly Count");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(String.valueOf(limits.getWeeklyCount()));
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(isActive ? String.valueOf(limitDetails.getTxCountLimit()) : "-");
        field.setColor("red");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel("Weekly Amount");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(SystemHelper.formatAmount(MPayContext.getInstance().getSystemParameters(), limits.getWeeklyAmount()));
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(isActive ? SystemHelper.formatAmount(MPayContext.getInstance().getSystemParameters(), limitDetails.getTxAmountLimit()) : "-");
        field.setColor("red");
        dynamicFields.add(field);
	}

	private void setDailyLimits(AccountLimits limits, List<DynamicField> dynamicFields, MPAY_LimitsDetail limitDetails, boolean isActive) {
		DynamicField field;
		field = new DynamicField();
        field.setLabel("Daily Count");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(String.valueOf(limits.getDailyCount()));
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(isActive ? String.valueOf(limitDetails.getTxCountLimit()) : "-");
        field.setColor("red");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel("Daily Amount");
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(SystemHelper.formatAmount(MPayContext.getInstance().getSystemParameters(), limits.getDailyAmount()));
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(isActive ? SystemHelper.formatAmount(MPayContext.getInstance().getSystemParameters(), limitDetails.getTxAmountLimit()) : "-");
        field.setColor("red");
        dynamicFields.add(field);
	}

    private MPAY_LimitsDetail getLimitDetails(List<MPAY_Limit> limits, long limitTypeId, long messageTypeId) {
        MPAY_Limit limit = selectFirst(limits, having(on(MPAY_Limit.class).getRefType().getId(), Matchers.equalTo(limitTypeId)));
        return selectFirst(limit.getRefLimitLimitsDetails(), having(on(MPAY_LimitsDetail.class).getMsgType().getId(), Matchers.equalTo(messageTypeId)));
    }
}
