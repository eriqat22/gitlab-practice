package com.progressoft.mpay.registration.corporates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class CorporateServiceCityChangeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(CorporateServiceCityChangeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside CorporateServiceCityChangeHandler ------------");
		MPAY_CorpoarteService corporate = (MPAY_CorpoarteService)changeHandlerContext.getEntity();
		corporate.setArea(null);
	}
}
