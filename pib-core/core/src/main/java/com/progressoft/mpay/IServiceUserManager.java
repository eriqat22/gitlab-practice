package com.progressoft.mpay;

import com.progressoft.jfw.model.bussinessobject.security.Tenant;

public interface IServiceUserManager {

	boolean enableServiceUser(String tenantId);

	void disableServiceUser();

	String getUserName();

	Tenant getServiceTenant();
}
