package com.progressoft.mpay.registration.customers;

public class CustomerWorkflowStatuses {
	public static final String MODIFICATION = "100004";
	public static final String APPROVED = "100005";
	public static final String DELETED = "100006";
	public static final String INTEGRATION = "100007";
	public static final String SUSPENDED = "100014";
	public static final String CREATED = "1000230";

	private CustomerWorkflowStatuses() {

	}
}
