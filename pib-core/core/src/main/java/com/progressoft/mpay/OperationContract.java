package com.progressoft.mpay;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class OperationContract {

    public static final String TOKEN = "token";
    public static final String SECTION = "section";

    @POST
    @Path("/operation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response operation(
            @QueryParam(OperationContract.TOKEN) String token, @QueryParam(OperationContract.SECTION) String section, String body) {
        return null;
    }

    @GET
    @Path("/operation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getoperation(
            @QueryParam(OperationContract.TOKEN) String token, @QueryParam(OperationContract.SECTION) String section, String body) {
        return null;
    }
}
