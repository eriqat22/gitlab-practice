package com.progressoft.mpay.payments.cashin;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CashIn;


public class ValidateCashInRequest implements Validator {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ValidateCashInRequest.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("\n\r===inside ValidateCashInRequest....");

		InvalidInputException iie = new InvalidInputException();

		MPAY_CashIn cashin = (MPAY_CashIn) arg0.get(WorkflowService.WF_ARG_BEAN);

		if (!cashin.getRefCustMob().getRefCustomer().getIsActive()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("Customer.isnotActive"));
		}

		if (!cashin.getRefCustMob().getIsActive()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("Customer.mobile.isnotActive"));
		}

		if (cashin.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("Amount.shouldBeGreaterThanZero"));
		}

		if (iie.getErrors().size() > 0) {
			throw iie;
		}
	}
}