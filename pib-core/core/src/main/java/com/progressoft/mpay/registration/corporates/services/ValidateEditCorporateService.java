package com.progressoft.mpay.registration.corporates.services;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.registration.corporates.ValidateCorporateService;

public class ValidateEditCorporateService implements Validator {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ValidateCorporateService.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside ValidateEditCorporateService-----");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);

		if (service.getName() != null)
			service.setName(service.getName().toUpperCase());
		if (service.getAlias() != null)
			service.setAlias(service.getAlias().toUpperCase());
		if (service.getMpclearAlias() != null)
			service.setMpclearAlias(service.getMpclearAlias().toUpperCase());

		validateServiceName(service);
	}

	private void validateServiceName(MPAY_CorpoarteService service) throws InvalidInputException {
		Map<String, Object> nameProperties = new HashMap<>();
		nameProperties.put("name", service.getName());
		InvalidInputException cause = Unique.validate(itemDao, service, nameProperties, MPayErrorMessages.SERVICE_INVALID_NAME_IS_USED);
		if (cause.getErrors().size() > 0)
			throw cause;
	}

}
