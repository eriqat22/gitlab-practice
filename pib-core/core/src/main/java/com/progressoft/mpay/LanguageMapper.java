package com.progressoft.mpay;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import com.progressoft.mpay.exceptions.MPayGenericException;

public class LanguageMapper {
	private static final long DEFAULT_LANGUAGE_LONG_CODE = 1;
	private static LanguageMapper instance;
	private Map<String, String> languages;

	private LanguageMapper() {
		initialize();
	}

	public static LanguageMapper getInstance() {
		if (instance == null)
			instance = new LanguageMapper();

		return instance;
	}

	private void initialize() {
		languages = new HashMap<>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try {
			Document document = factory.newDocumentBuilder()
					.parse(classLoader.getResourceAsStream("/config/app/spring/LanguagesMapping.xml"));
			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expression = xpath.compile("/languages/language");
			NodeList languagesNodes = (NodeList) expression.evaluate(document, XPathConstants.NODESET);
			for (int i = 0; i < languagesNodes.getLength(); i++) {
				NamedNodeMap attributes = languagesNodes.item(i).getAttributes();
				languages.put(attributes.getNamedItem("code").getTextContent(), attributes.getNamedItem("localeCode").getTextContent());
			}
		} catch (Exception e) {
			throw new MPayGenericException("File not found", e);
		}
	}

	public String  getLanguageLocaleCode(String code) {
		return this.languages.get(code);
	}

	public long getLanguageLongByLocaleCode(String localeCode) {
		if (!languages.containsValue(localeCode))
			return DEFAULT_LANGUAGE_LONG_CODE;

		for (Entry<String, String> language : languages.entrySet()) {
			if (language.getValue().equals(localeCode))
				return Long.parseLong(language.getKey());
		}
		return DEFAULT_LANGUAGE_LONG_CODE;
	}

	public String getLanguageCodeByLocaleCode(String localeCode) {
		if (!languages.containsValue(localeCode))
			return null;

		for (Entry<String, String> language : languages.entrySet()) {
			if (language.getValue().equals(localeCode))
				return language.getKey();
		}
		return null;
	}
}
