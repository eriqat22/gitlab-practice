package com.progressoft.mpay.registration.corporates;

public enum CorporateServiceWorkflowStatuses {
	APPROVAL("200102"), REJECTED("200103"), MODIFICATION("200104"), APPROVED("200105"), DELETED("200106"), INTEGRATION("200107"), DELETING("200108"), MODIFIED("200110"), CANCELING("200111"), SUSPENDED("200115");

	private final String status;

	CorporateServiceWorkflowStatuses(String value) {
		this.status = value;
	}

	@Override
	public String toString() {
		return this.status;
	}
}
