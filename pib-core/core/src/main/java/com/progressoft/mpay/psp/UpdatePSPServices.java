package com.progressoft.mpay.psp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entity.BankType;

public class UpdatePSPServices implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(UpdatePSPServices.class);

    @Autowired
    AddPSPServices addPSPServicesFunction;
    @Autowired
    private ItemDao itemDao;

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("Inside UpdatePSPServices....");

        MPAY_Bank bank = (MPAY_Bank) transientVars.get(WorkflowService.WF_ARG_BEAN);

        if (bank.getSettlementParticipant().equals(BankType.SETTLEMENT)) {
            updateService(bank, transientVars, args, ps);
        }
    }

    @SuppressWarnings("rawtypes")
	private void updateService(MPAY_Bank bank, Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        Filter filter = new Filter();
        filter.setProperty("bank.code");
        filter.setFirstOperand(bank.getCode());
        filter.setOperator(Operator.EQ);
        List<Filter> filters = new ArrayList<>();
        filters.add(filter);

        List<MPAY_CorpoarteService> pspService = itemDao.getItems(MPAY_CorpoarteService.class, null, filters, null, null);
        if (pspService.isEmpty()) {
            addPSPServicesFunction.execute(transientVars, args, ps);
            return;
        }
        if (!bank.getTellerCode().equals(pspService.get(0).getMpclearAlias())) {
            for (MPAY_CorpoarteService srv : pspService) {
                srv.setMpclearAlias(bank.getTellerCode());
                if (srv.getAlias() != null && srv.getAlias().length() > 0) {
                    srv.setAlias(bank.getTellerCode() + srv.getAlias().substring(srv.getAlias().length() - 2));
                }
                srv.setMas(1L);
                itemDao.merge(srv);
            }
        }

    }

}
