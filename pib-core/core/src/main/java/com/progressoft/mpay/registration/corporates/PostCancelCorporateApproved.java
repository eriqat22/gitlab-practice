package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.security.Tenant;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.CorporateMetaData;
import com.progressoft.mpay.entity.MPAYView;

public class PostCancelCorporateApproved implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCancelCorporateApproved.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =====");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		revertCorporateChanges(corporate);
		handleServices(corporate);
	}

	private void revertCorporateChanges(MPAY_Corporate corporate) {
		if (corporate.getApprovedData() == null)
			return;

		CorporateMetaData approvedCorporate = CorporateMetaData.fromJson(corporate.getApprovedData());
		approvedCorporate.fillCorporateFromMetaData(corporate);
		corporate.setPrefLang(LookupsLoader.getInstance().getLanguage(approvedCorporate.getPrefLangId()));
		corporate.setCity(LookupsLoader.getInstance().getCity(approvedCorporate.getCityCode()));
		revertTenants(corporate, approvedCorporate);
		corporate.setApprovedData(null);
	}

	private void revertTenants(MPAY_Corporate corporate, CorporateMetaData approvedCorporate) {
		if (approvedCorporate.getTenants() == null || approvedCorporate.getTenants().isEmpty()) {
			if (corporate.getTenants() == null || corporate.getTenants().isEmpty())
				return;
			corporate.setTenants(null);
			return;
		}
		addRemovedTenants(corporate, approvedCorporate);
		removeAddedTenants(corporate, approvedCorporate);
	}

	private void addRemovedTenants(MPAY_Corporate corporate, CorporateMetaData approvedCorporate) {
		List<Tenant> tenants = DataProvider.instance().listTenants();
		Iterator<String> oldTenants = approvedCorporate.getTenants().iterator();
		do {
			String tenantName = oldTenants.next();
			Tenant oldTenant = selectFirst(corporate.getTenants(), having(on(Tenant.class).getName(), Matchers.equalTo(tenantName)));
			if (oldTenant != null)
				continue;
			Tenant tenant = selectFirst(tenants, having(on(Tenant.class).getName(), Matchers.equalTo(tenantName)));
			corporate.getTenants().add(tenant);
		} while (oldTenants.hasNext());
	}

	private void removeAddedTenants(MPAY_Corporate corporate, CorporateMetaData approvedCorporate) {
		if (corporate.getTenants() == null || corporate.getTenants().isEmpty())
			return;
		Iterator<Tenant> newTenants = corporate.getTenants().iterator();

		do {
			Tenant tenant = newTenants.next();
			String existedTenant = selectFirst(approvedCorporate.getTenants(), having(on(String.class), Matchers.equalTo(tenant.getName())));
			if (existedTenant == null)
				corporate.getTenants().remove(tenant);
		} while (newTenants.hasNext());
	}

	private void handleServices(MPAY_Corporate corporate) throws WorkflowException {
		logger.debug("Inside HandleServices =====");
		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId(), CorporateServiceWorkflowStatuses.CANCELING.toString());
		if (services == null || services.isEmpty())
			return;
		Iterator<MPAY_CorpoarteService> servicesIterator = corporate.getRefCorporateCorpoarteServices().listIterator();
		if (servicesIterator.hasNext()) {
			MPAY_CorpoarteService service = servicesIterator.next();
			JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.APPROVE, new WorkflowException());
		}
	}
}