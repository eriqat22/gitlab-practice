package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Map;

@Component
public class PreCreateServiceAccount implements FunctionProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(PreCreateServiceAccount.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        LOGGER.debug("Inside execute PreCreateServiceAccount");
        MPAY_ServiceAccount serviceAccount = (MPAY_ServiceAccount) arg0.get(WorkflowService.WF_ARG_BEAN);
        serviceAccount.setIsActive(false);
        serviceAccount.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
    }

}