package com.progressoft.mpay.requesttypes;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_RequestTypesDetail;
import com.progressoft.mpay.entities.MPAY_RequestTypesScheme;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;

@Component
public class PostCancelRequestTypes implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCancelRequestTypes.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		try {
			logger.debug("Inside PostCreateLimitsScheme----------------");
			MPAY_RequestTypesScheme requestType = (MPAY_RequestTypesScheme) arg0.get(WorkflowService.WF_ARG_BEAN);
			for (MPAY_RequestTypesDetail detail : requestType.getRefSchemeRequestTypesDetails()) {
				if (detail.getJfwDraft() != null)
					JfwHelper.executeAction(MPAYView.REQUEST_TYPES_DETAILS, detail, "SVC_Cancel", new WorkflowException());
			}
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}
}