package com.progressoft.mpay.psp;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Corporate;

public class ValidatePSPExisted implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidatePSPExisted.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("***inside ValidatePSPExisted");

		MPAY_Corporate psp;
			psp = PSP.get();
		if (psp == null) {
			InvalidInputException iie = new InvalidInputException();
			iie.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS, "The Payment service provider not defined");
			throw iie;
		}
	}
}