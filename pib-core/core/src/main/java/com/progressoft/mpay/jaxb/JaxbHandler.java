package com.progressoft.mpay.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.progressoft.mpay.sms.NotificationRequest;
import com.progressoft.mpay.sms.NotificationResponse;

public class JaxbHandler {
	private static JAXBContext jaxbContext;

	private JaxbHandler() {
	}

	public static JAXBContext getJaxbContextInstance() throws JAXBException {
		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(NotificationRequest.class, NotificationResponse.class);
		}
		return jaxbContext;
	}
}