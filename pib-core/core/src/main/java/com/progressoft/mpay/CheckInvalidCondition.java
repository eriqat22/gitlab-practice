package com.progressoft.mpay;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;

public class CheckInvalidCondition implements Condition {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		return Result.pass(transientVar, Result.VALIDATION_RESULT, Result.FAILED);
	}

}
