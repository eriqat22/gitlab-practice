package com.progressoft.mpay;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;

public class RejectedCondition implements Condition {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		return Result.pass(ps, Result.PROCESSING_RESULT, Result.REJECT);
	}
}
