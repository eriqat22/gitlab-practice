package com.progressoft.mpay.entity;

public class ResponseCodesBase {
    public static final String SUCCESS = "00";
    public static final String INVALID_SENDER_ROUTE = "01";
    public static final String INACTIVE_SENDER_INSTRUCTING_PARTICIPANT = "02";
    public static final String INVALID_RECEIVER_ROUTE = "03";
    public static final String INACTIVE_RECEIVER_INSTRUCTING_PARTICIPANT = "04";
    public static final String INACTIVE_DEBTOR_PARTICIPANT = "05";
    public static final String INACTIVE_CREDITOR_PARTICIPANT = "06";
    public static final String INVALID_COMMUNICATION_CHANNEL = "07";
    public static final String INVALID_AMOUNT = "08";
    public static final String INVALID_RESPONSE_CODE = "09";
    public static final String INVALID_RETURNED_REASON = "10";
    public static final String RESPONSE_DOES_NOT_MATCH_REQUEST = "11";
    public static final String INCORRECT_MESSAGE_IN_QUEUE = "12";
    public static final String DUPLICATE_MESSAGE_ID = "13";
    public static final String DUPLICATE_COMMUNICATION_ID = "14";
    public static final String NO_AVAILABLE_SETTLEMENT_WINDOW = "15";
    public static final String REQUEST_DOES_NOT_EXIST = "16";
    public static final String REQUEST_IS_NOT_PENDING = "17";
    public static final String MESSAGE_PROCESSING_TIMED_OUT = "18";
    public static final String REPLY_TIMED_OUT = "19";
    public static final String FAILED_TO_PARSE_MESSAGE = "20";
    public static final String FAILED_TO_PERSIST = "21";
    public static final String INVALID_REQUESTING_PARTICIPANT = "22";
    public static final String REQUESTING_PARTICIPANT_AND_SENDER_PARTICIPANT_DO_NOT_MATCH = "23";
    public static final String DEBTOR_PARTICIPANT_ACCOUNT_DOES_NOT_EXIST = "24";
    public static final String CREDITOR_PARTICIPANT_ACCOUNT_DOES_NOT_EXIST = "25";
    public static final String ORIGINAL_MESSAGE_NOT_FOUND = "27";
    public static final String INVALID_MESSAGE_ID = "28";
    public static final String INVALID_ENQUIRY_SENDER = "29";
    public static final String NOT_SUPPORTED_OPERATION = "30";
    public static final String DEBTOR_CLIENT_IS_BLACK_LISTED = "31";
    public static final String DEBTOR_CLIENT_INACTIVE = "32";
    public static final String DEBTOR_CLIENT_IS_NOT_DEFINED = "33";
    public static final String CREDITOR_CLIENT_IS_BLACK_LISTED = "34";
    public static final String CREDITOR_CLIENT_INACTIVE = "35";
    public static final String CREDITOR_CLIENT_IS_NOT_DEFINED = "36";
    public static final String SENDER_AND_RECEIVER_ROUTES_NOT_MATCHED = "38";
    public static final String INVALID_CITY_CODE = "39";
    public static final String INVALID_COUNTRY_CODE = "40";
    public static final String CLIENT_IDENTIFIER_IS_EMPTY = "41";
    public static final String CLIENT_ID_TYPE_UNDEFINED = "42";
    public static final String CLIENT_IS_DEFINED = "43";
    public static final String OWNER_IS_DEFINED = "44";
    public static final String INVALID_MOBILE_NUMBER = "45";
    public static final String INVALID_SERVICE_NAME = "46";
    public static final String INVALID_REGISTRATION_CODE = "47";
    public static final String ACCOUNT_DEFINED = "48";
    public static final String ACCOUNT_NUMBER_IS_EMPTY = "49";
    public static final String INACTIVE_SETTLEMENT_BANK = "50";
    public static final String CLIENT_DAILY_BALANCE_EXCEEDED = "51";
    public static final String PAYMENT_TYPE_NOT_SUPPORTED = "52";
    public static final String CLIENT_IS_NOT_DEFINED = "53";
    public static final String INVALID_CURRENCY = "54";
    public static final String CLIENT_TYPE_UNDEFINED = "55";
    public static final String SENDER_OFFLINE = "56";
    public static final String RECEIVER_OFFLINE = "57";
    public static final String SENDER_LOGIN_TIMEOUT = "58";
    public static final String RECEIVER_LOGIN_TIMEOUT = "59";
    public static final String INVALID_SECURITY_KEY = "60";
    public static final String INVALID_SECURITY_KEY_LENGTH = "61";
    public static final String INVALID_ALIAS = "62";

    ResponseCodesBase() {
    }
}
