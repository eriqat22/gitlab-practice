package com.progressoft.mpay.exceptions;

public class InvalidArgumentException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public InvalidArgumentException(String message, Exception exp) {
        super(message, exp);
    }

    public InvalidArgumentException(String message) {
        super(message);
    }

    public InvalidArgumentException(Exception ex) {
        super(ex);
    }
}
