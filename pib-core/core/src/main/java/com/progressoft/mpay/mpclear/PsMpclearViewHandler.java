package com.progressoft.mpay.mpclear;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.ClientTypes;

public class PsMpclearViewHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(PsMpclearViewHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("***inside Ps Mpclear View Handler: " + changeHandlerContext.getSource());

		try {
			MPAY_Corporate psMplcear = (MPAY_Corporate) changeHandlerContext.getEntity();
			MPAY_ClientType type = LookupsLoader.getInstance().getClientType(ClientTypes.PS_MPCLEAR);
			psMplcear.setClientType(type);
			changeHandlerContext.getFieldsAttributes().get("clientType").setEnabled(false);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new BusinessException("An error occured in the Ps Mpclear View Handler", ex);
		}
	}
}
