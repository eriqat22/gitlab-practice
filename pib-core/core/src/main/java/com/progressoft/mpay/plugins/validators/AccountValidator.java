package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.registration.corporates.CorporateServiceWorkflowStatuses;
import com.progressoft.mpay.registration.customers.MobileAccountWorkflowStatuses;

public class AccountValidator {
	private static final Logger logger = LoggerFactory.getLogger(AccountValidator.class);

	private AccountValidator() {

	}

	public static ValidationResult validate(MPAY_MobileAccount mobileAccount, boolean isSender,boolean isFinacial) {
		logger.debug("Inside Validate ...");
		if (mobileAccount == null || mobileAccount.getRefAccount() == null) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_ACCOUNT_NOT_DEFINED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_ACCOUNT_NOT_DEFINED, null, false);
		}

		if (!mobileAccount.getIsActive() || !mobileAccount.getRefAccount().getIsActive()) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_ACCOUNT_IN_ACTIVE, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_ACCOUNT_IN_ACTIVE, null, false);
		}

		if (mobileAccount.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.SUSPENDED.toString())) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_ACCOUNT_SUSPENDED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_ACCOUNT_SUSPENDED, null, false);
		}
		if(isFinacial) {
			ValidationResult validationResult = DormantAccountValidator.validateMobileAccountAgainstDormantCase(mobileAccount);
			if (!validationResult.isValid())
				return validationResult;
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	public static ValidationResult validate(MPAY_ServiceAccount serviceAccount, boolean isSender,boolean isFinacial) {
		logger.debug("Inside Validate ...");
		if (serviceAccount == null || serviceAccount.getRefAccount() == null) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_ACCOUNT_NOT_DEFINED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_ACCOUNT_NOT_DEFINED, null, false);
		}
		if (!serviceAccount.getIsActive() || !serviceAccount.getRefAccount().getIsActive()) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_ACCOUNT_IN_ACTIVE, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_ACCOUNT_IN_ACTIVE, null, false);
		}
		if (serviceAccount.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.SUSPENDED.toString())) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_ACCOUNT_SUSPENDED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_ACCOUNT_SUSPENDED, null, false);
		}
		if(isFinacial) {

			ValidationResult validationResult = DormantAccountValidator.validateServiceAccountAgainstDormantCase(serviceAccount);
			if (!validationResult.isValid())

				return validationResult;
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
