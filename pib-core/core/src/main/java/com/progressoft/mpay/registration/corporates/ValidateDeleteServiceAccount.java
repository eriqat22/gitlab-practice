package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class ValidateDeleteServiceAccount implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteServiceAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside validate ==========");
		InvalidInputException iie = new InvalidInputException();

		MPAY_ServiceAccount account = (MPAY_ServiceAccount) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (account.getIsDefault() || account.getIsSwitchDefault()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.ACCOUNT_DELETE_DEFAULT));
			throw iie;
		}
		if (account.getRefAccount() != null && account.getRefAccount().getBalance().intValue() > 0) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DELETE_ITEM_ACCOUNT));
			throw iie;
		}
	}
}
