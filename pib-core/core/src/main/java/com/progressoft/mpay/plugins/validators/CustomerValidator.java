package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.registration.customers.CustomerWorkflowStatuses;

public class CustomerValidator {
	private static final Logger logger = LoggerFactory.getLogger(CustomerValidator.class);

	private CustomerValidator() {

	}

	public static ValidationResult validate(MPAY_Customer customer, boolean isSender) {
		logger.debug("Inside Validate ...");
		if (!customer.getIsActive() || !customer.getIsRegistered())
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_CUSTOMER_NOT_ACTIVE, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_CUSTOMER_NOT_ACTIVE, null, false);

		return validateSuspenssion(customer, isSender);
	}

	private static ValidationResult validateSuspenssion(MPAY_Customer customer, boolean isSender) {
		if (customer.getStatusId().getCode().equals(CustomerWorkflowStatuses.SUSPENDED)) {
			if (isSender)
				return new ValidationResult(ReasonCodes.SENDER_CUSTOMER_SUSPENDED, null, false);
			else
				return new ValidationResult(ReasonCodes.RECEIVER_CUSTOMER_SUSPENDED, null, false);
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
