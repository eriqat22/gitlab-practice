package com.progressoft.mpay.migs.request;

import java.util.HashMap;
import java.util.Map;

import com.progressoft.mpay.migs.mvc.controller.MigsContext;
import com.progressoft.mpay.migs.mvc.controller.MigsException;
import com.progressoft.mpay.migs.mvc.controller.MigsHAshingImp;
import com.progressoft.mpay.migs.mvc.controller.MigsHashing;

public class MigsHAshingBuilder {

	static final String DEFAULT = "DEFAULT";

	static Map<String, MigsHashing> types = new HashMap<>();

	private MigsHAshingBuilder() {

	}

	public static MigsHashing getMigsHAshing(MigsContext context) {
		initializeTypes(context);
		if (types.containsKey(DEFAULT))
			return types.get(DEFAULT);
		else
			throw new MigsException("Not Supported Message");
	}

	private static void initializeTypes(MigsContext context) {
		if(types.isEmpty())
			types.put(DEFAULT, new MigsHAshingImp(context));
	}
}