package com.progressoft.mpay;

public class MPayErrorMessages {
    public static final String MOBILE_INVALID_STATUS = "mobile.invalid.status";
    public static final String ACCOUNT_INVALID_STATUS = "account.invalid.status";
    public static final String MOBILE_UNDER_MODIFICATION = "mobile.under.modification";
    public static final String MOBILE_ACCOUNT_UNDER_MODIFICATION = "mobile.account.under.modification";
    public static final String DELETE_ITEM_ACCOUNT = "delete.item.account";
    public static final String ACCOUNT_DELETE_DEFAULT = "account.delete.default";
    public static final String INVALID_MAS_LENGTH = "invalid.mas.length";
    public static final String DUPLICATE_MAS = "duplicate.mas";
    public static final String DUPLICATE_EXTERNAL_ACCOUNT = "duplicate.external.account";
    public static final String DUPLICATE_ISDEFUALT = "duplicate.isdefualt";
    public static final String ACCOUNT_MAX_REACHED = "account.max.reached";
    public static final String MOBILE_INVALID_NUMBER_IS_USED = "mobile.invalid.numberIsUsed";
    public static final String MOBILE_INVALID_ALIAS_IS_USED = "mobile.invalid.aliasIsUsed";
    public static final String CUSTOMER_MARKEDFOR_DELETION = "Customer.markedforDeletion";
    public static final String MOBILE_MAX_REACHED = "mobile.max.reached";
    public static final String DUPLICATE_REGISTRATION_ID = "duplicate.registrationId";
    public static final String CORPORATE_INVALID_SHOULD_HAVE_ONE_SERVICE = "corporate.invalid.shouldHaveOneService";
    public static final String SERVICE_INVALID_ALIAS_IS_USED = "service.invalid.aliasIsUsed";
    public static final String SERVICE_INVALID_NAME_IS_USED = "service.invalid.nameIsUsed";
    public static final String SYSTEM_IS_LOGGED_OUT = "SystemIsLoggedOut";
    public static final String SERVICE_UNDER_MODIFICATION = "service.under.modification";
    public static final String CORPORATE_INVALID_NOT_REGISTERED = "Corporate.invalid.notRegistered";
    public static final String SERVICE_INVALID_PENDING = "service.invalid.Pending";
    public static final String SERVICE_ACCOUNT_UNDER_MODIFICATION = "service.account.under.modification";
    public static final String SERVICE_MAX_REACHED = "service.max.reached";
    public static final String CORPORATE_SERVICE_ISREQUIRED = "Corporate.service.isrequired";
    public static final String CORPORATE_ISNOT_ACTIVE = "corporate.isnotActive";
    public static final String CORPORATE_SERVICE_ISNOT_ACTIVE = "corporate.service.isnotActive";
    public static final String AMOUNT_SHOULD_BE_GREATER_THAN_ZERO = "Amount.shouldBeGreaterThanZero";
    public static final String ACCOUNT_NOT_FOUND = "account.not.found";
    public static final String AMOUNT_SHOULD_BE_GREATER_THAN_MIN_BALANCE = "Amount.shouldBeGreaterThanMinBalance";
    public static final String CUSTOMER_INVALID_DOB_FUTURE = "customer.invalid.DOBFuture";
    public static final String CUSTOMER_INVALID_AGE = "customer.invalid.Age";
    public static final String DUPLICATE_CUSTOMER_ID = "duplicat.customer.item.id";
    public static final String INSUFFICIENT_FUNDS = "InsufficientFunds";
    public static final String SERVICE_LOGO_NOT_FOUND = "ServiceLogoNotFound";
    public static final String NO_FILE_UPLOADED = "NoFileUploaded";
    public static final String ALLOWED_PROFILE_IS_REQUIRED = "allowedProfileIsRequired";
    public static final String DELETE_PARENT_CORPORATE_NOT_ALLOWED = "delete.parent.corporate";

    private MPayErrorMessages() {
    }
}
