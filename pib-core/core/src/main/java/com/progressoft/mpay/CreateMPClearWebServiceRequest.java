package com.progressoft.mpay;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.apache.cxf.binding.soap.SoapHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.mpclear.MpClearIntegMsg;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class CreateMPClearWebServiceRequest implements Processor {

    private static final Logger logger = LoggerFactory
            .getLogger(CreateMPClearWebServiceRequest.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        logger.debug("inside CreateMPClearWebServiceRequest ........... ");

        MpClearIntegMsg msgLog = MessageSerializer.deSerializeIntegMsg(exchange
                .getIn().getBody().toString());
        exchange.setProperty("integMsgLog", msgLog);

        IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msgLog.getContent());
        isomsg.setField(
                7,
                new IsoValue<>(IsoType.DATE10, SystemHelper.formatDate(
                        SystemHelper.getCalendar().getTime(), "MMddHHmmss")));
        String contents = new String(isomsg.writeData());
        msgLog.setContent(contents);

        Calendar date = getSigningTimestamp();
        String signingDate = SystemHelper.formatDate(date.getTime(),
                SystemParameters.getInstance().getSigningDateFormat());

        String token = new MPayCryptographer().generateToken(SystemParameters.getInstance(), msgLog.getContent(),
                signingDate, IntegMessagesSource.SYSTEM);
        msgLog.setToken(token);

        msgLog.setSigningStamp(new Timestamp(date.getTimeInMillis()));

        exchange.setProperty("msgtype", msgLog.getContent().substring(0, 5));

        String request = "<ProcessMessage xmlns=\"http://www.progressoft.com/mpclear\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">"
                + "<message>"
                + msgLog.getContent()
                + "</message>"
                + "<agentId>"
                + msgLog.getRefSender()
                + "</agentId>"
                + "<token>"
                + msgLog.getToken()
                + "</token>"
                + "<date>"
                + SystemHelper.formatDate(msgLog.getSigningStamp(),
                SystemParameters.getInstance().getSigningDateFormat())
                + "</date>" + "</ProcessMessage>";

        logger.debug("*** Request");
        logger.debug(request);
        XmlConverter converter = new XmlConverter();
        List<Source> outElements = new ArrayList<>();

        @SuppressWarnings("deprecation")
        Document outDocument = converter.toDOMDocument(request);
        outElements.add(new DOMSource(outDocument.getDocumentElement()));
        CxfPayload<SoapHeader> responsePayload = new CxfPayload<>(
                null, outElements, null);
        exchange.getOut().setBody(responsePayload);
    }


    private Calendar getSigningTimestamp() {
        Calendar date = SystemHelper.getCalendar();
        date.set(Calendar.MILLISECOND, 0);
        return date;
    }
}