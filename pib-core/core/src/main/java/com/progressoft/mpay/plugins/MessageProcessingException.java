package com.progressoft.mpay.plugins;

public class MessageProcessingException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final String reasonCode;
	private final String reasonDescription;
	private final String processingStatus;
	private final boolean passErrorDescription;

	public MessageProcessingException(String reasonCode, String reasonDescription, String processingStatus, boolean passErrorDescription) {
		this.reasonCode = reasonCode;
		this.reasonDescription = reasonDescription;
		this.processingStatus = processingStatus;
		this.passErrorDescription = passErrorDescription;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public String getReasonDescription() {
		return reasonDescription;
	}

	public String getProcessingStatus() {
		return processingStatus;
	}

	public boolean isPassErrorDescription() {
		return passErrorDescription;
	}
}
