package com.progressoft.mpay.migs.mvc.controller;

public class MIGSParameters {

	public static final String VPC_TXN_RESPONSE_CODE = "vpc_TxnResponseCode";
	public static final String VPC_TRANSACTION_NO = "vpc_TransactionNo";
	public static final String VPC_RECEIPT_NO = "vpc_ReceiptNo";
	public static final String VPC_MESSAGE = "vpc_Message";
	public static final String VPC_CSC_RESULT_CODE = "vpc_CSCResultCode";
	public static final String VPC_CARD_NUM = "vpc_CardNum";
	public static final String VPC_CARD = "vpc_Card";
	public static final String VPC_BATCH_NO = "vpc_BatchNo";
	public static final String VPC_AVS_RESULT_CODE = "vpc_AVSResultCode";
	public static final String VPC_AVS_REQUEST_CODE = "vpc_AVSRequestCode";
	public static final String VPC_AUTHORIZE_ID = "vpc_AuthorizeId";
	public static final String VPC_ACQ_RESPONSE_CODE = "vpc_AcqResponseCode";
	public static final String VPC_ACQ_CSC_RESP_CODE = "vpc_AcqCSCRespCode";
	public static final String VPC_ACQ_AVS_RESP_CODE = "vpc_AcqAVSRespCode";
	public static final String VPC_SECURE_HASH = "vpc_SecureHash";
	public static final String VPC_VERSION = "vpc_Version";
	public static final String VPC_RETURN_URL = "vpc_ReturnURL";
	public static final String VPC_ORDER_INFO = "vpc_OrderInfo";
	public static final String VPC_MERCHANT = "vpc_Merchant";
	public static final String VPC_MERCH_TXN_REF = "vpc_MerchTxnRef";
	public static final String VPC_LOCALE = "vpc_Locale";
	public static final String VPC_COMMAND = "vpc_Command";
	public static final String VPC_AMOUNT = "vpc_Amount";
	public static final String VPC_ACCESS_CODE = "vpc_AccessCode";
	public static final String VPC_CURRENCY = "vpc_Currency";
	public static final String VPC_SECURE_HASH_TYPE = "vpc_SecureHashType";
	public static final String SZ = "SZ";
	public static final String AMOUNT_KIND = "Amount_kind";
	public static final String TITLE = "Title";

	public static final String INVALID_TOKEN = "Invalid Token";
	public static final String AMOUNT = "amount";
	public static final String MESSAGE_ID = "msgId";
	public static final String SERVICE_TYPE_CODE = "serviceTypeCode";
	public static final String CODE = "code";
	public static final String TOKEN = "token";
	public static final String CHECK_SUM_CODE = "checkSumCode";
	public static final String LANGUAGE = "lang";
	public static final String TENANT_ID = "tenantId";
	public static final String DEVICE_ID = "deviceId";
	public static final String JSON = "json";
	public static final String REDIRECT = "redirect:";
	public static final String ENGLISH_LANGUAGE_ID = "1";
	public static final String ARABIC_LANGUAGE_LOCALE = "ar_EG";
	public static final String ENGLISH_LANGUAGE_LOCALE = "en_US";
	public static final String MOBILE_NUMBER = "mobileNumber";
	public static final String OPERATION = "operation";
	public static final String NEW_VERSION = "new";
	public static final String MIGS_RESPONSE_SUCCESS_CODE = "0";
	public static final String SERVICE_UNAVAILABLE_ERROR = "Service Unavailable now please try later";
	public static final String REJECTED_HMTL_TAGS = "<html> <center> <head> Migs Transaction </head> <br><br><br> Your Transaction rejected <br><br> </center> </html>";
	public static final String ACCEPTED_HTML_TAGS = "<html> <center> <head> Migs Transaction </head> <br><br><br> Your Transaction proceeded successfully <br><br> </center> </html>";
	public static final String ERROR_DESCRIPTION = "errorDescription";
	public static final String ERROR_PAGE_TAGS_OPEN = "<html> <body> <center>";
	public static final String ERROR_PAGE_TAGS_CLOSE = "</center> </body> </html>";
	public static final String ERROR_PATH = "/migs/error.migs";
	public static final String EGP = "EGP";
	public static final String SHA256 = "SHA256";
	public static final String VPC_RISK_OVERALL_RESULT = "vpc_RiskOverallResult";

	private MIGSParameters() {

	}
}