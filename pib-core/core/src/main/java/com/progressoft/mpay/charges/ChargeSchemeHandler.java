package com.progressoft.mpay.charges;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ChargesScheme;

public class ChargeSchemeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(ChargeSchemeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {

		MPAY_ChargesScheme scheme = (MPAY_ChargesScheme) changeHandlerContext.getEntity();
		logger.debug("ChargeSchemeHandler  ----------------------------");

		scheme.setCurrency(SystemParameters.getInstance().getDefaultCurrency());

	}
}
