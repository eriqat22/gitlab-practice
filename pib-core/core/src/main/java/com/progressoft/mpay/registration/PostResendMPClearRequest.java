package com.progressoft.mpay.registration;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entities.MPAY_CashOut;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclear.IMPClearClient;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.mpclear.MPayCommunicatorHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;

public class PostResendMPClearRequest implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostResendMPClearRequest.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside PostResendMPClearRequest===========");

		String viewName = (String) arg0.get(WorkflowService.WF_ARG_VIEW_NAME);
		Object bean = arg0.get(WorkflowService.WF_ARG_BEAN);
		if (isRegistration(viewName, bean))
			return;
		if (viewName.equals(MPAYView.NETWORK_MANAGEMENT.viewName) || viewName.equals(MPAYView.CORPORATE_SERVICE_VIEWS.viewName)) {
			handleNetworkManagementResned(((MPAY_NetworkManagement) bean).getRefLastMsgLog());
		} else if (viewName.equals(MPAYView.CASH_IN.viewName) || viewName.equals(MPAYView.CORPORATE_SERVICE_VIEWS.viewName)) {
			MPAY_CashIn cashIn = (MPAY_CashIn) bean;
			cashIn.setRefLastMsgLog(handleFinancialResned(cashIn.getRefLastMsgLog()));
			DataProvider.instance().updateCashIn(cashIn);
		} else if (viewName.equals(MPAYView.CASH_OUT.viewName) || viewName.equals(MPAYView.CORPORATE_SERVICE_VIEWS.viewName)) {
			MPAY_CashOut cashOut = (MPAY_CashOut) bean;
			cashOut.setRefLastMsgLog(handleFinancialResned(cashOut.getRefLastMsgLog()));
			DataProvider.instance().updateCashOut(cashOut);
		} else if (viewName.equals(MPAYView.PENDING_MP_CLEAR_MESSAGES.viewName)) {
			MPAY_MpClearIntegMsgLog integMessage = (MPAY_MpClearIntegMsgLog) bean;
			handleGenericResend(integMessage);
		}
	}

	private boolean isRegistration(String viewName, Object bean) throws WorkflowException {
		if (isCustomer(viewName, bean))
			return true;
		if (viewName.equals(MPAYView.CORPORATE.viewName)) {
			handleRegistrationResned(((MPAY_Corporate) bean).getRefLastMsgLog());
			return true;
		} else if (viewName.equals(MPAYView.CORPORATE_SERVICES.viewName) || viewName.equals(MPAYView.CORPORATE_SERVICE_VIEWS.viewName)) {
			handleRegistrationResned(((MPAY_CorpoarteService) bean).getRefCorporate().getRefLastMsgLog());
			return true;
		} else if (viewName.equals(MPAYView.SERVICE_ACCOUNTS.viewName) || viewName.equals(MPAYView.SERVICE_ACCOUNTS_VIEW.viewName)) {
			handleRegistrationResned(((MPAY_ServiceAccount) bean).getService().getRefCorporate().getRefLastMsgLog());
			return true;
		}

		return false;
	}

	private boolean isCustomer(String viewName, Object bean) throws WorkflowException {
		if (viewName.equals(MPAYView.CUSTOMER.viewName)) {
			handleRegistrationResned(((MPAY_Customer) bean).getRefLastMsgLog());
			return true;
		} else if (viewName.equals(MPAYView.CUSTOMER_MOBILES.viewName) || viewName.equals(MPAYView.CUSTOMER_MOBILE_VIEWS.viewName)) {
			handleRegistrationResned(((MPAY_CustomerMobile) bean).getRefCustomer().getRefLastMsgLog());
			return true;
		} else if (viewName.equals(MPAYView.MOBILE_ACCOUNTS.viewName) || viewName.equals(MPAYView.MOBILE_ACCOUNTS_VIEW.viewName)) {
			handleRegistrationResned(((MPAY_MobileAccount) bean).getMobile().getRefCustomer().getRefLastMsgLog());
			return true;
		}
		return false;
	}

	private void handleGenericResend(MPAY_MpClearIntegMsgLog integMessage) throws WorkflowException {
		if (integMessage.getIntegMsgType().equals(Integer.toHexString(MPClearIntegrationMessageTypes.NETWORK_MANAGEMENT_REQUEST)))
			handleNetworkManagementResned(integMessage);
		else if (integMessage.getIntegMsgType().equals(Integer.toHexString(MPClearIntegrationMessageTypes.REGISTRATION_REQUEST)))
			handleRegistrationResned(integMessage);
		else if (integMessage.getIntegMsgType().equals(Integer.toHexString(MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST)) || integMessage.getIntegMsgType().equals(Integer.toHexString(MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)))
			handleFinancialResned(integMessage);
		else
			throw new WorkflowException("Resend for this message not allowed");
	}

	private void handleRegistrationResned(MPAY_MpClearIntegMsgLog msg) throws WorkflowException {
		MPAY_MpClearIntegMsgLog retry = ClientManagement.cloneMessageLogForSuspectDuplicate(getCoreComponents(), msg);
		retry.setRefMessage(msg.getRefMessage());
		boolean isCustomer = msg.getRefLastMsgLogCorporates() == null || msg.getRefLastMsgLogCorporates().isEmpty();
		try {
			cancelPendingMessages(msg.getMessageId());
			MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).processRegistration(retry, isCustomer);
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}

	private void handleNetworkManagementResned(MPAY_MpClearIntegMsgLog msg) throws WorkflowException {
		MPAY_MpClearIntegMsgLog retry = ClientManagement.cloneMessageLogForSuspectDuplicate(getCoreComponents(), msg);
		try {
			cancelPendingMessages(msg.getMessageId());
			MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).processNetworkMessage	(retry);
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}

	private MPAY_MpClearIntegMsgLog handleFinancialResned(MPAY_MpClearIntegMsgLog msg) throws WorkflowException {
		IMPClearClient client = MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor());
		MPAY_MpClearIntegMsgLog retry = ClientManagement.cloneMessageLogForSuspectDuplicate(getCoreComponents(), msg);
		try {
			cancelPendingMessages(msg.getMessageId());
			if (msg.getIntegMsgType().equals(IntegMessageTypes.TYPE_1200)) {
				retry.setRefMessage(msg.getRefMessage());
				client.processOnlineFinancialRequest(retry, MPayCommunicatorHelper.getCommunicator());
			} else if (msg.getIntegMsgType().equals(IntegMessageTypes.TYPE_1220)) {
				retry.setRefMessage(msg.getRefMessage());
				client.processOfflineFinancialRequest(retry, MPayCommunicatorHelper.getCommunicator());
			} else
				throw new WorkflowException("Integration message type not supported: IntegMsgType = " + msg.getIntegMsgType());
			return retry;
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}

	private void cancelPendingMessages(String messageId) {
		List<MPAY_MpClearIntegMsgLog> messages = DataProvider.instance().getPendingMPClearMessages(messageId);
		if (messages == null || messages.isEmpty())
			return;
		for (MPAY_MpClearIntegMsgLog message : messages) {
			message.setIsCanceled(true);
			DataProvider.instance().mergeMPClearMessageLog(message);
		}
	}

	private static CoreComponents getCoreComponents() {
		return new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance());
	}
}