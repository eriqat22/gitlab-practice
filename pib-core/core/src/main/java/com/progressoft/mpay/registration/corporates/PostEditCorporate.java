package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.AppException;
import com.progressoft.jfw.model.bussinessobject.attachments.AttachmentItem;
import com.progressoft.jfw.model.bussinessobject.core.AbstractJFWEntity;
import com.progressoft.jfw.model.messaging.channels.JFWMessageChannel;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.push.message.JFWInfoMessage;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_CorporateAtt;
import com.progressoft.mpay.entity.CorporateMetaData;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class PostEditCorporate implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostEditCorporate.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {

        try {
            logger.debug("Inside execute =====");
            MPAY_Corporate originalCorporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN_ORIGINAL);
            MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
            //saveLogo(corporate);
            if (corporate.getIsRegistered())
                presistApprovedData(corporate, originalCorporate);
        }catch (Exception ex) {
            throw new WorkflowException(ex.getMessage(), ex);
        }
    }

    private void saveLogo(MPAY_Corporate corporate) throws IOException {
        List<MPAY_CorporateAtt> attachments = MPayContext.getInstance().getDataProvider()
                .getAttachmentsWhere(MPAY_CorporateAtt.class, "recordId = '" + corporate.getId() + "'");
        MPAY_CorporateAtt attachment = Collections.max(attachments, Comparator.comparing(AbstractJFWEntity::getCreationDate));
        String imagesPath = MPayContext.getInstance().getSystemParameters().getImagePath();
        if (attachment != null && imagesPath != null)
            FileUtils.writeByteArrayToFile(new File(Paths.get(imagesPath).resolve(attachment.getName()).toUri()), attachment.getAttFile());
    }

    private void presistApprovedData(MPAY_Corporate corporate, MPAY_Corporate originalCorporate) {
        CorporateMetaData corporateMetaData = new CorporateMetaData(originalCorporate);
        corporate.setApprovedData(corporateMetaData.toString());
    }


}