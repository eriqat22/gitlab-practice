package com.progressoft.mpay.migs.mvc.controller;

import java.util.HashMap;
import java.util.Map;

public class MigsHandlerFactory {

	static final String REQUEST = "REQUEST";
	static final String RESPONSE = "RESPONSE";
	static final String ERROR = "ERROR";

	static Map<String, MigsHandler> types = new HashMap<>();

	private MigsHandlerFactory() {

	}

	public static MigsHandler getMigsHandler(String request) {
		initializeTypes();
		if (types.containsKey(request))
			return types.get(request);
		else
			throw new MigsException("Not Supported Message");

	}

	private static void initializeTypes() {
		types.put(REQUEST, new MigsRequestHandler());
		types.put(RESPONSE, new MigsResponseHandler());
		types.put(ERROR, new MigsErrorHandler());
	}

}
