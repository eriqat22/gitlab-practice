package com.progressoft.mpay.migs.mvc.controller;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.migs.request.MigsRequest;
import com.progressoft.mpay.migs.response.MigsResponse;

public class MigsMarshaller {

	public String marshallMigsObject(Object object) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MigsRequest.class, MigsResponse.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			StringWriter writer = new StringWriter();
			marshaller.marshal(object, writer);
			String xml = writer.toString();
			writer.close();
			return xml;
		} catch (Exception e) {
			throw new MPayGenericException("Error while marshallMigsObject", e);
		}
	}
}
