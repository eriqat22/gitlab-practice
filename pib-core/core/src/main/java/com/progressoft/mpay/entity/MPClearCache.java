package com.progressoft.mpay.entity;

public class MPClearCache {

	Long id;
	String registrationId;
	long clientType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public long getClientType() {
		return clientType;
	}

	public void setClientType(long clientType) {
		this.clientType = clientType;
	}

}
