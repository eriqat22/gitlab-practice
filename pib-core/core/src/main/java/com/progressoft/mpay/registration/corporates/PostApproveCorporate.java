package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclear.MPClearClientHelper;

public class PostApproveCorporate implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostApproveCorporate.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("Inside execute =====");
        MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
        if (!corporate.getIsRegistered()) {
            MPAY_CorpoarteService service = getPendingService(corporate);
            if (service == null)
                throw new WorkflowException("Corporate doesn't have pending service");
            JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.APPROVE, new WorkflowException());
        } else {
            boolean hasPendingService = true;
            MPAY_CorpoarteService service = getPendingService(corporate);
            if (service == null) {
                hasPendingService = false;
                service = getFirstRegisteredService(corporate);
            }
            if (service == null)
                throw new WorkflowException("Corproate doesn't have registered service");
            if (hasPendingService)
                JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.APPROVE, new WorkflowException());
            MPAY_ServiceAccount account = handlePendingAccounts(corporate);
            if (!hasPendingService)
                handleUpdateClient(corporate, service, account);
        }
    }

    private MPAY_ServiceAccount getFirstApprovedAccount(MPAY_CorpoarteService service) {
        logger.debug("inside getFirstApprovedAccount ============");
        List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
        if (accounts == null || accounts.isEmpty())
            return null;

        return selectFirst(accounts, having(on(MPAY_ServiceAccount.class).getIsRegistered(), Matchers.equalTo(true)));
    }

    private void handleUpdateClient(MPAY_Corporate corporate, MPAY_CorpoarteService service, MPAY_ServiceAccount account) throws WorkflowException {
        logger.debug("inside handleUpdateClient ============");
        MPAY_ServiceAccount innerAccount = account;
        if (innerAccount == null)
            innerAccount = getFirstApprovedAccount(service);
        if (innerAccount == null)
            throw new WorkflowException("Corproate doesn't have registered account");
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).updateCorporate(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()),
        		corporate, service, innerAccount);
    }

    private MPAY_ServiceAccount handlePendingAccounts(MPAY_Corporate corporate) throws WorkflowException {
        logger.debug("inside handlePendingAccounts ============");
        List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId());
        if (services == null || services.isEmpty())
            return null;
        Iterator<MPAY_CorpoarteService> servicesIterator = services.iterator();

        do {
            MPAY_CorpoarteService service = servicesIterator.next();
            List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
            MPAY_ServiceAccount account = selectFirst(accounts, having(on(MPAY_ServiceAccount.class).getStatusId().getCode(), Matchers.equalTo(ServiceAccountWorkflowStatuses.APPROVAL.toString())));
            if (account != null) {
                JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.APPROVE, new WorkflowException());
                return account;
            }
        } while (servicesIterator.hasNext());
        return null;
    }

    private MPAY_CorpoarteService getPendingService(MPAY_Corporate corporate) throws WorkflowException {
        logger.debug("inside getPendingService ============");
        List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId(), CorporateServiceWorkflowStatuses.APPROVAL.toString());
        if (services == null || services.isEmpty())
            return null;
        if (services.size() > 1)
            throw new WorkflowException("Corporate has more than one modified service");
        return services.get(0);
    }

    private MPAY_CorpoarteService getFirstRegisteredService(MPAY_Corporate corporate) throws WorkflowException {
        logger.debug("inside getFirstRegisteredService ============");
        List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId());
        if (services == null || services.isEmpty())
            return null;

        return selectFirst(services, having(on(MPAY_CorpoarteService.class).getIsRegistered(), Matchers.equalTo(true)));
    }
}