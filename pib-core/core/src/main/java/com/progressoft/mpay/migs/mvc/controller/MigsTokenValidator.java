package com.progressoft.mpay.migs.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import com.progressoft.mpay.HashingAlgorithm;
import com.progressoft.mpay.entities.MPAY_AppsChecksum;

public class MigsTokenValidator extends TokenValidator {

	public MigsTokenValidator(MigsContext context) {
		super(context);
	}

	@Override
	public boolean isValidToken(HttpServletRequest request) {
		String valuesToHash = prepareValuesToCheckFromRequest(request);
		if (isEmptyToken(request))
			return false;
		String hashValue = HashingAlgorithm.hash(context.getSystemParameters(), valuesToHash);
		return hashValue.compareTo(request.getParameter(MIGSParameters.TOKEN).replace(" ", "+")) == 0;
	}

	private boolean isEmptyToken(HttpServletRequest request) {
		return request.getParameter(MIGSParameters.TOKEN) == null || request.getParameter(MIGSParameters.TOKEN).trim().isEmpty();
	}

	private String prepareValuesToCheckFromRequest(HttpServletRequest request) {
		try {
			return new StringBuilder().append(request.getParameter(MIGSParameters.AMOUNT))
					.append(getCheckSumFromSystem(request.getParameter(MIGSParameters.CHECK_SUM_CODE)))
					.append(request.getParameter(MIGSParameters.LANGUAGE)).append(request.getParameter(MIGSParameters.MOBILE_NUMBER))
					.append(request.getParameter(MIGSParameters.MESSAGE_ID)).append(request.getParameter(MIGSParameters.OPERATION))
					.append(request.getParameter(MIGSParameters.SERVICE_TYPE_CODE)).append(request.getParameter(MIGSParameters.TENANT_ID)).toString();
		} catch (Exception exception) {
			throw new MigsException(exception);
		}
	}

	private String getCheckSumFromSystem(String checkSumCode) {
		MPAY_AppsChecksum checkSum = getCheckSumFromCode(checkSumCode);
		return checkSum == null ? "" : checkSum.getChecksum();
	}

	private MPAY_AppsChecksum getCheckSumFromCode(String checkSumCode) {
		MPAY_AppsChecksum checksum = context.getItemDao().getItem(MPAY_AppsChecksum.class, MIGSParameters.CODE, checkSumCode);
		return checksum != null ? checksum : null;
	}
}
