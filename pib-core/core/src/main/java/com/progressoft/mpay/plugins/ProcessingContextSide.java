package com.progressoft.mpay.plugins;

import java.math.BigDecimal;

import com.progressoft.mpay.AccountLimits;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_ClientsCommission;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class ProcessingContextSide {
	private MPAY_CustomerMobile mobile;
	private MPAY_MobileAccount mobileAccount;
	private MPAY_Customer customer;
	private MPAY_CorpoarteService service;
	private MPAY_ServiceAccount serviceAccount;
	private MPAY_Corporate corporate;
	private MPAY_Bank bank;
	private MPAY_Account account;
	private MPAY_Profile profile;
	private BigDecimal charge;
	private BigDecimal tax;
	private boolean isBanked;
	private String info;
	private AccountLimits limits;
	private MPAY_ClientsCommission commission;
	private String notes;

	public ProcessingContextSide(){
		this.charge = BigDecimal.ZERO;
		this.tax = BigDecimal.ZERO;
		this.isBanked = false;
	}

	public MPAY_CustomerMobile getMobile() {
		return mobile;
	}
	public void setMobile(MPAY_CustomerMobile mobile) {
		this.mobile = mobile;
	}
	public MPAY_MobileAccount getMobileAccount() {
		return mobileAccount;
	}

	public void setMobileAccount(MPAY_MobileAccount mobileAccount) {
		this.mobileAccount = mobileAccount;
	}

	public MPAY_Customer getCustomer() {
		return customer;
	}
	public void setCustomer(MPAY_Customer customer) {
		this.customer = customer;
	}
	public MPAY_CorpoarteService getService() {
		return service;
	}
	public void setService(MPAY_CorpoarteService service) {
		this.service = service;
	}
	public MPAY_ServiceAccount getServiceAccount() {
		return serviceAccount;
	}

	public void setServiceAccount(MPAY_ServiceAccount serviceAccount) {
		this.serviceAccount = serviceAccount;
	}

	public MPAY_Corporate getCorporate() {
		return corporate;
	}
	public void setCorporate(MPAY_Corporate corporate) {
		this.corporate = corporate;
	}
	public MPAY_Bank getBank() {
		return bank;
	}
	public void setBank(MPAY_Bank bank) {
		this.bank = bank;
	}
	public MPAY_Account getAccount() {
		return account;
	}
	public void setAccount(MPAY_Account account) {
		this.account = account;
	}
	public BigDecimal getCharge() {
		return charge;
	}
	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}
	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public MPAY_Profile getProfile() {
		return profile;
	}
	public void setProfile(MPAY_Profile profile) {
		this.profile = profile;
	}
	public boolean isBanked() {
		return isBanked;
	}
	public void setBanked(boolean isBanked) {
		this.isBanked = isBanked;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public AccountLimits getLimits() {
		return limits;
	}

	public void setLimits(AccountLimits limits) {
		this.limits = limits;
	}

	public MPAY_ClientsCommission getCommission() {
		return commission;
	}

	public void setCommission(MPAY_ClientsCommission commission) {
		this.commission = commission;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
