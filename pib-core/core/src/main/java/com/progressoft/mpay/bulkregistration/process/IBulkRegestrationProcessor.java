package com.progressoft.mpay.bulkregistration.process;

import com.progressoft.mpay.entities.MPAY_BulkRegistration;

@FunctionalInterface
public interface IBulkRegestrationProcessor {
	void process(MPAY_BulkRegistration bulkRegistration);

}
