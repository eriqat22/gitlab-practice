package com.progressoft.mpay.registration.customers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class NotificationBalanceThresholdHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(NotificationBalanceThresholdHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("***inside notification balance threshold handler: " + changeHandlerContext.getSource());

		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) changeHandlerContext.getEntity();

		boolean isEnabled = mobile.getBalanceThreshold() > 0;
		changeHandlerContext.getFieldsAttributes().get("dailyBalanceTimeFrm").setEnabled(isEnabled);
		changeHandlerContext.getFieldsAttributes().get("dailyBalanceTimeTo").setEnabled(isEnabled);
	}
}
