package com.progressoft.mpay.notifications;

import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SmsNotificationType;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_NotificationTemplate;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.exceptions.NotificationException;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.NotificationContext;
import com.progressoft.mpay.registration.customers.CustomerRegistrationNotificationContext;
import org.apache.commons.lang.NullArgumentException;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Objects;

public final class NotificationHelper implements INotificationHelper {
    private static final Logger logger = LoggerFactory.getLogger(NotificationHelper.class);
    private static NotificationHelper instance;

    private VelocityEngine ve;
    private VelocityContext context;

    public NotificationHelper() {
        try {
            ve = new VelocityEngine();
            ve.init();
            context = new VelocityContext();
        } catch (Exception e) {
            throw new NotificationException("Error while initializing NotificationHelper", e);
        }
    }

    public static NotificationHelper getInstance() {
        logger.debug("Inside getInstance ...");
        if (instance == null)
            instance = new NotificationHelper();

        return instance;
    }

    public static void addNotification( MPAY_MPayMessage message, List<MPAY_Notification> notifications, String receiverMobile, MPAY_Language language,
                                        Boolean channelEnabled, NotificationContext notificationContext, long notificationType, String channel ) {
        if (channelEnabled != null && Objects.nonNull(receiverMobile)) {
        	MPAY_Notification notification = NotificationHelper.getInstance().createNotification(message, notificationContext, notificationType,
        			message.getRefOperation().getId(), language.getId(), receiverMobile, channel);
        	if(notification != null)
        		notifications.add(notification);
        }
    }

    @Override
    public MPAY_Notification createNotification( MPAY_MPayMessage message, NotificationContext notificationContext,
                                                 long typeId, long operationId, long languageId, String receiver, String channelCode ) {
        try {
            logger.debug("Inside CreateNotification ...");
            MPAY_NotificationTemplate template = LookupsLoader.getInstance().getNotificationTemplate(operationId,
                    typeId, languageId);
            if (template == null)
                return null;
            String notificationBody = evaulateVelocityTemplate(notificationContext, template);
            return prepareMpayNotification(message, notificationContext, receiver, channelCode, notificationBody, false);
        } catch (Exception e) {
            logger.error("Error in CreateNotification", e);
            return null;
        }
    }

    private MPAY_Notification prepareMpayNotification( MPAY_MPayMessage message, NotificationContext notificationContext, String receiver, String channelCode, String notificationBody, boolean isRegistrationNotification ) {
        MPAY_Notification notification = new MPAY_Notification();
        if (message != null)
            notification.setRefMessage(message);
        if (isRegistrationNotification) {
            CustomerRegistrationNotificationContext customerRegistrationNotificationContext = (CustomerRegistrationNotificationContext) notificationContext;
            String pinCode = customerRegistrationNotificationContext.getPinCode();
            if (pinCode != null && !pinCode.isEmpty())
                notification.setExtraData1(pinCode);
        } else
            notification.setExtraData1(notificationContext.getExtraData1());
        notification.setExtraData2(notificationContext.getExtraData2());
        notification.setExtraData3(notificationContext.getExtraData3());
        notification.setReceiver(receiver);
        notification.setContent(notificationBody);
        notification.setRefChannel(LookupsLoader.getInstance().getNotificationChannel(channelCode));
        return notification;
    }

    private String evaulateVelocityTemplate( NotificationContext notificationContext, MPAY_NotificationTemplate template )
            throws IOException {
        StringWriter body = new StringWriter();
        context.put("context", notificationContext);
        ve.evaluate(context, body, "PAYMSG", template.getTemplate());
        return body.toString();
    }

    public void createRegistrationNotification( MPAY_MPayMessage message, NotificationContext notificationContext,
                                                long typeId, long operationId, long languageId, String receiver, String channelCode ) {
        try {
            logger.debug("Inside CreateNotification ...");
            MPAY_NotificationTemplate template = LookupsLoader.getInstance().getNotificationTemplate(operationId,
                    typeId, languageId);
            if (template == null)
                return;


            String notificationBody = evaulateVelocityTemplate(notificationContext, template);
            MPAY_Notification notification = prepareMpayNotification(message, notificationContext, receiver,
                    channelCode, notificationBody, true);
            DataProvider.instance().persistNotification(notification);
            sendToActiveMQ(notification, SmsNotificationType.REGISTRATION);
        } catch (Exception e) {
            logger.error("Error While create Registration Notification ", e);
        }
    }

    @Override
    public void sendToActiveMQ( MPAY_Notification notification, SmsNotificationType smsNotificationType ) {
        logger.debug("Inside SendToActiveMQ ...");
        JfwHelper.sendToActiveMQ(notification, notification.getTenantId(), MPAYView.NOTIFICATIONS.viewName,
                smsNotificationType.getQueueName());
    }

    @Override
    public void sendNotifications( MessageProcessingResult result ) {
        logger.debug("Inside SendNotifications...");
        if (result.getNotifications() == null || result.getNotifications().isEmpty())
            return;
        for (MPAY_Notification notification : result.getNotifications()) {
            try {
                DataProvider.instance().persistNotification(notification);
                sendToActiveMQ(notification, SmsNotificationType.GENERAL);
            } catch (Exception e) {
                if (notification != null) {
                    notification.setSuccess(false);
                    DataProvider.instance().updateNotification(notification);
                }
                logger.error("Error in SendNotifications", e);
            }

        }
    }

    @Override
    public void sendNotifications( MPAY_Notification notification ) {
        logger.debug("Inside SendNotifications ...");
        if (notification == null)
            throw new NullArgumentException("notification");
        try {
            sendToActiveMQ(notification, SmsNotificationType.GENERAL);
        } catch (Exception e) {
            logger.error("Error in SendNotifications", e);
        }
        notification.setSuccess(false);
        DataProvider.instance().updateNotification(notification);
    }
}
