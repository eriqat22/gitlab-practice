package com.progressoft.mpay.networkmanagement;

import java.util.Date;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class NetworkManagement {
    public static final int DATE_OF_BIRTH_LEN = 16;

    private NetworkManagement() {
    }

    public static IsoMessage createLoginMessage() throws WorkflowException {
        return createNetworkManagementMessage("LOGIN");
    }

    public static IsoMessage createLogoutMessage() throws WorkflowException {
        return createNetworkManagementMessage("LOGOUT");
    }

    private static IsoMessage createNetworkManagementMessage(String actoinName) throws WorkflowException {
        IsoMessage message = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).newMessage(0x1800);

        String messageID = DataProvider.instance().getNextMPClearMessageId();

        message.setIsoHeader("");
        message.setBinary(false);
        message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime())));
        message.setField(48, new IsoValue<>(IsoType.LLLVAR, actoinName));
        message.setField(111, new IsoValue<>(IsoType.LLLVAR, MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).generateSecurityKey()));
        message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID));

        return message;
    }

    private static String formatIsoDate(Date date) {
        return SystemHelper.formatDate(date, "MMddHHmmss");
    }
}