package com.progressoft.mpay.registration.corporates;

import static com.progressoft.mpay.MPayErrorMessages.SERVICE_LOGO_NOT_FOUND;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CorpoarteServiceAtt;
import com.progressoft.mpay.entities.MPAY_Corporate;

@Component
@SuppressWarnings({ "rawtypes", "deprecation" })
public class ValidateCorpHasServiceWithLogo implements Validator {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(ValidateCorpHasServiceWithLogo.class);

    @Override
    public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside ValidateCorpHasServiceWithLogo -----");
        MPAY_Corporate mpay_corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
        for (MPAY_CorpoarteService service : mpay_corporate.getRefCorporateCorpoarteServices()) {
            String where = "recordId=" + service.getId().toString() + " AND imageType='1'";
            Long itemCount = itemDao.getItemCount(MPAY_CorpoarteServiceAtt.class, null, where);
            if (itemCount == 0) {
                InvalidInputException iie = new InvalidInputException();
                iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(SERVICE_LOGO_NOT_FOUND));
                throw iie;
            }
        }
    }
}
