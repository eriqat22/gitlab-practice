package com.progressoft.mpay.messages;

public class NotificationType {
	public static final long ACCEPTED_SENDER = 1;
	public static final long REJECTED_SENDER = 2;
	public static final long ACCEPTED_RECEIVER = 3;
	public static final long REJECTED_RECEIVER = 4;
	public static final long TRANSACTION_REVERSAL = 5;

	private NotificationType() {

	}
}
