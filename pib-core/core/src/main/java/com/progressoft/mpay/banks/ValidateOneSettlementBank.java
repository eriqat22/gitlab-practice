package com.progressoft.mpay.banks;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entity.BankType;

public class ValidateOneSettlementBank implements Validator {

	@Autowired
	private ItemDao itemDao;
	private static final Logger logger = LoggerFactory.getLogger(ValidateOneSettlementBank.class);

	@SuppressWarnings({ "rawtypes", })
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {

		logger.debug("***inside ValidateOneSettlementBank");

		MPAY_Bank bank = (MPAY_Bank) arg0.get(WorkflowService.WF_ARG_BEAN);

		if (!BankType.SETTLEMENT.equals(bank.getSettlementParticipant()))
			return;

		InvalidInputException iie = Unique.validate(itemDao, bank, "settlementParticipant", BankType.SETTLEMENT,
				AppContext.getMsg("bank.OneSettlementCanbedefined"));
		if (iie.getErrors().size() > 0)
			throw iie;
	}
}