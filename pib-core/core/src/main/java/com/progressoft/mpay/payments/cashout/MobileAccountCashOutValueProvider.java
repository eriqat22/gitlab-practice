package com.progressoft.mpay.payments.cashout;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.registration.ClientManagement;

public class MobileAccountCashOutValueProvider {

	@Autowired
	private ItemDao itemDao;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<Object, Object> getMobileAccounts(String mobileId) {
		Map<Object, Object> accounts = new HashMap();
		if (mobileId != null && mobileId.length() > 0) {
			MPAY_CustomerMobile mobile = itemDao.getItem(MPAY_CustomerMobile.class, mobileId);
			for (MPAY_MobileAccount account : mobile.getMobileMobileAccounts()) {
				if (account.getBankedUnbanked().equals(BankedUnbankedFlag.UNBANKED)) {
					accounts.put(ClientManagement.getRegistrationId(account),
							ClientManagement.getRegistrationId(account));
				}
			}
		}

		return accounts;
	}
}