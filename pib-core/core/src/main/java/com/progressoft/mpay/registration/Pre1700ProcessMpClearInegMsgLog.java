package com.progressoft.mpay.registration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CustIntegActionType;
import com.progressoft.mpay.entities.MPAY_CustIntegMessage;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgType;
import com.progressoft.mpay.entity.CustomerManagementActionTypes;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoValue;

public class Pre1700ProcessMpClearInegMsgLog implements FunctionProvider {
	private static final Logger logger = LoggerFactory.getLogger(Pre1700ProcessMpClearInegMsgLog.class);

	@Autowired
	private ItemDao itemDao;

	@SuppressWarnings({ "rawtypes" })
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("Pre1700ProcessMpClearInegMsgLog++++++++++++++++++++++++++++");
		try {
			MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);

			MPAY_CustIntegMessage integMsg = new MPAY_CustIntegMessage();
			integMsg.setRefMsgLog(msg);

			IsoMessage m = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());
			MPAY_Customer customer = getCustomer(m);

			MPAY_CustomerMobile mobile = getmobile(m);
			integMsg.setRefCustomer(customer);
			integMsg.setRefMobile(mobile);
			MPAY_MpClearIntegMsgType msgType = new MPAY_MpClearIntegMsgType();
			msgType.setCode(IntegMessageTypes.TYPE_1700);

			integMsg.setIntegMsgType(msgType);

			integMsg.setRefMessage(m.getField(115).toString());
			integMsg.setActionType(setActionType(m));

			JfwHelper.createEntity(MPAYView.CUST_1700_REG_MSGS, integMsg);

		} catch (Exception e) {
			throw new WorkflowException(e);
		}

	}

	private MPAY_CustIntegActionType setActionType(IsoMessage m) {
		IsoValue<Object> at = m.getField(48);
		CustomerManagementActionTypes x = CustomerManagementActionTypes.valueOf(at.toString());
		MPAY_CustIntegActionType cat = new MPAY_CustIntegActionType();
		cat.setCode(x.name());
		return cat;
	}

	private MPAY_CustomerMobile getmobile(IsoMessage m) {
		String account1 = MPClearHelper.getEncodedString(m, 62);
		String mobilenumber = account1.substring(SystemParameters.getInstance().getRoutingCodeLength() + SystemParameters.getInstance().getMobileAccountSelectorLength() + 1);
		return DataProvider.instance().getCustomerMobile(mobilenumber);
	}

	private MPAY_Customer getCustomer(IsoMessage message) {
		// IDVALUE
		String customerID = MPClearHelper.getEncodedString(message, 110);

		// nationality
		IsoValue<Object> customerType = message.getField(111);

		Filter customerIDFilter = new Filter();
		customerIDFilter.setOperator(Operator.EQ);
		customerIDFilter.setProperty("idNum");
		customerIDFilter.setFirstOperand(customerID);

		Filter customerIDtypeFilter = new Filter();
		customerIDtypeFilter.setOperator(Operator.EQ);
		customerIDtypeFilter.setProperty("idType");
		customerIDtypeFilter.setFirstOperand(customerType);

		List<Filter> filters = new ArrayList<>();
		filters.add(customerIDtypeFilter);
		filters.add(customerIDFilter);

		List<MPAY_Customer> customers = itemDao.getItems(MPAY_Customer.class, null, filters, null, null);
		return customers.get(0);
	}

}
