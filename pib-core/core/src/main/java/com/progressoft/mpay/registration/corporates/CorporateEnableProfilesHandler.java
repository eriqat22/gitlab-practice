package com.progressoft.mpay.registration.corporates;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.ClientTypes;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CorporateEnableProfilesHandler implements ChangeHandler {
    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        MPAY_Corporate corporate = (MPAY_Corporate) changeHandlerContext.getEntity();
        MPAY_ClientType clientType = corporate.getClientType();
        if (Objects.isNull(clientType))
            return;

        boolean isAgent = clientType.getCode().equals(ClientTypes.AGENT_CODE);
        changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.ALLOWED_PROFILE).setEnabled(isAgent);
    }
}
