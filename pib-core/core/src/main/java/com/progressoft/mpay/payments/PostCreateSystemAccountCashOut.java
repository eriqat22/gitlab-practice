package com.progressoft.mpay.payments;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_SystemAccountsCashOut;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;

public class PostCreateSystemAccountCashOut implements FunctionProvider {

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		MPAY_SystemAccountsCashOut cashOut = (MPAY_SystemAccountsCashOut) arg0.get(WorkflowService.WF_ARG_BEAN);
		SystemAccountsCashOutHelper.setDestinationData(cashOut);
		cashOut.setReason(MPayContext.getInstance().getLookupsLoader().getReason(ReasonCodes.VALID));
		cashOut.setProcessingStatus(MPayContext.getInstance().getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
	}
}
