package com.progressoft.mpay.banks;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Bank;

public class CheckIfBankHasClients implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfBankHasClients.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		return !DataProvider.instance().checkIfBankHasActiveAccounts(((MPAY_Bank)transientVar.get(WorkflowService.WF_ARG_BEAN)).getId(), LookupsLoader.getInstance().getPSP().getId());
	}
}

