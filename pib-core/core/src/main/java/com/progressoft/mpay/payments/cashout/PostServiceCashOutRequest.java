package com.progressoft.mpay.payments.cashout;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.payments.CashMessage;
import com.progressoft.mpay.payments.CashRequestHelper;
import com.progressoft.mpay.payments.PaymentsOperations;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.psp.PSP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

public class PostServiceCashOutRequest implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostServiceCashOutRequest.class);

    @SuppressWarnings({"rawtypes"})
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside execute =======================");
        MPAY_ServiceCashOut cashOut = (MPAY_ServiceCashOut) arg0.get(WorkflowService.WF_ARG_BEAN);

        try {
            MPayRequest request = new MPayRequest(PaymentsOperations.SERVICE_CASH_OUT, cashOut.getRefCorporateService().getName(), ReceiverInfoType.CORPORATE, null,
                    LanguageMapper.getInstance().getLanguageLongByLocaleCode(SystemParameters.getInstance().getSystemLanguage().getCode()), UUID.randomUUID().toString(), cashOut.getTenantId());
            request.getExtraData().add(new ExtraData(CashMessage.AMOUNT_KEY, cashOut.getAmount().toString()));
            request.getExtraData().add(new ExtraData(CashMessage.CHARGE_AMOUNT_KEY, cashOut.getChargeAmount().toString()));
            request.getExtraData().add(new ExtraData(CashMessage.RECEIVER_KEY, getPSPCashOutService(cashOut.getRefCorporateService()).getName()));
            request.getExtraData().add(new ExtraData(CashMessage.RECEIVER_TYPE_KEY, ReceiverInfoType.CORPORATE));
            request.getExtraData().add(new ExtraData(CashMessage.SENDER_ACCOUNT_KEY, cashOut.getRefServiceAccount()));
            request.getExtraData().add(new ExtraData(CashMessage.NOTES_KEY, cashOut.getComments()));
            MessageProcessingContext context = new MessageProcessingContext(DataProvider.instance(), LookupsLoader.getInstance(), SystemParameters.getInstance(), new MPayCryptographer(),
                    NotificationHelper.getInstance());
            context.setRequest(request);
            context.setOriginalRequest(request.toString());
            context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
            context.setOperation(LookupsLoader.getInstance().getEndPointOperation(request.getOperation()));
            MessageProcessor processor = MessageProcessorHelper.getProcessor(context.getOperation());
            if (processor == null)
                throw new WorkflowException("Service Cash Out processor not available");

            MPAY_BulkPayment bulkPayment = context.getDataProvider().getBulkPayment(context.getSystemParameters().getBulkPaymentDefaultId());
            if (bulkPayment == null)
                throw new WorkflowException("Default Bulk Payment ID Not Found");
            MPAY_MPayMessage message = MessageProcessorHelper.createMessage(new ServiceUserManager(), context, ProcessingStatusCodes.PENDING, null, null, bulkPayment);
            ArrayList<MPAY_ServiceCashOut> serviceCashOuts = new ArrayList<>();
            serviceCashOuts.add(cashOut);
            message.setRefMessageServiceCashOut(serviceCashOuts);
            cashOut.setRefMessage(message);
            context.getExtraData().put("cashOut", cashOut);
            DataProvider.instance().persistMPayMessage(message);
            context.setMessage(message);
            MessageProcessingResult result = processor.processMessage(context);
            cashOut.setProcessingStatus(result.getMessage().getProcessingStatus());
            cashOut.setReason(result.getMessage().getReason());
            cashOut.setReasonDesc(result.getMessage().getReasonDesc());
            cashOut.setRefMessage(message);
            String response = CashRequestHelper.generateResponse(Long.parseLong(result.getMessage().getReference()), result.getMessage().getProcessingStatus().getCode(),
                    result.getMessage().getReason(), SystemParameters.getInstance().getSystemLanguage());
            result.getMessage().setResponseContent(response);
            DataProvider.instance().persistProcessingResult(result);
            if (result.getStatus().getCode().equals(ProcessingStatusCodes.REJECTED) || result.getStatus().getCode().equals(ProcessingStatusCodes.FAILED)) {
                JfwHelper.executeActionWithServiceUser(MPAYView.SERVICE_CASH_OUT, cashOut.getId(), "SVC_Reject");
            } else if (result.getStatus().getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED) && result.getMpClearMessage() != null && !result.isHandleMPClearOffline()) {
                MPClearHelper.sendToActiveMQ(result.getMpClearMessage(), MPayContext.getInstance().getCommunicator());
            }

        } catch (Exception e) {

            throw new WorkflowException(e);
        }
    }

    MPAY_CorpoarteService getPSPCashOutService(MPAY_CorpoarteService service) throws WorkflowException {
        MPAY_CorpoarteService pspCashOutService = new MPAY_CorpoarteService();
        MPAY_Corporate psp = PSP.get();
        for (MPAY_CorpoarteService oService : psp.getRefCorporateCorpoarteServices()) {
            if (MessageCodes.CO.toString().equals(oService.getPaymentType().getCode()) && oService.getBank().getId() == service.getBank().getId()) {
                pspCashOutService = oService;
            }
        }

        return pspCashOutService;
    }
}