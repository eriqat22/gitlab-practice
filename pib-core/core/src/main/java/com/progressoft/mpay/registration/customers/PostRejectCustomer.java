package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PostRejectCustomer implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostRejectCustomer.class);
    private static final String CUSTOMER_REJECTION = "Customer Registration";

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("Inside execute ============");
        MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
        customer.setIsActive(false);
        rejectMobiles(customer);
        handleRequestMessageIfExist(customer);
        if (MPayContext.getInstance().getSystemParameters().isSmsNotificationEnabledOnCustomerRejection())
            sendSmsNotificationOnRejection(customer);
    }

    private void sendSmsNotificationOnRejection(MPAY_Customer customer) {
        Long languageId = LanguageMapper.getInstance().getLanguageLongByLocaleCode(customer.getPrefLang().getCode());
        MPAY_NotificationTemplate customerRejection = MPayContext.getInstance().getLookupsLoader().getNotificationTemplateByOperationName(CUSTOMER_REJECTION
                , NotificationType.REJECTED_SENDER, languageId);
        MPAY_Notification smsNotification = createSmsNotification(customer.getMobileNumber(), customerRejection.getTemplate());
        MPayContext.getInstance().getNotificationHelper().sendNotifications(smsNotification);
    }


    private MPAY_Notification createSmsNotification(String receiver, String notificationBody) {
        MPAY_Notification notification = new MPAY_Notification();
        notification.setReceiver(receiver);
        notification.setContent(notificationBody);
        notification.setRefChannel(LookupsLoader.getInstance().getNotificationChannel(NotificationChannelsCode.SMS));
        notification.setSuccess(false);
        return notification;
    }

    private void rejectMobiles(MPAY_Customer customer) throws WorkflowException {
        logger.debug("Inside RejectMobiles ============");

        List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId());
        if (mobiles == null || mobiles.isEmpty())
            return;
        Iterator<MPAY_CustomerMobile> mobilesIterator = mobiles.listIterator();
        do {
            MPAY_CustomerMobile mobile = mobilesIterator.next();
            if (mobile.getStatusId() != null && mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.APPROVAL)
                    || mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.INTEGRATION))
                JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, CustomerMobileWorkflowServiceActions.REJECT, new WorkflowException());
        } while (mobilesIterator.hasNext());
    }

    private void handleRequestMessageIfExist(MPAY_Customer customer) {
        if (customer.getIsRegistered())
            return;
        if (customer.getHint() != null && customer.getHint().trim().length() > 0) {
            MPAY_MPayMessage registrationMessage = DataProvider.instance().getMPayMessage(Long.parseLong(customer.getHint()));
            if (registrationMessage != null) {
                if (!registrationMessage.getProcessingStatus().getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED))
                    return;
                registrationMessage.setReason(LookupsLoader.getInstance().getReason(ReasonCodes.REJECTED_BY_NATIONAL_SWITCH));
                registrationMessage.setReasonDesc(customer.getRejectionNote());
                registrationMessage.setProcessingStatus(LookupsLoader.getInstance().getProcessingStatus(ProcessingStatusCodes.REJECTED));
                DataProvider.instance().mergeMPayMessage(registrationMessage);
            }
        }
    }
}
