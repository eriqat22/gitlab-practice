package com.progressoft.mpay.common;

import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.exceptions.NullArgumentException;

public class CoreComponents {
	private final IDataProvider dataProvider;
	private final ISystemParameters systemParameters;
	private final ILookupsLoader lookupsLoader;

	public CoreComponents(IDataProvider dataProvider, ISystemParameters systemParameters, ILookupsLoader lookupsLoader) {
		if(dataProvider == null)
			throw new NullArgumentException("dataProvider");
		if(systemParameters == null)
			throw new NullArgumentException("systemParameters");
		if(lookupsLoader == null)
			throw new NullArgumentException("lookupsLoader");
		this.dataProvider = dataProvider;
		this.systemParameters = systemParameters;
		this.lookupsLoader = lookupsLoader;
	}

	public IDataProvider getDataProvider() {
		return this.dataProvider;
	}
	public ISystemParameters getSystemParameters() {
		return this.systemParameters;
	}
	public ILookupsLoader getLookupsLoader() {
		return this.lookupsLoader;
	}
}
