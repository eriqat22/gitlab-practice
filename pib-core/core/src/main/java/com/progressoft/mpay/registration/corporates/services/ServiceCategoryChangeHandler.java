package com.progressoft.mpay.registration.corporates.services;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_AccountCategory;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entity.AccountCategory;
import com.progressoft.mpay.registration.customers.CustomerHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ServiceCategoryChangeHandler implements ChangeHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomerHandler.class);

    @Override
    public void handle(ChangeHandlerContext context) throws InvalidInputException {
        logger.debug("inside ServiceCategoryChangeHandler*******************-----------------");

        MPAY_CorpoarteService service = (MPAY_CorpoarteService) context.getEntity();
        MPAY_AccountCategory category = service.getCategory();
        if (Objects.nonNull(category)) {
            boolean isAgent = category.getCode().equals(AccountCategory.AGENT);
            context.getFieldsAttributes().get(MPAY_CorpoarteService.SERVICES).setVisible(isAgent);
        }
    }
}