package com.progressoft.mpay.payments.cashin;

import java.util.HashMap;
import java.util.Map;

import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.registration.ClientManagement;

public class ServiceAccountCashValueProvider {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<Object, Object> getServiceAccounts(String serviceId) {
		Map<Object, Object> accounts = new HashMap();
		if (serviceId != null && serviceId.length() > 0) {
			MPAY_CorpoarteService service = MPayContext.getInstance().getDataProvider().getCorporateService(Long.parseLong(serviceId));
			for (MPAY_ServiceAccount account : service.getServiceServiceAccounts()) {
				if (account.getBankedUnbanked().equals(BankedUnbankedFlag.UNBANKED) && account.getIsRegistered()) {
					String registrationId = ClientManagement.getRegistrationId(account);
					accounts.put(registrationId, registrationId);
				}
			}
		}

		return accounts;
	}
}