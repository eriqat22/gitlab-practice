package com.progressoft.mpay.mpclear;

public class ProcessingCodeRanges {
	public static final int DEBIT_START_VALUE = 0;
	public static final int DEBIT_END_VALUE = 200000;
	public static final int CREDIT_START_VALUE = 200000;
	public static final int CREDIT_END_VALUE = 290000;

	private ProcessingCodeRanges() {

	}
}
