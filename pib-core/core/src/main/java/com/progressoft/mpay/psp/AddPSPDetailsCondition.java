package com.progressoft.mpay.psp;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Corporate;

public class AddPSPDetailsCondition implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(AddPSPDetailsCondition.class);

	@SuppressWarnings({ "rawtypes" })
	@Override
	public boolean passesCondition(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("Inside AddPSPDetailsCondition Condition.....");
		boolean result = false;
//		MPAY_Corporate psp = (MPAY_Corporate) transientVars.get(WorkflowService.WF_ARG_BEAN);
//		if (psp == null || psp.getRefPSPPspDetails() == null) {
//			result = true;
//		}

		return result;
	}

}