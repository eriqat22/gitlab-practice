package com.progressoft.mpay.registration.customers;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerKyc;
import com.progressoft.mpay.templates.KycTemplateHandler;
import com.progressoft.mpay.templates.TemplateBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

public class CustomerHandler implements ChangeHandler {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(CustomerHandler.class);

    @Override
    public void handle(ChangeHandlerContext context) throws InvalidInputException {
        logger.debug("inside CustomerHandler*******************-----------------");
        try {
            CustomersChangeHandler handler = (CustomersChangeHandler) Class.forName(MPayContext.getInstance().getSystemParameters().getCustomersChangeHandler()).newInstance();
            handler.handleCustomer(context);
            MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "code", MPayContext.getInstance().getSystemParameters().getDefaultBankCode());
            MPAY_Customer customer = (MPAY_Customer) context.getEntity();
            initEnableChannels(context, customer);
            customer.setBank(defaultBank);
            MPAY_CustomerKyc kycTemplate = customer.getKycTemplate();
            if (Objects.isNull(kycTemplate))
                return;

            KycTemplateHandler kycTemplateHandler = new KycTemplateHandler(customer);
            kycTemplateHandler.handleTemplateOf(kycTemplate, TemplateBehaviour.VISIBLE, context);
            kycTemplateHandler.handleTemplateOf(kycTemplate, TemplateBehaviour.ENABLED, context);
            kycTemplateHandler.handleTemplateOf(kycTemplate, TemplateBehaviour.REQ, context);

        } catch (Exception e) {
            logger.error("Failed to create change handler instance", e);
            throw new InvalidInputException(e.getMessage());
        }
    }

    private void initEnableChannels(ChangeHandlerContext context, MPAY_Customer customer) {
        if (customer.getStatusId() == null) {
            customer.setEnableEmail(false);
            customer.setEnablePushNotification(false);
            customer.setEnableSMS(false);
            context.getFieldsAttributes().get(MPAY_Customer.EMAIL).setRequired(customer.getEnableEmail());
        }
    }
}
