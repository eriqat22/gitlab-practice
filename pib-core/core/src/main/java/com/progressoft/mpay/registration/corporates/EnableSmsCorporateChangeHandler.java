package com.progressoft.mpay.registration.corporates;

import com.progressoft.jfw.workflow.WfChangeHandler;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Corporate;
import org.springframework.stereotype.Component;

@Component
public class EnableSmsCorporateChangeHandler extends WfChangeHandler<MPAY_Corporate> {
    @Override
    public void handle(WfChangeHandlerContext<MPAY_Corporate> wfContext) {
        wfContext.field(MPAY_Corporate.MOBILE_NUMBER)
                .setRequired(wfContext.getEntity().getEnableSMS());
    }
}
