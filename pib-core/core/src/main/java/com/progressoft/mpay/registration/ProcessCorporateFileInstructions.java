package com.progressoft.mpay.registration;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.jfw.integration.JfwFacade;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Intg_Corp_Reg_Instr;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.exceptions.InvalidRecordException;

/**
 * @author u163
 */
public class ProcessCorporateFileInstructions implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessCorporateFileInstructions.class);

    /**
     *
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.debug("inside process file instruction file registration processor...");
        String body = (String) exchange.getIn().getBody();
        LOGGER.debug("body=" + body);

        String tenantId = (String) exchange.getProperty("tenantId");
        boolean enabledByMe = IntegrationUtils.enableServiceUser(tenantId);
        ItemDao itemDao = (ItemDao) AppContext.getApplicationContext().getBean("itemDao");
        List<MPAY_Intg_Corp_Reg_Instr> instrs = itemDao.getItems(MPAY_Intg_Corp_Reg_Instr.class, null, prepareFilter(),
                null, null);
        LOGGER.debug("Retreived Instructions = " + instrs.size());
        processCorporateInstructions(instrs, itemDao, tenantId);
        if (enabledByMe)
            IntegrationUtils.disableServiceUser();


    }


    private void processCorporateInstructions(List<MPAY_Intg_Corp_Reg_Instr> instrs, ItemDao itemDao, String tenantId) {
        LOGGER.debug("***********inside processCorporateInstructions function***********");
        MPAY_Profile profile = null;

        if (!instrs.isEmpty()) {
            profile = itemDao.getItem(MPAY_Profile.class, "code", "Default");
            if (profile == null)
                throw new UnsupportedOperationException("Default profile is not defined");
        }

        for (MPAY_Intg_Corp_Reg_Instr inst : instrs) {
            LOGGER.debug("Going to register corporate instruction with record id at mpclear = "
                    + inst.getRegInstrRecordId());
            MPAY_Corporate corporate = new MPAY_Corporate();
            try {

                validateCorporateInstruction(inst);

                setCorporateInformation(inst, corporate);

                MPAY_CorpoarteService corpService = new MPAY_CorpoarteService();
                setCorporateServiceInformation(profile, inst, corporate, corpService);

                persistCorporateAndExecuteWF(corporate, corpService, inst, tenantId);

                executeInstructionWF(inst);

            } catch (Exception exp) {
                LOGGER.info("Error while registering corporate instruction", exp);
                handleFailedInstruction(inst, exp.getMessage(), itemDao, tenantId);
            }

        }
    }

    private void validateCorporateInstruction(MPAY_Intg_Corp_Reg_Instr inst) {
        if (inst.getRegInstrRecordId() == null || "-1".equals(inst.getRegInstrRecordId())) {
            throw new InvalidRecordException("Invalid Record ID");
        }

    }

    private void handleFailedInstruction(MPAY_Intg_Corp_Reg_Instr inst, String error, ItemDao itemDao, String tenantId) {
        IntegrationUtils.enableServiceUser(tenantId);
        LOGGER.debug("Handling failed instruction....");
        inst.setRegInstrProcessingStts(LookupsLoader.getInstance().getRegInstrStatus("RJCT"));
        handleInstructionError(inst, error);
        inst.setReason(LookupsLoader.getInstance().getReason("40"));
        itemDao.merge(inst);
        try {
            JfwHelper.executeAction(MPAYView.CORPORATE_FILE_INSTRUCTIONS, inst.getId(), "SVC_Failed");
        } catch (WorkflowException e) {
            LOGGER.error("error... change me later", e);
        } finally {
            IntegrationUtils.disableServiceUser();
        }

    }

    private void handleInstructionError(MPAY_Intg_Corp_Reg_Instr inst, String error) {
        if (error != null && error.length() > 5000) {
            inst.setReasonDescription(error.substring(0, 5000));
        } else {
            inst.setReasonDescription(error);
        }
    }

    private void executeInstructionWF(MPAY_Intg_Corp_Reg_Instr inst) throws WorkflowException {
        JfwHelper.executeAction(MPAYView.CORPORATE_FILE_INSTRUCTIONS, inst.getId(), "SVC_Advance");
    }


    private void persistCorporateAndExecuteWF(MPAY_Corporate corporate, MPAY_CorpoarteService corpService,
                                              MPAY_Intg_Corp_Reg_Instr inst, String tenantId) throws WorkflowException {
        LOGGER.debug("Persisting Corporate/Service into database and execute workflow actions (create and approve).");
        try {

            IntegrationUtils.enableServiceUser(tenantId);
            List<MPAY_CorpoarteService> corpServices = new ArrayList<>();
            corpServices.add(corpService);

            corporate.setRefCorporateCorpoarteServices(corpServices);

            corporate.setInstId(inst);

            Map<Option, Object> nestedOptions = new EnumMap<>(Option.class);
            Map<String, String> nestedViewsMap = new HashMap<>();
            nestedViewsMap.put("refCorporateCorpoarteServices", MPAYView.CORPORATE_SERVICES.viewName);
            nestedOptions.put(Option.NESTED_VIEWS, nestedViewsMap);
            nestedOptions.put(Option.ORG_SHORT_NAME, "SUPER");

            Map<String, Object> transientVar = new HashMap<>();
            transientVar.put("dontCheckMaxService", "1");

            JfwFacade jfwFacade = AppContext.getApplicationContext().getBean("defaultJfwFacade", JfwFacade.class);
            jfwFacade.createEntity(MPAYView.CORPORATE.viewName, corporate, transientVar, nestedOptions);

            MPAY_Corporate corporateEntity = AppContext.getServiceLocator().getItemManagmentService()
                    .getItem(corporate.getClass().getName(), String.valueOf(corporate.getId()));

            LOGGER.debug("Executing Approve action on the corporate entity");

            Map<Option, Object> options = new EnumMap<>(Option.class);
            options.put(Option.ACTION_NAME, "Approve");

            jfwFacade.executeAction(MPAYView.CORPORATE.viewName, null, corporateEntity, transientVar, options);
        } catch (Exception ex) {
            LOGGER.error("error... change me later", ex);
            throw new WorkflowException(ex);
        } finally {
            IntegrationUtils.disableServiceUser();
        }
    }

    private void setCorporateServiceInformation(MPAY_Profile profile, MPAY_Intg_Corp_Reg_Instr inst,
                                                MPAY_Corporate corporate, MPAY_CorpoarteService corpService) {
        MPAY_Bank bank = LookupsLoader.getInstance().getBank(inst.getBankShortName());

        corpService.setName(inst.getServiceName());
        corpService.setDescription(inst.getServiceDescription());
        corpService.setServiceCategory(inst.getServiceCategory());
        corpService.setServiceType(inst.getServiceType());
        corpService.setRefCorporate(corporate);
        corpService.setAlias(inst.getAlias());
        corpService.setExternalAcc(inst.getExternalAcc());
        corpService.setBankedUnbanked(bank.getSettlementParticipant());
        corpService.setBank(LookupsLoader.getInstance().getBank(inst.getBankShortName()));
        if (inst.getMobileAccSelector() != null) {
            corpService.setMas(Long.parseLong(inst.getMobileAccSelector()));
        }
        MPAY_MessageType messageType = LookupsLoader.getInstance().getMessageTypes(inst.getPaymentType());
        corpService.setPaymentType(messageType);
        corpService.setRefProfile(profile);
        corpService.setIsActive(true);
    }


    private void setCorporateInformation(MPAY_Intg_Corp_Reg_Instr inst, MPAY_Corporate corporate) {
        corporate.setName(inst.getCorporateName());
        corporate.setDescription(inst.getDescription());
        corporate.setClientType(LookupsLoader.getInstance().getClientType(inst.getClientType()));
        try {
            corporate.setRegistrationDate(SystemHelper.parseDate(inst.getRegistrationDate(), "yyyy-MM-dd"));
        } catch (Exception exp) {
            throw new InvalidArgumentException("Invalid Corporate Registration Date Format.", exp);
        }
        corporate.setRegistrationId(inst.getRegistrationId());
        corporate.setPrefLang(LookupsLoader.getInstance().getLanguageByCode(inst.getPrefLang()));
        corporate.setPhoneOne(inst.getPhoneOne());
        corporate.setPhoneTwo(inst.getPhoneTwo());
        corporate.setEmail(inst.getEmail());
        corporate.setPobox(inst.getPobox());
        corporate.setZipCode(inst.getZipCode());
        corporate.setBuildingNum(inst.getBuildingNum());
        corporate.setStreetName(inst.getStreetName());
        corporate.setCity(LookupsLoader.getInstance().getCity(inst.getCity()));
        corporate.setNote(inst.getNote());
        corporate.setRefCountry(LookupsLoader.getInstance().getCountry(inst.getNationality()));
        corporate.setIsActive(true);
        corporate.setClientRef(inst.getClientRef());
    }

    private List<Filter> prepareFilter() {
        List<Filter> filters = new ArrayList<>();
        filters.add(Filter.get("statusId.name", Operator.LIKE, RegistrationConstants.WF_REG_INSTRS_STTS_NEW));
        filters.add(Filter.get("regInstrProcessingStts.code", Operator.LIKE, RegistrationConstants.REG_INSTRS_STTS_NES));
        return filters;
    }

}
