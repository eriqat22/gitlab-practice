package com.progressoft.mpay.messages;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MPayRequest extends MPayMessage {
	private static final Logger logger = LoggerFactory.getLogger(MPayRequest.class);

	public static final String SENDER_KEY = "sender";
	public static final String SENDER_TYPE_KEY = "senderType";
	public static final String PIN_CODE_KEY = "pin";
	public static final String REQUESTED_ID_KEY = "requestedId";
	public static final String SHOP_ID_KEY = "shopId";
	public static final String CHANNEL_ID_KEY = "channelId";
	public static final String WALLET_ID_KEY = "walletId";
	public static final String IS_AUTHORIZED = "isAuthorized";
	public static final String RECEIVER_KEY = "receiver";
	public static final String RECEIVER_TYPE_KEY = "receiverType";
	public static final String AMOUNT_KEY = "amount";
	public static final String NOTES_KEY = "notes";

	private String operation;
	private String sender;
	private String senderType;
	private String deviceId;
	private long lang;
	private String msgId;
	private String pin;
	private String tenant;
	private String checksum;
	private String requestedId;
	private String shopId;
	private String channelId;
	private String walletId;
	private Integer isAuthorized;
	private String receiver;
	private String receiverType;
	private BigDecimal amount;

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(String receiverType) {
		this.receiverType = receiverType;
	}

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public String getReceiverAccount() {
		return receiverAccount;
	}

	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	private String senderAccount;
	private String receiverAccount;
	private String notes;

	public MPayRequest() {
		extraData = new ArrayList<>();
	}

	public MPayRequest(String operation, String sender, String senderType, String deviceId, long languageCode, String messageId,
			String tenant) {
		this.operation = operation;
		this.sender = sender;
		this.senderType = senderType;
		this.deviceId = deviceId;
		this.lang = languageCode;
		this.msgId = messageId;
		this.extraData = new ArrayList<>();
		this.tenant = tenant;
	}

	public Integer getIsAuthorized() {
		return isAuthorized;
	}

	public void setIsAuthorized(Integer isAuthorized) {
		this.isAuthorized = isAuthorized;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String mob) {
		this.sender = mob;
	}

	public String getSenderType() {
		return senderType;
	}

	public void setSenderType(String senderType) {
		this.senderType = senderType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public long getLang() {
		return lang;
	}

	public void setLang(long lang) {
		this.lang = lang;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	public String getTenant() {
		return tenant;
	}

	public String getRequestedId() {
		return requestedId;
	}

	public void setRequestedId(String requestedId) {
		this.requestedId = requestedId;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public static MPayRequest fromJson(String json) {
		try {
			Gson gson = new GsonBuilder().create();
			MPayRequest request = gson.fromJson(json, MPayRequest.class);
			if (request == null)
				return null;
			if (validateRequest(request))
				return request;
			return null;
		} catch (Exception ex) {
			logger.debug("Error while fromJson", ex);
			return null;
		}
	}

	private static boolean validateRequest(MPayRequest request) {
		if (request.getTenant() == null || request.getTenant().trim().length() == 0)
			return false;
		if (request.getOperation() == null || request.getOperation().length() == 0)
			return false;
		if (request.getMsgId() == null || request.getMsgId().trim().length() == 0)
			return false;
		return true;
	}

	public String getValue(String key) {
		ExtraData data = selectFirst(getExtraData(), having(on(ExtraData.class).getKey(), Matchers.equalTo(key)));
		if (data == null)
			return null;
		return data.getValue();
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}

	public String toValues(String checkSum) {
		StringBuilder sb = new StringBuilder();
		sb.append(getStringValue(getAmount()));
		sb.append(getStringValue(getChannelId()));
		sb.append(getStringValue(checkSum));
		sb.append(getStringValue(getDeviceId()));
		if (checkIsAuthrized(getIsAuthorized()))
			sb.append(getIsAuthorized());
		sb.append(getLang());
		sb.append(getStringValue(getNotes()));
		sb.append(getStringValue(getMsgId()));
		sb.append(getStringValue(getOperation()));
		sb.append(getStringValue(getPin()));
		sb.append(getStringValue(getReceiver()));
		sb.append(getStringValue(getReceiverAccount()));
		sb.append(getStringValue(getReceiverType()));
		sb.append(getStringValue(getRequestedId()));
		sb.append(getStringValue(getSender()));
		sb.append(getStringValue(getSenderAccount()));
		sb.append(getStringValue(getSenderType()));
		sb.append(getStringValue(getShopId()));
		sb.append(getStringValue(getTenant()));
		sb.append(getStringValue(getWalletId()));
		writeExtraDataValues(sb);
		return sb.toString();
	}

	private boolean checkIsAuthrized(Integer isAuthorized) {
		if (isAuthorized == null)
			return false;
		return true;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String isStringNull(String value) {
		return value == null ? "" : value;
	}
}
