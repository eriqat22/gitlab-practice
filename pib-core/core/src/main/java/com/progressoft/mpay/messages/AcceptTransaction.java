package com.progressoft.mpay.messages;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;

public class AcceptTransaction implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(AcceptTransaction.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside AcceptTransaction ==========");
		MPAY_Transaction transaction = (MPAY_Transaction) arg0.get(WorkflowService.WF_ARG_BEAN);
		MessageProcessor processor = MessageProcessorHelper.getProcessor(transaction.getRefOperation());
		if (processor == null)
			throw new WorkflowException("Service Unavailable, no available processor to handle");

		try {
			MessageProcessingResult result = processor.accept(new MessageProcessingContext(DataProvider.instance(), LookupsLoader.getInstance(), SystemParameters.getInstance(), new MPayCryptographer(), NotificationHelper.getInstance()));
			if (result == null)
				throw new WorkflowException("Acceptance not applicable in this kind of transactions");
		} catch (Exception e) {
			throw new WorkflowException(e.getMessage(), e);
		}
	}
}
