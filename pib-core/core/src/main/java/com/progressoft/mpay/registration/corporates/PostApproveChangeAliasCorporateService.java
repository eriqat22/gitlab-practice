package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.UUID;

public class PostApproveChangeAliasCorporateService implements FunctionProvider {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(PostApproveChangeAliasCorporateService.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map arg1, PropertySet arg2) throws WorkflowException {
        /*logger.debug("inside execute--------------------------");
        MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN_DRAFT);
        MPAY_Corporate corporate = service.getRefCorporate();

        service.setMpclearAlias(service.getAlias());
        MPAY_EndPointOperation operation = LookupsLoader.getInstance()
                .getEndPointOperation(SystemParameters.getInstance().getAliasChangeEndPointOperation());
        String messageId = UUID.randomUUID().toString();
        MPayRequest request = new MPayRequest(operation.getOperation(), service.getName(), ReceiverInfoType.CORPORATE,
                "MPAY_UI", LanguageMapper.getInstance().getLanguageLongByLocaleCode(SystemParameters.getInstance().getSystemLanguage().getCode()), messageId,
                AppContext.getCurrentTenant());
        request.getExtraData().add(new ExtraData("alias", service.getNewAlias()));
        MPAY_MPayMessage message = new MPAY_MPayMessage();
        message.setMessageID(messageId);
        message.setTenantId(AppContext.getCurrentTenant());
        message.setReference(String.valueOf(DataProvider.instance().getNextReferenceNumberSequence()));
        message.setSender(service.getName());
        message.setRefOperation(operation);
        message.setRequestContent(request.toString());
        message.setMessageType(message.getRefOperation().getMessageType());
        message.setProcessingStamp(SystemHelper.getSystemTimestamp());
        message.setProcessingStatus(
                LookupsLoader.getInstance().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED));
        message.setReason(LookupsLoader.getInstance().getReason(ReasonCodes.VALID));
        DataProvider.instance().persistMPayMessage(message);
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor())
                .changeCorporateServiceAlias(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(),
                        LookupsLoader.getInstance()), corporate, service, message, service.getAlias());*/

        MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN);

        Long itemCount = itemDao.getItemCount(MPAY_CorpoarteService.class, null, "alias='" + service.getNewAlias() + "' and id!=" + service.getId());
        if (itemCount != 0)
            throw new InvalidInputException("Alias already used, please choose another alias");
        service.setAlias(service.getNewAlias());
        service.setNewAlias("");
    }
}
