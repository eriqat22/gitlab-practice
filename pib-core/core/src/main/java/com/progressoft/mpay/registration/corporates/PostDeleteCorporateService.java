package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PostDeleteCorporateService implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostDeleteCorporateService.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PostDeleteCorporateService.execute =========");
        MPAY_CorpoarteService corporateService = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
        corporateService.setIsRegistered(false);
        corporateService.setIsActive(false);
        handleDevices(corporateService.getId());
        handleAccounts(corporateService.getId());
    }

    private void handleDevices(long serviceId) throws WorkflowException {
        List<MPAY_CorporateDevice> devices = DataProvider.instance().listServiceDevices(serviceId);
        if (devices == null || devices.isEmpty())
            return;
        Iterator<MPAY_CorporateDevice> devicesIterators = devices.listIterator();
        if (devicesIterators.hasNext()) {
            MPAY_CorporateDevice device = devicesIterators.next();
            JfwHelper.executeAction(MPAYView.CORPORATE_DEVICES, device, CorporateDeviceWorkflowServiceActions.DELETE, new WorkflowException());
        }
    }

    private void handleAccounts(long serviceId) throws WorkflowException {
        List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(serviceId);
        if (accounts == null || accounts.isEmpty())
            return;
//        Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
//        if (accountsIterator.hasNext()) {
//            MPAY_ServiceAccount account = accountsIterator.next();
//            JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.DELETE, new WorkflowException());
//        }
        for (MPAY_ServiceAccount account : accounts) {
            JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.DELETE, new WorkflowException());
        }
    }
}