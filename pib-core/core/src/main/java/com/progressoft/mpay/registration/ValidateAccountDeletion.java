package com.progressoft.mpay.registration;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class ValidateAccountDeletion {

	private ValidateAccountDeletion() {

	}
	public static void validate(MPAY_CustomerMobile mobile) throws InvalidInputException {
		List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
		if (accounts == null || accounts.isEmpty())
			return;

		Iterator<MPAY_MobileAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_MobileAccount account = accountsIterator.next();
			validateWallet(account.getRefAccount());
		} while (accountsIterator.hasNext());
	}

	public static void validate(MPAY_CorpoarteService service) throws InvalidInputException {
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
		if (accounts == null || accounts.isEmpty())
			return;

		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_ServiceAccount account = accountsIterator.next();
			validateWallet(account.getRefAccount());
		} while (accountsIterator.hasNext());
	}

	public static void validateWallet(MPAY_Account account) throws InvalidInputException {
		if (account == null)
			return;
		if (account.getBalance().compareTo(BigDecimal.ZERO ) > 0) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DELETE_ITEM_ACCOUNT));
			throw iie;
		}
	}
}
