package com.progressoft.mpay.plugins.validators;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class SignInValidator {
	private static final Logger logger = LoggerFactory.getLogger(SignInValidator.class);

	private SignInValidator() {

	}

	public static ValidationResult validate(ProcessingContext context, MPAY_CustomerMobile mobile, String deviceId) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		if (mobile == null || deviceId == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		MPAY_CustomerDevice device = MPayHelper.getCustomerDevice(context.getDataProvider(), deviceId,
				context.getSender().getMobile().getId());
		if (device == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		if (!device.getIsOnline())
			return new ValidationResult(ReasonCodes.LOGIN_EXPIRED, null, false);
		if (!validateLoginExpiry(device.getLastLoginTime(),
				context.getSystemParameters().getMobileSignInValidityInMin()))
			return new ValidationResult(ReasonCodes.LOGIN_EXPIRED, null, false);

		device.setLastLoginTime(SystemHelper.getSystemTimestamp());
		context.getDataProvider().mergeCustomerDevice(device);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	public static ValidationResult validate(ProcessingContext context, MPAY_CorpoarteService service, String deviceId) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		if (service == null || deviceId == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		MPAY_CorporateDevice device = MPayHelper.getCorporateDevice(context.getDataProvider(), deviceId,
				context.getSender().getService().getId());
		if (device == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		if (!device.getIsOnline())
			return new ValidationResult(ReasonCodes.LOGIN_EXPIRED, null, false);
		if (!validateLoginExpiry(device.getLastLoginTime(),
				context.getSystemParameters().getServiceSignInValidityInMin()))
			return new ValidationResult(ReasonCodes.LOGIN_EXPIRED, null, false);

		device.setLastLoginTime(SystemHelper.getSystemTimestamp());
		context.getDataProvider().mergeCorporateDevice(device);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static boolean validateLoginExpiry(Timestamp lastLogin, long validityInMin) {
		if (lastLogin == null)
			return false;
		long seconds = 60;
		long milliseconds = 1000;
		Timestamp lastSignWithValidityInMinute = new Timestamp(
				lastLogin.getTime() + validityInMin * seconds * milliseconds);
		return lastSignWithValidityInMinute.after(SystemHelper.getSystemTimestamp());
	}
}
