package com.progressoft.mpay.payments.cashin;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclear.MPayCommunicatorHelper;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.payments.CashMessage;
import com.progressoft.mpay.payments.CashRequestHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.NotificationContext;
import com.progressoft.mpay.psp.PSP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

public class PostCashInRequest implements FunctionProvider {

    public static final int OPERATION_ID = 6;
    private static final Logger logger = LoggerFactory.getLogger(PostCashInRequest.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {

        logger.debug("***inside PostCashInRequest");
        MPAY_CashIn customerCashIn = (MPAY_CashIn) arg0.get(WorkflowService.WF_ARG_BEAN);
        customerCashIn.setReason(null);
        customerCashIn.setReasonDesc(null);
        if (!SystemNetworkStatus.isSystemOnline(DataProvider.instance())) {
            throw new WorkflowException(LookupsLoader.getInstance().getReason(ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE)
                    .getReasonsNLS().get(SystemParameters.getInstance().getSystemLanguage().getId()).getDescription());
        }
        try {
            MPayRequest request = new MPayRequest("cashin",
                    getPSPCashInService(customerCashIn.getRefCustMob()).getAlias(), ReceiverInfoType.ALIAS, null,
                    LanguageMapper.getInstance()
                            .getLanguageLongByLocaleCode(SystemParameters.getInstance().getSystemLanguage().getCode()),
                    UUID.randomUUID().toString(), customerCashIn.getTenantId());
            request.getExtraData()
                    .add(new ExtraData(CashMessage.AMOUNT_KEY, customerCashIn.getTotalAmount().toString()));
            request.getExtraData()
                    .add(new ExtraData(CashMessage.CHARGE_AMOUNT_KEY, customerCashIn.getChargeAmount().toString()));
            request.getExtraData()
                    .add(new ExtraData(CashMessage.TAX_AMOUNT_KEY, customerCashIn.getTaxAmount().toString()));
            request.getExtraData()
                    .add(new ExtraData(CashMessage.RECEIVER_KEY, customerCashIn.getRefCustMob().getMobileNumber()));
            request.getExtraData().add(new ExtraData(CashMessage.RECEIVER_TYPE_KEY, ReceiverInfoType.MOBILE));
            request.getExtraData()
                    .add(new ExtraData(CashMessage.RECEIVER_ACCOUNT_KEY, customerCashIn.getRefMobileAccount()));
            request.getExtraData().add(new ExtraData(CashMessage.NOTES_KEY, customerCashIn.getComments()));

            MessageProcessingContext context = new MessageProcessingContext(DataProvider.instance(),
                    LookupsLoader.getInstance(), SystemParameters.getInstance(), new MPayCryptographer(),
                    new NotificationHelper());
            context.setRequest(request);
            context.setOriginalRequest(request.toString());
            context.setOperation(LookupsLoader.getInstance().getEndPointOperation(request.getOperation()));
            context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
            MessageProcessor processor = MessageProcessorHelper.getProcessor(context.getOperation());
            if (processor == null)
                throw new WorkflowException("Cash In processor not available");
            MPAY_BulkPayment bulkPayment = context.getDataProvider()
                    .getBulkPayment(context.getSystemParameters().getBulkPaymentDefaultId());
            if (bulkPayment == null)
                throw new WorkflowException("Default Bulk Payment ID Not Found");
            MPAY_MPayMessage message = MessageProcessorHelper.createMessage(new ServiceUserManager(), context,
                    ProcessingStatusCodes.PENDING, null, null, bulkPayment);
            customerCashIn.setRefMessage(message);
            message.setRefMessageCashIn(new ArrayList<>());
            message.getRefMessageCashIn().add(customerCashIn);
            DataProvider.instance().persistMPayMessage(message);
            context.setMessage(message);
            context.getExtraData().put("cashIn", customerCashIn);
            MessageProcessingResult result = processor.processMessage(context);
            customerCashIn.setProcessingStatus(result.getMessage().getProcessingStatus());
            customerCashIn.setReason(result.getMessage().getReason());
            customerCashIn.setReasonDesc(result.getMessage().getReasonDesc());
            String response = CashRequestHelper.generateResponse(Long.parseLong(result.getMessage().getReference()),
                    result.getMessage().getProcessingStatus().getCode(), result.getMessage().getReason(),
                    SystemParameters.getInstance().getSystemLanguage());
            result.getMessage().setResponseContent(response);

            DataProvider.instance().persistProcessingResult(result);
            if (result.getStatus().getCode().equals(ProcessingStatusCodes.REJECTED) || result.getStatus().getCode().equals(ProcessingStatusCodes.FAILED)) {
                JfwHelper.executeActionWithServiceUser(MPAYView.CASH_IN, customerCashIn.getId(), "SVC_Reject");
                sendRejectionLimitsSmsIfEnabeled(customerCashIn, context, message, result);
                sendRejectionWalletCapSmsIfEnabeled(customerCashIn, context, message, result);

            } else if (result.getMpClearMessage() != null && !result.isHandleMPClearOffline()) {
                customerCashIn.setRefLastMsgLog(result.getMpClearMessage());
                MPClearHelper.sendToActiveMQ(result.getMpClearMessage(), MPayCommunicatorHelper.getCommunicator());
                DataProvider.instance().mergeCashIn(customerCashIn);
            }

        } catch (Exception e) {
            throw new WorkflowException(e);
        }

    }

    private void sendRejectionLimitsSmsIfEnabeled(MPAY_CashIn customerCashIn, MessageProcessingContext context, MPAY_MPayMessage message, MessageProcessingResult result) {
        if (context.getSystemParameters().isSmsNotificationEnabledOnCashinExceededLimits() &&
                hasExceededCountOrAmountLimits(result.getReasonCode()))
            sendSmsNotificationOnRejection(message, customerCashIn.getRefCustMob());
    }

    private void sendRejectionWalletCapSmsIfEnabeled(MPAY_CashIn customerCashIn, MessageProcessingContext context, MPAY_MPayMessage message, MessageProcessingResult result) {
        if (context.getSystemParameters().isSmsNotificationEnabledOnCashinExceededLimits() &&
                hasWalletCapLimits(result.getReasonCode()))
            sendSmsNotificationOnRejection(message, customerCashIn.getRefCustMob());
    }

    private boolean hasWalletCapLimits(String reasonCode) {
        return reasonCode.equals(ReasonCodes.WALLET_CAP_REACHED);
    }

    private boolean hasExceededCountOrAmountLimits(String reasonCode) {
        return (reasonCode.equals(ReasonCodes.EXCEEDED_DAILY_AMOUNT_LIMIT) ||
                reasonCode.equals(ReasonCodes.EXCEEDED_DAILY_COUNT_LIMIT) ||
                reasonCode.equals(ReasonCodes.EXCEEDED_WEEKLY_AMOUNT_LIMIT) ||
                reasonCode.equals(ReasonCodes.EXCEEDED_WEEKLY_COUNT_LIMIT) ||
                reasonCode.equals(ReasonCodes.EXCEEDED_MONTHLY_AMOUNT_LIMIT) ||
                reasonCode.equals(ReasonCodes.EXCEEDED_MONTHLY_COUNT_LIMIT) ||
                reasonCode.equals(ReasonCodes.EXCEEDED_YEARLY_AMOUNT_LIMIT) ||
                reasonCode.equals(ReasonCodes.EXCEEDED_YEARLY_COUNT_LIMIT)) ||
                reasonCode.equals(ReasonCodes.TRANSACTION_AMOUNT_NOT_ALLOWED);
    }

    private void sendSmsNotificationOnRejection(MPAY_MPayMessage message, MPAY_CustomerMobile customerMobile) {
        Long languageId = customerMobile.getRefCustomer().getPrefLang().getId();
        NotificationContext notificationContext = new NotificationContext();
        notificationContext.setReference(message.getReference());
        notificationContext.setReasonDescription(message.getReason().getReasonsNLS().get(SystemParameters.getInstance().getSystemLanguage().getCode()).getDescription());
        MPAY_Notification smsNotification = MPayContext.getInstance().getNotificationHelper().createNotification(message, notificationContext, NotificationType.REJECTED_RECEIVER, OPERATION_ID, languageId, customerMobile.getMobileNumber(), NotificationChannelsCode.SMS);
        MPayContext.getInstance().getNotificationHelper().sendNotifications(smsNotification);
    }

    private MPAY_CorpoarteService getPSPCashInService(MPAY_CustomerMobile oMobile) throws WorkflowException {
        MPAY_CorpoarteService pspCashInService = new MPAY_CorpoarteService();
        MPAY_Corporate psp = PSP.get();
        for (MPAY_CorpoarteService oService : psp.getRefCorporateCorpoarteServices()) {
            if (MessageCodes.CI.toString().equals(oService.getPaymentType().getCode())
                    && oService.getBank().getId().equals(oMobile.getBank().getId())) {
                pspCashInService = oService;
            }
        }
        return pspCashInService;
    }
}