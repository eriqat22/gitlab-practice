package com.progressoft.mpay.mpclear;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MPClearClientHelper {

	private static final Logger logger = LoggerFactory.getLogger(MPClearClientHelper.class);
	private static IMPClearClient client;

	private MPClearClientHelper() {

	}

	public static IMPClearClient getClient(String processor) {
		if(client != null)
			return client;
		try {
			Class<?> cls = Class.forName(processor);
			return (IMPClearClient)cls.newInstance();
		} catch (Exception e) {
			logger.error("Error in getClient, processor: " + processor, e);
			return null;
		}
	}
}
