package com.progressoft.mpay.requesttypes;

import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_RequestTypesDetail;
import com.progressoft.mpay.entities.MPAY_RequestTypesScheme;
import com.progressoft.mpay.entity.MPAYView;

public class RequestTypeSchemeApproval {

	private static ReentrantLock lock = new ReentrantLock();

	private RequestTypeSchemeApproval() {

	}

	public static void processMessageTypes(MPAY_RequestTypesScheme requestType) throws WorkflowException {
		try {
			lock.lock();
			Iterator<MPAY_RequestTypesDetail> details = requestType.getRefSchemeRequestTypesDetails().iterator();
			while (details.hasNext()) {
				MPAY_RequestTypesDetail detail = details.next();
				if (detail.getJfwDraft() != null)
					JfwHelper.executeAction(MPAYView.REQUEST_TYPES_DETAILS, detail, "SVC_Approve", new WorkflowException());
			}
		} finally {
			lock.unlock();
		}
	}
}
