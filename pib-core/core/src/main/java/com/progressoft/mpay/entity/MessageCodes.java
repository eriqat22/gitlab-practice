package com.progressoft.mpay.entity;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_MessageType;

public enum MessageCodes {

	INVALID("z"), P2P("P2P"), CI("CI"), CO("CO"), PIN("PIN"), BAL("BAL"), NEWPIN("NEWPIN"), BILL("BILL"), GOV("GOV"), B2P("B2P"), G2P("G2P"), EPAYADV("EPAYADV"), REQACTIVATE("REQACTIVATE"), ACTIVATE(
			"ACTIVATE"), CHANGEPWD("CHANGEPWD"), MINI("MINI"), LGN("LGN"), SAL("SAL"), ALIAS("ALIAS"), AUTH("AUTH"), SUSRACTV("SUSRACTV"), SRVS("SRVS"), ATM("ATM"), SIGNIN(
					"SIGNIN"), DEACTIVATE("DEACTIVATE"), BILLINQ("BILLINQ"), BILLFEE("BILLFEE"), FEES("FEES"), CLEARING("CLEARING"), GENERIC("GENERIC"), COMMISSIONS("CMSION"), INCOME_VAT("INCOMEVAT"), EXPENSES_VAT("EXPENSESVAT"),
	SRVCI("SRVCI"), SRVCO("SRVCO");

	private String value;

	MessageCodes(String value) {
		this.value = value;
	}

	public boolean compare(MPAY_MessageType msgType) {
		if (msgType == null)
			throw new NullArgumentException("msgType");
		return msgType.getCode().equals(this.value);
	}

	@Override
	public String toString() {
		return value;
	}
}