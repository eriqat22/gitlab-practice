package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.List;
import java.util.Map;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.attachments.AttachmentItem;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.entities.*;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import org.springframework.beans.factory.annotation.Autowired;

public class PostApproveCorporateService implements FunctionProvider {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(PostApproveCorporateService.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside execute ============");
        MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
        saveAttachment(service, arg0.get(WorkflowService.WF_ARG_VIEW_NAME));
        checkDuplicateAlias(service);
        if (!service.getIsRegistered()) {
            List<MPAY_ServiceAccount> accounts = getPendingAccount(service);
            if (accounts == null)
                throw new WorkflowException("Service doesn't have pending service accounts");
            accounts.forEach(account -> {
                try {
                    JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, CorporateServiceWorkflowServiceActions.APPROVE, new WorkflowException());
                } catch (WorkflowException e) {
                    e.printStackTrace();
                }
            });
        } else {
            List<MPAY_ServiceAccount> accounts = getPendingAccount(service);
            MPAY_ServiceAccount account = null;
            if (accounts == null)
                account = getFirstRegisteredAccount(service);
            if (account == null)
                throw new WorkflowException("Service doesn't have an approved accounts");
            handleUpdateService(service.getRefCorporate(), service, account);
        }
    }

    private void checkDuplicateAlias(MPAY_CorpoarteService service) throws InvalidInputException {
        Long itemCount = itemDao.getItemCount(MPAY_CorpoarteService.class, null, "alias='" + service.getNewAlias() + "' and id!=" + service.getId());
        if (itemCount != 0)
            throw new InvalidInputException("Alias already used, please choose another alias");
    }

    private void saveAttachment(MPAY_CorpoarteService service, Object viewName) {
        String where = "recordId=" + service.getId().toString() + " AND imageType='1'";
        if (viewName.toString().equals(MPAYView.CORPORATE_SERVICE_VIEWS.viewName)) {
            List<MPAY_CorpoarteServicesViewAtt> items = itemDao.getItems(MPAY_CorpoarteServicesViewAtt.class, 0, 0, null, null, where, null);
            if (items == null || items.size() == 0) {
                List<MPAY_CorpoarteServiceAtt> serviceItems = itemDao.getItems(MPAY_CorpoarteServiceAtt.class, 0, 0, null, null, where, null);
                if (serviceItems == null || serviceItems.size() == 0)
                    return;
                MPAY_CorpoarteServicesViewAtt viewAttachment = new MPAY_CorpoarteServicesViewAtt();
                persistAttachment(serviceItems.get(0), viewAttachment);
                return;
            }
            MPAY_CorpoarteServiceAtt viewAttachment = new MPAY_CorpoarteServiceAtt();
            persistAttachment(items.get(0), viewAttachment);
        }
    }

    private void persistAttachment(AttachmentItem currentAttachment, AttachmentItem newAttachment) {
        newAttachment.setImageType(currentAttachment.getImageType());
        newAttachment.setAttachmentToken(currentAttachment.getAttachmentToken());
        newAttachment.setAttFile(currentAttachment.getAttFile());
        newAttachment.setAttSource(currentAttachment.getAttSource());
        newAttachment.setComments(currentAttachment.getComments());
        newAttachment.setContentType(currentAttachment.getContentType());
        newAttachment.setEntityId(currentAttachment.getEntityId());
        newAttachment.setName(currentAttachment.getName());
        newAttachment.setRecordId(currentAttachment.getRecordId());
        newAttachment.setSize(currentAttachment.getSize());
        newAttachment.setTenantId(currentAttachment.getTenantId());
        newAttachment.setOrgId(currentAttachment.getOrgId());
        itemDao.persist(newAttachment);
    }

    private List<MPAY_ServiceAccount> getPendingAccount(MPAY_CorpoarteService service) throws WorkflowException {
        logger.debug("inside getPendingAccount ============");
        List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId(), ServiceAccountWorkflowStatuses.APPROVAL.toString());
        if (accounts == null || accounts.isEmpty())
            return null;
//        if (accounts.size() > 1)
//            throw new WorkflowException("Service has more than one pending service account");
        return accounts;
    }

    private MPAY_ServiceAccount getFirstRegisteredAccount(MPAY_CorpoarteService service) throws WorkflowException {
        logger.debug("inside GetFirstRegisteredAccount ============");
        List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
        if (accounts == null || accounts.isEmpty())
            throw new WorkflowException("Service should have a pending service account");
        return selectFirst(accounts, having(on(MPAY_ServiceAccount.class).getIsRegistered(), Matchers.equalTo(true)));
    }

    private void handleUpdateService(MPAY_Corporate corporate, MPAY_CorpoarteService service, MPAY_ServiceAccount account) throws WorkflowException {
        logger.debug("inside HandleUpdateService ============");
        if (!StringUtils.equals(service.getName(), service.getNewName()) || corporate.getApprovedData() != null)
            sendMPClearIntegrationMessage(corporate, service, account);
        else
            JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.APPROVE, new WorkflowException());

        if (account.getStatusId().getCode().equals(ServiceAccountWorkflowStatuses.APPROVAL.toString()))
            JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.APPROVE, new WorkflowException());
    }

    private void sendMPClearIntegrationMessage(MPAY_Corporate corporate, MPAY_CorpoarteService service, MPAY_ServiceAccount account) throws WorkflowException {
        logger.debug("inside sendMPClearIntegrationMessage ============");
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).updateCorporate(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), corporate, service, account);
    }
}
