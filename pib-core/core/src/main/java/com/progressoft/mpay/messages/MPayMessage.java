package com.progressoft.mpay.messages;

import java.util.Collections;
import java.util.List;

public class MPayMessage {
	protected List<ExtraData> extraData;

	public void setExtraData(List<ExtraData> extraData) {
		this.extraData = extraData;
	}

	public List<ExtraData> getExtraData() {
		return extraData;
	}

	protected void writeExtraDataValues(StringBuilder sb) {
		ExtraDataComparer comparer = new ExtraDataComparer();
		if (getExtraData() != null && !getExtraData().isEmpty()) {
			Collections.sort(getExtraData(), comparer);
			for (ExtraData data : getExtraData()) {
				sb.append(getStringValue(data.getValue()));
			}
		}
	}

	protected String getStringValue(Object value) {
		if (value == null)
			return "";
		return value.toString();
	}
}
