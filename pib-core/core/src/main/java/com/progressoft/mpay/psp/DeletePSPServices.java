package com.progressoft.mpay.psp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.registration.corporates.PSPServiceWorkflowServiceActions;
import com.progressoft.mpay.registration.corporates.ServiceAccountWorkflowServiceActions;

public class DeletePSPServices implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(DeletePSPServices.class);

	@Autowired
	private ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Add CachIn/CachOut service for PSP....");

		MPAY_Bank bank = (MPAY_Bank) arg0.get(WorkflowService.WF_ARG_BEAN);
		deleteService(bank);
		bank.setDeletedFlag(true);
		bank.setDeletedBy(AppContext.getCurrentUser().getUsername());
		bank.setDeletedOn(SystemHelper.getSystemTimestamp());
	}

	private void deleteService(MPAY_Bank bank) throws WorkflowException {

		Filter filter = new Filter();
		filter.setProperty("bank.code");
		filter.setFirstOperand(bank.getCode());
		filter.setOperator(Operator.EQ);
		List<Filter> filters = new ArrayList<>();
		filters.add(filter);

		List<MPAY_CorpoarteService> services = itemDao.getItems(MPAY_CorpoarteService.class, null, filters, null, null);
		handleServices(services, bank);
	}

	private void handleServices(List<MPAY_CorpoarteService> services, MPAY_Bank bank) throws WorkflowException {
		if (services == null || services.isEmpty())
			return;
		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			service.setMpclearAlias(bank.getTellerCode());
			service.setDeletedFlag(true);
			service.setIsActive(false);
			service.setIsRegistered(false);
			JfwHelper.executeAction(MPAYView.PSP_SERVICES, service, PSPServiceWorkflowServiceActions.DELETE, new WorkflowException());
			handleAccounts(service.getServiceServiceAccounts());
		} while (servicesIterator.hasNext());
	}

	private void handleAccounts(List<MPAY_ServiceAccount> accounts) throws WorkflowException {
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_ServiceAccount account = accountsIterator.next();
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.DELETE, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
}
