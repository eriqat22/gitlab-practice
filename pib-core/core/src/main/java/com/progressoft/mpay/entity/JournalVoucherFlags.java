package com.progressoft.mpay.entity;

public class JournalVoucherFlags {
	public static final String DEBIT = "1";
	public static final String CREDIT = "2";

	private JournalVoucherFlags() {

	}
}
