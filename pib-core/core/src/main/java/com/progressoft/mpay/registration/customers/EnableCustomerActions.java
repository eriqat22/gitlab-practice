package com.progressoft.mpay.registration.customers;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;

public class EnableCustomerActions implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(EnableCustomerActions.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_Customer customer = (MPAY_Customer) transientVar.get(WorkflowService.WF_ARG_BEAN);
		List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId());
		if (mobiles.size() > 1) {
			MPAY_CustomerMobile newMobile = selectFirst(mobiles, having(on(MPAY_CustomerMobile.class).getIsRegistered(), Matchers.equalTo(false)));
			if (newMobile != null)
				return false;
		}
		Iterator<MPAY_CustomerMobile> mobilesIterator = mobiles.iterator();
		do {
			MPAY_CustomerMobile mobile = mobilesIterator.next();
			List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
			if(accounts.size() == 1)
				continue;
			MPAY_MobileAccount newAccount = selectFirst(accounts, having(on(MPAY_MobileAccount.class).getIsRegistered(), Matchers.equalTo(false)));
			if (newAccount != null)
				return false;
		} while (mobilesIterator.hasNext());

		return true;
	}
}
