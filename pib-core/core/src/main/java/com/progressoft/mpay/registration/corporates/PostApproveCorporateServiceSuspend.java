package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostApproveCorporateServiceSuspend implements FunctionProvider {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(PostApproveCorporateServiceSuspend.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN);
		service.setIsActive(false);
		handleServiceAccounts(service);
	}

	private void handleServiceAccounts(MPAY_CorpoarteService service) throws WorkflowException {
		logger.debug("Inside HandleServiceAccounts ================");
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_ServiceAccount account = accountsIterator.next();
			if (!account.getStatusId().getCode().equals(ServiceAccountWorkflowStatuses.DELETED.toString()) && !account.getStatusId().getCode().equals(ServiceAccountWorkflowStatuses.APPROVED.toString())) {
				InvalidInputException iie = new InvalidInputException();
				iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.SERVICE_ACCOUNT_UNDER_MODIFICATION));
				throw iie;
			}
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.SUSPEND, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
}