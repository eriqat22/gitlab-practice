package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostCorporateServiceRegistrationApproval implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCorporateServiceRegistrationApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN);
		service.setIsRegistered(true);
		applyServiceChanges(service);
		handleAccount(service);
	}

	private void handleAccount(MPAY_CorpoarteService service) throws WorkflowException {
		logger.debug("inside HandleAccount--------------------------");
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId(), ServiceAccountWorkflowStatuses.INTEGRATION.toString());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_ServiceAccount account = accountsIterator.next();
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.APPROVE, new WorkflowException());
		} while (accountsIterator.hasNext());
	}

	private void applyServiceChanges(MPAY_CorpoarteService service) {
		logger.debug("inside ApplyServiceChanges--------------------------");
		if (service.getNewName() != null) {
			service.setName(service.getNewName());
			service.setNewName(null);
		}
		if (service.getNewDescription() != null) {
			service.setDescription(service.getNewDescription());
			service.setNewDescription(null);
		}
		if (service.getNewAlias() != null) {
			service.setAlias(service.getNewAlias());
			service.setNewAlias(null);
		}
		service.setApprovedData(null);
	}
}
