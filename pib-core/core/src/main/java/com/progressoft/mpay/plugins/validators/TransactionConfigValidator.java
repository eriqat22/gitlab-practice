package com.progressoft.mpay.plugins.validators;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_TransactionConfig;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.ValidationResult;

public class TransactionConfigValidator {
	private static final Logger logger = LoggerFactory.getLogger(TransactionConfigValidator.class);

	private TransactionConfigValidator() {

	}

	public static ValidationResult validate(MPAY_TransactionConfig config, BigDecimal amount) {
		logger.debug("Inside Validate ...");
		if(amount == null)
			throw new NullArgumentException("amount");
		if (config == null)
			return new ValidationResult(ReasonCodes.UNSUPPORTED_REQUEST, null, false);
		if (amount.floatValue() < config.getMinAmount() || amount.floatValue() > config.getMaxAmount())
			return new ValidationResult(ReasonCodes.TRANSACTION_AMOUNT_NOT_ALLOWED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
