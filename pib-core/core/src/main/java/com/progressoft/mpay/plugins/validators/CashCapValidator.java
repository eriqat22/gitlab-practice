package com.progressoft.mpay.plugins.validators;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class CashCapValidator {
	private static final Logger logger = LoggerFactory.getLogger(CashCapValidator.class);

	private CashCapValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		BigDecimal cashCap = context.getSystemParameters().getCashCap();
		BigDecimal currentBalance = context.getDataProvider().getTotalCashBalance().multiply(new BigDecimal(-1));

		if (currentBalance.add(context.getAmount()).compareTo(cashCap) > 0)
			return new ValidationResult(ReasonCodes.CASH_CAP_EXCEEDED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
