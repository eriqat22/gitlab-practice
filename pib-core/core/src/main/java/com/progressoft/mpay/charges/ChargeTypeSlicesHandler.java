package com.progressoft.mpay.charges;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_ChargesSlice;

public class ChargeTypeSlicesHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(ChargeTypeSlicesHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside ChargeTypeSlicesHandler --------------------");

		MPAY_ChargesSlice slice = (MPAY_ChargesSlice) changeHandlerContext.getEntity();

		ChargeHandlersHelper.handleChargeType(changeHandlerContext.getFieldsAttributes(), slice);

	}

}
