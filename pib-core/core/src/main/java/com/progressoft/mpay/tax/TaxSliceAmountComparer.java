package com.progressoft.mpay.tax;

import java.util.Comparator;

import com.progressoft.mpay.entities.MPAY_TaxSlice;

public class TaxSliceAmountComparer implements Comparator<MPAY_TaxSlice> {

	@Override
	public int compare(MPAY_TaxSlice o1, MPAY_TaxSlice o2) {
		if (o1 == null) {
			throw new IllegalArgumentException("o1 is null");
		}
		if (o2 == null) {
			throw new IllegalArgumentException("o2 is null");
		}

		return o1.getTxAmountLimit().compareTo(o2.getTxAmountLimit());
	}
}
