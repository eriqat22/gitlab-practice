package com.progressoft.mpay.plugins.validators;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class AmountValidator {
	private static final Logger logger = LoggerFactory.getLogger(AmountValidator.class);

	private AmountValidator() {

	}
	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		if(context.getAmount() == null)
			return new ValidationResult(ReasonCodes.INVALID_AMOUNT, null, false);
		int decimalCount = context.getSystemParameters().getDecimalCount();
		if (getDecimalCounts(context.getAmount()) > decimalCount)
			return new ValidationResult(ReasonCodes.INVALID_AMOUNT, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static int getDecimalCounts(BigDecimal amount) {
		logger.debug("Inside GetDecimalCounts ...");
		String amountString = amount.stripTrailingZeros().toPlainString();
		int index = amountString.indexOf('.');
		return index < 0 ? 0 : amountString.length() - index - 1;
	}
}
