package com.progressoft.mpay.bulkpayment.function;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.bulkpayment.process.IBulkPaymentProcessor;
import com.progressoft.mpay.entities.MPAY_BulkPayment;

@SuppressWarnings("rawtypes")
public class CreateBulkPaymentFileCreate implements FunctionProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateBulkPaymentFileCreate.class);
    @Autowired
    private IBulkPaymentProcessor processor;

    @Override
    public void execute(Map transientVar, Map args, PropertySet propertySet) throws WorkflowException {
        MPAY_BulkPayment bulkPayment = (MPAY_BulkPayment) transientVar.get(WorkflowService.WF_ARG_BEAN);
        LOGGER.debug("Inside execute CreateBulkPaymentFileCreate ...");
        try {
            processor.process(bulkPayment);
        } catch (Exception e) {
            throw new WorkflowException(e);
        }

    }
}