package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostDeleteCustomerMobile implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostDeleteCustomerMobile.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside execute =========");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) arg0.get(WorkflowService.WF_ARG_BEAN);
		mobile.setIsRegistered(false);
		mobile.setIsActive(false);
		handleDevices(mobile);
		handleAccounts(mobile);
	}

	private void handleDevices(MPAY_CustomerMobile mobile) throws WorkflowException {
		logger.debug("inside HandleDevices =========");
		List<MPAY_CustomerDevice> devices = DataProvider.instance().listMobileDevices(mobile.getId());
		if (devices == null || devices.isEmpty())
			return;
		Iterator<MPAY_CustomerDevice> devicesIterator = devices.listIterator();
		do {
			MPAY_CustomerDevice device = devicesIterator.next();
			JfwHelper.executeAction(MPAYView.CUSTOMER_DEVICES, device, CustomerDeviceWorkflowServiceActions.DELETE, new WorkflowException());
		} while (devicesIterator.hasNext());
	}

	private void handleAccounts(MPAY_CustomerMobile mobile) throws WorkflowException {
		logger.debug("inside HandleAccounts =========");
		List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_MobileAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_MobileAccount account = accountsIterator.next();
			JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.DELETE, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
}