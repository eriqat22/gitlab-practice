package com.progressoft.mpay.payments.cashout;

import java.math.BigDecimal;
import java.util.Map;

import com.progressoft.mpay.payments.CashRequestHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CashOut;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.tax.TaxCalculator;

public class PostCashOutCalcuateCharge implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCashOutCalcuateCharge.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside execute =====================");
		MPAY_CashOut customerCashOut = (MPAY_CashOut) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (customerCashOut.getAmount() != null && customerCashOut.getAmount().doubleValue() > 0 && customerCashOut.getRefCustMob() != null) {
			String refAccount = CashRequestHelper.prepareRefMobileAccount(customerCashOut.getRefMobileAccount());
			customerCashOut.setRefMobileAccount(refAccount);
			MPAY_MobileAccount account = SystemHelper.getMobileAccount(MPayContext.getInstance().getSystemParameters(), customerCashOut.getRefCustMob(), refAccount);
			if (account == null)
				return;
			MPAY_Profile profile = account.getRefProfile();
			BigDecimal chargeAmount = ChargesCalculator.calculateCharge(MPayContext.getInstance().getDataProvider(), customerCashOut.getAmount(), MessageCodes.CO.toString(), profile, true);
			BigDecimal taxAmount = TaxCalculator.calculateTax(MPayContext.getInstance().getDataProvider(), chargeAmount, MessageCodes.CO.toString(), profile, MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
			customerCashOut.setTransAmount(customerCashOut.getAmount());
			customerCashOut.setChargeAmount(chargeAmount);
			customerCashOut.setTaxAmount(taxAmount);
			customerCashOut.setTotalAmount(customerCashOut.getAmount().add(chargeAmount).add(taxAmount));
		}
	}
}