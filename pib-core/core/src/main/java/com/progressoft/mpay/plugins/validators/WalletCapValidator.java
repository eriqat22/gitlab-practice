package com.progressoft.mpay.plugins.validators;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class WalletCapValidator {
	private static final Logger logger = LoggerFactory.getLogger(WalletCapValidator.class);

	private WalletCapValidator() {

	}

	public static ValidationResult validate(ProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		if(context.getReceiver().getProfile() == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		if (BigDecimal.ZERO.equals(context.getReceiver().getProfile().getLimitsScheme().getWalletCap()) || !context.getReceiver().getProfile().getLimitsScheme().getIsActive())
			return new ValidationResult(ReasonCodes.VALID, null, true);
		if (context.getReceiver().getAccount().getBalance().add(context.getAmount()).compareTo(context.getReceiver().getProfile().getLimitsScheme().getWalletCap()) > 0)
			return new ValidationResult(ReasonCodes.WALLET_CAP_REACHED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
