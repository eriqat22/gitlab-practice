package com.progressoft.mpay.genericentity;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.util.LoggerHelper;
import com.progressoft.mpay.entities.MPAY_CustomerKyc;
import com.progressoft.mpay.entities.MPAY_Profile;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Component
public class PreCreateGenericEntity implements FunctionProvider {

    private static final Logger logger = LoggerHelper.getLogger();
    @Resource
    private ItemDao itemDao;


    @Override
    public void execute(Map map, Map map1, PropertySet propertySet) throws WorkflowException {
        logger.debug("Inside execute ===== PreCreateGenericEntity");
        Object entity = map.get(WorkflowService.WF_ARG_BEAN);
        if (entity instanceof MPAY_CustomerKyc) {
            validateCreateCustomerKyc((MPAY_CustomerKyc) entity);
        }
    }

    private void validateCreateCustomerKyc(MPAY_CustomerKyc customerKyc) throws InvalidInputException {
        for (MPAY_Profile profile : customerKyc.getProfiles()) {
            List mappedProfiles = itemDao.getEntityManager().createNativeQuery("select PROFILES_ID from MPAY_CUSTOMERKYCPROFILES where PROFILES_ID='" + profile.getId() + "'").getResultList();
            if (!mappedProfiles.isEmpty()) {
                throw new InvalidInputException(profile.getName() + " is already used in another KYC template.");
            }
        }
    }
}
