package com.progressoft.mpay.registration.customers;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.registration.CommonClientProperties;

public abstract class CustomersChangeHandler {
	private static final Logger logger = LoggerFactory.getLogger(CustomersChangeHandler.class);

	protected abstract void handleExtraCustomerFields(ChangeHandlerContext changeHandlerContext, MPAY_Customer customer);
	protected abstract void handleExtraMobileFields(ChangeHandlerContext changeHandlerContext, MPAY_CustomerMobile mobile);
	protected abstract void handleExtraAccountFields(ChangeHandlerContext changeHandlerContext, MPAY_MobileAccount account);

	public void handleCustomer(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handleCustomer*******************-----------------");

		MPAY_Customer customer = (MPAY_Customer) changeHandlerContext.getEntity();
		if (customer.getStatusId() != null) {
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.MOBILE_NUMBER).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.ALIAS).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.BANKED_UNBANKED_PROP).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.BANK).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.EXTERNAL_ACC_PROP).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.IBAN).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.NOTIFICATION_SHOW_TYPE).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.NFC_SERIAL).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.REF_PROFILE_PROP).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(MPAY_Customer.ENABLE_EMAIL).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(MPAY_Customer.ENABLE_PUSH_NOTIFICATION).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(MPAY_Customer.ENABLE_S_M_S).setEnabled(false);
		} else {
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.MOBILE_NUMBER).setRequired(true);
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.MOBILE_NUMBER).setRegex(MPayContext.getInstance().getSystemParameters().getMobileNumberRegex());
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.BANKED_UNBANKED_PROP).setRequired(true);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.BANK).setRequired(true);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.MAS).setRequired(true);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.REF_PROFILE_PROP).setRequired(true);
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.REFERRAL_KEY).setVisible(false);
			customer.setIsActive(true);
		}
		if (customer.getIsRegistered()!=null && customer.getIsRegistered()) {
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.NATIONALITY).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.ID_TYPE).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.CLIENT_TYPE).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CustomerProperties.ID_NUM).setEnabled(false);
		}
		handleExtraCustomerFields(changeHandlerContext, customer);
	}

	public void handleCustomerMobile(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handleCustomerMobile*******************-----------------");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) changeHandlerContext.getEntity();
		if (mobile.getStatusId() == null) {
			mobile.setIsActive(true);
			return;
		}
		Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
		for (UIPortletAttributes ui : portletsAttributes) {
			ui.setVisible(true);
		}
		if (mobile.getIsRegistered()) {
			if (changeHandlerContext.getFieldsAttributes().get(CustomerProperties.REF_CUSTOMER) != null)
				changeHandlerContext.getFieldsAttributes().get(CustomerProperties.REF_CUSTOMER).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.ALIAS).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.BANKED_UNBANKED_PROP).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.BANK).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.EXTERNAL_ACC_PROP).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.IBAN).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.MAS).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.IS_REGISTERED).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.REF_PROFILE_PROP).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.RETRY_COUNT).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.IS_BLOCKED).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.MOBILE_NUMBER).setEnabled(false);
		}
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.MOBILE_NUMBER).setRegex(MPayContext.getInstance().getSystemParameters().getMobileNumberRegex());
		handleExtraMobileFields(changeHandlerContext, mobile);
	}

	public void handleMobileAccount(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handleMobileAccount ------------");
		MPAY_MobileAccount account = (MPAY_MobileAccount) changeHandlerContext.getEntity();
		if (account.getStatusId() == null) {
			account.setIsActive(true);
			return;
		}
		changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.IS_DEFAULT).setEnabled(false);
		changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.IS_SWITCH_DEFAULT).setEnabled(false);
		if (account.getStatusId() == null) {
			account.setIsActive(true);
			return;
		}
		if (account.getIsRegistered()) {
			if (changeHandlerContext.getFieldsAttributes().get(CustomerProperties.MOBILE) != null)
				changeHandlerContext.getFieldsAttributes().get(CustomerProperties.MOBILE).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.BANKED_UNBANKED_PROP).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.BANK).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.EXTERNAL_ACC_PROP).setEnabled(true);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.MAS).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.REF_PROFILE_PROP).setEnabled(true);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.REF_ACCOUNT).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.IS_DEFAULT).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(CommonClientProperties.IS_SWITCH_DEFAULT).setEnabled(false);
		}
		handleExtraAccountFields(changeHandlerContext, account);
	}
}
