package com.progressoft.mpay.mpclear.plugins;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.ICryptographer;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgType;
import com.progressoft.mpay.entities.MPAY_TransactionConfig;
import com.progressoft.mpay.notifications.INotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.solab.iso8583.IsoMessage;

public class MPClearProcessingContext {
	private MPAY_MpClearIntegMsgLog message;
	private IsoMessage isoMessage;
	private MPAY_MpClearIntegMsgType messageType;
	private MPAY_MpClearIntegMsgLog originalMessage;
	private MPAY_TransactionConfig transactionConfig;
	private MPAY_EndPointOperation operation;
	private IDataProvider dataProvider;
	private ILookupsLoader lookupsLoader;
	private ISystemParameters systemParameters;
	private String transactionNature;
	private ICryptographer cryptographer;
	private INotificationHelper notificationHelper;

	public MPClearProcessingContext(ProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");

		this.dataProvider = context.getDataProvider();
		this.lookupsLoader = context.getLookupsLoader();
		this.systemParameters = context.getSystemParameters();
		this.cryptographer = context.getCryptographer();
		this.notificationHelper = context.getNotificationHelper();
	}

	public MPAY_MpClearIntegMsgLog getMessage() {
		return message;
	}

	public void setMessage(MPAY_MpClearIntegMsgLog message) {
		this.message = message;
	}

	public IsoMessage getIsoMessage() {
		return isoMessage;
	}

	public void setIsoMessage(IsoMessage isoMessage) {
		this.isoMessage = isoMessage;
	}

	public MPAY_MpClearIntegMsgType getMessageType() {
		return messageType;
	}

	public void setMessageType(MPAY_MpClearIntegMsgType messageType) {
		this.messageType = messageType;
	}

	public MPAY_MpClearIntegMsgLog getOriginalMessage() {
		return originalMessage;
	}

	public void setOriginalMessage(MPAY_MpClearIntegMsgLog originalMessage) {
		this.originalMessage = originalMessage;
	}

	public MPAY_TransactionConfig getTransactionConfig() {
		return transactionConfig;
	}

	public void setTransactionConfig(MPAY_TransactionConfig transactionConfig) {
		this.transactionConfig = transactionConfig;
	}

	public MPAY_EndPointOperation getOperation() {
		return operation;
	}

	public void setOperation(MPAY_EndPointOperation operation) {
		this.operation = operation;
	}

	public IDataProvider getDataProvider() {
		return this.dataProvider;
	}

	public ILookupsLoader getLookupsLoader() {
		return this.lookupsLoader;
	}

	public ISystemParameters getSystemParameters() {
		return this.systemParameters;
	}

	public String getTransactionNature() {
		return transactionNature;
	}

	public void setTransactionNature(String transactionNature) {
		this.transactionNature = transactionNature;
	}

	public ICryptographer getCryptographer() {
		return cryptographer;
	}

	public void setCryptographer(ICryptographer cryptographer) {
		this.cryptographer = cryptographer;
	}

	public INotificationHelper getNotificationHelper() {
		return notificationHelper;
	}

	public void setNotificationHelper(INotificationHelper notificationHelper) {
		this.notificationHelper = notificationHelper;
	}
}
