package com.progressoft.mpay.registration;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.nio.charset.Charset;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.NotImplementedException;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.SmsNotificationType;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CorpIntegMessage;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.CorporateManagementActionTypes;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.registration.corporates.CorporateServiceWorkflowServiceActions;
import com.progressoft.mpay.registration.corporates.CorporateServiceWorkflowStatuses;
import com.progressoft.mpay.registration.corporates.CorporateWorkflowServiceActions;
import com.progressoft.mpay.registration.corporates.CorporateWorkflowStatuses;
import com.progressoft.mpay.registration.corporates.ServiceAccountWorkflowServiceActions;
import com.solab.iso8583.IsoMessage;

public class PostProcessCorp1710MpClearIntegMsg implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostProcessCorp1710MpClearIntegMsg.class);
	protected EntityManager em;

	@Autowired
	private ItemDao itemDao;

	@PersistenceContext(unitName = "JFWUnit")
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map arg1, PropertySet ps) throws WorkflowException {

		logger.debug("inside PostProcessCorp1710MpClearIntegMsg function .");
		MPAY_CorpIntegMessage response = (MPAY_CorpIntegMessage) transientVars.get(WorkflowService.WF_ARG_BEAN);
		long reason = Long.parseLong(response.getResponse().getCode());

		try {
			if (reason == 0) {
				handleMPClearResponse(response, true);
			} else if (reason > 0 && reason < 99) {
				handleMPClearResponse(response, false);
			}
		} catch (JAXBException exception) {
			logger.error("Error while processing registration response ", exception);
		}
	}

	private void handleMPClearResponse(MPAY_CorpIntegMessage response, boolean isAccepted)
			throws WorkflowException, JAXBException {
		if (CorporateManagementActionTypes.ADD_CLIENT.compare(response.getRefOriginalMsg().getActionType())) {
			handleAddClient(response, isAccepted);
		} else if (CorporateManagementActionTypes.UPDATE_CLIENT.compare(response.getRefOriginalMsg().getActionType())) {
			handleUpdateClient(response, isAccepted);
		} else if (CorporateManagementActionTypes.REMOVE_CLIENT.compare(response.getRefOriginalMsg().getActionType())) {
			handleRemoveClient(response, isAccepted);
		} else if (CorporateManagementActionTypes.ADD_ACCOUNT.compare(response.getRefOriginalMsg().getActionType())) {
			handleAddAccount(response, isAccepted);
		} else if (CorporateManagementActionTypes.REMOVE_ACCOUNT
				.compare(response.getRefOriginalMsg().getActionType())) {
			handleRemoveAccount(response, isAccepted);
		} else if (CorporateManagementActionTypes.CHANGE_ALIAS.compare(response.getRefOriginalMsg().getActionType())) {
			handleChangeAlias(response, isAccepted);
		} else if (CorporateManagementActionTypes.SET_DEFAULT_ACCOUNT
				.compare(response.getRefOriginalMsg().getActionType())) {
			handleSetDefaultAccount(response, isAccepted);
		} else {
			throw new NotImplementedException("Corporate integration action code: '"
					+ response.getRefOriginalMsg().getActionType().getCode() + "' is not implemented");
		}
	}

	private void handleAddAccount(MPAY_CorpIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_ServiceAccount account = getAccount(response);
		MPAYView view;
		JFWEntity entity;
		if (!account.getService().getRefCorporate().getIsRegistered()) {
			view = MPAYView.CORPORATE;
			entity = account.getService().getRefCorporate();
		} else if (!account.getService().getIsRegistered()) {
			view = MPAYView.CORPORATE_SERVICE_VIEWS;
			entity = account.getService();
		} else {
			view = MPAYView.SERVICE_ACCOUNTS_VIEW;
			entity = account;
		}

		if (isAccepted)
			JfwHelper.executeAction(view, entity, CorporateWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(view, entity, CorporateWorkflowServiceActions.REJECT, new WorkflowException());
	}

	private void handleAddClient(MPAY_CorpIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_Corporate corporate = response.getRefCorporate();
		if (isAccepted)
			JfwHelper.executeAction(MPAYView.CORPORATE, corporate, CorporateWorkflowServiceActions.APPROVE,
					new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.CORPORATE, corporate, CorporateWorkflowServiceActions.REJECT,
					new WorkflowException());
	}

	private void handleUpdateClient(MPAY_CorpIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_Corporate corporate = response.getRefCorporate();
		MPAY_CorpoarteService service = response.getRefService();
		try {
			if (corporate.getJfwDraft() != null)
				corporate = (MPAY_Corporate) JfwHelper.createObjectFromDraft(
						response.getRefCorporate().getJfwDraft().getDraftData(), response.getRefCorporate(), itemDao);
			if (service.getJfwDraft() != null)
				service = (MPAY_CorpoarteService) JfwHelper.createObjectFromDraft(
						response.getRefService().getJfwDraft().getDraftData(), response.getRefService(), itemDao);
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
		if (corporate == null)
			corporate = response.getRefCorporate();
		if (service == null)
			service = response.getRefService();

		MPAYView view = null;
		JFWEntity entity = null;
		if (corporate.getStatusId().getCode().equals(CorporateWorkflowStatuses.INTEGRATION)) {
			view = MPAYView.CORPORATE;
			entity = corporate;
		} else if (service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.INTEGRATION.toString())) {
			view = MPAYView.CORPORATE_SERVICE_VIEWS;
			entity = service;
		}
		if (isAccepted)
			JfwHelper.executeAction(view, entity, CorporateWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(view, entity, CorporateWorkflowServiceActions.REJECT, new WorkflowException());
	}

	private void handleRemoveClient(MPAY_CorpIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_Corporate corporate = response.getRefCorporate();
		if (isAccepted)
			JfwHelper.executeAction(MPAYView.CORPORATE, corporate, CorporateWorkflowServiceActions.DELETE,
					new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.CORPORATE, corporate, CorporateWorkflowServiceActions.REJECT,
					new WorkflowException());
	}

	private void handleRemoveAccount(MPAY_CorpIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_ServiceAccount account = getAccount(response);
		MPAY_CorpoarteService service = response.getRefService();
		MPAYView view;
		JFWEntity entity;
		if (service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.INTEGRATION.toString())) {
			view = MPAYView.CORPORATE_SERVICE_VIEWS;
			entity = service;
		} else {
			view = MPAYView.SERVICE_ACCOUNTS_VIEW;
			entity = account;
		}
		if (isAccepted)
			JfwHelper.executeAction(view, entity, CorporateWorkflowServiceActions.DELETE, new WorkflowException());
		else
			JfwHelper.executeAction(view, entity, CorporateWorkflowServiceActions.REJECT, new WorkflowException());
	}

	private void handleSetDefaultAccount(MPAY_CorpIntegMessage response, boolean isAccepted) throws WorkflowException {
		MPAY_CorpoarteService service = response.getRefService();
		MPAY_ServiceAccount account = getAccount(response);

		if (isAccepted) {
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account,
					ServiceAccountWorkflowServiceActions.APPROVE, new WorkflowException());
			for (MPAY_ServiceAccount acc : service.getServiceServiceAccounts()) {
				if (acc.getId() != account.getId() && acc.getIsSwitchDefault()) {
					acc.setIsSwitchDefault(false);
					DataProvider.instance().updateServiceAccount(acc);
				}
			}
		} else {
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account,
					ServiceAccountWorkflowServiceActions.DEFAULT_REJECT, new WorkflowException());
		}
	}

	private MPAY_ServiceAccount getAccount(MPAY_CorpIntegMessage response) throws WorkflowException {
		MPAY_CorpIntegMessage originalMessage = response.getRefOriginalMsg();
		IsoMessage originalIsoMessage = ISO8583Parser
				.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath())
				.parseMessage(originalMessage.getRefMsgLog().getContent());
		String registrationId = originalIsoMessage.getField(62).getValue().toString();
		byte[] bytes = Base64.decodeBase64(registrationId);
		registrationId = new String(bytes, Charset.forName("UTF-16LE"));
		String routeCode = registrationId.substring(0, SystemParameters.getInstance().getRoutingCodeLength());
		long mas = Long.parseLong(registrationId.substring(SystemParameters.getInstance().getRoutingCodeLength(),
				SystemParameters.getInstance().getRoutingCodeLength()
						+ SystemParameters.getInstance().getMobileAccountSelectorLength()));
		MPAY_CorpoarteService service = response.getRefService();
		return selectFirst(service.getServiceServiceAccounts(),
				having(on(MPAY_ServiceAccount.class).getBank().getCode(), Matchers.equalTo(routeCode))
						.and(having(on(MPAY_ServiceAccount.class).getMas(), Matchers.equalTo(mas))));
	}

	private void handleChangeAlias(MPAY_CorpIntegMessage response, boolean isAccepted)
			throws WorkflowException, JAXBException {
		MPAY_EndPointOperation aliasOperation;
		if (response.getRefOriginalMsg().getRefMsgLog().getRefMessage() == null)
			aliasOperation = LookupsLoader.getInstance()
					.getEndPointOperation(SystemParameters.getInstance().getAliasChangeEndPointOperation());
		else
			aliasOperation = response.getRefOriginalMsg().getRefMsgLog().getRefMessage().getRefOperation();
		MessageProcessor processor = MessageProcessorHelper.getProcessor(aliasOperation);
		if (processor == null)
			throw new WorkflowException("Alias Change Processor with code = "
					+ SystemParameters.getInstance().getAliasChangeEndPointOperation()
					+ " not found, make sure to change it in system configuration");
		MPClearProcessingContext context = new MPClearProcessingContext(
				new ProcessingContext(DataProvider.instance(), LookupsLoader.getInstance(),
						SystemParameters.getInstance(), new MPayCryptographer(), NotificationHelper.getInstance()));
		context.setMessage(response.getRefMsgLog());
		context.setIsoMessage(ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath())
				.parseMessage(response.getRefMsgLog().getContent()));
		context.setOperation(aliasOperation);
		context.setOriginalMessage(response.getRefOriginalMsg().getRefMsgLog());
		context.setMessageType(LookupsLoader.getInstance().getMpClearIntegrationMessageTypes("1710"));
		IntegrationProcessingContext integContext = new IntegrationProcessingContext(context);
		IntegrationProcessingResult result = processor.processIntegration(integContext);
		if (result != null) {
			for (MPAY_Notification notification : result.getNotifications()) {
				DataProvider.instance().persistNotification(notification);
				sendNotificationToMQ(notification);
			}
		}
		if (!response.getRefService().getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.INTEGRATION.toString()))
			return;

		if (isAccepted)
			JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, response.getRefService(),
					CorporateServiceWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, response.getRefService(),
					CorporateServiceWorkflowServiceActions.REJECT_ALIAS, new WorkflowException());
	}

	private void sendNotificationToMQ(MPAY_Notification notification) throws WorkflowException, JAXBException {
		JfwHelper.sendToActiveMQ(notification, notification.getTenantId(), MPAYView.NOTIFICATIONS.viewName,
				SmsNotificationType.GENERAL.getQueueName());
	}
}