package com.progressoft.mpay.transactions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.entities.MPAY_JvDetail;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.BalancePostingResult;
import com.progressoft.mpay.entity.JournalVoucherFlags;
import com.progressoft.mpay.entity.JvDetailsTypes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.TransactionHelper;

public class AccountingHelper {
	private static final Logger logger = LoggerFactory.getLogger(TransactionHelper.class);

	private AccountingHelper() {

	}

	public static JVPostingResult postAccounts(IDataProvider dataProvider, ILookupsLoader lookupsLoader, MPAY_Transaction transaction) {
		logger.debug("Inside PostAccounts ...");
		if (dataProvider == null)
			throw new NullArgumentException("dataProvider");
		if (lookupsLoader == null)
			throw new NullArgumentException("lookupsLoader");
		if (transaction == null)
			throw new NullArgumentException("transaction");
		if (transaction.getId()!=null && transaction.getId() == 0)
			throw new InvalidOperationException("transaction should be persisted before posting");

		List<MPAY_JvDetail> postedJVs = new ArrayList<>();
		JVPostingResult result = JVPostingResult.success();
		List<MPAY_JvDetail> jvs = transaction.getRefTrsansactionJvDetails();
		jvs.sort(Comparator.comparing(MPAY_JvDetail::getSerial));
		for (MPAY_JvDetail jv : jvs) {
			result = postJV(dataProvider, jv);
			if (!result.isSuccess())
				break;
			postedJVs.add(jv);
		}

		if (!result.isSuccess() && !postedJVs.isEmpty())
			reverseJVs(dataProvider, lookupsLoader, transaction, postedJVs);

		return result;
	}

	private static JVPostingResult postJV(IDataProvider dataProvider, MPAY_JvDetail jv) {
		logger.debug("Inside PostJV ...");
		if (jv.getId() !=null && jv.getId() == 0)
			throw new InvalidOperationException("jv should be persisted before posting");
		JVPostingResult result = new JVPostingResult();
		if (jv.getIsPosted() || jv.getRefAccount().getIsBanked()) {
			result.setSuccess(true);
			return result;
		}
		try {
			BalancePostingResult postingResult = dataProvider.callUpdateBalanceSP(jv.getRefAccount().getId(), jv.getAmount(), jv.getDebitCreditFlag().equals(JournalVoucherFlags.DEBIT));
			result.setSuccess(postingResult.isSuccess());
			if (!result.isSuccess()) {
				jv.setBalanceBefore(jv.getRefAccount().getBalance());
				jv.setBalanceAfter(jv.getRefAccount().getBalance());
				result.setReason(ReasonCodes.INSUFFICIENT_FUNDS);
			} else {
				jv.setIsPosted(true);
				jv.setBalanceBefore(postingResult.getBalanceBefore());
				jv.setBalanceAfter(postingResult.getBalanceAfter());
			}
		} catch (SQLException e) {
			logger.debug("Error while postJV", e);
			result.setSuccess(false);
			result.setReason(e.getMessage());
		}

		return result;
	}

	private static void reverseJVs(IDataProvider dataProvider, ILookupsLoader lookupsLoader, MPAY_Transaction transaction, List<MPAY_JvDetail> postedJVs) {
		logger.debug("Inside ReverseJVs ...");
		List<MPAY_JvDetail> reversalJVs = new ArrayList<>();
		for (MPAY_JvDetail jv : postedJVs) {
			if (jv.getRefAccount().getIsBanked())
				continue;
			String debitCreditType;
			if (jv.getDebitCreditFlag().equals(JournalVoucherFlags.DEBIT))
				debitCreditType = JournalVoucherFlags.CREDIT;
			else
				debitCreditType = JournalVoucherFlags.DEBIT;
			MPAY_JvDetail reversalJV = TransactionCreator.createJVDetail(transaction, debitCreditType, jv.getRefAccount(), jv.getAmount(), lookupsLoader.getJvTypes(JvDetailsTypes.REVERSAL.toString()),
					"Reverse - " + jv.getDescription());
			reversalJVs.add(reversalJV);
		}
		for (MPAY_JvDetail jv : transaction.getRefTrsansactionJvDetails()) {
			if (jv.getBalanceBefore() == null)
				jv.setBalanceBefore(jv.getRefAccount().getBalance());
			if (jv.getBalanceAfter() == null)
				jv.setBalanceAfter(jv.getRefAccount().getBalance());
		}
		dataProvider.mergeTransaction(transaction);
		for (MPAY_JvDetail jv : reversalJVs) {
			postJV(dataProvider, jv);
		}
	}
}
