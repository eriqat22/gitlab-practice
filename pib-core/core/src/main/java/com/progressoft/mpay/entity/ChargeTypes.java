package com.progressoft.mpay.entity;

public final class ChargeTypes {
	public static final String FIXED = "1";
	public static final String PERCENTAGE = "2";

	private ChargeTypes() {

	}
}
