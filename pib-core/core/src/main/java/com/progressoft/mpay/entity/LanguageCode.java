package com.progressoft.mpay.entity;

public final class LanguageCode {
    public static final String ENGLISH = "1";
    public static final String ARBIC = "3";

    private LanguageCode() {
    }

}
