package com.progressoft.mpay.payments;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_SystemAccountsCashOut;

public class SystemAccountsCashOutHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(SystemAccountsCashOutHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside SystemAccountsCashOutHandler ------------");
		MPAY_SystemAccountsCashOut entity = (MPAY_SystemAccountsCashOut) changeHandlerContext.getEntity();
		if (entity.getStatusId() == null) {
			entity.setCurrency(SystemParameters.getInstance().getDefaultCurrency());
		} else {
			changeHandlerContext.getFieldsAttributes().get("sourceService").setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get("currency").setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get("amount").setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get("comments").setEnabled(false);
		}
	}
}