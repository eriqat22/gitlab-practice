package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.CustomerMobileMetaData;
import com.progressoft.mpay.entity.MPAYView;

public class PostCancelCustomerMobileApproved implements FunctionProvider {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(PostCancelCustomerMobileApproved.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =============");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) arg0.get(WorkflowService.WF_ARG_BEAN);
		handleAccounts(mobile);
		revertMobile(mobile);
	}

	private void revertMobile(MPAY_CustomerMobile mobile) {
		if (mobile.getApprovedData() == null)
			return;

		mobile.setNewAlias(null);
		mobile.setNewMobileNumber(null);

		CustomerMobileMetaData approvedMobile = CustomerMobileMetaData.fromJson(mobile.getApprovedData());
		approvedMobile.fillCustomerMobileFromMetaData(mobile);
		mobile.setApprovedData(null);

		DataProvider.instance().mergeCustomerMobile(mobile);
	}

	private void handleAccounts(MPAY_CustomerMobile mobile) throws WorkflowException {
		logger.debug("Inside HandleAccounts =============");
		List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId(), MobileAccountWorkflowStatuses.CANCELING.toString());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_MobileAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_MobileAccount account = accountsIterator.next();
			JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.APPROVE, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
}