package com.progressoft.mpay.sms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author u445
 *
 *	this class for response from the integration point back to MPAY ,
 *	the status should back is SUCCESS or FAIL
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status","errorDescription"
})
@XmlRootElement(name="NotificationResponse")
public class NotificationResponse {

	@XmlElement(name="Status")
	private String status;

	@XmlElement(name="ErrorDescription")
	private String errorDescription;

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getErrorDescription() {
		return errorDescription;
	}
}