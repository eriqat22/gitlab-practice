package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostDeleteCorporateServiceMPClearApproval implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostDeleteCorporateServiceMPClearApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside execute =========");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
		service.setIsRegistered(false);
		service.setIsActive(false);
		handleDevices(service);
		handleAccounts(service);
	}

	private void handleDevices(MPAY_CorpoarteService service) throws WorkflowException {
		logger.debug("inside HandleDevices =========");
		List<MPAY_CorporateDevice> devices = DataProvider.instance().listServiceDevices(service.getId());
		if (devices == null || devices.isEmpty())
			return;
		Iterator<MPAY_CorporateDevice> devicesIterator = devices.listIterator();
		do {
			MPAY_CorporateDevice device = devicesIterator.next();
			JfwHelper.executeAction(MPAYView.CORPORATE_DEVICES, device, CorporateDeviceWorkflowServiceActions.DELETE, new WorkflowException());
		} while (devicesIterator.hasNext());
	}

	private void handleAccounts(MPAY_CorpoarteService service) throws WorkflowException {
		logger.debug("inside HandleAccounts =========");
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_ServiceAccount account = accountsIterator.next();
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.DELETE, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
}