package com.progressoft.mpay.registration;

import java.util.List;

import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class ValidateNoMobilesAreOnModifiedState {
	public static void  validate(MPAY_Customer customer) throws WorkflowException {
		InvalidInputException iie = new InvalidInputException();
		List<MPAY_CustomerMobile> mobiles = customer.getRefCustomerCustomerMobiles();
		for (MPAY_CustomerMobile mobile : mobiles) {
			if (mobile.getStatusId().getDescription().equals("Modified")
					|| mobile.getStatusId().getDescription().equals("Under Modification")) {
				iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("mobile.under.modification"));
				throw iie;
			}
		}
	}
}
