package com.progressoft.mpay;

import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ClientTypes;
import com.progressoft.mpay.entity.MPClearCache;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.entity.PSPCache;
import com.progressoft.mpay.mpclearinteg.MPClearFinancialProcessor;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ch.lambdaj.Lambda.*;

public class LookupsCache {
    public static final String CLIENT_TYPE_CODE = "clientType.code";
    public static final String LANGUAGE_ID = "languageId: ";
    public static final String CODE = "code: ";
    public static final String CITY_CODE = "id";
    private static final Logger logger = LoggerFactory.getLogger(MPClearFinancialProcessor.class);
    private static final String CITY_NAME = "name ";

    @Autowired
    private ItemDao itemDao;

    public LookupsCache() {
        logger.debug("Inside LookupsCache....");
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_City> listCities() {
        logger.debug("Inside ListCities....");
        return (List<MPAY_City>) getListFromCache("mpay.City", MPAY_City.class);
    }

    @SuppressWarnings("unchecked")
    public List<JfwCountry> listCountries() {
        logger.debug("Inside ListCountries....");
        return (List<JfwCountry>) getListFromCache("mpay.Country", JfwCountry.class);
    }

    @SuppressWarnings("unchecked")
    public List<JFWCurrency> listCurrencies() {
        logger.debug("Inside ListCurrencies....");
        return (List<JFWCurrency>) getListFromCache("mpay.Currency", JFWCurrency.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_ClientType> listClientTypes() {
        logger.debug("Inside listClientTypes....");
        return (List<MPAY_ClientType>) getListFromCache("mpay.ClientType", MPAY_ClientType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_IDType> listIDTypes() {
        logger.debug("Inside ListIDTypes....");
        return (List<MPAY_IDType>) getListFromCache("mpay.IDType", MPAY_IDType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_Language> listLanguages() {
        logger.debug("Inside ListLanguages....");
        return (List<MPAY_Language>) getListFromCache("mpay.Language", MPAY_Language.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_ChannelType> listChannelTypes() {
        logger.debug("Inside listChannelTypes....");
        return (List<MPAY_ChannelType>) getListFromCache("mpay.ChannelType", MPAY_ChannelType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_BalanceType> listBalanceTypes() {
        logger.debug("Inside listBalanceTypes....");
        return (List<MPAY_BalanceType>) getListFromCache("mpay.BalanceType", MPAY_BalanceType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_MpClearIntegMsgType> listMPClearIntegMessageTypes() {
        logger.debug("Inside LstMPClearIntegMessageTypes....");
        return (List<MPAY_MpClearIntegMsgType>) getListFromCache("mpay.MpClearIntegrationMessageTypes", MPAY_MpClearIntegMsgType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_CustIntegActionType> listCustomerIntegActionTypes() {
        logger.debug("Inside ListCustomerIntegActionTypes....");
        return (List<MPAY_CustIntegActionType>) getListFromCache("mpay.CustomerIntegrationActionTypes", MPAY_CustIntegActionType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_MpClearIntegRjctReason> listMPClearRejectionReasons() {
        logger.debug("Inside ListMPClearRejectionReasons....");
        return (List<MPAY_MpClearIntegRjctReason>) getListFromCache("mpay.MpClearIntegrationReasons", MPAY_MpClearIntegRjctReason.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_MpClearResponseCode> listMPClearIntegResponseCodes() {
        logger.debug("Inside ListMPClearIntegResponseCodes....");
        return (List<MPAY_MpClearResponseCode>) getListFromCache("mpay.MpClearIntegrationResponseCodes", MPAY_MpClearResponseCode.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_NetworkManActionType> listNetworkManagementActionTypes() {
        logger.debug("Inside listNetworkManagementActionTypes....");
        return (List<MPAY_NetworkManActionType>) getListFromCache("mpay.NetworkManagementActionTypes", MPAY_NetworkManActionType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_JvType> listJVTypes() {
        logger.debug("Inside listJVTypes....");
        return (List<MPAY_JvType>) getListFromCache("mpay.JvTypes", MPAY_JvType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_AccountType> listAccountTypes() {
        logger.debug("Inside listAccountTypes....");
        return (List<MPAY_AccountType>) getListFromCache("mpay.AccountTypes", MPAY_AccountType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_Reason> listReasons() {
        logger.debug("Inside listReasons....");
        return (List<MPAY_Reason>) getListFromCache("mpay.Reason", MPAY_Reason.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_SysConfig> listSystemConfigurations() {
        logger.debug("Inside listSystemConfigurations....");
        return (List<MPAY_SysConfig>) getListFromCache("mpay.SystemConfigurationsList", MPAY_SysConfig.class);
    }



    @SuppressWarnings("unchecked")
    public List<MPAY_TransactionsConfig> listTransactionConfigs() {
        logger.debug("Inside listTransactionConfigs....");
        return (List<MPAY_TransactionsConfig>) getListFromCache("mpay.TransactionsConfig", MPAY_TransactionsConfig.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_MessageType> listMessageTypes() {
        logger.debug("Inside listMessageTypes....");
        return (List<MPAY_MessageType>) getListFromCache("mpay.MessageTypes", MPAY_MessageType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_Profile> listProfiles() {
        logger.debug("Inside listProfiles....");
        return (List<MPAY_Profile>) getListFromCache("mpay.Profiles", MPAY_Profile.class);
    }

    @SuppressWarnings("unchecked")
    List<MPAY_Reg_File_Stt> listRegFileStatuses() {
        logger.debug("Inside listRegFileStatuses....");
        return (List<MPAY_Reg_File_Stt>) getListFromCache("mpay.Reg_File_Stts", MPAY_Reg_File_Stt.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_Reg_Instrs_Stt> listRegInstructionStatuses() {
        logger.debug("Inside listRegInstructionStatuses....");
        return (List<MPAY_Reg_Instrs_Stt>) getListFromCache("mpay.Reg_Instrs_Stts", MPAY_Reg_Instrs_Stt.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_Bank> listBanks() {
        logger.debug("Inside listBanks....");
        return (List<MPAY_Bank>) getListFromCache("mpay.banks", MPAY_Bank.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_ePayStatusCode> listEPayStatusCodes() {
        logger.debug("Inside listEPayStatusCodes....");
        return (List<MPAY_ePayStatusCode>) getListFromCache("mpay.ePayStatusCode", MPAY_ePayStatusCode.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_NotificationTemplate> listNotificationTemplates() {
        logger.debug("Inside listNotificationTemplates....");
        return (List<MPAY_NotificationTemplate>) getListFromCache("mpay.NotificationTemplates", MPAY_NotificationTemplate.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_BankIntegStatus> listBankIntegStatuses() {
        logger.debug("Inside listBankIntegStatuses....");
        return (List<MPAY_BankIntegStatus>) getListFromCache("mpay.BankIntegStatus", MPAY_BankIntegStatus.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_EndPointOperation> listEndPointOperations() {
        logger.debug("Inside listEndPointOperations....");
        return (List<MPAY_EndPointOperation>) getListFromCache("mpay.EndPointOperations", MPAY_EndPointOperation.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_ProcessingStatus> listProcessingStatuses() {
        logger.debug("Inside listProcessingStatuses....");
        return (List<MPAY_ProcessingStatus>) getListFromCache("mpay.ProcessingStatuses", MPAY_ProcessingStatus.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_TransactionConfig> listTransactionsConfigs() {
        logger.debug("Inside listTransactionsConfigs....");
        return (List<MPAY_TransactionConfig>) getListFromCache("mpay.TransactionsConfigs", MPAY_TransactionConfig.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_TransactionDirection> listTransactionsDirections() {
        logger.debug("Inside listTransactionsDirections....");
        return (List<MPAY_TransactionDirection>) getListFromCache("mpay.TransactionDirections", MPAY_TransactionDirection.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_LimitsType> listLimitsTypes() {
        logger.debug("Inside listLimitsTypes....");
        return (List<MPAY_LimitsType>) getListFromCache("mpay.LimitsTypes", MPAY_LimitsType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_TransactionType> listTransactionTypes() {
        logger.debug("Inside listTransactionTypes....");
        return (List<MPAY_TransactionType>) getListFromCache("mpay.TransactionTypes", MPAY_TransactionType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_Reasons_NLS> listReasonsNLS() {
        logger.debug("Inside listReasonsNLS....");
        return (List<MPAY_Reasons_NLS>) getListFromCache("mpay.ReasonsNLS", MPAY_Reasons_NLS.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_NotificationChannel> listNotificationChannels() {
        logger.debug("Inside listNotificationChannels....");
        return (List<MPAY_NotificationChannel>) getListFromCache("mpay.notificationChannels", MPAY_NotificationChannel.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_ServiceType> listServiceTypes() {
        logger.debug("Inside listServiceTypes....");
        return (List<MPAY_ServiceType>) getListFromCache("mpay.serviceTypes", MPAY_ServiceType.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_ServiceIntegReason> listServiceIntegReasons() {
        logger.debug("Inside listServiceIntegReasons....");
        return (List<MPAY_ServiceIntegReason>) getListFromCache("mpay.ServiceIntegReasons", MPAY_ServiceIntegReason.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_ServiceIntegReasons_NLS> listServiceIntegReasonsNLS() {
        logger.debug("Inside MPAY_ServiceIntegReasons_NLS....");
        return (List<MPAY_ServiceIntegReasons_NLS>) getListFromCache("mpay.ServiceIntegReasonsNLS", MPAY_ServiceIntegReasons_NLS.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_AppsChecksum> listAppsChecksums() {
        logger.debug("Inside listAppsChecksums....");
        return (List<MPAY_AppsChecksum>) getListFromCache("mpay.appschecksums", MPAY_AppsChecksum.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_ServicesCategory> listServiceCategories() {
        logger.debug("Inside listServiceCategories....");
        return (List<MPAY_ServicesCategory>) getListFromCache("mpay.serviceCategories", MPAY_ServicesCategory.class);
    }

    public MPAY_ServicesCategory getServiceCategory(String code) {
        logger.debug("Inside getServiceCategory....");
        logger.debug("code: " + code);
        List<MPAY_ServicesCategory> categories = listServiceCategories();
        return selectFirst(categories, having(on(MPAY_ServicesCategory.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_ServicesCategory getServiceCategory(long id) {
        logger.debug("Inside getServiceCategory....");
        logger.debug("id: " + id);
        List<MPAY_ServicesCategory> categories = listServiceCategories();
        return selectFirst(categories, having(on(MPAY_ServicesCategory.class).getId(), Matchers.equalTo(id)));
    }

    public MPAY_AppsChecksum getAppChecksum(String code) {
        logger.debug("Inside getAppChecksum....");
        logger.debug("checksum: " + code);

        List<MPAY_AppsChecksum> checksums = listAppsChecksums();
        return selectFirst(checksums, having(on(MPAY_AppsChecksum.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_ServiceIntegReasons_NLS getServiceIntegReasonNLS(long reasonId, long languageId) {
        logger.debug("Inside getServiceIntegReasonNLS....");
        logger.debug("reasonId: " + reasonId);
        logger.debug(LANGUAGE_ID + languageId);
        List<MPAY_ServiceIntegReasons_NLS> reasons = listServiceIntegReasonsNLS();
        return selectFirst(reasons, having(on(MPAY_ServiceIntegReasons_NLS.class).getRsnId().getId(), Matchers.equalTo(reasonId))
                .and(having(on(MPAY_ServiceIntegReasons_NLS.class).getId(), Matchers.equalTo(languageId))));
    }

    public MPAY_ServiceIntegReason getServiceIntegReason(String code) {
        logger.debug("Inside getServiceIntegReason....");
        logger.debug(CODE + code);
        List<MPAY_ServiceIntegReason> reasons = listServiceIntegReasons();
        return selectFirst(reasons, having(on(MPAY_ServiceIntegReason.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_Profile getProfile(String code) {
        logger.debug("Inside getProfile....");
        logger.debug(CODE + code);
        List<MPAY_Profile> profiles = listProfiles();
        return selectFirst(profiles, having(on(MPAY_Profile.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_Profile getProfile(long id) {
        logger.debug("Inside getProfile....");
        logger.debug("id: " + id);
        List<MPAY_Profile> profiles = listProfiles();
        return selectFirst(profiles, having(on(MPAY_Profile.class).getId(), Matchers.equalTo(id)));
    }

    public MPAY_ServiceType getServiceType(String code) {
        logger.debug("Inside getServiceType....");
        logger.debug(CODE + code);
        List<MPAY_ServiceType> types = listServiceTypes();
        return selectFirst(types, having(on(MPAY_ServiceType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_NotificationChannel getNotificationChannel(String code) {
        logger.debug("Inside getNotificationChannel....");
        logger.debug(CODE + code);
        List<MPAY_NotificationChannel> channels = listNotificationChannels();
        return selectFirst(channels, having(on(MPAY_NotificationChannel.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_Reasons_NLS getReasonNLS(long reasonId, String languageCode) {
        logger.debug("Inside getReasonNLS....");
        logger.debug("reasonId: " + reasonId);
        logger.debug(LANGUAGE_ID + languageCode);
        List<MPAY_Reasons_NLS> reasons = listReasonsNLS();
        return reasonNls(reasonId, reasons, languageCode);
    }

    private MPAY_Reasons_NLS reasonNls(long reasonId, List<MPAY_Reasons_NLS> reasons, String languageCode) {
        Optional<MPAY_Reasons_NLS> reasonNls = reasons.stream().filter(reason -> reason.getRsnId().getId() == reasonId && languageCode.equals(reason.getLanguageCode())).findFirst();
        return reasonNls.isPresent() ? reasonNls.get() : null;
    }

    public MPAY_TransactionType getTransactionType(String code) {
        logger.debug("Inside getTransactionType....");
        logger.debug(CODE + code);
        List<MPAY_TransactionType> directions = listTransactionTypes();
        return selectFirst(directions, having(on(MPAY_TransactionType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_BalanceType getBalanceType(String code) {
        logger.debug("Inside getBalanceType....");
        logger.debug(CODE + code);
        List<MPAY_BalanceType> types = listBalanceTypes();
        return selectFirst(types, having(on(MPAY_BalanceType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_TransactionDirection getTransactionDirection(long id) {
        logger.debug("Inside getTransactionDirection....");
        logger.debug("id: " + id);
        List<MPAY_TransactionDirection> directions = listTransactionsDirections();
        return selectFirst(directions, having(on(MPAY_TransactionDirection.class).getId(), Matchers.equalTo(id)));
    }

    public MPAY_TransactionConfig getTransactionConfig(long senderType, boolean isSenderBanked, long receiverType, boolean isReceiverBanked, long messageType, String transactionTypeCode,
                                                       long operationId) {
        logger.debug("Inside getTransactionConfig....");
        logger.debug("senderType: " + senderType);
        logger.debug("isSenderBanked: " + isSenderBanked);
        logger.debug("isReceiverBanked: " + isReceiverBanked);
        logger.debug("transactionTypeCode: " + transactionTypeCode);
        List<MPAY_TransactionConfig> configs = listTransactionsConfigs();
        return selectFirst(configs,
                having(on(MPAY_TransactionConfig.class).getSenderType().getId(), Matchers.equalTo(senderType))
                        .and(having(on(MPAY_TransactionConfig.class).getReceiverType().getId(), Matchers.equalTo(receiverType)))
                        .and(having(on(MPAY_TransactionConfig.class).getMessageType().getId(), Matchers.equalTo(messageType)))
                        .and(having(on(MPAY_TransactionConfig.class).getIsSenderBanked(), Matchers.equalTo(isSenderBanked)))
                        .and(having(on(MPAY_TransactionConfig.class).getIsReceiverBanked(), Matchers.equalTo(isReceiverBanked)))
                        .and(having(on(MPAY_TransactionConfig.class).getRefType().getCode(), Matchers.equalTo(transactionTypeCode)))
                        .and(having(on(MPAY_TransactionConfig.class).getRefOperation().getId(), Matchers.equalTo(operationId))));
    }

    public MPAY_TransactionConfig getTransactionConfig(long senderType, boolean isSenderBanked, long receiverType, boolean isReceiverBanked, long messageType, String transactionTypeCode) {
        logger.debug("Inside getTransactionConfig....");
        logger.debug("senderType: " + senderType);
        logger.debug("isSenderBanked: " + isSenderBanked);
        logger.debug("isReceiverBanked: " + isReceiverBanked);
        logger.debug("transactionTypeCode: " + transactionTypeCode);
        List<MPAY_TransactionConfig> configs = listTransactionsConfigs();
        return selectFirst(configs,
                having(on(MPAY_TransactionConfig.class).getSenderType().getId(), Matchers.equalTo(senderType))
                        .and(having(on(MPAY_TransactionConfig.class).getReceiverType().getId(), Matchers.equalTo(receiverType)))
                        .and(having(on(MPAY_TransactionConfig.class).getMessageType().getId(), Matchers.equalTo(messageType)))
                        .and(having(on(MPAY_TransactionConfig.class).getIsSenderBanked(), Matchers.equalTo(isSenderBanked)))
                        .and(having(on(MPAY_TransactionConfig.class).getIsReceiverBanked(), Matchers.equalTo(isReceiverBanked)))
                        .and(having(on(MPAY_TransactionConfig.class).getRefType().getCode(), Matchers.equalTo(transactionTypeCode))));
    }

    public MPAY_ClientType getCustomerClientType() {
        logger.debug("Inside getCustomerClientType....");
        List<MPAY_ClientType> types = listClientTypes();
        return selectFirst(types, having(on(MPAY_ClientType.class).getIsCustomer(), Matchers.equalTo(true)));
    }

    public List<MPAY_ClientType> getCorporateClientType() {
        logger.debug("Inside getCustomerClientType....");
        List<MPAY_ClientType> types = listClientTypes();
        return types.stream().filter(t -> t.getIsCustomer().equals(false)).collect(Collectors.toList());
    }

    public MPAY_ProcessingStatus getProcessingStatus(String code) {
        logger.debug("Inside getProcessingStatus....");
        logger.debug(CODE + code);
        if (code == null)
            return null;

        List<MPAY_ProcessingStatus> statuses = listProcessingStatuses();
        return selectFirst(statuses, having(on(MPAY_ProcessingStatus.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_EndPointOperation getEndPointOperation(String operation) {
        logger.debug("Inside getEndPointOperation....");
        logger.debug("operation: " + operation);
        if (operation == null)
            return null;
        List<MPAY_EndPointOperation> operations = listEndPointOperations();
        return selectFirst(operations, having(on(MPAY_EndPointOperation.class).getOperation(), Matchers.equalToIgnoringCase(operation)));
    }

    public MPAY_City getCity(String code) {
        logger.debug("Inside getCity....");
        logger.debug(CODE + code);
        if (code == null)
            return null;

        List<MPAY_City> cities = listCities();
        return selectFirst(cities, having(on(MPAY_City.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_City getCityById(long id) {
        logger.debug("Inside getCityById....");
        logger.debug(CITY_CODE + id);
        List<MPAY_City> cities = listCities();
        return selectFirst(cities, having(on(MPAY_City.class).getId(), Matchers.equalTo(id)));
    }

    public JfwCountry getCountry(String code) {
        logger.debug("Inside getCountry....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<JfwCountry> countries = listCountries();

        return selectFirst(countries, having(on(JfwCountry.class).getCode(), Matchers.equalTo(code)));

    }

    public JFWCurrency getCurrency(String code) {
        logger.debug("Inside getCurrency....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<JFWCurrency> currencies = listCurrencies();

        return selectFirst(currencies, having(on(JFWCurrency.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_ClientType getClientType(String code) {
        logger.debug("Inside getClientType....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_ClientType> types = listClientTypes();
        return selectFirst(types, having(on(MPAY_ClientType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_IDType getIDType(String code) {
        logger.debug("Inside getIDType....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_IDType> types = listIDTypes();
        return selectFirst(types, having(on(MPAY_IDType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_Language getLanguage(long id) {
        logger.debug("Inside getLanguage....");
        logger.debug("id: " + id);
        List<MPAY_Language> languages = listLanguages();
        return selectFirst(languages, having(on(MPAY_Language.class).getId(), Matchers.equalTo(id)));
    }

    public MPAY_Language getLanguageByCode(String code) {
        logger.debug("Inside getLanguageByCode....");
        logger.debug(CODE + code);
        List<MPAY_Language> languages = listLanguages();
        return selectFirst(languages, having(on(MPAY_Language.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_ChannelType getChannelType(String code) {
        logger.debug("Inside getChannelType....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_ChannelType> types = listChannelTypes();
        return selectFirst(types, having(on(MPAY_ChannelType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_MpClearIntegMsgType getMpClearIntegrationMessageTypes(String code) {
        logger.debug("Inside getMpClearIntegrationMessageTypes....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_MpClearIntegMsgType> types = listMPClearIntegMessageTypes();
        return selectFirst(types, having(on(MPAY_MpClearIntegMsgType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_CustIntegActionType getCustomerIntegrationActionTypes(String code) {
        logger.debug("Inside getCustomerIntegrationActionTypes....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_CustIntegActionType> types = listCustomerIntegActionTypes();
        return selectFirst(types, having(on(MPAY_CustIntegActionType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_MpClearIntegRjctReason getMpClearIntegrationReasons(String code) {
        logger.debug("Inside getMpClearIntegrationReasons....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_MpClearIntegRjctReason> reasons = listMPClearRejectionReasons();
        return selectFirst(reasons, having(on(MPAY_MpClearIntegRjctReason.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_MpClearResponseCode getMpClearIntegrationResponseCodes(String code) {
        logger.debug("Inside getMpClearIntegrationResponseCodes....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_MpClearResponseCode> codes = listMPClearIntegResponseCodes();
        return selectFirst(codes, having(on(MPAY_MpClearResponseCode.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_NetworkManActionType getNetworkManagementActionTypes(String code) {
        logger.debug("Inside getNetworkManagementActionTypes....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_NetworkManActionType> types = listNetworkManagementActionTypes();
        return selectFirst(types, having(on(MPAY_NetworkManActionType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_AccountType getAccountTypes(String code) {
        logger.debug("Inside getAccountTypes....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_AccountType> types = listAccountTypes();
        return selectFirst(types, having(on(MPAY_AccountType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_JvType getJvTypes(String code) {
        logger.debug("Inside getJvTypes....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_JvType> types = listJVTypes();
        return selectFirst(types, having(on(MPAY_JvType.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_MessageType getMessageTypes(String code) {
        logger.debug("Inside getMessageTypes....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_MessageType> types = listMessageTypes();
        return selectFirst(types, having(on(MPAY_MessageType.class).getCode(), Matchers.equalToIgnoringCase(code)));
    }

    public MPAY_MessageType getFirstFinancialMessageType() {
        logger.debug("Inside getFirstFinancialMessageType....");
        List<MPAY_MessageType> types = listMessageTypes();
        return selectFirst(types, having(on(MPAY_MessageType.class).getIsFinancial(), Matchers.equalTo(true)));
    }

    public MPAY_MessageType getMessageTypes(long id) {
        logger.debug("Inside getMessageTypes....");
        logger.debug("id: " + id);
        List<MPAY_MessageType> types = listMessageTypes();
        return selectFirst(types, having(on(MPAY_MessageType.class).getId(), Matchers.equalTo(id)));
    }

    public MPAY_Reason getReason(String code) {
        logger.debug("Inside getReason....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_Reason> reasons = listReasons();
        return selectFirst(reasons, having(on(MPAY_Reason.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_Reason getReasonByMPClearCode(String code) {
        logger.debug("Inside getReasonByMPClearCode....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_Reason> reasons = listReasons();
        return selectFirst(reasons, having(on(MPAY_Reason.class).getMpClearCode(), Matchers.equalTo(code)));
    }

    public MPAY_SysConfig getSystemConfigurations(String key) {
        logger.debug("Inside getSystemConfigurations....");
        logger.debug("key: " + key);
        if (key == null)
            return null;
        List<MPAY_SysConfig> configs = listSystemConfigurations();
        return selectFirst(configs, having(on(MPAY_SysConfig.class).getConfigKey(), Matchers.equalTo(key)));
    }

    public MPAY_TransactionsConfig getTransactionsConfig(long clientType, long messageType, String bankedUnbanked, boolean isSender) {
        logger.debug("Inside getTransactionsConfig....");
        logger.debug("clientType: " + clientType);
        logger.debug("messageType: " + messageType);
        logger.debug("bankedUnbanked: " + bankedUnbanked);
        logger.debug("isSender: " + isSender);

        return selectFirst(listTransactionConfigs(),
                having(on(MPAY_TransactionsConfig.class).getClientType().getId(), Matchers.equalTo(clientType))
                        .and(having(on(MPAY_TransactionsConfig.class).getMessageType().getId(), Matchers.equalTo(messageType)))
                        .and(having(on(MPAY_TransactionsConfig.class).getBankedUnbanked(), Matchers.equalTo(bankedUnbanked)))
                        .and(having(on(MPAY_TransactionsConfig.class).getIsSender(), Matchers.equalTo(isSender))));
    }

    public MPAY_MessageType getMessageTypeByMPClearCode(String code) {
        logger.debug("Inside getMessageTypeByMPClearCode....");
        logger.debug(CODE + code);
        List<MPAY_MessageType> types = listMessageTypes();
        return selectFirst(types, having(on(MPAY_MessageType.class).getMpClearCode(), Matchers.equalTo(code)));
    }

    public MPAY_Reg_File_Stt getRegFileStatus(String code) {
        logger.debug("Inside getRegFileStatus....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_Reg_File_Stt> statuses = listRegFileStatuses();
        return selectFirst(statuses, having(on(MPAY_Reg_File_Stt.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_Reg_Instrs_Stt getRegInstrStatus(String code) {
        logger.debug("Inside getRegInstrStatus....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_Reg_Instrs_Stt> statuses = listRegInstructionStatuses();
        return selectFirst(statuses, having(on(MPAY_Reg_Instrs_Stt.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_Bank getBank(String name) {
        logger.debug("Inside getBank....");
        logger.debug("name: " + name);
        if (name == null)
            return null;
        List<MPAY_Bank> banks = listBanks();
        return selectFirst(banks, having(on(MPAY_Bank.class).getName(), Matchers.equalTo(name)));
    }

    public MPAY_Bank getBankByCode(String code) {
        logger.debug("Inside getBankByCode....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_Bank> banks = listBanks();
        return selectFirst(banks, having(on(MPAY_Bank.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_Bank getBank(long id) {
        logger.debug("Inside getBank....");
        logger.debug("id: " + id);
        List<MPAY_Bank> banks = listBanks();
        return selectFirst(banks, having(on(MPAY_Bank.class).getId(), Matchers.equalTo(id)));
    }

    public MPAY_ePayStatusCode getePayStatusCode(String code) {
        logger.debug("Inside getePayStatusCode....");
        logger.debug(CODE + code);
        if (code == null)
            return null;

        List<MPAY_ePayStatusCode> codes = listEPayStatusCodes();
        return selectFirst(codes, having(on(MPAY_ePayStatusCode.class).getCode(), Matchers.equalTo(code)));
    }

    public JFWCurrency getDefaultCurrency() {
        logger.debug("Inside getDefaultCurrency....");
        String code = getSystemConfigurations(SysConfigKeys.DEFAULT_CURRENCY).getConfigValue();
        return itemDao.getItem(JFWCurrency.class, "stringISOCode", code);
    }

    public PSPCache getPSP() {
        logger.debug("Inside getPSP....");
        return getPSPFromDB();
    }

    private PSPCache getPSPFromDB() {
        logger.debug("Inside getPSPFromDB....");
        PSPCache oPSPCache = new PSPCache();
        MPAY_Corporate psp = itemDao.getItem(MPAY_Corporate.class, CLIENT_TYPE_CODE, ClientTypes.SERVICE_PROVIDER_CODE);
        if (psp == null)
            return null;
        oPSPCache.setNationalID(psp.getPspNationalID());//psp.getRefPSPPspDetails().get(0).getPspNationalID());
        oPSPCache.setName(psp.getName());
        oPSPCache.setId(psp.getId());

        oPSPCache.setPspChargeService(getCorporateService(psp, psp.getName() + MessageCodes.FEES.toString()));
        oPSPCache.setPspMPClearService(getCorporateService(psp, psp.getName() + MessageCodes.CLEARING.toString()));
        oPSPCache.setPspCashInService(getCorporateService(psp, psp.getName() + MessageCodes.CI.toString()));
        oPSPCache.setPspCashOutService(getCorporateService(psp, psp.getName() + MessageCodes.CO.toString()));
        oPSPCache.setPspTaxesService(getCorporateService(psp, psp.getName() + MessageCodes.INCOME_VAT.toString()));
        oPSPCache.setPspTaxesService(getCorporateService(psp, psp.getName() + MessageCodes.EXPENSES_VAT.toString()));
        oPSPCache.setPspCommissionsService(getCorporateService(psp, psp.getName() + MessageCodes.COMMISSIONS.toString()));

        oPSPCache.setPspChargeAccount(getCorporateServiceAccount(oPSPCache.getPspChargeService()));
        oPSPCache.setPspMPClearAccount(getCorporateServiceAccount(oPSPCache.getPspMPClearService()));
        oPSPCache.setPspCashInAccount(getCorporateServiceAccount(oPSPCache.getPspCashInService()));
        oPSPCache.setPspCashOutAccount(getCorporateServiceAccount(oPSPCache.getPspCashOutService()));
        oPSPCache.setClientTypeId(psp.getClientType().getId());
        oPSPCache.setPspTaxesAccount(getCorporateServiceAccount(oPSPCache.getPspTaxesService()));
        oPSPCache.setPspCommissionsAccount(getCorporateServiceAccount(oPSPCache.getPspCommissionsService()));
        return oPSPCache;
    }

    private MPAY_Account getCorporateServiceAccount(MPAY_CorpoarteService service) {
        if (service == null || service.getServiceServiceAccounts().isEmpty())
            return null;
        return service.getServiceServiceAccounts().get(0).getRefAccount();
    }

    private MPAY_CorpoarteService getCorporateService(MPAY_Corporate corporate, String serviceName) {
        return selectFirst(corporate.getRefCorporateCorpoarteServices(), having(on(MPAY_CorpoarteService.class).getName(), Matchers.equalTo(serviceName)));
    }

    public MPClearCache getMPClear() {
        logger.debug("Inside getMPClear....");
        return getMPClearFromDB();
    }

    private MPClearCache getMPClearFromDB() {
        logger.debug("Inside getMPClearFromDB....");
        MPAY_Corporate mpClear = itemDao.getItem(MPAY_Corporate.class, CLIENT_TYPE_CODE, ClientTypes.PS_MPCLEAR);
        if (mpClear == null)
            return null;
        MPClearCache oMPClearCache = new MPClearCache();
        oMPClearCache.setRegistrationId(mpClear.getRegistrationId());
        oMPClearCache.setId(mpClear.getId());
        oMPClearCache.setClientType(mpClear.getClientType().getId());
        return oMPClearCache;
    }

    public MPAY_NotificationTemplate getNotificationTemplate(long operationId, long typeId, long languageId) {
        return loadNotificationTemplateByIdOrName(String.valueOf(operationId), false, typeId, languageId);
    }

    public MPAY_NotificationTemplate getNotificationTemplateByOperationName(String operationName, long typeId, long languageId) {
        return loadNotificationTemplateByIdOrName(operationName, true, typeId, languageId);
    }

    private MPAY_NotificationTemplate loadNotificationTemplateByIdOrName(String operationIdOrName, boolean isName, long typeId, long languageId) {
        logger.debug(isName ? "Inside getNotificationTemplateByOperationName...." : "Inside getNotificationTemplate....");
        logger.debug((isName ? "operationName: " : "operationId") + operationIdOrName);
        logger.debug("typeId: " + typeId);
        logger.debug(LANGUAGE_ID + languageId);
        operationIdOrName = isName ? "'" + operationIdOrName + "'" : operationIdOrName;
        String whereClause = (isName ? "refOperation.name = " : "refOperation.id = ") + operationIdOrName + " AND refType.code = '" + typeId + "' AND refLanguage.id = " + languageId;
        List<MPAY_NotificationTemplate> templates = itemDao.getItems(MPAY_NotificationTemplate.class, null, null, whereClause, null);
        return templates.isEmpty() ? null : templates.get(0);
    }


    public MPAY_BankIntegStatus getBankIntegStatus(String code) {
        logger.debug("Inside getBankIntegStatus....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<MPAY_BankIntegStatus> statuses = listBankIntegStatuses();
        return selectFirst(statuses, having(on(MPAY_BankIntegStatus.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_TransactionSize getTransactionSize(String code) {
        logger.debug("Inside getTransactionSize....");
        List<MPAY_TransactionSize> transactionsSize = listTransactionSize();
        return selectFirst(transactionsSize, having(on(MPAY_TransactionSize.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_CorpLegalEntity getCorpLegalEntity(String code) {
        logger.debug("Inside getCorpLegalEntity....");
        List<MPAY_CorpLegalEntity> legalEntities = listCorpLegalEntities();
        return selectFirst(legalEntities, having(on(MPAY_CorpLegalEntity.class).getCode(), Matchers.equalTo(code)));
    }

    public MPAY_NotificationService getNotificationService(String code) {
        logger.debug("Inside getTransactionSize....");
        List<MPAY_NotificationService> services = listNotificationServices();
        return selectFirst(services, having(on(MPAY_NotificationService.class).getCode(), Matchers.equalTo(code)));
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_CorpLegalEntity> listCorpLegalEntities() {
        logger.debug("Inside listCorpLegalEntities....");
        return (List<MPAY_CorpLegalEntity>) getListFromCache("mpay.CorpLegalEntity", MPAY_CorpLegalEntity.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_NotificationService> listNotificationServices() {
        logger.debug("Inside ListNotificationServices....");
        return (List<MPAY_NotificationService>) getListFromCache("mpay.NotificationServices", MPAY_NotificationService.class);
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_TransactionSize> listTransactionSize() {
        logger.debug("Inside listTransactionSize....");
        return (List<MPAY_TransactionSize>) getListFromCache("mpay.transactionSize", MPAY_TransactionSize.class);
    }

    private Object getListFromCache(String key, Class<? extends JFWEntity> type) {
        logger.debug("Inside getListFromCache....");
        logger.debug("key: " + key);
        return itemDao.getItems(type);
    }

    public JfwCountry getCountryByIsoCode(String code) {
        logger.debug("Inside getCountry....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<JfwCountry> countries = listCountries();

        return selectFirst(countries, having(on(JfwCountry.class).getCountryStringIsoCode(), Matchers.equalTo(code)));

    }

    public JfwCountry getCountryByDescription(String code) {
        logger.debug("Inside getCountry....");
        logger.debug(CODE + code);
        if (code == null)
            return null;
        List<JfwCountry> countries = listCountries();

        return selectFirst(countries, having(on(JfwCountry.class).getDescription(), Matchers.equalTo(code)));

    }

    public List<MPAY_Language> getLanguges() {
        return ((List<MPAY_Language>) getListFromCache("MPAY_Language", MPAY_Language.class)).stream().filter(l -> !l.getCode().equals("2")).collect(Collectors.toList());
    }

    public MPAY_CityArea getAreaByCode(String code) {
        logger.debug("Inside getAreaByCode....");
        List<MPAY_CityArea> areas = (List<MPAY_CityArea>) getListFromCache("mpay.CityArea", MPAY_CityArea.class);
        List<MPAY_CityArea> filterdAreas = areas.stream().filter(area -> area.getCode().equalsIgnoreCase(code)).collect(Collectors.toList());
        return filterdAreas.size() == 0 ? null : filterdAreas.get(0);
    }

    public List<MPAY_City> getCityByName(String name) {
        logger.debug("Inside getCityByName....");
        logger.debug(CITY_NAME + name);
        List<MPAY_City> cities = listCities();
        return cities.stream().filter(c -> c.getName().equals(name)).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_AccountCategory> listAccountCategories() {
        logger.debug("Inside listServiceCategories....");
        return (List<MPAY_AccountCategory>) getListFromCache("mpay.accountCategories", MPAY_AccountCategory.class);
    }

    public MPAY_AccountCategory getAccountCategory(long id) {
        logger.debug("Inside getAccountCategory....");
        logger.debug(String.format("id: %d", id));
        List<MPAY_AccountCategory> categories = listAccountCategories();
        return selectFirst(categories, having(on(MPAY_AccountCategory.class).getId(), Matchers.equalTo(id)));
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_CustomerKyc> listCustomerKyc() {
        logger.debug("Inside listCustomerKyc....");
        return (List<MPAY_CustomerKyc>) getListFromCache("mpay.CustomerKyc", MPAY_CustomerKyc.class);
    }

    public MPAY_CustomerKyc getCustomerKyc(long id) {
        logger.debug("Inside getCustomerKyc....");
        logger.debug("id: " + id);
        List<MPAY_CustomerKyc> customerKycs = listCustomerKyc();
        return selectFirst(customerKycs, having(on(MPAY_CustomerKyc.class).getId(), Matchers.equalTo(id)));
    }

    @SuppressWarnings("unchecked")
    public List<MPAY_CorporateKyc> listCorporateKyc() {
        logger.debug("Inside listCorporateKyc....");
        return (List<MPAY_CorporateKyc>) getListFromCache("mpay.CorporateKyc", MPAY_CorporateKyc.class);
    }

    public MPAY_CorporateKyc getCorporateKyc(long id) {
        logger.debug("Inside getCorporateKyc....");
        logger.debug("id: " + id);
        List<MPAY_CorporateKyc> mpay_corporateKycs = listCorporateKyc();
        return selectFirst(mpay_corporateKycs, having(on(MPAY_CorporateKyc.class).getId(), Matchers.equalTo(id)));
    }

    public MPAY_CorporateKyc getCorporateKyc(String code) {
        logger.debug("Inside getCorporateKyc....");
        logger.debug("code: " + code);
        List<MPAY_CorporateKyc> mpay_corporateKycs = listCorporateKyc();
        return selectFirst(mpay_corporateKycs, having(on(MPAY_CorporateKyc.class).getCode(), Matchers.equalTo(code)));

    }
}
