package com.progressoft.mpay.plugins;

public class ValidationResult {
	private String reasonCode;
	private String reasonDescription;
	private boolean isValid;

	public ValidationResult() {
		//Default
	}

	public ValidationResult(String reasonCode, String reasonDescription, boolean isValid) {
		this.reasonCode = reasonCode;
		this.reasonDescription = reasonDescription;
		this.isValid = isValid;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReasonDescription() {
		return reasonDescription;
	}

	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
}
