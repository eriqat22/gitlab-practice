package com.progressoft.mpay.migs.mvc.controller;

public class MigsErrorHandler implements MigsHandler {

	@Override
	public String handle(MigsContext migsContext) {
		return MIGSParameters.ERROR_PAGE_TAGS_OPEN + MIGSParameters.SERVICE_UNAVAILABLE_ERROR + MIGSParameters.ERROR_PAGE_TAGS_CLOSE;
	}
}