package com.progressoft.mpay.tax;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_TaxScheme;
import com.progressoft.mpay.entities.MPAY_TaxSchemeDetail;
import com.progressoft.mpay.entity.MPAYView;

public class PreDeleteTaxScheme implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PreDeleteTaxScheme.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside PreDeleteTaxScheme-------");
		MPAY_TaxScheme scheme = (MPAY_TaxScheme) arg0.get(WorkflowService.WF_ARG_BEAN);

		for (MPAY_TaxSchemeDetail taxDetail : scheme.getRefSchemeTaxSchemeDetails()) {
			Map<Option, Object> options = new EnumMap<>(Option.class);
			options.put(Option.ACTION_NAME, "SVC_Delete");
			JfwHelper.executeAction(MPAYView.TAX_SCHEME_DETAILS, taxDetail, options);
		}
	}
}
