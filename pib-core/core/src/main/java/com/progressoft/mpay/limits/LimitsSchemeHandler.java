package com.progressoft.mpay.limits;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;

public class LimitsSchemeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(LimitsSchemeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {

		MPAY_LimitsScheme scheme = (MPAY_LimitsScheme) changeHandlerContext.getEntity();
		logger.debug("LimitsSchemeHandler  ----------------------------");

		scheme.setCurrency(SystemParameters.getInstance().getDefaultCurrency());

	}
}
