package com.progressoft.mpay.migs.mvc.controller;

import java.math.BigDecimal;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_BankIntegMessage;

public class MigsBankIntegrationMessage {

	public void insert(MigsContext context) {
		try {
			MPAY_BankIntegMessage bankIntegMessage = prepareMpayIntegrationMessage(context);
			MPAY_BankIntegMessage msgByRequestId = getIntegrationMessageByRequestId(context);
			if (msgByRequestId != null)
				bankIntegMessage.setId(msgByRequestId.getId());
			context.getItemDao().merge(bankIntegMessage);
		} catch (Exception exception) {
			throw new MigsException(exception);
		}
	}

	private MPAY_BankIntegMessage prepareMpayIntegrationMessage(MigsContext context) {
		try {
			MPAY_BankIntegMessage integMessage = new MPAY_BankIntegMessage();
			integMessage.setRequestID(context.getMigsRequest().getVpcMerchTxnRef());
			integMessage.setRequestContent(new MigsMarshaller().marshallMigsObject(context.getMigsRequest()));
			integMessage.setRequestDate(SystemHelper.getSystemTimestamp());
			integMessage.setIsReversal(false);
			integMessage.setIsReversed(false);
			integMessage.setRefBank(getDefaultMigsBank(context));
			integMessage.setRequestToken(context.getMigsRequest().getVpcSecureHash());
			integMessage.setRefStatus(context.getLookupLoader().getBankIntegStatus(BankIntegrationStatus.PENDING));
			integMessage.setAmount(new BigDecimal(context.getMigsRequest().getVpcAmount()));
			integMessage.setCurrency(context.getSystemParameters().getDefaultCurrency());
			return integMessage;
		} catch (Exception e) {
			throw new MigsException(e);
		}
	}

	private MPAY_BankIntegMessage getIntegrationMessageByRequestId(MigsContext context) {
		return context.getItemDao().getItem(MPAY_BankIntegMessage.class, "requestID", context.getMigsRequest().getVpcMerchTxnRef());
	}

	private MPAY_Bank getDefaultMigsBank(MigsContext context) {
		return context.getItemDao().getItem(MPAY_Bank.class, "code", context.getSystemParameters().getRegistrationBankCode());
	}
}