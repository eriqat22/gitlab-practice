package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class ValidateServiceAccount implements Validator {
	private static final Logger logger = LoggerFactory.getLogger(ValidateServiceAccount.class);

	@Autowired
	ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("validate =======");
		MPAY_ServiceAccount serviceAccount = (MPAY_ServiceAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
		//Handle JFW Bug calling invalid validator in invalid step
		if(serviceAccount.getStatusId() != null)
			return;
		validateMaxAccounts(serviceAccount);
		validateMasLength(serviceAccount);
		MPAY_CorpoarteService service = serviceAccount.getService();
		if (service.getServiceServiceAccounts() == null || service.getServiceServiceAccounts().isEmpty())
			return;
		for (MPAY_ServiceAccount account : service.getServiceServiceAccounts()) {
			if (account.getId() == serviceAccount.getId())
				continue;
			validateAccounts(account, serviceAccount);
		}
	}

	private void validateAccounts(MPAY_ServiceAccount account, MPAY_ServiceAccount currentAccount) throws InvalidInputException {
		InvalidInputException iie = new InvalidInputException();
		String storedRegistrationId = account.getBank().getCode() + String.valueOf(account.getMas());
		if (storedRegistrationId.equals(currentAccount.getBank().getCode() + String.valueOf(currentAccount.getMas()))) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_MAS));
			throw iie;
		}
		if (currentAccount.getExternalAcc() != null && currentAccount.getExternalAcc().equals(account.getExternalAcc())) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_EXTERNAL_ACCOUNT));
			throw iie;
		}
		if (currentAccount.getIsDefault() && account.getIsDefault()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_ISDEFUALT));
			throw iie;
		}
	}

	private void validateMaxAccounts(MPAY_ServiceAccount serviceAccount) throws InvalidInputException {
		if (!serviceAccount.getService().getIsRegistered())
			return;
		List<MPAY_ServiceAccount> undeletedServiceAccounts = select(serviceAccount.getService().getServiceServiceAccounts(),
				having(on(MPAY_ServiceAccount.class).getDeletedFlag(), Matchers.nullValue()).or(having(on(MPAY_ServiceAccount.class).getDeletedFlag(), Matchers.equalTo(false))));
		MPAY_ClientType type = serviceAccount.getService().getRefCorporate().getClientType();
		if(type.getMaxNumberOfAccount() == 0)
			return;
		if (undeletedServiceAccounts.size() >= type.getMaxNumberOfAccount()) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.ACCOUNT_MAX_REACHED));
			throw iie;
		}
	}

	private void validateMasLength(MPAY_ServiceAccount serviceAccount) throws InvalidInputException {
		if (String.valueOf(serviceAccount.getMas()).length() > SystemParameters.getInstance().getMobileAccountSelectorLength()) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg(MPayErrorMessages.INVALID_MAS_LENGTH), SystemParameters.getInstance().getMobileAccountSelectorLength()));
			throw iie;
		}
	}
}