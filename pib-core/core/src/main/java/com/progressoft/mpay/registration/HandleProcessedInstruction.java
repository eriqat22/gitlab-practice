package com.progressoft.mpay.registration;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Intg_Cust_Reg_Instr;
import com.progressoft.mpay.entities.MPAY_Intg_Reg_File;

public class HandleProcessedInstruction implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(HandleProcessedInstruction.class);

	@Autowired
	private ItemDao itemDao;

	protected EntityManager em;

	@PersistenceContext(unitName = "JFWUnit")
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}


	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside HandleProcessedInstruction...");
		MPAY_Intg_Cust_Reg_Instr inst = (MPAY_Intg_Cust_Reg_Instr) transientVars.get(WorkflowService.WF_ARG_BEAN);

		MPAY_Intg_Reg_File file = inst.getRefRegFile();
		long processedCount = 0;
		try {
			processedCount = FileRegistrationHelper.callUpdateProcessedCountSP(file, em);
		} catch (SQLException e) {
			logger.error("error...change me later", e);
			throw new WorkflowException(e);
		}
		logger.debug("stored procedure-processed count=" + processedCount);

		file.setRegFileProcessedRecords(processedCount);

		itemDao.merge(file);

		if (file.getRegFileProcessedRecords() == file.getRegFileRecords()) {
			logger.debug("Note: Number of file's records equal processed records.");
			modifyFileStts(file);
			JfwHelper.sendToActiveMQ(file, Boolean.FALSE);
		}
	}

	/**
	 *
	 * @param file
	 */
	private void modifyFileStts(MPAY_Intg_Reg_File file) {
		List<MPAY_Intg_Cust_Reg_Instr> insts = file.getRefRegFileIntgCustRegInstrs();
		long size = insts.size();
		long succsSize = select(
				insts,
				having(on(MPAY_Intg_Cust_Reg_Instr.class).getRegInstrProcessingStts().getCode(),
						Matchers.equalTo("SUCS"))).size();
		long rjctSize = select(
				insts,
				having(on(MPAY_Intg_Cust_Reg_Instr.class).getRegInstrProcessingStts().getCode(),
						Matchers.equalTo("RJCT"))).size();

		if (size == succsSize) {
			logger.debug("Going to change file status into 'ACPT'");
			file.setRegFileProcessingStts(LookupsLoader.getInstance().getRegFileStatus("ACPT"));
		} else if (size == rjctSize) {
			logger.debug("Going to change file status into 'RJCT'");
			file.setRegFileProcessingStts(LookupsLoader.getInstance().getRegFileStatus("RJCT"));
		} else {
			logger.debug("Going to change file status into 'PART'");
			file.setRegFileProcessingStts(LookupsLoader.getInstance().getRegFileStatus("PART"));
		}

		itemDao.merge(file);

	}

}
