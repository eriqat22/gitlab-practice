package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Pre1710EvaluationMpClearInegMsgLog implements FunctionProvider {

	@Autowired
	private ItemDao itemDao;
	private static final Logger logger = LoggerFactory.getLogger(Pre1710EvaluationMpClearInegMsgLog.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {

		logger.debug("inside Pre1710EvaluationMpClearInegMsgLog---------------------------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());
		String originalMessageId = isomsg.getField(116).toString();

		MPAY_MpClearIntegMsgLog originalMessage = (MPAY_MpClearIntegMsgLog) itemDao.getItem(
				MPAY_MpClearIntegMsgLog.class, "messageId", originalMessageId);
		if (null == originalMessage)
			throw new WorkflowException("Original message not found");
		msg.setHint(originalMessage.getHint());
		transientVars.put("clientType", originalMessage.getHint().substring(originalMessage.getHint().length() - 1));

	}
}
