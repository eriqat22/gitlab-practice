package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.CorporateServiceMetaData;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.registration.customers.MobileAccountWorkflowStatuses;

public class PostCancelCorporateServiceApproved implements FunctionProvider {
	private static final Logger logger = LoggerFactory.getLogger(PostCancelCorporateServiceApproved.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =============");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
		handleAccounts(service);
		revertService(service);
	}

	private void revertService(MPAY_CorpoarteService service) {
		if (service.getApprovedData() == null)
			return;

		service.setNewAlias(null);
		service.setNewName(null);
		service.setNewDescription(null);

		CorporateServiceMetaData approvedService = CorporateServiceMetaData.fromJson(service.getApprovedData());
		approvedService.fillCorporateServiceFromMetaData(service);
//		service.setNotificationChannel(LookupsLoader.getInstance().getNotificationChannel(approvedService.getNotificationChannelCode()));
		service.setServiceCategory(LookupsLoader.getInstance().getServiceCategory(approvedService.getServiceCategoryId()));
		service.setServiceType(LookupsLoader.getInstance().getServiceType(approvedService.getServiceTypeCode()));
		service.setApprovedData(null);

		DataProvider.instance().mergeCorporateService(service);
	}

	private void handleAccounts(MPAY_CorpoarteService service) throws WorkflowException {
		logger.debug("Inside HandleAccounts =============");
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId(), MobileAccountWorkflowStatuses.CANCELING.toString());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_ServiceAccount account = accountsIterator.next();
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.APPROVE, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
}