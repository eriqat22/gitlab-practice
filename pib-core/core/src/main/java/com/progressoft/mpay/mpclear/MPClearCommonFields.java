package com.progressoft.mpay.mpclear;

public class MPClearCommonFields {
    public static final int PROCESSING_CODE = 3;
    public static final int AMOUNT = 4;
    public static final int TRANSMISSION_DATE = 7;
    public static final int REASON_CODE = 44;
    public static final int REASON_DESCRIPTION = 47;
    public static final int PAYMENT_TYPE = 104;
    public static final int IS_ENCODED = 113;
    public static final int MESSAGE_ID = 115;
    public static final int ORIGINAL_MESSAGE_ID = 116;
    private MPClearCommonFields() {
    }
}
