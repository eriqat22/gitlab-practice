package com.progressoft.mpay.migs.mvc.controller;

import com.progressoft.jfw.integration.IntegrationUtils;

public class MigsServiceUserManager {

	private MigsServiceUserManager() {

	}

	public static boolean enableServiceUser(String tenantName) {
		return IntegrationUtils.enableServiceUser(tenantName);
	}

	public static void disableServiceUser(boolean enabledServiceUser) {
		if (enabledServiceUser)
			IntegrationUtils.disableServiceUser();
	}

}
