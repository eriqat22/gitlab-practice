package com.progressoft.mpay.registration.bulk;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class CorporateBulkRegistrationAggregator implements AggregationStrategy {

	@SuppressWarnings("unchecked")
	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		CorporateRegistrationLine registrationLine = newExchange.getIn().getBody(CorporateRegistrationLine.class);
		if(oldExchange == null) {
			List<CorporateRegistrationLine> registrationLines = new ArrayList<>();
			registrationLines.add(registrationLine);
			newExchange.getIn().setBody(registrationLines);
			return newExchange;
		} else {
			List<CorporateRegistrationLine> registrationLines = (List<CorporateRegistrationLine>) oldExchange.getIn().getBody();
			registrationLines.add(registrationLine);
			oldExchange.getIn().setBody(registrationLines);
			return oldExchange;
		}
	}

}