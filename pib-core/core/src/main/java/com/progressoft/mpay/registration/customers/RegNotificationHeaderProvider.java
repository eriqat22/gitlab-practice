package com.progressoft.mpay.registration.customers;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.jfw.integration.NotificationHeadersProvider;
import com.progressoft.jfw.model.bussinessobject.core.IJFWEntity;
import com.progressoft.jfw.shared.workflow.WorkflowAction;
import com.progressoft.jfw.util.simpleview.SimpleView;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entity.LanguageCode;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;

public class RegNotificationHeaderProvider implements NotificationHeadersProvider {

	private static final Logger logger = LoggerFactory.getLogger(RegNotificationHeaderProvider.class);

	@Override
	public Map<String, String> getHeaders(IJFWEntity entity, SimpleView simpleView, WorkflowAction action) {
		Map<String, String> headers = new HashMap<>();

		try {
			logger.debug("inside RegNotificationHeaderProvider");

			MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) entity;
			generateAndPersistNotification(mobile);
		} catch (Exception ex) {
			logger.error("An exception occured while generating new PIN code", ex);
		}

		return headers;
	}

	public void generateAndPersistNotification(MPAY_CustomerMobile mobile) {
		MPAY_EndPointOperation operation = LookupsLoader.getInstance().getEndPointOperation("cust");
		CustomerRegistrationNotificationContext context = new CustomerRegistrationNotificationContext();
		MPAY_Language language = mobile.getRefCustomer().getPrefLang();
		if (LanguageCode.ENGLISH.equals(language.getCode()))
			context.setAppName(SystemParameters.getInstance().getApplicationNameEn());
		else
			context.setAppName(SystemParameters.getInstance().getApplicationNameAr());
		MPAY_Notification notification = NotificationHelper.getInstance().createNotification(null, context, NotificationType.ACCEPTED_SENDER, operation.getId(), language.getId(),
				mobile.getMobileNumber(), NotificationChannelsCode.SMS);
		DataProvider.instance().persistNotification(notification);
	}
}
