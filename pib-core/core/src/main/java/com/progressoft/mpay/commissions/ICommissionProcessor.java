package com.progressoft.mpay.commissions;

import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;

@FunctionalInterface
public interface ICommissionProcessor {
	void process(ProcessingContext context, ProcessingContextSide side, CommissionDirections direction);
}
