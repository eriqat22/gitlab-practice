package com.progressoft.mpay.payments.cashout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ServiceCashOut;

public class ServiceCashOutChangeHandler implements ChangeHandler {

	private static final String REF_SERVICE_ACCOUNT = "refServiceAccount";
	private static final Logger logger = LoggerFactory.getLogger(ServiceCashOutChangeHandler.class);
	private static final String CALCULATION_STEP = "100606";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("***inside CashOutChangeHandler : " + changeHandlerContext.getSource());

		MPAY_ServiceCashOut cashOut = (MPAY_ServiceCashOut) changeHandlerContext.getEntity();

		cashOut.setCurrency(SystemParameters.getInstance().getDefaultCurrency());

		if (cashOut.getStatusId() != null && CALCULATION_STEP.equals(cashOut.getStatusId().getCode())) {
			changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setVisible(false);
		} else {
			changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setVisible(true);
		}

	}

}