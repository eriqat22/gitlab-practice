package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;

public class HandleMobileAccountCancel implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(HandleMobileAccountCancel.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute======================");
		MPAY_MobileAccount account = (MPAY_MobileAccount) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (account.getIsRegistered()) {
			resetMobileAccount(account);
			JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.APPROVE, new WorkflowException());
		} else
			JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.REJECT, new WorkflowException());
	}

	private void resetMobileAccount(MPAY_MobileAccount account) {
		account.setNewProfile(null);
		account.setNewExternalAcc(null);
		if (account.getApprovedData() != null)
			MobileAccountMetaData.fromJson(account.getApprovedData()).fillMobileAccountFromMetaData(account);
		account.setApprovedData(null);
	}
}