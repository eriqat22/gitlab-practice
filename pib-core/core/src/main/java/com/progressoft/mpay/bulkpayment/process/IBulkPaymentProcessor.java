package com.progressoft.mpay.bulkpayment.process;

import com.progressoft.mpay.entities.MPAY_BulkPayment;

@FunctionalInterface
public interface IBulkPaymentProcessor {
	void process(MPAY_BulkPayment bulkPayment);
}
