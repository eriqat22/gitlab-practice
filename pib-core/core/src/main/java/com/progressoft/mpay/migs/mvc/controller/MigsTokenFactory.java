package com.progressoft.mpay.migs.mvc.controller;

import java.util.HashMap;
import java.util.Map;

public class MigsTokenFactory {

	static final String DEFAULT = "DEFAULT";
	static Map<String, TokenValidator> types = new HashMap<>();

	private MigsTokenFactory() {

	}

	public static TokenValidator getTokenValidator(MigsContext context) {
		initializeTypes(context);
		if (types.containsKey(DEFAULT))
			return types.get(DEFAULT);
		else
			throw new MigsException("Not Supported Message");
	}

	private static void initializeTypes(MigsContext context) {
		if(types.isEmpty())
			types.put(DEFAULT, new MigsTokenValidator(context));
	}
}
