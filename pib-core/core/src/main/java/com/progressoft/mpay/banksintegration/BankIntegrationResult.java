package com.progressoft.mpay.banksintegration;

import com.progressoft.mpay.entities.MPAY_BankIntegMessage;

public class BankIntegrationResult {
	private MPAY_BankIntegMessage integMessage;
	private String status;
	private String statusDescription;
	private String reasonCode;
	private String reasonDescription;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public MPAY_BankIntegMessage getIntegMessage() {
		return integMessage;
	}

	public void setIntegMessage(MPAY_BankIntegMessage integMessage) {
		this.integMessage = integMessage;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReasonDescription() {
		return reasonDescription;
	}

	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}
}
