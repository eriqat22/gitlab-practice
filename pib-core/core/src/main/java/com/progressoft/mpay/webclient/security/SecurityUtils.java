package com.progressoft.mpay.webclient.security;

import com.progressoft.mpay.HashingAlgorithm;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.entities.MPAY_AppsChecksum;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.ExtraDataComparer;
import com.progressoft.mpay.messages.MPayRequest;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

public final class SecurityUtils {
    private SecurityUtils() {
    }

    public static String createToken(MPayRequest requestModel) {
        if (requestModel == null) {
            return null;
        } else {
            MPAY_AppsChecksum appChecksum = MPayContext.getInstance().getLookupsLoader().getAppChecksum(MPayContext.getInstance().getSystemParameters().getCoreAppCheckSumCode());
            String checksum = "";
            if (MPayContext.getInstance().getSystemParameters().getEnableAppsChecksums())
                checksum = appChecksum.getChecksum();
            String hash = HashingAlgorithm.hash(MPayContext.getInstance().getSystemParameters(), requestModel.toValues(checksum));
            if (!MPayContext.getInstance().getSystemParameters().enableTokenEncryption())
                return hash;
            return MPayCryptographer.DefaultCryptographer.INSTANCE.get().encrypt(hash, false);
        }
    }
}
