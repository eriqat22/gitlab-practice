package com.progressoft.mpay.limits;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;

public class ValidateEditLimitsScheme implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateEditLimitsScheme.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside ValidateEditLimitsScheme --------------------- ");

		InvalidInputException iie = new InvalidInputException();

		MPAY_LimitsScheme limitScheme = (MPAY_LimitsScheme) arg0.get(WorkflowService.WF_ARG_BEAN);
		for (MPAY_Limit limit : limitScheme.getRefSchemeLimits()) {
			for (MPAY_LimitsDetail limitDetail : limit.getRefLimitLimitsDetails()) {
				if (limitDetail.getMsgType() == null) {
					iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("limit.invalid.attachedToProfile"));
					throw iie;
				}
			}
		}
	}
}
