package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.progressoft.mpay.LookupsLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;

public class ValidateEditMobileAccount implements Validator {
	private static final Logger logger = LoggerFactory.getLogger(ValidateEditMobileAccount.class);

	private static final String LIGHT_WALLET_PROFILE = "Light Wallet Profile Id";
	private static final String AUTO_REGISTERED_PROFILE = "Auto Registered Wallet Profile Id";
	Long lightWallet;
	Long autoRegistration;

	@Autowired
	ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("validate =======");

		MPAY_MobileAccount currentAccount = (MPAY_MobileAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
		validateMasLength(currentAccount);
		MPAY_CustomerMobile mobile = currentAccount.getMobile();
		List<MPAY_MobileAccount> allAccounts = DataProvider.instance().listMobileAccounts(mobile.getId());
		if (allAccounts == null || allAccounts.isEmpty())
			return;
		Iterator<MPAY_MobileAccount> accounts = allAccounts.iterator();
		do {
			MPAY_MobileAccount account = accounts.next();
			if (account.getId() == currentAccount.getId())
				continue;
			validateEditAccount(account, currentAccount);

		} while (accounts.hasNext());
	}

	private void validateEditAccount(MPAY_MobileAccount account, MPAY_MobileAccount currentAccount) throws InvalidInputException {

		lightWallet = Long.valueOf(LookupsLoader.getInstance().getSystemConfigurations(LIGHT_WALLET_PROFILE).getConfigValue());
		autoRegistration = Long.valueOf(LookupsLoader.getInstance().getSystemConfigurations(AUTO_REGISTERED_PROFILE).getConfigValue());

		InvalidInputException cause = new InvalidInputException();
		String storedRegistrationId = account.getBank().getCode() + String.valueOf(account.getMas());
		if (storedRegistrationId.equals(currentAccount.getBank().getCode() + String.valueOf(currentAccount.getMas()))) {
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_MAS));
			throw cause;
		}
		if (currentAccount.getExternalAcc() != null && currentAccount.getExternalAcc().equals(account.getExternalAcc())) {
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_EXTERNAL_ACCOUNT));
			throw cause;
		}
		/*if (currentAccount.getIsDefault() && account.getIsDefault()) {
			if(lightWallet.compareTo(account.getRefProfile().getId()) != 0 || autoRegistration.compareTo(account.getRefProfile().getId()) != 0) {
				cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_ISDEFUALT));
				throw cause;
			}
		}*/
	}

	private void validateMasLength(MPAY_MobileAccount mobileAccount) throws InvalidInputException {
		if (String.valueOf(mobileAccount.getMas()).length() > SystemParameters.getInstance().getMobileAccountSelectorLength()) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg(MPayErrorMessages.INVALID_MAS_LENGTH), SystemParameters.getInstance().getMobileAccountSelectorLength()));
			throw iie;
		}
	}
}