package com.progressoft.mpay.transactions;

import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.JournalVoucherFlags;
import com.progressoft.mpay.entity.JvDetailsTypes;
import com.progressoft.mpay.plugins.ProcessingContext;

public class DebitTransactionCreator extends TransactionCreator {

	@Override
	protected void createOnusJVs(ProcessingContext context, MPAY_Transaction transaction) {
		createJVDetail(transaction, JournalVoucherFlags.DEBIT, context.getReceiver().getAccount(), context.getAmount(), context.getLookupsLoader().getJvTypes(JvDetailsTypes.TRANSFER.toString()), null);
		createJVDetail(transaction, JournalVoucherFlags.CREDIT, context.getSender().getAccount(), context.getAmount(), context.getLookupsLoader().getJvTypes(JvDetailsTypes.TRANSFER.toString()), null);
		}

	@Override
	protected void createOutwardJVs(ProcessingContext context, MPAY_Transaction transaction) {
		createJVDetail(transaction, JournalVoucherFlags.DEBIT, context.getLookupsLoader().getPSP().getPspMPClearAccount(), context.getAmount(), context.getLookupsLoader().getJvTypes(JvDetailsTypes.TRANSFER.toString()), null);
		createJVDetail(transaction, JournalVoucherFlags.CREDIT, context.getSender().getAccount(), context.getAmount(), context.getLookupsLoader().getJvTypes(JvDetailsTypes.TRANSFER.toString()), null);
	}

	@Override
	protected void createInwardJVs(ProcessingContext context, MPAY_Transaction transaction) {
		createJVDetail(transaction, JournalVoucherFlags.DEBIT, context.getReceiver().getAccount(), context.getAmount(), context.getLookupsLoader().getJvTypes(JvDetailsTypes.TRANSFER.toString()), null);
		createJVDetail(transaction, JournalVoucherFlags.CREDIT, context.getLookupsLoader().getPSP().getPspMPClearAccount(), context.getAmount(), context.getLookupsLoader().getJvTypes(JvDetailsTypes.TRANSFER.toString()), null);
	}

}
