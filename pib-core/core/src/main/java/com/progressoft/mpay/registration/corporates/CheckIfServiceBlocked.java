package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class CheckIfServiceBlocked implements Condition {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVar.get(WorkflowService.WF_ARG_BEAN);
		return service.getIsBlocked();
	}
}