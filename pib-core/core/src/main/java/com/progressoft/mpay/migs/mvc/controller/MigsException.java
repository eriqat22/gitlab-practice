package com.progressoft.mpay.migs.mvc.controller;

public class MigsException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public MigsException(String message) {
		super(message);
	}

	public MigsException(Throwable cause) {
		super(cause);
	}

	public MigsException(String message , Throwable cause) {
		super(message,cause);
	}

}
