package com.progressoft.mpay.payments.cashin;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.payments.CashMessage;
import com.progressoft.mpay.payments.CashRequestHelper;
import com.progressoft.mpay.payments.PaymentsOperations;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.psp.PSP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

public class PostServiceCashInRequest implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostServiceCashInRequest.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside execute =======================");

        MPAY_ServiceCashIn serviceCashIn = (MPAY_ServiceCashIn) arg0.get(WorkflowService.WF_ARG_BEAN);

        if (!SystemNetworkStatus.isSystemOnline(DataProvider.instance())) {
            throw new WorkflowException(
                    LookupsLoader.getInstance().getReason(ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE).getReasonsNLS().get(SystemParameters.getInstance().getSystemLanguage().getId()).getDescription());
        }
        try {
            MPayRequest request = new MPayRequest(PaymentsOperations.SERVICE_CASH_IN, getPSPCashInService(serviceCashIn.getRefCorporateService()).getAlias(), ReceiverInfoType.ALIAS, null,
                    LanguageMapper.getInstance().getLanguageLongByLocaleCode(SystemParameters.getInstance().getSystemLanguage().getCode()), UUID.randomUUID().toString(), serviceCashIn.getTenantId());
            request.getExtraData().add(new ExtraData(CashMessage.AMOUNT_KEY, serviceCashIn.getTotalAmount().toString()));
            request.getExtraData().add(new ExtraData(CashMessage.CHARGE_AMOUNT_KEY, serviceCashIn.getChargeAmount().toString()));
            request.getExtraData().add(new ExtraData(CashMessage.RECEIVER_KEY, serviceCashIn.getRefCorporateService().getName()));
            request.getExtraData().add(new ExtraData(CashMessage.RECEIVER_TYPE_KEY, ReceiverInfoType.CORPORATE));
            request.getExtraData().add(new ExtraData(CashMessage.RECEIVER_ACCOUNT_KEY, serviceCashIn.getRefServiceAccount()));
            request.getExtraData().add(new ExtraData(CashMessage.NOTES_KEY, serviceCashIn.getComments()));
            MessageProcessingContext context = new MessageProcessingContext(DataProvider.instance(), LookupsLoader.getInstance(), SystemParameters.getInstance(), new MPayCryptographer(),
                    new NotificationHelper());
            context.setRequest(request);
            context.setOriginalRequest(request.toString());
            context.setOperation(LookupsLoader.getInstance().getEndPointOperation(request.getOperation()));
            context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
            MessageProcessor processor = MessageProcessorHelper.getProcessor(context.getOperation());
            if (processor == null)
                throw new WorkflowException("Service Cash In processor not available");

            MPAY_BulkPayment bulkPayment = context.getDataProvider().getBulkPayment(context.getSystemParameters().getBulkPaymentDefaultId());
            if (bulkPayment == null)
                throw new WorkflowException("Default Bulk Payment ID Not Found");
            MPAY_MPayMessage message = MessageProcessorHelper.createMessage(new ServiceUserManager(), context, ProcessingStatusCodes.PENDING, null, null, bulkPayment);
            message.setRefMessageServiceCashIn(new ArrayList<MPAY_ServiceCashIn>());
            message.getRefMessageServiceCashIn().add(serviceCashIn);
            serviceCashIn.setRefMessage(message);
            DataProvider.instance().persistMPayMessage(message);
            context.setMessage(message);
            MessageProcessingResult result = processor.processMessage(context);
            serviceCashIn.setProcessingStatus(result.getMessage().getProcessingStatus());
            serviceCashIn.setReason(result.getMessage().getReason());
            serviceCashIn.setReasonDesc(result.getMessage().getReasonDesc());
            String response = CashRequestHelper.generateResponse(Long.parseLong(result.getMessage().getReference()), result.getMessage().getProcessingStatus().getCode(),
                    result.getMessage().getReason(), SystemParameters.getInstance().getSystemLanguage());
            result.getMessage().setResponseContent(response);
            DataProvider.instance().persistProcessingResult(result);
            if (result.getStatus().getCode().equals(ProcessingStatusCodes.REJECTED) || result.getStatus().getCode().equals(ProcessingStatusCodes.FAILED)) {
                JfwHelper.executeActionWithServiceUser(MPAYView.SERVICE_CASH_IN, serviceCashIn.getId(), "SVC_Reject");
            }
            if (result.getStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED)) {
                NotificationHelper.getInstance().sendNotifications(result);
            } else if (result.getStatus().getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED) && result.getMpClearMessage() != null && !result.isHandleMPClearOffline()) {
                MPClearHelper.sendToActiveMQ(result.getMpClearMessage(), MPayContext.getInstance().getCommunicator());
            }
//            result.getNotifications().forEach(n -> context.getDataProvider().persistNotification(n));

        } catch (Exception e) {

            throw new WorkflowException(e);
        }
    }

    MPAY_CorpoarteService getPSPCashInService(MPAY_CorpoarteService service) throws WorkflowException {
        MPAY_CorpoarteService pspCashInService = new MPAY_CorpoarteService();
        MPAY_Corporate psp = PSP.get();
        for (MPAY_CorpoarteService oService : psp.getRefCorporateCorpoarteServices()) {
            if (MessageCodes.CI.toString().equals(oService.getPaymentType().getCode()) && oService.getBank().getId() == service.getBank().getId()) {
                pspCashInService = oService;
            }
        }

        return pspCashInService;
    }
}