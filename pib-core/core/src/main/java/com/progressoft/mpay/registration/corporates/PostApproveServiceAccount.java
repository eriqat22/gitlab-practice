package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclear.IMPClearClient;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static java.util.Objects.nonNull;

public class PostApproveServiceAccount implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostApproveServiceAccount.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute--------------------------");
        MPAY_ServiceAccount account = (MPAY_ServiceAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
        if (nonNull(account.getIsRegistered()) && account.getIsRegistered())
            JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.APPROVE, new WorkflowException());
        else
            handleAddAccount(account);
    }

    private void handleAddAccount(MPAY_ServiceAccount account) throws WorkflowException {
        logger.debug("inside HandleAddAccount--------------------------");
        IMPClearClient client = MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor());
        if (!account.getService().getRefCorporate().getIsRegistered())
            client.addCorporate(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), account.getService().getRefCorporate(), account.getService());
        else
            client.addServiceAccount(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), account.getService().getRefCorporate(), account.getService(), account);
    }
}
