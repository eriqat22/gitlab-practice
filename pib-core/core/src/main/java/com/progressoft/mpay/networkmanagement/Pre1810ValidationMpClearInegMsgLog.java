package com.progressoft.mpay.networkmanagement;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegRjctReason;
import com.progressoft.mpay.entities.MPAY_MpClearResponseCode;
import com.progressoft.mpay.entities.MPAY_NetworkManIntegMsg;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.MpClearIntegRjctReasonCodes;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Pre1810ValidationMpClearInegMsgLog implements FunctionProvider {

	@Autowired
	private ItemDao itemDao;
	private static final Logger logger = LoggerFactory.getLogger(Pre1810ValidationMpClearInegMsgLog.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside Pre1810ValidationMpClearInegMsgLog -----------------------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());
		MPAY_MpClearIntegRjctReason rsn = new MPAY_MpClearIntegRjctReason();
		try {
			rsn = technicalValidation(msg, isomsg);
		} catch (BusinessException e) {
			throw new WorkflowException(e);
		}
		if (rsn.getCode().equals(MpClearIntegRjctReasonCodes.VALID)) {
			transientVars.put(Result.VALIDATION_RESULT, Result.SUCCESSFUL);
		} else {
			transientVars.put(Result.VALIDATION_RESULT, Result.FAILED);
			rsn = LookupsLoader.getInstance().getMpClearIntegrationReasons(rsn.getCode());
			msg.setReason(rsn);
		}
	}

	private MPAY_MpClearIntegRjctReason technicalValidation(MPAY_MpClearIntegMsgLog msg, IsoMessage isomsg) {
		MPAY_MpClearIntegRjctReason rsn = new MPAY_MpClearIntegRjctReason();
		rsn.setCode(MpClearIntegRjctReasonCodes.VALID);

		if (!msg.getSource().equalsIgnoreCase(IntegMessagesSource.MP_CLEAR)) {
			rsn.setCode(MpClearIntegRjctReasonCodes.INVALID_SOURCE);
		}
		if (!msg.getRefSender().equals(LookupsLoader.getInstance().getMPClear().getRegistrationId())) {
			rsn.setCode(MpClearIntegRjctReasonCodes.INVALID_SENDER_ID);
		}

		MPAY_NetworkManIntegMsg originalNetworkMessage = itemDao.getItem(MPAY_NetworkManIntegMsg.class, "refMessage", isomsg.getField(116).toString());
		if (originalNetworkMessage == null) {
			rsn.setCode(MpClearIntegRjctReasonCodes.INVALID_ORIGINAL_MESSAGE_ID);
		}

		MPAY_NetworkManIntegMsg networkMessage = itemDao.getItem(MPAY_NetworkManIntegMsg.class, "refMessage", isomsg.getField(115).toString());
		if (networkMessage != null) {
			rsn.setCode(MpClearIntegRjctReasonCodes.DUPLICATE_MESSAGE_ID);
		}

		MPAY_MpClearResponseCode oMpClearIntegRjctReason = LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(isomsg.getField(44).toString());
		if (oMpClearIntegRjctReason == null) {
			rsn.setCode(MpClearIntegRjctReasonCodes.INVALID_RESPONSE_CODE);
		}

		return rsn;
	}
}
