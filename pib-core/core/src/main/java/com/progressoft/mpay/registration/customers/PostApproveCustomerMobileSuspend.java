package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostApproveCustomerMobileSuspend implements FunctionProvider {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(PostApproveCustomerMobileSuspend.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN);
		mobile.setIsActive(false);
		handleMobileAccounts(mobile);
	}

	private void handleMobileAccounts(MPAY_CustomerMobile mobile) throws WorkflowException {
		logger.debug("inside HandleMobileAccounts--------------------------");
		List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_MobileAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_MobileAccount account = accountsIterator.next();
			if (!account.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.DELETED.toString()) && !account.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.APPROVED.toString())) {
				InvalidInputException iie = new InvalidInputException();
				iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.MOBILE_ACCOUNT_UNDER_MODIFICATION));
				throw iie;
			}
			JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.SUSPEND, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
}
