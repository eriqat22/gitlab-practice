package com.progressoft.mpay.valueprovider;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.progressoft.mpay.MPayContext;

@Component
public class OperatorValueProvider {

	public Map<String, String> getOperators() {
		String operatorParameter = MPayContext.getInstance().getSystemParameters().getOperators();
		List<String> operatorSplits = Arrays.asList(operatorParameter.split(","));
		Map<String, String> map = new HashMap<>();
		operatorSplits.stream().forEach(o -> map.put(o, o));
		return map;
	}
}