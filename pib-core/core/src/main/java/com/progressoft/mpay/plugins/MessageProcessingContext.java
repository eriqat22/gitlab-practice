package com.progressoft.mpay.plugins;

import com.progressoft.mpay.ICryptographer;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.notifications.INotificationHelper;

public class MessageProcessingContext extends ProcessingContext {
	private MPayRequest request;
	private String originalRequest;
	private String token;
	private MPAY_Language language;

	public MessageProcessingContext(IDataProvider dataProvider, ILookupsLoader lookupsLoader, ISystemParameters systemParameters, ICryptographer cryptographer, INotificationHelper notificationHelper) {
		super(dataProvider, lookupsLoader, systemParameters, cryptographer, notificationHelper);
	}

	public MessageProcessingContext(MPClearProcessingContext mpClearContext) {
		super(mpClearContext.getDataProvider(), mpClearContext.getLookupsLoader(), mpClearContext.getSystemParameters(), mpClearContext.getCryptographer(), mpClearContext.getNotificationHelper());
		setTransactionConfig(mpClearContext.getTransactionConfig());
		setOperation(mpClearContext.getOperation());
		setTransactionNature(mpClearContext.getTransactionNature());
		if (mpClearContext.getOriginalMessage() != null)
			setMessage(mpClearContext.getOriginalMessage().getRefMessage())	;
	}

	public MPayRequest getRequest() {
		return request;
	}

	public void setRequest(MPayRequest request) {
		this.request = request;
	}

	public String getOriginalRequest() {
		return originalRequest;
	}

	public void setOriginalRequest(String originalRequest) {
		this.originalRequest = originalRequest;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public MPAY_Language getLanguage() {
		return language;
	}

	public void setLanguage(MPAY_Language language) {
		this.language = language;
	}
}