package com.progressoft.mpay.requesttypes;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_RequestTypesDetail;
import com.progressoft.mpay.entities.MPAY_RequestTypesScheme;
import com.progressoft.mpay.entity.MPAYView;

public class PostDeleteRequestTypes implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostApproveRequestTypes.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		try {
			logger.debug("Inside PostCreateLimitsScheme----------------");
			MPAY_RequestTypesScheme requestType = (MPAY_RequestTypesScheme) arg0.get(WorkflowService.WF_ARG_BEAN);
			processMessageTypes(requestType);
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}

	private void processMessageTypes(MPAY_RequestTypesScheme requestType) throws WorkflowException {
		List<MPAY_RequestTypesDetail> details = requestType.getRefSchemeRequestTypesDetails();
		for (MPAY_RequestTypesDetail detail : details) {
            logger.debug("Delete details for message type: " + detail.getMsgType().getDescription());
            JfwHelper.executeAction(MPAYView.REQUEST_TYPES_DETAILS, detail.getId(), "Delete");
        }
	}
}