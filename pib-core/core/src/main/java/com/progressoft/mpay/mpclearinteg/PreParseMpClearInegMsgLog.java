package com.progressoft.mpay.mpclearinteg;

import java.util.Calendar;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegRjctReason;
import com.progressoft.mpay.entity.MpClearIntegRjctReasonCodes;
import com.solab.iso8583.IsoMessage;

public class PreParseMpClearInegMsgLog implements FunctionProvider {

	private static final Logger logger = LoggerFactory
			.getLogger(PreParseMpClearInegMsgLog.class);

	@Autowired
	private ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps)
			throws WorkflowException {
		logger.debug("Inside PreParseMpClearInegMsgLog ==========");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars
				.get(WorkflowService.WF_ARG_BEAN);

		String reasonCode = veryfiSignature(msg);
		if (!reasonCode.equalsIgnoreCase(MpClearIntegRjctReasonCodes.VALID)) {
			msg.setIntegMsgType(null);
			MPAY_MpClearIntegRjctReason rsn = LookupsLoader.getInstance().getMpClearIntegrationReasons(reasonCode);
			msg.setReason(rsn);
		} else {
			IsoMessage isoMessage = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath())
					.parseMessage(msg.getContent());
			msg.setIntegMsgType(String.format("%04x", isoMessage.getType()));
			if (msg.getHint() == null)
				msg.setHint(" ");
			itemDao.merge(msg);
		}

	}

	private String veryfiSignature(MPAY_MpClearIntegMsgLog msg)
			throws WorkflowException {
		String reasonCode;

		Calendar cal = Calendar.getInstance();
		cal.setTime(msg.getSigningStamp());

		boolean result = new MPayCryptographer().verifyToken(SystemParameters.getInstance(), msg.getContent(),
				msg.getToken(), cal, msg.getSource());

		if (!result)
			reasonCode = MpClearIntegRjctReasonCodes.INVALID_SIGNATURE;
		else
			reasonCode = MpClearIntegRjctReasonCodes.VALID;

		return reasonCode;
	}
}
