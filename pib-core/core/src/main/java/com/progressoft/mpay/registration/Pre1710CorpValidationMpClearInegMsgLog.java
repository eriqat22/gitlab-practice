package com.progressoft.mpay.registration;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CorpIntegMessage;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegRjctReason;
import com.progressoft.mpay.entities.MPAY_MpClearResponseCode;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.MpClearIntegRjctReasonCodes;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Pre1710CorpValidationMpClearInegMsgLog implements FunctionProvider {

	@Autowired
	private ItemDao itemDao;
	private static final Logger logger = LoggerFactory.getLogger(Pre1710CorpValidationMpClearInegMsgLog.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {

		logger.debug("inside Pre1710CorpValidationMpClearInegMsgLog---------------------------");
		MPAY_MpClearIntegMsgLog messageLog = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		IsoMessage isoMessage = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(messageLog.getContent());

		String reasonCode = technicalValidation(messageLog, isoMessage);
		if (reasonCode == null)
			transientVars.put(Result.VALIDATION_RESULT, Result.SUCCESSFUL);
		else {
			transientVars.put(Result.VALIDATION_RESULT, Result.FAILED);
			messageLog.setReason(itemDao.getItem(MPAY_MpClearIntegRjctReason.class, "code", reasonCode));
		}
	}

	private String technicalValidation(MPAY_MpClearIntegMsgLog messageLog, IsoMessage isoMessage) {
		if (!messageLog.getSource().equalsIgnoreCase(IntegMessagesSource.MP_CLEAR))
			return MpClearIntegRjctReasonCodes.INVALID_SOURCE;

		MPAY_MpClearResponseCode responseCode = LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(isoMessage.getField(44).toString());
		if (null == responseCode)
			return MpClearIntegRjctReasonCodes.INVALID_RESPONSE_CODE;

		MPAY_CorpIntegMessage originalMessage = getOrigionalMessageByMessageID(isoMessage.getField(116).toString());
		if (originalMessage == null)
			return MpClearIntegRjctReasonCodes.INVALID_ORIGINAL_MESSAGE_ID;

		if (!originalMessage.getIntegMsgType().getCode().equalsIgnoreCase(IntegMessageTypes.TYPE_1700))
			return MpClearIntegRjctReasonCodes.INVALID_ORIGINAL_MESSAGE_ID;

		return null;
	}

	private MPAY_CorpIntegMessage getOrigionalMessageByMessageID(String messageId) {
		MPAY_CorpIntegMessage value = null;

		List<MPAY_CorpIntegMessage> items = itemDao.getItems(MPAY_CorpIntegMessage.class, 0, 1, null, null, "refMessage='" + messageId + "'", null);
		if (items != null && !items.isEmpty())
			value = items.get(0);
		return value;
	}
}
