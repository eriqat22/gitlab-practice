package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemNetworkStatus;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class PSPValidator {
	private static final Logger logger = LoggerFactory.getLogger(PSPValidator.class);

	private PSPValidator() {

	}

	public static ValidationResult validate(ProcessingContext context, boolean isOnlineRequired) {
		logger.debug("Inside Validate ...");
		logger.debug("isOnlineRequired:" + isOnlineRequired);
		if (context == null)
			throw new NullArgumentException("context");
		if (!SystemNetworkStatus.isSystemOnline(context.getDataProvider()) && isOnlineRequired)
			return new ValidationResult(ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE, null, false);
		if (context.getSystemParameters().getOfflineModeEnabled() && isOnlineRequired)
			return new ValidationResult(ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
