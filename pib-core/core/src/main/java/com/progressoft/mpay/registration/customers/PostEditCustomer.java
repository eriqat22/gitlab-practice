package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entity.CustomerMetaData;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class PostEditCustomer implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostEditCustomer.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("Inside execute =====");
        MPAY_Customer originalCustomer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN_ORIGINAL);
        MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
        customer.setFullName(customer.getFirstName() + " " + customer.getMiddleName() + " " + customer.getLastName());

        if (customer.getIsRegistered()) {
            presistApprovedData(customer, originalCustomer);
        }

    }

    private void presistApprovedData(MPAY_Customer customer, MPAY_Customer originalCustomer) {
        CustomerMetaData customerMetaData = new CustomerMetaData(originalCustomer);
        customer.setApprovedData(customerMetaData.toString());
    }

    private String getOperator(String customerOperatorValue) {
        try {
            JSONObject json = new JSONObject(customerOperatorValue);
            if (json.isNull("key"))
                return customerOperatorValue;
            return json.getString("key");
        } catch (Exception e) {
            return customerOperatorValue;
        }
    }
}