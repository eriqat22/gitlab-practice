package com.progressoft.mpay.messages;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.OperationContract;
import com.progressoft.mpay.ServiceUserManager;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.mpclear.MPayCommunicatorHelper;
import com.progressoft.mpay.notifications.NotificationHelper;

public class MPayMessageHandler implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(MPayMessageHandler.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		logger.debug("Inside ... MPayMessageHandler");
		String body = (String) exchange.getIn().getBody();
		String token = exchange.getIn().getHeader(OperationContract.TOKEN, String.class);
		MPayRequestProcessor processor = new MPayRequestProcessor((IDataProvider) AppContext.getApplicationContext().getBean("DataProvider"), SystemParameters.getInstance(),
				LookupsLoader.getInstance(), new MPayCryptographer(), NotificationHelper.getInstance(), MPayCommunicatorHelper.getCommunicator());
		MPayResponseEnvelope response = processor.processMessage(body, token, new ServiceUserManager());
		exchange.getOut().setBody(response.toString());
	}
}
