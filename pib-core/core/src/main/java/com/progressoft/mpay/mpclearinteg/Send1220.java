package com.progressoft.mpay.mpclearinteg;

import java.util.Calendar;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemNetworkStatus;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.mpclear.MPayCommunicatorHelper;

public class Send1220 implements Processor {
	private static final Logger logger = LoggerFactory.getLogger(Send1220.class);

	@Autowired
	private ItemDao itemDao;

	@Override
	@SuppressWarnings("unchecked")
	public void process(Exchange exchange) throws Exception {
		try {
			IntegrationUtils.enableServiceUser((String) exchange.getProperty("tenantId"));
			logger.debug("inside Send1220...");

			if (!SystemNetworkStatus.isSystemOnline(DataProvider.instance())) {
				logger.debug("Cannot send 1220 messages while offline, existing processing");
				return;
			}

			String statement = "FROM MPAY_MpClearIntegMsgLog WHERE isProcessed=0 AND isCanceled=0 AND integMsgType='"
					+ IntegMessageTypes.TYPE_1220 + "' AND tenantId='" + (String) exchange.getProperty("tenantId") + "'";
			
			List<MPAY_MpClearIntegMsgLog> messages = itemDao.getEntityManager().createQuery(statement).getResultList();
			
			StringBuffer updateQueryBuffer = new StringBuffer();
			
			if(!messages.isEmpty()) {
			
			for (MPAY_MpClearIntegMsgLog message : messages) {
				updateQueryBuffer.append(updateSignature(message));
				MPClearHelper.sendToActiveMQ(message, MPayCommunicatorHelper.getCommunicator());
			}
			
			itemDao.getEntityManager().createQuery(updateQueryBuffer.toString()).executeUpdate();
			} else {
				logger.info("There is no data to send <><> 1220 ");
			}
		} catch (Exception ex) {
			logger.error(ex.toString(), ex);
		}
	}

	private Calendar getSigningTimestamp() {
		Calendar date = SystemHelper.getCalendar();
		date.set(Calendar.MILLISECOND, 0);
		return date;
	}

	private String updateSignature(MPAY_MpClearIntegMsgLog msgLog) throws WorkflowException {
		Calendar date = getSigningTimestamp();
		String signingDate = SystemHelper.formatDate(date.getTime(),
				SystemParameters.getInstance().getSigningDateFormat());
		String token = new MPayCryptographer().generateToken(SystemParameters.getInstance(), msgLog.getContent(),
				signingDate, IntegMessagesSource.SYSTEM);
		return "UPDATE MPAY_MpClearIntegMsgLog SET token = '" + token + "' WHERE id = " + msgLog.getId() + ";\n\r";
	}
}