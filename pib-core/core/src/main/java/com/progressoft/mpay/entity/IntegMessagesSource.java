package com.progressoft.mpay.entity;

public final class IntegMessagesSource {
    public static final String SYSTEM = "1";
    public static final String MP_CLEAR = "2";
    public static final String E_PAY = "3";
    public static final String E_FAWATERCOM = "4";
    private IntegMessagesSource() {
    }
}
