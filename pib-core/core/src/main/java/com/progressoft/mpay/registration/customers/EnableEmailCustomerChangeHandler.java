package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.workflow.WfChangeHandler;
import com.progressoft.jfw.workflow.WfChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Customer;
import org.springframework.stereotype.Component;

@Component
public class EnableEmailCustomerChangeHandler extends WfChangeHandler<MPAY_Customer> {
    @Override
    public void handle(WfChangeHandlerContext<MPAY_Customer> wfContext) {
        wfContext.field(MPAY_Customer.EMAIL)
                .setRequired(wfContext.getEntity().getEnableEmail());
    }
}
