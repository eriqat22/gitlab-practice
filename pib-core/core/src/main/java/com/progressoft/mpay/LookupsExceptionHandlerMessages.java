package com.progressoft.mpay;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.ErrorHandler;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.util.simpleview.SimpleView;
import com.progressoft.mpay.entities.MPAY_Bank;

public class LookupsExceptionHandlerMessages extends ErrorHandler {

    @SuppressWarnings("rawtypes")
	private static final Set<Class> classes = new HashSet<>();

    static {
        classes.add(DataIntegrityViolationException.class);
        classes.add(TransactionSystemException.class);
        classes.add(DataIntegrityViolationException.class);
        classes.add(TransactionSystemException.class);
        classes.add(SQLIntegrityConstraintViolationException.class);
    }


    @Autowired
    private ItemDao itemDao;

    @Override
    public InvalidInputException handleError(Exception ex, SimpleView simpleView, Object entity, int actionKey) {
        InvalidInputException cause = null;
        if (supportedExceptionType(ex) && entity instanceof JFWLookableEntity) {
            cause = Unique.validate(itemDao, (JFWLookableEntity) entity, "name",
                    ((JFWLookableEntity) entity).getName(), "duplicat.lookup.item.name");
        }
        if (simpleView != null && cause == null) {
            cause = new InvalidInputException();
            cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("duplicat.lookup.item.code"));
            return cause;
        }

        if (entity instanceof MPAY_Bank && cause == null) {
            cause = Unique.validate(itemDao, (MPAY_Bank) entity, "tellerCode",
                    ((MPAY_Bank) entity).getTellerCode(), "duplicat.bank.item.tellercode");
        }
        return cause;
    }

    private boolean supportedExceptionType(Exception ex) {
        return classes.parallelStream().anyMatch(c -> c.isInstance(ex.getCause()) || c.isInstance(ex));
    }
}
