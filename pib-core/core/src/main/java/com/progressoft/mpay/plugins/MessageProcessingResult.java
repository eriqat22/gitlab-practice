package com.progressoft.mpay.plugins;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Transaction;

public class MessageProcessingResult {
	private MPAY_MPayMessage message;
	private MPAY_Transaction transaction;
	private MPAY_ProcessingStatus status;
	private String statusDescription;
	private String reasonCode;
	private MPAY_MpClearIntegMsgLog mpClearMessage;
	private boolean handleMPClearOffline;
	private List<MPAY_Notification> notifications;
	private String response;

	public MessageProcessingResult(){
		this.setNotifications(new ArrayList<MPAY_Notification>());
	}

	public MPAY_MPayMessage getMessage() {
		return message;
	}
	public void setMessage(MPAY_MPayMessage message) {
		this.message = message;
	}
	public MPAY_Transaction getTransaction() {
		return transaction;
	}
	public void setTransaction(MPAY_Transaction transaction) {
		this.transaction = transaction;
	}
	public MPAY_ProcessingStatus getStatus() {
		return status;
	}
	public void setStatus(MPAY_ProcessingStatus status) {
		this.status = status;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public MPAY_MpClearIntegMsgLog getMpClearMessage() {
		return mpClearMessage;
	}
	public void setMpClearMessage(MPAY_MpClearIntegMsgLog mpClearMessage) {
		this.mpClearMessage = mpClearMessage;
	}
	public boolean isHandleMPClearOffline() {
		return handleMPClearOffline;
	}
	public void setHandleMPClearOffline(boolean handleMPClearOffline) {
		this.handleMPClearOffline = handleMPClearOffline;
	}

	public List<MPAY_Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<MPAY_Notification> notifications) {
		this.notifications = notifications;
	}

	public static MessageProcessingResult create(MessageProcessingContext context, MPAY_ProcessingStatus status, MPAY_Reason reason, String reasonDescription){
		MessageProcessingResult result = new MessageProcessingResult();
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		result.setReasonCode(reason.getCode());
		if(context.getMessage() != null){
			context.getMessage().setProcessingStatus(status);
			context.getMessage().setReason(reason);
			context.getMessage().setReasonDesc(reasonDescription);
			result.setMessage(context.getMessage());
		}
		if(context.getTransaction() != null){
			context.getTransaction().setProcessingStatus(status);
			context.getTransaction().setReason(reason);
			context.getTransaction().setReasonDesc(reasonDescription);
			result.setTransaction(context.getTransaction());
		}

		return result;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
