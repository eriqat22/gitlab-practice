package com.progressoft.mpay.transactions;

import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.messages.TransactionTypeCodes;

public class TransactionCreatorFactory {

	private TransactionCreatorFactory() {

	}

	public static TransactionCreator create(String type) {
		if (TransactionTypeCodes.DIRECT_CREDIT.equals(type))
			return new CreditTransactionCreator();
		else if (TransactionTypeCodes.DIRECT_DEBIT.equals(type))
			return new DebitTransactionCreator();
		else
			throw new InvalidArgumentException("type");
	}
}
