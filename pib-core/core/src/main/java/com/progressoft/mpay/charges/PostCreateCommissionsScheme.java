package com.progressoft.mpay.charges;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Commission;
import com.progressoft.mpay.entities.MPAY_CommissionScheme;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entity.MPAYView;

public class PostCreateCommissionsScheme implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCreateCommissionsScheme.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		try {
			logger.debug("Inside PostCreateCommissionsScheme----------------");
			MPAY_CommissionScheme scheme = (MPAY_CommissionScheme) arg0.get(WorkflowService.WF_ARG_BEAN);

			List<MPAY_MessageType> msgTypes = LookupsLoader.getInstance().getMessageTypes();
			for (MPAY_MessageType messageType : msgTypes) {
				MPAY_Commission details = new MPAY_Commission();
				details.setIsActive(false);
				details.setRefCommissionScheme(scheme);
				details.setMsgType(messageType);
				details.setDescription(messageType.getDescription());
				JfwHelper.createEntity(MPAYView.COMMISSIONS, details);
				logger.debug("Setting commission details for message type: " + messageType.getDescription());
			}

		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}
}
