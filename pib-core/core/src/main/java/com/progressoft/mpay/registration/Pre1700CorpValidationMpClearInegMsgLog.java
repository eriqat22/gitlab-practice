package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entity.IntegMessagesSource;

public class Pre1700CorpValidationMpClearInegMsgLog implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(Pre1700CorpValidationMpClearInegMsgLog.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("Inside execute ---------------");

		MPAY_MpClearIntegMsgLog messageLog = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		String pspId = LookupsLoader.getInstance().getPSP().getNationalID();
		if (!messageLog.getSource().equalsIgnoreCase(IntegMessagesSource.SYSTEM) || !messageLog.getRefSender().equalsIgnoreCase(pspId))
			transientVars.put(Result.VALIDATION_RESULT, Result.FAILED);
		else
			transientVars.put(Result.VALIDATION_RESULT, Result.SUCCESSFUL);
	}
}
