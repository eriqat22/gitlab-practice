package com.progressoft.mpay.banks;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.HashingAlgorithm;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_BanksUser;

public class ValidateBankUsersLicense implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateBankMetaData.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside ValidateBankMetaData ==========");
		InvalidInputException iie = new InvalidInputException();
		MPAY_BanksUser bankUser = (MPAY_BanksUser) arg0.get(WorkflowService.WF_ARG_BEAN);
		String license = MPayContext.getInstance().getSystemParameters().getBanksUsersLicense();
		String beforeKey = "P$LICEN$E";
		String afterKey = "B@NKU$ER$";
		int count = -1;
		for (int i = 0; i <= 100; i++) {
			String result = HashingAlgorithm.hash(MPayContext.getInstance().getSystemParameters(), beforeKey + i + afterKey);
			if (result.equals(license)) {
				count = i;
				break;
			}
		}
		if (count == -1) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg("banksusers.invalidlicense"), SystemParameters.getInstance().getRoutingCodeLength()));
			throw iie;
		}
		List<MPAY_BanksUser> banksUsersForUnique = MPayContext.getInstance().getDataProvider().listBanksUsers().stream().filter(u -> u.getId() != bankUser.getId()).collect(Collectors.toList());
		if (banksUsersForUnique.stream().filter(u -> u.getUserName().equalsIgnoreCase(bankUser.getUserName())).count() > 0) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg("banksusers.usernamealreadyinuse"), SystemParameters.getInstance().getRoutingCodeLength()));
			throw iie;
		}
		if (count == 0)
			return;
		List<MPAY_BanksUser> banksUsersForLicense = MPayContext.getInstance().getDataProvider().listBanksUsers().stream().filter(distinctByKey(u -> u.getBank().getId()))
				.collect(Collectors.toList());
		if (banksUsersForLicense.size() > count) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg("banksusers.maxcountreached"), SystemParameters.getInstance().getRoutingCodeLength()));
			throw iie;
		}
	}
	private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
	{
		Map<Object, Boolean> map = new ConcurrentHashMap<>();
		return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}
}