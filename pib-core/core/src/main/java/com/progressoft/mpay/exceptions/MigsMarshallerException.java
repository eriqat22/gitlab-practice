package com.progressoft.mpay.exceptions;

public class MigsMarshallerException extends RuntimeException {
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public MigsMarshallerException(Exception e) {
        super(e);
    }
}
