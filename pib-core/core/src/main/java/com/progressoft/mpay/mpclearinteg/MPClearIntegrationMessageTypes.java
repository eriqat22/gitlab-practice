package com.progressoft.mpay.mpclearinteg;

public class MPClearIntegrationMessageTypes {

	public static final Integer ONLINE_FINANCIAL_REQUEST = 0x1200;
	public static final Integer ONLINE_FINANCIAL_RESPONSE = 0x1210;
	public static final Integer OFFLINE_FINANCIAL_REQUEST = 0x1220;
	public static final Integer OFFLINE_FINANCIAL_RESPONSE = 0x1230;
	public static final Integer REVERSAL_ADVISE_REQUEST = 0x1420;
	public static final Integer REVERSAL_ADVISE_RESPONSE = 0x1430;
	public static final Integer REGISTRATION_REQUEST = 0x1700;
	public static final Integer REGISTRATION_RESPONSE = 0x1710;
	public static final Integer NETWORK_MANAGEMENT_REQUEST = 0x1800;
	public static final Integer NETWORK_MANAGEMENT_RESPONSE = 0x1810;
	public static final Integer GENERIC_RESPONSE = 0x1644;

	private MPClearIntegrationMessageTypes() {

	}
}
