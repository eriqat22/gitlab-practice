package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.mpclear.MPClearClientHelper;

public class PostApproveDeleteCorporateService implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostApproveDeleteCorporateService.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute--------------------------");
        MPAY_CorpoarteService corpService = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN);
        MPAY_Corporate corporate = corpService.getRefCorporate();
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).removeServiceAccount(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), corporate, corpService, corpService.getServiceServiceAccounts().get(0));
    }
}
