package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.registration.ValidateAccountDeletion;
import com.progressoft.mpay.registration.ValidateNoMobilesAreOnModifiedState;

public class ValidateDeleteCustomer implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteCustomer.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside validate =======================");
		MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
		List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId());

		if (mobiles == null || mobiles.isEmpty())
			return;

		ValidateNoMobilesAreOnModifiedState.validate(customer);

		Iterator<MPAY_CustomerMobile> mobilesIterator = mobiles.listIterator();
		do {
			MPAY_CustomerMobile mobile = mobilesIterator.next();
			ValidateAccountDeletion.validate(mobile);
		} while (mobilesIterator.hasNext());
	}
}