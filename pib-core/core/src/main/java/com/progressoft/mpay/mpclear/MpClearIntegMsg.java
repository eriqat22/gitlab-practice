package com.progressoft.mpay.mpclear;

import java.io.Serializable;
import java.sql.Timestamp;

@SuppressWarnings("serial")
public class MpClearIntegMsg implements Serializable {

	Timestamp signingStamp;
	String content;
	String refSender;
	String token;

	public Timestamp getSigningStamp() {
		return signingStamp;
	}

	public void setSigningStamp(Timestamp signingStamp) {
		this.signingStamp = signingStamp;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRefSender() {
		return refSender;
	}

	public void setRefSender(String refSender) {
		this.refSender = refSender;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
