package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class CheckIfLastService implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfLastService.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVar.get(WorkflowService.WF_ARG_BEAN);
		List<MPAY_CorpoarteService> services = select(
				service.getRefCorporate().getRefCorporateCorpoarteServices(),
				having(on(MPAY_CorpoarteService.class).getDeletedFlag(), Matchers.nullValue()).or(having(on(MPAY_CorpoarteService.class).getDeletedFlag(), Matchers.equalTo(false))).and(
						having(on(MPAY_CorpoarteService.class).getId(), Matchers.not(service.getId()))));
		return !services.isEmpty();
	}
}