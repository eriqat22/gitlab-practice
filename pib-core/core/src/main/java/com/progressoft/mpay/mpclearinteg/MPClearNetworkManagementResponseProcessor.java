package com.progressoft.mpay.mpclearinteg;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;

import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.mpclear.plugins.MPClearMessageProcessor;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingResult;

public class MPClearNetworkManagementResponseProcessor extends MPClearMessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MPClearNetworkManagementResponseProcessor.class);

	@Override
	public MPClearProcessingResult processMessage(MPClearProcessingContext context) {
		logger.debug("inside MPClearNetworkManagementResponseProcessor...");

		if (context == null)
			throw new NullArgumentException("context");

		context.getDataProvider().persistMPClearIntegMessageLog(context.getMessage());
		Map<String, Object> args = new HashMap<>();
		args.put(WorkflowService.WF_ARG_BEAN, context.getMessage());

		execute("PreParseMpClearInegMsgLog", args);
		execute("Pre1810ValidationMpClearInegMsgLog", args);

		if (args.get(Result.VALIDATION_RESULT).equals(Result.SUCCESSFUL))
			execute("Post1810ProcessMpClearInegMsgLog", args);
		else
			execute("PreRejectMpClearIntegMsgLog", args);
		return null;
	}

	private void execute(String beanName, Map<String, Object> args) {
		try {
			((FunctionProvider) AppContext.getApplicationContext().getBean(beanName)).execute(args, null, null);
		} catch (BeansException | WorkflowException e) {
			logger.error("Error while Process1810MPClear message", e);
		}
	}
}
