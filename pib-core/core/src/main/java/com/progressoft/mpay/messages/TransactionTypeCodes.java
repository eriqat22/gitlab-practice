package com.progressoft.mpay.messages;

public class TransactionTypeCodes {
	public static final String DIRECT_CREDIT = "1";
	public static final String DIRECT_DEBIT = "2";

	private TransactionTypeCodes() {

	}
}
