package com.progressoft.mpay.payments;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import javax.ws.rs.NotSupportedException;

import org.hamcrest.Matchers;

import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_SystemAccountsCashOut;
import com.progressoft.mpay.entity.MessageCodes;

public class SystemAccountsCashOutHelper {

	private SystemAccountsCashOutHelper() {

	}

	public static void setDestinationData(MPAY_SystemAccountsCashOut entity) {
		if (entity.getSourceService() == null) {
			entity.setDestinationService(new MPAY_CorpoarteService());
			entity.setSourceServiceBalance(null);
			entity.setDestinationServiceBalance(null);
		} else {
			entity.setDestinationService(getDestinationService(entity.getSourceService()));
			entity.setSourceServiceBalance(entity.getSourceService().getServiceServiceAccounts().get(0).getRefAccount().getBalance());
			entity.setDestinationServiceBalance(entity.getDestinationService().getServiceServiceAccounts().get(0).getRefAccount().getBalance());
		}
	}

	private static MPAY_CorpoarteService getDestinationService(MPAY_CorpoarteService source) {
		if (source.getPaymentType().getCode().equals(MessageCodes.CI.toString()) || source.getPaymentType().getCode().equals(MessageCodes.CLEARING.toString())|| source.getPaymentType().getCode().equals(MessageCodes.COMMISSIONS.toString()))
			return selectFirst(source.getRefCorporate().getRefCorporateCorpoarteServices(),
					having(on(MPAY_CorpoarteService.class).getPaymentType().getCode(), Matchers.equalTo(MessageCodes.GENERIC.toString())));
		else if (source.getPaymentType().getCode().equals(MessageCodes.FEES.toString()) || source.getPaymentType().getCode().equals(MessageCodes.INCOME_VAT.toString())
				|| source.getPaymentType().getCode().equals(MessageCodes.EXPENSES_VAT.toString()))
			return selectFirst(source.getRefCorporate().getRefCorporateCorpoarteServices(),
					having(on(MPAY_CorpoarteService.class).getPaymentType().getCode(), Matchers.equalTo(MessageCodes.CO.toString())));
		else
			throw new NotSupportedException("Service with payment type code: " + source.getPaymentType().getCode() + " not supported");

	}
}
