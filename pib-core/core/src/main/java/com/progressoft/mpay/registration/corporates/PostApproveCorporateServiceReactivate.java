package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostApproveCorporateServiceReactivate implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostApproveCorporateServiceReactivate.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN);
		service.setIsActive(true);
		handleServiceAccounts(service);
	}

	private void handleServiceAccounts(MPAY_CorpoarteService service) throws WorkflowException {
		logger.debug("inside HandleServiceAccounts--------------------------");
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId(), ServiceAccountWorkflowStatuses.SUSPENDED.toString());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_ServiceAccount account = accountsIterator.next();
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.REACTIVATE, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
}