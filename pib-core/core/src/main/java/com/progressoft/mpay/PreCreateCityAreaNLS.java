package com.progressoft.mpay;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CityAreas_NLS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class PreCreateCityAreaNLS implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PreCreateCityAreaNLS.class);

    @Override
    public void execute(Map arg0, Map map1, PropertySet propertySet) throws WorkflowException {
        logger.debug("start PreCreateCityAreaNLS.execute ----------------");
        MPAY_CityAreas_NLS cityAreas_nls = (MPAY_CityAreas_NLS) arg0.get(WorkflowService.WF_ARG_BEAN);
        cityAreas_nls.setAreaLanguage(LookupsLoader.getInstance().getLanguageByCode("en"));
    }
}
