package com.progressoft.mpay;

public class Constants {

	// jfwCommonsParamNames
	public static final String CODE = "code";
	public static final String STATUSID_CODE = "statusId.code";
	public static final String STATUSID = "statusId";
	// whereClause properties
	public static final String REF_MESSAGE_ID = "REFMESSAGEID";
	public static final String DEVICE_ID_PROPERTY = "deviceID";
	public static final String MSG_ID = "msgId";
	public static final String ACTIVE_DEVICE = "activeDevice";
	public static final String SENDERMOBILEID = "SENDERMOBILEID";
	// actionName
	public static final String DEELET = "Delete";
	public static final String APPROVE = "Approve";
	public static final String SVC_APPROVE = "SVC_Approve";
	public static final String SVC_DELETE = "SVC_Delete";
	// errors description
	public static final String INTERNAL_SYSTEM_ERROR = "Internal System Error";
	// messagesProperties
	public static final String MSG_DETAILS_SUBITEM_PARAM_NAME = "refReferenceMessageDetails";
	// msgDetailsParamNames
	public static final String DEVICE_ID = "deviceId";
	public static final String MESSAGE_ID = "messageId";
	public static final String ATM_CODE = "atmCode";
	public static final String CLIENT_ID = "clientId";
	public static final String PASSKEY = "password";
	public static final String ACTIVATION_CODE = "activationCode";
	public static final String DEVICE_NAME = "deviceName";
	// serviceTypeOptions
	public static final String SERVICE_MP_CLEAR_ALIAS = "mpclearAlias";
	public static final String SERVICE_ALIAS = "alias";
	public static final String SERVICE_NAME = "name";
	// CustomerMobileParam
	public static final String MOBILE_NUMBER = "mobileNumber";
	public static final String MOBILE_ALIAS = "alias";
	// sttsCodes
	public static final String GENERIC_ACTIVE = "104605";
	public static final String PSP_ACCOUNTS_SERVICE_NAME = "BANKACCOUNT";
	public static final String OTP = "OTP";
	public static final String CARD_NUMBER_KEY = "cardNumber";
	public static final String CARD_PIN_KEY = "cardPIN";
	public static final String CUSTOMERS = "customer";
	private Constants() {
	}
}
