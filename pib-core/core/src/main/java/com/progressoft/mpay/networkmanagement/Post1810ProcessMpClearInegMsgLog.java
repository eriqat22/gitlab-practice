package com.progressoft.mpay.networkmanagement;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgType;
import com.progressoft.mpay.entities.MPAY_MpClearResponseCode;
import com.progressoft.mpay.entities.MPAY_NetworkManIntegMsg;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Post1810ProcessMpClearInegMsgLog implements FunctionProvider {

	@Autowired
	private ItemDao itemDao;
	private static final Logger logger = LoggerFactory.getLogger(Post1810ProcessMpClearInegMsgLog.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("insode Post1810ProcessMpClearInegMsgLog---------------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());
		MPAY_NetworkManIntegMsg originalNetworkMessage;
		MPAY_NetworkManIntegMsg networkMessage = new MPAY_NetworkManIntegMsg();
		networkMessage.setRefMsgLog(msg);
		MPAY_MpClearIntegMsgType msgType = LookupsLoader.getInstance().getMpClearIntegrationMessageTypes(IntegMessageTypes.TYPE_1810);

		networkMessage.setIntegMsgType(msgType);

		networkMessage.setRefMessage(isomsg.getField(115).toString());

		originalNetworkMessage = itemDao.getItem(MPAY_NetworkManIntegMsg.class, "refMessage", isomsg.getField(116)
				.toString());
		MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).updateResponseReceived(isomsg.getField(116).toString());
		networkMessage.setRefOriginalMsg(originalNetworkMessage);
		MPAY_MpClearResponseCode mpClearResponseCode = LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(isomsg.getField(44).toString());
		networkMessage.setResponse(mpClearResponseCode);
		if (isomsg.getField(47) != null)
			networkMessage.setNarration(isomsg.getField(47).toString());

		try {
			itemDao.persist(networkMessage);
			Map<String, Object> v = new HashMap<>();
			v.put(WorkflowService.WF_ARG_BEAN, networkMessage);
			PreValidate1810MpClearIntegMsg oPreValidate1810MpClearIntegMsg = AppContext.getApplicationContext()
					.getBean("PreValidate1810MpClearIntegMsg", PreValidate1810MpClearIntegMsg.class);
			oPreValidate1810MpClearIntegMsg.execute(v, null, null);
			if (v.get(Result.VALIDATION_RESULT).equals(Result.SUCCESSFUL)) {

				PostProcess1810MpClearIntegMsg oPostProcess1810MpClearIntegMsg = AppContext.getApplicationContext()
						.getBean("PostProcess1810MpClearIntegMsg", PostProcess1810MpClearIntegMsg.class);
				oPostProcess1810MpClearIntegMsg.execute(v, null, null);
			}

		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}
}
