package com.progressoft.mpay.entity;

public class MessageTransceiverType {
	public static final String CUSTOMER = "Cust";
	public static final String CORPORATE = "Corp";
	public static final String EXTERNAL = "Ext";

	private MessageTransceiverType() {

	}
}
