package com.progressoft.mpay.registration;

import java.nio.charset.Charset;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class ClientManagement {

	private static final String MSG_LOG_NAME = "msgLog";
	public static final int CITY_CODE_LEN = 16;
	public static final int COUNTR_CODE_LEN = 16;
	public static final int DATE_OF_BIRTH_LEN = 10;
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientManagement.class);
	private static final String TIME_FORMAT = "MMddHHmmss";
	private static final String DATE_FORMAT = "yyyyMMdd";

	private ClientManagement() {

	}

	public static String createAddClientMessage(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile, MPAY_MpClearIntegMsgLog msgLog) {
		LOGGER.debug("Inside createAddClientMessage ===============");
		if (coreComponents == null)
			throw new NullArgumentException("coreComponents");
		if (customer == null)
			throw new NullArgumentException("customer");
		if (mobile == null)
			throw new NullArgumentException("mobile");
		if (msgLog == null)
			throw new NullArgumentException(MSG_LOG_NAME);
		MPAY_Customer actualCustomer = customer;
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String address = buildAddress(actualCustomer);
			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			String name = getCustomerName(actualCustomer);
			String account = formatRegistrationId(coreComponents.getSystemParameters(), mobile);

			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "ADCL", "ADCL".length()));
			message.setField(49, new IsoValue<>(IsoType.ALPHA, coreComponents.getSystemParameters().getDefaultCurrency().getStringISOCode(), 3));
			setEncodedString(message, 62, IsoType.LLLVAR, account);
			message.setField(105, new IsoValue<>(IsoType.NUMERIC, actualCustomer.getCity().getCode(), CITY_CODE_LEN));
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, actualCustomer.getNationality().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			message.setField(107, new IsoValue<>(IsoType.NUMERIC, "00" + formatIsoDate(actualCustomer.getDob(), DATE_FORMAT), DATE_OF_BIRTH_LEN));
			message.setField(109, new IsoValue<>(IsoType.LLVAR, actualCustomer.getClientType().getCode(), actualCustomer.getClientType().getCode().length()));
			setEncodedString(message, 110, IsoType.LLVAR, actualCustomer.getIdNum());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, actualCustomer.getIdType().getCode(), actualCustomer.getIdType().getCode().length()));
			setEncodedString(message, 112, IsoType.LLLVAR, address);
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
			setEncodedString(message, 120, IsoType.LLLVAR, name);
			if (mobile.getAlias() != null)
				setEncodedString(message, 63, IsoType.LLLVAR, mobile.getAlias());
			if (null != actualCustomer.getClientRef())
				setEncodedString(message, 61, IsoType.LLLVAR, actualCustomer.getClientRef());
			if (actualCustomer.getNote() != null)
				setEncodedString(message, 124, IsoType.LLLVAR, actualCustomer.getNote());
			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createAddClientMessage (customer)", e);
		}
	}

	private static void setMessageEncoded(IsoMessage message) {
		LOGGER.debug("Inside setMessageEncoded ==============");
		message.setField(113, new IsoValue<>(IsoType.LLLVAR, "1", 1));
	}

	private static void setEncodedString(IsoMessage message, int fieldNumber, IsoType type, String value) {
		LOGGER.debug("Inside setEncodedString ==============");
		byte[] bytes = value.getBytes(Charset.forName("UTF-16LE"));
		String base64String = Base64.encodeBase64String(bytes);
		message.setField(fieldNumber, new IsoValue<>(type, base64String, base64String.length()));
	}

	private static String getCustomerName(MPAY_Customer customer) {
		LOGGER.debug("Inside getCustomerName ==============");
		String value = customer.getFirstName();
		if (customer.getMiddleName() != null)
			value = value + " " + customer.getMiddleName();
		value = value + " " + customer.getLastName();
		return value;
	}

	public static String createAddClientMessage(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService service, MPAY_MpClearIntegMsgLog msgLog) {
		LOGGER.debug("Inside createAddClientMessage ==============");
		if (coreComponents == null)
			throw new NullArgumentException("coreComponents");
		if (corporate == null)
			throw new NullArgumentException("corporate");
		if (service == null)
			throw new NullArgumentException("service");
		if (msgLog == null)
			throw new NullArgumentException(MSG_LOG_NAME);
		MPAY_Corporate actualCorporate = corporate;

		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String address = buildAddress(actualCorporate);
			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			String account = formatCorporateAccount(coreComponents, service);
			String corpRegIdType = corporate.getIdType().getCode();

			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "ADCL", "ADCL".length()));
			message.setField(49, new IsoValue<>(IsoType.ALPHA, coreComponents.getSystemParameters().getDefaultCurrency().getStringISOCode(), 3));
			setEncodedString(message, 62, IsoType.LLLVAR, account);
			message.setField(105, new IsoValue<>(IsoType.NUMERIC, actualCorporate.getCity().getCode(), CITY_CODE_LEN));
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, actualCorporate.getRefCountry().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			message.setField(107, new IsoValue<>(IsoType.NUMERIC, "00" + formatIsoDate(actualCorporate.getRegistrationDate(), DATE_FORMAT), DATE_OF_BIRTH_LEN));
			message.setField(109, new IsoValue<>(IsoType.LLVAR, actualCorporate.getClientType().getCode(), actualCorporate.getClientType().getCode().length()));
			setEncodedString(message, 110, IsoType.LLVAR, actualCorporate.getRegistrationId());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, corpRegIdType, corpRegIdType.length()));
			setEncodedString(message, 112, IsoType.LLLVAR, address);
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
			setEncodedString(message, 120, IsoType.LLLVAR, actualCorporate.getName());
			if (service.getAlias() != null)
				setEncodedString(message, 63, IsoType.LLLVAR, service.getAlias());
			if (null != actualCorporate.getClientRef())
				setEncodedString(message, 61, IsoType.LLLVAR, actualCorporate.getClientRef());
			if (actualCorporate.getNote() != null)
				setEncodedString(message, 124, IsoType.LLLVAR, actualCorporate.getNote());
			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createAddClientMessage (corporate)", e);
		}
	}

	public static String createUpdateClientMessage(CoreComponents components, MPAY_Corporate corporate, MPAY_CorpoarteService service, MPAY_ServiceAccount account, MPAY_MpClearIntegMsgLog msgLog) {
		LOGGER.debug("Inside createUpdateClientMessage ==============");
		if (components == null)
			throw new NullArgumentException("components");
		if (corporate == null)
			throw new NullArgumentException("corporate");
		if (service == null)
			throw new NullArgumentException("service");
		if (account == null)
			throw new NullArgumentException("account");
		if (msgLog == null)
			throw new NullArgumentException(MSG_LOG_NAME);
		IsoMessage message = ISO8583Parser.getInstance(components.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);
		String address = buildAddress(corporate);
		String messageID = components.getDataProvider().getNextMPClearMessageId();
		String oldAccount = formatRegistrationId(components.getSystemParameters(), account, service.getName());
		String newAccount;
		if (service.getNewName() == null)
			newAccount = oldAccount;
		else
			newAccount = formatRegistrationId(components.getSystemParameters(), account, service.getNewName());

		String corpRegIdType = corporate.getIdType().getCode();

		message.setBinary(false);
		setMessageEncoded(message);
		message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
		message.setField(48, new IsoValue<>(IsoType.LLLVAR, "UDCL", "UDCL".length()));
		message.setField(49, new IsoValue<>(IsoType.ALPHA, components.getSystemParameters().getDefaultCurrency().getStringISOCode(), 3));
		setEncodedString(message, 60, IsoType.LLLVAR, oldAccount);
		setEncodedString(message, 62, IsoType.LLLVAR, newAccount);
		message.setField(105, new IsoValue<>(IsoType.NUMERIC, corporate.getCity().getCode(), CITY_CODE_LEN));
		message.setField(106, new IsoValue<>(IsoType.NUMERIC, corporate.getRefCountry().getNumericISOCode().toString(), COUNTR_CODE_LEN));
		message.setField(107, new IsoValue<>(IsoType.NUMERIC, "00" + formatIsoDate(corporate.getRegistrationDate(), DATE_FORMAT), DATE_OF_BIRTH_LEN));
		message.setField(109, new IsoValue<>(IsoType.LLVAR, corporate.getClientType().getCode(), corporate.getClientType().getCode().length()));
		setEncodedString(message, 110, IsoType.LLVAR, corporate.getRegistrationId());
		message.setField(111, new IsoValue<>(IsoType.LLLVAR, corpRegIdType, corpRegIdType.length()));
		setEncodedString(message, 112, IsoType.LLLVAR, address);
		message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
		setEncodedString(message, 120, IsoType.LLLVAR, corporate.getName());
		if (null != corporate.getClientRef())
			setEncodedString(message, 61, IsoType.LLLVAR, corporate.getClientRef());
		msgLog.setMessageId(messageID);
		return new String(message.writeData());
	}

	public static String createUpdateClientMessage(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile, MPAY_MobileAccount account, MPAY_MpClearIntegMsgLog msgLog) {
		LOGGER.debug("Inside createUpdateClientMessage ==============");
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String address = buildAddress(customer);
			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			String name = getCustomerName(customer);
			String oldAccount = formatRegistrationId(coreComponents.getSystemParameters(), account, mobile.getMobileNumber());
			String newAccount;
			if (mobile.getNewMobileNumber() == null)
				newAccount = oldAccount;
			else
				newAccount = formatRegistrationId(coreComponents.getSystemParameters(), account, mobile.getNewMobileNumber());

			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "UDCL"));
			setEncodedString(message, 60, IsoType.LLLVAR, oldAccount);
			setEncodedString(message, 62, IsoType.LLLVAR, newAccount);
			message.setField(105, new IsoValue<>(IsoType.NUMERIC, customer.getCity().getCode(), CITY_CODE_LEN));
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, customer.getNationality().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			message.setField(107, new IsoValue<>(IsoType.NUMERIC, "00" + formatIsoDate(customer.getDob(), DATE_FORMAT), DATE_OF_BIRTH_LEN));
			message.setField(109, new IsoValue<>(IsoType.LLVAR, customer.getClientType().getCode(), customer.getClientType().getCode().length()));
			setEncodedString(message, 110, IsoType.LLVAR, customer.getIdNum());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, customer.getIdType().getCode(), customer.getIdType().getCode().length()));
			setEncodedString(message, 112, IsoType.LLLVAR, address);
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
			setEncodedString(message, 120, IsoType.LLLVAR, name);
			if (null != customer.getClientRef())
				setEncodedString(message, 61, IsoType.LLLVAR, customer.getClientRef());
			if (customer.getNote() != null)
				setEncodedString(message, 124, IsoType.LLLVAR, customer.getNote());

			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createUpdateClientMessage (customer)", e);
		}
	}

	public static String createAddAccountMessage(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile, MPAY_MobileAccount account, MPAY_MpClearIntegMsgLog msgLog) {
		LOGGER.debug("Inside createAddAccountMessage, customer ==============");
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			String registrationId = formatRegistrationId(coreComponents.getSystemParameters(), account, mobile.getMobileNumber());

			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "ADAC"));
			message.setField(49, new IsoValue<>(IsoType.ALPHA, coreComponents.getSystemParameters().getDefaultCurrency().getStringISOCode(), 3));
			setEncodedString(message, 62, IsoType.LLLVAR, registrationId);
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, customer.getNationality().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			setEncodedString(message, 110, IsoType.LLVAR, customer.getIdNum());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, customer.getIdType().getCode(), customer.getIdType().getCode().length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createAddAccountMessage (customer)", e);
		}
	}

	public static String createAddAccountMessage(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService service, MPAY_ServiceAccount account, MPAY_MpClearIntegMsgLog msgLog) {
		LOGGER.debug("Inside createAddAccountMessage ==============");
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			String registrationId = formatCorporateAccount(coreComponents.getSystemParameters(), account, service.getName());
			String corpRegIdType = corporate.getIdType().getCode();

			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "ADAC"));
			message.setField(49, new IsoValue<>(IsoType.ALPHA, coreComponents.getSystemParameters().getDefaultCurrency().getStringISOCode(), 3));
			setEncodedString(message, 62, IsoType.LLLVAR, registrationId);
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, corporate.getRefCountry().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			setEncodedString(message, 110, IsoType.LLVAR, corporate.getRegistrationId());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, corpRegIdType, corpRegIdType.length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
			msgLog.setMessageId(messageID);

			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createAddAccountMessage (corporate with account)", e);
		}
	}

	public static String createRemoveAccountMessage(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile, MPAY_MobileAccount account, MPAY_MpClearIntegMsgLog msgLog) {
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "RMAC"));
			setEncodedString(message, 62, IsoType.LLLVAR, formatRegistrationId(coreComponents.getSystemParameters(), account, mobile.getMobileNumber()));
			setEncodedString(message, 110, IsoType.LLVAR, customer.getIdNum());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, customer.getIdType().getCode(), customer.getIdType().getCode().length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createRemoveAccountMessage (customer)", e);
		}
	}

	public static String createSetDefaultAccountMessage(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile, MPAY_MobileAccount account, MPAY_MpClearIntegMsgLog msgLog) {
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			String registrationId = formatRegistrationId(coreComponents.getSystemParameters(), account, mobile.getMobileNumber());

			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "DFLT"));
			setEncodedString(message, 62, IsoType.LLLVAR, registrationId);
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, customer.getNationality().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			setEncodedString(message, 110, IsoType.LLVAR, customer.getIdNum());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, customer.getIdType().getCode(), customer.getIdType().getCode().length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createSetDefaultAccountMessage (customer)", e);
		}
	}

	public static String createSetDefaultAccountMessage(CoreComponents components, MPAY_Corporate corporate, MPAY_CorpoarteService service, MPAY_ServiceAccount defaultAccount, MPAY_MpClearIntegMsgLog msgLog) {
		try {
			IsoMessage message = ISO8583Parser.getInstance(components.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);
			String messageId = components.getDataProvider().getNextMPClearMessageId();
			String corporateRegistrationIdType = corporate.getIdType().getCode();
			message.setIsoHeader("");
			setMessageEncoded(message);
			message.setBinary(false);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "DFLT"));
			setEncodedString(message, 62, IsoType.LLLVAR, formatCorporateAccount(components.getSystemParameters(), defaultAccount, service.getName()));
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, corporate.getRefCountry().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			setEncodedString(message, 110, IsoType.LLVAR, corporate.getRegistrationId());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, corporateRegistrationIdType, corporateRegistrationIdType.length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageId, messageId.length()));
			msgLog.setMessageId(messageId);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createSetDefaultAccountMessage (corporate)", e);
		}
	}

	public static String createRemoveAccountMessage(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService corpService, MPAY_ServiceAccount account, MPAY_MpClearIntegMsgLog msgLog) {
		try {
			IsoMessage isoMessage = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String messageId = coreComponents.getDataProvider().getNextMPClearMessageId();
			String idType = corporate.getIdType().getCode();
			setMessageEncoded(isoMessage);
			isoMessage.setIsoHeader("");
			isoMessage.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			isoMessage.setField(48, new IsoValue<>(IsoType.LLLVAR, "RMAC"));
			setEncodedString(isoMessage, 62, IsoType.LLLVAR, formatCorporateAccount(coreComponents.getSystemParameters(), account, corpService.getName()));
			isoMessage.setField(110, new IsoValue<>(IsoType.LLVAR, corporate.getRegistrationId(), corporate.getRegistrationId().length()));
			isoMessage.setField(111, new IsoValue<>(IsoType.LLLVAR, idType.toString(), idType.toString().length()));
			isoMessage.setField(115, new IsoValue<>(IsoType.LLLVAR, messageId, messageId.length()));
			msgLog.setMessageId(messageId);
			return new String(isoMessage.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createRemoveAccountMessage (corporate)", e);
		}
	}

	public static String createRemoveClientMessage(CoreComponents coreComponents, MPAY_Customer customer, MPAY_MpClearIntegMsgLog msgLog) {
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);
			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();

			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "RMCL"));
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, customer.getNationality().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			setEncodedString(message, 110, IsoType.LLVAR, customer.getIdNum());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, customer.getIdType().getCode(), customer.getIdType().getCode().length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));

			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createRemoveClientMessage (customer)", e);
		}
	}

	public static String createRemoveClientMessage(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_MpClearIntegMsgLog msgLog) {
		try {
			String corpIdType = corporate.getIdType().getCode();
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);
			String messageId = coreComponents.getDataProvider().getNextMPClearMessageId();

			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "RMCL"));
			message.setField(106, new IsoValue<>(IsoType.NUMERIC, corporate.getRefCountry().getNumericISOCode().toString(), COUNTR_CODE_LEN));
			setEncodedString(message, 110, IsoType.LLVAR, corporate.getRegistrationId());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, corpIdType.toString(), corpIdType.toString().length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageId, messageId.length()));

			msgLog.setMessageId(messageId);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createRemoveClientMessage (corporate)", e);
		}
	}

	public static String createChangeAliasMessage(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile, MPAY_MpClearIntegMsgLog msgLog, String alias) {
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			String account = formatRegistrationId(coreComponents.getSystemParameters(), mobile);

			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "CHAL"));
			setEncodedString(message, 62, IsoType.LLLVAR, account);
			setEncodedString(message, 110, IsoType.LLVAR, customer.getIdNum());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, customer.getIdType().getCode(), customer.getIdType().getCode().length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));
			if (alias != null)
				setEncodedString(message, 63, IsoType.LLLVAR, alias);
			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createChangeAliasMessage (customer, mobile, messageLog)", e);
		}
	}

	public static String createChangeAliasMessage(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService service, MPAY_MpClearIntegMsgLog msgLog) {
		MPAY_CorpoarteService actualService = service;
		try {
			IsoMessage message = ISO8583Parser.getInstance(coreComponents.getSystemParameters().getISO8583ConfigFilePath()).newMessage(0x1700);

			String messageID = coreComponents.getDataProvider().getNextMPClearMessageId();
			String account = formatCorporateAccount(coreComponents, actualService);
			String corpRegIdType = corporate.getIdType().getCode();

			message.setIsoHeader("");
			message.setBinary(false);
			setMessageEncoded(message);
			message.setField(7, new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime(), TIME_FORMAT)));
			message.setField(48, new IsoValue<>(IsoType.LLLVAR, "CHAL"));
			setEncodedString(message, 62, IsoType.LLLVAR, account);
			setEncodedString(message, 110, IsoType.LLVAR, corporate.getRegistrationId());
			message.setField(111, new IsoValue<>(IsoType.LLLVAR, corpRegIdType, corpRegIdType.length()));
			message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));

			if (actualService.getNewAlias() != null)
				setEncodedString(message, 63, IsoType.LLLVAR, actualService.getNewAlias());
			if (null != corporate.getClientRef())
				setEncodedString(message, 61, IsoType.LLLVAR, corporate.getClientRef());
			msgLog.setMessageId(messageID);
			return new String(message.writeData());
		} catch (Exception e) {
			throw new InvalidOperationException("Error while createChangeAliasMessage (corporate, service, messageLog)", e);
		}
	}

	public static String buildAddress(MPAY_Customer customer) {
		StringBuilder builder = new StringBuilder();

		if (!SystemHelper.isNullOrEmpty(customer.getBuildingNum()))
			builder.append("BuildingNumber:").append(customer.getBuildingNum()).append("|");
		if (!SystemHelper.isNullOrEmpty(customer.getPhoneOne()))
			builder.append("Phone1:").append(customer.getPhoneOne()).append("|");
		if (!SystemHelper.isNullOrEmpty(customer.getPhoneTwo()))
			builder.append("Phone2:").append(customer.getPhoneTwo()).append("|");
		if (!SystemHelper.isNullOrEmpty(customer.getPobox()))
			builder.append("POBox:").append(customer.getPobox()).append("|");
		if (!SystemHelper.isNullOrEmpty(customer.getZipCode()))
			builder.append("ZipCode:").append(customer.getZipCode()).append("|");
		if (!SystemHelper.isNullOrEmpty(customer.getStreetName()))
			builder.append("StreetName:").append(customer.getStreetName()).append("|");
		if (!SystemHelper.isNullOrEmpty(customer.getEmail()))
			builder.append("Email:").append(customer.getEmail());

		if (builder.length() > 0 && builder.charAt(builder.length() - 1) == '|')
			return builder.substring(0, builder.length() - 1);
		else
			return builder.toString();
	}

	public static String buildAddress(MPAY_Corporate corporate) {
		StringBuilder builder = new StringBuilder();

		if (!SystemHelper.isNullOrEmpty(corporate.getBuildingNum()))
			builder.append("BuildingNumber:").append(corporate.getBuildingNum()).append("|");
		if (!SystemHelper.isNullOrEmpty(corporate.getPhoneOne()))
			builder.append("Phone1:").append(corporate.getPhoneOne()).append("|");
		if (!SystemHelper.isNullOrEmpty(corporate.getPhoneTwo()))
			builder.append("Phone2:").append(corporate.getPhoneTwo()).append("|");
		if (!SystemHelper.isNullOrEmpty(corporate.getPobox()))
			builder.append("POBox:").append(corporate.getPobox()).append("|");
		if (!SystemHelper.isNullOrEmpty(corporate.getZipCode()))
			builder.append("ZipCode:").append(corporate.getZipCode()).append("|");
		if (!SystemHelper.isNullOrEmpty(corporate.getStreetName()))
			builder.append("StreetName:").append(corporate.getStreetName()).append("|");
		if (!SystemHelper.isNullOrEmpty(corporate.getEmail()))
			builder.append("Email:").append(corporate.getEmail());

		if (builder.length() > 0 && builder.charAt(builder.length() - 1) == '|')
			return builder.substring(0, builder.length() - 1);
		else
			return builder.toString();
	}

	private static String formatIsoDate(Date date, String format) {
		return SystemHelper.formatDate(date, format);
	}

	public static String formatRegistrationId(ISystemParameters systemParameters, MPAY_CustomerMobile mobile) {
		String mobileNumber = mobile.getMobileNumber();
		MPAY_MobileAccount account = mobile.getMobileMobileAccounts().get(0);
		String formatMas = "%0" + systemParameters.getMobileAccountSelectorLength() + "d";
		return account.getBank().getCode() + String.format(formatMas, account.getMas()) + "M" + mobileNumber;
	}

	public static String formatRegistrationId(ISystemParameters systemParameters, MPAY_MobileAccount account, String mobileNumber) {
		String bankCode = account.getBank().getCode();
		String formatMas = "%0" + systemParameters.getMobileAccountSelectorLength() + "d";
		return bankCode + String.format(formatMas, account.getMas()) + "M" + mobileNumber;
	}

	public static String formatRegistrationId(ISystemParameters systemParameters, MPAY_ServiceAccount account, String name) {
		String bankCode = account.getBank().getCode();
		String formatMas = "%0" + systemParameters.getMobileAccountSelectorLength() + "d";
		return bankCode + String.format(formatMas, account.getMas()) + "C" + name;
	}

	public static String formatCustomerAccount(String mobileNumber, String bankCode, long mas) {
		String formatMas = "%0" + SystemParameters.getInstance().getMobileAccountSelectorLength() + "d";
		return bankCode + String.format(formatMas, mas) + "M" + mobileNumber;
	}

	public static String formatCorporateAccount(CoreComponents coreComponents, MPAY_CorpoarteService service) {
		String serviceName = service.getName();
		MPAY_ServiceAccount account = coreComponents.getDataProvider().listServiceAccounts(service.getId()).get(0);
		String formatMas = "%0" + coreComponents.getSystemParameters().getMobileAccountSelectorLength() + "d";
		return account.getBank().getCode() + String.format(formatMas, account.getMas()) + "C" + serviceName;

	}

	public static String formatCorporateAccount(ISystemParameters systemParameters, MPAY_ServiceAccount account, String serviceName) {
		String formatMas = "%0" + systemParameters.getMobileAccountSelectorLength() + "d";
		return account.getBank().getCode() + String.format(formatMas, account.getMas()) + "C" + serviceName;

	}

	public static String getRegistrationId(MPAY_ServiceAccount account) {
		String formatMas = "%0" + SystemParameters.getInstance().getMobileAccountSelectorLength() + "d";
		return account.getBank().getCode() + String.format(formatMas, account.getMas());
	}

	public static String getRegistrationId(MPAY_MobileAccount account) {
		String formatMas = "%0" + SystemParameters.getInstance().getMobileAccountSelectorLength() + "d";
		return account.getBank().getCode() + String.format(formatMas, account.getMas());
	}

	public static MPAY_MpClearIntegMsgLog cloneMessageLogForSuspectDuplicate(CoreComponents components, MPAY_MpClearIntegMsgLog message) {
		MPAY_MpClearIntegMsgLog msgLog = new MPAY_MpClearIntegMsgLog();
		IsoMessage isoMessage = ISO8583Parser.getInstance(components.getSystemParameters().getISO8583ConfigFilePath()).parseMessage(message.getContent());
		isoMessage.setField(114, new IsoValue<>(IsoType.LLLVAR, "1", 1));
		msgLog.setContent(new String(isoMessage.writeData()));
		msgLog.setHint(message.getHint());
		msgLog.setMessageId(message.getMessageId());
		msgLog.setIsCanceled(false);
		msgLog.setIsProcessed(true);
		msgLog.setResponseReceived(true);
		return msgLog;
	}
}
