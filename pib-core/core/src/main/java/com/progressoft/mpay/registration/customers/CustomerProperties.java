package com.progressoft.mpay.registration.customers;

public class CustomerProperties {
	public static final String MOBILE_NUMBER = "mobileNumber";
	public static final String NOTIFICATION_SHOW_TYPE = "notificationShowType";
	public static final String NFC_SERIAL = "nfcSerial";
	public static final String NATIONALITY = "nationality";
	public static final String ID_TYPE = "idType";
	public static final String CLIENT_TYPE = "clientType";
	public static final String ID_NUM = "idNum";
	public static final String REF_CUSTOMER = "refCustomer";
	public static final String MOBILE = "mobile";
	public static final String BANK_BRANCH = "bankBranch";
	public static final String REFERRAL_KEY = "referralkey";

	public static final String FIRST_TEXT_FIELD = "firstTextField";
	public static final String SECOND_TEXT_FIELD = "secondTextField";
	public static final String THIRD_TEXT_FIELD = "thirdTextField";
	public static final String FOURTH_TEXT_FIELD = "forthTextField";
	public static final String FIFTH_TEXT_FIELD = "fifthTextField";

	private CustomerProperties() {

	}
}
