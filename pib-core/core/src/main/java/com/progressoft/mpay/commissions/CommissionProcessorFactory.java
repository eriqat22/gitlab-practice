package com.progressoft.mpay.commissions;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommissionProcessorFactory {
	private static final Logger logger = LoggerFactory.getLogger(CommissionProcessorFactory.class);
	private static CommissionProcessorFactory instance;

	private Map<String, ICommissionProcessor> processors;

	private CommissionProcessorFactory() {
		processors = new HashMap<>();
	}

	public static CommissionProcessorFactory getInstance() {
		if (instance == null)
			instance = new CommissionProcessorFactory();
		return instance;
	}

	public ICommissionProcessor getProcessor(String processor) {
		try {
			if (!processors.containsKey(processor)) {
				Class<?> cls = Class.forName(processor);
				processors.put(processor, (ICommissionProcessor) cls.newInstance());
			}
			return processors.get(processor);
		} catch (Exception e) {
			logger.error("Error in GetProcessor", e);
			return null;
		}
	}
}
