package com.progressoft.mpay.networkmanagement;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.Result;

public class PreValidate1810MpClearIntegMsg implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PreValidate1810MpClearIntegMsg.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Map transientVars, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside PreValidate1810MpClearIntegMsg ====");
		transientVars.put(Result.VALIDATION_RESULT, Result.SUCCESSFUL);
	}
}
