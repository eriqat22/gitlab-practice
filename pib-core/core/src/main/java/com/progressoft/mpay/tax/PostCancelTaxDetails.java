package com.progressoft.mpay.tax;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_TaxSchemeDetail;
import com.progressoft.mpay.entities.MPAY_TaxSlice;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

@Component
public class PostCancelTaxDetails implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCancelTaxDetails.class);
    private static final String TAX_SLICES_DELETED_STATUS = "312003";

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PostApproveTaxDetails ----------------");
        MPAY_TaxSchemeDetail schemeDetails = (MPAY_TaxSchemeDetail) arg0.get(WorkflowService.WF_ARG_BEAN);
        if(schemeDetails.getRefTaxDetailsTaxSlices() == null)
        	return;
        for (MPAY_TaxSlice slice : schemeDetails.getRefTaxDetailsTaxSlices()) {
            if (!TAX_SLICES_DELETED_STATUS.equals(slice.getStatusId().getCode())) {
                logger.debug("Approving slice with TX amount" + slice.getTxAmountLimit() + ". . . . .");
                Map<Option, Object> options = new EnumMap<>(Option.class);
                options.put(Option.ACTION_NAME, "SVC_Cancel");
                JfwHelper.executeAction(MPAYView.TAX_SLICES, slice, options);
            }
        }
    }

}
