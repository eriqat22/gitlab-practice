package com.progressoft.mpay.registration.customers;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.AccountMetaData;

import flexjson.JSONDeserializer;

public class MobileAccountMetaData extends AccountMetaData {

	public MobileAccountMetaData() {
		// Default
	}

	public MobileAccountMetaData(MPAY_MobileAccount account) {
		if (account == null)
			throw new NullArgumentException("account");

		this.externalAcc = account.getExternalAcc();
		this.profileId = account.getRefProfile().getId();
	}

	public void fillMobileAccountFromMetaData(MPAY_MobileAccount account) {
		if (account == null)
			throw new NullArgumentException("account");

		account.setExternalAcc(this.externalAcc);
	}

	public static MobileAccountMetaData fromJson(String data) {
		JSONDeserializer<MobileAccountMetaData> deserializer = new JSONDeserializer<>();
		return deserializer.deserialize(data, MobileAccountMetaData.class);
	}
}