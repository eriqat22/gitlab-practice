package com.progressoft.mpay.entity;

public class NotificationShowTypeNames {
	public static final String MOBILE_NUMBER = "Mobile Number";
	public static final String ALIAS = "Alias";

	private NotificationShowTypeNames() {

	}
}
