package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static ch.lambdaj.Lambda.*;

public class PostApproveCustomerMobile implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostApproveCustomerMobile.class);
    @Autowired
    ItemDao itemDao;

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside execute ============");
        MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) arg0.get(WorkflowService.WF_ARG_BEAN);
        checkDuplicateAlias(mobile);
        if (!mobile.getIsRegistered()) {
            List<MPAY_MobileAccount> accounts = getPendingAccount(mobile);
            if (accounts == null)
                throw new WorkflowException("Mobile doesn't have pending mobile accounts");
            for (MPAY_MobileAccount account : accounts)
                JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.APPROVE, new WorkflowException());
        } else {
            MPAY_MobileAccount account = null;
            List<MPAY_MobileAccount> accounts = getPendingAccount(mobile);
            if (accounts == null)
                account = getFirstRegisteredAccount(mobile);
            if (account == null)
                throw new WorkflowException("Mobile doesn't have an approved account");
            handleUpdateMobile(mobile.getRefCustomer(), mobile, account);
        }
    }

    private void checkDuplicateAlias(MPAY_CustomerMobile mobile) throws InvalidInputException {
        Long itemCount = itemDao.getItemCount(MPAY_CustomerMobile.class, null, "alias='" + mobile.getNewAlias() + "' and id!=" + mobile.getId());
        if (itemCount != 0)
            throw new InvalidInputException("Alias already used, please choose another alias");
    }

    private String getOperator(String customerOperatorValue) {
        try {
            JSONObject json = new JSONObject(customerOperatorValue);
            if (json.isNull("key"))
                return customerOperatorValue;
            return json.getString("key");
        } catch (Exception e) {
            return customerOperatorValue;
        }
    }

    private List<MPAY_MobileAccount> getPendingAccount(MPAY_CustomerMobile mobile) throws WorkflowException {
        logger.debug("inside getPendingAccount ============");
        List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId(),
                MobileAccountWorkflowStatuses.APPROVAL.toString());
        if (accounts == null || accounts.isEmpty())
            return null;
//		if (accounts.size() > 1)
//			throw new WorkflowException("Mobile has more than one pending mobile account");
        return accounts;
    }

    private MPAY_MobileAccount getFirstRegisteredAccount(MPAY_CustomerMobile mobile) throws WorkflowException {
        logger.debug("inside GetFirstRegisteredAccount ============");
        List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
        if (accounts == null || accounts.isEmpty())
            throw new WorkflowException("Mobile should have a pending mobile account");
        return selectFirst(accounts, having(on(MPAY_MobileAccount.class).getIsRegistered(), Matchers.equalTo(true)));
    }

    private void handleUpdateMobile(MPAY_Customer customer, MPAY_CustomerMobile mobile, MPAY_MobileAccount account)
            throws WorkflowException {
        logger.debug("inside HandleUpdateMobile ============");
        if (!StringUtils.equals(mobile.getMobileNumber(), mobile.getNewMobileNumber())
                || customer.getApprovedData() != null)
            sendMPClearIntegrationMessage(customer, mobile, account);
        else
            JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile,
                    CustomerMobileWorkflowServiceActions.APPROVE, new WorkflowException());
        if (account.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.APPROVAL.toString()))
            JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.APPROVE,
                    new WorkflowException());
    }

    private void sendMPClearIntegrationMessage(MPAY_Customer customer, MPAY_CustomerMobile mobile,
                                               MPAY_MobileAccount account) throws WorkflowException {
        logger.debug("inside sendMPClearIntegrationMessage ============");
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor())
                .updateCustomer(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(),
                        LookupsLoader.getInstance()), customer, mobile, account);

    }
}
