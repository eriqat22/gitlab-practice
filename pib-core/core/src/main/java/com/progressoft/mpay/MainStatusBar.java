package com.progressoft.mpay;

import java.util.Date;

import com.progressoft.jfw.shared.Status;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entity.StatusCodes;

public class MainStatusBar {

	public Status getLoggedInStatus() {
		Status status = new Status();
		if (SystemNetworkStatus.isSystemOnline(DataProvider.instance())) {
			status.setIconStyle("icon-ok");
			status.setText("<b style='color:green'>Logged in</b>");
		} else {
			status.setIconStyle("icon-warn");
			status.setText("<b style='color:green'>Logged out</b>");
		}
		return status;
	}

	public Status getLoggedInExpiryDate() {
		MPAY_NetworkManagement oNetMsg = DataProvider.instance().getLastNetworkManagement();
		Status status = new Status();
		if (oNetMsg != null && StatusCodes.LOGGED_IN.compare(oNetMsg.getStatusId()) && oNetMsg.getLoginExpiration() != null) {
			Date date = new Date(oNetMsg.getLoginExpiration().getTime());
			status.setIconStyle("icon-ok");
			status.setText("Expiry Date <b style='color:green'>" + SystemHelper.formatDate(date, "yyyy-MM-dd HH:mm:ss") + "</b>");
		} else {
			status.setIconStyle("icon-warn");
			status.setText("Expiry Date <b style='color:green'>------</b>");
		}
		return status;
	}
}
