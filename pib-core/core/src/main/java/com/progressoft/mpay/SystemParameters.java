package com.progressoft.mpay;

import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_SysConfig;

import java.math.BigDecimal;
import java.util.Objects;

public class SystemParameters implements ISystemParameters {

    private static ISystemParameters instance;

    protected SystemParameters() {

    }

    public static ISystemParameters getInstance() {
        if (instance == null)
            instance = (ISystemParameters) AppContext.getApplicationContext().getBean("SystemParameters");

        return instance;
    }

    @Override
    public JFWCurrency getDefaultCurrency() {
        return LookupsLoader.getInstance().getDefaultCurrency();
    }

    @Override
    public int getPinCodeLength() {
        return Integer.parseInt(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PIN_CODE_LENGTH).getConfigValue());
    }

    @Override
    public int getMnoIntegrationTimeout() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.MNO_INTEGRATION_TIMEOUT_SECONDS).getConfigValue());
    }

    @Override
    public String getSigningDateFormat() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SIGNING_DATE_FORMAT).getConfigValue();
    }

    @Override
    public String getPersonClienttype() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PERSON_CLIENT_TYPE).getConfigValue();
    }

    @Override
    public String getDefaultEncoding() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.DEFAULT_ENCODING).getConfigValue();
    }

    @Override
    public String getMPClearCertificateAlias() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PS_MP_CLEAR_CERTIFICATE_ALIAS)
                .getConfigValue();
    }

    @Override
    public String getEPayCertificateAlias() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.EPAY_CERTIFICATE_ALIAS)
                .getConfigValue();
    }

    @Override
    public String getCertificatePSP() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PSP_CERTIFICATE_ALIAS)
                .getConfigValue();
    }

    @Override
    public String getEfawatercomCertificateAlias() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.E_FAWATERCOM_CERTIFICATE_ALIAS)
                .getConfigValue();
    }

    @Override
    public String getKeyStorePath() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.KEY_STORE_PATH).getConfigValue();
    }

    @Override
    public String getKeyStorePass() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.KEY_STORE_PASS).getConfigValue();
    }

    @Override
    public int getRoutingCodeLength() {
        return Integer.parseInt(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ROUTING_CODE_LENGTH)
                .getConfigValue());
    }

    @Override
    public int getTxCount() {
        return Integer.parseInt(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MINI_TX_COUNT).getConfigValue());
    }

    @Override
    public int getMobileAccountSelectorLength() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.MOBILE_ACCOUNT_SELECTOR_LENGTH).getConfigValue());
    }

    @Override
    public int getMpClearGracePeriod() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.PS_MP_CLEAR_COMMUNICATION_GRACE_PERIOD).getConfigValue());
    }

    @Override
    public String getAmountFormat() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AMOUNT_FORMAT).getConfigValue();
    }

    @Override
    public String getAccountRegex() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ACCOUNT_REGEX).getConfigValue();
    }

    @Override
    public String getChargesAmountFormat() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CHARGES_AMOUNT_FORMAT)
                .getConfigValue();
    }

    @Override
    public boolean getSendActivationCodeUponCustomerMobileRegistration() {
        return "1".equals(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SEND_ACTIVATION_CODE)
                .getConfigValue());
    }

    @Override
    public String getDefaultProfile() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.DEFAULT_PROFILE).getConfigValue();
    }

    @Override
    public int getCorporateRegistrationIdType() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.CORPORATE_REGISTRATION_ID_TYPE).getConfigValue());
    }

    @Override
    public int getMpClearLoginValidtyHours() {
        return Integer.parseInt(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.LOGIN_VALIDITY_HOURS)
                .getConfigValue());
    }

    @Override
    public int getATMCodeValidityMinutes() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.ATM_CODE_VALIDITY_MINUTES).getConfigValue());
    }

    @Override
    public int getATMCodeLength() {
        return Integer.parseInt(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ATM_CODE_LENGTH).getConfigValue());
    }

    @Override
    public int getActivationCodeLength() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.ACTIVATION_CODE_LENGTH).getConfigValue());
    }

    @Override
    public boolean getForceFailed1644() {
        return Integer.parseInt(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.FORCE_FAILED_1644)
                .getConfigValue()) == 1;
    }

    @Override
    public boolean getForceFailed56() {
        return Integer.parseInt(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.FORCE_FAILED_56)
                .getConfigValue()) == 1;
    }

    @Override
    public String getCountryCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.COUNTRY_CODE).getConfigValue();
    }

    @Override
    public int getMPClearResendWaitingTime() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.MPCLEAR_INTEG_RESEND_WAITING_TIME_SECONDS).getConfigValue());
    }

    @Override
    public MPAY_Language getSystemLanguage() {
        String languageCode = LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SYSTEM_LANGUAGE)
                .getConfigValue();
        return LookupsLoader.getInstance().getLanguageByCode(languageCode);
    }

    @Override
    public boolean getOfflineModeEnabled() {
        return Integer.parseInt(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.OFFLINE_MODE_ENABLED)
                .getConfigValue()) == 1;
    }

    @Override
    public int getPinCodeMaxRetryCount() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.PIN_CODE_MAX_RETRY_COUNT).getConfigValue());
    }

    @Override
    public String getHashingAlogorithm() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.HASHING_ALGORTHIM).getConfigValue();
    }

    @Override
    public int getDecimalCount() {
        return Integer.parseInt(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.DECIMAL_COUNT).getConfigValue());
    }

    @Override
    public String getAliasChangeEndPointOperation() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ALIAS_CHANGE_END_POINT_OPERATION)
                .getConfigValue();
    }

    @Override
    public String getApplicationNameEn() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.APPLICATION_NAME_EN).getConfigValue();
    }

    @Override
    public String getApplicationNameAr() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.APPLICATION_NAME_AR).getConfigValue();
    }

    @Override
    public String getRegistrationBankCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CUSTOMER_REGISTRATION_BANK_CODE)
                .getConfigValue();
    }

    @Override
    public String getRegistrationProfileCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CUSTOMER_REGISTRATION_PROFILE_CODE)
                .getConfigValue();
    }

    @Override
    public String getDefaultBankCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.DEFAULT_BANK_CODE).getConfigValue();
    }

    @Override
    public String getMigsVpcVersion() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.VPC_VERSION).getConfigValue();
    }

    @Override
    public String getMigsVpcCurrency() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.VPC_CURRENCY).getConfigValue();
    }

    @Override
    public String getMigsVpcAccessCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.VPC_ACCESS_CODE).getConfigValue();
    }

    @Override
    public String getMigsVpcCommand() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.VPC_COMMAND).getConfigValue();
    }

    @Override
    public String getMigsVpcMershant() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.VPC_MERCHANT).getConfigValue();
    }

    @Override
    public String getMigsVpcRedirectUrl() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.VPC_REDIRECT_URL).getConfigValue();
    }

    @Override
    public String getMigsVpcTitle() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.VPC_TITLE).getConfigValue();
    }

    @Override
    public String getMigsSZ() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SZ).getConfigValue();
    }

    @Override
    public String getMigsAmountKind() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AMOUNT_KIND).getConfigValue();
    }

    @Override
    public String getMigsUrl() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MIGS_URL).getConfigValue();
    }

    @Override
    public String getMigsSecureSecret() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SECURE_SECRET).getConfigValue();
    }

    @Override
    public String getAirTimeServiceTypeCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AIR_TIME_SERVICE_TYPE_CODE)
                .getConfigValue();
    }

    @Override
    public String getDonationsServiceTypeCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.DONATIONS_SERVICE_TYPE_CODE)
                .getConfigValue();
    }

    @Override
    public String getEncryptionCertificateAlias() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ENCRYPTION_CERTIFICATE_ALIAS)
                .getConfigValue();
    }

    @Override
    public boolean enableTokenEncryption() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.ENABLE_TOKEN_ENCRYPTION).getConfigValue()) == 1;
    }

    @Override
    public int getOtpValidityInMinutes() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.OTP_VALIDITY_IN_MINUTES).getConfigValue());
    }

    @Override
    public int getMobileSignInValidityInMin() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.MOBILE_SIGN_IN_VALIDITY_IN_MINITUES).getConfigValue());
    }

    @Override
    public int getServiceSignInValidityInMin() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.SERVICE_SIGN_IN_VALIDITY_IN_MINITUES).getConfigValue());
    }

    @Override
    public int getPasswordMaxRetryCount() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.PASSKEY_MAX_RETRY_COUNT).getConfigValue());
    }

    @Override
    public boolean getEnableAppsChecksums() {
        return Integer.parseInt(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ENABLE_APPS_CHECKSUMS)
                .getConfigValue()) == 1;
    }

    @Override
    public String getAgentCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AGENT_CODE).getConfigValue();
    }

    @Override
    public BigDecimal getCashCap() {
        return new BigDecimal(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PSP_CASH_CAP).getConfigValue());
    }

    @Override
    public String getMobileNumberRegex() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MOBILE_NUMBER_REGEX).getConfigValue();
    }

    @Override
    public String getEmailRegex() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.EMAIL_REGEX).getConfigValue();
    }

    @Override
    public String getSMTPHost() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SMTP_HOST).getConfigValue();
    }

    @Override
    public int getSMTPPort() {
        return Integer.parseInt(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SMTP_PORT).getConfigValue());
    }

    @Override
    public String getEmailSubject() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.EMAIL_SUBJECT).getConfigValue();
    }

    @Override
    public String getEmailFrom() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.EMAIL_FROM).getConfigValue();
    }

    @Override
    public String getPushNotificationURL() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PUSH_NOTIFICATION_URL)
                .getConfigValue();
    }

    @Override
    public String getAPIKey() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.FCM_API_KEY).getConfigValue();
    }

    @Override
    public String getISO8583ConfigFilePath() {
        return "/config/app/spring/j8583-config.xml";
    }

    @Override
    public String getMPClearProcessor() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MPCLEAR_PROCESSOR).getConfigValue();
    }

    @Override
    public String getGAMParkingProductId() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.GAM_PARKING_PRODUCT_ID)
                .getConfigValue();
    }

    @Override
    public long getBulkRegistrationDefaultId() {
        return Long.parseLong(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.BULK_REGISTRATION_DEFAULT_ID).getConfigValue());
    }

    @Override
    public long getBulkPaymentDefaultId() {
        return Long.parseLong(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.BULK_PAYMENT_DEFAULT_ID)
                .getConfigValue());
    }

    @Override
    public String getPinExpiryTimeInHours() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PIN_EXPIRY_TIME_IN_HOURS)
                .getConfigValue();
    }

    @Override
    public String getMEPSServiceName() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MEPS_SERVICE_NAME).getConfigValue();
    }

    @Override
    public String getMEPSPaymentEndpoint() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MEPS_PAYMENT_ENDPOINT)
                .getConfigValue();
    }

    @Override
    public String getMEPSReversalEndpoint() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MEPS_REVERSAL_ENDPOINT)
                .getConfigValue();
    }

    @Override
    public String getPasswordExpiryTimeInHours() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PASS_WORD_EXPIRY_TIME_IN_HOURS)
                .getConfigValue();
    }

    @Override
    public String getMEPSISO8583ConfigFilePath() {
        return "/config/app/spring/j8583-config-meps.xml";
    }

    @Override
    public String getBulkPaymentOperations() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.BULK_PAYMENT_OPERATION_NAME)
                .getConfigValue();
    }

    @Override
    public boolean isSystemStandAlone() {
        return "1".equals(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.IS_SYSTEM_STAND_ALONE)
                .getConfigValue());
    }

    @Override
    public String getCustomersChangeHandler() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CUSTOMERS_CHANGE_HANDLER)
                .getConfigValue();
    }

    @Override
    public String getCorporatesChangeHandler() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CORPORATES_CHANGE_HANDLER)
                .getConfigValue();
    }

    @Override
    public String getEmailUsername() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.EMAIL_USERNAME).getConfigValue();
    }

    @Override
    public String getEmailPassword() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.EMAIL_PASS_WORD).getConfigValue();
    }

    @Override
    public String getAgentTypeCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AGENT_TYPE_CODE).getConfigValue();
    }

    @Override
    public String getBanksUsersLicense() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.BANKS_USERS_LICENSE).getConfigValue();
    }

    @Override
    public String getEnableRegistrationOTP() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ENABLE_REGISTRATION_OTP)
                .getConfigValue();
    }

    @Override
    public long getCashCapWatermark() {
        return Long.parseLong(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CASH_CAP_WATERMARK).getConfigValue());
    }

    @Override
    public String getCashCAPNotificationEmail() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CASH_CAP_NOTIFICATION_EMAIL)
                .getConfigValue();
    }

    @Override
    public boolean getCashCAPNotificationSent() {
        return "1".equals(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CASH_CAP_NOTIFICATION_SENT)
                .getConfigValue());
    }

    @Override
    public boolean allowYaqeenAddressToBeNull() {
        return "1".equals(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.IS_ADDRESS_UNKNOWN).getConfigValue());
    }

    @Override
    public String getDefaultCity() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.DEFAULT_CITY).getConfigValue();
    }

    @Override
    public Long getNumberOfAllowsOtpRequest() {
        return Long.parseLong(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.NUMBER_OF_ALLOWS_OTP_REQUEST).getConfigValue());
    }

    @Override
    public String getVIbanCheckDigits() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.VIRTUAL_IBAN_CHECK_DIGITS)
                .getConfigValue();
    }

    @Override
    public int getActivationCodeValidity() {
        return Integer.parseInt(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.ACTIVATION_CODE_VALIDITY).getConfigValue());
    }

    @Override
    public String getPushNotificationService() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PUSH_NOTIFICATION_SERVICE_CODE)
                .getConfigValue();
    }

    @Override
    public String middleWareUrl() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MIDDLE_WARE_URL).getConfigValue();
    }

    @Override
    public String getMobileNumber() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MOBILE_NUMBER).getConfigValue();
    }

    @Override
    public String getDeploymentLocation() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.DEPLOYMENT_LOCATION).getConfigValue();
    }

    @Override
    public String getServiceAdminMessageTypeCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ADMIN_MESSAGE_TYPE_CODE)
                .getConfigValue();
    }

    @Override
    public boolean getSendPinCodeUponRegistration() {
        return "1".equals(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.SEND_PIN_CODE_UPON_REGESTRATION).getConfigValue());
    }

    @Override
    public int getAmlLowerScoreHit() {
        return Integer.parseInt(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AML_LOWER_SCORE_HIT)
                .getConfigValue());
    }

    @Override
    public int getAmlUpperScoreHit() {
        return Integer.parseInt(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AML_UPPER_SCORE_HIT)
                .getConfigValue());
    }

    @Override
    public int getAmlNumberOfHits() {
        return Integer.parseInt(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AML_NUMBER_OF_HITS).getConfigValue());
    }

    @Override
    public String getATMPublicKey() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ATM_PUBLIC_KEY).getConfigValue();
    }

    @Override
    public String getAmlEndPointOperation() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.AML_ENDPOINT_OPERATION)
                .getConfigValue();
    }

    @Override
    public boolean isSmsNotificationEnabledOnCustomerRejection() {
        MPAY_SysConfig systemConfigurations = LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.CUSTOMER_REGISTRATION_ENABLE_REJECTION_NOTIFICATION);
        return !Objects.isNull(systemConfigurations) && systemConfigurations.getConfigValue().equals("1");
    }

    @Override
    public boolean isSmsNotificationEnabledOnCashinExceededLimits() {
        MPAY_SysConfig systemConfigurations = LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.CASHIN_ENABLE_EXCEEDING_LIMITS_NOTIFICATION);
        return !Objects.isNull(systemConfigurations) && systemConfigurations.getConfigValue().equals("1");

    }

    @Override
    public String getATMMessageTypeCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ATM_MESSAGE_TYPE_CODE)
                .getConfigValue();
    }

    @Override
    public boolean getMaskMobileNumberInNotification() {
        return "1".equals(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.MASK_MOBILE_NUMBER_IN_NOTIFICATIONS).getConfigValue());
    }

    @Override
    public int getDormantMobileAccountTimeoutInDays() {
        MPAY_SysConfig systemConfigurations = LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.DORMANT_MOBILE_ACCOUNT_TIMEOUT_IN_DAYS);
        return (Objects.isNull(systemConfigurations)) ? 0 : Integer.parseInt(systemConfigurations.getConfigValue());
    }

    @Override
    public int getDormantServiceAccountTimeoutInDays() {
        MPAY_SysConfig systemConfigurations = LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.DORMANT_SERVICE_ACCOUNT_TIMEOUT_IN_DAYS);
        return (Objects.isNull(systemConfigurations)) ? 0 : Integer.parseInt(systemConfigurations.getConfigValue());
    }

    @Override
    public BigDecimal getATMMultiplies() {
        return BigDecimal.valueOf(Double.parseDouble(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ATM_MULTIPLIES)
                .getConfigValue()));
    }

    @Override
    public boolean getIsCustomerSetActiveOrNot() {
        return "1".equals(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.MAKE_CUSTOMER_ACTIVE_IN_SHORT_CIRCUIT).getConfigValue());
    }

    @Override
    public String getOperationVersion() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.OPERTION_VERSION).getConfigValue();
    }

    @Override
    public String getClientId() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CLIENT_ID).getConfigValue();
    }

    @Override
    public String getClearingSystemId() {
        // TODO to be implemented by MARS team ...
        return "CBQ";
    }

    @Override
    public String getMpClearBicCode() {
        // TODO to be implemented by MARS team ...
        // it should follow this regex: "[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}"
        return "MPCLEARX";
    }

    @Override
    public String getSendingPSPBicCode() {
        // TODO to be implemented by MARS team ...
        // it should follow this regex: "[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}"
        return "ZAINXXXX";
    }

    @Override
    public String getSendingAgentShortName() {
        // TODO to be implemented by MARS team ...
        return "BOJ";
    }

    @Override
    public String getOperators() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.OPERATORS).getConfigValue();
    }


    @Override
    public String getMobileNotificationChannels() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.MOBILE_NOTIFICATIONS_CHANNELS).getConfigValue();

    }

    @Override
    public String getTollFreeNumber() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.TROLL_FREE_NUMBER).getConfigValue();
    }

    @Override
    public String getInterNationalNumber() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.INERNATIONALNUMBER).getConfigValue();
    }

    @Override
    public String getComplainsLinkURL() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.COMPLAINS_LINK_URL).getConfigValue();
    }

    @Override
    public String getATMlocationsURL() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.ATM_LOCATIONS_URL).getConfigValue();
    }

    @Override
    public String getHomeNotificationsCount() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.HOME_NOTIFICATIONS_COUNT).getConfigValue();
    }

    @Override
    public String getPageNotificationsCount() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.PAGE_NOTIFICATIONS_COUNT).getConfigValue();
    }

    @Override
    public boolean createServiceVirtualIbanOrNot() {
        return  LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CREATE_SERVICE_VIRTAUL_IBAN).getConfigValue().equals("1");
    }

    @Override
    public boolean getSendPinCodeUponCorporateRegistration() {
        return "1".equals(LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.SEND_PIN_CODE_UPON_CORPORATE_REGISTRATION).getConfigValue());
    }

    @Override
    public String getImagePath() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.IMAGES_PATH).getConfigValue();
    }

    @Override
    public String getCoreAppCheckSumCode() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.CORE_CHECK_SUM).getConfigValue();
    }

    @Override
    public String getReferralCommissionAmount() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.REFERRAL_COMMISSION_AMOUNT).getConfigValue();
    }

    @Override
    public String getRefereeCommissionAmount() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.REFEREE_COMMISSION_AMOUNT).getConfigValue();
    }

    @Override
    public int getRefundPeriodInDays() {
        MPAY_SysConfig systemConfigurations = LookupsLoader.getInstance()
                .getSystemConfigurations(SysConfigKeys.REFUND_PERIOD_IN_DAYS);
        return (Objects.isNull(systemConfigurations)) ? 0 : Integer.parseInt(systemConfigurations.getConfigValue());
    }

    @Override
    public String getSmppUrl() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SMPP_URL).getConfigValue();
    }


    @Override
    public String getSmsServiceId() {
        return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.SMS_SERVICE_ID).getConfigValue();
    }

    @Override
    public Long getKycWalletId() {
        return Long.valueOf(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.LIGHT_WALLET).getConfigValue());
    }

    @Override
    public Long getKycFullWalletId() {
        return Long.valueOf(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.FULL_WALLET_KYC_ID).getConfigValue());
    }

	@Override
	public String getOperationsAllowedToBerefunded() {
		return LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.REFUND_ALLOWED_OPERATIONS).getConfigValue();
	}
}