package com.progressoft.mpay;

public class NotificationChannelsCode {
    public static final String SMS = "1";
    public static final String EMAIL = "2";
    public static final String PUSH_NOTIFICATION = "3";

    private NotificationChannelsCode() {
    }
}
