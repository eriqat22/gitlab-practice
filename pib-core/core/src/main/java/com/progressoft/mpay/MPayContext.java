package com.progressoft.mpay;

import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.mpay.mpclear.MPayCommunicatorHelper;
import com.progressoft.mpay.notifications.INotificationHelper;
import com.progressoft.mpay.notifications.NotificationHelper;

public class MPayContext {
	private static MPayContext instance;

	private ILookupsLoader lookupsLoader;
	private ISystemParameters systemParameters;
	private IDataProvider dataProvider;
	private INotificationHelper notificationHelper;
	private Communicator communicator;
	private ICryptographer cryptographer;
	private IServiceUserManager serviceUserManager;

	private MPayContext() {
		this.lookupsLoader = LookupsLoader.getInstance();
		this.systemParameters = SystemParameters.getInstance();
		this.dataProvider = DataProvider.instance();
		this.communicator = MPayCommunicatorHelper.getCommunicator();
		this.cryptographer = new MPayCryptographer();
		this.notificationHelper = new NotificationHelper();
		this.serviceUserManager = new ServiceUserManager();
	}

	private MPayContext(ILookupsLoader lookupsLoader, ISystemParameters systemParameters, IDataProvider dataProvider ,
			INotificationHelper notificationHelper , Communicator communicator , ICryptographer cryptographer , IServiceUserManager serviceUserManager) {
		this.lookupsLoader = lookupsLoader;
		this.systemParameters = systemParameters;
		this.dataProvider = 	dataProvider;
		this.notificationHelper = notificationHelper;
		this.communicator = communicator;
		this.cryptographer = cryptographer;
		this.serviceUserManager = serviceUserManager;
	}

	public static MPayContext getInstance() {
		if (instance == null)
			instance = new MPayContext();
		return instance;
	}

	public static void initialize(ILookupsLoader lookupsLoader, ISystemParameters systemParameters, IDataProvider dataProvider ,
			INotificationHelper notificationHelper , Communicator communicator , ICryptographer cryptographer , IServiceUserManager serviceUserManager) {
		instance = new MPayContext(lookupsLoader, systemParameters, dataProvider,notificationHelper,communicator,cryptographer,serviceUserManager);
	}

	public ILookupsLoader getLookupsLoader() {
		return this.lookupsLoader;
	}

	public ISystemParameters getSystemParameters() {
		return this.systemParameters;
	}

	public IDataProvider getDataProvider() {
		return this.dataProvider;
	}

	public INotificationHelper getNotificationHelper() {
		return this.notificationHelper;
	}

	public Communicator getCommunicator() {
		return this.communicator;
	}

	public ICryptographer getCryptographer() {
		return this.cryptographer;
	}

	public IServiceUserManager getServiceUser() {
		return this.serviceUserManager;
	}
}
