package com.progressoft.mpay.registration.customers;

public class CustomerWorkflowServiceActions {
	public static final String APPROVE = "SVC_Approve";
	public static final String REJECT = "SVC_Reject";
	public static final String DELETE = "SVC_Delete";
	public static final String DELETE_APPROVAL = "SVC_DeleteApproval";
	public static final String RESEND = "Resend";

	private CustomerWorkflowServiceActions() {

	}
}
