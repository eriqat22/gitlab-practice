package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.messages.ProcessingStatusCodes;

public class PostCustomerRegistrationApproval implements FunctionProvider {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(PostCustomerRegistrationApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =====");
		MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
		handleRequestMessageIfExist(customer);

		customer.setIsRegistered(true);
		handleMobiles(customer);
		customer.setApprovedData(null);
	}

	private void handleRequestMessageIfExist(MPAY_Customer customer) {
		if (customer.getIsRegistered())
			return;
		if (customer.getHint() != null && customer.getHint().trim().length() > 0) {
			MPAY_MPayMessage registrationMessage = DataProvider.instance()
					.getMPayMessage(Long.parseLong(customer.getHint()));
			if (registrationMessage != null) {
				if (!registrationMessage.getProcessingStatus().getCode()
						.equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED))
					return;
				registrationMessage.setProcessingStatus(
						LookupsLoader.getInstance().getProcessingStatus(ProcessingStatusCodes.ACCEPTED));
				DataProvider.instance().mergeMPayMessage(registrationMessage);
			}
		}
	}

	private void handleMobiles(MPAY_Customer customer) throws WorkflowException {
		List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId());
		if (mobiles == null || mobiles.isEmpty())
			return;
		Iterator<MPAY_CustomerMobile> mobilesIterator = mobiles.listIterator();
		do {
			MPAY_CustomerMobile mobile = mobilesIterator.next();
			if (mobile.getStatusId() != null
					&& mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.APPROVAL)
					|| mobile.getStatusId().getCode().equals(CustomerMobileWorkflowStatuses.INTEGRATION))
				JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile,
						CustomerMobileWorkflowServiceActions.APPROVE, new WorkflowException());
		} while (mobilesIterator.hasNext());
	}
}