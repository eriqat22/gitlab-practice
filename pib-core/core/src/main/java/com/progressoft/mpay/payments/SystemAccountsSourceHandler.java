package com.progressoft.mpay.payments;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_SystemAccountsCashOut;

public class SystemAccountsSourceHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(SystemAccountsSourceHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside SystemAccountsSourceHandler ------------");

		MPAY_SystemAccountsCashOut entity = (MPAY_SystemAccountsCashOut) changeHandlerContext.getEntity();
		SystemAccountsCashOutHelper.setDestinationData(entity);
	}
}