package com.progressoft.mpay.registration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;

public class RegFileChangeHandler implements ChangeHandler{

	private static final Logger logger = LoggerFactory.getLogger(RegFileChangeHandler.class);

	@Override
	public void handle(ChangeHandlerContext arg0) throws InvalidInputException {
		logger.debug("inside RegFileChangeHandler");

	}

}
