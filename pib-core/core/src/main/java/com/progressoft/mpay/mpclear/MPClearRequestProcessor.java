package com.progressoft.mpay.mpclear;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.mpay.ICryptographer;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.SmsNotificationType;
import com.progressoft.mpay.common.RequestProcessor;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgType;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.mpclear.plugins.MPClearMessageProcessor;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingResult;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.notifications.INotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.solab.iso8583.IsoMessage;

public class MPClearRequestProcessor extends RequestProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MPClearHelper.class);

	public MPClearRequestProcessor(IDataProvider dataProvider, ISystemParameters systemParameters, ILookupsLoader lookupsLoader, ICryptographer cryptographer, INotificationHelper notificationHelper, Communicator communicator) {
		super(dataProvider, systemParameters, lookupsLoader, cryptographer, notificationHelper, communicator);
	}

	public void processRequest(String content, String sender, String token, String date) {
		try {
			MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(content, sender, token, date);

			persistMessageLog(messageLog);
			IsoMessage isoMessage = ISO8583Parser.getInstance(systemParameters.getISO8583ConfigFilePath()).parseMessage(messageLog.getContent());
			if (isoMessage == null)
				throw new NotSupportedException("Invalid request received: " + content);
			String messageTypeCode = Integer.toHexString(isoMessage.getType());
			MPAY_MpClearIntegMsgType messageType = lookupsLoader.getMpClearIntegrationMessageTypes(messageTypeCode);
			if(isoMessage.getField(44).getValue().toString().equals("21") && !messageType.getIsRequest())
				return;
			MPClearMessageProcessor processor = MPClearMessageProcessorHelper.getProcessor(messageType);
			if (processor == null)
				throw new NotSupportedException("Not supported messageType: " + messageTypeCode);

			messageLog.setMessageId(isoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString());
			messageLog.setIntegMsgType(messageTypeCode);
			dataProvider.mergeMPClearMessageLog(messageLog);
			MPClearProcessingContext context = new MPClearProcessingContext(new ProcessingContext(dataProvider, lookupsLoader, systemParameters, cryptographer, notificationHelper));
			context.setMessage(messageLog);
			context.setIsoMessage(isoMessage);
			context.setMessageType(messageType);
			MPClearProcessingResult result = processor.processMessage(context);
			if (result != null) {
				dataProvider.persistMPClearProcessingResult(result);
				persistAndSendMPClearResponse(result);
				sendNotifications(result.getNotifications());
			}
		} catch (NotSupportedException | MessageParsingException e) {
			LOGGER.error("Error while processRequest", e);
			throw e;
		} catch (Exception e) {
			throw new MPayGenericException("Error while processRequest", e);
		}
	}

	private void sendNotifications(List<MPAY_Notification> notifications) {
		if (notifications == null || notifications.isEmpty())
			return;
		for (MPAY_Notification notification : notifications) {
			try {
				dataProvider.persistNotification(notification);
				notificationHelper.sendToActiveMQ(notification, SmsNotificationType.GENERAL);
			} catch (Exception e) {
				LOGGER.error("An error while sending notification ", e);
			}
		}
	}

	private void persistAndSendMPClearResponse(MPClearProcessingResult result) {
		if (result.getResponse() != null)
			try {
				persistMessageLog(result.getResponse());
				MPClearHelper.sendToActiveMQ(result.getResponse(), communicator);
			} catch (Exception e) {
				LOGGER.error("Error while PersistAndSendMPClearResponse", e);
			}
	}

	private void persistMessageLog(MPAY_MpClearIntegMsgLog messageLog){
		dataProvider.persistMPClearIntegMessageLog(messageLog);
	}
}
