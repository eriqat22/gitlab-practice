package com.progressoft.mpay.registration.customers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.ErrorHandler;
import com.progressoft.jfw.util.simpleview.SimpleView;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class CustomerMobileErrorHandler extends ErrorHandler {

	@Autowired
	private ItemDao itemDao;

	@Override
	public InvalidInputException handleError(Exception ex, SimpleView simpleView, Object entity, int actionKey) {
		InvalidInputException cause = null;
		MPAY_CustomerMobile customerMobile = (MPAY_CustomerMobile) entity;
		if (ex instanceof DataIntegrityViolationException || ex instanceof TransactionSystemException
				|| ex.getCause() instanceof DataIntegrityViolationException
				|| ex.getCause() instanceof TransactionSystemException) {

			cause = Unique.validate(itemDao, customerMobile, "mobileNumber", customerMobile.getMobileNumber(),
					"duplicat.customer.item.mobile.number");
			if (cause != null)
				return cause;

			cause = Unique.validate(itemDao, customerMobile, "externalAcc", customerMobile.getExternalAcc(),
					"duplicat.customer.item.mobile.externalacc");
			if (cause != null)
				return cause;

			cause = Unique.validate(itemDao, customerMobile, "alias", customerMobile.getAlias(),
					"duplicat.customer.item.mobile.alias");
			if (cause != null)
				return cause;

		}
		return cause;
	}
}
