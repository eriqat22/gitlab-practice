package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_MobileAccount;

public class PostDeleteMobileAccountMPClearApproval implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostDeleteMobileAccountMPClearApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside execute =========");
		MPAY_MobileAccount account = (MPAY_MobileAccount) arg0.get(WorkflowService.WF_ARG_BEAN);
		account.setIsRegistered(false);
		account.setIsActive(false);
		if (account.getRefAccount() == null)
			return;
		account.getRefAccount().setIsActive(false);
		account.setDeletedFlag(true);
		account.getRefAccount().setDeletedBy(account.getDeletedBy());
		account.getRefAccount().setDeletedFlag(account.getDeletedFlag());
		account.getRefAccount().setDeletedOn(account.getDeletedOn());
	}
}