package com.progressoft.mpay.registration.customers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.shared.DynamicField;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.CustomerMobileMetaData;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;

public class DisplayCustomerMobileChanges implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(DisplayCustomerMobileChanges.class);
	private static final String MODIFIED_COLOR = "blue";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handle--------------------------");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) changeHandlerContext.getEntity();
		try {
			Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
			for (UIPortletAttributes ui : portletsAttributes) {
				ui.setVisible(true);
			}
			renderFields(changeHandlerContext, mobile, CustomerMobileMetaData.fromJson(mobile.getApprovedData()));
		} catch (Exception e) {
			throw new InvalidInputException(e);
		}

	}

	private void renderFields(ChangeHandlerContext changeHandlerContext, MPAY_CustomerMobile mobile, CustomerMobileMetaData approvedMobile) {
		logger.debug("inside RenderFields--------------------------");
		List<DynamicField> dynamicFields = new ArrayList<>();

		DynamicField field = new DynamicField();
		field.setLabel(AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.FIELD));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.OLD_DATA));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.NEW_DATA));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.MOBILE_NUMBER));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(approvedMobile.getMobileNumber());
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(mobile.getNewMobileNumber());
		if (!StringUtils.equals(approvedMobile.getMobileNumber(), mobile.getNewMobileNumber()))
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.NFC_SERIAL));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(approvedMobile.getNfcSerial());
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(mobile.getNfcSerial());
		if (!StringUtils.equals(approvedMobile.getNfcSerial(), mobile.getNfcSerial()))
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.NOTIFICATION_SHOW_TYPE));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(String.valueOf(approvedMobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) ? AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.ALIAS) : AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.MOBILE_NUMBER)));
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(String.valueOf(mobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) ? AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.ALIAS) : AppContext.getMsg(CustomerMobileMetaDataTranslationKeys.MOBILE_NUMBER)));
		if (!StringUtils.equals(approvedMobile.getNotificationShowType(), mobile.getNotificationShowType()))
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		changeHandlerContext.getPortletsAttributes().get("Info").setVisible(true);
		changeHandlerContext.getPortletsAttributes().get("Info").setDynamicPortlet(true);
		changeHandlerContext.getPortletsAttributes().get("Info").setDynamicFields(dynamicFields);
	}
}
