package com.progressoft.mpay;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.internal.SessionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.bussinessobject.attachments.AttachmentItem;
import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.bussinessobject.core.JfwDraft;
import com.progressoft.jfw.model.bussinessobject.security.Tenant;
import com.progressoft.jfw.model.bussinessobject.security.User;
import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.shared.Condition;
import com.progressoft.jfw.shared.JFWCurrencyInfoProvider;
import com.progressoft.jfw.shared.JfwCurrencyInfo;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.entities.MPAY_ATM_Voucher;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_AmlCase;
import com.progressoft.mpay.entities.MPAY_AmlHit;
import com.progressoft.mpay.entities.MPAY_BankIntegMessage;
import com.progressoft.mpay.entities.MPAY_BanksUser;
import com.progressoft.mpay.entities.MPAY_BulkPayment;
import com.progressoft.mpay.entities.MPAY_BulkPaymentAtt;
import com.progressoft.mpay.entities.MPAY_BulkRegistration;
import com.progressoft.mpay.entities.MPAY_BulkRegistrationAtt;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entities.MPAY_CashOut;
import com.progressoft.mpay.entities.MPAY_ChargesScheme;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;
import com.progressoft.mpay.entities.MPAY_ClientsCommission;
import com.progressoft.mpay.entities.MPAY_ClientsOTP;
import com.progressoft.mpay.entities.MPAY_CommissionScheme;
import com.progressoft.mpay.entities.MPAY_CorpIntegMessage;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustIntegMessage;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerAtt;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_ExternalIntegMessage;
import com.progressoft.mpay.entities.MPAY_FinancialIntegMsg;
import com.progressoft.mpay.entities.MPAY_IDType;
import com.progressoft.mpay.entities.MPAY_Installment;
import com.progressoft.mpay.entities.MPAY_JvDetail;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_NetworkManIntegMsg;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_RegistrationOTP;
import com.progressoft.mpay.entities.MPAY_RequestTypesScheme;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entities.MPAY_ServiceCashOut;
import com.progressoft.mpay.entities.MPAY_ServiceIntegMessage;
import com.progressoft.mpay.entities.MPAY_ServiceIntegReason;
import com.progressoft.mpay.entities.MPAY_ServiceType;
import com.progressoft.mpay.entities.MPAY_ServicesCategory;
import com.progressoft.mpay.entities.MPAY_SysConfig;
import com.progressoft.mpay.entities.MPAY_TaxScheme;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.BalancePostingResult;
import com.progressoft.mpay.entity.ClientTypes;
import com.progressoft.mpay.entity.JVsDetailsFilter;
import com.progressoft.mpay.entity.MessageInquiryFilter;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.entity.TransactionDetailsFilter;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingResult;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.registration.corporates.CorporateServiceWorkflowStatuses;
import com.progressoft.mpay.registration.corporates.CorporateWorkflowStatuses;

@SuppressWarnings({ "unchecked", "null" })
public class DataProvider implements IDataProvider {
	public static final String REFERENCE = "reference";
	private static final String UNDELETED_ENTITY = "deletedFlag is null or deletedFlag = false";
	private static final String TRANSACTION_ARG = "transaction";
	private static final String DATE_TIME_FORMAT = "dd/mm/yyyy HH24:MI:SS";
	private static final String JV_DETAILS_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
	private static final String RESULT = "result";
	private static final String MOBILE = "mobile";
	private static final String INSIDE_LIST_MINI_TRANSACTIONS = "Inside ListMiniTransactions....";
	private static final String TRANS_DATE = "transDate";
	private static final String SERVICE = "service";
	private static final String REQUEST_ID = "requestID";
	private static final String AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE = " and (deletedFlag = null or deletedFlag = false)";
	private static final String STATUS_ID_CODE = " and statusId.code = ";
	private static final String INSIDE_GET_CUSTOMER_MOBILE = "Inside GetCustomerMobile....";
	private static final String ALIAS = "alias";
	private static final String ACCOUNT = "account";
	private static final String INSTALLMENT = "installment";
	private static final String MESSAGE = "message";
	private static final String MESSAGE_ID = "messageId: ";
	private static final Logger LOGGER = LoggerFactory.getLogger(DataProvider.class);
	private static IDataProvider instance;
	@Autowired
	ItemDao itemDao;
	@PersistenceContext
	private EntityManager em;

	public static IDataProvider instance() {
		LOGGER.debug("Inside Instance....");
		if (instance == null)
			instance = (IDataProvider) AppContext.getApplicationContext().getBean("DataProvider");
		return instance;
	}

	@Override
	public MPAY_CustomerMobile getCustomerMobile(long id) {
		LOGGER.debug(INSIDE_GET_CUSTOMER_MOBILE);
		LOGGER.debug("id: " + id);
		return itemDao.getItem(MPAY_CustomerMobile.class, id);
	}

	@Override
	public MPAY_CustomerMobile getCustomerMobile(String mobileInfo) {
		LOGGER.debug(INSIDE_GET_CUSTOMER_MOBILE);
		LOGGER.debug("mobileInfo: " + mobileInfo);
		return getCustomerMobile(mobileInfo, ReceiverInfoType.MOBILE);
	}

	@Override
	public MPAY_CustomerMobile getCustomerMobile(String mobileInfo, String infoType) {
		LOGGER.debug(INSIDE_GET_CUSTOMER_MOBILE);
		LOGGER.debug("mobileInfo: " + mobileInfo);
		LOGGER.debug("infoType: " + infoType);
		if (mobileInfo == null)
			return null;
		if (ReceiverInfoType.ALIAS.equals(infoType))
			return itemDao.getItem(MPAY_CustomerMobile.class, ALIAS, mobileInfo);
		else if (ReceiverInfoType.MOBILE.equals(infoType))
			return itemDao.getItem(MPAY_CustomerMobile.class, "mobileNumber", mobileInfo);
		else if (ReceiverInfoType.IBAN.equals(infoType)) {
			MPAY_MobileAccount account = itemDao.getItem(MPAY_MobileAccount.class, "iban", mobileInfo);
			if (account == null)
				return null;
			return account.getMobile();
		} else
			return null;
	}

	@Override
	public MPAY_CorpoarteService getCorporateService(String serviceInfo, String serviceInfoType) {
		LOGGER.debug("Inside GetCorporateService....");
		LOGGER.debug("serviceInfo: " + serviceInfo);
		LOGGER.debug("serviceInfoType: " + serviceInfoType);
		if (serviceInfo == null)
			return null;
		if (ReceiverInfoType.ALIAS.equals(serviceInfoType))
			return itemDao.getItem(MPAY_CorpoarteService.class, ALIAS, serviceInfo);
		else if (ReceiverInfoType.CORPORATE.equals(serviceInfoType))
			return itemDao.getItem(MPAY_CorpoarteService.class, "name", serviceInfo);
		else if (ReceiverInfoType.MP_CLEAR_ALIAS.equals(serviceInfoType))
			return itemDao.getItem(MPAY_CorpoarteService.class, "mpclearAlias", serviceInfo);
		else
			return null;
	}

	@Override
	public MPAY_CorpoarteService getCorporateServiceByMPClearAlias(String mpClearAlias) {
		LOGGER.debug("Inside GetCorporateServiceByMPClearAlias....");
		LOGGER.debug("mpClearAlias: " + mpClearAlias);
		return getCorporateService(mpClearAlias, ReceiverInfoType.MP_CLEAR_ALIAS);
	}

	@Override
	public MPAY_CorpoarteService getCorporateService(long id) {
		LOGGER.debug("Inside GetCorporateService....");
		LOGGER.debug("id: " + id);
		return itemDao.getItem(MPAY_CorpoarteService.class, id);
	}

	@Override
	public MPAY_Corporate getCorporate(String name) {
		LOGGER.debug("Inside GetCorporate....");
		LOGGER.debug("name: " + name);
		return itemDao.getItem(MPAY_Corporate.class, "name", name);
	}

	@Override
	@Transactional
	public void persistNotification(MPAY_Notification notification) {
		LOGGER.debug("Inside PersistNotification....");
		if (notification == null)
			throw new NullArgumentException("notification");
		if (notification.getId() != null && notification.getId() == 0)
			itemDao.persist(notification);
		else
			itemDao.merge(notification);
	}

	@Override
	public void persistAccount(MPAY_Account account) {
		LOGGER.debug("Inside PersistAccount....");
		if (account == null)
			throw new NullArgumentException(ACCOUNT);
		itemDao.persist(account);
	}

	@Override
	public void persistMPClearIntegMessageLog(MPAY_MpClearIntegMsgLog messageLog) {
		LOGGER.debug("Inside PersistMPClearIntegMessageLog....");
		if (messageLog == null)
			throw new NullArgumentException("messageLog");
		itemDao.persist(messageLog);
	}

	@Override
	public void persistFinancialIntegMessage(MPAY_FinancialIntegMsg message) {
		LOGGER.debug("Inside PersistFinancialIntegMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.persist(message);
	}

	@Override
	public MPAY_MPayMessage getMPayMessage(long id) {
		LOGGER.debug("Inside GetMPayMessage....");
		LOGGER.debug("id: " + id);
		return itemDao.getItem(MPAY_MPayMessage.class, id);
	}

	@Override
	public MPAY_MPayMessage getMPayMessage(String reference) {
		LOGGER.debug("Inside GetMPayMessage....");
		LOGGER.debug("reference: " + reference);
		return itemDao.getItem(MPAY_MPayMessage.class, REFERENCE, reference);
	}

	@Override
	public MPAY_ChargesSchemeDetail getChargeScheme(String messageTypeCode, long chargeSchemeId) {
		LOGGER.debug("Inside GetChargeScheme....");
		LOGGER.debug("messageTypeCode: " + messageTypeCode);
		LOGGER.debug("chargeSchemeId: " + chargeSchemeId);
		List<Filter> filters = new ArrayList<>();
		filters.add(Filter.get("msgType.code", Operator.EQ, messageTypeCode));
		filters.add(Filter.get("refChargesScheme.id", Operator.EQ, chargeSchemeId));
		List<MPAY_ChargesSchemeDetail> details = itemDao.getItems(MPAY_ChargesSchemeDetail.class, null, filters, null,
				null);
		if (details.isEmpty())
			return null;
		return details.get(0);
	}

	@Override
	public long getProfileLimitID(long profileId, long messageTypeId) {
		LOGGER.debug("Inside GetProfileLimitID....");
		LOGGER.debug("profileId: " + profileId);
		LOGGER.debug("messageTypeId: " + messageTypeId);
		String statement = "SELECT details.id FROM MPAY_LimitsSchemeDetail details WHERE details.isActive = true AND details.draftStatus IS NULL AND (details.deletedFlag IS NULL OR details.deletedFlag <> true) AND details.msgType.id = :messageTypeId and details.refLimitsScheme.id = (SELECT profile.limitsScheme.id FROM MPAY_Profile profile WHERE profile.draftStatus IS NULL AND (profile.deletedFlag IS NULL OR profile.deletedFlag <> true) AND profile.id = :profileId)";
		Query query = em.createQuery(statement);
		query.setParameter("messageTypeId", messageTypeId);
		query.setParameter("profileId", profileId);
		try {
			return (long) query.getSingleResult();
		} catch (NoResultException ex) {
			LOGGER.info("GetProfileLimitID", ex);
			return 0L;
		}
	}

	@Override
	public MPAY_Corporate getCorporate(long id) {
		LOGGER.debug("Inside GetCorporate....");
		LOGGER.debug("id: " + id);
		return (MPAY_Corporate) ItemDaoHelper.getUndeleted(itemDao.getItem(MPAY_Corporate.class, id));
	}

	@Override
	public MPAY_Customer getCustomer(long id) {
		LOGGER.debug("Inside GetCustomer....");
		LOGGER.debug("id: " + id);
		return (MPAY_Customer) ItemDaoHelper.getUndeleted(itemDao.getItem(MPAY_Customer.class, id));
	}

	@Override
	public MPAY_NetworkManagement getNetworkManagement(long id) {
		LOGGER.debug("Inside GetNetworkManagement....");
		LOGGER.debug("id: " + id);
		return itemDao.getItem(MPAY_NetworkManagement.class, id);
	}

	@Override
	public MPAY_CashIn getCashIn(long id) {
		LOGGER.debug("Inside GetCashIn....");
		LOGGER.debug("id: " + id);
		return itemDao.getItem(MPAY_CashIn.class, id);
	}

	@Override
	public MPAY_CashOut getCashOut(long id) {
		LOGGER.debug("Inside GetCashOut....");
		LOGGER.debug("id: " + id);
		return itemDao.getItem(MPAY_CashOut.class, id);
	}

	@Override
	public void presistCustomerIntegMessage(MPAY_CustIntegMessage message) {
		LOGGER.debug("Inside PresistCustomerIntegMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.persist(message);
	}

	@Override
	public void persistCorporateIntegMessage(MPAY_CorpIntegMessage message) {
		LOGGER.debug("Inside PersistCorporateIntegMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.persist(message);
	}

	@Override
	public void persistNetowkrIntegMessage(MPAY_NetworkManIntegMsg message) {
		LOGGER.debug("Inside PersistNetowkrIntegMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.persist(message);
	}

	@Override
	public void persistBankIntegMessage(MPAY_BankIntegMessage message) {
		LOGGER.debug("Inside PersistBankIntegMessage...");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		if (message.getId() != null && message.getId() > 0)
			itemDao.merge(message);
		else
			itemDao.persist(message);
	}

	@Override
	public void updateCustomer(MPAY_Customer customer) {
		LOGGER.debug("Inside customer....");
		if (customer == null)
			throw new NullArgumentException("customer");
		itemDao.merge(customer);
	}

	@Override
	public void updateCorporate(MPAY_Corporate corporate) {
		LOGGER.debug("Inside corporate....");
		if (corporate == null)
			throw new NullArgumentException("corporate");
		itemDao.merge(corporate);
	}

	@Override
	public void updateNetworkManagement(MPAY_NetworkManagement networkManagement) {
		LOGGER.debug("Inside UpdateNetworkManagement....");
		if (networkManagement == null)
			throw new NullArgumentException("networkManagement");
		itemDao.merge(networkManagement);
	}

	@Override
	public void updateCashIn(MPAY_CashIn cashIn) {
		LOGGER.debug("Inside UpdateCashIn....");
		if (cashIn == null)
			throw new NullArgumentException("cashIn");
		itemDao.merge(cashIn);
	}

	@Override
	public void updateCashOut(MPAY_CashOut cashOut) {
		LOGGER.debug("Inside UpdateCashOut....");
		if (cashOut == null)
			throw new NullArgumentException("cashOut");
		itemDao.merge(cashOut);
	}

	@Override
	public void updateFinancialIntegMessage(MPAY_FinancialIntegMsg message) {
		LOGGER.debug("Inside UpdateFinancialIntegMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.merge(message);
	}

	@Override
	public void updateBankIntegMessage(MPAY_BankIntegMessage message) {
		LOGGER.debug("Inside UpdateBankIntegMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.merge(message);
	}

	@Override
	public BigDecimal getBalance(long accountId) {
		LOGGER.debug("Inside GetBalance....");
		LOGGER.debug("accountId: " + accountId);
		MPAY_Account account = itemDao.getItem(MPAY_Account.class, accountId);
		if (account == null)
			return BigDecimal.ZERO;
		return account.getBalance();
	}

	@Override
	public MPAY_FinancialIntegMsg getFinancialMessageByRefMessage(String messageId) {
		LOGGER.debug("Inside GetFinancialMessageByRefMessage....");
		LOGGER.debug(MESSAGE_ID + messageId);
		String whereClause = "refMessage = '" + messageId + "' and isCanceled = false";
		List<MPAY_FinancialIntegMsg> messages = itemDao.getItems(MPAY_FinancialIntegMsg.class, null, null, whereClause,
				null);
		if (messages == null || messages.isEmpty())
			return null;
		return messages.get(0);
	}

	@Override
	public long getNextReferenceNumberSequence() {
		LOGGER.debug("Inside getNextReferenceNumberSequence....");
		return getSequenceValue("GetNewReferenceNumber(?)");
	}

	private long getSequenceValue(String procedureName) {
		if (procedureName == null)
			return 0;
		long sequence = -1;
		String args = "{call " + procedureName + "}";
		Connection cc = ((SessionImpl) em.getDelegate()).connection();
		try (CallableStatement callableStatement = cc.prepareCall(args)) {
			callableStatement.registerOutParameter(1, Types.BIGINT);
			callableStatement.execute();
			sequence = callableStatement.getLong(1);
		} catch (SQLException ex) {
			throw new MPayGenericException("An error occurd while getting next Reference Number sequence", ex);
		}

		return sequence;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void persistMPayMessage(MPAY_MPayMessage message) {
		LOGGER.debug("Inside PersistMPayMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.persist(message);
	}

	@Override
	public void persistProcessingResult(MessageProcessingResult result) {
		LOGGER.debug("Inside PersistProcessingResult....");
		if (result == null)
			throw new NullArgumentException(RESULT);
		if (result.getMessage() != null)
			if (result.getMessage().getId() > 0)
				itemDao.merge(result.getMessage());
			else
				itemDao.persist(result.getMessage());
		if (result.getTransaction() != null) {

			if (result.getTransaction().getRefTrsansactionJvDetails().size() > 0) {
				updateLastTransactionDate(result.getTransaction().getReceiverServiceAccount());
				updateLastTransactionDate(result.getTransaction().getSenderServiceAccount());
				updateLastTransactionDate(result.getTransaction().getReceiverMobileAccount());
				updateLastTransactionDate(result.getTransaction().getSenderMobileAccount());
			}

			itemDao.persist(result.getTransaction());
			for (MPAY_JvDetail jv : result.getTransaction().getRefTrsansactionJvDetails()) {
				itemDao.persist(jv);
			}
		}
		if (result.getMpClearMessage() != null)
			itemDao.persist(result.getMpClearMessage());
	}

	private void updateLastTransactionDate(MPAY_ServiceAccount serviceAccount) {
		if (serviceAccount != null)
			serviceAccount.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
	}

	private void updateLastTransactionDate(MPAY_MobileAccount mobileAccount) {
		if (mobileAccount != null)
			mobileAccount.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void persistMEPSProcessingResult(MessageProcessingResult result) {
		LOGGER.debug("Inside PersistProcessingResult....");
		if (result == null)
			throw new NullArgumentException(RESULT);
		if (result.getMessage() != null)
			if (result.getMessage().getId() > 0)
				itemDao.merge(result.getMessage());
			else
				itemDao.persist(result.getMessage());
		if (result.getTransaction() != null) {
			itemDao.persist(result.getTransaction());
			for (MPAY_JvDetail jv : result.getTransaction().getRefTrsansactionJvDetails()) {
				itemDao.persist(jv);
			}
		}
		if (result.getMpClearMessage() != null)
			itemDao.persist(result.getMpClearMessage());
	}

	@Override
	public void mergeProcessingResult(MessageProcessingResult result) {
		LOGGER.debug("Inside MergeProcessingResult....");
		if (result == null)
			throw new NullArgumentException(RESULT);
		if (result.getMessage() != null)
			itemDao.merge(result.getMessage());
		if (result.getTransaction() != null) {
			for (MPAY_JvDetail jv : result.getTransaction().getRefTrsansactionJvDetails()) {
				itemDao.merge(jv);
			}
			itemDao.merge(result.getTransaction());
		}
		if (result.getMpClearMessage() != null)
			itemDao.persist(result.getMpClearMessage());
		if (result.getNotifications() != null && !result.getNotifications().isEmpty()) {
			for (MPAY_Notification notification : result.getNotifications()) {
				if (notification == null)
					throw new InvalidArgumentException("notification == null");
				itemDao.persist(notification);
			}
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void mergeMEPSProcessingResult(MessageProcessingResult result) {
		LOGGER.debug("Inside MergeProcessingResult....");
		if (result == null)
			throw new NullArgumentException(RESULT);
		if (result.getMessage() != null)
			itemDao.merge(result.getMessage());
		handleTransactionReversal(result);
		persistMPClearMessage(result);
		if (result.getNotifications() != null && !result.getNotifications().isEmpty()) {
			for (MPAY_Notification notification : result.getNotifications()) {
				if (notification == null)
					throw new InvalidArgumentException("notification == null");
				itemDao.persist(notification);
			}
		}
	}

	private void handleTransactionReversal(MessageProcessingResult result) {
		if (result.getTransaction() != null) {
			for (MPAY_JvDetail jv : result.getTransaction().getRefTrsansactionJvDetails()) {
				if (jv.getId() != null && jv.getId() > 0)
					itemDao.merge(jv);
				else
					itemDao.persist(jv);
			}
			itemDao.merge(result.getTransaction());
		}
	}

	private void persistMPClearMessage(MessageProcessingResult result) {
		if (result.getMpClearMessage() == null)
			return;
		if (result.getMpClearMessage().getId() != null && result.getMpClearMessage().getId() > 0)
			itemDao.merge(result.getMpClearMessage());
		else
			itemDao.persist(result.getMpClearMessage());
	}

	@Override
	public void persistMPClearProcessingResult(MPClearProcessingResult result) {
		LOGGER.debug("Inside PersistMPClearProcessingResult.....");
		if (result == null)
			throw new NullArgumentException(RESULT);

		if (result.getMpClearMessage() != null)
			itemDao.merge(result.getMpClearMessage());
		if (result.getMpayMessage() != null)
			itemDao.merge(result.getMpayMessage());
		if (result.getTransaction() != null) {
			result.getTransaction().getRefTrsansactionJvDetails().stream().filter(detail -> (detail.getId() == null))
					.forEach(itemDao::persist);
			itemDao.merge(result.getTransaction());
		}
	}

	@Override
	public void mergeMPClearMessageLog(MPAY_MpClearIntegMsgLog messageLog) {
		LOGGER.debug("Inside MergeMPClearMessageLog....");
		if (messageLog == null)
			throw new NullArgumentException("messageLog");
		itemDao.merge(messageLog);
	}

	@Override
	public MPAY_MpClearIntegMsgLog getMPClearMessage(String messageId) {
		LOGGER.debug("Inside GeMPClearMessage....");
//		return itemDao.getItem(MPAY_MpClearIntegMsgLog.class, "messageId", messageId);
		return (MPAY_MpClearIntegMsgLog) itemDao.getEntityManager()
				.createQuery("FROM MPAY_MpClearIntegMsgLog WHERE messageId= '" + messageId + "'").getSingleResult();
	}

	@Override
	public List<MPAY_MpClearIntegMsgLog> getPendingMPClearMessages(String messageId) {
		LOGGER.debug("Inside GetPendingMPClearMessages....");
		String query = "FROM MPAY_MpClearIntegMsgLog WHERE messageId ='" + messageId
				+ "' AND isCanceled = 0 AND responseReceived = 0";
		return itemDao.getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public void mergeIntegrationResult(IntegrationProcessingResult result) {
		LOGGER.debug("Inside MergeIntegrationResult....");
		if (result == null)
			throw new NullArgumentException(RESULT);
		if (result.getMpayMessage() != null)
			itemDao.merge(result.getMpayMessage());
		if (result.getTransaction() != null) {
			itemDao.merge(result.getTransaction());
			for (MPAY_JvDetail jv : result.getTransaction().getRefTrsansactionJvDetails()) {
				itemDao.merge(jv);
			}
		}
		if (result.getMpClearOutMessage() != null)
			itemDao.persist(result.getMpClearOutMessage());
		if (result.getNotifications() != null) {
			for (MPAY_Notification notification : result.getNotifications())
				itemDao.persist(notification);
		}
	}

	@Override
	public void persistIntegrationResult(IntegrationProcessingResult result) {
		LOGGER.debug("Inside PersistIntegrationResult....");
		if (result == null)
			throw new NullArgumentException(RESULT);
		if (result.getMpayMessage() != null)
			itemDao.persist(result.getMpayMessage());
		if (result.getTransaction() != null) {
			itemDao.persist(result.getTransaction());
			for (MPAY_JvDetail jv : result.getTransaction().getRefTrsansactionJvDetails()) {
				itemDao.persist(jv);
			}
		}
		if (result.getMpClearOutMessage() != null)
			itemDao.persist(result.getMpClearOutMessage());
		if (result.getNotifications() != null) {
			for (MPAY_Notification notification : result.getNotifications())
				itemDao.persist(notification);
		}
	}

	@Override
	public void persistCustomerDevice(MPAY_CustomerDevice device) {
		LOGGER.debug("Inside PersistCustomerDevice....");
		if (device == null)
			throw new NullArgumentException("device");
		itemDao.persist(device);
	}

	@Override
	public void persistCorporateDevice(MPAY_CorporateDevice device) {
		LOGGER.debug("Inside PersistCorporateDevice....");
		if (device == null)
			throw new NullArgumentException("device");
		itemDao.persist(device);
	}

	@Override
	public void mergeCustomerMobile(MPAY_CustomerMobile mobile) {
		LOGGER.debug("Inside MergeCustomerMobile....");
		if (mobile == null)
			throw new NullArgumentException(MOBILE);
		itemDao.merge(mobile);
	}

	@Override
	public MPAY_CustomerDevice getCustomerDevice(String deviceId) {
		LOGGER.debug("Inside GetCustomerDevice....");
		LOGGER.debug("deviceId: " + deviceId);
		String whereClause = "deviceID  = '" + deviceId + "' and (deletedFlag is null or deletedFlag = false)";
		List<MPAY_CustomerDevice> devices = itemDao.getItems(MPAY_CustomerDevice.class, null, null, whereClause, null);
		if (devices.isEmpty())
			return null;
		return devices.get(0);
	}

	@Override
	public void updateNotification(MPAY_Notification notification) {
		LOGGER.debug("Inside UpdateNotification....");
		if (notification == null)
			throw new NullArgumentException("notification");
		itemDao.merge(notification);
	}

	@Override
	public void mergeProcessingContext(MessageProcessingContext context) {
		LOGGER.debug("Inside MergeProcessingContext....");
		if (context == null)
			throw new NullArgumentException("context");
		if (context.getMessage() != null)
			itemDao.merge(context.getMessage());
		if (context.getTransaction() != null) {
			itemDao.merge(context.getTransaction());
			if (context.getTransaction().getRefTrsansactionJvDetails() != null) {
				for (MPAY_JvDetail jv : context.getTransaction().getRefTrsansactionJvDetails())
					itemDao.merge(jv);
			}
		}
	}

	@Override
	public MPAY_Transaction getTransactionByMessageId(long messageId) {
		LOGGER.debug("Inside GetTransactionByMessageId....");
		LOGGER.debug(MESSAGE_ID + messageId);
		return itemDao.getItem(MPAY_Transaction.class, "refMessage.id", messageId);
	}

	@Override
	public void persistTransaction(MPAY_Transaction transaction) {
		LOGGER.debug("Inside PersistTransaction....");
		if (transaction == null)
			throw new NullArgumentException(TRANSACTION_ARG);
		itemDao.persist(transaction);
		if (transaction.getRefTrsansactionJvDetails() != null) {
			for (MPAY_JvDetail jv : transaction.getRefTrsansactionJvDetails())
				itemDao.persist(jv);
		}
	}

	@Override
	public void mergeTransaction(MPAY_Transaction transaction) {
		LOGGER.debug("Inside mergeTransaction....");
		if (transaction == null)
			throw new NullArgumentException(TRANSACTION_ARG);

		if (transaction.getRefTrsansactionJvDetails() != null) {
			for (MPAY_JvDetail jv : transaction.getRefTrsansactionJvDetails())
				if (jv.getId() == null)
					itemDao.persist(jv);
				else
					itemDao.merge(jv);
		}

		itemDao.merge(transaction);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void mergeMEPSTransaction(MPAY_Transaction transaction) {
		LOGGER.debug("Inside PersistTransaction....");
		if (transaction == null)
			throw new NullArgumentException(TRANSACTION_ARG);

		if (transaction.getRefTrsansactionJvDetails() != null) {
			for (MPAY_JvDetail jv : transaction.getRefTrsansactionJvDetails())
				if (jv.getId() == null)
					itemDao.persist(jv);
				else
					itemDao.merge(jv);
		}

		itemDao.merge(transaction);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void reverseTransaction(long id, String reversedBy) {
		LOGGER.debug("Inside reverseTransaction....");
		MPAY_Transaction transaction = itemDao.getItem(MPAY_Transaction.class, id);
		if (transaction == null)
			return;
		transaction.setIsReversed(true);
		transaction.setReversedBy(reversedBy);
		itemDao.merge(transaction);
	}

	@Override
	public void mergeCashIn(MPAY_CashIn cashIn) {
		LOGGER.debug("Inside MergeCashIn....");
		if (cashIn == null)
			throw new NullArgumentException("cashIn");
		itemDao.merge(cashIn);
	}

	@Override
	public void mergeCashOut(MPAY_CashOut cashOut) {
		LOGGER.debug("Inside MergeCashOut....");
		if (cashOut == null)
			throw new NullArgumentException("cashOut");
		itemDao.merge(cashOut);
	}

	@Override
	public void mergeServiceCashOut(MPAY_ServiceCashOut serviceCashOut) {
		LOGGER.debug("Inside mergeServiceCashOut....");
		if (serviceCashOut == null)
			throw new NullArgumentException("serviceCashOut");
		itemDao.merge(serviceCashOut);
	}

	@Override
	public MPAY_Account getAccount(long accountId) {
		LOGGER.debug("Inside getAccount....");
		LOGGER.debug("accountId: " + accountId);
		return itemDao.getItem(MPAY_Account.class, accountId);
	}

	@Override
	public void mergeCustomerDevice(MPAY_CustomerDevice device) {
		LOGGER.debug("Inside MergeCustomerDevice....");
		itemDao.merge(device);
	}

	@Override
	public void mergeCorporateDevice(MPAY_CorporateDevice device) {
		LOGGER.debug("Inside MergeCorporateDevice....");
		itemDao.merge(device);
	}

	@Override
	public void mergeMPayMessage(MPAY_MPayMessage message) {
		LOGGER.debug("Inside MergeMPayMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.merge(message);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void mergeMEPSMPayMessage(MPAY_MPayMessage message) {
		LOGGER.debug("Inside MergeMPayMessage....");
		if (message == null)
			throw new NullArgumentException("message");
		itemDao.merge(message);
	}

	@Override
	public List<MPAY_Transaction> listMiniTransactions(MPAY_CustomerMobile mobile, int maxCount) {
		LOGGER.debug(INSIDE_LIST_MINI_TRANSACTIONS);
		if (mobile == null)
			throw new NullArgumentException(MOBILE);
		LOGGER.debug("mobileId: " + mobile.getId());
		String whereClause = "processingStatus.id = '1' and (senderMobile.id = " + mobile.getId()
				+ " or receiverMobile.id = " + mobile.getId() + ")";
		java.util.Map<String, Boolean> sortMap = new java.util.HashMap<>();
		sortMap.put(TRANS_DATE, false);
		return itemDao.getItems(MPAY_Transaction.class, 0, maxCount, sortMap, null, whereClause, null);
	}

	@Override
	public List<MPAY_Transaction> listMiniTransactions(MPAY_CustomerMobile mobile, Timestamp fromTime,
			Timestamp toTime) {
		LOGGER.debug(INSIDE_LIST_MINI_TRANSACTIONS);
		if (mobile == null)
			throw new NullArgumentException("mobile");
		LOGGER.debug("mobileId: " + mobile.getId());
		String fromTimeFormatted = SystemHelper.formatDate(fromTime, JV_DETAILS_TIME_FORMAT);
		String toTimeFormatted = SystemHelper.formatDate(toTime, JV_DETAILS_TIME_FORMAT);
		StringBuilder builder = new StringBuilder();
		builder.append("processingStatus.id = '1' and (senderMobile.id = ").append(mobile.getId())
				.append(" or receiverMobile.id = ").append(mobile.getId()).append(") and (transDate <= TO_DATE('")
				.append(toTimeFormatted).append("', '" + DATE_TIME_FORMAT + "') and transDate >= TO_DATE('")
				.append(fromTimeFormatted).append("','" + DATE_TIME_FORMAT + "'))");
		java.util.Map<String, Boolean> sortMap = new java.util.HashMap<>();
		sortMap.put(TRANS_DATE, false);
		return itemDao.getItems(MPAY_Transaction.class, 0, 1000, sortMap, null, builder.toString(), null);
	}

	@Override
	public List<MPAY_Transaction> listMiniTransactions(MPAY_CorpoarteService service, int maxCount) {
		LOGGER.debug(INSIDE_LIST_MINI_TRANSACTIONS);
		if (service == null)
			throw new NullArgumentException(SERVICE);
		LOGGER.debug("serviceId: " + service.getId());
		String whereClause = "processingStatus.id = '1' and (senderService.id = " + service.getId()
				+ " or receiverService.id = " + service.getId() + ")";
		java.util.Map<String, Boolean> sortMap = new java.util.HashMap<>();
		sortMap.put(TRANS_DATE, false);
		return itemDao.getItems(MPAY_Transaction.class, 0, maxCount, sortMap, null, whereClause, null);
	}

	@Override
	public List<MPAY_Transaction> listMiniTransactions(MPAY_CorpoarteService service, Timestamp fromTime,
			Timestamp toTime) {
		LOGGER.debug(INSIDE_LIST_MINI_TRANSACTIONS);
		if (service == null)
			throw new NullArgumentException(SERVICE);
		LOGGER.debug("serviceId: " + service.getId());
		String fromTimeFormatted = SystemHelper.formatDate(fromTime, JV_DETAILS_TIME_FORMAT);
		String toTimeFormatted = SystemHelper.formatDate(toTime, JV_DETAILS_TIME_FORMAT);
		StringBuilder builder = new StringBuilder();
		builder.append("processingStatus.id = '1' and (senderService.id = ").append(service.getId())
				.append(" or receiverService.id = ").append(service.getId()).append(") and (transDate <= TO_DATE('")
				.append(toTimeFormatted).append("', '" + DATE_TIME_FORMAT + "') and transDate >= TO_DATE('")
				.append(fromTimeFormatted).append("', '" + DATE_TIME_FORMAT + "')) ");
		java.util.Map<String, Boolean> sortMap = new java.util.HashMap<>();
		sortMap.put(TRANS_DATE, false);
		return itemDao.getItems(MPAY_Transaction.class, 0, 1000, sortMap, null, builder.toString(), null);
	}

	@Override
	public Boolean isAliasUsed(String alias) {
		LOGGER.debug("Inside IsAliasUsed....");
		LOGGER.debug("alias: " + alias);
		List<Filter> filters = new ArrayList<>();
		filters.add(Filter.get(ALIAS, Operator.EQ, alias));
		List<MPAY_CustomerMobile> dbMobiles = itemDao.getItems(MPAY_CustomerMobile.class, null, filters, null, null);
		List<MPAY_CorpoarteService> dbServices = itemDao.getItems(MPAY_CorpoarteService.class, null, filters, null,
				null);
		dbMobiles = ItemDaoHelper.filterUndeleted(dbMobiles);
		dbServices = ItemDaoHelper.filterUndeleted(dbServices);
		return (dbMobiles != null && !dbMobiles.isEmpty()) || (dbServices != null && !dbServices.isEmpty());
	}

	@Override
	public void persistServiceIntegrationMessage(MPAY_ServiceIntegMessage message) {
		LOGGER.debug("Inside PersistServiceIntegrationMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.persist(message);
	}

	@Override
	public void updateServiceIntegrationMessage(MPAY_ServiceIntegMessage message) {
		LOGGER.debug("Inside UpdateServiceIntegrationMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.merge(message);
	}

	@Override
	public MPAY_ServiceIntegMessage getServiceIntegrationMessageByRequestId(String requestId) {
		LOGGER.debug("Inside GetServiceIntegrationMessageByRequestId....");
		LOGGER.debug("requestId: " + requestId);
		return itemDao.getItem(MPAY_ServiceIntegMessage.class, REQUEST_ID, requestId);
	}

	@Override
	public void persistExternalIntegrationMessage(MPAY_ExternalIntegMessage message) {
		LOGGER.debug("Inside PersistExternalIntegrationMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.persist(message);
	}

	@Override
	public void updateExternalIntegrationMessage(MPAY_ExternalIntegMessage message) {
		LOGGER.debug("Inside UpdateExternalIntegrationMessage....");
		if (message == null)
			throw new NullArgumentException(MESSAGE);
		itemDao.merge(message);
	}

	@Override
	public MPAY_ExternalIntegMessage getExternalIntegrationMessageByRequestId(String requestId) {
		LOGGER.debug("Inside GetExternalIntegrationMessageByRequestId....");
		LOGGER.debug("requestId: " + requestId);
		return itemDao.getItem(MPAY_ExternalIntegMessage.class, REQUEST_ID, requestId);
	}

	@Override
	public List<MPAY_Corporate> listActiveCorporates() {
		LOGGER.debug("Inside ListActiveCorporates....");

		String queryString = "FROM MPAY_Corporate c WHERE (c.tenantId = '" + AppContext.getCurrentTenant()
				+ "' OR EXISTS(FROM c.tenants ten WHERE ten.name = '" + AppContext.getCurrentTenant() + "'))"
				+ "AND c.isActive = 1 AND c.isRegistered = 1 AND statusId.code = '" + CorporateWorkflowStatuses.APPROVED
				+ "'";
		Query query = em.createQuery(queryString);
		return query.getResultList();
	}

	@Override
	public MPAY_Customer getCustomer(String idType, String idValue) {
		LOGGER.debug("Inside GetCustomer....");
		LOGGER.debug("idType: " + idType);
		LOGGER.debug("idValue: " + idValue);
		List<Filter> filters = new ArrayList<>();
		Filter f = new Filter();
		f.setProperty("idType.code");
		f.setFirstOperand(idType);
		f.setOperator(Operator.EQ);
		f.setCondition(Condition.AND);
		filters.add(f);

		Filter f1 = new Filter();
		f1.setProperty("idNum");
		f1.setFirstOperand(idValue);
		f1.setOperator(Operator.EQ);
		f1.setCondition(Condition.AND);
		filters.add(f1);

		Filter f2 = new Filter();
		f2.setProperty("deletedFlag");
		f2.setFirstOperand(true);
		f2.setOperator(Operator.NE);
		f2.setCondition(Condition.AND);
		filters.add(f2);

		List<MPAY_Customer> customers = itemDao.getItems(MPAY_Customer.class, null, filters, null, null);
		if (customers.isEmpty())
			return null;
		return customers.get(0);
	}

	@Override
	public void refreshEntity(Object entity) {
		LOGGER.debug("Inside RefreshEntity....");
		if (entity == null)
			throw new NullArgumentException("entity");
		itemDao.refresh(entity);
	}

	@Override
	public MPAY_BankIntegMessage getBankIntegrationMessage(String requestID) {
		LOGGER.debug("Inside GetBankIntegrationMessage....");
		LOGGER.debug("requestID: " + requestID);
		return itemDao.getItem(MPAY_BankIntegMessage.class, REQUEST_ID, requestID);
	}

	@Override
	public void mergeCorporateService(MPAY_CorpoarteService service) {
		LOGGER.debug("Inside MergeCorporateService....");
		if (service == null)
			throw new NullArgumentException(SERVICE);
		itemDao.merge(service);
	}

	@Override
	public MPAY_CorporateDevice getCorporateDevice(String deviceId) {
		LOGGER.debug("Inside GetCorporateDevice....");
		LOGGER.debug("deviceId: " + deviceId);
		String whereClause = "deviceID  = '" + deviceId + "' and (deletedFlag is null or deletedFlag = false)";
		List<MPAY_CorporateDevice> devices = itemDao.getItems(MPAY_CorporateDevice.class, null, null, whereClause,
				null);
		if (devices.isEmpty())
			return null;
		return devices.get(0);
	}

	@Override
	public MPAY_ClientsOTP getOtpClientByCode(String code) {
		LOGGER.debug("Inside getOtpClientByCode");
		LOGGER.debug("code : " + code);
		return itemDao.getItem(MPAY_ClientsOTP.class, "code", code);
	}

	@Override
	public void updateMobileAccount(MPAY_MobileAccount account) {
		LOGGER.debug("Inside UpdateMobileAccount");
		if (account == null)
			throw new NullArgumentException(ACCOUNT);

		itemDao.merge(account);
	}

	@Override
	public void updateServiceAccount(MPAY_ServiceAccount account) {
		LOGGER.debug("Inside UpdateServiceAccount");
		if (account == null)
			throw new NullArgumentException(ACCOUNT);

		itemDao.merge(account);
	}

	@Override
	public void updateAccount(MPAY_Account account) {
		LOGGER.debug("Inside UpdateAccount");
		if (account == null)
			throw new NullArgumentException(ACCOUNT);

		itemDao.merge(account);
	}

	@Override
	public synchronized boolean updateAccountLimit(long accountId, long messageTypeId, BigDecimal amount, int count,
			Date date) throws SQLException {
		LOGGER.debug("Inside UpdateAccountLimit ...");
		Connection connection = ((SessionImpl) em.getDelegate()).connection();
		CallableStatement statement = null;
		try {
			statement = connection.prepareCall("{call UPDATE_CLIENTS_LIMITS(?, ?, ?, ?, ?, ?)}");
			statement.setLong(1, accountId);
			statement.setLong(2, messageTypeId);
			statement.setBigDecimal(3, amount);
			statement.setInt(4, count);
			statement.setDate(5, date);
			statement.registerOutParameter(6, Types.DECIMAL);
			statement.executeUpdate();
			int rowsCount = statement.getInt(6);
			statement.close();
			return rowsCount > 0;
		} catch (SQLIntegrityConstraintViolationException e) {
			LOGGER.error("failing in updateAccountLimit caused by trying to insert negative value");
			return false;
		} finally {
			if (statement != null) {
				statement.close();
			}
		}
	}

	@Override
	public AccountLimits getAccountLimits(long accountId, long messageTypeId) throws SQLException {
		LOGGER.debug("Inside GetMobileLimits ...");
		Connection connection = ((SessionImpl) em.getDelegate()).connection();
		CallableStatement statement = null;
		try {
			AccountLimits limits = new AccountLimits();
			statement = connection.prepareCall(
					"{call GET_ACCOUNT_LIMITS(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?)}");
			statement.setLong(1, accountId);
			statement.setLong(2, messageTypeId);
			statement.registerOutParameter(3, Types.DATE);
			statement.registerOutParameter(4, Types.INTEGER);
			statement.registerOutParameter(5, Types.INTEGER);
			statement.registerOutParameter(6, Types.INTEGER);
			statement.registerOutParameter(7, Types.INTEGER);
			statement.registerOutParameter(8, Types.DECIMAL);
			statement.registerOutParameter(9, Types.INTEGER);
			statement.registerOutParameter(10, Types.DECIMAL);
			statement.registerOutParameter(11, Types.INTEGER);
			statement.registerOutParameter(12, Types.DECIMAL);
			statement.registerOutParameter(13, Types.INTEGER);
			statement.registerOutParameter(14, Types.DECIMAL);

			statement.registerOutParameter(15, Types.INTEGER);
			statement.registerOutParameter(16, Types.DECIMAL);

			statement.registerOutParameter(17, Types.INTEGER);
			statement.registerOutParameter(18, Types.DECIMAL);

			statement.registerOutParameter(19, Types.INTEGER);
			statement.registerOutParameter(20, Types.DECIMAL);

			statement.registerOutParameter(21, Types.INTEGER);
			statement.registerOutParameter(22, Types.DECIMAL);

			statement.registerOutParameter(23, Types.DECIMAL);
			statement.registerOutParameter(24, Types.DECIMAL);
			statement.registerOutParameter(25, Types.DECIMAL);
			statement.registerOutParameter(26, Types.DECIMAL);
			statement.registerOutParameter(27, Types.DECIMAL);
			statement.registerOutParameter(28, Types.DECIMAL);
			statement.registerOutParameter(29, Types.DECIMAL);
			statement.registerOutParameter(30, Types.DECIMAL);
			statement.executeUpdate();

			limits.setAccountId(accountId);
			limits.setMessageTypeId(messageTypeId);

			limits.setDailyDate(statement.getDate(3));
			limits.setWeekNumber(statement.getInt(4));
			limits.setMonth(statement.getInt(5));
			limits.setYear(statement.getInt(6));
			limits.setDailyCount(statement.getInt(7));
			limits.setDailyAmount(statement.getBigDecimal(8));
			limits.setWeeklyCount(statement.getInt(9));
			limits.setWeeklyAmount(statement.getBigDecimal(10));
			limits.setMonthlyCount(statement.getInt(11));
			limits.setMonthlyAmount(statement.getBigDecimal(12));
			limits.setYearlyCount(statement.getInt(13));
			limits.setYearlyAmount(statement.getBigDecimal(14));

			limits.setTotalDailyCount(statement.getInt(15));
			limits.setTotalDailyAmount(statement.getBigDecimal(16));
			limits.setTotalWeeklyCount(statement.getInt(17));
			limits.setTotalWeeklyAmount(statement.getBigDecimal(18));
			limits.setTotalMonthlyCount(statement.getInt(19));
			limits.setTotalMonthlyAmount(statement.getBigDecimal(20));
			limits.setTotalYearlyCount(statement.getInt(21));
			limits.setTotalYearlyAmount(statement.getBigDecimal(22));

			return limits;
		} finally {
			if (statement != null) {
				statement.close();
			}
		}
	}

	@Override
	public void persistOTP(MPAY_ClientsOTP otp) {
		LOGGER.debug("Inside PersistOTP....");
		if (otp == null)
			throw new NullArgumentException("otp");
		itemDao.persist(otp);
	}

	@Override
	public boolean checkIfBankHasActiveAccounts(long bankId, long paymentServiceProviderId) {
		long mobileAccounts = itemDao.getItemCount(MPAY_MobileAccount.class, null,
				"bank.id = " + bankId + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE);
		if (mobileAccounts > 0)
			return true;

		long serviceAccounts = itemDao.getItemCount(MPAY_ServiceAccount.class, null,
				"bank.id = " + bankId + " and service.refCorporate.id != " + paymentServiceProviderId
						+ AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE);
		return serviceAccounts > 0;
	}

	@Override
	public MPAY_CorpoarteService getCorporateServiceByTypeWithoutTenant(String serviceInfo, String serviceInfoType) {
		String propertyWhereClause = getPropertyWhereClauseFromServiceInfoType(serviceInfoType);
		Query getCorporateService = prepareCorporateServiceQuery(propertyWhereClause, serviceInfo);
		List<MPAY_CorpoarteService> corporateServices = getResultListFromQuery(getCorporateService);
		return !corporateServices.isEmpty() ? corporateServices.get(0) : null;
	}

	private String getPropertyWhereClauseFromServiceInfoType(String serviceInfoType) {
		String propertyWhereClause = null;
		if (ReceiverInfoType.ALIAS.equals(serviceInfoType)) {
			propertyWhereClause = ALIAS;
		} else if (ReceiverInfoType.CORPORATE.equals(serviceInfoType)) {
			propertyWhereClause = "name";
		} else if (ReceiverInfoType.MP_CLEAR_ALIAS.equals(serviceInfoType)) {
			propertyWhereClause = "mpclearAlias";
		}
		return propertyWhereClause;
	}

	private Query prepareCorporateServiceQuery(String propertyWhereClause, String serviceInfo) {
		return em.createQuery("FROM MPAY_CorpoarteService service WHERE (service.tenantId = '"
				+ AppContext.getCurrentTenant() + "' "
				+ "OR EXISTS(FROM service.refCorporate.tenants ten WHERE ten.name = '" + AppContext.getCurrentTenant()
				+ "'))" + " AND " + propertyWhereClause + " = '" + serviceInfo + "' AND service.isActive = true");
	}

	private List<MPAY_CorpoarteService> getResultListFromQuery(Query getCorporateService) {
		return getCorporateService.getResultList();
	}

	@Override
	public MPAY_MpClearIntegMsgLog getMPClearMessageLog(long id) {
		LOGGER.debug("Inside getMPClearMessageLog....");
		return itemDao.getItem(MPAY_MpClearIntegMsgLog.class, id);
	}

	@Override
	public BigDecimal getTotalCashBalance() {
		MPAY_Corporate psp = itemDao.getItem(MPAY_Corporate.class, "clientType.code",
				ClientTypes.SERVICE_PROVIDER_CODE);
		if (psp == null)
			return BigDecimal.ZERO;

		List<MPAY_CorpoarteService> services = psp.getRefCorporateCorpoarteServices();
		if (services == null)
			return BigDecimal.ZERO;

		BigDecimal totalBalance = BigDecimal.ZERO;
		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			totalBalance = totalBalance.add(service.getServiceServiceAccounts().get(0).getRefAccount().getBalance());
		} while (servicesIterator.hasNext());
		return totalBalance;
	}

	@Override
	public void updateSystemParameter(String configKey, String configValue) {
		MPAY_SysConfig config = itemDao.getItem(MPAY_SysConfig.class, "configKey", configKey);
		config.setConfigValue(configValue);
		itemDao.merge(config);
	}

	@Override
	public WorkflowStatus getWorkflowStatus(String code) {
		LOGGER.debug("getWorkflowStatus, code = " + code);
		return itemDao.getItem(WorkflowStatus.class, "code", code);
	}

	public void persistCustomer(MPAY_Customer customer) {
		LOGGER.debug("PersistCustomer ===============");
		if (customer == null)
			throw new NullArgumentException("customer");
		itemDao.persist(customer);
	}

	@Override
	public MPAY_NetworkManagement getLastNetworkManagement() {
		LOGGER.debug("getLastNetworkManagement ===============");
		List<MPAY_NetworkManagement> networkfManagements = itemDao.getItems(MPAY_NetworkManagement.class);
		if (networkfManagements.isEmpty())
			return null;
		return networkfManagements.get(0);
	}

	@Override
	public BalancePostingResult callUpdateBalanceSP(long accountId, BigDecimal jvAmount, boolean isDebit)
			throws SQLException {
		LOGGER.debug("Inside callUpdateBalanceSP ...");
		Connection connection = ((SessionImpl) em.getDelegate()).connection();
		CallableStatement statement = null;
		BigDecimal amount = jvAmount;
		if (isDebit)
			amount = jvAmount.negate();
		try {
			statement = connection.prepareCall("{call updateBalances(?, ?, ?, ?, ?)}");
			statement.setLong(1, accountId);
			statement.setBigDecimal(2, amount);
			statement.registerOutParameter(3, Types.INTEGER);
			statement.registerOutParameter(4, Types.DECIMAL);
			statement.registerOutParameter(5, Types.DECIMAL);
			statement.executeUpdate();
			int rowsCount = statement.getInt(3);
			return new BalancePostingResult(rowsCount > 0, statement.getBigDecimal(4), statement.getBigDecimal(5));
		} finally {
			if (statement != null) {
				statement.close();
			}
		}
	}

	@Override
	public void updateWorkflowStep(String stepKey, long workflowId) {
		String updateStatement = "UPDATE JPACurrentStep s SET s.stepId = " + stepKey
				+ " WHERE s.workflowEntry.id = :workflowId";
		Query updateQuery = em.createQuery(updateStatement);
		updateQuery.setParameter("workflowId", workflowId);
		updateQuery.executeUpdate();
	}

	@Override
	public String getNextMPClearMessageId() {
		long sequence = -1;

		Connection cc = ((SessionImpl) em.getDelegate()).connection();
		try (CallableStatement callableStatement = cc.prepareCall("{call GetNewSeqVal_MessageId(?)}")) {
			callableStatement.registerOutParameter(1, Types.BIGINT);
			callableStatement.execute();
			sequence = callableStatement.getLong(1);
		} catch (SQLException ex) {
			throw new MPayGenericException("An error occurd while getting next message sequence", ex);
		}
		return String.format("%s%s%023d", LookupsLoader.getInstance().getPSP().getNationalID(),
				SystemHelper.formatDate(SystemHelper.getSystemTimestamp(), "ddMMyyyy"), sequence);

	}

	@Override
	public List<MPAY_CustomerMobile> listCustomerMobiles(long customerId, String statusCode) {
		return itemDao.getItems(MPAY_CustomerMobile.class, null, null, "refCustomer.id = " + customerId + STATUS_ID_CODE
				+ "'" + statusCode + "'" + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_MobileAccount> listMobileAccounts(long mobileId, String statusCode) {
		return itemDao.getItems(MPAY_MobileAccount.class, null, null, "mobile.id = " + mobileId + STATUS_ID_CODE + "'"
				+ statusCode + "'" + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_CustomerMobile> listCustomerMobiles(long customerId) {
		return itemDao.getItems(MPAY_CustomerMobile.class, null, null,
				"refCustomer.id = " + customerId + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_MobileAccount> listMobileAccounts(long mobileId) {
		return itemDao.getItems(MPAY_MobileAccount.class, null, null,
				"mobile.id = " + mobileId + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_CustomerDevice> listMobileDevices(long mobileId) {
		return itemDao.getItems(MPAY_CustomerDevice.class, null, null,
				"refCustomerMobile.id = " + mobileId + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_CorpoarteService> listCorporateServices(long corporateId) {
		return itemDao.getItems(MPAY_CorpoarteService.class, null, null,
				"refCorporate.id = " + corporateId + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_CorpoarteService> listCorporateServices(long corporateId, String statusCode) {
		return itemDao.getItems(MPAY_CorpoarteService.class, null, null, "refCorporate.id = " + corporateId
				+ STATUS_ID_CODE + "'" + statusCode + "'" + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_ServiceAccount> listServiceAccounts(long serviceId) {
		return itemDao.getItems(MPAY_ServiceAccount.class, null, null,
				"service.id = " + serviceId + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_ServiceAccount> listServiceAccounts(long serviceId, String statusCode) {
		return itemDao.getItems(MPAY_ServiceAccount.class, null, null, "service.id = " + serviceId + STATUS_ID_CODE
				+ "'" + statusCode + "'" + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<MPAY_CorporateDevice> listServiceDevices(long serviceId) {
		return itemDao.getItems(MPAY_CorporateDevice.class, null, null,
				"refCorporateService.id = " + serviceId + AND_DELETED_FLAG_NULL_OR_DELETED_FLAG_FALSE, null);
	}

	@Override
	public List<Tenant> listTenants() {
		LOGGER.debug("inside listTenants");
		String queryString = "from Tenant";
		Query query = em.createQuery(queryString);
		return (List<Tenant>) query.getResultList();
	}

	@Override
	public MPAY_Transaction getTransactionByTransactionRef(String transactionRef) {
		LOGGER.debug("Inside GetTransactionByTransactionRef....");
		LOGGER.debug(MESSAGE_ID + transactionRef);
		return itemDao.getItem(MPAY_Transaction.class, REFERENCE, transactionRef);
	}

	@Override
	public MPAY_ServiceType getMpayServiceTypeByCode(String code) {
		LOGGER.debug("Inside getMpayServiceTypeByCode....");
		LOGGER.debug("code: " + code);
		return itemDao.getItem(MPAY_ServiceType.class, "code", code);
	}

	@Override
	public void deleteClientOTP(MPAY_ClientsOTP otp) {
		LOGGER.debug("Inside deleteClientOTP....");
		itemDao.removeItem(otp);
	}

	@Override
	public List<MPAY_Transaction> searchTransactions(TransactionDetailsFilter filter) {
		LOGGER.debug("Inside SearchTransactions....");
		if (filter == null)
			throw new NullArgumentException("filter");
		String fromTimeFormatted = SystemHelper.formatDate(filter.getFromTime(), JV_DETAILS_TIME_FORMAT);
		String toTimeFormatted = SystemHelper.formatDate(filter.getToTime(), JV_DETAILS_TIME_FORMAT);
		StringBuilder builder = new StringBuilder();
		builder.append("(transDate <= TO_DATE('").append(toTimeFormatted)
				.append("', 'dd/mm/yyyy HH24:MI:SS') and transDate >= TO_DATE('").append(fromTimeFormatted)
				.append("', 'dd/mm/yyyy HH24:MI:SS')) ");

		handleSenderAndReceiver(filter, builder);

		if (filter.getTxType() != null && filter.getTxType().trim().length() > 0)
			builder.append(" and refType.code = '").append(filter.getTxType()).append("'");

		if (filter.getTxDirection() != null && filter.getTxDirection().trim().length() > 0)
			builder.append(" and direction.code = '").append(filter.getTxDirection()).append("'");

		if (filter.getTxOperation() != null && filter.getTxOperation().trim().length() > 0)
			builder.append(" and refOperation.operation = '").append(filter.getTxOperation()).append("'");

		if (filter.getTxReference() != null && filter.getTxReference().trim().length() > 0)
			builder.append(" and reference = '").append(filter.getTxReference()).append("'");

		java.util.Map<String, Boolean> sortMap = new java.util.HashMap<>();
		sortMap.put(TRANS_DATE, false);
		return itemDao.getItems(MPAY_Transaction.class, 0, 1000, sortMap, null, builder.toString(), null);
	}

	private void handleSenderAndReceiver(TransactionDetailsFilter filter, StringBuilder builder) {
		if (filter.getTxSenderMobile() != null && filter.getTxSenderMobile().trim().length() > 0)
			builder.append(" and senderMobile.mobileNumber = '").append(filter.getTxSenderMobile()).append("'");
		if (filter.getTxReceiverMobile() != null && filter.getTxReceiverMobile().trim().length() > 0)
			builder.append(" and receiverMobile.mobileNumber = '").append(filter.getTxReceiverMobile()).append("'");

		if (filter.getTxSenderService() != null && filter.getTxSenderService().trim().length() > 0)
			builder.append(" and senderService.name = '").append(filter.getTxSenderService()).append("'");
		if (filter.getTxReceiverService() != null && filter.getTxReceiverService().trim().length() > 0)
			builder.append(" and receiverService.name = '").append(filter.getTxReceiverService()).append("'");
	}

	@Override
	public List<MPAY_JvDetail> searchJVs(JVsDetailsFilter filter) {
		LOGGER.debug("Inside SearchJVs....");
		if (filter == null)
			throw new NullArgumentException("filter");
		String fromTimeFormatted = SystemHelper.formatDate(filter.getFromTime(), JV_DETAILS_TIME_FORMAT);
		String toTimeFormatted = SystemHelper.formatDate(filter.getToTime(), JV_DETAILS_TIME_FORMAT);
		StringBuilder builder = new StringBuilder();
		builder.append("(refTrsansaction.transDate <= TO_DATE('").append(toTimeFormatted)
				.append("', 'dd/mm/yyyy HH24:MI:SS') and refTrsansaction.transDate >= TO_DATE('")
				.append(fromTimeFormatted).append("', 'dd/mm/yyyy HH24:MI:SS')) ");

		if (filter.getAccountNo() != null && filter.getAccountNo().trim().length() > 0)
			builder.append(" and refAccount.accNumber = '").append(filter.getAccountNo()).append("'");

		if (filter.getType() != null && filter.getType().trim().length() > 0)
			builder.append(" and jvType.code = '").append(filter.getType()).append("'");

		if (filter.getFlag() != null && filter.getFlag().trim().length() > 0)
			builder.append(" and debitCreditFlag = '").append(filter.getFlag()).append("'");

		if (filter.getTransactionId() > 0)
			builder.append(" and refTrsansaction.id = ").append(filter.getTransactionId());

		java.util.Map<String, Boolean> sortMap = new java.util.HashMap<>();
		sortMap.put("refTrsansaction.transDate", false);
		return itemDao.getItems(MPAY_JvDetail.class, 0, 1000, sortMap, null, builder.toString(), null);
	}

	@Override
	public JfwCurrencyInfo getCurrencyInfo(JFWCurrency currency) {
		try {
			return JFWCurrencyInfoProvider.getCurrency(currency.getStringISOCode(), currency.getCode());
		} catch (Exception e) {
			throw new MPayGenericException("Error while getting currency info", e);
		}
	}

	@Override
	public long getNewAccountNumberSequence() {
		LOGGER.debug("Inside getNewAccountNumberSequence....");
		return getSequenceValue("GetNewAccountNumber(?)");
	}

	@Override
	public MPAY_IDType getIdTypeByCode(String code) {
		LOGGER.debug("Inside getIdTypeByCode....");
		return itemDao.getItem(MPAY_IDType.class, "code", code);
	}

	@Override
	public MPAY_BulkRegistrationAtt getBulkRegistrationAttachment(String recordId) {
		return itemDao.getItem(MPAY_BulkRegistrationAtt.class, "recordId", recordId);
	}

	@Override
	public void persistCustomerAtt(MPAY_BulkRegistrationAtt registrationAtt) {
		itemDao.persist(registrationAtt);
	}

	@Override
	public MPAY_BulkPaymentAtt getBulkPaymentFileAttachment(String recordId) {
		return itemDao.getItem(MPAY_BulkPaymentAtt.class, "recordId", recordId);
	}

	@Override
	public void persistPaymentAttachment(MPAY_BulkPaymentAtt bulkPaymentAtt) {
		itemDao.persist(bulkPaymentAtt);
	}

	@Override
	public JfwDraft getJFWDraft(long id) {
		return itemDao.getItem(JfwDraft.class, id);
	}

	@Override
	public MPAY_BulkRegistration getBulkRegistration(long id) {
		return itemDao.getItem(MPAY_BulkRegistration.class, id);
	}

	@Override
	public MPAY_BulkPayment getBulkPayment(long id) {
		return itemDao.getItem(MPAY_BulkPayment.class, id);
	}

	@Override
	public boolean uniqueMobile(String value) {
		String whereClause = "mobileNumber  = '" + value + "' and statusId.code !='100106' ";
		List<MPAY_CustomerMobile> mobiles = itemDao.getItems(MPAY_CustomerMobile.class, null, null, whereClause, null);
		return mobiles.isEmpty();
	}

	@Override
	public boolean uniqueAlias(String value) {
		String whereClause = "alias  = '" + value + "' and statusId.code !='100106' ";
		List<MPAY_CustomerMobile> mobiles = itemDao.getItems(MPAY_CustomerMobile.class, null, null, whereClause, null);
		return mobiles.isEmpty();
	}

	@Override
	public boolean uniqueNationalId(MPAY_Customer customer) {
		Map<String, Object> props = new HashMap<>();
		props.put("idNum", customer.getIdNum());
		InvalidInputException cause = Unique.validate(itemDao, customer, props,
				MPayErrorMessages.DUPLICATE_CUSTOMER_ID);

		return cause.getErrors().size() <= 0;

	}

	@Override
	public MPAY_MPayMessage getMPayMessageByMessageId(String messageId) {
		LOGGER.debug("Inside getMPayMessageByMessageId....");
		LOGGER.debug("messageId: " + messageId);
		return itemDao.getItem(MPAY_MPayMessage.class, "messageID", messageId);
	}

	@Override
	public MPAY_ServiceIntegReason getSeviceIntegReasonByMappingCode(String mappingReasonCode, String processorCode) {
		String whereClause = "mpayMappingReason.code = '" + mappingReasonCode + "' and processorCode = '"
				+ processorCode + "'";
		List<MPAY_ServiceIntegReason> reasons = itemDao.getItems(MPAY_ServiceIntegReason.class, null, null, whereClause,
				null);
		if (reasons == null || reasons.isEmpty())
			return null;
		return reasons.get(0);
	}

	@Override
	public List<MPAY_ChargesScheme> listChargeSchemes() {
		return itemDao.getItems(MPAY_ChargesScheme.class, null, null, UNDELETED_ENTITY, null);
	}

	@Override
	public List<MPAY_LimitsScheme> listLimitSchemes() {
		return itemDao.getItems(MPAY_LimitsScheme.class, null, null, UNDELETED_ENTITY, null);
	}

	@Override
	public List<MPAY_TaxScheme> listTaxSchemes() {
		return itemDao.getItems(MPAY_TaxScheme.class, null, null, UNDELETED_ENTITY, null);
	}

	@Override
	public List<MPAY_CommissionScheme> listCommissionScheme() {
		return itemDao.getItems(MPAY_CommissionScheme.class, null, null, UNDELETED_ENTITY, null);
	}

	@Override
	public List<MPAY_RequestTypesScheme> listRequestTypeSchemes() {
		return itemDao.getItems(MPAY_RequestTypesScheme.class, null, null, UNDELETED_ENTITY, null);
	}

	@Override
	public void updateCustomerMobile(MPAY_CustomerMobile customerMobile) {
		LOGGER.debug("Inside customer....");
		if (customerMobile == null)
			throw new NullArgumentException("customer");
		itemDao.merge(customerMobile);
	}

	@Override
	public long getNumberOfCustomerMobiles(long customerId) {
		LOGGER.debug("Inside GetNumberOfCustomerMobiles....");
		LOGGER.debug("customerId: ", customerId);
		String whereClause = "refCustomer.id  = '" + customerId + "' and (deletedFlag is null or deletedFlag = false)";
		List<MPAY_CustomerMobile> mobiles = itemDao.getItems(MPAY_CustomerMobile.class, null, null, whereClause, null);
		if (mobiles.isEmpty())
			return 0;
		return mobiles.size();
	}

	@Override
	public long getNumberOfCorporateServices(long corporateId) {
		LOGGER.debug("Inside getNumberOfCorporateServices....");
		LOGGER.debug("corporateId: ", corporateId);
		String whereClause = "refCorporate.id  = '" + corporateId
				+ "' and (deletedFlag is null or deletedFlag = false)";
		List<MPAY_CorpoarteService> services = itemDao.getItems(MPAY_CorpoarteService.class, null, null, whereClause,
				null);
		if (services.isEmpty())
			return 0;
		return services.size();
	}

	@Override
	public User getUserByUserNameAndPassword(String userName, String password) {
		LOGGER.debug("Inside GetUserByUserNameAndPassword");
		LOGGER.debug("userName: ", userName);
		LOGGER.debug("password: ", password);
		String whereClause = "username = '" + userName + "' and password = '" + password + "'";
		List<User> users = itemDao.getItems(User.class, null, null, whereClause, null);

		if (users == null || users.isEmpty())
			return null;
		return users.get(0);

	}

	@Override
	public MPAY_Corporate getCorporateByIdTypeAndRegistrationId(String idTypeCode, String registrationId) {
		LOGGER.debug("Inside getCorporateByIdTypeAndRegistrationId");
		LOGGER.debug("idTypeCode: ", idTypeCode);
		LOGGER.debug("registrationId: ", registrationId);
		String whereClause = "idType.code = '" + idTypeCode + "' and REGISTRATIONID = '" + registrationId + "'";
		List<MPAY_Corporate> corporates = itemDao.getItems(MPAY_Corporate.class, null, null, whereClause, null);

		if (corporates == null || corporates.isEmpty())
			return null;
		return corporates.get(0);
	}

	@Override
	public List<MPAY_BanksUser> listBanksUsers() {
		return itemDao.getItems(MPAY_BanksUser.class, null, null, "deletedFlag is null or deletedFlag = false", null);
	}

	@Override
	public MPAY_BanksUser getBankUser(String userName, String password) {
		String whereClause = "username = '" + userName + "' and password = '" + password
				+ "' and (deletedFlag is null or deletedFlag = false)";
		List<MPAY_BanksUser> banksUsers = itemDao.getItems(MPAY_BanksUser.class, null, null, whereClause, null);
		if (banksUsers == null || banksUsers.isEmpty())
			return null;
		return banksUsers.get(0);
	}

	@Override
	public void updateBankUser(MPAY_BanksUser bankUser) {
		itemDao.merge(bankUser);
	}

	@Override
	public MPAY_Corporate getCorporateByRegistrationId(String registrationId) {
		LOGGER.debug("Inside getCorporateByRegistrationId");
		LOGGER.debug("registrationId: ", registrationId);
		String whereClause = "REGISTRATIONID = '" + registrationId + "'";
		List<MPAY_Corporate> corporates = itemDao.getItems(MPAY_Corporate.class, null, null, whereClause, null);
		if (corporates == null || corporates.isEmpty())
			return null;
		return corporates.get(0);
	}

	@Override
	public void persistRegOTP(MPAY_RegistrationOTP regOtp) {
		LOGGER.debug("Inside Persist RegOtp....");
		if (regOtp == null)
			throw new NullArgumentException("regOtp");
		itemDao.persist(regOtp);
	}

	@Override
	public MPAY_RegistrationOTP getRegOtpByCode(String code) {
		return itemDao.getItem(MPAY_RegistrationOTP.class, "code", code);
	}

	@Override
	public void deleteRegOTP(MPAY_RegistrationOTP regOtp) {
		itemDao.removeItem(regOtp);
	}

	@Override
	public MPAY_ClientsCommission getClientCommission(long messageTypeId, long accountId) {
		LOGGER.debug("Inside getClientCommission");
		LOGGER.debug("messageTypeId: ", messageTypeId);
		LOGGER.debug("accountId: ", accountId);
		String whereClause = "messageType.id = " + messageTypeId + " and account.id = " + accountId;
		List<MPAY_ClientsCommission> commissions = itemDao.getItems(MPAY_ClientsCommission.class, null, null,
				whereClause, null);
		if (commissions == null || commissions.isEmpty())
			return null;
		return commissions.get(0);
	}

	@Override
	public void updateClientCommission(MPAY_ClientsCommission commission) {
		if (commission == null)
			throw new NullArgumentException("commission");
		if (commission.getId() != null && commission.getId() > 0)
			itemDao.merge(commission);
		else
			itemDao.persist(commission);
	}

	@Override
	public MPAY_Customer getCustomerByNationalId(String nationalId) {
		if (Objects.isNull(nationalId))
			throw new IllegalArgumentException("national id should not be null");
		return itemDao.getItem(MPAY_Customer.class, "idNum", nationalId);
	}

	@Override
	public MPAY_Customer getCustomerByMobileNumber(String mobileNumber) {
		if (Objects.isNull(mobileNumber))
			throw new IllegalArgumentException("mobileNumber should not be null");
		return itemDao.getItem(MPAY_Customer.class, "mobileNumber", mobileNumber);
	}

	@Override
	public void persistAmlCase(MPAY_AmlCase amlCase) {
		itemDao.persist(amlCase);
		if (amlCase.getRefCaseAmlHits() == null || amlCase.getRefCaseAmlHits().isEmpty())
			return;
		for (MPAY_AmlHit hit : amlCase.getRefCaseAmlHits()) {
			itemDao.persist(hit);
		}
	}

	@Override
	public MPAY_ExternalIntegMessage getSaudiPostIntegrationMessage(String mobileNumber, String nationalId,
			String reasonCode) {
		String whereClause = "reqField1 = '" + nationalId + "' and reqField2 = '" + mobileNumber
				+ "' and reasonCode = '" + reasonCode + "'";
		List<MPAY_ExternalIntegMessage> messages = itemDao.getItems(MPAY_ExternalIntegMessage.class, null, null,
				whereClause, null);
		if (messages == null || messages.isEmpty())
			return null;
		return messages.get(0);
	}

	@Override
	public MPAY_CorpoarteService getCorporateServiceByName(String serviceName) {
		LOGGER.debug("Inside getCorporateServiceByName....");
		LOGGER.debug("service-name: " + serviceName);
		return itemDao.getItem(MPAY_CorpoarteService.class, "name", serviceName);
	}

	@Override
	public void persistATMVoucher(MPAY_ATM_Voucher voucher) {
		itemDao.persist(voucher);
	}

	@Override
	public void updateATMVoucher(MPAY_ATM_Voucher voucher) {
		itemDao.merge(voucher);
	}

	@Override
	public long getNextATMVoucherSequence() {
		LOGGER.debug("Inside getNextATMVoucherSequence....");
		return getSequenceValue("GetNewATMVoucher(?)");
	}

	@Override
	public List<MPAY_ATM_Voucher> listMobileATMVouchers(long mobileId, Timestamp fromTime, Timestamp toTime,
			String voucher) {
		String fromTimeFormatted = null;
		String toTimeFormatted = null;
		StringBuilder builder = new StringBuilder();
		builder.append("refMobile.id = " + mobileId);
		if (fromTime != null) {
			fromTimeFormatted = SystemHelper.formatDate(fromTime, JV_DETAILS_TIME_FORMAT);
			builder.append(" and requestDate >= TO_DATE('").append(fromTimeFormatted)
					.append("','" + DATE_TIME_FORMAT + "')");
		}
		if (toTime != null) {
			toTimeFormatted = SystemHelper.formatDate(toTime, JV_DETAILS_TIME_FORMAT);
			builder.append(" and requestDate <= TO_DATE('").append(toTimeFormatted)
					.append("', '" + DATE_TIME_FORMAT + "')");
		}
		if (!StringUtils.isEmpty(voucher)) {
			builder.append(" and value = '" + voucher + "'");
		}
		java.util.Map<String, Boolean> sortMap = new java.util.HashMap<>();
		sortMap.put("requestDate", false);
		return itemDao.getItems(MPAY_ATM_Voucher.class, 0, 1000, sortMap, null, builder.toString(), null);
	}

	@Override
	public List<MPAY_ATM_Voucher> listPendingMobileATMVouchers(long mobileId) {
		StringBuilder builder = new StringBuilder();
		String formattedCurrentDate = SystemHelper.formatDate(SystemHelper.getSystemTimestamp(),
				JV_DETAILS_TIME_FORMAT);
		builder.append("refMobile.id = " + mobileId);
		builder.append(" and isUsed = 0 and isCanceled = 0");
		builder.append(" and expiryDate >= TO_DATE('").append(formattedCurrentDate)
				.append("', '" + DATE_TIME_FORMAT + "')");
		return itemDao.getItems(MPAY_ATM_Voucher.class, 0, 1000, null, null, builder.toString(), null);
	}

	@Override
	public MPAY_ATM_Voucher getVoucher(String value) {
		return itemDao.getItem(MPAY_ATM_Voucher.class, "value", value);
	}

	@Override
	public List<MPAY_Customer> getCustomersByExternalAccount(String mobileNumber, String excternalAccount) {
		LOGGER.debug("Inside getCustomersByExternalAccount ....");
		if (excternalAccount == null)
			throw new NullArgumentException("value should not be null");
		String whereClause = "mobileNumber = '" + mobileNumber + "' and externalAcc = '" + excternalAccount + "'";
		List<MPAY_Customer> customers = itemDao.getItems(MPAY_Customer.class, null, null, whereClause, null);
		LOGGER.info("where clause for getCustomersByExternalAccount" + ":" + whereClause);
		if (customers == null || customers.isEmpty())
			return new ArrayList<>();
		return customers;
	}

	@Override
	public List<MPAY_Customer> getCustomersByExternalAccount(String excternalAccount) {
		LOGGER.debug("Inside getCustomersByExternalAccount ....");
		if (excternalAccount == null)
			throw new NullArgumentException("value should not be null");
		String whereClause = "externalAcc = '" + excternalAccount + "'";
		List<MPAY_Customer> customers = itemDao.getItems(MPAY_Customer.class, null, null, whereClause, null);
		LOGGER.info("where clause for getCustomersByExternalAccount" + ":" + whereClause);
		if (customers == null || customers.isEmpty())
			return new ArrayList<>();
		return customers;
	}

	@Override
	public List<MPAY_Notification> getMPAYNotificationByRefMessageId(Long refMessageId) {
		LOGGER.debug("refMessageId: " + refMessageId);
		return itemDao.getItems(MPAY_Notification.class, null, null, "refMessage.id=" + refMessageId, null);
	}

	@Override
	public void updateVoucherToUser(MPAY_ATM_Voucher voucher) {
		voucher.setIsUsed(true);
		itemDao.merge(voucher);
	}

	@Override
	public List<MPAY_MessageType> getMessageTypesByCodeOrName(String code, String name) {
		LOGGER.debug("getMessageTypesByCodeOrName: " + code);
		return itemDao.getItems(MPAY_MessageType.class, null, null, "code='" + code + "' or  name='" + name + "'",
				null);
	}

	@Override
	public void updateLanguageStatus(MPAY_Language language) {
		LOGGER.debug("updateLanguageStatus");
		itemDao.merge(language);
	}

	@Override
	public void updateServiceCategoriesStatus(MPAY_ServicesCategory category) {
		LOGGER.debug("updateServiceCategoriesStatus");
		itemDao.merge(category);
	}

	@Override
	public void updateEntity(Object o) {
		LOGGER.debug("updateEntity");
		itemDao.merge(o);
	}

	@Override
	public MPAY_LimitsScheme getLimitSchemeByCode(String code) {
		return itemDao.getItem(MPAY_LimitsScheme.class, "code", code);
	}

	@Override
	public MPAY_TaxScheme getTaxSchemeByCode(String code) {
		return itemDao.getItem(MPAY_TaxScheme.class, "code", code);
	}

	@Override
	public MPAY_CommissionScheme getCommissionScheneByCode(String code) {
		return itemDao.getItem(MPAY_CommissionScheme.class, "code", code);
	}

	@Override
	public MPAY_ChargesScheme getChargeSchemeByCode(String code) {
		return itemDao.getItem(MPAY_ChargesScheme.class, "code", code);
	}

	@Override
	public MPAY_RequestTypesScheme getRequestTypeScheme(String code) {
		return itemDao.getItem(MPAY_RequestTypesScheme.class, "code", code);
	}

	@Override
	public List<MPAY_ServiceType> getAllServiceTypesCategory() {
		return itemDao.getItems(MPAY_ServiceType.class);

	}

	@Override
	public MPAY_Customer getCustomerByIdNumber(String idNumber) {
		LOGGER.debug("Inside GetCustomerByIdNumber....");
		LOGGER.debug("idNum" + ":" + idNumber);
		Query query = em.createQuery(
				"SELECT c FROM MPAY_Customer c WHERE  c.idNum =:idNum And (c.deletedFlag =NULL OR c.deletedFlag<>true)");
		query.setParameter("idNum", idNumber);
		List<?> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		return (MPAY_Customer) resultList.get(0);
	}

	@Override
	public MPAY_Customer getCustomerByFullName(String fullName) {
		LOGGER.debug("Inside GetCustomerByFullName....");
		LOGGER.debug("fullName" + ":" + fullName);
		Query query = em.createQuery(
				"SELECT c FROM MPAY_Customer c WHERE  c.fullName =:fullName And (c.deletedFlag =NULL OR c.deletedFlag<>true)");
		query.setParameter("fullName", fullName);
		List<?> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		return (MPAY_Customer) resultList.get(0);

	}

	@Override
	public MPAY_CustomerMobile getCustomerMobileByAlias(String alias) {
		LOGGER.debug("Inside GetCustomer....");
		LOGGER.debug("alias" + ":" + alias);
		Query query = em.createQuery(
				"SELECT c FROM MPAY_CustomerMobile c WHERE  c.alias =:alias And (c.deletedFlag =NULL OR c.deletedFlag<>true)");
		query.setParameter("alias", alias);
		List<?> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		return (MPAY_CustomerMobile) resultList.get(0);
	}

	@Override
	public MPAY_CustomerMobile getCustomerMobileByNfcSerial(String nfcSerial) {
		LOGGER.debug("Inside GetCustomer....");
		LOGGER.debug("nfc" + ":" + nfcSerial);
		Query query = em.createQuery(
				"SELECT c FROM MPAY_CustomerMobile c WHERE  c.nfcSerial =:nfcSerial And (c.deletedFlag =NULL OR c.deletedFlag<>true)");
		query.setParameter("nfcSerial", nfcSerial);
		List<?> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		return (MPAY_CustomerMobile) resultList.get(0);
	}

	@Override
	public MPAY_ClientsOTP getOtpClientByFullData(String pinCode, Long messageTypeId, Long senderMobileId,
			Long senderServiceId) {

		String whereClause = "code = '" + pinCode + "' AND senderMobile.id=" + senderMobileId + " AND senderService.id="
				+ senderServiceId + " AND messageType.id=" + messageTypeId;
		List<MPAY_ClientsOTP> items = itemDao.getItems(MPAY_ClientsOTP.class, null, null, whereClause, null);

		if (items.isEmpty()) {
			whereClause = "code = '" + pinCode + "' AND senderMobile.id=" + senderMobileId;
			items = itemDao.getItems(MPAY_ClientsOTP.class, null, null, whereClause, null);
		}

		return items.isEmpty() ? null : items.get(0);
	}

	@Override
	public void updateClientOtp(MPAY_ClientsOTP clientOtp) {
		LOGGER.debug("Inside updateClientOtp....");
		LOGGER.debug("clientOtp " + ":" + clientOtp.getId());
		itemDao.merge(clientOtp);
	}

	@Override
	public MPAY_ClientsOTP getExistingOtpByFullData(Long messageTypeId, Long senderMobileId, Long senderServiceId) {
		String whereClause = "senderMobile.id=" + senderMobileId + " AND senderService.id=" + senderServiceId
				+ " AND messageType.id=" + messageTypeId;
		List<MPAY_ClientsOTP> items = itemDao.getItems(MPAY_ClientsOTP.class, null, null, whereClause, null);
		return items.isEmpty() ? null : items.get(0);
	}

	@Override
	public MPAY_Corporate getCorporateByServiceName(String serviceName) {
		LOGGER.debug("Inside getCorporateByRegistrationId");
		LOGGER.debug("serviceName: ", serviceName);
		String whereClause = "serviceName = '" + serviceName + "'";
		List<MPAY_Corporate> corporates = itemDao.getItems(MPAY_Corporate.class, null, null, whereClause, null);
		if (corporates == null || corporates.isEmpty())
			return null;
		return corporates.get(0);
	}

	@Override
	public List<MPAY_MPayMessage> getMPayMessages(MessageInquiryFilter filter) {
		LOGGER.debug("Inside getMPayMessages....");
		if (filter == null)
			throw new NullArgumentException("filter");
		String fromTimeFormatted = SystemHelper.formatDate(filter.getFromTime(), JV_DETAILS_TIME_FORMAT);
		String toTimeFormatted = SystemHelper.formatDate(filter.getToTime(), JV_DETAILS_TIME_FORMAT);
		StringBuilder builder = new StringBuilder();
		builder.append("(processingStamp <= TO_DATE('").append(toTimeFormatted)
				.append("', 'dd/mm/yyyy HH24:MI:SS') and processingStamp >= TO_DATE('").append(fromTimeFormatted)
				.append("', 'dd/mm/yyyy HH24:MI:SS')) ");

		handleSender(filter, builder);

		if (filter.getMsgReference() != null && filter.getMsgReference().trim().length() > 0)
			builder.append(" and reference = '").append(filter.getMsgReference()).append("'");

		if (filter.getMessageType() != null && filter.getMessageType().trim().length() > 0)
			builder.append(" and messageType.code = '").append(filter.getMessageType()).append("'");

		if (filter.getProcessingStatus() != null && filter.getProcessingStatus().trim().length() > 0)
			builder.append(" and processingStatus.code = '").append(filter.getProcessingStatus()).append("'");

		if (filter.getMsgReasonId() != null && filter.getMsgReasonId().trim().length() > 0)
			builder.append(" and reason.code = '").append(filter.getMsgReasonId()).append("'");

		if (filter.getMsgRequestedId() != null && filter.getMsgRequestedId().trim().length() > 0)
			builder.append(" and requestedId = '").append(filter.getMsgRequestedId()).append("'");

		if (filter.getMsgShopId() != null && filter.getMsgShopId().trim().length() > 0)
			builder.append(" and shopId = '").append(filter.getMsgShopId()).append("'");

		if (filter.getMsgChannelId() != null && filter.getMsgChannelId().trim().length() > 0)
			builder.append(" and channelId = '").append(filter.getMsgChannelId()).append("'");
		java.util.Map<String, Boolean> sortMap = new java.util.HashMap<>();
		sortMap.put("processingStamp", false);
		return itemDao.getItems(MPAY_MPayMessage.class, 0, 1000, sortMap, null, builder.toString(), null);
	}

	@Override
	public List<MPAY_Notification> listOfNotificationsByReceiverMobileNumber(String receiverMobileNumber) {
		LOGGER.debug("receiverMobileNumber: " + receiverMobileNumber);
		return itemDao.getItems(MPAY_Notification.class, null, null, "receiver='" + receiverMobileNumber + "'", null);
	}

	private void handleSender(MessageInquiryFilter filter, StringBuilder builder) {
		if (filter.getSenderMobile() != null && filter.getSenderMobile().trim().length() > 0)
			builder.append(" and sender = '").append(filter.getSenderMobile()).append("'");
		if (filter.getSenderService() != null && filter.getSenderService().trim().length() > 0)
			builder.append(" and sender = '").append(filter.getSenderService()).append("'");
	}

	@Override
	public String getNextCityCodeSequence() {
		LOGGER.debug("Inside getNextCityCodeSequence");
		BigDecimal decimal = (BigDecimal) em.createNativeQuery("SELECT CITY_CODE_SEQ.NEXTVAL FROM DUAL")
				.getSingleResult();
		return decimal.toString();
	}

	@Override
	public void persistObject(Object obj) {
		LOGGER.debug("Inside persistObject....");
		if (obj == null)
			throw new InvalidArgumentException(obj.getClass().getName());
		itemDao.persist(obj);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void updateMpclearMegLogToResponseReceived(String messageId) {
		LOGGER.debug("Inside updateMpclearMegLogToResponseReceived");
		itemDao.getEntityManager()
				.createQuery(
						"UPDATE MPAY_MpClearIntegMsgLog SET responseReceived = 1 WHERE messageId = '" + messageId + "'")
				.executeUpdate();
	}

	@Override
	public <T extends JFWEntity> List<T> getEntitiesWhere(Class<T> entity, String where) {
		LOGGER.debug("Inside getEntityWhere");
		return itemDao.getItems(entity, null, null, where, null);
	}

	@Override
	public <T extends AttachmentItem> List<T> getAttachmentsWhere(Class<T> entity, String where) {
		LOGGER.debug("Inside getAttachmentsWhere");
		return itemDao.getItems(entity, null, null, where, null);
	}

	@Override
	public void mergeEntity(JFWEntity entity) {
		LOGGER.debug("Inside mergeEntity....");
		if (entity == null)
			throw new InvalidArgumentException(entity.getClass().getName());
		itemDao.merge(entity);
	}

	@Override
	public List<MPAY_CorpoarteService> getCorporatesByParentCorporateId(Long parentCorporateId) {
		return itemDao.getItems(MPAY_CorpoarteService.class, null, null, "refParentCorporate=" + parentCorporateId,
				null);
	}

	@Override
	public MPAY_Customer getCustomerByNfcSerial(String nfcSerial) {
		LOGGER.debug("Inside getCustomerByNfcSerial....");
		LOGGER.debug("nfc" + ":" + nfcSerial);
		Query query = em.createQuery(
				"SELECT c FROM MPAY_Customer c WHERE  c.nfcSerial =:nfcSerial And (c.deletedFlag =NULL OR c.deletedFlag<>true)");
		query.setParameter("nfcSerial", nfcSerial);
		List<?> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		return (MPAY_Customer) resultList.get(0);
	}

	@Override
	public void updateCorporateService(MPAY_CorpoarteService service) {
		LOGGER.debug("Inside updateCorporateService....");
		if (service == null)
			throw new NullArgumentException("service");
		itemDao.merge(service);
	}

	@Override
	public MPAY_CorpoarteService getCorporateServiceByAlias(String alias) {
		LOGGER.debug("Inside getCorporateServiceByAlias....");
		LOGGER.debug("alias" + ":" + alias);
		Query query = em.createQuery(
				"SELECT s FROM MPAY_CorpoarteService s WHERE  s.alias =:alias And (s.deletedFlag =NULL OR s.deletedFlag<>true)");
		query.setParameter("alias", alias);
		List<?> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		return (MPAY_CorpoarteService) resultList.get(0);
	}

	@Override
	public MPAY_Corporate getCorporateByAlias(String alias) {
		LOGGER.debug("Inside getCorporateByAlias....");
		LOGGER.debug("alias" + ":" + alias);
		Query query = em.createQuery(
				"SELECT c FROM MPAY_Corporate c WHERE  c.alias =:alias And (c.deletedFlag =NULL OR c.deletedFlag<>true)");
		query.setParameter("alias", alias);
		List<?> resultList = query.getResultList();
		if (resultList.isEmpty())
			return null;
		return (MPAY_Corporate) resultList.get(0);
	}

	@Override
	public void persistCustomerAtt(MPAY_CustomerAtt customerAtt) {
		itemDao.persist(customerAtt);
	}

	@Override
	public void persistInstallment(MPAY_Installment installment) {
		LOGGER.debug("Inside PersistAccount....");
		if (installment == null)
			throw new NullArgumentException(INSTALLMENT);
		itemDao.persist(installment);
	}

	@Override
	public List<MPAY_CorpoarteService> listServiceByFirstThreeLetters(String firstThreeLetters) {
		return em.createQuery(
				"FROM MPAY_CorpoarteService WHERE name LIKE :firstThreeLetters AND statusId.code = :approvedStatus AND refCorporate.clientType.code = :merchantCode")
				.setParameter("merchantCode", ClientTypes.MERCHANT)
				.setParameter("approvedStatus", CorporateServiceWorkflowStatuses.APPROVED.toString())
				.setParameter("firstThreeLetters", firstThreeLetters + "%").getResultList();
	}

	@Override
	public boolean isOperationAllowedToRefund(Long operationId) {
		String[] ids = SystemParameters.getInstance().getOperationsAllowedToBerefunded().split(",");
		for(String id : ids) {
			if(id.equals(String.valueOf(operationId)))
				return true;
		}
		return false;
	}

}