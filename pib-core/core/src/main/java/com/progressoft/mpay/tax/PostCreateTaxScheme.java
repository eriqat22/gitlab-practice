package com.progressoft.mpay.tax;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_TaxScheme;
import com.progressoft.mpay.entities.MPAY_TaxSchemeDetail;
import com.progressoft.mpay.entity.MPAYView;

public class PostCreateTaxScheme implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCreateTaxScheme.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		try {
			logger.debug("Inside PostCreateTaxScheme----------------");
			MPAY_TaxScheme scheme = (MPAY_TaxScheme) arg0.get(WorkflowService.WF_ARG_BEAN);

			List<MPAY_MessageType> msgTypes = LookupsLoader.getInstance().getMessageTypes();
			for (MPAY_MessageType messageType : msgTypes) {
				if (!messageType.getIsFinancial())
					continue;
				MPAY_TaxSchemeDetail chargeDetails = new MPAY_TaxSchemeDetail();
				chargeDetails.setIsActive(false);
				chargeDetails.setRefScheme(scheme);
				chargeDetails.setMsgType(messageType);
				chargeDetails.setDescription(messageType.getDescription());
				JfwHelper.createEntity(MPAYView.TAX_SCHEME_DETAILS, chargeDetails);
				logger.debug("Setting tax details for message " + messageType.getDescription());
			}
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}
}
