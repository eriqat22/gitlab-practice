package com.progressoft.mpay.migs.mvc.controller;

@FunctionalInterface
interface MigsHandler {
	String handle(MigsContext migsContext);
}
