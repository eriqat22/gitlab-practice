package com.progressoft.mpay;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entity.ReasonCodes;

public final class Result {

	public static final String POSTED = "Posted";
	public static final String REJECT = "Reject";
	public static final String PROCESSING_RESULT = ReasonCodes.KEY;

	public static final String VALIDATION_RESULT = "validationResult";
	public static final String FAILED = "Failed";
	public static final String SUCCESSFUL = "Successful";

	public static final String REVERSAL_RESULT = "reversalResult";
	public static final String REVERSAL_FAILED = "ReversalFailed";
	public static final String REVERSAL_SUCCESSFUL = "ReversalSuccessful";

	private Result() {

	}

	public static String getResult(MPAY_Reason reason) {
		String result = FAILED;
		if (reason.getCode().equals(ReasonCodes.VALID))
			result = SUCCESSFUL;
		return result;
	}

	@SuppressWarnings("rawtypes")
	public static boolean pass(Map map, String key, String value) {
		return map.get(key) == null ? false : ((String) map.get(key)).equals(value);
	}

	public static boolean pass(PropertySet ps, String key, String value) {
		return ps.getString(key) == null ? false : ps.getString(key).equals(value);
	}
}
