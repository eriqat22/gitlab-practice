package com.progressoft.mpay.plugins.helpers;

import org.apache.commons.lang3.StringUtils;

import com.progressoft.mpay.entities.MPAY_ClientsOTP;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.PluginsCommonConstants;
import com.progressoft.mpay.plugins.ProcessingContext;

public class OTPHanlder {
	private OTPHanlder() {

	}

	public static void loadOTP(ProcessingContext context, String pinCode) {
		if (context == null)
			throw new NullArgumentException("context");
		if (StringUtils.isEmpty(pinCode) || context.getOperation() == null
				|| context.getOperation().getMessageType() == null)
			return;
		if (context.getOperation().getMessageType().getIsOTPEnabled())
			context.getExtraData().put(PluginsCommonConstants.OTP_KEY,
					context.getDataProvider().getOtpClientByCode(pinCode));
	}

	public static void removeOTP(ProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");

		MPAY_ClientsOTP otp = (MPAY_ClientsOTP) context.getExtraData().get(PluginsCommonConstants.OTP_KEY);
		if (otp != null)
			context.getDataProvider().deleteClientOTP(otp);
	}

	public static void loadOTPWithFullData(ProcessingContext context, String pinCode, Long messageTypeId,
			Long senderMobileId, Long SenderServiceId) {
		if (context == null)
			throw new NullArgumentException("context");
		if (StringUtils.isEmpty(pinCode) || context.getOperation() == null
				|| context.getOperation().getMessageType() == null)
			return;
		if (context.getOperation().getMessageType().getIsOTPEnabled())
			context.getExtraData().put(PluginsCommonConstants.OTP_KEY,
					context.getDataProvider().getOtpClientByFullData(pinCode,messageTypeId,senderMobileId,SenderServiceId));
	}

}
