package com.progressoft.mpay.banksintegration;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_BankIntegMessage;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.plugins.ProcessingContext;

public class BankIntegrationContext {
	private MPAY_Transaction transaction;
	private ProcessingContext processingContext;
	private BankIntegrationMethod method;
	private MPAY_BankIntegMessage integMessage;
	private MPAY_BankIntegMessage originalIntegMessage;
	private MPAY_Bank bank;
	private IDataProvider dataProvider;
	private ILookupsLoader lookupsLoader;
	private ISystemParameters systemParameters;

	public BankIntegrationContext(IDataProvider dataProvider, ILookupsLoader lookupsLoader, ISystemParameters systemParameters, MPAY_Bank bank, MPAY_Transaction transaction,
			BankIntegrationMethod method) {
		if (dataProvider == null)
			throw new NullArgumentException("dataProvider");
		if (lookupsLoader == null)
			throw new NullArgumentException("lookupsLoader");
		if (systemParameters == null)
			throw new NullArgumentException("systemParameters");
		if (bank == null)
			throw new NullArgumentException("bank");
//		if (transaction == null)
//			throw new NullArgumentException("transaction");
		if (method == null)
			throw new NullArgumentException("method");

		this.bank = bank;
		this.transaction = transaction;
		this.method = method;
		this.dataProvider = dataProvider;
		this.lookupsLoader = lookupsLoader;
		this.systemParameters = systemParameters;
	}

	public BankIntegrationContext(MPAY_Bank bank, ProcessingContext processingContext, BankIntegrationMethod method, MPAY_Transaction transaction) {
		if (bank == null)
			throw new NullArgumentException("bank");
		if (processingContext == null)
			throw new NullArgumentException("processingContext");
		if (method == null)
			throw new NullArgumentException("method");
//		if (transaction == null)
//			throw new NullArgumentException("transaction");

		this.bank = bank;
		this.processingContext = processingContext;
		this.method = method;
		this.transaction = transaction;
		this.dataProvider = processingContext.getDataProvider();
		this.lookupsLoader = processingContext.getLookupsLoader();
		this.systemParameters = processingContext.getSystemParameters();
	}

	public MPAY_Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(MPAY_Transaction transaction) {
		this.transaction = transaction;
	}

	public BankIntegrationMethod getMethod() {
		return method;
	}

	public void setMethod(BankIntegrationMethod method) {
		this.method = method;
	}

	public MPAY_BankIntegMessage getIntegMessage() {
		return integMessage;
	}

	public void setIntegMessage(MPAY_BankIntegMessage integMessage) {
		this.integMessage = integMessage;
	}

	public MPAY_Bank getBank() {
		return bank;
	}

	public void setBank(MPAY_Bank bank) {
		this.bank = bank;
	}

	public MPAY_BankIntegMessage getOriginalIntegMessage() {
		return originalIntegMessage;
	}

	public void setOriginalIntegMessage(MPAY_BankIntegMessage originalIntegMessage) {
		this.originalIntegMessage = originalIntegMessage;
	}

	public ProcessingContext getProcessingContext() {
		return processingContext;
	}

	public void setProcessingContext(ProcessingContext processingContext) {
		this.processingContext = processingContext;
	}

	public IDataProvider getDataProvider() {
		return this.dataProvider;
	}

	public ILookupsLoader getLookupsLoader() {
		return this.lookupsLoader;
	}

	public ISystemParameters getSystemParameters() {
		return this.systemParameters;
	}
}
