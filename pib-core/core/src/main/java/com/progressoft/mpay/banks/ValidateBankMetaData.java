package com.progressoft.mpay.banks;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_Bank;

public class ValidateBankMetaData implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateBankMetaData.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside ValidateBankMetaData ==========");
		InvalidInputException iie = new InvalidInputException();

		MPAY_Bank bank = (MPAY_Bank) arg0.get(WorkflowService.WF_ARG_BEAN_DRAFT);
		if (bank == null || (bank.getStatusId() != null && "108803".equals(bank.getStatusId().getCode()))) {
			bank = (MPAY_Bank) arg0.get(WorkflowService.WF_ARG_BEAN);
		}
		if (bank.getCode().length() != SystemParameters.getInstance().getRoutingCodeLength()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg("banks.invalidroutecodelength"), SystemParameters.getInstance().getRoutingCodeLength()));
		}

		if (iie.getErrors().size() > 0) {
			throw iie;
		}

	}

}
