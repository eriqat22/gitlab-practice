package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class CheckIfLastServiceAccount implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfLastServiceAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_ServiceAccount account = (MPAY_ServiceAccount) transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (account.getIsDefault() || account.getIsSwitchDefault())
			return false;
		List<MPAY_ServiceAccount> accounts = select(
				account.getService().getServiceServiceAccounts(),
				having(on(MPAY_ServiceAccount.class).getDeletedFlag(), Matchers.nullValue()).or(having(on(MPAY_ServiceAccount.class).getDeletedFlag(), Matchers.equalTo(false))).and(
						having(on(MPAY_ServiceAccount.class).getId(), Matchers.not(account.getId()))));
		return !accounts.isEmpty();
	}
}