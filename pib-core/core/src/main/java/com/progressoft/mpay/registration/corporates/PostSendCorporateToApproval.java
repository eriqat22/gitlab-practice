package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostSendCorporateToApproval implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostSendCorporateToApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =====");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		handleServices(corporate);
	}

	private void handleServices(MPAY_Corporate corporate) throws WorkflowException {
		logger.debug("Inside HandleServices =====");
		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId());
		if (services == null || services.isEmpty())
			return;
		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			if (service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.MODIFIED.toString()) || service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.REJECTED.toString()))
				JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.SEND_TO_APPROVAL, new WorkflowException());
			else if (service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.MODIFICATION.toString())) {
				InvalidInputException iie = new InvalidInputException();
				iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.SERVICE_UNDER_MODIFICATION));
				throw iie;
			}
			handleAccounts(service);

		} while (servicesIterator.hasNext());
	}

	private void handleAccounts(MPAY_CorpoarteService service) throws WorkflowException {
		logger.debug("Inside HandleAccounts ================");
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_ServiceAccount account = accountsIterator.next();
			if (account.getStatusId().getCode().equals(ServiceAccountWorkflowStatuses.MODIFIED.toString()) || account.getStatusId().getCode().equals(ServiceAccountWorkflowStatuses.REJECTED.toString()))
				JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.SEND_APPROVAL, new WorkflowException());
			else if (account.getStatusId().getCode().equals(ServiceAccountWorkflowStatuses.MODIFICATION.toString())) {
				InvalidInputException iie = new InvalidInputException();
				iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.SERVICE_ACCOUNT_UNDER_MODIFICATION));
				throw iie;
			}
		} while (accountsIterator.hasNext());
	}
}