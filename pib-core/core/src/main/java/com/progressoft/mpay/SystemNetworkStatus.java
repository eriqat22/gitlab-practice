package com.progressoft.mpay;

import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entity.StatusCodes;

public final class SystemNetworkStatus {

	private SystemNetworkStatus() {

	}
	public static boolean isSystemOnline(IDataProvider dataProvider) {
		MPAY_NetworkManagement netMan = dataProvider.getLastNetworkManagement();
		if (netMan == null)
			return false;
		return StatusCodes.LOGGED_IN.compare(netMan.getStatusId());
	}
}
