package com.progressoft.mpay.messages;

public class ExtraData {
	private String key;
	private String value;

	public ExtraData(){
		//Default
	}

	public ExtraData(String key, String value){
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
