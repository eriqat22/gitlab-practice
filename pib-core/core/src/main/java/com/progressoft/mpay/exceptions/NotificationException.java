package com.progressoft.mpay.exceptions;

public class NotificationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NotificationException(String message) {
		super(message);
	}

	public NotificationException(String message, Exception e) {
		super(message, e);
	}
}
