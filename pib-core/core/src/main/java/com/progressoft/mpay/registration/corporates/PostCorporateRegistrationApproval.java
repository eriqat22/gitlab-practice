package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.messages.ProcessingStatusCodes;

public class PostCorporateRegistrationApproval implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCorporateRegistrationApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =====");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		handleRequestMessageIfExist(corporate);
		corporate.setIsRegistered(true);
		corporate.setIsActive(true);
		handleServices(corporate);
		corporate.setApprovedData(null);
	}

	private void handleRequestMessageIfExist(MPAY_Corporate corporate) {
		if (corporate.getIsRegistered())
			return;
		if (corporate.getHint() != null && corporate.getHint().trim().length() > 0) {
			MPAY_MPayMessage registrationMessage = DataProvider.instance().getMPayMessage(Long.parseLong(corporate.getHint()));
			if (registrationMessage != null) {
				if (!registrationMessage.getProcessingStatus().getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED))
					return;
				registrationMessage.setProcessingStatus(LookupsLoader.getInstance().getProcessingStatus(ProcessingStatusCodes.ACCEPTED));
				DataProvider.instance().mergeMPayMessage(registrationMessage);
			}
		}
	}

	private void handleServices(MPAY_Corporate corporate) throws WorkflowException {
		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId());
		if (services == null || services.isEmpty())
			return;
		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			if (service.getStatusId() != null && service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.APPROVAL.toString())
					|| service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.INTEGRATION.toString()))
				JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.APPROVE, new WorkflowException());
		} while (servicesIterator.hasNext());
	}
}