package com.progressoft.mpay.payments;

public class PaymentsOperations {
	public static final String CUSTOMER_UPGRADE_KYC = "payUpgradeCommission";
	public static final String CUSTOMER_CASH_IN = "cashin";
	public static final String CUSTOMER_CASH_OUT = "cashout";
	public static final String SERVICE_CASH_IN = "srvcashin";
	public static final String SERVICE_CASH_OUT = "srvcashout";
	public static final String TRANSACTION_REVERSAL = "reverse";

	private PaymentsOperations() {

	}
}
