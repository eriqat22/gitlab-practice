package com.progressoft.mpay.registration;

public class FileRegistrationTypes {

	public static final String FILE_TYPE_CUSTOMER = "1";
	public static final String FILE_TYPE_CORPORATE = "2";

	private FileRegistrationTypes() {

	}

}
