package com.progressoft.mpay.entity;

public final class BeneficiaryFlag {
	public static final String Beneficiary = "1";
	public static final String NonBeneficiary = "2";

	private BeneficiaryFlag() {

	}
}
