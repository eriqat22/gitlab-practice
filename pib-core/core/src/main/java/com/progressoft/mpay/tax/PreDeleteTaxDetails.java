package com.progressoft.mpay.tax;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_TaxSchemeDetail;
import com.progressoft.mpay.entities.MPAY_TaxSlice;
import com.progressoft.mpay.entity.MPAYView;

public class PreDeleteTaxDetails implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PreDeleteTaxDetails.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PreDeleteTaxDetails ----------------");
        MPAY_TaxSchemeDetail schemeDetails = (MPAY_TaxSchemeDetail) arg0.get(WorkflowService.WF_ARG_BEAN);
        schemeDetails.setIsActive(Boolean.FALSE);
        for (MPAY_TaxSlice slice : schemeDetails.getRefTaxDetailsTaxSlices()) {
            logger.debug("DELETING slice with TX amount" + slice.getTxAmountLimit() + ". . . . .");
            Map<Option, Object> options = new EnumMap<>(Option.class);
            options.put(Option.ACTION_NAME, "Delete");
            JfwHelper.executeAction(MPAYView.TAX_SLICES, slice, options);
        }
    }

}
