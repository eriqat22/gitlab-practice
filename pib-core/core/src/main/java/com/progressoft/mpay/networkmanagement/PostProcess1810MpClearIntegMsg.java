package com.progressoft.mpay.networkmanagement;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_NetworkManIntegMsg;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.NetworkManActionTypes;

public class PostProcess1810MpClearIntegMsg implements FunctionProvider {

	@Autowired
	private ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(PostProcess1810MpClearIntegMsg.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside PostProcess1810MpClearIntegMsg ====");
		MPAY_NetworkManIntegMsg msg = (MPAY_NetworkManIntegMsg) transientVars.get(WorkflowService.WF_ARG_BEAN);
		int sttsCode = Integer.parseInt(msg.getResponse().getCode());

		Map<Option, Object> option1 = new EnumMap<>(Option.class);
		String whereClause = "tenantId = '" + AppContext.getCurrentTenant() + "'";
		MPAY_NetworkManagement netMan = itemDao.getItems(MPAY_NetworkManagement.class, null, null, whereClause, null).get(0);
		if (sttsCode == 0) {
			if (NetworkManActionTypes.LOGIN.compare(netMan.getOperationType()))
				option1.put(Option.ACTION_NAME, "SVC_AcceptLogin");
			else
				option1.put(Option.ACTION_NAME, "SVC_AcceptLogout");
		} else {
			option1.put(Option.ACTION_NAME, "SVC_Reject");
		}
		JfwHelper.executeAction(MPAYView.NETWORK_MANAGEMENT, netMan, option1);
	}

}
