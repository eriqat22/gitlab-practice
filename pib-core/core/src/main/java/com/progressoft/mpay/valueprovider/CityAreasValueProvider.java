package com.progressoft.mpay.valueprovider;

import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_City;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CityAreasValueProvider {

    public Map<String, String> getCityAreas(String cityId) {
        Map<String, String> areas = new HashMap<>();
        if (cityId != null && cityId.length() > 0 && NumberUtils.isDigits(cityId)) {
            MPAY_City city = MPayContext.getInstance().getLookupsLoader().getCityById(Long.parseLong(cityId));
            city.getCityIdCityAreas().forEach(a -> areas.put(a.getId().toString(), a.getName()));
        }
        return areas;
    }
}
