package com.progressoft.mpay.registration;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entities.MPAY_CashOut;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entity.MPAYView;

public class ValidateResendTime implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateResendTime.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside ValidateResendTime ==========");
		InvalidInputException iie = new InvalidInputException();
		String viewName = (String) arg0.get(WorkflowService.WF_ARG_VIEW_NAME);
		Object bean = arg0.get(WorkflowService.WF_ARG_BEAN);
		MPAY_MpClearIntegMsgLog msg = ResendValidatorHelper.tryGetCustomer(viewName, bean);
		if (msg != null) {
			validateTime(iie, msg);
			return;
		}
		msg = ResendValidatorHelper.tryGetCorporate(viewName, bean);
		if (msg != null) {
			validateTime(iie, msg);
			return;
		}

		if (viewName.equals(MPAYView.NETWORK_MANAGEMENT.viewName)) {
			msg = ((MPAY_NetworkManagement) bean).getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.CASH_IN.viewName)) {
			msg = ((MPAY_CashIn) bean).getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.CASH_OUT.viewName)) {
			msg = ((MPAY_CashOut) bean).getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.PENDING_MP_CLEAR_MESSAGES.viewName)) {
			msg = (MPAY_MpClearIntegMsgLog) bean;
		} else {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, "Resend unavailable on current message");
			throw iie;
		}

		validateTime(iie, msg);
	}

	private void validateTime(InvalidInputException iie, MPAY_MpClearIntegMsgLog msg) throws InvalidInputException {
		if (msg.getReason() != null) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("resend.response.received"));
			throw iie;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(msg.getCreationDate().getTime());
		int resendWaitingTime = SystemParameters.getInstance().getMPClearResendWaitingTime();
		cal.add(Calendar.SECOND, resendWaitingTime);

		if (new Date().before(cal.getTime())) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg("resend.wating.time"), resendWaitingTime));
			throw iie;
		}
	}
}
