package com.progressoft.mpay;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.modules.security.auth.MD5PasswordEncoder;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.exceptions.MPayCryptographicException;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.openssl.PEMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.crypto.Cipher;
import java.io.*;
import java.nio.charset.Charset;
import java.security.*;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Calendar;

public class MPayCryptographer implements ICryptographer {

	public enum DefaultCryptographer {
		INSTANCE;
		public MPayCryptographer get() {
			File file = new File(
					MPClearClientHelper.class.getProtectionDomain().getCodeSource().getLocation().getPath());

			return new MPayCryptographer(SystemParameters.getInstance().getDefaultEncoding(),
					SystemParameters.getInstance().getEncryptionCertificateAlias(),
					file.getParentFile().getAbsolutePath() + "/" + SystemParameters.getInstance().getKeyStorePath(),
					SystemParameters.getInstance().getKeyStorePass());
		}
	}

	public static final String CAN_NOT_LOAD_CERTIFICATE = "Can't load certificate";
	public static final String FAILED_TO_DECODE_STRING = "Failed to decode string";
	public static final String FAILED_TO_LOAD_SIGNATURE_ALGORITHM = "Failed to load signature algorithm";
	public static final String INVALID_SIGNING_KEY = "Invalid signing key";
	public static final String ENCRYPTION_FAILED = "Encryption failed";
	private static final Logger LOG = LoggerFactory.getLogger(MPayCryptographer.class);
	private String encoding;
	private String certificateAlias;
	private String keyStorePath;
	private String keyStorePass;

	public MPayCryptographer(String encoding, String certificateAlias, String keyStorePath, String keyStorePass) {
		this.encoding = encoding;
		this.certificateAlias = certificateAlias;
		this.keyStorePath = keyStorePath;
		this.keyStorePass = keyStorePass;
	}

	public MPayCryptographer() {
		// Default
	}

	@Override
	public String generateToken(ISystemParameters systemParameters, String message, String signingDate,
								String integrationSource) {
		String certificate = getCertificateInfo(systemParameters, integrationSource);
		File file = new File(MPClearClientHelper.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		MPayCryptographer crypto = new MPayCryptographer(systemParameters.getDefaultEncoding(), certificate,
				file.getParentFile().getAbsolutePath() + "/" + systemParameters.getKeyStorePath(),
				systemParameters.getKeyStorePass());
		return crypto.sign(message + signingDate);
	}

	@Override
	public boolean verifyToken(ISystemParameters systemParameters, String message, String signature,
							   Calendar signingDate, String integrationSource) {
		String certificate;
		String signingDateString;

		if (signingDate == null)
			signingDateString = "";
		else
			signingDateString = SystemHelper.formatDate(signingDate.getTime(), systemParameters.getSigningDateFormat());

		certificate = getCertificateInfo(systemParameters, integrationSource);

		File file = new File(MPClearClientHelper.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		MPayCryptographer crypto = new MPayCryptographer(systemParameters.getDefaultEncoding(), certificate,
				file.getParentFile().getAbsolutePath() + "/" + systemParameters.getKeyStorePath(),
				systemParameters.getKeyStorePass());

		return crypto.verify(message + signingDateString, signature);
	}

	@Override
	public String sign(String data) {
		try {
			byte[] binaryData = SystemHelper.decodeString(data, encoding);
			byte[] signature = sign(binaryData);
			return SystemHelper.encodeBase64(signature);
		} catch (UnsupportedEncodingException e) {
			LOG.error(FAILED_TO_DECODE_STRING, e);
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public byte[] sign(byte[] data) {
		try {
			RSAPrivateKey privateKey = (RSAPrivateKey) loadCertificatePrivateKey();
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initSign(privateKey);
			sig.update(data);
			return sig.sign();
		} catch (Exception e) {
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public boolean verify(String data, String signature) {
		try {
			byte[] binaryData = SystemHelper.decodeString(data, encoding);
			byte[] binarySig = SystemHelper.decodeBase64(signature);
			return verify(binaryData, binarySig);
		} catch (UnsupportedEncodingException e) {
			LOG.error(FAILED_TO_DECODE_STRING, e);
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public boolean verify(byte[] data, byte[] signature) {
		try {
			X509Certificate cert = loadCertificate();
			PublicKey publicKey = cert.getPublicKey();
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initVerify(publicKey);
			sig.update(data);
			return sig.verify(signature);
		} catch (Exception e) {
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public String encrypt(String data, boolean withPrivateKey) {
		try {
			byte[] plainBytes = SystemHelper.decodeString(data, encoding);
			byte[] encryptedBytes = encrypt(plainBytes, withPrivateKey);
			return SystemHelper.encodeBase64(encryptedBytes);
		} catch (UnsupportedEncodingException e) {
			LOG.error(FAILED_TO_DECODE_STRING, e);
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public String decrypt(String data) {
		try {
			byte[] decodedBytes = SystemHelper.decodeBase64(data);
			byte[] plainBytes = decrypt(decodedBytes);
			return SystemHelper.encodeString(plainBytes, encoding);
		} catch (UnsupportedEncodingException e) {
			LOG.error(FAILED_TO_DECODE_STRING, e);
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public byte[] decrypt(byte[] data) {
		RSAPrivateKey privateKey = (RSAPrivateKey) loadCertificatePrivateKey();
		try {
			Cipher encrypter = Cipher.getInstance("RSA");
			encrypter.init(Cipher.DECRYPT_MODE, privateKey);
			return encrypter.doFinal(data);
		} catch (Exception e) {
			LOG.error("Decryption failed", e);
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public byte[] encrypt(byte[] data, boolean withPrivateKey) {
		Key key;

		if (withPrivateKey)
			key = loadCertificatePrivateKey();
		else {
			X509Certificate cert = loadCertificate();
			key = cert.getPublicKey();
		}

		try {
			Cipher encrypter = Cipher.getInstance("RSA");
			encrypter.init(Cipher.ENCRYPT_MODE, key);
			return encrypter.doFinal(data);
		} catch (Exception e) {
			LOG.error(ENCRYPTION_FAILED, e);
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public String getCertificateSerial() {
		X509Certificate cert = loadCertificate();
		String hex = cert.getSerialNumber().toString(16);
		return SystemHelper.formatHex(hex);
	}

	@Override
	public String encryptCertificateSerial() {
		X509Certificate cert = loadCertificate();
		try {
			String hex = cert.getSerialNumber().toString(16);
			String hexFormat = SystemHelper.formatHex(hex);
			byte[] encryptedData = encrypt(hexFormat.getBytes(), false);
			return SystemHelper.encodeBase64(encryptedData);
		} catch (Exception e) {
			LOG.error(ENCRYPTION_FAILED, e);
			throw new MPayCryptographicException(e);
		}
	}

	private KeyStore loadKeyStore() throws WorkflowException {
		String filename = keyStorePath;
		try (FileInputStream is = new FileInputStream(filename)) {
			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(is, keyStorePass.toCharArray());
			is.close();
			return keystore;
		} catch (Exception e) {
			LOG.error(CAN_NOT_LOAD_CERTIFICATE, e);
			throw new WorkflowException(CAN_NOT_LOAD_CERTIFICATE, e);
		}
	}

	@Override
	public X509Certificate loadCertificate() {
		KeyStore keystore;
		try {
			keystore = loadKeyStore();
			return (X509Certificate) keystore.getCertificate(certificateAlias);

		} catch (Exception e) {
			LOG.error(CAN_NOT_LOAD_CERTIFICATE, e);
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public Key loadCertificatePrivateKey() {
		KeyStore keystore;
		try {
			keystore = loadKeyStore();
			return keystore.getKey(certificateAlias, keyStorePass.toCharArray());

		} catch (Exception e) {
			LOG.error(CAN_NOT_LOAD_CERTIFICATE, e);
			throw new MPayCryptographicException(e);
		}
	}

	@Override
	public String createJsonToken(ISystemParameters systemParameters, String values) {
		LOG.info("Values: " + values);
		String hashValue = HashingAlgorithm.hash(systemParameters, values);
		LOG.info("HashValue: " + hashValue);

		if (systemParameters.enableTokenEncryption())
			hashValue = encryptJsonToken(systemParameters, hashValue);

		return hashValue;
	}

	@Override
	public String decryptJsonToken(ISystemParameters systemParameters, String token) {
		try {
			String certificate = systemParameters.getEncryptionCertificateAlias();
			File file = new File(
					MPClearClientHelper.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			MPayCryptographer crypto = new MPayCryptographer(systemParameters.getDefaultEncoding(), certificate,
					file.getParentFile().getAbsolutePath() + "/" + systemParameters.getKeyStorePath(),
					systemParameters.getKeyStorePass());
			return crypto.decrypt(token);
		} catch (Exception e) {
			LOG.error("Error while decrypt token", e);
			return null;
		}
	}

	@Override
	public String encryptJsonToken(ISystemParameters systemParameters, String value) {
		try {
			String certificate = systemParameters.getEncryptionCertificateAlias();
			File file = new File(
					MPClearClientHelper.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			MPayCryptographer crypto = new MPayCryptographer(systemParameters.getDefaultEncoding(), certificate,
					file.getParentFile().getAbsolutePath() + "/" + systemParameters.getKeyStorePath(),
					systemParameters.getKeyStorePass());
			return crypto.encrypt(value, true);
		} catch (Exception e) {
			LOG.error("Error while decrypt token", e);
			return null;
		}
	}

	@Override
	public String md5Hashing(Credential credential) {
		MD5PasswordEncoder encoder = new MD5PasswordEncoder();
		return encoder.encodePassword(credential.getPassword(), credential.getUserName());
	}

	private String getCertificateInfo(ISystemParameters systemParameters, String integrationSource) {
		String certificate;
		if (integrationSource.equals(IntegMessagesSource.MP_CLEAR)) {
			certificate = systemParameters.getMPClearCertificateAlias();
		} else if (integrationSource.equals(IntegMessagesSource.E_PAY)) {
			certificate = systemParameters.getEPayCertificateAlias();
		} else if (integrationSource.equals(IntegMessagesSource.E_FAWATERCOM)) {
			certificate = systemParameters.getEfawatercomCertificateAlias();
		} else {
			certificate = systemParameters.getCertificatePSP();
		}
		return certificate;
	}

	@Override
	public boolean verifySign(String signature, String value, String publicKey) {
		try {
			PublicKey key = getPublicKey(publicKey);
			Signature sig = Signature.getInstance("SHA256withRSA");
			sig.initVerify(key);
			sig.update(value.getBytes(Charset.forName("UTF-8")));
			return sig.verify(Base64.getDecoder().decode(signature));
		} catch (Exception e) {
			throw new MPayCryptographicException(e);
		}
	}
	private PublicKey getPublicKey(String publicKey) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		if (publicKey.contains("-----BEGIN RSA PUBLIC KEY-----")) {
			PEMParser pemParser = new PEMParser(new StringReader(publicKey));
			SubjectPublicKeyInfo subjPubKeyInfo = (SubjectPublicKeyInfo) pemParser.readObject();
			RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(subjPubKeyInfo);
			RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent());
			KeyFactory kf = KeyFactory.getInstance("RSA");
			return kf.generatePublic(rsaSpec);
		} else {
			return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey)));
		}
	}
}
