package com.progressoft.mpay.registration.corporates.services;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.profiles.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class PreCreateCorporateService implements FunctionProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(PreCreateCorporateService.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        LOGGER.debug("Inside execute PreCreateCorporateService");
        MPAY_CorpoarteService corporateService = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
        corporateService.setRetryCount(0L);
        corporateService.setIsBlocked(false);
        corporateService.setIsRegistered(false);
        corporateService.setIsActive(true);
        corporateService.setNotificationChannel(MPayContext.getInstance().getLookupsLoader().getNotificationChannel(NotificationChannelsCode.SMS));

    }

}