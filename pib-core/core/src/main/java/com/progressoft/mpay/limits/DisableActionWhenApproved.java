package com.progressoft.mpay.limits;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;
import com.progressoft.mpay.entity.MPAYView;
public class DisableActionWhenApproved implements Condition {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		String viewName = (String) transientVar.get(WorkflowService.WF_ARG_VIEW_NAME);
		if (viewName.equals(MPAYView.LIMITS.viewName)) {
			MPAY_Limit limit = (MPAY_Limit) transientVar.get(WorkflowService.WF_ARG_BEAN);
			if (limit.getRefScheme() != null && "102105".equals(limit.getRefScheme().getStatusId().getCode()))
				return false;
		} else if (viewName.equals(MPAYView.LIMITS_DETAILS.viewName)) {
			MPAY_LimitsDetail limit = (MPAY_LimitsDetail) transientVar.get(WorkflowService.WF_ARG_BEAN);
			if (limit.getRefLimit() != null && limit.getRefLimit().getRefScheme() != null && "102105".equals(limit.getRefLimit().getRefScheme().getStatusId().getCode()))
				return false;
		}

		return true;
	}
}