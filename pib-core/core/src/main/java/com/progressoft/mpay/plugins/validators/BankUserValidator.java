package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_BanksUser;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;

public class BankUserValidator {

	private static final Logger logger = LoggerFactory.getLogger(BankUserValidator.class);

	private BankUserValidator() {

	}

	public static ValidationResult validate(MPAY_BanksUser bankUser) {
		logger.debug("Inside Validate ...");
		if (bankUser == null)
			return new ValidationResult(ReasonCodes.INCORRECT_USERNAME_OR_PASSWORD, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
