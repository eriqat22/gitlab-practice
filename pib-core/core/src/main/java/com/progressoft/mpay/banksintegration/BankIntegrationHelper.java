package com.progressoft.mpay.banksintegration;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entity.BankType;
import com.progressoft.mpay.exceptions.InvalidArgumentException;

public class BankIntegrationHelper {

	private static final Logger logger = LoggerFactory.getLogger(BankIntegrationHelper.class);
	private BankIntegrationHelper() {

	}

	public static BankIntegrationProcessor getProcessor(MPAY_Bank bank) {
		logger.debug("Inside GetProcessor ....");
		if (bank == null)
			throw new NullArgumentException("bank");
		if(bank.getSettlementParticipant().equals(BankType.SETTLEMENT))
			return null;
		if (bank.getProcessor() == null || "".equals(bank.getProcessor().trim()))
			throw new InvalidArgumentException("bank.getProcessor() is null or empty");

		try {
			Class<?> cls = Class.forName(bank.getProcessor());
			return (BankIntegrationProcessor) cls.newInstance();
		} catch (Exception e) {
			logger.error("Error while getting bank integratin processor", e);
			return null;
		}
	}
}
