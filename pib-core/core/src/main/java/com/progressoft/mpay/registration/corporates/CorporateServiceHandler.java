package com.progressoft.mpay.registration.corporates;

import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AccountCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.MPayContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

public class CorporateServiceHandler implements ChangeHandler {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(CorporateServiceHandler.class);

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside CorporateServiceHandler*******************-----------------");
        try {

            MPAY_CorpoarteService service = (MPAY_CorpoarteService) changeHandlerContext.getEntity();

            CorporatesChangeHandler handler = (CorporatesChangeHandler) Class.forName(MPayContext.getInstance().getSystemParameters().getCorporatesChangeHandler()).newInstance();
            handler.handleCorporateService(changeHandlerContext);

            handleProvidedServices(changeHandlerContext);

            MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "code", MPayContext.getInstance().getSystemParameters().getDefaultBankCode());
            service.setBank(defaultBank);
            changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.MOBILE_NUMBER).setRequired(service.getEnableSMS());
            changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.EMAIL).setRequired(service.getEnableEmail());
        } catch (Exception e) {
            logger.error("Failed to create change handler instance", e);
            throw new InvalidInputException(e.getMessage());
        }
    }

    private void handleProvidedServices(ChangeHandlerContext changeHandlerContext) {
        MPAY_CorpoarteService service = (MPAY_CorpoarteService) changeHandlerContext.getEntity();
        MPAY_AccountCategory category = service.getCategory();
        if (Objects.nonNull(category)) {
            boolean isAgent = category.getCode().equals(AccountCategory.AGENT);
            changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.SERVICES).setVisible(isAgent);
        }
    }
}
