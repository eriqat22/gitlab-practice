package com.progressoft.mpay.templates;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.bussinessobject.core.IJFWEntity;
import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.progressoft.mpay.templates.TemplateBehaviour.*;

public class KycTemplateHandler {
    private final IJFWEntity entity;

    public <T1 extends IJFWEntity> KycTemplateHandler(T1 entity) {
        this.entity = entity;
    }

    public <T extends JFWLookableEntity> void handleTemplateOf(T targetTemplate, TemplateBehaviour behaviour, ChangeHandlerContext context) {
        try {
            Field[] declaredFields = entity.getClass().getDeclaredFields();
            List<Field> fields = excludeConstants(declaredFields);
            Map<String, Method> methodsFields = mapFieldsWithMethods(targetTemplate, behaviour, fields);

            for (Map.Entry<String, Method> entry : methodsFields.entrySet()) {
                Boolean returnValue = (Boolean) entry.getValue().invoke(targetTemplate);
                if (VISIBLE.equals(behaviour))
                    context.getFieldsAttributes().get(entry.getKey()).setVisible(returnValue);
                else if (ENABLED.equals(behaviour))
                    context.getFieldsAttributes().get(entry.getKey()).setEnabled(returnValue);
                else if (REQ.equals(behaviour))
                    context.getFieldsAttributes().get(entry.getKey()).setRequired(returnValue);
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private <T extends JFWLookableEntity> Map<String, Method> mapFieldsWithMethods(T targetTemplate, TemplateBehaviour type, List<Field> fields) {
        Map<String, Method> methodsFields = new HashMap<>();

        List<Method> methodList = Arrays.stream(targetTemplate.getClass().getDeclaredMethods())
                .filter(isMethodWithTypeOf(type))
                .collect(Collectors.toList());

        for (Field field : fields) {
            methodList.stream()
                    .filter(matchesField(field, type))
                    .forEach(method -> methodsFields.put(field.getName(), method));
        }

        return methodsFields;
    }

    private Predicate<Method> matchesField(Field field, TemplateBehaviour type) {
        return m -> m.getName().replace("get", "").toLowerCase()
                .equalsIgnoreCase(field.getName() + type.name());
    }

    private List<Field> excludeConstants(Field[] fields) {
        return Arrays.stream(fields).filter(f -> Character.isLowerCase(f.getName().charAt(0))).collect(Collectors.toList());
    }

    private Predicate<Method> isMethodWithTypeOf(TemplateBehaviour type) {
        return m -> (m.getName().startsWith("get") &&
                m.getName().toLowerCase().endsWith(type.name().toLowerCase()));
    }
}
