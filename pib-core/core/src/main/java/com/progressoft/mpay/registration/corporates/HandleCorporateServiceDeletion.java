package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class HandleCorporateServiceDeletion implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(HandleCorporateServiceDeletion.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute ====================");

		MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (service.getIsRegistered())
			JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.DELETE_APPROVAL, new WorkflowException());
		else {
			handleServiceAccounts(service);
			JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.DELETE, new WorkflowException());
		}
	}

	private void handleServiceAccounts(MPAY_CorpoarteService service) throws WorkflowException {
		Iterator<MPAY_ServiceAccount> accounts = service.getServiceServiceAccounts().listIterator();
		if (accounts.hasNext()) {
			MPAY_ServiceAccount account = accounts.next();
			if (account.getDeletedFlag() == null || !account.getDeletedFlag())
				JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.DELETE, new WorkflowException());
		}
	}
}