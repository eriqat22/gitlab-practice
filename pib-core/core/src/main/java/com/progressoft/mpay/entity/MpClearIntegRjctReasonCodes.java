package com.progressoft.mpay.entity;

public class MpClearIntegRjctReasonCodes {
    public static final String VALID = "0";
    public static final String INVALID_SIGNATURE = "1";
    public static final String UNABLE_TO_PARSE = "2";
    public static final String INVALID_SOURCE = "3";
    public static final String INVALID_SENDER_ID = "4";
    public static final String INVALID_RESPONSE_CODE = "5";
    public static final String DUPLICATE_MESSAGE_ID = "6";
    public static final String INVALID_ORIGINAL_MESSAGE_ID = "7";
    public static final String INVALID_ORIGINAL_MESSAGE_STATUS = "8";
    public static final String ORIGINAL_MESSAGE_ALREADY_REPLIED = "9";
    public static final String INVALID_CUSTOMER_STATUS = "10";
    public static final String INVALID_MOBILE_STATUS = "11";
    public static final String NETWORK_MANAGEMENT_REQUEST_ALREADY_REPLIED = "12";
    public static final String INVALID_NETWORK_MANAGEMENT_REQUEST_STATUS = "13";
    public static final String UNSUPPORTED_MESSAGE_TYPE = "14";
    public static final String INVALID_PAYMENT_REQUEST_STATUS = "15";
    public static final String INVALID_CORPORATE_STATUS = "16";
    public static final String INVALID_SERVICE_STATUS = "17";

    private MpClearIntegRjctReasonCodes() {
    }

}
