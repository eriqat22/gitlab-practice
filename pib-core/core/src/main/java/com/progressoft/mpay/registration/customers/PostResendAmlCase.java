package com.progressoft.mpay.registration.customers;

import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.ServiceUserManager;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_AmlCase;
import com.progressoft.mpay.entities.MPAY_BulkPayment;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;

public class PostResendAmlCase implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostResendAmlCase.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside PostAcceptAmlCase--------------------------");

		MPAY_AmlCase amlCase = (MPAY_AmlCase) transientVars.get(WorkflowService.WF_ARG_BEAN);

		MPayRequest request = new MPayRequest(MPayContext.getInstance().getSystemParameters().getAmlEndPointOperation(),
				amlCase.getMobileNumber(), ReceiverInfoType.MOBILE, null,
				LanguageMapper.getInstance()
						.getLanguageLongByLocaleCode(SystemParameters.getInstance().getSystemLanguage().getCode()),
				UUID.randomUUID().toString(), amlCase.getTenantId());

		MessageProcessingContext context = new MessageProcessingContext(DataProvider.instance(),
				LookupsLoader.getInstance(), SystemParameters.getInstance(), new MPayCryptographer(),
				new NotificationHelper());
		context.setRequest(request);
		context.setOriginalRequest(request.toString());
		context.setOperation(LookupsLoader.getInstance().getEndPointOperation(request.getOperation()));
		context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
		MessageProcessor processor = MessageProcessorHelper.getProcessor(context.getOperation());
		if (processor == null)
			throw new WorkflowException("AML processor not available");
		MPAY_BulkPayment bulkPayment = context.getDataProvider()
				.getBulkPayment(context.getSystemParameters().getBulkPaymentDefaultId());
		if (bulkPayment == null)
			throw new WorkflowException("Default Bulk Payment ID Not Found");
		MPAY_MPayMessage message = MessageProcessorHelper.createMessage(new ServiceUserManager(), context,
				ProcessingStatusCodes.PENDING, null, null, bulkPayment);
		DataProvider.instance().persistMPayMessage(message);
		context.setMessage(message);
		MessageProcessingResult result = processor.processMessage(context);
		if(result.getStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED)) {
			JfwHelper.executeAction(MPAYView.AML_CASES, amlCase, "SVC_Close", new WorkflowException());
		}
	}
}
