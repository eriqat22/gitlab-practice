package com.progressoft.mpay.bulkregistration.function;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.bulkregistration.process.IBulkRegestrationProcessor;
import com.progressoft.mpay.entities.MPAY_BulkRegistration;

@SuppressWarnings("rawtypes")
public class CreateBulkRegistrationFileCreate implements FunctionProvider {

	@Autowired
	private IBulkRegestrationProcessor processor;

	private static final Logger logger = LoggerFactory.getLogger(CreateBulkRegistrationFileCreate.class);

	@Override
	public void execute(Map transientVar, Map args, PropertySet propertySet) throws InvalidInputException {
		MPAY_BulkRegistration bulkRegistration = (MPAY_BulkRegistration) transientVar.get(WorkflowService.WF_ARG_BEAN);
		logger.debug("Inside  excute CreateBulkRegistrationFileCreate ...");
		try {
			processor.process(bulkRegistration);

		} catch (Exception e) {
			logger.debug("Error While excute CreateBulkRegistrationFileCreate",e);
			InvalidInputException cause = new InvalidInputException();
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, e.getMessage());
			throw cause;
		}
	}
}