package com.progressoft.mpay.entity;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_CustomerMobile;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class CustomerMobileMetaData {
	private String mobileNumber;
	private long operatorId;
	private String nfcSerial;
	private String notificationShowType;

	public CustomerMobileMetaData() {
		//Default
	}

	public CustomerMobileMetaData(MPAY_CustomerMobile mobile) {
		if (mobile == null)
			throw new NullArgumentException("mobile");

		this.mobileNumber = mobile.getMobileNumber();
		this.nfcSerial = mobile.getNfcSerial();
		this.notificationShowType = mobile.getNotificationShowType();
	}

	public void fillCustomerMobileFromMetaData(MPAY_CustomerMobile mobile) {
		if (mobile == null)
			throw new NullArgumentException("mobile");

		mobile.setMobileNumber(this.mobileNumber);
		mobile.setNfcSerial(this.getNfcSerial());
		mobile.setNotificationShowType(this.notificationShowType);
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public long getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(long operatorId) {
		this.operatorId = operatorId;
	}

	public String getNfcSerial() {
		return nfcSerial;
	}

	public void setNfcSerial(String nfcSerial) {
		this.nfcSerial = nfcSerial;
	}

	public String getNotificationShowType() {
		return notificationShowType;
	}

	public void setNotificationShowType(String notificationShowType) {
		this.notificationShowType = notificationShowType;
	}

	@Override
	public String toString() {
		JSONSerializer serializer = new JSONSerializer().exclude("*.class");
		return serializer.serialize(this);
	}

	public static CustomerMobileMetaData fromJson(String data) {
		JSONDeserializer<CustomerMobileMetaData> deserializer = new JSONDeserializer<>();
		return deserializer.deserialize(data, CustomerMobileMetaData.class);
	}
}
