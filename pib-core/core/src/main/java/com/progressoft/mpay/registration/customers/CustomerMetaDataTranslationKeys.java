package com.progressoft.mpay.registration.customers;

public class CustomerMetaDataTranslationKeys extends CommonMetaDataTranslationKeys {
	public static final String FIRST_NAME = "CustomerFirstName";
	public static final String MIDDLE_NAME = "CustomerMiddleName";
	public static final String LAST_NAME = "CustomerLastName";
	public static final String DATE_OF_BIRTH = "CustomerDateOfBirth";
	public static final String KYC_TEMPLATES = "CustomerKycTemplate";
}
