package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class EnableShowChanges implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(EnableShowChanges.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object sender = transientVar.get(WorkflowService.WF_ARG_BEAN);

		if (sender instanceof MPAY_MobileAccount)
			return checkMobileAccount((MPAY_MobileAccount) sender);
		else if (sender instanceof MPAY_CustomerMobile)
			return checkMobile((MPAY_CustomerMobile) sender);
		else if (sender instanceof MPAY_Customer)
			return checkCustomer((MPAY_Customer) sender);
		else if (sender instanceof MPAY_ServiceAccount)
			return checkServiceAccount((MPAY_ServiceAccount) sender);
		else if (sender instanceof MPAY_CorpoarteService)
			return checkService((MPAY_CorpoarteService) sender);
		else if (sender instanceof MPAY_Corporate)
			return checkCorporate((MPAY_Corporate) sender);

		return false;
	}

	private boolean checkCorporate(MPAY_Corporate corporate) {
		logger.debug("inside checkCorporate ------------");
		return corporate.getApprovedData() != null;
	}

	private boolean checkService(MPAY_CorpoarteService service) {
		logger.debug("inside checkService ------------");
		return service.getApprovedData() != null;
	}

	private boolean checkServiceAccount(MPAY_ServiceAccount account) {
		logger.debug("inside checkServiceAccount ------------");
		return account.getApprovedData() != null;
	}

	private boolean checkMobileAccount(MPAY_MobileAccount account) {
		logger.debug("inside checkMobileAccount ------------");
		return account.getApprovedData() != null;
	}

	private boolean checkMobile(MPAY_CustomerMobile mobile) {
		logger.debug("inside checkMobile ------------");
		return mobile.getApprovedData() != null;
	}

	private boolean checkCustomer(MPAY_Customer customer) {
		logger.debug("inside checkCustomer ------------");
		return customer.getApprovedData() != null;
	}
}