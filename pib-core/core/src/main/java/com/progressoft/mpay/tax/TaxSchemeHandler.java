package com.progressoft.mpay.tax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_TaxScheme;

public class TaxSchemeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(TaxSchemeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {

		MPAY_TaxScheme scheme = (MPAY_TaxScheme) changeHandlerContext.getEntity();
		logger.debug("TaxSchemeHandler  ----------------------------");

		scheme.setCurrency(SystemParameters.getInstance().getDefaultCurrency());

	}
}
