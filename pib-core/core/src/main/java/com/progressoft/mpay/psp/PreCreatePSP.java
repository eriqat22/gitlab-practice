package com.progressoft.mpay.psp;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Corporate;

public class PreCreatePSP implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PreCreatePSP.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("\n\r===inside PostCreatePSP....");
		MPAY_Corporate psp = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		psp.setIsRegistered(true);
		psp.setMaxNumberOfDevices(0L);
	}
}
