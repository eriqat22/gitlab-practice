package com.progressoft.mpay.registration.customers;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;

@SuppressWarnings("rawtypes")
public class SetCustomerDeviceFlagToFalse implements FunctionProvider {
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		MPAY_CustomerDevice customerDevice = (MPAY_CustomerDevice) transientVars.get(WorkflowService.WF_ARG_BEAN);
		customerDevice.setActiveDevice(false);
	}
}