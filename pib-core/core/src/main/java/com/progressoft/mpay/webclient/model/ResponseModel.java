
package com.progressoft.mpay.webclient.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.ExtraDataComparer;

import java.util.Iterator;

public class ResponseModel {
//    public static final Creator<ResponseModel> CREATOR = new Creator<ResponseModel>() {
//        public ResponseModel createFromParcel(Parcel source) {
//            return new ResponseModel(source);
//        }
//
//        public ResponseModel[] newArray(int size) {
//            return new ResponseModel[size];
//        }
//    };
    @SerializedName("response")
    private ResponseFieldsModel response;
    @SerializedName("token")
    private String token;

    public static ResponseModel createResponse(String json) {
        Gson gson = (new GsonBuilder()).create();
        return (ResponseModel)gson.fromJson(json, ResponseModel.class);
    }

    public ResponseFieldsModel getResponse() {
        return this.response;
    }

    public void setResponse(ResponseFieldsModel response) {
        this.response = response;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String buildSecureHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.isStringNull(this.response.getDesc()));
        sb.append(this.isStringNull(this.response.getErrorCd()));
        sb.append(this.response.getRef());
        sb.append(this.response.getStatusCode());
        ExtraDataComparer comparer = new ExtraDataComparer();
        if (this.response.getExtraData() != null && this.response.getExtraData().size() > 0) {
            this.response.getExtraData().sort(comparer);
            Iterator var3 = this.response.getExtraData().iterator();

            while(var3.hasNext()) {
                ExtraData data = (ExtraData)var3.next();
                sb.append(this.isStringNull(data.getValue()));
            }
        }

        return sb.toString();
    }

    private String isStringNull(String value) {
        return value == null ? "" : value;
    }


}
