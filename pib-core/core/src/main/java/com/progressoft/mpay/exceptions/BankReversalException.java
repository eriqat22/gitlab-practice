package com.progressoft.mpay.exceptions;

public class BankReversalException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public BankReversalException(Exception ex) {
        super(ex);
    }

    public BankReversalException(String message) {
        super(message);
    }

}
