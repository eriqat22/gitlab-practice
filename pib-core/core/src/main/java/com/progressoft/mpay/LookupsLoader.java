package com.progressoft.mpay;

import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPClearCache;
import com.progressoft.mpay.entity.PSPCache;

import java.util.List;
import java.util.stream.Collectors;

public class LookupsLoader implements ILookupsLoader {

    private static ILookupsLoader instance;
    LookupsCache lookupsCache = null;

    public LookupsLoader() {
        lookupsCache = AppContext.getApplicationContext().getBean("LookupsCache", LookupsCache.class);
    }

    public static ILookupsLoader getInstance() {
        if (instance == null)
            instance = new LookupsLoader();

        return instance;
    }

    @Override
    public MPAY_City getCity(String code) {
        return lookupsCache.getCity(code);
    }

    @Override
    public MPAY_City getCityById(long id) {
        return lookupsCache.getCityById(id);
    }

    @Override
    public List<MPAY_City> getCitiesByName(String name) {
        return lookupsCache.getCityByName(name);
    }

    @Override
    public MPAY_ClientType getClientType(String code) {
        return lookupsCache.getClientType(code);
    }

    @Override
    public List<MPAY_ClientType> listClientTypes() {
        return lookupsCache.listClientTypes();
    }

    @Override
    public MPAY_IDType getIDType(String code) {
        return lookupsCache.getIDType(code);
    }

    @Override
    public MPAY_ChannelType getChannelType(String code) {
        return lookupsCache.getChannelType(code);
    }

    @Override
    public JfwCountry getCountry(String code) {
        return lookupsCache.getCountry(code);
    }

    @Override
    public JfwCountry getCountryByIsoCode(String code) {
        return lookupsCache.getCountryByIsoCode(code);
    }

    @Override
    public JFWCurrency getCurrency(String code) {
        return lookupsCache.getCurrency(code);
    }

    @Override
    public MPAY_MpClearIntegMsgType getMpClearIntegrationMessageTypes(String code) {
        return lookupsCache.getMpClearIntegrationMessageTypes(code);
    }

    @Override
    public MPAY_CustIntegActionType getCustomerIntegrationActionTypes(String code) {
        return lookupsCache.getCustomerIntegrationActionTypes(code);
    }

    @Override
    public MPAY_MpClearIntegRjctReason getMpClearIntegrationReasons(String code) {
        return lookupsCache.getMpClearIntegrationReasons(code);
    }

    @Override
    public MPAY_MpClearResponseCode getMpClearIntegrationResponseCodes(String code) {
        return lookupsCache.getMpClearIntegrationResponseCodes(code);
    }

    @Override
    public MPAY_NetworkManActionType getNetworkManagementActionTypes(String code) {
        return lookupsCache.getNetworkManagementActionTypes(code);
    }

    @Override
    public MPAY_AccountType getAccountTypes(String code) {
        return lookupsCache.getAccountTypes(code);
    }

    @Override
    public MPAY_JvType getJvTypes(String code) {
        return lookupsCache.getJvTypes(code);
    }

    @Override
    public MPAY_MessageType getMessageTypes(String code) {
        return lookupsCache.getMessageTypes(code);
    }

    @Override
    public MPAY_MessageType getMessageTypes(long id) {
        return lookupsCache.getMessageTypes(id);
    }

    @Override
    public MPAY_Reason getReason(String code) {
        return lookupsCache.getReason(code);
    }

    @Override
    public MPAY_SysConfig getSystemConfigurations(String key) {
        return lookupsCache.getSystemConfigurations(key);
    }

    @Override
    public List<MPAY_SysConfig> listSystemConfigurations() {
        return lookupsCache.listSystemConfigurations();
    }

    @Override
    public List<MPAY_City> listCities() {
        return lookupsCache.listCities();
    }

    @Override
    public MPAY_TransactionsConfig getTransactionsConfig(long clientType, long messageType, String bankedUnbanked, boolean isSender) {
        return lookupsCache.getTransactionsConfig(clientType, messageType, bankedUnbanked, isSender);
    }

    @Override
    public List<MPAY_MessageType> getMessageTypes() {
        return lookupsCache.listMessageTypes();
    }

    @Override
    public MPAY_Bank getBank(String name) {
        return lookupsCache.getBank(name);
    }

    @Override
    public List<MPAY_Bank> listBanks() {
        return lookupsCache.listBanks();
    }

    @Override
    public MPAY_Bank getBank(long id) {
        return lookupsCache.getBank(id);
    }

    @Override
    public MPAY_Reg_File_Stt getRegFileStatus(String code) {
        if (code == null)
            return null;
        return lookupsCache.getRegFileStatus(code);
    }

    @Override
    public MPAY_Reg_Instrs_Stt getRegInstrStatus(String code) {
        if (code == null)
            return null;
        return lookupsCache.getRegInstrStatus(code);
    }

    @Override
    public MPAY_ePayStatusCode getePayStatusCode(String epayCode) {
        return lookupsCache.getePayStatusCode(epayCode);
    }

    @Override
    public JFWCurrency getDefaultCurrency() {
        return lookupsCache.getDefaultCurrency();
    }

    @Override
    public List<MPAY_ChannelType> listChannelTypes() {
        return lookupsCache.listChannelTypes();
    }

    @Override
    public List<MPAY_MessageType> listMessageTypes() {
        return lookupsCache.listMessageTypes();
    }

    @Override
    public MPAY_MessageType getMessageTypeByMPClearCode(String code) {
        return lookupsCache.getMessageTypeByMPClearCode(code);
    }

    @Override
    public MPAY_Reason getReasonByMPClearCode(String code) {
        return lookupsCache.getReasonByMPClearCode(code);
    }

    @Override
    public PSPCache getPSP() {
        return lookupsCache.getPSP();
    }

    @Override
    public MPClearCache getMPClear() {
        return lookupsCache.getMPClear();
    }

    @Override
    public MPAY_NotificationTemplate getNotificationTemplate(long operationId, long typeId, long languageId) {
        return lookupsCache.getNotificationTemplate(operationId, typeId, languageId);
    }

    @Override
    public MPAY_NotificationTemplate getNotificationTemplateByOperationName(String operationName, long typeId, long languageId) {
        return lookupsCache.getNotificationTemplateByOperationName(operationName, typeId, languageId);
    }

    @Override
    public MPAY_BankIntegStatus getBankIntegStatus(String code) {
        return lookupsCache.getBankIntegStatus(code);
    }

    @Override
    public MPAY_Language getLanguage(long id) {
        return lookupsCache.getLanguage(id);
    }

    @Override
    public MPAY_Language getLanguageByCode(String code) {
        return lookupsCache.getLanguageByCode(code);
    }

    @Override
    public MPAY_EndPointOperation getEndPointOperation(String operation) {
        return lookupsCache.getEndPointOperation(operation);
    }

    @Override
    public MPAY_ProcessingStatus getProcessingStatus(String code) {
        return lookupsCache.getProcessingStatus(code);
    }

    @Override
    public MPAY_TransactionConfig getTransactionConfig(long senderType, boolean isSenderBanked, long receiverType, boolean isReceiverBanked, long messageType, String transactionTypeCode,
                                                       long operationId) {
        return lookupsCache.getTransactionConfig(senderType, isSenderBanked, receiverType, isReceiverBanked, messageType, transactionTypeCode, operationId);
    }

    @Override
    public MPAY_TransactionConfig getTransactionConfig(long senderType, boolean isSenderBanked, long receiverType, boolean isReceiverBanked, long messageType, String transactionTypeCode) {
        return lookupsCache.getTransactionConfig(senderType, isSenderBanked, receiverType, isReceiverBanked, messageType, transactionTypeCode);
    }

    @Override
    public MPAY_ClientType getCustomerClientType() {
        return lookupsCache.getCustomerClientType();
    }

    @Override
    public MPAY_ClientType getCorporateClientType(String clientCode) {
        List<MPAY_ClientType> collect = lookupsCache.getCorporateClientType().stream().filter(c -> c.getCode().equals(clientCode)).collect(Collectors.toList());
        return collect.size() > 0 ? collect.get(0) : null;
    }

    @Override
    public MPAY_TransactionDirection getTransactionDirection(long id) {
        return lookupsCache.getTransactionDirection(id);
    }

    @Override
    public List<MPAY_LimitsType> listLimitsTypes() {
        return lookupsCache.listLimitsTypes();
    }

    @Override
    public MPAY_TransactionType getTransactionType(String code) {
        return lookupsCache.getTransactionType(code);
    }

    @Override
    public MPAY_Reasons_NLS getReasonNLS(long reasonId, String languageCode) {
        return lookupsCache.getReasonNLS(reasonId, languageCode);
    }

    @Override
    public MPAY_MessageType getFirstFinancialMessageType() {
        return lookupsCache.getFirstFinancialMessageType();
    }

    @Override
    public MPAY_NotificationChannel getNotificationChannel(String code) {
        return lookupsCache.getNotificationChannel(code);
    }

    @Override
    public MPAY_ServiceType getServiceType(String code) {
        return lookupsCache.getServiceType(code);
    }

    @Override
    public MPAY_ServicesCategory getServiceCategory(String code) {
        return lookupsCache.getServiceCategory(code);
    }

    @Override
    public MPAY_ServicesCategory getServiceCategory(long id) {
        return lookupsCache.getServiceCategory(id);
    }

    @Override
    public MPAY_Bank getBankByCode(String code) {
        return lookupsCache.getBankByCode(code);
    }

    @Override
    public MPAY_Profile getProfile(String code) {
        return lookupsCache.getProfile(code);
    }

    @Override
    public MPAY_Profile getProfile(long id) {
        return lookupsCache.getProfile(id);
    }

    @Override
    public MPAY_ServiceIntegReason getServiceIntegReason(String code) {
        return lookupsCache.getServiceIntegReason(code);
    }

    @Override
    public MPAY_ServiceIntegReasons_NLS getServiceIntegReasonNLS(long reasonId, long languageId) {
        return lookupsCache.getServiceIntegReasonNLS(reasonId, languageId);
    }

    @Override
    public MPAY_AppsChecksum getAppChecksum(String code) {
        return lookupsCache.getAppChecksum(code);
    }

    @Override
    public MPAY_BalanceType getBalanceType(String code) {
        return lookupsCache.getBalanceType(code);
    }

    @Override
    public MPAY_TransactionSize getTransactionSizeByCode(String code) {
        return lookupsCache.getTransactionSize(code);
    }

    @Override
    public MPAY_CorpLegalEntity getLegalEntityByCode(String code) {
        return lookupsCache.getCorpLegalEntity(code);
    }

    @Override
    public MPAY_NotificationService getNotifictionService(String code) {
        return lookupsCache.getNotificationService(code);
    }

    @Override
    public List<MPAY_Language> getLanguages() {
        return lookupsCache.getLanguges();
    }

    @Override
    public MPAY_CityArea getAreaByCode(String areaCode) {
        return lookupsCache.getAreaByCode(areaCode);
    }

    @Override
    public MPAY_AccountCategory getAccountCategory(long id) {
        return lookupsCache.getAccountCategory(id);
    }

    @Override
    public MPAY_CustomerKyc getCustomerKyc(long id) {
        return lookupsCache.getCustomerKyc(id);
    }

    @Override
    public MPAY_CorporateKyc getCorporateKyc(long id) {
        return lookupsCache.getCorporateKyc(id);
    }

    @Override
    public MPAY_CorporateKyc getCorporateKycByCode(String code) {
        return lookupsCache.getCorporateKyc("code");
    }

    @Override
    public JfwCountry getCountryByDescription(String code) {
        return lookupsCache.getCountryByDescription(code);
    }


}
