package com.progressoft.mpay.payments.cashin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ServiceCashIn;

public class ServiceCashInChangeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(ServiceCashInChangeHandler.class);
	private static final String CALCULATION_STEP = "100506";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handle =====================");
		MPAY_ServiceCashIn cashIn = (MPAY_ServiceCashIn) changeHandlerContext.getEntity();
		cashIn.setCurrency(SystemParameters.getInstance().getDefaultCurrency());
		if (cashIn.getStatusId() != null && CALCULATION_STEP.equals(cashIn.getStatusId().getCode())) {
			changeHandlerContext.getFieldsAttributes().get("refServiceAccount").setVisible(false);
		} else {
			changeHandlerContext.getFieldsAttributes().get("refServiceAccount").setVisible(true);
		}
	}
}