package com.progressoft.mpay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.jfw.integration.ListEntitiesResult;
import com.progressoft.jfw.integration.camel.message.NotificationMessage;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.shared.Operator;

public class GetNetworkMangementStatus implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(GetNetworkMangementStatus.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		String jfwViewName = "MPAY_NetworkManagements.View";
		String tenantId = "serviceTenant";
		ProducerTemplate producer = exchange.getContext().createProducerTemplate();
		String jfwCamelComUri = "jfw:listEntities?viewName=" + jfwViewName + "&username=service_user&tenantId=" + tenantId + "&orgShortName=*";

		List<Filter> filters = new ArrayList<>();

		Filter jfwOutEntityFilter = new Filter();
		jfwOutEntityFilter.setProperty("operationType.code");
		jfwOutEntityFilter.setOperator(Operator.EQ);
		jfwOutEntityFilter.setFirstOperand("LOGIN");
		filters.add(jfwOutEntityFilter);
		Map<String, Object> headers = new HashMap<>();
		headers.put("startPosition", 0);
		headers.put("maxCount", 1);
		Object response = producer.requestBodyAndHeaders(jfwCamelComUri, filters, headers);
		ListEntitiesResult list = (ListEntitiesResult) (response);
		List<NotificationMessage> messages = list.getResultedEntitiesInfo();
		if (messages.isEmpty()) {
			logger.error("returned size=0,view name=" + jfwViewName);
			exchange.setProperty("isLoggedIn", "false");
		} else {
			exchange.setProperty("isLoggedIn", "true");
		}
	}

}
