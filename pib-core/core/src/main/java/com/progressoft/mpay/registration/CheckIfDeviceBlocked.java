package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.registration.customers.CheckIfMobileBlocked;

public class CheckIfDeviceBlocked implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfMobileBlocked.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object device = transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (device instanceof MPAY_CustomerDevice) {
			return ((MPAY_CustomerDevice) device).getIsBlocked();
		} else {
			return ((MPAY_CorporateDevice) device).getIsBlocked();
		}
	}

}