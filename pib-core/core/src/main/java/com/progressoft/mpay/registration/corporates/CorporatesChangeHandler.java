package com.progressoft.mpay.registration.corporates;

import java.util.Collection;

import com.progressoft.mpay.MPayContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.registration.EffectiveStatusHelper;

public abstract class CorporatesChangeHandler {
    private static final Logger logger = LoggerFactory.getLogger(CorporatesChangeHandler.class);

    protected abstract void handleExtraCorporateFields(ChangeHandlerContext changeHandlerContext, MPAY_Corporate corporate);

    protected abstract void handleExtraServiceFields(ChangeHandlerContext changeHandlerContext, MPAY_CorpoarteService service);

    protected abstract void handleExtraAccountFields(ChangeHandlerContext changeHandlerContext, MPAY_ServiceAccount account);

    public void handleCorporate(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside handleCorporate*******************-----------------");

        MPAY_Corporate corporate = (MPAY_Corporate) changeHandlerContext.getEntity();
        try {

            WorkflowStatus status = EffectiveStatusHelper.getEffectiveStatus(corporate);
            if (status == null) {
                corporate.setIsActive(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.SERVICE_NAME).setRequired(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.SERVICE_DESCRIPTION).setRequired(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.MOBILE_NUMBER).setRegex(MPayContext.getInstance().getSystemParameters().getMobileNumberRegex());
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.BANKED_UNBANKED).setRequired(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.BANK).setRequired(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.MAS).setRequired(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.PAYMENT_TYPE).setRequired(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.REF_PROFILE).setRequired(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.SERVICE_CATEGORY).setRequired(true);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.SERVICE_TYPE).setRequired(true);
            } else {
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.SERVICE_NAME).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.SERVICE_DESCRIPTION).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.ALIAS).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.BANKED_UNBANKED).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.BANK).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.EXTERNAL_ACC).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.IBAN).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.MAS).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.PAYMENT_TYPE).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.REF_PROFILE).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.SERVICE_CATEGORY).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.SERVICE_TYPE).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.ENABLE_EMAIL).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.ENABLE_PUSH_NOTIFICATION).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.ENABLE_S_M_S).setEnabled(false);
            }
            if (corporate.getIsRegistered() != null && corporate.getIsRegistered()) {
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.CLIENT_TYPE).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.REGISTRATION_ID).setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get(MPAY_Corporate.REF_COUNTRY).setEnabled(false);
            }
        } catch (WorkflowException e) {
            throw new InvalidInputException(e);
        }
    }

    public void handleCorporateService(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside CompanyServiceHandler ***");

        MPAY_CorpoarteService service = (MPAY_CorpoarteService) changeHandlerContext.getEntity();

        Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
        for (UIPortletAttributes ui : portletsAttributes) {
            ui.setVisible(true);
        }

        if (service.getName() != null)
            service.setName(service.getName().toUpperCase());
        if (service.getAlias() != null)
            service.setAlias(service.getAlias().toUpperCase());
        if (service.getMpclearAlias() != null)
            service.setMpclearAlias(service.getMpclearAlias().toUpperCase());

        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.DESCRIPTION).setRequired(true);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.SERVICE_CATEGORY).setRequired(true);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.SERVICE_TYPE).setRequired(true);

        if (service.getStatusId() == null) {
            service.setIsActive(true);
            return;
        }

        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.BANKED_UNBANKED).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.BANK).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.EXTERNAL_ACC).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.IBAN).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.MAS).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.PAYMENT_TYPE).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.REF_PROFILE).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.MOBILE_NUMBER).setRegex(MPayContext.getInstance().getSystemParameters().getMobileNumberRegex());

        if (service.getIsRegistered()) {
            if (changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.REF_CORPORATE) != null)
                changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.REF_CORPORATE).setEnabled(false);
            changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.ALIAS).setEnabled(false);
        }
    }

    public void handleServiceAccount(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside ServiceAccountsViewChangeHandler ------------");

        MPAY_ServiceAccount serviceAccount = (MPAY_ServiceAccount) changeHandlerContext.getEntity();
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.IS_DEFAULT).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.IS_SWITCH_DEFAULT).setEnabled(false);
        if (serviceAccount.getStatusId() == null) {
            serviceAccount.setIsActive(true);
            return;
        }
        if (!serviceAccount.getIsRegistered())
            return;
        if (changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.SERVICE) != null)
            changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.SERVICE).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.BANK).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.EXTERNAL_ACC).setEnabled(true);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.BANKED_UNBANKED).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.REF_PROFILE).setEnabled(true);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.MAS).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.REF_ACCOUNT).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.IS_DEFAULT).setEnabled(false);
        changeHandlerContext.getFieldsAttributes().get(MPAY_ServiceAccount.IS_SWITCH_DEFAULT).setEnabled(false);
    }
}
