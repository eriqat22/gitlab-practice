package com.progressoft.mpay;

import java.security.Key;
import java.security.cert.X509Certificate;
import java.util.Calendar;

import com.opensymphony.workflow.WorkflowException;

public interface ICryptographer {

	String generateToken(ISystemParameters systemParameters, String message, String signingDate, String integrationSource);
	boolean verifyToken(ISystemParameters systemParameters, String message, String signature, Calendar signingDate, String integrationSource);
	String sign(String data);
	byte[] sign(byte[] data) throws WorkflowException;
	boolean verify(String data, String signature);
	boolean verify(byte[] data, byte[] signature);
	String encrypt(String data, boolean withPrivateKey);
	String decrypt(String data) throws WorkflowException;
	byte[] decrypt(byte[] data) throws WorkflowException;
	byte[] encrypt(byte[] data, boolean withPrivateKey);
	String getCertificateSerial() throws WorkflowException;
	String encryptCertificateSerial() throws WorkflowException;
	X509Certificate loadCertificate() throws WorkflowException;
	Key loadCertificatePrivateKey() throws WorkflowException;
	String createJsonToken(ISystemParameters systemParameters, String values);
	String decryptJsonToken(ISystemParameters systemParameters, String token);
	String encryptJsonToken(ISystemParameters systemParameters, String value);

	String md5Hashing(Credential credential);

    boolean verifySign(String token, String hash, String publicKey);
}
