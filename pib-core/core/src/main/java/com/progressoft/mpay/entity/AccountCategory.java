package com.progressoft.mpay.entity;


public class AccountCategory {
    public static final String AGENT = "1";
    public static final String MERCHANT = "2";
    public static final String CUSTOMER = "3";
    public static final String COMMISSION = "4";
}
