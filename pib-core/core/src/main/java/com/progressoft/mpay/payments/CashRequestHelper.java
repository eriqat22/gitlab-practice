package com.progressoft.mpay.payments;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.payments.cashout.PostServiceCashOutRequest;

public class CashRequestHelper {

	private static final Logger logger = LoggerFactory.getLogger(PostServiceCashOutRequest.class);

	private CashRequestHelper() {
	}

	public static String generateResponse(long reference, String processingCode, MPAY_Reason reason, MPAY_Language language) {
		MPayResponse response = new MPayResponse();
		response.setErrorCd(reason.getCode());
		String desc;

		if (reason.getReasonsNLS().size() == 0) {
			desc = reason.getDescription();
		} else {
			desc = reason.getReasonsNLS().get(language.getCode()).getDescription();
		}
		response.setRef(reference);
		response.setDesc(desc);
		response.setStatusCode(processingCode);
		String responseString = response.toString();
		logger.debug("Response: " + responseString);
		return responseString;
	}

	public static String prepareRefMobileAccount(String refMobileAccount) {
		if(refMobileAccount ==null)
			return null;
		if(refMobileAccount.contains("{value={value=")){
			String s = refMobileAccount.split(",")[0];
			return  s.replace("{value={value=","").trim();
		}
		if (refMobileAccount.contains("key") || refMobileAccount.contains("value")) {
			String[] split = refMobileAccount.split("=");
			return split[1].replace(",", "").replace("key", "").trim();
		}
		return refMobileAccount.trim();
	}
}
