package com.progressoft.mpay.templates;

public enum TemplateBehaviour {
    VISIBLE,
    ENABLED,
    REQ
}