package com.progressoft.mpay.mpclearinteg;

import com.progressoft.jfw.integration.camel.types.ComponentOption;
import com.progressoft.jfw.integration.comm.CommunicationConfig;
import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.jfw.integration.comm.JfwMessage;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.MessageSerializer;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.MpClearIntegMsg;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MPClearHelper {
	private static final String REASON_DESCRIPTION = ", reasonDescription: ";
	private static final String SYSTEM_PARAMETERS_KEY = "systemParameters";
	private static final String MESSAGE_KEY = "message";
	private static final String MMDD_HHMMSS = "MMddHHmmss";
	private static final Logger LOGGER = LoggerFactory.getLogger(MPClearHelper.class);

	private MPClearHelper() {
	}

	public static void setMessageEncoded(IsoMessage message) {
		LOGGER.debug("Inside setMessageEncoded ===============");
		if (message == null)
			throw new NullArgumentException(MESSAGE_KEY);
		message.setField(113, new IsoValue<>(IsoType.LLLVAR, "1", 1));
	}

	public static void setEncodedString(IsoMessage message, int fieldNumber, IsoType type, String value) {
		LOGGER.debug("Inside setEncodedString, fieldNumber: " + fieldNumber + ", type: " + type + ", value: " + value);
		if (message == null)
			throw new NullArgumentException(MESSAGE_KEY);
		byte[] bytes = value.getBytes(Charset.forName("UTF-16LE"));
		String base64String = Base64.encodeBase64String(bytes);
		message.setField(fieldNumber, new IsoValue<>(type, base64String, base64String.length()));
	}

	public static String getEncodedString(IsoMessage message, int fieldNumber) {
		LOGGER.debug("Inside getEncodedString, fieldNumber: " + fieldNumber);
		if (message == null)
			throw new NullArgumentException(MESSAGE_KEY);
		String fieldString = message.getField(fieldNumber).toString();
		Object isEncoded = message.getField(113);
		if (isEncoded == null || !"1".equals(isEncoded.toString()))
			return fieldString;

		byte[] bytes = Base64.decodeBase64(fieldString);
		return new String(bytes, Charset.forName("UTF-16LE"));
	}

	public static String getAccount(ISystemParameters systemParameters, MPAY_CustomerMobile mobile,
			MPAY_MobileAccount mobileAccount, boolean isAlias) {
		LOGGER.debug("Inside getAccount, mobile =============");
		if (systemParameters == null)
			throw new NullArgumentException(SYSTEM_PARAMETERS_KEY);
		if (mobile == null)
			throw new NullArgumentException("mobile");
		if (mobileAccount == null)
			throw new NullArgumentException("mobileAccount");
		boolean userAlias = isAlias && mobile.getAlias() != null;
		String formatMas = "%0" + systemParameters.getMobileAccountSelectorLength() + "d";
		return mobileAccount.getBank().getCode() + String.format(formatMas, mobileAccount.getMas())
				+ (userAlias ? ReceiverInfoType.ALIAS : ReceiverInfoType.MOBILE) + (userAlias ? mobile.getAlias()
						: SystemHelper.formatMobileNumber(systemParameters, mobile.getMobileNumber()));
	}

	public static String getAccount(ISystemParameters systemParameters, MPAY_CorpoarteService service,
			MPAY_ServiceAccount account, boolean isAlias) {
		LOGGER.debug("Inside getAccount, service =============");
		if (systemParameters == null)
			throw new NullArgumentException(SYSTEM_PARAMETERS_KEY);
		if (service == null)
			throw new NullArgumentException("service");
		if (account == null)
			throw new NullArgumentException("account");

		boolean useAlias = isAlias && service.getAlias() != null;
		String receiver = useAlias ? service.getAlias() : service.getName();
		String receiverType = useAlias ? ReceiverInfoType.ALIAS : ReceiverInfoType.CORPORATE;
		if (!StringUtils.isEmpty(service.getMpclearAlias())) {
			receiver = service.getMpclearAlias();
			receiverType = ReceiverInfoType.CORPORATE;
		}
		String formatMas = "%0" + systemParameters.getMobileAccountSelectorLength() + "d";
		return account.getBank().getCode() + String.format(formatMas, account.getMas()) + receiverType + receiver;
	}

	public static String getPSPAccount(ISystemParameters systemParameters, MPAY_CorpoarteService service,
			MPAY_ServiceAccount account) {
		LOGGER.debug("Inside getPSPAccount =============");
		if (systemParameters == null)
			throw new NullArgumentException(SYSTEM_PARAMETERS_KEY);
		if (service == null)
			throw new NullArgumentException("service");
		if (account == null)
			throw new NullArgumentException("account");

		String formatMas = "%0" + systemParameters.getMobileAccountSelectorLength() + "d";
		return account.getBank().getCode() + String.format(formatMas, account.getMas()) + "C"
				+ service.getMpclearAlias();
	}

	public static String getAccount(ISystemParameters systemParameters, String receiver, String receiverType) {
		LOGGER.debug("Inside getAccount, receiver: " + receiver + ", receiverType: " + receiverType);
		if (systemParameters == null)
			throw new NullArgumentException("systemParameters");
		String formatMas = "%0" + systemParameters.getMobileAccountSelectorLength() + "d";
		String mas = "0";

		if ("M".equals(receiverType)) {
			return getEmptyRoutingCode(systemParameters) + String.format(formatMas, Long.valueOf(mas)) + receiverType
					+ SystemHelper.formatMobileNumber(systemParameters, receiver);
		} else {
			return getEmptyRoutingCode(systemParameters) + String.format(formatMas, Long.valueOf(mas)) + receiverType
					+ receiver;
		}
	}

	private static String getEmptyRoutingCode(ISystemParameters systemParameters) {
		LOGGER.debug("Inside getEmptyRoutingCode =====================");
		String format = "%0" + systemParameters.getRoutingCodeLength() + "d";
		return String.format(format, 0);
	}

	public static String formatIsoDate(Date date) {
		LOGGER.debug("Inside formatIsoDate =====================");
		if (date == null)
			throw new NullArgumentException("date");
		return SystemHelper.formatDate(date, MMDD_HHMMSS);
	}

	private static Calendar getSigningTimestamp() {
		LOGGER.debug("Inside getSigningTimestamp =====================");
		Calendar date = SystemHelper.getCalendar();
		date.set(Calendar.MILLISECOND, 0);
		return date;
	}

	public static void sendToActiveMQ(MPAY_MpClearIntegMsgLog mpClearMessage, Communicator communicator) {
		LOGGER.debug("Inside sendToActiveMQ =====================");
		if (mpClearMessage == null)
			throw new NullArgumentException("mpClearMessage");
		if (communicator == null)
			throw new NullArgumentException("communicator");

		MpClearIntegMsg oMpClearIntegMsg = new MpClearIntegMsg();
		oMpClearIntegMsg.setContent(mpClearMessage.getContent());
		oMpClearIntegMsg.setRefSender(mpClearMessage.getRefSender());
		oMpClearIntegMsg.setSigningStamp(mpClearMessage.getSigningStamp());
		oMpClearIntegMsg.setToken(mpClearMessage.getToken());
		String messageBody = MessageSerializer.serialize(oMpClearIntegMsg);

		Map<String, String> headers = new HashMap<>();
		headers.put(ComponentOption.TENANT_ID.getName(), mpClearMessage.getTenantId());

		JfwMessage message = new JfwMessage();
		message.setBody(messageBody);
		message.setHeaders(headers);

		CommunicationConfig commConfig = new CommunicationConfig();
		commConfig.setRequestQueueName("mpay.mpclear.out");
		communicator.asyncSend(commConfig, message);
		mpClearMessage.setIsProcessed(true);
		DataProvider.instance().mergeMPClearMessageLog(mpClearMessage);
	}

	public static MPAY_MpClearIntegMsgLog createMPClearMessageLog(String content, String sender, String token,
			String date) throws ParseException {
		LOGGER.debug("Inside createMPClearMessageLog, content: " + content + ", sender: " + sender + ", token: " + token
				+ ", date: " + date);
		MPAY_MpClearIntegMsgLog msgLog = new MPAY_MpClearIntegMsgLog();
		msgLog.setContent(content);
		if (!SystemHelper.isNullOrEmpty(content)) {
			msgLog.setIntegMsgType(content.length() >= 4 ? content.substring(0, 4) : content);
		} else {
			msgLog.setIntegMsgType("");
		}
		msgLog.setSource(IntegMessagesSource.MP_CLEAR);
		msgLog.setContent(content);
		msgLog.setRefSender(sender);
		msgLog.setToken(token);
		msgLog.setSigningStamp(new Timestamp(SystemHelper.parseDate(date, "yyyy-MM-dd'T'HH:mm:ss").getTime()));
		msgLog.setIsProcessed(false);
		msgLog.setIsCanceled(false);
		msgLog.setResponseReceived(false);
		return msgLog;
	}

	public static MPAY_MpClearIntegMsgLog createMPClearMessageLog(ProcessingContext context, IsoMessage isoMessage,
			String integType) {
		LOGGER.debug("Inside createMPClearMessageLog =====================");
		String isoMessageContent = new String(isoMessage.writeData());
		MPAY_MpClearIntegMsgLog messageLog = new MPAY_MpClearIntegMsgLog();
		messageLog.setContent(isoMessageContent);
		Calendar date = MPClearClientHelper.getClient(context.getSystemParameters().getMPClearProcessor())
				.getSigningTimestamp();
		String signingDate = SystemHelper.formatDate(date.getTime(),
				context.getSystemParameters().getSigningDateFormat());
		String token = context.getCryptographer().generateToken(context.getSystemParameters(), isoMessageContent,
				signingDate, IntegMessagesSource.SYSTEM);
		String pspId = getPspId(context);
		messageLog.setIntegMsgType(integType);
		messageLog.setSource(IntegMessagesSource.SYSTEM);
		messageLog.setRefSender(pspId);
		messageLog.setSigningStamp(new Timestamp(date.getTimeInMillis()));
		messageLog.setToken(token);
		messageLog.setMessageId(isoMessage.getField(115).getValue().toString());
		messageLog.setIsProcessed(false);
		messageLog.setIsCanceled(false);
		messageLog.setResponseReceived(false);
		return messageLog;
	}

	public static MPAY_MpClearIntegMsgLog createMPClearResponse(ProcessingContext context, String originalMessageId,
			Integer messageType, String reasonCode, String reasonDescription, String transactionTypeCode) {
		LOGGER.debug("Inside createMPClearResponse, reasonCode: " + reasonCode + REASON_DESCRIPTION + reasonDescription
				+ ", transactionTypeCode: " + transactionTypeCode);
		if (originalMessageId == null)
			return null;
		MPAY_MpClearIntegMsgLog msgLog = new MPAY_MpClearIntegMsgLog();
		IsoMessage isoMessage = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
				.newMessage(messageType);
		String messageId = context.getDataProvider().getNextMPClearMessageId();
		String currencyCode = context.getSystemParameters().getDefaultCurrency().getCode();
		String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), BigDecimal.ZERO,
				AmountType.CHARGE);
		isoMessage.setIsoHeader("");
		isoMessage.setBinary(false);
		if (transactionTypeCode != null)
			isoMessage.setField(MPClearCommonFields.PROCESSING_CODE,
					new IsoValue<>(IsoType.NUMERIC, transactionTypeCode, 6));
		isoMessage.setField(MPClearCommonFields.TRANSMISSION_DATE,
				new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime())));
		isoMessage.setField(MPClearCommonFields.REASON_CODE,
				new IsoValue<>(IsoType.LLVAR, reasonCode, reasonCode.length()));
		isoMessage.setField(46, new IsoValue<>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
		if (reasonDescription != null)
			isoMessage.setField(MPClearCommonFields.REASON_DESCRIPTION,
					new IsoValue<>(IsoType.LLLVAR, reasonDescription, reasonDescription.length()));
		else
			isoMessage.setField(MPClearCommonFields.REASON_DESCRIPTION, new IsoValue<>(IsoType.LLLVAR, "", 0));
		isoMessage.setField(49, new IsoValue<>(IsoType.ALPHA, currencyCode, 3));
		return fillCommonResponseFields(context, originalMessageId, messageType, msgLog, isoMessage, messageId);
	}

	public static MPAY_MpClearIntegMsgLog createMPClearResponse(ProcessingContext context, String originalMessageId,
			MPAY_MpClearIntegMsgType messageType, String reasonCode, String reasonDescription,
			String transactionTypeCode) {
		LOGGER.debug("Inside createMPClearResponse, reasonCode: " + reasonCode + REASON_DESCRIPTION + reasonDescription
				+ ", transactionTypeCode: " + transactionTypeCode);
		if (originalMessageId == null)
			return null;
		Integer messageTypeCode = Integer.parseInt(messageType.getResponseCode(), 16);
		MPAY_MpClearIntegMsgLog msgLog = new MPAY_MpClearIntegMsgLog();
		IsoMessage isoMessage = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
				.newMessage(messageTypeCode);
		String messageId = context.getDataProvider().getNextMPClearMessageId();
		String currencyCode = context.getSystemParameters().getDefaultCurrency().getCode();
		String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), BigDecimal.ZERO,
				AmountType.CHARGE);
		isoMessage.setIsoHeader("");
		isoMessage.setBinary(false);
		if (transactionTypeCode != null)
			isoMessage.setField(MPClearCommonFields.PROCESSING_CODE,
					new IsoValue<>(IsoType.NUMERIC, transactionTypeCode, 6));
		isoMessage.setField(MPClearCommonFields.TRANSMISSION_DATE,
				new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime())));
		isoMessage.setField(MPClearCommonFields.REASON_CODE,
				new IsoValue<>(IsoType.LLVAR, reasonCode, reasonCode.length()));
		if (messageType.getIsFinancial()) {
			isoMessage.setField(46, new IsoValue<>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
			isoMessage.setField(49, new IsoValue<>(IsoType.ALPHA, currencyCode, 3));
		}
		if (reasonDescription != null)
			isoMessage.setField(MPClearCommonFields.REASON_DESCRIPTION,
					new IsoValue<>(IsoType.LLLVAR, reasonDescription, reasonDescription.length()));
		else
			isoMessage.setField(MPClearCommonFields.REASON_DESCRIPTION, new IsoValue<>(IsoType.LLLVAR, "", 0));
		return fillCommonResponseFields(context, originalMessageId, messageTypeCode, msgLog, isoMessage, messageId);
	}

	public static MPAY_MpClearIntegMsgLog createMPClearReversalResponse(ProcessingContext context,
			String originalMessageId, Integer messageType, String reasonCode, String reasonDescription) {
		LOGGER.debug("Inside createMPClearResponse, originalMessageId: " + originalMessageId + "messageType: "
				+ messageType + ", reasonCode: " + reasonCode + REASON_DESCRIPTION + reasonDescription);
		if (originalMessageId == null)
			return null;
		MPAY_MpClearIntegMsgLog msgLog = new MPAY_MpClearIntegMsgLog();
		IsoMessage isoMessage = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
				.newMessage(messageType);
		String messageId = context.getDataProvider().getNextMPClearMessageId();
		isoMessage.setIsoHeader("");
		isoMessage.setBinary(false);
		isoMessage.setField(MPClearCommonFields.TRANSMISSION_DATE,
				new IsoValue<>(IsoType.DATE10, formatIsoDate(SystemHelper.getCalendar().getTime())));
		isoMessage.setField(MPClearCommonFields.REASON_CODE,
				new IsoValue<>(IsoType.LLVAR, reasonCode, reasonCode.length()));
		if (reasonDescription != null)
			isoMessage.setField(MPClearCommonFields.REASON_DESCRIPTION,
					new IsoValue<>(IsoType.LLLVAR, reasonDescription, reasonDescription.length()));
		else
			isoMessage.setField(MPClearCommonFields.REASON_DESCRIPTION, new IsoValue<>(IsoType.LLLVAR, "", 0));
		return fillCommonResponseFields(context, originalMessageId, messageType, msgLog, isoMessage, messageId);
	}

	public static Integer getResponseType(Integer requestType) {
		LOGGER.debug("Inside getResponseType, requestType: " + requestType);
		if (requestType.equals(MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST))
			return MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE;
		else if (requestType.equals(MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST))
			return MPClearIntegrationMessageTypes.REVERSAL_ADVISE_RESPONSE;
		else
			throw new NotSupportedException("requestType = " + requestType);
	}

	public static BigDecimal getAmount(IsoMessage message) {
		LOGGER.debug("Inside getAmount =====================");
		BigDecimal amount = new BigDecimal(message.getField(4).toString());
		return amount.divide(new BigDecimal("1000"));
	}

	private static MPAY_MpClearIntegMsgLog fillCommonResponseFields(ProcessingContext context, String originalMessageId,
			Integer messageType, MPAY_MpClearIntegMsgLog msgLog, IsoMessage isoMessage, String messageId) {
		LOGGER.debug("Inside fillCommonResponseFields, originalMessageId: " + originalMessageId + ", messageType: "
				+ messageType + ", messageId: " + messageId);
		String contents;
		isoMessage.setField(MPClearCommonFields.MESSAGE_ID,
				new IsoValue<>(IsoType.LLLVAR, messageId, messageId.length()));
		isoMessage.setField(MPClearCommonFields.ORIGINAL_MESSAGE_ID,
				new IsoValue<>(IsoType.LLLVAR, originalMessageId, originalMessageId.length()));

		contents = new String(isoMessage.writeData());
		Calendar date = getSigningTimestamp();
		String signingDate = SystemHelper.formatDate(date.getTime(),
				context.getSystemParameters().getSigningDateFormat());
		String token = context.getCryptographer().generateToken(context.getSystemParameters(), contents, signingDate,
				IntegMessagesSource.SYSTEM);
		String pspId = getPspId(context);
		msgLog.setContent(contents);
		msgLog.setIntegMsgType(Integer.toHexString(messageType));
		msgLog.setSource(IntegMessagesSource.SYSTEM);
		msgLog.setRefSender(pspId);
		msgLog.setSigningStamp(new Timestamp(date.getTimeInMillis()));
		msgLog.setToken(token);
		msgLog.setMessageId(messageId);
		msgLog.setIsProcessed(false);
		msgLog.setIsCanceled(false);
		msgLog.setResponseReceived(false);
		return msgLog;
	}

	private static String getPspId(ProcessingContext context) {
		boolean isStandAlone = context.getSystemParameters().isSystemStandAlone();
		return isStandAlone ? context.getLookupsLoader().getMPClear().getRegistrationId() : context.getLookupsLoader().getPSP().getNationalID();
	}
}