package com.progressoft.mpay.payments.cashout;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entities.MPAY_ServiceCashOut;

public class ValidateServiceCashOutRequest implements Validator {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ValidateServiceCashOutRequest.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside validate =============================");

		InvalidInputException iie = new InvalidInputException();

		MPAY_ServiceCashOut cashout = (MPAY_ServiceCashOut) arg0.get(WorkflowService.WF_ARG_BEAN);

		if (cashout.getRefCorporateService() == null) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.CORPORATE_SERVICE_ISREQUIRED));
			throw iie;
		}

		if (!cashout.getRefCorporateService().getRefCorporate().getIsActive()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.CORPORATE_ISNOT_ACTIVE));
			throw iie;
		}

		if (!cashout.getRefCorporateService().getIsActive()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.CORPORATE_SERVICE_ISNOT_ACTIVE));
			throw iie;
		}

		if (cashout.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.AMOUNT_SHOULD_BE_GREATER_THAN_ZERO));
			throw iie;
		}

		MPAY_ServiceAccount account = SystemHelper.getServiceAccount(SystemParameters.getInstance(), cashout.getRefCorporateService(), cashout.getRefServiceAccount());
		if (account == null || account.getRefAccount() == null) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.ACCOUNT_NOT_FOUND));
			throw iie;
		}
		BigDecimal balance = account.getRefAccount().getMinBalance();
		BigDecimal amount = cashout.getAmount();
		BigDecimal result = balance.subtract(amount);

		if (cashout.getAmount().compareTo(result) < 1) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.AMOUNT_SHOULD_BE_GREATER_THAN_MIN_BALANCE));
			throw iie;
		}
	}
}