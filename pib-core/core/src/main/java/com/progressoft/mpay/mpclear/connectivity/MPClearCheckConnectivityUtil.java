package com.progressoft.mpay.mpclear.connectivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.Properties;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.apache.cxf.binding.soap.SoapHeader;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.jfw.model.bussinessobject.Util;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.exceptions.MPayGenericException;

public class MPClearCheckConnectivityUtil {

    @PersistenceContext
    private EntityManager entityManager;

    private static final String AGENT_ID_CLOSE = "</agentId>";
    private static final String AGENT_ID_OPEN = "<agentId>";
    private static final String IS_ONLINE_CLOSE = "</IsOnline>";
    private static final String IS_ONLINE_OPEN = "<IsOnline xmlns=\"http://www.progressoft.com/mpclear\">";
    private static final String OFFLINE_MODE_ENABLED_CONFIG_KEY = "Offline Mode Enabled";
    private static final String TRUE_INTEGER = "1";
    private static final String FALSE_INTEGER = "0";
    private static final String TRUE = "TRUE";
    private static final String SYSTEM_TENANT = "SYSTEM";
    private static final String PAYMENT_SERVICE_PROVIDER_CODE = "100";
    private static final String SYS_CONFIG_CACHE_NAME = "com.progressoft.mpay.entities.MPAY_SysConfig";
    private static final String PAY_LOAD_MESSAGE_PROPERTY_NAME = "payLoadMessage";
    private static final String BEHAVIOR = "behavior";

    public void setOfflineModeParameter(@Header(BEHAVIOR) String behavior) {
        boolean enabledByMe = false;
        try {
            enabledByMe = enableServiceUser();
            String configValue = getConfigValueAccordingToBehaviour(behavior);
            updateSystemParameterAndFlushCacheIfNeed(configValue);
        } finally {
            if (enabledByMe)
                disableServiceUser();
        }
    }

    private void updateSystemParameterAndFlushCacheIfNeed(String configValue) {
        if (needToChangeIfValueChange(configValue)) {
            DataProvider.instance().updateSystemParameter(OFFLINE_MODE_ENABLED_CONFIG_KEY, configValue);
            Util.removeCacheByName(SYS_CONFIG_CACHE_NAME);
        }
    }

    private boolean needToChangeIfValueChange(String configValue) {
        String currentConfigValue = getCurrentConfigValue();
        return configValue.equals(currentConfigValue) ? false : true;
    }

    private String getCurrentConfigValue() {
        return LookupsLoader.getInstance().getSystemConfigurations(OFFLINE_MODE_ENABLED_CONFIG_KEY).getConfigValue();
    }

    private boolean enableServiceUser() {
        return IntegrationUtils.enableServiceUser(SYSTEM_TENANT);
    }

    private String getConfigValueAccordingToBehaviour(String behavior) {
        return behavior.equals(TRUE) ? TRUE_INTEGER : FALSE_INTEGER;
    }

    public void setIsOnlinePayloadMessage(Exchange exchange, @Properties Map<String, Object> properties) {
        String payloadMessage = prepareIsOnlinePayLoadMessage();
        properties.put(PAY_LOAD_MESSAGE_PROPERTY_NAME, payloadMessage);
        DOMSource source;
        try {
            source = getDOMSourceFromPayLoadMessage(payloadMessage);
        } catch (IOException | SAXException | ParserConfigurationException e) {
            throw new MPayGenericException("Failed when setIsOnlinePayloadMessage", e);
        }
        List<Source> listOfSourceElements = intilizeListOfSource(source);
        CxfPayload<SoapHeader> requestPayLoad = initilizeCXFPayloadMessage(listOfSourceElements);
        exchange.getOut().setBody(requestPayLoad);
    }

    private String prepareIsOnlinePayLoadMessage() {
        return new StringBuilder().append(IS_ONLINE_OPEN).append(AGENT_ID_OPEN).append(100).append(AGENT_ID_CLOSE).append(IS_ONLINE_CLOSE).toString();
    }

//	private int getPSPNationalID() {
//		MPAY_Corporate anyCorporate = getFirstMpayCorporateServiceFromSystem();
//
//		if(pspHasNationalID(anyCorporate)) {
//			return Integer.parseInt(anyCorporate.getRefPSPPspDetails().get(0).getPspNationalID());
//		}
//		return 0;
//	}

//	private boolean pspHasNationalID(MPAY_Corporate anyCorporate) {
//		if(anyCorporate != null && anyCorporate.getRefPSPPspDetails() != null)
//			return true;
//		return false;
//	}

    private MPAY_Corporate getFirstMpayCorporateServiceFromSystem() {
        String query = "FROM MPAY_Corporate c WHERE c.clientType.code = '" + PAYMENT_SERVICE_PROVIDER_CODE + "'";
        Query result = entityManager.createQuery(query);
        return !result.getResultList().isEmpty() ? (MPAY_Corporate) result.getResultList().get(0) : null;
    }

    @SuppressWarnings("deprecation")
    private DOMSource getDOMSourceFromPayLoadMessage(String payloadMessage) throws IOException, SAXException, ParserConfigurationException {
        XmlConverter converter = new XmlConverter();
        Document outDocument = converter.toDOMDocument(payloadMessage);
        return new DOMSource(outDocument.getDocumentElement());
    }

    private CxfPayload<SoapHeader> initilizeCXFPayloadMessage(List<Source> outElements) {
        return new CxfPayload<>(null, outElements, null);
    }

    private List<Source> intilizeListOfSource(DOMSource source) {
        List<Source> outElements = new ArrayList<>();
        outElements.add(source);
        return outElements;
    }

    private void disableServiceUser() {
        IntegrationUtils.disableServiceUser();
    }
}