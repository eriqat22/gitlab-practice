package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.MPAYView;

public class PostCancelCorporateRejected implements FunctionProvider {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(PostCancelCorporateRejected.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =====");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		handleServices(corporate);
	}

	private void handleServices(MPAY_Corporate corporate) throws WorkflowException {
		logger.debug("Inside HandleServices =====");
		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId(), CorporateServiceWorkflowStatuses.CANCELING.toString());
		if (services == null || services.isEmpty())
			return;
		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.REJECT, new WorkflowException());
		} while (servicesIterator.hasNext());
	}
}
