package com.progressoft.mpay.charges;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Commission;
import com.progressoft.mpay.entities.MPAY_CommissionSlice;
import com.progressoft.mpay.entity.MPAYView;

public class PreDeleteCommissionDetails implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PreDeleteCommissionDetails.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PreDeleteCommissionDetails ----------------");
        MPAY_Commission schemeDetails = (MPAY_Commission) arg0.get(WorkflowService.WF_ARG_BEAN);
        schemeDetails.setIsActive(false);
        for (MPAY_CommissionSlice param : schemeDetails.getRefCommissionCommissionSlices()) {
            Map<Option, Object> options = new EnumMap<>(Option.class);
            options.put(Option.ACTION_NAME, "Delete");
            JfwHelper.executeAction(MPAYView.COMMISSIONS_SLICES, param, options);
        }
    }

}
