package com.progressoft.mpay.plugins.validators;

import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CorporateValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(CorporateValidator.class);

    private CorporateValidator() {

    }

    public static ValidationResult validate(MPAY_Corporate corporate, boolean isSender) {
        LOGGER.debug("Inside Validate ...");
        LOGGER.debug("isSender ", isSender);
        if (!corporate.getIsActive() || !corporate.getIsRegistered()) {
            if (isSender)
                return new ValidationResult(ReasonCodes.SENDER_CORPORATE_NOT_ACTIVE, null, false);
            else
                return new ValidationResult(ReasonCodes.RECEIVER_CORPORATE_NOT_ACTIVE, null, false);
        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
