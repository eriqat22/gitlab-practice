package com.progressoft.mpay.registration.corporates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.BankedUnbankedFlag;

public class CorporateServiceBankedUnbankedChangeHandler implements ChangeHandler {

	private static final String EXTERNAL_ACC = "externalAcc";
	private static final Logger logger = LoggerFactory.getLogger(CorporateServiceBankedUnbankedChangeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
//		logger.debug("inside handle ------------");
//		if(changeHandlerContext.getFieldsAttributes().get(EXTERNAL_ACC) == null)
//			return;
//		String flag = "";
//		Object entity = changeHandlerContext.getEntity();
//		if (entity instanceof MPAY_Corporate)
//			flag = ((MPAY_Corporate) entity).getBankedUnbanked();
//		if (entity instanceof MPAY_CorpoarteService)
//			flag = ((MPAY_CorpoarteService) entity).getBankedUnbanked();
//		else if (entity instanceof MPAY_ServiceAccount)
//			flag = ((MPAY_ServiceAccount) entity).getBankedUnbanked();
//
//		setExternalAccount(flag, changeHandlerContext);
	}

	private void setExternalAccount(String flag, ChangeHandlerContext changeHandlerContext) {
		if (flag.equalsIgnoreCase(BankedUnbankedFlag.BANKED))
			setBanked(changeHandlerContext);
		else
			setUnbanked(changeHandlerContext);
		changeHandlerContext.getFieldsAttributes().get("bank").setClearSelections(true);
	}

	private void setBanked(ChangeHandlerContext changeHandlerContext) {
		changeHandlerContext.getFieldsAttributes().get(EXTERNAL_ACC).setRequired(true);
	}

	private void setUnbanked(ChangeHandlerContext changeHandlerContext) {
		changeHandlerContext.getFieldsAttributes().get(EXTERNAL_ACC).setRequired(false);
	}
}
