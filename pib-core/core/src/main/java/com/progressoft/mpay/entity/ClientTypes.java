package com.progressoft.mpay.entity;

public class ClientTypes {

	public static final String PERSON_CODE = "10";
	public static final String BUSINESS_CODE = "30";
	public static final String GOVERNMENT_CODE = "40";
	public static final String AGENT_CODE = "50";
	public static final String PS_MPCLEAR = "SYS-10";
	public static final String SERVICE_PROVIDER_CODE = "100";
	public static final String MNO_CODE = "SYS-20";
	public static final String MERCHANT = "650";
	
	private ClientTypes() {

	}
}
