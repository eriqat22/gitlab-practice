package com.progressoft.mpay.registration.corporates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.ErrorHandler;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.util.simpleview.SimpleView;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_Corporate;

public class CorporateErrorHandler extends ErrorHandler {

	@Autowired
	private ItemDao itemDao;

	@Override
	public InvalidInputException handleError(Exception ex, SimpleView simpleView, Object entity, int actionKey) {

		if (ex instanceof InvalidInputException)
			return (InvalidInputException) ex;

		InvalidInputException cause = null;

		if (ex instanceof DataIntegrityViolationException || isTransactionSystemException(ex) || ex.getCause() instanceof DataIntegrityViolationException && entity instanceof MPAY_Corporate) {
			cause = Unique.validate(itemDao, (MPAY_Corporate) entity, "name", ((MPAY_Corporate) entity).getName(), "duplicat.corporate.item.name");

			if (cause == null)
				cause = Unique.validate(itemDao, (MPAY_Corporate) entity, "description", ((MPAY_Corporate) entity).getDescription(), "duplicat.corporate.item.name");
		}

		if (simpleView != null && cause == null) {
			cause = new InvalidInputException();
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("duplicate.registrationId"));
			return cause;
		}

		return cause;
	}

	private boolean isTransactionSystemException(Exception ex) {
		return ex instanceof TransactionSystemException || ex.getCause() instanceof TransactionSystemException;
	}
}
