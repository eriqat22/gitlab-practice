package com.progressoft.mpay.charges;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;
import com.progressoft.mpay.entity.MessageCodes;

public class ChargesSchemeDetailsHandler implements ChangeHandler {

    private static final String RECEIVE_CHARGE_BEARER_PERCENT = "receiveChargeBearerPercent";
    private static final String SEND_CHARGE_BEARER_PERCENT = "sendChargeBearerPercent";
    private static final Logger logger = LoggerFactory.getLogger(ChargesSchemeDetailsHandler.class);

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {

        logger.debug("Inside ChargesSchemeDetailsHandler ------------");
        MPAY_ChargesSchemeDetail details = (MPAY_ChargesSchemeDetail) changeHandlerContext.getEntity();

        if (details.getMsgType().getCode().equals(MessageCodes.CI.name())) {
            details.setSendChargeBearerPercent(0L);
            details.setReceiveChargeBearerPercent(100L);
            changeHandlerContext.getFieldsAttributes().get(RECEIVE_CHARGE_BEARER_PERCENT).setEnabled(true);
            changeHandlerContext.getFieldsAttributes().get(SEND_CHARGE_BEARER_PERCENT).setEnabled(false);

        } else if (details.getMsgType().getCode().equals(MessageCodes.CO.name())) {
            details.setSendChargeBearerPercent(0L);
            details.setReceiveChargeBearerPercent(100L);
            changeHandlerContext.getFieldsAttributes().get(RECEIVE_CHARGE_BEARER_PERCENT).setEnabled(true);
            changeHandlerContext.getFieldsAttributes().get(SEND_CHARGE_BEARER_PERCENT).setEnabled(false);
        } else if (!details.getMsgType().getIsFinancial()) {
            details.setSendChargeBearerPercent(100L);
            details.setReceiveChargeBearerPercent(0L);
            changeHandlerContext.getFieldsAttributes().get(RECEIVE_CHARGE_BEARER_PERCENT).setEnabled(false);
            changeHandlerContext.getFieldsAttributes().get(SEND_CHARGE_BEARER_PERCENT).setEnabled(false);
        }
    }

}
