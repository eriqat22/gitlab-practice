package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.MPAYView;

public class PostCancelCustomerRejected implements FunctionProvider {
	private static final Logger logger = LoggerFactory.getLogger(PostCancelCustomerRejected.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =====");
		MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
		handleMobiles(customer);
	}

	private void handleMobiles(MPAY_Customer customer) throws WorkflowException {
		logger.debug("Inside HandleMobiles =====");
		List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId(), CustomerMobileWorkflowStatuses.CANCELING);
		if (mobiles == null || mobiles.isEmpty())
			return;
		Iterator<MPAY_CustomerMobile> mobilesIterator = mobiles.listIterator();
		do {
			MPAY_CustomerMobile mobile = mobilesIterator.next();
			JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, CustomerMobileWorkflowServiceActions.REJECT, new WorkflowException());
		} while (mobilesIterator.hasNext());
	}
}
