package com.progressoft.mpay.mpclear;

import java.util.Calendar;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public interface IMPClearClient {
	void addCustomer(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile);

	void addCorporate(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService service);

	void updateCustomer(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile,
			MPAY_MobileAccount account);

	void updateCorporate(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService service,
			MPAY_ServiceAccount account);

	void removeCustomer(CoreComponents coreComponents, MPAY_Customer customer);

	void removeCorporate(CoreComponents coreComponents, MPAY_Corporate corporate);

	void addMobileAccount(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile,
			MPAY_MobileAccount account);

	void addServiceAccount(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService service,
			MPAY_ServiceAccount account);

	void changeMobileAlias(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile,
			MPAY_MPayMessage message, String alias);

	void changeServiceAlias(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService service);

	void removeMobileAccount(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile,
			MPAY_MobileAccount account);

	void removeServiceAccount(CoreComponents coreComponents, MPAY_Corporate corporate, MPAY_CorpoarteService service,
			MPAY_ServiceAccount account);

	void setDefaultMobileAccount(CoreComponents coreComponents, MPAY_Customer customer, MPAY_CustomerMobile mobile,
			MPAY_MobileAccount account);

	void setDefaultServiceAccount(CoreComponents coreComponents, MPAY_Corporate corporate,
			MPAY_CorpoarteService service, MPAY_ServiceAccount account);

	void processOfflineFinancialRequest(MPAY_MpClearIntegMsgLog msgLog, Communicator communicator)
			throws WorkflowException;

	void processOnlineFinancialRequest(MPAY_MpClearIntegMsgLog msgLog, Communicator communicator)
			throws WorkflowException;

	void createNetworkManagement(MPAY_NetworkManagement oNetworkManagement);

	void processNetworkMessage(MPAY_MpClearIntegMsgLog msgLog) throws WorkflowException;

	void processRegistration(MPAY_MpClearIntegMsgLog msgLog, boolean isCustomer);

	String generateSecurityKey() throws WorkflowException;

	Calendar getSigningTimestamp();

	void updateResponseReceived(String messageId);

	void changeCorporateServiceAlias(CoreComponents coreComponents, MPAY_Corporate corporate,
			MPAY_CorpoarteService service, MPAY_MPayMessage message, String alias);
}
