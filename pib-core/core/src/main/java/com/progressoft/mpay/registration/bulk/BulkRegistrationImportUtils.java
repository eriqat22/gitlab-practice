package com.progressoft.mpay.registration.bulk;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.Properties;
import org.apache.camel.language.Simple;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.jfw.integration.JfwFacade;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.bussinessobject.security.Tenant;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_City;
import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_IDType;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_ServiceType;
import com.progressoft.mpay.entities.MPAY_ServicesCategory;
import com.progressoft.mpay.entities.MPAY_SysConfig;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPAYView;

public class BulkRegistrationImportUtils {

	private static final String CODE = "code='";

	private static final String DATE_FORMAT = "yyyy-MM-dd";

	private static final String THE_NUMBER_OF_FIELD_IS_NOT_CORRECT = " - The number of field is not correct";

	@Autowired
	private ItemDao itemDao;

	@Autowired
	@Qualifier("defaultJfwFacade")
	private JfwFacade facade;

	private static final int PATH_SIZE = 3;
	private static final String FOLDER_SOURCE = "Folder";
	private static final String FAILED = "failed";
	private static final String SUCCESS = "success";
	private static final String PERSON_CLIENT_TYPE_CODE = "150";

	private static final Logger LOGGER = LoggerFactory.getLogger(BulkRegistrationImportUtils.class);

	public Set<String> createCustomerLineSet() {
		return Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
	}

	public void validatePathAndSetPathProperties(@Header("CamelFileNameConsumed") String camelFileNameConsumed, @Properties Map<String, Object> properties, @Body String body, @Header("CamelFileNameOnly") String fileName) {
		String[] pathSplits = camelFileNameConsumed.split("\\\\");
		if (pathSplits.length != PATH_SIZE) {
			throw new IllegalArgumentException("The path should be c:\\bulk_registration\\{[customer,corporate]}\\{tenant name}");
		}

		String tenantName = pathSplits[0];
		String whereClause = "name = '" + tenantName + "'";
		if (itemDao.getItemCount(Tenant.class, null, whereClause) == 0) {
			throw new IllegalArgumentException("The tenant " + tenantName + " does not exist in the system");
		}

		String type = pathSplits[1];
		if (!(!"customer".equalsIgnoreCase(type) || !"corporate".equalsIgnoreCase(type))) {
			throw new IllegalArgumentException("The type " + type + " does not support");
		}

		properties.put("type", type);
		properties.put("tenantId", tenantName);
		properties.put("source", FOLDER_SOURCE);
		properties.put("customerLineSet", createCustomerLineSet());
		properties.put("corporateLineSet", createCorporateLineSet());
		properties.put("fileFullPath", camelFileNameConsumed);
		properties.put("filePath", tenantName + "\\" + type);
		properties.put("originalBody", body);
		properties.put("fileName", fileName);
		properties.put("dateFormatter", new SimpleDateFormat("yyy-MM-dd"));

		LOGGER.debug("The file " + camelFileNameConsumed + " has valid path");
	}

	private Object createCorporateLineSet() {
		return Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
	}

	public CustomerRegistraionLine parseCustomerDataFromLine(@Body String line, @Simple("${property.CamelSplitIndex}") int splitIndex) {
		CustomerRegistraionLine registraionLine = new CustomerRegistraionLine();
		String[] dataFromLine = line.split(",");
		try {
			if (dataFromLine.length != 28) {
				registraionLine.setLine(line);
				registraionLine.setLineNumber(splitIndex + 1);
				registraionLine.setResolution(FAILED);
				registraionLine.setErrorDescription(THE_NUMBER_OF_FIELD_IS_NOT_CORRECT);
			} else {
				registraionLine.setFirstName(dataFromLine[2]);
				registraionLine.setMiddleName(dataFromLine[3]);
				registraionLine.setLastName(dataFromLine[4]);
				registraionLine.setCity(dataFromLine[5]);
				registraionLine.setNationality(dataFromLine[6]);
				registraionLine.setIdType(dataFromLine[7]);
				registraionLine.setIdNum(dataFromLine[8]);
				registraionLine.setBirthDate(dataFromLine[9]);
				registraionLine.setClientRef(dataFromLine[10]);
				registraionLine.setPhoneOne(dataFromLine[11]);
				registraionLine.setPhoneTwo(dataFromLine[12]);
				registraionLine.setPrefLang(dataFromLine[13]);
				registraionLine.setEmail(dataFromLine[14]);
				registraionLine.setPobox(dataFromLine[15]);
				registraionLine.setZipCode(dataFromLine[16]);
				registraionLine.setBuildingNum(dataFromLine[17]);
				registraionLine.setStreetName(dataFromLine[18]);
				registraionLine.setNote(dataFromLine[19]);
				registraionLine.setMobileNumber(dataFromLine[20]);
				registraionLine.setOperator(dataFromLine[21]);
				registraionLine.setNfcSerial(dataFromLine[22]);
				registraionLine.setAlias(dataFromLine[23]);
				registraionLine.setBankShortName(dataFromLine[24]);
				registraionLine.setMobileAccSelector(dataFromLine[25]);
				registraionLine.setNotificationShowType(dataFromLine[26]);
				registraionLine.setExternalAcc(dataFromLine[27]);
				registraionLine.setLine(line);
				registraionLine.setLineNumber(splitIndex + 1);
			}
			return registraionLine;

		} catch (Exception e) {
			registraionLine.setLine(line);
			registraionLine.setLineNumber(splitIndex + 1);
			registraionLine.setResolution(FAILED);
			registraionLine.setErrorDescription(THE_NUMBER_OF_FIELD_IS_NOT_CORRECT);
			LOGGER.error("Error while reading line", e);
			return registraionLine;
		}
	}

	public void validateCustomerDataFromLine(@Body CustomerRegistraionLine registraionLine, @Simple("${property.customerLineSet}") Set<String> customerLineSet) throws ParseException {
		if (StringUtils.isBlank(StringUtils.strip(registraionLine.getLine(), ","))) {
			registraionLine.setResolution(FAILED);
			registraionLine.setErrorDescription(" - Empty Line");
		} else if (!customerLineSet.add(registraionLine.getLine())) {
			registraionLine.setResolution(FAILED);
			registraionLine.setErrorDescription(" - Dupplicate Line");
		} else {
			validateCustomerData(registraionLine);
		}
	}

	public void validateCorporateDataFromLine(@Body CorporateRegistrationLine registraionLine, @Simple("${property.corporateLineSet}") Set<String> corporateLineSet) throws ParseException {
		if (StringUtils.isBlank(StringUtils.strip(registraionLine.getLine(), ","))) {
			registraionLine.setResolution(FAILED);
			registraionLine.setErrorDescription(" - Empty Line");
		} else if (!corporateLineSet.add(registraionLine.getLine())) {
			registraionLine.setResolution(FAILED);
			registraionLine.setErrorDescription(" - Dupplicate Line");
		} else {
			validateCorporaterData(registraionLine);
		}
	}

	private void validateCorporaterData(CorporateRegistrationLine registraionLine) throws ParseException {
		StringBuilder errors = new StringBuilder();

		errors.append(validateCorporateInfo(registraionLine));
		errors.append(validateServiceInfo(registraionLine));

		if (errors.length() > 0) {
			registraionLine.setResolution(FAILED);
			registraionLine.setErrorDescription(errors.toString());
		} else {
			registraionLine.setResolution(SUCCESS);
		}
	}

	private String validateServiceInfo(CorporateRegistrationLine registraionLine) {
		StringBuilder errors = new StringBuilder();
		if (StringUtils.isBlank(registraionLine.getBankShortName()) || !validBank(registraionLine.getBankShortName())) {
			errors.append(" , bank not valid ");
		}

		validateServiceFinancial(registraionLine, errors);

		if (StringUtils.isBlank(registraionLine.getMobileAccSelector()) || !StringUtils.isNumeric(registraionLine.getMobileAccSelector())) {
			errors.append(" , mobile account selector not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getPaymentType()) || !validPaymentType(registraionLine.getPaymentType())) {
			errors.append(" , payment type not valid ");
		}

		return errors.toString();
	}

	private void validateServiceFinancial(CorporateRegistrationLine registraionLine, StringBuilder errors) {
		if (StringUtils.isBlank(registraionLine.getServiceCategory()) || !validServiceCategory(registraionLine.getServiceCategory())) {
			errors.append(" , service category not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getServiceType()) || !validServiceType(registraionLine.getServiceType())) {
			errors.append(" , service type not valid ");
		}
	}

	private String validateCorporateInfo(CorporateRegistrationLine registraionLine) throws ParseException {
		StringBuilder errors = new StringBuilder();
		validateCorporateMetaData(registraionLine, errors);

		if (StringUtils.isBlank(registraionLine.getRegistrationDate()) || !validDate(registraionLine.getRegistrationDate()) || isFutureDate(registraionLine.getRegistrationDate())) {
			errors.append(" , registration date not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getRegistrationId())) {
			errors.append(" , registration id not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getPrefLang()) || !validPrefLanguage(registraionLine.getPrefLang())) {
			errors.append(" , pref lang not valid ");
		}

		validateCorporateLocation(registraionLine, errors);
		return errors.toString();
	}

	private void validateCorporateLocation(CorporateRegistrationLine registraionLine, StringBuilder errors) {
		if (StringUtils.isBlank(registraionLine.getCity()) || !validCity(registraionLine.getCity())) {
			errors.append(" , city not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getNationality()) || !validNationality(registraionLine.getNationality())) {
			errors.append(" , Nationality not valid");
		}
	}

	private void validateCorporateMetaData(CorporateRegistrationLine registraionLine, StringBuilder errors) {
		if (StringUtils.isBlank(registraionLine.getCorporateName()) || StringUtils.isNumeric(registraionLine.getCorporateName())) {
			errors.append(" , corporate name not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getDescription())) {
			errors.append(" , description not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getClientType()) || !isValidClientType(registraionLine.getClientType())) {
			errors.append(" , client type not valid ");
		}
	}

	private boolean validNationality(String nationality) {
		return LookupsLoader.getInstance().getCountry(nationality) == null ? false : true;
	}

	private boolean validPaymentType(String paymentType) {
		return LookupsLoader.getInstance().getMessageTypes(paymentType) == null ? false : true;
	}

	private boolean validServiceType(String serviceType) {
		return itemDao.getItem(MPAY_ServiceType.class, "code", serviceType) == null ? false : true;
	}

	private boolean validServiceCategory(String serviceCategory) {
		return itemDao.getItem(MPAY_ServicesCategory.class, "code", serviceCategory) == null ? false : true;
	}

	private boolean isFutureDate(String registrationDate) throws ParseException {
		return new SimpleDateFormat(DATE_FORMAT).parse(registrationDate).after(new Date());
	}

	private boolean isValidClientType(String clientType) {
		return itemDao.getItem(MPAY_ClientType.class, "code", clientType) == null ? false : true;
	}

	public void createCustomerAndMobile(@Body CustomerRegistraionLine registraionLine, @Simple("${property.tenantId}") String tenantId) throws WorkflowException {
		try {
			MPAY_Customer customer = prepareCustomer(registraionLine);
			persistCustomerAndExecuteWF(customer, tenantId);
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}

	public void createCorporateAndService(@Body CorporateRegistrationLine registraionLine, @Simple("${property.tenantId}") String tenantId) throws WorkflowException {
		try {
			MPAY_Corporate corporate = prepareCorporate(registraionLine);
			MPAY_CorpoarteService service = prepareCorporateService(registraionLine, corporate);
			List<MPAY_CorpoarteService> corporateServices = new ArrayList<>();
			corporateServices.add(service);
			corporate.setRefCorporateCorpoarteServices(corporateServices);
			persistCorporateAndExecuteWF(corporate, tenantId);
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}

	private void persistCorporateAndExecuteWF(MPAY_Corporate corporate, String tenantId) throws WorkflowException {
		boolean enabledByMe = false;
		try {
			enabledByMe = IntegrationUtils.enableServiceUser(tenantId);
			corporate.setOrgId(AppContext.getCurrentPrefOrg().getId());
			Map<Option, Object> nestedOptions = new EnumMap<>(Option.class);
			Map<String, String> nestedViewsMap = new HashMap<>();
			nestedViewsMap.put("refCorporateCorpoarteServices", MPAYView.CORPORATE_SERVICES.viewName);
			nestedOptions.put(Option.NESTED_VIEWS, nestedViewsMap);
			nestedOptions.put(Option.ORG_SHORT_NAME, "SUPER");
			Map<String, Object> transientVar = new HashMap<>();
			transientVar.put("dontCheckMaxService", "1");
			facade.createEntity(MPAYView.CORPORATE.viewName, corporate, transientVar, nestedOptions);
			Map<Option, Object> options = new EnumMap<>(Option.class);
			options.put(Option.ACTION_NAME, "Approve");
			facade.executeAction(MPAYView.CORPORATE.viewName, null, corporate, transientVar, options);
		} catch (Exception ex) {
			LOGGER.error("Error while while creating corporate ", ex);
			throw new WorkflowException(ex);
		} finally {
			if (enabledByMe) {
				IntegrationUtils.disableServiceUser();
			}
		}
	}

	private MPAY_Corporate prepareCorporate(CorporateRegistrationLine registraionLine) throws ParseException {
		MPAY_Corporate corporate = new MPAY_Corporate();

		corporate.setName(registraionLine.getCorporateName());
		corporate.setDescription(registraionLine.getDescription());
		corporate.setClientType(LookupsLoader.getInstance().getClientType(registraionLine.getClientType()));
		corporate.setRegistrationDate(new SimpleDateFormat(DATE_FORMAT).parse(registraionLine.getRegistrationDate()));
		corporate.setRegistrationId(registraionLine.getRegistrationId());
		corporate.setPrefLang(LookupsLoader.getInstance().getLanguageByCode(registraionLine.getPrefLang()));
		corporate.setPhoneOne(registraionLine.getPhoneOne());
		corporate.setPhoneTwo(registraionLine.getPhoneTwo());
		corporate.setEmail(registraionLine.getEmail());
		corporate.setPobox(registraionLine.getPobox());
		corporate.setZipCode(registraionLine.getZipCode());
		corporate.setBuildingNum(registraionLine.getBuildingNum());
		corporate.setStreetName(registraionLine.getStreetName());
		corporate.setCity(LookupsLoader.getInstance().getCity(registraionLine.getCity()));
		corporate.setNote(registraionLine.getNote());
		corporate.setRefCountry(LookupsLoader.getInstance().getCountry(registraionLine.getNationality()));
		corporate.setIsActive(true);
		corporate.setClientRef(registraionLine.getClientRef());

		return corporate;
	}

	private MPAY_CorpoarteService prepareCorporateService(CorporateRegistrationLine registraionLine, MPAY_Corporate corporate) {
		MPAY_CorpoarteService corpService = new MPAY_CorpoarteService();
		MPAY_Bank bank = LookupsLoader.getInstance().getBank(registraionLine.getBankShortName());
		MPAY_Profile profile = itemDao.getItem(MPAY_Profile.class, "code", "Default");
		MPAY_MessageType messageType = LookupsLoader.getInstance().getMessageTypes(registraionLine.getPaymentType());
		MPAY_ServiceType serviceType = LookupsLoader.getInstance().getServiceType(registraionLine.getServiceType());
		MPAY_ServicesCategory serviceCategory = itemDao.getItem(MPAY_ServicesCategory.class, "code", registraionLine.getServiceCategory());

		corpService.setName(registraionLine.getServiceName());
		corpService.setDescription(registraionLine.getServiceDescription());
		corpService.setServiceCategory(serviceCategory);
		corpService.setServiceType(serviceType);
		corpService.setRefCorporate(corporate);
		corpService.setAlias(registraionLine.getAlias());
		corpService.setExternalAcc(registraionLine.getExternalAcc());
		corpService.setBankedUnbanked(bank.getSettlementParticipant());
		corpService.setBank(LookupsLoader.getInstance().getBank(registraionLine.getBankShortName()));
		corpService.setMas(Long.parseLong(registraionLine.getMobileAccSelector()));
		corpService.setPaymentType(messageType);
		corpService.setRefProfile(profile);
		corpService.setIsActive(true);
		corpService.setOrgId(corporate.getOrgId());

		return corpService;
	}

	public void putResultsBodyOnProperties(@Properties Map<String, Object> properties, @Body List<BulkRegistrationLine> registrationLines) {
		LOGGER.debug("Generate response files");
		String failedLinesBody = getCustomerFailedLinesBody(registrationLines);
		properties.put("failedLineBody", failedLinesBody);
	}

	public CustomerRegistraionLine setCustomerFailedData(@Simple("${property.CamelSplitIndex}") int index, @Simple("${property.CamelExceptionCaught}") String exceptionCaught, @Body CustomerRegistraionLine customerRegistrationLine,
			@Simple("${property.CamelExceptionCaught}") Throwable camelExceptionCaught) {
		camelExceptionCaught.getMessage();
		customerRegistrationLine.setResolution(FAILED);
		customerRegistrationLine.setLineNumber(index + 1);
		StringBuilder causes = null;
		if (camelExceptionCaught.getCause().getCause() instanceof InvalidInputException) {
			causes = new StringBuilder();
			for (Object value : ((InvalidInputException) camelExceptionCaught.getCause().getCause()).getErrors().values()) {
				causes.append((String) value).append(",");
			}
		}
		customerRegistrationLine.setErrorDescription(causes == null ? exceptionCaught : causes.toString());
		return customerRegistrationLine;
	}

	public CorporateRegistrationLine setCorporateFailedData(@Simple("${property.CamelSplitIndex}") int index, @Simple("${property.CamelExceptionCaught}") String exceptionCaught, @Body CorporateRegistrationLine corporateRegistrationLine,
			@Simple("${property.CamelExceptionCaught}") Throwable camelException) {
		camelException.getMessage();
		corporateRegistrationLine.setResolution(FAILED);
		corporateRegistrationLine.setLineNumber(index + 1);
		StringBuilder erros = null;
		if (camelException.getCause().getCause() instanceof InvalidInputException) {
			erros = new StringBuilder();
			for (Object value : ((InvalidInputException) camelException.getCause().getCause()).getErrors().values()) {
				erros.append((String) value).append(",");
			}
		}
		corporateRegistrationLine.setErrorDescription(erros == null ? exceptionCaught : erros.toString());
		return corporateRegistrationLine;
	}

	private void validateCustomerData(CustomerRegistraionLine registraionLine) throws ParseException {
		StringBuilder errors = new StringBuilder();

		errors.append(validateCustomerInfo(registraionLine));
		errors.append(validateCustomerMobile(registraionLine));

		if (errors.length() > 0) {
			registraionLine.setResolution(FAILED);
			registraionLine.setErrorDescription(errors.toString());
		} else {
			registraionLine.setResolution(SUCCESS);
		}

	}

	private String validateCustomerInfo(CustomerRegistraionLine registraionLine) throws ParseException {
		StringBuilder errors = new StringBuilder();

		errors.append(validateCustomerName(registraionLine));

		if (StringUtils.isBlank(registraionLine.getIdType()) || !validIdType(registraionLine.getIdType())) {
			errors.append(" , ID type not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getIdNum()) || !StringUtils.isNumeric(registraionLine.getIdNum())) {
			errors.append(" , ID Number not valid ");
		}

		validateDateOfBirth(registraionLine, errors);

		if (StringUtils.isBlank(registraionLine.getClientRef())) {
			errors.append(" , client ref not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getPrefLang()) || !validPrefLanguage(registraionLine.getPrefLang())) {
			errors.append(" , pref lang not valid ");
		}

		validateCustomerLocation(registraionLine, errors);

		return errors.toString();
	}

	private void validateDateOfBirth(CustomerRegistraionLine registraionLine, StringBuilder errors) throws ParseException {
		if (StringUtils.isBlank(registraionLine.getBirthDate()) || !validDate((String) registraionLine.getBirthDate()) || !validAge(registraionLine.getBirthDate())) {
			errors.append(" , birth date not valid ");
		}
	}

	private void validateCustomerLocation(CustomerRegistraionLine registraionLine, StringBuilder errors) {
		if (StringUtils.isBlank(registraionLine.getNationality()) || !validNationality(registraionLine.getNationality())) {
			errors.append(" , nationality not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getCity()) || !validCity(registraionLine.getCity())) {
			errors.append(" , city not valid ");
		}
	}

	private String validateCustomerName(CustomerRegistraionLine registraionLine) {
		StringBuilder errors = new StringBuilder();

		if (StringUtils.isBlank(registraionLine.getFirstName()) || StringUtils.isNumeric(registraionLine.getFirstName())) {
			errors.append(" , first name not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getMiddleName()) || StringUtils.isNumeric(registraionLine.getMiddleName())) {
			errors.append(" , middle name not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getLastName()) || StringUtils.isNumeric(registraionLine.getLastName())) {
			errors.append(" , last name not valid ");
		}

		return errors.toString();
	}

	private String validateCustomerMobile(CustomerRegistraionLine registraionLine) {
		StringBuilder errors = new StringBuilder();

		if (StringUtils.isBlank(registraionLine.getMobileNumber()) || !StringUtils.isNumeric(registraionLine.getMobileNumber())) {
			errors.append(" , mobile number not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getOperator()) || !validOperator(registraionLine.getOperator())) {
			errors.append(" , operator not valid ");
		}

		if (StringUtils.isBlank(registraionLine.getBankShortName()) || !validBank(registraionLine.getBankShortName())) {
			errors.append(" , bank not valid ");
		}

		if (StringUtils.isNotBlank(registraionLine.getMobileAccSelector()) && !StringUtils.isNumeric(registraionLine.getMobileAccSelector())) {
			errors.append(" , Mobile Account Selector not valid ");
		}

		return errors.toString();
	}

	private boolean validCity(String cityCode) {
		return itemDao.getItemCount(MPAY_City.class, null, CODE + cityCode + "'") > 0 ? true : false;
	}

	private boolean validIdType(String idTypeCode) {
		return itemDao.getItemCount(MPAY_IDType.class, null, CODE + idTypeCode + "'") > 0 ? true : false;
	}

	private boolean validDate(String birthDate) {
		try {
			new SimpleDateFormat(DATE_FORMAT).parse(birthDate);
			return true;
		} catch (Exception e) {
			LOGGER.error("Error while parsing birthDate", e);
			return false;
		}
	}

	private boolean validAge(String birthDate) throws ParseException {
		MPAY_SysConfig r = LookupsLoader.getInstance().getSystemConfigurations("Min Age");
		Date date = new SimpleDateFormat(DATE_FORMAT).parse(birthDate);
		if (DateUtils.addYears(date, Integer.parseInt(r.getConfigValue())).after(new Date())) {
			return false;
		}
		return true;
	}

	private boolean validPrefLanguage(String prefLangCode) {
		return itemDao.getItemCount(MPAY_Language.class, null, CODE + prefLangCode + "'") > 0 ? true : false;
	}

	private boolean validOperator(String operatorName) {
		return itemDao.getItemCount(MPAY_Corporate.class, null, "name='" + operatorName + "'") > 0 ? true : false;
	}

	private boolean validBank(String bankName) {
		return itemDao.getItemCount(MPAY_Bank.class, null, "name='" + bankName + "'") > 0 ? true : false;
	}

	private MPAY_Customer prepareCustomer(CustomerRegistraionLine registraionLine) throws ParseException {
		MPAY_Customer customer = new MPAY_Customer();
		String defaultProfileSysConfigValue = SystemParameters.getInstance().getDefaultProfile();
		MPAY_Profile profile = itemDao.getItem(MPAY_Profile.class, "code", defaultProfileSysConfigValue);

		if (profile == null) {
			throw new IllegalArgumentException("Default Profile not defined");
		}
		customer.setFirstName(registraionLine.getFirstName());
		customer.setMiddleName(registraionLine.getMiddleName());
		customer.setLastName(registraionLine.getLastName());
		customer.setIsActive(true);
		customer.setNationality(LookupsLoader.getInstance().getCountry(registraionLine.getNationality()));
		customer.setIdType(LookupsLoader.getInstance().getIDType(registraionLine.getIdType()));
		customer.setIdNum(registraionLine.getIdNum());
		customer.setDob(new SimpleDateFormat(DATE_FORMAT).parse(registraionLine.getBirthDate()));
		customer.setClientType(LookupsLoader.getInstance().getClientType(PERSON_CLIENT_TYPE_CODE));
		customer.setClientRef(registraionLine.getClientRef());
		customer.setPhoneOne(registraionLine.getPhoneOne());
		customer.setPhoneTwo(registraionLine.getPhoneTwo());
		customer.setCity(LookupsLoader.getInstance().getCity(registraionLine.getCity()));
		customer.setPrefLang(LookupsLoader.getInstance().getLanguageByCode(registraionLine.getPrefLang()));
		customer.setEmail(registraionLine.getEmail());
		customer.setPobox(registraionLine.getPobox());
		customer.setZipCode(registraionLine.getZipCode());
		customer.setBuildingNum(registraionLine.getBuildingNum());
		customer.setStreetName(registraionLine.getStreetName());
		customer.setNote(registraionLine.getNote());
		customer.setAlias(registraionLine.getAlias());
		customer.setNfcSerial(registraionLine.getNfcSerial());
		customer.setRefProfile(profile);
		customer.setMobileNumber(registraionLine.getMobileNumber());
		customer.setBankedUnbanked(BankedUnbankedFlag.BANKED);
		customer.setBank(LookupsLoader.getInstance().getBank(registraionLine.getBankShortName()));
		customer.setNotificationShowType(registraionLine.getNotificationShowType());
		customer.setExternalAcc(registraionLine.getExternalAcc());

		return customer;
	}

	private void persistCustomerAndExecuteWF(MPAY_Customer customer, String tenantId) throws WorkflowException {
		LOGGER.debug("Persisting Customer/Mobile into database and execute workflow actions (create and approve).");
		boolean enableByMe = false;
		try {
			enableByMe = IntegrationUtils.enableServiceUser(tenantId);
			customer.setOrgId(AppContext.getCurrentPrefOrg().getId());
			facade.createEntity(MPAYView.CUSTOMER.viewName, customer);
			Map<String, Object> transientVar = new HashMap<>();
			Map<Option, Object> options = new EnumMap<>(Option.class);
			options.put(Option.ACTION_NAME, "Approve");
			transientVar.put("dontCheckMaxService", "1");
			facade.executeAction(MPAYView.CUSTOMER.viewName, null, customer, transientVar, options);
		} catch (Exception ex) {
			LOGGER.error("Error while while creating customer", ex);
			throw new WorkflowException(ex);
		} finally {
			if (enableByMe) {
				IntegrationUtils.disableServiceUser();
			}
		}
	}

	private String getCustomerFailedLinesBody(List<BulkRegistrationLine> failedLines) {
		StringBuilder result = new StringBuilder();
		for (BulkRegistrationLine registraionLine : failedLines) {
			if (FAILED.equals(registraionLine.getResolution()))
				result.append(registraionLine.getLineNumber()).append(" - ").append(registraionLine.getErrorDescription()).append("\n");
		}
		return result.toString();
	}

	public void handleFileException(@Simple("${property.CamelExceptionCaught}") Throwable camelExceptionCaught, Exchange exchange) {
		exchange.getIn().setBody(camelExceptionCaught.getMessage());
	}

	public CorporateRegistrationLine parseCorporateDataFromLine(@Body String line, @Simple("${property.CamelSplitIndex}") int splitIndex) {
		CorporateRegistrationLine registraionLine = new CorporateRegistrationLine();
		String[] dataFromLine = line.split(",");
		try {
			if (dataFromLine.length != 29) {
				registraionLine.setLine(line);
				registraionLine.setLineNumber(splitIndex + 1);
				registraionLine.setErrorDescription(THE_NUMBER_OF_FIELD_IS_NOT_CORRECT);
			} else {
				registraionLine.setCorporateName(dataFromLine[2]);
				registraionLine.setDescription(dataFromLine[3]);
				registraionLine.setClientType(dataFromLine[4]);
				registraionLine.setRegistrationDate(dataFromLine[5]);
				registraionLine.setRegistrationId(dataFromLine[6]);
				registraionLine.setPrefLang(dataFromLine[7]);
				registraionLine.setPhoneOne(dataFromLine[8]);
				registraionLine.setPhoneTwo(dataFromLine[9]);
				registraionLine.setEmail(dataFromLine[10]);
				registraionLine.setPobox(dataFromLine[11]);
				registraionLine.setZipCode(dataFromLine[12]);
				registraionLine.setBuildingNum(dataFromLine[13]);
				registraionLine.setStreetName(dataFromLine[14]);
				registraionLine.setNationality(dataFromLine[15]);
				registraionLine.setCity(dataFromLine[16]);
				registraionLine.setClientRef(dataFromLine[17]);
				registraionLine.setNote(dataFromLine[18]);
				registraionLine.setBankShortName(dataFromLine[19]);
				registraionLine.setServiceName(dataFromLine[20]);
				registraionLine.setAlias(dataFromLine[21]);
				registraionLine.setExternalAcc(dataFromLine[22]);
				registraionLine.setMobileAccSelector(dataFromLine[23]);
				registraionLine.setPaymentType(dataFromLine[24]);
				registraionLine.setServiceCategory(dataFromLine[26]);
				registraionLine.setServiceType(dataFromLine[27]);
				registraionLine.setServiceDescription(dataFromLine[28]);
				registraionLine.setLine(line);
				registraionLine.setLineNumber(splitIndex + 1);
			}
			return registraionLine;

		} catch (Exception e) {
			LOGGER.error("Error while parseCorporateDataFromLine", e);
			registraionLine.setLine(line);
			registraionLine.setLineNumber(splitIndex + 1);
			registraionLine.setResolution(FAILED);
			registraionLine.setErrorDescription(THE_NUMBER_OF_FIELD_IS_NOT_CORRECT);
			return registraionLine;
		}
	}
}