package com.progressoft.mpay.entity;

public class TaxTypes {
	public static final String FIXED = "1";
	public static final String PERCENTAGE = "2";

	private TaxTypes() {

	}
}
