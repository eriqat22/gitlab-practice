package com.progressoft.mpay.messages;

public class MessageParsingException extends Exception {

	private static final long serialVersionUID = 1L;

	private final String field;

	public MessageParsingException(String field) {
		super(field);
		this.field = field;
	}

	public String getField() {
		return field;
	}
}