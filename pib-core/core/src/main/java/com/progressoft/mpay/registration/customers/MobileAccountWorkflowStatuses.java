package com.progressoft.mpay.registration.customers;

public enum MobileAccountWorkflowStatuses {
	APPROVAL("101002"), REJECTED("101003"), MODIFICATION("101004"), APPROVED("101005"), DELETED("101006"), INTEGRATION("101007"), DELETING("101008"), MODIFIED("101010"), CANCELING("101011"), SUSPENDED("101014"),DORMANT("101017");

	private final String status;

	MobileAccountWorkflowStatuses(String value) {
		this.status = value;
	}

	@Override
	public String toString() {
		return this.status;
	}
}
