package com.progressoft.mpay;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

import javax.xml.transform.Source;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.apache.cxf.binding.soap.SoapHeader;
import org.slf4j.Logger;
import org.w3c.dom.Element;

import com.progressoft.jfw.util.LoggerHelper;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.exceptions.InvalidArgumentException;

public class HandleCXFProcessMessageOperationRequest implements Processor {

    private static final Logger LOGGER = LoggerHelper.getLogger();

    @SuppressWarnings("unchecked")
    @Override
    public void process(Exchange exchange) throws Exception {

        try {

            CxfPayload<SoapHeader> requestPayload = exchange.getIn().getBody(CxfPayload.class);
            List<Source> inElements = requestPayload.getBodySources();
            org.w3c.dom.Element msgEl = new XmlConverter().toDOMElement(inElements.get(0));

            if (msgEl.getChildNodes() == null || msgEl.getChildNodes().getLength() != 4)
                throw new InvalidArgumentException("Invalid argument input for send operation!");

            logDebugInfo(msgEl);
            MPAY_MpClearIntegMsgLog msgLog = prepareMessageLog(exchange, msgEl);
            exchange.getOut().setBody(msgLog);

        } catch (Exception ex) {
            LOGGER.error("Error while HandleCXFProcessMessageOperationRequest message", ex);
        }
    }

    private MPAY_MpClearIntegMsgLog prepareMessageLog(Exchange exchange, Element msgEl) throws ParseException {
        MPAY_MpClearIntegMsgLog msgLog = new MPAY_MpClearIntegMsgLog();
        String contents = msgEl.getChildNodes().item(0).getTextContent();
        setIntegrationMessageType(msgLog, contents);
        setMsgLogContents(msgEl, msgLog, contents);
        exchange.setProperty("msgType", contents.substring(0, 4));
        return msgLog;
    }

    private void setMsgLogContents(Element msgEl, MPAY_MpClearIntegMsgLog msgLog, String contents) throws ParseException {
        msgLog.setSource(IntegMessagesSource.MP_CLEAR);
        msgLog.setContent(contents);
        msgLog.setRefSender(msgEl.getChildNodes().item(1).getTextContent());
        msgLog.setToken(msgEl.getChildNodes().item(2).getTextContent());
        msgLog.setSigningStamp(new Timestamp(SystemHelper.parseDate(msgEl.getChildNodes().item(3).getTextContent(),
                "yyyy-MM-dd'T'HH:mm:ss").getTime()));
    }

    private void logDebugInfo(Element msgEl) {
        LOGGER.debug("msgEl.getChildNodes().item(0).getTextContent() {}", msgEl.getChildNodes().item(0)
                .getTextContent());
        LOGGER.debug("msgEl.getChildNodes().item(1).getTextContent() {}", msgEl.getChildNodes().item(1)
                .getTextContent());
        LOGGER.debug("msgEl.getChildNodes().item(2).getTextContent() {}", msgEl.getChildNodes().item(2)
                .getTextContent());
        LOGGER.debug("msgEl.getChildNodes().item(3).getTextContent() {}", msgEl.getChildNodes().item(3)
                .getTextContent());
    }

    private void setIntegrationMessageType(MPAY_MpClearIntegMsgLog msgLog, String contents) {
        if (SystemHelper.isNullOrEmpty(contents))
            msgLog.setIntegMsgType(contents.length() >= 4 ? contents.substring(4) : contents.substring(contents.length()));
        else
            msgLog.setIntegMsgType("");
    }
}