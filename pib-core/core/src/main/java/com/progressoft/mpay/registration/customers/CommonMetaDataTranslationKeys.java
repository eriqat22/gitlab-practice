package com.progressoft.mpay.registration.customers;

public class CommonMetaDataTranslationKeys {
	public static final String ACTIVE = "Active";
	public static final String INACTIVE = "Inactive";
	public static final String OLD_DATA = "OldData";
	public static final String NEW_DATA = "NewData";
	public static final String FIELD = "Field";
	public static final String NAME = "ChangesName";
	public static final String DESCRIPTION = "Description";
	public static final String REFERENCE = "Reference";
	public static final String PHONE_ONE = "PhoneOne";
	public static final String PHONE_TWO = "PhoneTwo";
	public static final String EMAIL = "Email";
	public static final String PO_BOX = "POBox";
	public static final String ZIP_CODE = "ZipCode";
	public static final String BUILDING_NUMBER = "BuildingNumber";
	public static final String STREEN_NAME = "StreetName";
	public static final String NOTE = "Note";
	public static final String NOTIFICATION_LANGUAGE = "NotificationLanguage";
	public static final String CITY = "City";

	protected CommonMetaDataTranslationKeys() {

	}
}
