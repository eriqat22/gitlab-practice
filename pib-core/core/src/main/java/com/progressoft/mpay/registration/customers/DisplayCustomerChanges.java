package com.progressoft.mpay.registration.customers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.shared.DynamicField;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.LocalizationHelper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_City;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entity.CustomerMetaData;

public class DisplayCustomerChanges implements ChangeHandler {

    private static final String PORTLET_NAME = "Basic Info";
    private static final Logger logger = LoggerFactory.getLogger(DisplayCustomerChanges.class);
    private static final String MODIFIED_COLOR = "blue";

    LocalizationHelper localizationHelper;

    public DisplayCustomerChanges() {
        localizationHelper = new LocalizationHelper();
    }

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside handle--------------------------");
        MPAY_Customer customer = (MPAY_Customer) changeHandlerContext.getEntity();
        try {
            Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
            for (UIPortletAttributes ui : portletsAttributes) {
                ui.setVisible(true);
            }
            renderFields(changeHandlerContext, customer, CustomerMetaData.fromJson(customer.getApprovedData()));
        } catch (Exception e) {
            throw new InvalidInputException(e);
        }

    }

    private void renderFields(ChangeHandlerContext changeHandlerContext, MPAY_Customer customer, CustomerMetaData approvedCustomer) {
        logger.debug("inside RenderFields--------------------------");
        List<DynamicField> dynamicFields = new ArrayList<>();

        DynamicField field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.FIELD));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.OLD_DATA));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.NEW_DATA));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.FIRST_NAME));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getFirstName());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getFirstName());
        handleChanged(approvedCustomer.getFirstName(), customer.getFirstName(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.MIDDLE_NAME));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getMiddleName());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getMiddleName());
        handleChanged(approvedCustomer.getMiddleName(), customer.getMiddleName(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.LAST_NAME));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getLastName());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getLastName());
        handleChanged(approvedCustomer.getLastName(), customer.getLastName(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.DATE_OF_BIRTH));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(String.valueOf(approvedCustomer.getDob()));
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(String.valueOf(customer.getDob()));
        if (approvedCustomer.getDob().compareTo(customer.getDob()) != 0)
            field.setLabelColor(MODIFIED_COLOR);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.REFERENCE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getClientRef());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getClientRef());
        handleChanged(approvedCustomer.getClientRef(), customer.getClientRef(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.PHONE_ONE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getPhoneOne());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getPhoneOne());
        handleChanged(approvedCustomer.getPhoneOne(), customer.getPhoneOne(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.PHONE_TWO));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getPhoneTwo());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getPhoneTwo());
        handleChanged(approvedCustomer.getPhoneTwo(), customer.getPhoneTwo(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.EMAIL));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getEmail());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getEmail());
        handleChanged(approvedCustomer.getEmail(), customer.getEmail(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.PO_BOX));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getPobox());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getPobox());
        handleChanged(approvedCustomer.getPobox(), customer.getPobox(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.ZIP_CODE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getZipCode());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getZipCode());
        handleChanged(approvedCustomer.getZipCode(), customer.getZipCode(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.BUILDING_NUMBER));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getBuildingNum());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getBuildingNum());
        handleChanged(approvedCustomer.getBuildingNum(), customer.getBuildingNum(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.STREEN_NAME));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getStreetName());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getStreetName());
        handleChanged(approvedCustomer.getStreetName(), customer.getStreetName(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.NOTE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getNote());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getNote());
        handleChanged(approvedCustomer.getNote(), customer.getNote(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.NOTIFICATION_LANGUAGE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        MPAY_Language approvedLanguage = MPayContext.getInstance().getLookupsLoader().getLanguage(approvedCustomer.getPrefLangId());

        field = new DynamicField();
        field.setLabel(approvedLanguage.getNameTranslation());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getPrefLang().getNameTranslation());
        if (approvedCustomer.getPrefLangId() != customer.getPrefLang().getId())
            field.setLabelColor(MODIFIED_COLOR);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.CITY));
        field.setLabelBold(true);
        dynamicFields.add(field);

        MPAY_City approvedCity = MPayContext.getInstance().getLookupsLoader().getCity(approvedCustomer.getCityCode());

        field = new DynamicField();
        field.setLabel(approvedCity.getNameTranslation());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getCity().getNameTranslation());
        handleChanged(approvedCustomer.getCityCode(), customer.getCity().getCode(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CustomerMetaDataTranslationKeys.KYC_TEMPLATES));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCustomer.getKycTemplate());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(customer.getKycTemplate().getName());
        handleChanged(approvedCustomer.getKycTemplate(), customer.getKycTemplate().getName(), field);
        dynamicFields.add(field);

        changeHandlerContext.getPortletsAttributes().get(PORTLET_NAME).setVisible(true);
        changeHandlerContext.getPortletsAttributes().get(PORTLET_NAME).setDynamicPortlet(true);
        changeHandlerContext.getPortletsAttributes().get(PORTLET_NAME).setDynamicFields(dynamicFields);
    }

    private void handleChanged(String oldValue, String newValue, DynamicField dynamicField) {
        if (!StringUtils.equals(oldValue, newValue))
            dynamicField.setLabelColor(MODIFIED_COLOR);
    }
}
