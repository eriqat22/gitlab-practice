package com.progressoft.mpay.networkmanagement;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.mpclear.MPClearClientHelper;

public class PostCreateNetworkManagment implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCreateNetworkManagment.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PostCreateNetworkManagment ====");
        String operationType = (String) arg1.get("operationType.code");
        logger.debug("Operation Type=" + operationType);
        MPAY_NetworkManagement oNetworkManagement = (MPAY_NetworkManagement) arg0.get(WorkflowService.WF_ARG_BEAN);
        oNetworkManagement.setOperationType(LookupsLoader.getInstance().getNetworkManagementActionTypes(operationType));
        oNetworkManagement.setDescription(oNetworkManagement.getOperationType().getName());
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).createNetworkManagement(oNetworkManagement);
    }
}