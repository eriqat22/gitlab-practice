package com.progressoft.mpay.registration.corporates;

public class CorporateWorkflowServiceActions {
	public static final String APPROVE = "SVC_Approve";
	public static final String REJECT = "SVC_Reject";
	public static final String DELETE = "SVC_Delete";
	public static final String DELETE_APPROVAL = "SVC_DeleteApproval";

	private CorporateWorkflowServiceActions() {

	}
}
