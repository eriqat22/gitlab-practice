package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.MPAYView;

public class PostRejectCorporate implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostRejectCorporate.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute ============");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		corporate.setIsActive(false);
		handleServices(corporate.getId());
	}

	private void handleServices(long corporateId) throws WorkflowException {
		logger.debug("Inside HandleServices ============");
		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporateId);
		if (services == null || services.isEmpty())
			return;
		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			if (service.getStatusId() != null && service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.APPROVAL.toString())
					|| service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.INTEGRATION.toString()))
				JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.REJECT, new WorkflowException());
		} while (servicesIterator.hasNext());
	}
}
