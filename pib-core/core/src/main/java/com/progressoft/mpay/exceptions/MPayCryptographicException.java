package com.progressoft.mpay.exceptions;

public class MPayCryptographicException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public MPayCryptographicException(Exception e) {
		super(e);
	}

}
