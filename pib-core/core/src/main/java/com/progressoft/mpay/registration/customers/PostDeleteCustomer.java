package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.MPAYView;

public class PostDeleteCustomer implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostDeleteCustomer.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside PostDeleteCustomer ====================");

		MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (customer.getIsRegistered())
			JfwHelper.executeAction(MPAYView.CUSTOMER, customer, CustomerWorkflowServiceActions.DELETE_APPROVAL,
					new WorkflowException());
		else {
			deleteCustomerMobiles(customer);
			JfwHelper.executeAction(MPAYView.CUSTOMER, customer, CustomerWorkflowServiceActions.DELETE,
					new WorkflowException());
		}
	}

	private void deleteCustomerMobiles(MPAY_Customer customer) throws WorkflowException {
		List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId());
		if (mobiles == null || mobiles.isEmpty())
			return;
		Iterator<MPAY_CustomerMobile> mobilesIterator = mobiles.listIterator();
		do {
			MPAY_CustomerMobile mobile = mobilesIterator.next();
			JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, CustomerMobileWorkflowServiceActions.DELETE,
					new WorkflowException());
		} while (mobilesIterator.hasNext());
	}
}
