package com.progressoft.mpay.entity;

public final class BankType {
	public static final String SETTLEMENT = "1";
	public static final String PARTICIPANT = "2";

	private BankType() {

	}
}
