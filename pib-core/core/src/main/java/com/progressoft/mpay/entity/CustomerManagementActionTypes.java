package com.progressoft.mpay.entity;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_CustIntegActionType;

public enum CustomerManagementActionTypes {
	ADD_CLIENT("ADCL"), ADD_ACCOUNT("ADAC"), UPDATE_CLIENT("UDCL"), REMOVE_CLIENT("RMCL"), REMOVE_ACCOUNT("RMAC"), CHANGE_ALIAS(
			"CHAL"), SET_DEFAULT_ACCOUNT("DFLT");

	private final String value;

	CustomerManagementActionTypes(String value) {
		this.value = value;
	}

	
	public boolean isEqual(MPAY_CustIntegActionType actionType) {
		if(actionType == null)
			throw new NullArgumentException("actionType");
		return actionType.getCode().equals(this.value);
	}

	@Override
	public String toString() {
		return value;
	}
}
