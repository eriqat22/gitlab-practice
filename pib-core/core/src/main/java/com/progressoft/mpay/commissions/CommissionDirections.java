package com.progressoft.mpay.commissions;

public enum CommissionDirections {
	NONE("NONE"), SENDER("SENDER"), RECEIVER("RECEIVER"), BOTH("BOTH");

	private String value;

	private CommissionDirections(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
