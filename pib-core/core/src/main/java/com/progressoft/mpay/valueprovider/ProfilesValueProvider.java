package com.progressoft.mpay.valueprovider;

import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.*;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ProfilesValueProvider {
    public Map<String, String> getByKyc(String kycTemplateId) {
        if (!validId(kycTemplateId))
            return Collections.emptyMap();

        return getProfilesByTemplate(kycTemplateId);
    }

    public Map<String, String> getByCustomerParent(String parentId) {
        if (!validId(parentId))
            return Collections.emptyMap();

        List<MPAY_Customer> customers = MPayContext.getInstance().getDataProvider().getEntitiesWhere(MPAY_Customer.class, "id = " + parentId);
        if (!customers.isEmpty())
            return getProfilesByTemplate(customers.get(0).getKycTemplate().getId().toString());

        List<MPAY_CustomerMobile> mobiles = MPayContext.getInstance().getDataProvider().getEntitiesWhere(MPAY_CustomerMobile.class, "id = " + parentId);
        if (!mobiles.isEmpty())
            return getProfilesByTemplate(mobiles.get(0).getRefCustomer().getKycTemplate().getId().toString());

        List<MPAY_MobileAccount> accounts = MPayContext.getInstance().getDataProvider().getEntitiesWhere(MPAY_MobileAccount.class, "id = " + parentId);
        if (!accounts.isEmpty())
            return getProfilesByTemplate(accounts.get(0).getMobile().getRefCustomer().getKycTemplate().getId().toString());

        return Collections.emptyMap();
    }

    public Map<String, String> getByCorporateParent(String parentId) {
        if (!validId(parentId))
            return Collections.emptyMap();

        List<MPAY_Corporate> corporates = MPayContext.getInstance().getDataProvider().getEntitiesWhere(MPAY_Corporate.class, "id = " + parentId);
        if (!corporates.isEmpty())
            return getProfilesByTemplate(corporates.get(0).getKycTemplate().getId().toString());

        List<MPAY_CorpoarteService> services = MPayContext.getInstance().getDataProvider().getEntitiesWhere(MPAY_CorpoarteService.class, "id = " + parentId);
        if (!services.isEmpty())
            return getProfilesByTemplate(services.get(0).getRefCorporate().getKycTemplate().getId().toString());

        List<MPAY_ServiceAccount> accounts = MPayContext.getInstance().getDataProvider().getEntitiesWhere(MPAY_ServiceAccount.class, "id = " + parentId);
        if (!accounts.isEmpty())
            return getProfilesByTemplate(accounts.get(0).getService().getRefCorporate().getKycTemplate().getId().toString());

        return Collections.emptyMap();
    }

    private Map<String, String> getProfilesByTemplate(String kycTemplateId) {
        Map<String, String> profiles = new HashMap<>();
        MPAY_CustomerKyc customerKyc = MPayContext.getInstance().getLookupsLoader().getCustomerKyc(Long.parseLong(kycTemplateId));
        if (Objects.nonNull(customerKyc))
            customerKyc.getProfiles().forEach(p -> profiles.put(p.getId().toString(), p.getName()));
        else {
            MPAY_CorporateKyc corporateKyc = MPayContext.getInstance().getLookupsLoader().getCorporateKyc(Long.parseLong(kycTemplateId));
            corporateKyc.getProfiles().forEach(p -> profiles.put(p.getId().toString(), p.getName()));
        }
        return profiles;
    }

    private boolean validId(String id) {
        return id != null && id.length() > 0 && NumberUtils.isDigits(id);
    }
}
