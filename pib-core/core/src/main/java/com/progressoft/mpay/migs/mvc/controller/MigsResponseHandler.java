package com.progressoft.mpay.migs.mvc.controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.MPAY_BankIntegMessage;
import com.progressoft.mpay.migs.response.MigsResponse;

public class MigsResponseHandler implements MigsHandler {
	private static final Logger logger = LoggerFactory.getLogger(MigsResponseHandler.class);
	@PersistenceContext
	private EntityManager em;

	@Override
	public String handle(MigsContext context) {
		HttpServletRequest response = context.getServletRequest();
		try {
			context.setMigsResponse(prepareMigsResponseFromMigsRequest(response));
			updateIntegrationMessage(context);
			if (isRejectedResponse(context)) {
				return generateRejectedTransactionPage();
			}
		} catch (Exception e) {
			logger.error("Error while handle", e);
			return new MigsErrorHandler().handle(context);
		}
		return generateHtmlSucessPage();
	}

	private MigsResponse prepareMigsResponseFromMigsRequest(HttpServletRequest request) {
		MigsResponse response = new MigsResponse();
		response.setVpcAcqAVSRespCode(request.getParameter(MIGSParameters.VPC_ACQ_AVS_RESP_CODE));
		response.setVpcAcqCSCRespCode(request.getParameter(MIGSParameters.VPC_ACQ_CSC_RESP_CODE));
		response.setVpcAcqResponseCode(request.getParameter(MIGSParameters.VPC_ACQ_RESPONSE_CODE));
		response.setVpcAmount(request.getParameter(MIGSParameters.VPC_AMOUNT));
		response.setVpcAuthorizeId(request.getParameter(MIGSParameters.VPC_AUTHORIZE_ID));
		response.setVpcAVSRequestCode(request.getParameter(MIGSParameters.VPC_AVS_REQUEST_CODE));
		response.setVpcAVSResultCode(request.getParameter(MIGSParameters.VPC_AVS_RESULT_CODE));
		response.setVpcBatchNo(request.getParameter(MIGSParameters.VPC_BATCH_NO));
		response.setVpcCard(request.getParameter(MIGSParameters.VPC_CARD));
		response.setVpcCardNum(request.getParameter(MIGSParameters.VPC_CARD_NUM));
		response.setVpcCommand(request.getParameter(MIGSParameters.VPC_COMMAND));
		response.setVpcCSCResultCode(request.getParameter(MIGSParameters.VPC_CSC_RESULT_CODE));
		response.setVpcLocale(request.getParameter(MIGSParameters.VPC_LOCALE));
		response.setVpcMerchant(request.getParameter(MIGSParameters.VPC_MERCHANT));
		response.setVpcMerchTxnRef(request.getParameter(MIGSParameters.VPC_MERCH_TXN_REF));
		response.setVpcMessage(request.getParameter(MIGSParameters.VPC_MESSAGE));
		response.setVpcOrderInfo(request.getParameter(MIGSParameters.VPC_ORDER_INFO));
		response.setVpcReceiptNo(request.getParameter(MIGSParameters.VPC_RECEIPT_NO));
		response.setVpcSecureHash(request.getParameter(MIGSParameters.VPC_SECURE_HASH));
		response.setVpcTransactionNo(request.getParameter(MIGSParameters.VPC_TRANSACTION_NO));
		response.setVpcTxnResponseCode(request.getParameter(MIGSParameters.VPC_TXN_RESPONSE_CODE));
		response.setVpcVersion(request.getParameter(MIGSParameters.VPC_VERSION));
		return response;
	}

	private void updateIntegrationMessage(MigsContext context) {
		try {
			MPAY_BankIntegMessage integMessage = getIntegrationMessageFromDB(context);
			integMessage.setResponseContent(new MigsMarshaller().marshallMigsObject(context.getMigsResponse()));
			integMessage.setResponseID(context.getMigsResponse().getVpcMerchTxnRef());
			integMessage.setResponseDate(SystemHelper.getSystemTimestamp());
			integMessage.setResponseToken(context.getMigsResponse().getVpcSecureHash());
			if (isRejectedResponse(context)) {
				integMessage.setRefStatus(LookupsLoader.getInstance().getBankIntegStatus(BankIntegrationStatus.REJECTED));
			}
			context.getItemDao().merge(integMessage);
		} catch (Exception e) {
			throw new MigsException(e);
		}
	}

	private MPAY_BankIntegMessage getIntegrationMessageFromDB(MigsContext context) {
		return context.getItemDaoWithoutTenant().getItem(MPAY_BankIntegMessage.class, "requestID", context.getMigsResponse().getVpcMerchTxnRef());
	}

	private boolean isRejectedResponse(MigsContext context) {
		return !MIGSParameters.MIGS_RESPONSE_SUCCESS_CODE.equals(context.getMigsResponse().getVpcTxnResponseCode());
	}

	private String generateRejectedTransactionPage() {
		return MIGSParameters.REJECTED_HMTL_TAGS;
	}

	private String generateHtmlSucessPage() {
		return MIGSParameters.ACCEPTED_HTML_TAGS;
	}
}