package com.progressoft.mpay.registration;

import java.math.BigDecimal;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_AccountType;
import com.progressoft.mpay.entities.MPAY_BalanceType;
import com.progressoft.mpay.entity.AccountsNature;

public class AccountsHelper {

    private AccountsHelper() {
    }

    public static MPAY_Account createAccount(String name, JFWCurrency currency, BigDecimal minBalance, MPAY_AccountType accType, String externalAccount, MPAY_BalanceType balanceType, boolean isBanked)
            throws WorkflowException {
        MPAY_Account account = new MPAY_Account();

        account.setAccNumber(String.valueOf(getNextAccountNumberSequence()));
        account.setAccName(name);
        account.setCurrency(currency);
        account.setMinBalance(minBalance);
        account.setBalance(minBalance);
        account.setAccCount(1L);
        account.setNature(AccountsNature.ASSETS);
        account.setAccountType(accType);
        account.setExternalAcc(externalAccount);
        account.setBalanceType(balanceType);
        account.setIsBanked(isBanked);
        account.setIsActive(true);
        return account;
    }


    private static long getNextAccountNumberSequence() throws WorkflowException {
        return DataProvider.instance().getNewAccountNumberSequence();
    }

    public static AccountsHelper getInstance() {
        return (AccountsHelper) AppContext.getApplicationContext().getBean("AccountsHelper");
    }
}
