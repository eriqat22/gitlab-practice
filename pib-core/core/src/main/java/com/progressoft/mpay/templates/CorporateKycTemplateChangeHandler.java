package com.progressoft.mpay.templates;

import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_CorporateKyc;

import java.util.Objects;

import static com.progressoft.mpay.templates.TemplateBehaviour.*;


public class CorporateKycTemplateChangeHandler implements ChangeHandler {

    @Override
    public void handle(ChangeHandlerContext context) {
        MPAY_Corporate corporate = (MPAY_Corporate) context.getEntity();
        MPAY_CorporateKyc kycTemplate = corporate.getKycTemplate();

        if (Objects.isNull(kycTemplate))
            return;

        KycTemplateHandler handler = new KycTemplateHandler(corporate);
        handler.handleTemplateOf(kycTemplate, VISIBLE, context);
        handler.handleTemplateOf(kycTemplate, ENABLED, context);
        handler.handleTemplateOf(kycTemplate, REQ, context);

        setTemplateDefaultValues(corporate, kycTemplate);
    }

    private void setTemplateDefaultValues(MPAY_Corporate corporate, MPAY_CorporateKyc kycTemplate) {
        corporate.setExpectedActivities(kycTemplate.getExpectedActivitiesDefault());
        corporate.setClientRef(kycTemplate.getClientRefDefault());
        corporate.setRegistrationAuthority(kycTemplate.getRegistrationAuthorityDefault());
        corporate.setRegistrationLocation(kycTemplate.getRegistrationLocationDefault());
        corporate.setChamberId(kycTemplate.getChamberIdDefault());
        corporate.setLegalEntity(kycTemplate.getLegalEntityDefault());
        corporate.setOtherLegalEntity(kycTemplate.getOtherLegalEntityDefault());
        corporate.setPhoneOne(kycTemplate.getPhoneOneDefault());
        corporate.setPhoneTwo(kycTemplate.getPhoneTwoDefault());
        corporate.setPobox(kycTemplate.getPoboxDefault());
        corporate.setZipCode(kycTemplate.getZipCodeDefault());
        corporate.setBuildingNum(kycTemplate.getBuildingNumDefault());
        corporate.setStreetName(kycTemplate.getStreetNameDefault());
        corporate.setLocation(kycTemplate.getLocationDefault());
    }


}
