package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.messages.ProcessingStatusCodes;

public class PostCustomerMobileRegistrationApproval implements FunctionProvider {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(PostCustomerMobileRegistrationApproval.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute --------------------------");
        MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN);
        if (mobile.getIsRegistered())
            updateMobileNumberInAccountName(mobile);
        mobile.setIsRegistered(true);
        applyMobileChanges(mobile);
        handleAccounts(mobile);
        handleRequestMessageIfExist(mobile);
    }

    private void updateMobileNumberInAccountName(MPAY_CustomerMobile mobile) {
        for (MPAY_MobileAccount mobileAccount : mobile.getMobileMobileAccounts()) {
            String[] split = mobileAccount.getRefAccount().getAccName().split(" ");
            String newMobileNumber = split.length > 1 ? mobileAccount.getMobile().getNewMobileNumber() + " " + split[1] : mobileAccount.getMobile().getNewMobileNumber();
            mobileAccount.getRefAccount().setAccName(newMobileNumber);
            DataProvider.instance().updateAccount(mobileAccount.getRefAccount());
        }
    }

    private void handleRequestMessageIfExist(MPAY_CustomerMobile customerMobile) {
        if (customerMobile.getHint() != null && customerMobile.getHint().trim().length() > 0) {
            MPAY_MPayMessage registrationMessage = DataProvider.instance()
                    .getMPayMessage(Long.parseLong(customerMobile.getHint()));
            if (registrationMessage != null) {
                if (!registrationMessage.getProcessingStatus().getCode()
                        .equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED))
                    return;
                registrationMessage.setProcessingStatus(
                        LookupsLoader.getInstance().getProcessingStatus(ProcessingStatusCodes.ACCEPTED));
                DataProvider.instance().mergeMPayMessage(registrationMessage);
            }
        }
    }

    private void handleAccounts(MPAY_CustomerMobile mobile) throws WorkflowException {
        logger.debug("Inside HandleAccounts =============");
        List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId(),
                MobileAccountWorkflowStatuses.INTEGRATION.toString());
        if (accounts == null || accounts.isEmpty())
            return;
        Iterator<MPAY_MobileAccount> accountsIterator = accounts.listIterator();
        do {
            MPAY_MobileAccount account = accountsIterator.next();
            JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.APPROVE,
                    new WorkflowException());
        } while (accountsIterator.hasNext());
    }

    private void applyMobileChanges(MPAY_CustomerMobile mobile) {
        logger.debug("inside ApplyMobileChanges--------------------------");
        if (mobile.getNewMobileNumber() != null) {
            mobile.setMobileNumber(mobile.getNewMobileNumber());
            mobile.setNewMobileNumber(null);
        }
        if (mobile.getNewAlias() != null) {
            mobile.setAlias(mobile.getNewAlias());
            mobile.setNewAlias(null);
        }
        mobile.setApprovedData(null);
    }

}
