package com.progressoft.mpay;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.entities.MPAY_ChargesScheme;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;
import com.progressoft.mpay.entities.MPAY_Commission;
import com.progressoft.mpay.entities.MPAY_CommissionScheme;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_RequestTypesDetail;
import com.progressoft.mpay.entities.MPAY_RequestTypesScheme;
import com.progressoft.mpay.entities.MPAY_TaxScheme;
import com.progressoft.mpay.entities.MPAY_TaxSchemeDetail;
import com.progressoft.mpay.entity.MPAYView;

public class NewMessageTypeHandler {

	private static final Logger logger = LoggerFactory.getLogger(NewMessageTypeHandler.class);
	private static final String APPROVAL_ACTION = "SVC_Approve";
	private static final String CHARGE_SCHEME_APPROVED_STATUS = "101805";
	private static final String LIMIT_APPROVED_STATUS = "102002";
	private static final String REQUEST_TYPE_SCHEME_APPROVED_STATUS = "904605";
	private static final String COMMISSION_APPROVED_STATUS = "211805";
	private static final String TAX_APPROVED_STATUS = "311805";

	private NewMessageTypeHandler() {

	}

	public static void execute(MPAY_MessageType messageType) throws WorkflowException {
		logger.debug("inside execute .......");
		try {
			if (messageType.getIsFinancial()) {
				handleChargeSchemes(messageType);
				handleLimitSchemes(messageType);
				handleCommissionSchemes(messageType);
				handleTaxSchemes(messageType);
			}
			handleRequestTypeSchemes(messageType);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new WorkflowException("An error occured while operator account.", ex);
		}
	}

	private static void handleTaxSchemes(MPAY_MessageType messageType) throws InvalidActionException {
		List<MPAY_TaxScheme> taxSchemes = MPayContext.getInstance().getDataProvider().listTaxSchemes();
		for (MPAY_TaxScheme tax : taxSchemes) {
			MPAY_TaxSchemeDetail chargeDetails = new MPAY_TaxSchemeDetail();
			chargeDetails.setIsActive(false);
			chargeDetails.setRefScheme(tax);
			chargeDetails.setMsgType(messageType);
			chargeDetails.setDescription(messageType.getDescription());
			JfwHelper.createEntity(MPAYView.TAX_SCHEME_DETAILS, chargeDetails);
			if (tax.getStatusId().getCode().equals(TAX_APPROVED_STATUS))
				JfwHelper.executeAction(MPAYView.TAX_SCHEME_DETAILS, chargeDetails, APPROVAL_ACTION,
						new InvalidActionException());
		}
	}

	private static void handleCommissionSchemes(MPAY_MessageType messageType) throws InvalidActionException {
		List<MPAY_CommissionScheme> commissionScheme = MPayContext.getInstance().getDataProvider()
				.listCommissionScheme();
		for (MPAY_CommissionScheme commission : commissionScheme) {
			MPAY_Commission details = new MPAY_Commission();
			details.setIsActive(false);
			details.setRefCommissionScheme(commission);
			details.setMsgType(messageType);
			details.setDescription(messageType.getDescription());
			JfwHelper.createEntity(MPAYView.COMMISSIONS, details);
			if (commission.getStatusId().getCode().equals(COMMISSION_APPROVED_STATUS))
				JfwHelper.executeAction(MPAYView.COMMISSIONS, details, APPROVAL_ACTION, new InvalidActionException());
		}
	}

	private static void handleChargeSchemes(MPAY_MessageType messageType) throws InvalidActionException {
		List<MPAY_ChargesScheme> charges = MPayContext.getInstance().getDataProvider().listChargeSchemes();
		for (MPAY_ChargesScheme chargeScheme : charges) {
			MPAY_ChargesSchemeDetail chargeDetails = new MPAY_ChargesSchemeDetail();
			chargeDetails.setIsActive(false);
			chargeDetails.setRefChargesScheme(chargeScheme);
			chargeDetails.setMsgType(messageType);
			chargeDetails.setDescription(messageType.getDescription());
			chargeDetails.setSendChargeBearerPercent(100L);

			chargeDetails.setRcvChrgMNOBnfcryPrcnt(0L);
			chargeDetails.setRcvrChrgBnkBnfcryPrcnt(0L);
			chargeDetails.setReceiveChargeBearerPercent(0L);
			chargeDetails.setChargeMPClearlPercent(0L);
			chargeDetails.setChargePSPPercent(0L);
			chargeDetails.setPspScope("Onus");
			chargeDetails.setChargeMPClearlPercent(0l);
			chargeDetails.setChargePSPPercent(100l);
			chargeDetails.setRcvChrgMNOBnfcryPrcnt(0l);
			chargeDetails.setSndrChrgMNOBnfcryPrcnt(0l);
			chargeDetails.setRcvrChrgBnkBnfcryPrcnt(0l);
			chargeDetails.setSndrChrgBnBnfcryPrcnt(0l);
			chargeDetails.setRefChargesDetailsChargesSlices(new ArrayList<>());
			chargeDetails.setSndrChrgMNOBnfcryPrcnt(0L);
			chargeDetails.setSndrChrgBnBnfcryPrcnt(0L);
			JfwHelper.createEntity(MPAYView.CHARGES_SCHEME_DETAILS, chargeDetails);
			if (chargeScheme.getStatusId().getCode().equals(CHARGE_SCHEME_APPROVED_STATUS))
				JfwHelper.executeAction(MPAYView.CHARGES_SCHEME_DETAILS, chargeDetails, APPROVAL_ACTION,
						new InvalidActionException());
		}
	}

	private static void handleLimitSchemes(MPAY_MessageType messageType) throws InvalidActionException {
		List<MPAY_LimitsScheme> limitSchemes = MPayContext.getInstance().getDataProvider().listLimitSchemes();
		for (MPAY_LimitsScheme limitScheme : limitSchemes) {
			List<MPAY_Limit> limits = limitScheme.getRefSchemeLimits();
			for (MPAY_Limit limit : limits) {
				MPAY_LimitsDetail limitDetails = new MPAY_LimitsDetail();
				limitDetails.setIsActive(false);
				limitDetails.setTxAmountLimit(BigDecimal.ZERO);
				limitDetails.setTxCountLimit(0L);
				limitDetails.setRefLimit(limit);
				limitDetails.setMsgType(messageType);
				logger.debug("Setting limit details for message " + messageType.getDescription());
				JfwHelper.createEntity(MPAYView.LIMITS_DETAILS, limitDetails);
				if (limit.getStatusId().getCode().equals(LIMIT_APPROVED_STATUS))
					JfwHelper.executeAction(MPAYView.LIMITS_DETAILS, limitDetails, APPROVAL_ACTION,
							new InvalidActionException());
			}
		}
	}

	private static void handleRequestTypeSchemes(MPAY_MessageType messageType) throws InvalidActionException {
		List<MPAY_RequestTypesScheme> requestTypes = MPayContext.getInstance().getDataProvider()
				.listRequestTypeSchemes();
		for (MPAY_RequestTypesScheme requestType : requestTypes) {
			MPAY_RequestTypesDetail details = new MPAY_RequestTypesDetail();
			details.setRefScheme(requestType);
			details.setIsActive(true);
			details.setMsgType(messageType);
			logger.debug("Setting limit details for message " + messageType.getDescription());
			JfwHelper.createEntity(MPAYView.REQUEST_TYPES_DETAILS, details);
			if (requestType.getStatusId().getCode().equals(REQUEST_TYPE_SCHEME_APPROVED_STATUS))
				JfwHelper.executeAction(MPAYView.REQUEST_TYPES_DETAILS, details, APPROVAL_ACTION,
						new InvalidActionException());
		}
	}
}