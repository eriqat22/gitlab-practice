package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.SystemNetworkStatus;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_Corporate;

public class ValidateCorporate implements Validator {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ValidateCorporate.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("validate ===========");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		validateSystemLoggedOut();
		validateMetaData(corporate);
	}

	private void validateMetaData(MPAY_Corporate corporate) throws InvalidInputException {
		InvalidInputException iie = new InvalidInputException();
		Map<String, Object> keyValuePair = new HashedMap();
		keyValuePair.put("registrationId", corporate.getRegistrationId());
		keyValuePair.put("refCountry.id", corporate.getRefCountry().getId());
		keyValuePair.put("idType.id", corporate.getIdType().getId());
		Unique.validate(itemDao, iie, corporate, keyValuePair, MPayErrorMessages.DUPLICATE_REGISTRATION_ID);
		if (iie.getErrors().size() > 0) {
			throw iie;
		}
	}

	public void validateSystemLoggedOut() throws InvalidInputException {
		if (SystemNetworkStatus.isSystemOnline(DataProvider.instance()))
			return;
		InvalidInputException iie = new InvalidInputException();
		iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.SYSTEM_IS_LOGGED_OUT));
		throw iie;
	}
}