package com.progressoft.mpay.messages;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.IServiceUserManager;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_BulkPayment;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessor;

public class MessageProcessorHelper {
	private static final Logger logger = LoggerFactory.getLogger(MessageProcessorHelper.class);
	private static HashMap<String, MessageProcessor> processors = new HashMap<>();

	private MessageProcessorHelper() {

	}

	public static MPAY_MPayMessage createMessage(IServiceUserManager serviceUserManager, MessageProcessingContext context, String processingStatusCode, MPAY_Reason reason, String reasonDescription,
			MPAY_BulkPayment bulkPayment) {
		logger.debug("Inside CreateMessage ...");
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		MPAY_MPayMessage message = new MPAY_MPayMessage();
		message.setRefOperation(context.getOperation());
		message.setMessageID(context.getRequest().getMsgId());
		message.setSender(context.getRequest().getSender());
		message.setRequestContent(context.getOriginalRequest());
		message.setRequestToken(context.getToken());
		message.setMessageType(context.getOperation().getMessageType());
		message.setProcessingStamp(SystemHelper.getSystemTimestamp());
		message.setProcessingStatus(status);
		message.setReason(reason);
		message.setReasonDesc(reasonDescription);
		message.setTenantId(serviceUserManager.getServiceTenant().getName());
		message.setOrgId(Long.parseLong(serviceUserManager.getServiceTenant().getSuperOrgId()));
		message.setCreatedBy(serviceUserManager.getUserName());
		message.setCreationDate(SystemHelper.getSystemTimestamp());
		message.setUpdatedBy(message.getCreatedBy());
		message.setUpdatingDate(message.getCreationDate());
		message.setReference(String.valueOf(context.getDataProvider().getNextReferenceNumberSequence()));
		message.setRequestedId(context.getRequest().getRequestedId());
		message.setShopId(context.getRequest().getShopId());
		message.setChannelId(context.getRequest().getChannelId());
		message.setWalletId(context.getRequest().getWalletId());
		message.setBulkPayment(bulkPayment);
		return message;
	}

	public static MessageProcessor getProcessor(MPAY_EndPointOperation operation) {
		logger.debug("Inside GetProcessor....");
		if (operation == null)
			return null;
		logger.debug("operation = " + operation.getOperation());
		logger.debug("processor name = " + operation.getProcessor());
		try {
			if (!processors.containsKey(operation.getOperation())) {
				Class<?> cls = Class.forName(operation.getProcessor());
				processors.put(operation.getOperation(), (MessageProcessor) cls.newInstance());
			}
			return processors.get(operation.getOperation());
		} catch (Exception e) {
			logger.error("Error in GetProcessor", e);
			return null;
		}
	}

	public static void flush() {
		processors.clear();
	}
}
