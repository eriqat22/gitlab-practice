package com.progressoft.mpay.registration.corporates.services;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_AccountCategory;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entity.AccountCategory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

public class SetFieldRequiredChangeHandler implements ChangeHandler {

    @Autowired
    ItemDao itemDao;
    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {

        changeHandlerContext.getFieldsAttributes().get("description").setRequired(true);
        changeHandlerContext.getFieldsAttributes().get("serviceCategory").setRequired(true);
        changeHandlerContext.getFieldsAttributes().get("serviceType").setRequired(true);

        if (changeHandlerContext.getEntity() instanceof MPAY_CorpoarteService) {
            MPAY_CorpoarteService service = (MPAY_CorpoarteService) changeHandlerContext.getEntity();
            if (service.getIsRegistered() != null && service.getIsRegistered()) {
                if (changeHandlerContext.getFieldsAttributes().get("refCorporate") != null)
                    changeHandlerContext.getFieldsAttributes().get("refCorporate").setEnabled(false);
                changeHandlerContext.getFieldsAttributes().get("alias").setEnabled(false);
            }
        }

        handleProvidedServices(changeHandlerContext);

    }

    private void handleProvidedServices(ChangeHandlerContext changeHandlerContext) {
        MPAY_CorpoarteService service = (MPAY_CorpoarteService) changeHandlerContext.getEntity();
        MPAY_AccountCategory category = service.getCategory();
        if (Objects.nonNull(category)) {
            boolean isAgent = category.getCode().equals(AccountCategory.AGENT);
            changeHandlerContext.getFieldsAttributes().get(MPAY_CorpoarteService.SERVICES).setVisible(isAgent);
        }

        MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "code", MPayContext.getInstance().getSystemParameters().getDefaultBankCode());
        service.setBank(defaultBank);
    }
}