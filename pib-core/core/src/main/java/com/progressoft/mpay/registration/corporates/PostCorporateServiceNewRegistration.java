package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostCorporateServiceNewRegistration implements FunctionProvider {
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
		service.setIsRegistered(true);
		MPAY_ServiceAccount account = service.getServiceServiceAccounts().get(0);
		if (!"990005".equals(account.getStatusId().getCode()))
			JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS, account, "SVC_Approve", new WorkflowException());
	}
}
