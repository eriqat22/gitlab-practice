package com.progressoft.mpay.tax;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JfwDraft;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_TaxScheme;
import com.progressoft.mpay.entities.MPAY_TaxSchemeDetail;

public class CheckIfTaxInEditMode implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfTaxInEditMode.class);
	private static final String TAX_SCHEME_EDIT_NEW_STATUS_CODE = "311804";
	private static final String TAX_SCHEME_EDIT_APPROVED_STATUS_CODE = "311806";
	private static final String TAX_SCHEME_EDIT_REJECTED_STATUS_CODE = "311809";

	@Autowired
	private ItemDao itemDao;
	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_TaxSchemeDetail details = (MPAY_TaxSchemeDetail) transientVar.get(WorkflowService.WF_ARG_BEAN);
		if(details.getRefScheme().getJfwDraft() == null)
			return false;
		JfwDraft schemeDraft = MPayContext.getInstance().getDataProvider().getJFWDraft(details.getRefScheme().getJfwDraft().getId());
		if (schemeDraft == null)
			return true;
		MPAY_TaxScheme scheme = (MPAY_TaxScheme) JfwHelper.createObjectFromDraft(schemeDraft.getDraftData(), new MPAY_TaxScheme(), itemDao);
		return scheme.getStatusId().getCode().equals(TAX_SCHEME_EDIT_NEW_STATUS_CODE) || scheme.getStatusId().getCode().equals(TAX_SCHEME_EDIT_APPROVED_STATUS_CODE) || scheme.getStatusId().getCode().equals(TAX_SCHEME_EDIT_REJECTED_STATUS_CODE);
	}
}