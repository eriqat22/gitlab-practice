package com.progressoft.mpay.bulkregistration.condition;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;

public class HideCustomerCreation implements Condition {

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		return false;
	}
}