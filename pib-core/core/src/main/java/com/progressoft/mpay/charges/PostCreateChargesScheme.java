package com.progressoft.mpay.charges;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_ChargesScheme;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entity.MPAYView;

public class PostCreateChargesScheme implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCreateChargesScheme.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		try {
			logger.debug("Inside PostCreateChargesScheme----------------");
			MPAY_ChargesScheme scheme = (MPAY_ChargesScheme) arg0.get(WorkflowService.WF_ARG_BEAN);

			List<MPAY_MessageType> msgTypes = LookupsLoader.getInstance().getMessageTypes();
			for (MPAY_MessageType messageType : msgTypes) {
				if (!messageType.getIsFinancial())
					continue;
				MPAY_ChargesSchemeDetail chargeDetails = new MPAY_ChargesSchemeDetail();
				chargeDetails.setIsActive(false);
				chargeDetails.setRefChargesScheme(scheme);
				chargeDetails.setMsgType(messageType);
				chargeDetails.setDescription(messageType.getDescription());
				chargeDetails.setSendChargeBearerPercent(100L);
				chargeDetails.setReceiveChargeBearerPercent(0L);
				chargeDetails.setChargeMPClearlPercent(0l);
				chargeDetails.setSndrChrgBnBnfcryPrcnt(0l);
				chargeDetails.setSndrChrgMNOBnfcryPrcnt(0l);
				chargeDetails.setChargePSPPercent(0L);
				chargeDetails.setRcvChrgMNOBnfcryPrcnt(0l);
				chargeDetails.setRcvrChrgBnkBnfcryPrcnt(0L);

				chargeDetails.setPspScope("Onus");
				JfwHelper.createEntity(MPAYView.CHARGES_SCHEME_DETAILS, chargeDetails);
				logger.debug("Setting charge details for message " + messageType.getDescription());
			}
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}
}
