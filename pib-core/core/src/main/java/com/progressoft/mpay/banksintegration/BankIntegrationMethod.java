package com.progressoft.mpay.banksintegration;

public enum BankIntegrationMethod {
    NONE,
    ACCOUNT_WITHDRAWAL,
    ACCOUNT_DEPOSIT,
    ACCOUNT_TRANSFER,
    REVERSAL,
    CHARGE,
    BALANCE_INQUIRY
}
