package com.progressoft.mpay.messages;

import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.mpay.ICryptographer;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.notifications.INotificationHelper;

public interface ServicesFactory {

	IDataProvider getDataProvider();

	ISystemParameters systemParameters();

	ILookupsLoader getLookupsLoader();

	ICryptographer getCryptographer();

	INotificationHelper getNotificationHelper();

	Communicator getcommunicator();
}
