package com.progressoft.mpay;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author U484
 *
 */

public class SHA1Hashing {

	private static final Logger logger = LoggerFactory.getLogger(SHA1Hashing.class);
	private SHA1Hashing() {

	}
	public static String sha1hashing(String stringObject) {
		logger.debug("Insid SHA1Hashing ..........");

		String hashedObject = null;
		try {
			MessageDigest msgDig = MessageDigest.getInstance("SHA-1");
			msgDig.reset();
			msgDig.update(stringObject.getBytes());
			hashedObject = new String(Base64.encodeBase64(msgDig.digest()));
		} catch (NoSuchAlgorithmException e) {
			logger.error("Error .... change me later", e);
			return null;
		}

		return hashedObject;
	}
}