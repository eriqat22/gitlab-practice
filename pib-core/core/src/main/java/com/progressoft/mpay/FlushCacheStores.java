package com.progressoft.mpay;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.Util;

public class FlushCacheStores implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(FlushCacheStores.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("flush cache stores....");
        String cacheList = "mpay.IDType,mpay.City,mpay.Language,mpay.TransactionsConfig,mpay.Reason";
        for (String aCacheArray : cacheList.split(",")) {
            Util.removeCacheByName(aCacheArray);
        }

    }

}
