package com.progressoft.mpay.entity;

import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class PSPCache {

	private Long id;
	private String name;
	private MPAY_CorpoarteService pspChargeService;
	private MPAY_CorpoarteService pspMPClearService;
	private MPAY_CorpoarteService pspCashInService;
	private MPAY_CorpoarteService pspCashOutService;
	private MPAY_CorpoarteService pspCommissionsService;
	private MPAY_CorpoarteService pspTaxesService;
	private MPAY_Account pspChargeAccount;
	private MPAY_Account pspMPClearAccount;
	private MPAY_Account pspCashInAccount;
	private MPAY_Account pspCashOutAccount;
	private MPAY_Account pspCommissionsAccount;
	private MPAY_Account pspTaxesAccount;
	private String nationalID;

	private long clientTypeId;

	public MPAY_CorpoarteService getPspChargeService() {
		return pspChargeService;
	}

	public void setPspChargeService(MPAY_CorpoarteService pspChargeService) {
		this.pspChargeService = pspChargeService;
	}

	public MPAY_CorpoarteService getPspMPClearService() {
		return pspMPClearService;
	}

	public void setPspMPClearService(MPAY_CorpoarteService pspMPClearService) {
		this.pspMPClearService = pspMPClearService;
	}

	public MPAY_CorpoarteService getPspCashInService() {
		return pspCashInService;
	}

	public void setPspCashInService(MPAY_CorpoarteService pspCashInService) {
		this.pspCashInService = pspCashInService;
	}

	public MPAY_CorpoarteService getPspCashOutService() {
		return pspCashOutService;
	}

	public void setPspCashOutService(MPAY_CorpoarteService pspCashOutService) {
		this.pspCashOutService = pspCashOutService;
	}

	public MPAY_CorpoarteService getPspCommissionsService() {
		return pspCommissionsService;
	}

	public void setPspCommissionsService(MPAY_CorpoarteService pspCommissionsService) {
		this.pspCommissionsService = pspCommissionsService;
	}

	public MPAY_CorpoarteService getPspTaxesService() {
		return pspTaxesService;
	}

	public void setPspTaxesService(MPAY_CorpoarteService pspTaxesService) {
		this.pspTaxesService = pspTaxesService;
	}

	public MPAY_Account getPspChargeAccount() {
		return pspChargeAccount;
	}

	public void setPspChargeAccount(MPAY_Account pspChargeAccount) {
		this.pspChargeAccount = pspChargeAccount;
	}

	public MPAY_Account getPspMPClearAccount() {
		return pspMPClearAccount;
	}

	public void setPspMPClearAccount(MPAY_Account pspMPClearAccount) {
		this.pspMPClearAccount = pspMPClearAccount;
	}

	public MPAY_Account getPspCashInAccount() {
		return pspCashInAccount;
	}

	public void setPspCashInAccount(MPAY_Account pspCashInAccount) {
		this.pspCashInAccount = pspCashInAccount;
	}

	public MPAY_Account getPspCashOutAccount() {
		return pspCashOutAccount;
	}

	public void setPspCashOutAccount(MPAY_Account pspCashOutAccount) {
		this.pspCashOutAccount = pspCashOutAccount;
	}

	public Long getId() {
		return id;
	}

	public MPAY_Account getPspCommissionsAccount() {
		return pspCommissionsAccount;
	}

	public void setPspCommissionsAccount(MPAY_Account pspCommissionsAccount) {
		this.pspCommissionsAccount = pspCommissionsAccount;
	}

	public MPAY_Account getPspTaxesAccount() {
		return pspTaxesAccount;
	}

	public void setPspTaxesAccount(MPAY_Account pspTaxesAccount) {
		this.pspTaxesAccount = pspTaxesAccount;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNationalID() {
		return nationalID;
	}

	public void setNationalID(String nationalID) {
		this.nationalID = nationalID;
	}

	public long getClientTypeId() {
		return clientTypeId;
	}

	public void setClientTypeId(long clientTypeId) {
		this.clientTypeId = clientTypeId;
	}
}
