package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.MPayContext;
import org.springframework.beans.factory.annotation.Autowired;

public class MobileAccountsViewChangeHandler implements ChangeHandler {

    private static final Logger logger = LoggerFactory.getLogger(MobileAccountsViewChangeHandler.class);

    @Autowired
    ItemDao itemDao;

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside MobileAccountsViewChangeHandler*******************-----------------");


        MPAY_MobileAccount account = (MPAY_MobileAccount) changeHandlerContext.getEntity();

        if (account.getStatusId() == null || account.getNewExternalAcc() == null)
            changeHandlerContext.getFieldsAttributes().get("newExternalAcc").setVisible(false);
        else
            changeHandlerContext.getFieldsAttributes().get("newExternalAcc").setVisible(true);

        if (account.getStatusId() == null || account.getNewProfile() == null)
            changeHandlerContext.getFieldsAttributes().get("newProfile").setVisible(false);
        else
            changeHandlerContext.getFieldsAttributes().get("newProfile").setVisible(true);
        MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "code", MPayContext.getInstance().getSystemParameters().getDefaultBankCode());
        account.setBank(defaultBank);

        try {
            CustomersChangeHandler handler = (CustomersChangeHandler) Class.forName(MPayContext.getInstance().getSystemParameters().getCustomersChangeHandler()).newInstance();
            handler.handleMobileAccount(changeHandlerContext);
        } catch (Exception e) {
            logger.error("Failed to create change handler instance", e);
            throw new InvalidInputException(e.getMessage());
        }
    }
}