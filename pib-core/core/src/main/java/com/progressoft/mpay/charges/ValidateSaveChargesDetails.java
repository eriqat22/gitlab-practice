package com.progressoft.mpay.charges;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;

public class ValidateSaveChargesDetails implements Validator {

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        MPAY_ChargesSchemeDetail detail = (MPAY_ChargesSchemeDetail) arg0.get(WorkflowService.WF_ARG_BEAN);
        if (detail.getDraftStatus() != null)
            detail = (MPAY_ChargesSchemeDetail) arg0.get(WorkflowService.WF_ARG_BEAN_DRAFT);
        InvalidInputException iie = new InvalidInputException();

//		if ((detail.getSendChargeBearerPercent() + detail.getReceiveChargeBearerPercent()) != 100) {
//			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("charges.invalid.percentage"));
//			throw iie;
//		}

        if ((detail.getSendChargeBearerPercent() > 100) || (detail.getReceiveChargeBearerPercent()) > 100) {
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("charges.invalid.percentage"));
            throw iie;
        }


        long totalPercentage = detail.getRcvChrgMNOBnfcryPrcnt() + detail.getSndrChrgMNOBnfcryPrcnt() + detail.getSndrChrgBnBnfcryPrcnt() + detail.getRcvrChrgBnkBnfcryPrcnt() + detail.getChargePSPPercent() + detail.getChargeMPClearlPercent();
        if (totalPercentage != 100) {
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("charges.invalid.percentage"));
            throw iie;
        }
    }
}
