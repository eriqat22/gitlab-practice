
package com.progressoft.mpay.webclient.request;


import com.progressoft.mpay.webclient.model.ResponseModel;

public interface MainResponse {

    void onSuccessResponse(String operation, ResponseModel responseModel);

    void onFailureResponse(Throwable throwable);

    void onPartiallyAccepted(String operation, ResponseModel responseModel);

    void onRejectedResponse(String operation, ResponseModel responseModel);

    void onSignOut(ResponseModel responseModel);

    void onFailed(ResponseModel responseModel);

    void invalidMessage();

    void parsingError();
}
