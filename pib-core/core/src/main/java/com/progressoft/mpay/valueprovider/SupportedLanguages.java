package com.progressoft.mpay.valueprovider;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Language;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SupportedLanguages {

    public Map<Long, String> getLanguages() {
        Map<Long,String> languagesMap = new HashMap<>();
        List<MPAY_Language> languges = LookupsLoader.getInstance().getLanguages();
        languges.forEach(l -> languagesMap.put( l.getId() , String.valueOf(l.getCode())));
        return languagesMap;
        
        
    }
}
