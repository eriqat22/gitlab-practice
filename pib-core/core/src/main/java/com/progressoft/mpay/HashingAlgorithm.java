package com.progressoft.mpay;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HashingAlgorithm {
	private static final Logger logger = LoggerFactory.getLogger(HashingAlgorithm.class);

	private HashingAlgorithm() {

	}

	public static String hash(ISystemParameters systemParameters, String stringObject) {
		logger.debug("Inside Hash ..........");
		String algorithm = null;
		String hashedObject = null;
		try {
			algorithm = systemParameters.getHashingAlogorithm();
			MessageDigest msgDig = MessageDigest.getInstance(algorithm);
			msgDig.reset();
			msgDig.update(stringObject.getBytes(Charset.forName(systemParameters.getDefaultEncoding())));
			hashedObject = new String(Base64.encodeBase64(msgDig.digest()));
		} catch (NoSuchAlgorithmException e) {
			logger.error("Hashing algorithm not supported, algorithm: " + algorithm, e);
			return null;
		} catch (Exception ex) {
			logger.error("Exception while creating instance from hashing algorithm", ex);
			return null;
		}

		return hashedObject;
	}
}
