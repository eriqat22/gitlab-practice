package com.progressoft.mpay.registration;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Intg_Corp_Reg_Instr;
import com.progressoft.mpay.entities.MPAY_Intg_Reg_File;
import com.progressoft.mpay.entities.MPAY_Reg_File_Stt;

public class HandleProcessedCorpInstruction implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(HandleProcessedCorpInstruction.class);

	protected EntityManager em;
	@Autowired
	private ItemDao itemDao;

	@PersistenceContext(unitName = "JFWUnit")
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside HandleProcessedCorpInstruction...");
		MPAY_Intg_Corp_Reg_Instr inst = (MPAY_Intg_Corp_Reg_Instr) transientVars.get(WorkflowService.WF_ARG_BEAN);
		MPAY_Intg_Reg_File file = inst.getRefRegFile();
		long processedCount = 0;
		try {
			processedCount = FileRegistrationHelper.callUpdateProcessedCountSP(file, em);
			logger.debug("stored procedure-processed count=" + processedCount);
			file.setRegFileProcessedRecords(processedCount);
			itemDao.merge(file);
		} catch (SQLException e) {
			logger.error("error... change me later", e);
			throw new WorkflowException(e);
		}

		if (file.getRegFileProcessedRecords() == file.getRegFileRecords()) {
			logger.debug("Note: Number of file's records equal processed records.");
			modifyFileStts(file);
			JfwHelper.sendToActiveMQ(file, Boolean.FALSE);
		}
	}

	private void modifyFileStts(MPAY_Intg_Reg_File file) {
		List<MPAY_Intg_Corp_Reg_Instr> insts = file.getRefRegFileIntgCorpRegInstrs();
		long size = insts.size();

		long successSize = insts.parallelStream().filter(f -> "SUCS".equals(f.getRegInstrProcessingStts().getCode())).count();
		long rejectSize = insts.parallelStream().filter(f -> "RJCT".equals(f.getRegInstrProcessingStts().getCode())).count();
		MPAY_Reg_File_Stt status;
		if (size == successSize) {
			logger.debug("going to change file status into 'ACPT'");
			status = LookupsLoader.getInstance().getRegFileStatus("ACPT");
		} else if (size == rejectSize) {
			logger.debug("going to change file status into 'RJCT'");
			status = LookupsLoader.getInstance().getRegFileStatus("RJCT");
		} else {
			logger.debug("going to change file status into 'PART'");
			status = LookupsLoader.getInstance().getRegFileStatus("PART");
		}
		file.setRegFileProcessingStts(status);
		itemDao.merge(file);
	}
}
