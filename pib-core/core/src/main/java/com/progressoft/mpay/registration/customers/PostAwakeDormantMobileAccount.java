package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclear.IMPClearClient;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Map;

import static java.util.Objects.nonNull;

@Component
public class PostAwakeDormantMobileAccount implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostAwakeDormantMobileAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_MobileAccount account = (MPAY_MobileAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
		account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
	}
}
