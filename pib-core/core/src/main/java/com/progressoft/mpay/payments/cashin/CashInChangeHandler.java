package com.progressoft.mpay.payments.cashin;

import com.progressoft.mpay.payments.CashRequestHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entity.ReasonCodes;

public class CashInChangeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(CashInChangeHandler.class);
	private static final String CALCULATION_STEP = "100506";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {

		logger.debug("***inside CashInChangeHandler : " + changeHandlerContext.getSource());
		MPAY_CashIn cashIn = (MPAY_CashIn) changeHandlerContext.getEntity();
		cashIn.setRefMobileAccount(CashRequestHelper.prepareRefMobileAccount(cashIn.getRefMobileAccount()));
		cashIn.setReasonDesc("");
		if(cashIn.getStatusId() == null) {
			cashIn.setCurrency(MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
		}
		if (cashIn.getStatusId() != null && CALCULATION_STEP.equals(cashIn.getStatusId().getCode())) {
			changeHandlerContext.getFieldsAttributes().get("refCustMob").setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get("refMobileAccount").setEnabled(false);
		}
	}

}