package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.AppValidationException;
import com.progressoft.jfw.workflow.WfContext;
import com.progressoft.jfw.workflow.WfValidator;
import com.progressoft.mpay.entities.MPAY_Customer;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CustomerEnableChannelsValidator extends WfValidator<MPAY_Customer> {

    @Override
    public void validate(WfContext<MPAY_Customer> wfContext) {
        MPAY_Customer customer = wfContext.getEntity();

        boolean isSMSEnabled = customer.getEnableSMS() != null && customer.getEnableSMS();
        if(isInvalidChannelReceiver(isSMSEnabled, customer.getMobileNumber()))
            throw new AppValidationException("Please enter mobile number");

        boolean isEmailEnabled = customer.getEnableEmail() != null && customer.getEnableEmail();
        if(isInvalidChannelReceiver(isEmailEnabled, customer.getEmail()))
            throw new AppValidationException("Please enter email address");
    }

    private boolean isInvalidChannelReceiver(Boolean isEnabled, String receiver) {
        return isEnabled && Objects.isNull(receiver);
    }
}
