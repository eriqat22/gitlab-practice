package com.progressoft.mpay;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CustomerKyc;
import com.progressoft.mpay.entity.MPAYView;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;


@Component
public class DeleteItemPostFunction implements FunctionProvider {

    @Resource
    private ItemDao itemDao;

    @Override
    public void execute(Map transientVars, Map args, PropertySet propertySet) throws WorkflowException {
        if (MPAYView.CUSTOMER_KYC.viewName.equals(transientVars.get("viewName"))) {
            MPAY_CustomerKyc customerKyc = (MPAY_CustomerKyc) transientVars.get(WorkflowService.WF_ARG_BEAN);
            itemDao.getEntityManager().createNativeQuery("DELETE MPAY_CUSTOMERKYCPROFILES WHERE CUSTOMERKYC_ID='" + customerKyc.getId() + "'").executeUpdate();
        }
    }
}
