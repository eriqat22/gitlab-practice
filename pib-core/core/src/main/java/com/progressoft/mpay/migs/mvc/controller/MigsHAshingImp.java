package com.progressoft.mpay.migs.mvc.controller;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.progressoft.mpay.migs.request.MigsRequest;

public class MigsHAshingImp extends MigsHashing{

	private static final String EQUAL = "=";
	private static final String MIGS_SECRET_KEY = "HmacSHA256";
	private static final String MIGS_ISO = "ISO-8859-1";
	private static final String AND = "&";
	private static final char[] HEX_TABLE = new char[] {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

	public MigsHAshingImp(MigsContext context) {
		super(context);
	}

	@Override
	public String generateHasing(MigsRequest migsRequest) {
		try {
			StringBuilder builder = prepareMigsURLHashingValues(migsRequest);
			byte[] bytesToHash = fromHexString(migsRequest.getServiceType().getSecureSecret(), 0, migsRequest.getServiceType().getSecureSecret().length());
			SecretKey key = new SecretKeySpec(bytesToHash, MIGS_SECRET_KEY);
			byte[] mac = getBytesFromMac(builder, key);
			return hex(mac);
		} catch (Exception exception) {
			throw new MigsException(exception);
		}
	}

	private byte[] getBytesFromMac(StringBuilder builder, SecretKey key)
			throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
		Mac m = intilizeMac(builder, key);
		return m.doFinal();
	}

	private Mac intilizeMac(StringBuilder builder, SecretKey key)
			throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
		Mac m = Mac.getInstance(MIGS_SECRET_KEY);
		m.init(key);
		m.update(builder.toString().getBytes(MIGS_ISO));
		return m;
	}

	private StringBuilder prepareMigsURLHashingValues(MigsRequest migsRequest) {
		StringBuilder builder = new StringBuilder();
		builder.append(MIGSParameters.VPC_ACCESS_CODE).append(EQUAL).append(migsRequest.getServiceType().getVpcAccessCode()).append(AND)
				.append(MIGSParameters.VPC_AMOUNT).append(EQUAL).append(migsRequest.getVpcAmount()).append(AND)
				.append(MIGSParameters.VPC_COMMAND).append(EQUAL).append(context.getSystemParameters().getMigsVpcCommand()).append(AND)
				.append(MIGSParameters.VPC_CURRENCY).append(EQUAL).append(context.getSystemParameters().getMigsVpcCurrency()).append(AND)
				.append(MIGSParameters.VPC_LOCALE).append(EQUAL).append(migsRequest.getVpcLocale()).append(AND)
				.append(MIGSParameters.VPC_MERCH_TXN_REF).append(EQUAL).append(migsRequest.getVpcMerchTxnRef()).append(AND)
				.append(MIGSParameters.VPC_MERCHANT).append(EQUAL).append(migsRequest.getServiceType().getVpcMerchant()).append(AND)
				.append(MIGSParameters.VPC_ORDER_INFO).append(EQUAL).append(migsRequest.getVpcOrderInfo()).append(AND)
				.append(MIGSParameters.VPC_RETURN_URL).append(EQUAL).append(migsRequest.getVpcReturnURL()).append(AND)
				.append(MIGSParameters.VPC_VERSION).append(EQUAL).append(context.getSystemParameters().getMigsVpcVersion());
		return builder;
	}

	public static byte[] fromHexString(String s, int offset, int length) {
		if (oddLength(length))
			return new byte[0];
		byte[] byteArray = new byte[length / 2];
		int j = 0;
		int end = offset + length;
		for (int i = offset; i < end; i += 2) {
			int highNibble = Character.digit(s.charAt(i), 16);
			int lowNibble = Character.digit(s.charAt(i + 1), 16);
			if (highNibble == -1 || lowNibble == -1) {
				return new byte[0];
			}
			byteArray[j++] = (byte) (((highNibble << 4) & 0xf0) | (lowNibble & 0x0f));
		}
		return byteArray;
	}

	private static boolean oddLength(int length) {
		return (length % 2) != 0;
	}

	private String hex(byte[] input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length; i++) {
            sb.append(HEX_TABLE[(input[i] >> 4) & 0xf]);
            sb.append(HEX_TABLE[input[i] & 0xf]);
        }
        return sb.toString();
    }
}
