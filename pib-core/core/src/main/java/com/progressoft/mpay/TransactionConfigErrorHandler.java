package com.progressoft.mpay;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.service.workflow.ErrorHandler;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.util.simpleview.SimpleView;

public class TransactionConfigErrorHandler extends ErrorHandler {

	@Override
	public InvalidInputException handleError(Exception ex, SimpleView simpleView, Object entity, int actionKey) {
		InvalidInputException iie = null;

		if (ex instanceof DataIntegrityViolationException || ex instanceof TransactionSystemException || ex.getCause() instanceof DataIntegrityViolationException
				|| ex.getCause() instanceof TransactionSystemException) {

			iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, "duplicat.transactionconfig.info");
			return iie;
		}

		return iie;
	}
}
