package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Map;

public class PreCreateMobileAccount implements FunctionProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(PreCreateMobileAccount.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        LOGGER.debug("inside execute PreCreateMobileAccount --------------------------");
        MPAY_MobileAccount account = (MPAY_MobileAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
        account.setIsActive(true);
        account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
    }
}
