package com.progressoft.mpay.charges;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CommissionParameter;
import com.progressoft.mpay.entities.MPAY_CommissionSlice;
import com.progressoft.mpay.entity.MPAYView;

public class PreDeleteCommissionSlice implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PreDeleteCommissionSlice.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PreDeleteCommissionSlice ----------------");
        MPAY_CommissionSlice slice = (MPAY_CommissionSlice) arg0.get(WorkflowService.WF_ARG_BEAN);
        for (MPAY_CommissionParameter param : slice.getRefSliceCommissionParameters()) {
            Map<Option, Object> options = new EnumMap<>(Option.class);
            options.put(Option.ACTION_NAME, "Delete");
            JfwHelper.executeAction(MPAYView.COMMISSIONS_PARAMETERS, param, options);
        }
    }

}
