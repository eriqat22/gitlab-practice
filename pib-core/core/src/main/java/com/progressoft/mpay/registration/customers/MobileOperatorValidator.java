package com.progressoft.mpay.registration.customers;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

@SuppressWarnings("rawtypes")
public class MobileOperatorValidator implements Validator {

	private static final String OPERATOR_NUMBER = "operatorNumber";
	private static final String MOBILE_NUMBER = "mobileNumber";
	private String operator;

	@Override
	public void validate(Map transientVar, Map args, PropertySet arg2) throws InvalidInputException, WorkflowException {
		Map<String, String> operatorMap = getOperatorMobileNumbe(transientVar);
		if (!operatorMap.get(MOBILE_NUMBER).substring(5, 7).equals(operatorMap.get(OPERATOR_NUMBER)))
			throw new InvalidInputException("Mobile operator not compatible with mobile number");
	}

	private Map<String, String> getOperatorMobileNumbe(Map transientVar) {
		Object object = transientVar.get(WorkflowService.WF_ARG_BEAN);
		Map<String, String> map = new HashMap<>();
		if (object instanceof MPAY_Customer) {
			MPAY_Customer customer = (MPAY_Customer) object;
			map.put(MOBILE_NUMBER, customer.getMobileNumber());
		} else if (object instanceof MPAY_CustomerMobile) {
			MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) object;
			map.put(MOBILE_NUMBER, mobile.getMobileNumber());
		}
		return map;
	}

	private String getOperator(String customerOperatorValue) {
		try {
			JSONObject json = new JSONObject(customerOperatorValue);
			if (json.isNull("key")) {
				operator = customerOperatorValue;
				return customerOperatorValue.split("-")[0].substring(1);
			}
			operator = json.getString("key");
			return json.getString("key").split("-")[0].substring(1);
		} catch (Exception e) {
			operator = customerOperatorValue;
			return customerOperatorValue.split("-")[0].substring(1);
		}
	}
}