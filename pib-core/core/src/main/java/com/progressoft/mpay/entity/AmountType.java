package com.progressoft.mpay.entity;

public final class AmountType {
	public static final int CHARGE = 1;
	public static final int AMOUNT = 2;

	private AmountType() {

	}
}
