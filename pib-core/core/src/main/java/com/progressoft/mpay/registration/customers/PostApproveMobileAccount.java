package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclear.IMPClearClient;
import com.progressoft.mpay.mpclear.MPClearClientHelper;

import static java.util.Objects.nonNull;

public class PostApproveMobileAccount implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostApproveMobileAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_MobileAccount account = (MPAY_MobileAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
		if (nonNull(account.getIsRegistered()) && account.getIsRegistered())
			JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			handleAddAccount(account);
	}

	private void handleAddAccount(MPAY_MobileAccount account) throws WorkflowException {
		logger.debug("inside HandleAddAccount--------------------------");
		IMPClearClient client = MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor());
		if (!account.getMobile().getRefCustomer().getIsRegistered())
			client.addCustomer(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), account.getMobile().getRefCustomer(), account.getMobile());
		else
			client.addMobileAccount(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), account.getMobile().getRefCustomer(), account.getMobile(), account);
	}
}
