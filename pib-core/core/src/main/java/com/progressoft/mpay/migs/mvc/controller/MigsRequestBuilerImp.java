package com.progressoft.mpay.migs.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import com.progressoft.mpay.migs.request.MigsRequest;

public class MigsRequestBuilerImp extends MigsRequestBuiler {

	public MigsRequestBuilerImp(MigsContext context) {
		super(context);
	}

	@Override
	public MigsRequest buildRequestObject(HttpServletRequest request) {
		return new MigsRequest.Builder(context)
		.withAmount(MigsAmountFormatter.formatAmount(request.getParameter(MIGSParameters.AMOUNT)))
		.withServiceTypeCode(request.getParameter(MIGSParameters.SERVICE_TYPE_CODE))
		.withCommand(context.getSystemParameters().getMigsVpcCommand())
		.withLocal(request.getParameter(MIGSParameters.LANGUAGE).equals(MIGSParameters.ENGLISH_LANGUAGE_ID)?
				MIGSParameters.ENGLISH_LANGUAGE_LOCALE : MIGSParameters.ARABIC_LANGUAGE_LOCALE)
		.withMerchTxnRef(request.getParameter(MIGSParameters.MESSAGE_ID))
		.withOrderInfo(request.getParameter(MIGSParameters.OPERATION))
		.withReturnURL(context.getSystemParameters().getMigsVpcRedirectUrl())
		.withVersion(context.getSystemParameters().getMigsVpcVersion())
		.withMobileNumber(request.getParameter(MIGSParameters.MOBILE_NUMBER))
		.withDeviceId(request.getParameter(MIGSParameters.DEVICE_ID))
		.withJson(request.getParameter(MIGSParameters.JSON))
		.withNewVersion(request.getParameter(MIGSParameters.NEW_VERSION))
		.withHashing()
		.make();
	}
}