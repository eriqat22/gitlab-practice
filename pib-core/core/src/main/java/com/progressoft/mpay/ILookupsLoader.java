package com.progressoft.mpay;

import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPClearCache;
import com.progressoft.mpay.entity.PSPCache;

import java.util.List;

public interface ILookupsLoader {
    MPAY_City getCity(String code);

    MPAY_City getCityById(long id);

    List<MPAY_City> getCitiesByName(String name);

    MPAY_ClientType getClientType(String code);

    List<MPAY_ClientType> listClientTypes();

    MPAY_IDType getIDType(String code);

    MPAY_ChannelType getChannelType(String code);

    JfwCountry getCountry(String code);

    JfwCountry getCountryByIsoCode(String code);

    JfwCountry getCountryByDescription(String code);

    JFWCurrency getCurrency(String code);

    MPAY_MpClearIntegMsgType getMpClearIntegrationMessageTypes(String code);

    MPAY_CustIntegActionType getCustomerIntegrationActionTypes(String code);

    MPAY_MpClearIntegRjctReason getMpClearIntegrationReasons(String code);

    MPAY_MpClearResponseCode getMpClearIntegrationResponseCodes(String code);

    MPAY_NetworkManActionType getNetworkManagementActionTypes(String code);

    MPAY_AccountType getAccountTypes(String code);

    MPAY_JvType getJvTypes(String code);

    MPAY_MessageType getMessageTypes(String code);

    MPAY_MessageType getMessageTypes(long id);

    MPAY_Reason getReason(String code);

    MPAY_SysConfig getSystemConfigurations(String key);

    List<MPAY_SysConfig> listSystemConfigurations();

    List<MPAY_City> listCities();

    MPAY_TransactionsConfig getTransactionsConfig(long clientType, long messageType, String bankedUnbanked, boolean isSender);

    List<MPAY_MessageType> getMessageTypes();

    MPAY_Bank getBank(String name);

    MPAY_Bank getBank(long id);

    List<MPAY_Bank> listBanks();

    MPAY_Reg_File_Stt getRegFileStatus(String code);

    MPAY_Reg_Instrs_Stt getRegInstrStatus(String code);

    MPAY_ePayStatusCode getePayStatusCode(String epayCode);

    JFWCurrency getDefaultCurrency();

    List<MPAY_ChannelType> listChannelTypes();

    List<MPAY_MessageType> listMessageTypes();

    MPAY_MessageType getMessageTypeByMPClearCode(String code);

    MPAY_Reason getReasonByMPClearCode(String code);

    PSPCache getPSP();

    MPClearCache getMPClear();

    MPAY_NotificationTemplate getNotificationTemplate(long operationId, long typeId, long languageId);

    MPAY_NotificationTemplate getNotificationTemplateByOperationName(String operationName, long typeId, long languageId);

    MPAY_BankIntegStatus getBankIntegStatus(String code);

    MPAY_Language getLanguage(long id);

    MPAY_Language getLanguageByCode(String code);

    MPAY_EndPointOperation getEndPointOperation(String operation);

    MPAY_ProcessingStatus getProcessingStatus(String code);

    MPAY_TransactionConfig getTransactionConfig(long senderType, boolean isSenderBanked, long receiverType, boolean isReceiverBanked, long messageType, String transactionTypeCode, long operationId);

    MPAY_TransactionConfig getTransactionConfig(long senderType, boolean isSenderBanked, long receiverType, boolean isReceiverBanked, long messageType, String transactionTypeCode);

    MPAY_ClientType getCustomerClientType();

    MPAY_ClientType getCorporateClientType(String code);

    MPAY_TransactionDirection getTransactionDirection(long id);

    List<MPAY_LimitsType> listLimitsTypes();

    MPAY_TransactionType getTransactionType(String code);

    MPAY_Reasons_NLS getReasonNLS(long reasonId, String languageCode);

    MPAY_MessageType getFirstFinancialMessageType();

    MPAY_NotificationChannel getNotificationChannel(String code);

    MPAY_ServiceType getServiceType(String code);

    MPAY_ServicesCategory getServiceCategory(String code);

    MPAY_ServicesCategory getServiceCategory(long id);

    MPAY_Bank getBankByCode(String code);

    MPAY_Profile getProfile(String code);

    MPAY_Profile getProfile(long id);

    MPAY_ServiceIntegReason getServiceIntegReason(String code);

    MPAY_ServiceIntegReasons_NLS getServiceIntegReasonNLS(long reasonId, long languageId);

    MPAY_AppsChecksum getAppChecksum(String code);

    MPAY_BalanceType getBalanceType(String code);

    MPAY_TransactionSize getTransactionSizeByCode(String code);

    MPAY_CorpLegalEntity getLegalEntityByCode(String code);

    MPAY_NotificationService getNotifictionService(String code);

    List<MPAY_Language> getLanguages();

    MPAY_CityArea getAreaByCode(String areaCode);

    MPAY_AccountCategory getAccountCategory(long id);

    MPAY_CustomerKyc getCustomerKyc(long id);

    MPAY_CorporateKyc getCorporateKyc(long id);

    MPAY_CorporateKyc getCorporateKycByCode(String fullKeyc);
}
