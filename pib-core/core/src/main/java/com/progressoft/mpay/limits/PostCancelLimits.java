package com.progressoft.mpay.limits;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

import static com.progressoft.mpay.entities.WF_LimitsDetails.*;

@Component
public class PostCancelLimits implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostApproveLimitsScheme.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("Inside PostCancelLimits.java-------");
        MPAY_Limit limit = (MPAY_Limit) arg0.get(WorkflowService.WF_ARG_BEAN);
        Map<Option, Object> options = new EnumMap<>(Option.class);
        options.put(Option.ACTION_NAME, ACTION_NAME_SVC_Cancel);
        for (MPAY_LimitsDetail limitDetail : limit.getRefLimitLimitsDetails()) {
            if (STEP_Deleted.equals(limitDetail.getStatusId().getCode()))
                continue;
            if (limitDetail.getJfwDraft() != null) {
                JfwHelper.executeAction(MPAYView.LIMITS_DETAILS, limitDetail, options);
            }
        }
    }
}
