package com.progressoft.mpay.registration.customers;

public class CustomerMobileWorkflowStatuses {
    public static final String APPROVAL = "100102";
    public static final String REJECTED = "100103";
    public static final String MODIFICATION = "100104";
    public static final String APPROVED = "100105";
    public static final String DELETED = "100106";
    public static final String INTEGRATION = "100107";
    public static final String DELETING = "100108";
    public static final String MODIFIED = "100110";
    public static final String CANCELING = "100111";
    public static final String SUSPENDED = "100115";

    private CustomerMobileWorkflowStatuses() {
    }
}
