package com.progressoft.mpay.registration.corporates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.shared.DynamicField;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_NotificationChannel;
import com.progressoft.mpay.entities.MPAY_ServiceType;
import com.progressoft.mpay.entities.MPAY_ServicesCategory;
import com.progressoft.mpay.entity.CorporateServiceMetaData;

public class DisplayCorporateServiceChanges implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(DisplayCorporateServiceChanges.class);
	private static final String MODIFIED_COLOR = "blue";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handle--------------------------");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) changeHandlerContext.getEntity();
		try {
			Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
			for (UIPortletAttributes ui : portletsAttributes) {
				ui.setVisible(true);
			}
			renderFields(changeHandlerContext, service, CorporateServiceMetaData.fromJson(service.getApprovedData()));
		} catch (Exception e) {
			throw new InvalidInputException(e);
		}

	}

	private void renderFields(ChangeHandlerContext changeHandlerContext, MPAY_CorpoarteService service, CorporateServiceMetaData approvedService) {
		logger.debug("inside RenderFields--------------------------");
		List<DynamicField> dynamicFields = new ArrayList<>();

		DynamicField field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.FIELD));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.OLD_DATA));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.NEW_DATA));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.NAME));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(service.getName());
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(service.getNewName());
		if (!StringUtils.equals(service.getNewName(), service.getName()))
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.DESCRIPTION));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(service.getDescription());
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(service.getNewDescription());
		if (!StringUtils.equals(service.getNewDescription(), service.getDescription()))
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

//		MPAY_NotificationChannel channel = LookupsLoader.getInstance().getNotificationChannel(approvedService.getNotificationChannelCode());

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.NOTIFICATION_CHANNEL));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
//		field.setLabel(channel.getNameTranslation());
		dynamicFields.add(field);

		field = new DynamicField();
//		field.setLabel(service.getNotificationChannel().getNameTranslation());
//		if (!StringUtils.equals(approvedService.getNotificationChannelCode(), service.getNotificationChannel().getCode()))
//			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.NOTIFICATION_RECEIVER));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
//		field.setLabel(approvedService.getNotificationReceiver());
		dynamicFields.add(field);

		field = new DynamicField();
//		field.setLabel(service.getNotificationReceiver());
//		if (!StringUtils.equals(approvedService.getNotificationReceiver(), service.getNotificationReceiver()))
//			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.SERVICE_CATEGORY));
		field.setLabelBold(true);
		dynamicFields.add(field);

		MPAY_ServicesCategory category = LookupsLoader.getInstance().getServiceCategory(approvedService.getServiceCategoryId());
		field = new DynamicField();
		field.setLabel(category.getNameTranslation());
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(service.getServiceCategory().getNameTranslation());
		if (approvedService.getServiceCategoryId() != service.getServiceCategory().getId())
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(CorporateServiceMetaDataTranslationKeys.SERVICE_TYPE));
		field.setLabelBold(true);
		dynamicFields.add(field);

		MPAY_ServiceType type = LookupsLoader.getInstance().getServiceType(approvedService.getServiceTypeCode());
		field = new DynamicField();
		field.setLabel(type.getName());
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(service.getServiceType().getName());
		if (!StringUtils.equals(approvedService.getServiceTypeCode(), service.getServiceType().getCode()))
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		changeHandlerContext.getPortletsAttributes().get("Info").setVisible(true);
		changeHandlerContext.getPortletsAttributes().get("Info").setDynamicPortlet(true);
		changeHandlerContext.getPortletsAttributes().get("Info").setDynamicFields(dynamicFields);
	}
}
