package com.progressoft.mpay.notifications;

@FunctionalInterface
public interface INotificationService {
	NotificationServiceResult sendNotification(NotificationServiceContext context);
}
