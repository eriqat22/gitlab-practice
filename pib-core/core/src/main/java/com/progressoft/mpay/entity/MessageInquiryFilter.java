package com.progressoft.mpay.entity;


import java.sql.Timestamp;

public class MessageInquiryFilter {
    private String senderService;
    private String senderMobile;
    private String msgReference;
    private Timestamp fromTime;
    private Timestamp toTime;
    private String processingStatus;
    private String messageType;
    private String msgReasonId;
    private String msgRequestedId;
    private String msgShopId;
    private String msgChannelId;


    public String getSenderService() {
        return senderService;
    }

    public void setSenderService(String senderService) {
        this.senderService = senderService;
    }

    public String getSenderMobile() {
        return senderMobile;
    }

    public void setSenderMobile(String senderMobile) {
        this.senderMobile = senderMobile;
    }

    public String getMsgReference() {
        return msgReference;
    }

    public void setMsgReference(String msgReference) {
        this.msgReference = msgReference;
    }

    public Timestamp getFromTime() {
        return fromTime;
    }

    public void setFromTime(Timestamp fromTime) {
        this.fromTime = fromTime;
    }

    public Timestamp getToTime() {
        return toTime;
    }

    public void setToTime(Timestamp toTime) {
        this.toTime = toTime;
    }

    public String getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(String processingStatus) {
        this.processingStatus = processingStatus;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMsgReasonId() {
        return msgReasonId;
    }

    public void setMsgReasonId(String msgReasonId) {
        this.msgReasonId = msgReasonId;
    }

    public String getMsgRequestedId() {
        return msgRequestedId;
    }

    public void setMsgRequestedId(String msgRequestedId) {
        this.msgRequestedId = msgRequestedId;
    }

    public String getMsgShopId() {
        return msgShopId;
    }

    public void setMsgShopId(String msgShopId) {
        this.msgShopId = msgShopId;
    }

    public String getMsgChannelId() {
        return msgChannelId;
    }

    public void setMsgChannelId(String msgChannelId) {
        this.msgChannelId = msgChannelId;
    }
}
