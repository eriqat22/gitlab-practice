package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class EnableServiceDeletion implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(EnableServiceDeletion.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition------------");
		MPAY_CorpoarteService mobile = (MPAY_CorpoarteService) transientVar.get(WorkflowService.WF_ARG_BEAN);
		return mobile.getRefCorporate().getRefCorporateCorpoarteServices().stream()
				.filter(service1 -> (service1.getDeletedFlag() == null || !service1.getDeletedFlag()))
				.filter(MPAY_CorpoarteService::getIsActive)
				.count() > 1;
	}
}