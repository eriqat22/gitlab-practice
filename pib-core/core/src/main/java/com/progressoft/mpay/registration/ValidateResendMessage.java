package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entities.MPAY_CashOut;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;
import com.progressoft.mpay.entity.MPAYView;

public class ValidateResendMessage implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(ValidateResendMessage.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("Inside ValidateResendMessage ==========");
		String viewName = (String) transientVar.get(WorkflowService.WF_ARG_VIEW_NAME);
		Object bean = transientVar.get(WorkflowService.WF_ARG_BEAN);
		MPAY_MpClearIntegMsgLog msg = ResendValidatorHelper.tryGetCustomer(viewName, bean);
		if (msg != null)
			return true;
		msg = ResendValidatorHelper.tryGetCorporate(viewName, bean);
		if (msg != null)
			return true;
		if (viewName.equals(MPAYView.NETWORK_MANAGEMENT.viewName)) {
			msg = ((MPAY_NetworkManagement) bean).getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.CASH_IN.viewName)) {
			msg = ((MPAY_CashIn) bean).getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.CASH_OUT.viewName)) {
			msg = ((MPAY_CashOut) bean).getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.PENDING_MP_CLEAR_MESSAGES.viewName)) {
			msg = (MPAY_MpClearIntegMsgLog) bean;
		} else {
			return false;
		}
		if (msg == null)
			return false;
		return true;
	}
}
