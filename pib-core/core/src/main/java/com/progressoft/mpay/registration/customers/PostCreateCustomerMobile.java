package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_SysConfig;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;

public class PostCreateCustomerMobile implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCreateCustomerMobile.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute--------------------------");
        MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN);
        mobile.setEnableSMS(true);
        mobile.setRetryCount(0L);
        mobile.setIsBlocked(false);
        mobile.setIsRegistered(false);
        mobile.setIsActive(true);
        mobile.setChannelType(LookupsLoader.getInstance().listChannelTypes().get(0));
        try {
            createMobileAccount(mobile);
            createCommissionMobileAccount(mobile);
        } catch (Exception e) {
            logger.error("Error while creating mobile account", e);
            throw new WorkflowException(e);
        }
    }

    private void createMobileAccount(MPAY_CustomerMobile mobile) {
        logger.debug("inside createMobileAccount--------------------------");
        ILookupsLoader lookupsLoader = LookupsLoader.getInstance();
        Long customerCategoryId = Long.valueOf(
                lookupsLoader.getSystemConfigurations(SysConfigKeys.CUSTOMER_CATEGORY_KEY).getConfigValue());

        MPAY_MobileAccount account = new MPAY_MobileAccount();
        account.setMobile(mobile);
        account.setCategory(lookupsLoader.getAccountCategory(customerCategoryId));
        account.setBankedUnbanked(mobile.getBankedUnbanked());
        account.setBank(mobile.getBank());
        account.setExternalAcc(mobile.getExternalAcc());
        account.setMas(mobile.getMas());
        account.setIsActive(mobile.getIsActive());
        account.setIsRegistered(mobile.getIsRegistered());
        account.setRefProfile(mobile.getRefProfile());
        account.setIsDefault(true);
        account.setIsSwitchDefault(true);
        account.setIban(mobile.getIban());
        account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
        mobile.setMobileMobileAccounts(new ArrayList<>());
        mobile.getMobileMobileAccounts().add(account);
        account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
        try {
            JfwHelper.createEntity(MPAYView.MOBILE_ACCOUNTS, account);
        } catch (InvalidActionException e) {
            throw new BusinessException(e);
        }
    }

    private void createCommissionMobileAccount(MPAY_CustomerMobile mobile) {
        logger.debug("inside createMobileAccount--------------------------");
        MPAY_MobileAccount account = new MPAY_MobileAccount();
        account.setMobile(mobile);
        account.setBankedUnbanked(mobile.getBankedUnbanked());
        account.setBank(mobile.getBank());
        account.setMas(mobile.getMas() + 1);// new mas
        account.setIsActive(mobile.getIsActive());
        account.setIsRegistered(mobile.getIsRegistered());
        account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));

        account.setIban(null);
        account.setIsDefault(false);
        account.setIsSwitchDefault(false);
        account.setExternalAcc(null);

        ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();
        MPAY_SysConfig commissionCategoryConfig = lookupsLoader.getSystemConfigurations(SysConfigKeys.COMMISSION_ACCOUNT_CATEGORY);
        MPAY_SysConfig commissionProfileConfig = lookupsLoader.getSystemConfigurations(SysConfigKeys.COMMISSION_PROFILE);

        if (commissionCategoryConfig != null)
            account.setCategory(lookupsLoader.getAccountCategory(Long.parseLong(commissionCategoryConfig.getConfigValue())));
        if (commissionProfileConfig != null)
            account.setRefProfile(lookupsLoader.getProfile(commissionProfileConfig.getConfigValue()));

        mobile.setMobileMobileAccounts(new ArrayList<>());
        mobile.getMobileMobileAccounts().add(account);
        account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
        try {
            JfwHelper.createEntity(MPAYView.MOBILE_ACCOUNTS, account);
        } catch (InvalidActionException e) {
            throw new BusinessException(e);
        }
    }
}
