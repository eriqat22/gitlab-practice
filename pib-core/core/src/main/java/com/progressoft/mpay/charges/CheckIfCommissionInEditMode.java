package com.progressoft.mpay.charges;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JfwDraft;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Commission;
import com.progressoft.mpay.entities.MPAY_CommissionScheme;

public class CheckIfCommissionInEditMode implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfCommissionInEditMode.class);
	private static final String COMMISSION_SCHEME_EDIT_NEW_STATUS_CODE = "211804";
	private static final String COMMISSION_SCHEME_EDIT_APPROVED_STATUS_CODE = "211806";
	private static final String COMMISSION_SCHEME_EDIT_REJECTED_STATUS_CODE = "211809";

	@Autowired
	private ItemDao itemDao;
	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_Commission details = (MPAY_Commission) transientVar.get(WorkflowService.WF_ARG_BEAN);
		if(details.getRefCommissionScheme().getJfwDraft() == null)
			return false;
		JfwDraft schemeDraft = MPayContext.getInstance().getDataProvider().getJFWDraft(details.getRefCommissionScheme().getJfwDraft().getId());
		if (schemeDraft == null)
			return true;
		MPAY_CommissionScheme scheme = (MPAY_CommissionScheme) JfwHelper.createObjectFromDraft(schemeDraft.getDraftData(), new MPAY_CommissionScheme(), itemDao);
		return scheme.getStatusId().getCode().equals(COMMISSION_SCHEME_EDIT_NEW_STATUS_CODE) || scheme.getStatusId().getCode().equals(COMMISSION_SCHEME_EDIT_APPROVED_STATUS_CODE) || scheme.getStatusId().getCode().equals(COMMISSION_SCHEME_EDIT_REJECTED_STATUS_CODE);
	}
}