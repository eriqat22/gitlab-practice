package com.progressoft.mpay.plugins.validators;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Rate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class FxRateValidator implements Validator {

    @Autowired
    ItemDao itemDao;

    @Override
    public void validate(Map map, Map map1, PropertySet propertySet) throws WorkflowException {
        MPAY_Rate rate = (MPAY_Rate) map.get(WorkflowService.WF_ARG_BEAN);
        String where = "baseCurrency.numericISOCode=" + rate.getBaseCurrency().getNumericISOCode() +
                " and foreignCurrency.numericISOCode=" + rate.getForeignCurrency().getNumericISOCode() +
                " and statusId.name != 'Deleted' and id != " + rate.getId();

        Long itemCount = itemDao.getItemCount(MPAY_Rate.class, null, where);

        if (itemCount > 0) {
            InvalidInputException iie = new InvalidInputException();
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, "Fx rate data already exist");
            throw iie;
        }
    }
}
