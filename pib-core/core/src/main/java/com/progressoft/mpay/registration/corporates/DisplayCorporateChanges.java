package com.progressoft.mpay.registration.corporates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.bussinessobject.security.Tenant;
import com.progressoft.jfw.shared.DynamicField;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.LocalizationHelper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_City;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entity.CorporateMetaData;

public class DisplayCorporateChanges implements ChangeHandler {

    public static final String BASIC_INFO = "Basic Info";
    private static final Logger logger = LoggerFactory.getLogger(DisplayCorporateChanges.class);
    private static final String MODIFIED_COLOR = "blue";
    LocalizationHelper localizationHelper;

    public DisplayCorporateChanges() {
        localizationHelper = new LocalizationHelper();
    }

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside handle--------------------------");
        MPAY_Corporate corporate = (MPAY_Corporate) changeHandlerContext.getEntity();
        try {
            Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
            for (UIPortletAttributes ui : portletsAttributes) {
                ui.setVisible(true);
            }
            renderFields(changeHandlerContext, corporate, CorporateMetaData.fromJson(corporate.getApprovedData()));
        } catch (Exception e) {
            throw new InvalidInputException(e);
        }
    }

    private void renderFields(ChangeHandlerContext changeHandlerContext, MPAY_Corporate corporate, CorporateMetaData approvedCorporate) {
        logger.debug("inside RenderFields--------------------------");
        List<DynamicField> dynamicFields = new ArrayList<>();

        DynamicField field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.FIELD));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.OLD_DATA));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.NEW_DATA));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.NAME));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getName());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getName());
        handleMofified(approvedCorporate.getName(), corporate.getName(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.DESCRIPTION));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getDescription());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getDescription());
        handleMofified(approvedCorporate.getDescription(), corporate.getDescription(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.REFERENCE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getClientRef());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getClientRef());
        handleMofified(approvedCorporate.getClientRef(), corporate.getClientRef(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.REGISTRATION_DATE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(String.valueOf(DateFormatUtils.format(approvedCorporate.getRegistrationDate(), "dd/MM/yyyy")));
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(String.valueOf(DateFormatUtils.format(corporate.getRegistrationDate(), "dd/MM/yyyy")));
        if (approvedCorporate.getRegistrationDate().compareTo(corporate.getRegistrationDate()) != 0)
            field.setLabelColor(MODIFIED_COLOR);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.PHONE_ONE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getPhoneOne());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getPhoneOne());
        handleMofified(approvedCorporate.getPhoneOne(), corporate.getPhoneOne(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.PHONE_TWO));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getPhoneTwo());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getPhoneTwo());
        handleMofified(approvedCorporate.getPhoneTwo(), corporate.getPhoneTwo(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.EMAIL));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getEmail());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getEmail());
        handleMofified(approvedCorporate.getEmail(), corporate.getEmail(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.PO_BOX));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getPobox());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getPobox());
        handleMofified(approvedCorporate.getPobox(), corporate.getPobox(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.ZIP_CODE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getZipCode());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getZipCode());
        handleMofified(approvedCorporate.getZipCode(), corporate.getZipCode(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.BUILDING_NUMBER));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getBuildingNum());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getBuildingNum());
        handleMofified(approvedCorporate.getBuildingNum(), corporate.getBuildingNum(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.STREEN_NAME));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getStreetName());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getStreetName());
        if (!StringUtils.equals(approvedCorporate.getStreetName(), corporate.getStreetName()))
            field.setLabelColor(MODIFIED_COLOR);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.NOTE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getNote());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getNote());
        handleMofified(approvedCorporate.getNote(), corporate.getNote(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.NOTIFICATION_LANGUAGE));
        field.setLabelBold(true);
        dynamicFields.add(field);

        MPAY_Language approvedLanguage = MPayContext.getInstance().getLookupsLoader().getLanguage(approvedCorporate.getPrefLangId());

        field = new DynamicField();
        field.setLabel(approvedLanguage.getNameTranslation());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getPrefLang().getNameTranslation());
        if (approvedCorporate.getPrefLangId() != corporate.getPrefLang().getId())
            field.setLabelColor(MODIFIED_COLOR);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.CITY));
        field.setLabelBold(true);
        dynamicFields.add(field);

        MPAY_City approvedCity = MPayContext.getInstance().getLookupsLoader().getCity(approvedCorporate.getCityCode());

        field = new DynamicField();
        field.setLabel(approvedCity.getNameTranslation());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getCity().getNameTranslation());
        handleMofified(approvedCorporate.getCityCode(), corporate.getCity().getCode(), field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.TENANTS));
        field.setLabelBold(true);
        dynamicFields.add(field);

        String formattedOldTenants = getFormattedTenants(approvedCorporate.getTenants());
        String formattedNewTenants = getFormattedTenants(getTenantNames(corporate.getTenants()));

        field = new DynamicField();
        field.setLabel(formattedOldTenants);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(formattedNewTenants);
        handleMofified(formattedOldTenants, formattedNewTenants, field);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(localizationHelper.localize(CorporateMetaDataTranslationKeys.KYC_TEMPLATES));
        field.setLabelBold(true);
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(approvedCorporate.getKycTemplate());
        dynamicFields.add(field);

        field = new DynamicField();
        field.setLabel(corporate.getName());
        handleMofified(approvedCorporate.getKycTemplate(), corporate.getKycTemplate().getName(), field);
        dynamicFields.add(field);

        changeHandlerContext.getPortletsAttributes().get(BASIC_INFO).setVisible(true);
        changeHandlerContext.getPortletsAttributes().get(BASIC_INFO).setDynamicPortlet(true);
        changeHandlerContext.getPortletsAttributes().get(BASIC_INFO).setDynamicFields(dynamicFields);
    }

    private void handleMofified(String oldValue, String newValue, DynamicField field) {
        if (!StringUtils.equals(oldValue, newValue))
            field.setLabelColor(MODIFIED_COLOR);
    }

    private List<String> getTenantNames(List<Tenant> tenants) {
        List<String> tenantNames = new ArrayList<>();
        if (tenants == null || tenants.isEmpty())
            return tenantNames;

        tenantNames.addAll(tenants.stream().map(Tenant::getName).collect(Collectors.toList()));
        return tenantNames;
    }

    private String getFormattedTenants(List<String> tenantNames) {
        StringBuilder result = new StringBuilder();
        if (tenantNames.isEmpty())
            return result.toString();
        tenantNames.sort(String::compareTo);

        result.append(tenantNames.get(0));
        if (tenantNames.size() == 1)
            return result.toString();
        for (int i = 1; i < tenantNames.size(); i++) {
            result.append(";");
            result.append(tenantNames.get(i));
        }
        return result.toString();
    }
}
