package com.progressoft.mpay.tax;

import java.math.BigDecimal;
import java.util.Map;

import com.progressoft.jfw.shared.UIFieldAttributes;
import com.progressoft.mpay.entities.MPAY_TaxSlice;
import com.progressoft.mpay.entity.TaxTypes;

public class TaxTypeSliceHandlerHelper {
	private static final String TAX_AMOUNT = "taxAmount";
	private static final String TAX_PERCENT = "taxPercent";
	private static final String MIN_AMOUNT = "minAmount";
	private static final String MAX_AMOUNT = "maxAmount";

	private TaxTypeSliceHandlerHelper() {

	}

	public static  void handleTaxType(Map<String, UIFieldAttributes> fieldsAttributes, MPAY_TaxSlice slice) {
		if (slice.getTaxType() == null || slice.getTaxType().equalsIgnoreCase(TaxTypes.FIXED)) {
			fieldsAttributes.get(TAX_AMOUNT).setEnabled(true);
			fieldsAttributes.get(TAX_AMOUNT).setRequired(true);

			fieldsAttributes.get(TAX_PERCENT).setEnabled(false);
			fieldsAttributes.get(MIN_AMOUNT).setEnabled(false);
			fieldsAttributes.get(MAX_AMOUNT).setEnabled(false);

			fieldsAttributes.get(TAX_PERCENT).setRequired(false);
			fieldsAttributes.get(MIN_AMOUNT).setRequired(false);
			fieldsAttributes.get(MAX_AMOUNT).setRequired(false);

			slice.setTaxPercent(BigDecimal.ZERO);
			slice.setMinAmount(null);
			slice.setMaxAmount(null);
		} else {
			fieldsAttributes.get(TAX_AMOUNT).setEnabled(false);
			fieldsAttributes.get(TAX_AMOUNT).setRequired(false);

			fieldsAttributes.get(TAX_PERCENT).setEnabled(true);
			fieldsAttributes.get(MIN_AMOUNT).setEnabled(true);
			fieldsAttributes.get(MAX_AMOUNT).setEnabled(true);

			fieldsAttributes.get(TAX_PERCENT).setRequired(true);
			fieldsAttributes.get(MIN_AMOUNT).setRequired(true);
			fieldsAttributes.get(MAX_AMOUNT).setRequired(true);

			slice.setTaxAmount(null);
		}
	}
}
