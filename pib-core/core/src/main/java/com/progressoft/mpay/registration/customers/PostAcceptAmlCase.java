package com.progressoft.mpay.registration.customers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.entities.MPAY_AmlCase;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_NotificationTemplate;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.plugins.NotificationContext;

public class PostAcceptAmlCase implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostAcceptAmlCase.class);
	private static final String AML_OPERATION = "aml";

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside PostAcceptAmlCase--------------------------");

		MPAY_AmlCase amlCase = (MPAY_AmlCase) transientVars.get(WorkflowService.WF_ARG_BEAN);
		amlCase.getRefCustomer().setIsActive(true);
		MPayContext.getInstance().getDataProvider().updateCustomer(amlCase.getRefCustomer());
		MPAY_Notification notification = createAcceptanceMessage(amlCase.getRefCustomer());
		MPayContext.getInstance().getDataProvider().persistNotification(notification);		
		MPayContext.getInstance().getNotificationHelper().sendNotifications(notification);
	}
	
	public static MPAY_Notification createAcceptanceMessage(MPAY_Customer customer) {
		List<MPAY_Notification> notifications = new ArrayList<>();
		long senderLanguageId = customer.getPrefLang().getId();
		MPAY_CustomerMobile senderMobile = customer.getRefCustomerCustomerMobiles().get(0);
		MPAY_EndPointOperation operation = MPayContext.getInstance().getLookupsLoader().getEndPointOperation(AML_OPERATION	);
		String sender = senderMobile.getMobileNumber();
		String notificationChannel = NotificationChannelsCode.SMS;

		NotificationContext notificationContext = new NotificationContext();
		notificationContext.setOperation(operation.getName());
		notificationContext.setSender(sender);

		 MPAY_NotificationTemplate template = new MPAY_NotificationTemplate();
		return MPayContext.getInstance().getNotificationHelper().createNotification(null, notificationContext,
				NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId, sender, notificationChannel);

	}
}
