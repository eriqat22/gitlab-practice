package com.progressoft.mpay.entity;

public final class IntegMessageTypes {
    public static final String TYPE_1700 = "1700";
    public static final String TYPE_1710 = "1710";
    public static final String TYPE_1800 = "1800";
    public static final String TYPE_1810 = "1810";
    public static final String TYPE_1200 = "1200";
    public static final String TYPE_1210 = "1210";
    public static final String TYPE_1220 = "1220";
    public static final String TYPE_1230 = "1230";
    public static final String TYPE_1430 = "1430";
    public static final String TYPE_1420 = "1420";
    private IntegMessageTypes() {
    }

}
