package com.progressoft.mpay.charges;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_ChargesScheme;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;
import com.progressoft.mpay.entity.MPAYView;

public class PostApproveChargesScheme implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostApproveChargesScheme.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("Inside PostApproveChargesScheme-------");
        MPAY_ChargesScheme scheme = (MPAY_ChargesScheme) arg0.get(WorkflowService.WF_ARG_BEAN);

        for (MPAY_ChargesSchemeDetail chargeDetail : scheme.getRefChargesSchemeChargesSchemeDetails()) {
            if (chargeDetail.getJfwDraft() != null) {
                Map<Option, Object> options = new EnumMap<>(Option.class);
                options.put(Option.ACTION_NAME, "SVC_Approve");
                JfwHelper.executeAction(MPAYView.CHARGES_SCHEME_DETAILS, chargeDetail, options);
            }
        }
    }
}
