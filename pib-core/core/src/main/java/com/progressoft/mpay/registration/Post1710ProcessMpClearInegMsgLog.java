package com.progressoft.mpay.registration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.openmbean.InvalidOpenTypeException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CustIntegMessage;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Post1710ProcessMpClearInegMsgLog implements FunctionProvider {

	@Autowired
	private ItemDao itemDao;
	private static final Logger logger = LoggerFactory.getLogger(Post1710ProcessMpClearInegMsgLog.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("Inside Post1710ProcessMpClearInegMsgLog-------------------------");

		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		MPAY_CustIntegMessage integ1710Message = new MPAY_CustIntegMessage();

		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());

		integ1710Message.setRefMsgLog(msg);
		MPAY_CustIntegMessage orgMessage = getOrigionalMessageByMessageID(isomsg.getField(116).toString());
		if (orgMessage == null)
			throw new InvalidOpenTypeException("original message not found with id = " + isomsg.getField(116).toString());
		if(orgMessage.getRefMsgLog().getRefMessage()!=null)
		msg.setRefMessage(orgMessage.getRefMsgLog().getRefMessage());
		MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).updateResponseReceived(isomsg.getField(116).toString());
		integ1710Message.setRefCustomer(orgMessage.getRefCustomer());
		integ1710Message.setRefMobile(orgMessage.getRefMobile());

		integ1710Message.setIntegMsgType(LookupsLoader.getInstance().getMpClearIntegrationMessageTypes(IntegMessageTypes.TYPE_1710));

		integ1710Message.setRefMessage(isomsg.getField(115).toString());

		orgMessage.setRefMessage(isomsg.getField(116).toString());

		integ1710Message.setRefOriginalMsg(orgMessage);

		integ1710Message.setActionType(orgMessage.getActionType());
		integ1710Message.setResponse(LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(isomsg.getField(44).toString()));

		if (isomsg.getField(47) != null)
			integ1710Message.setNarration(isomsg.getField(47).toString());

		try {

			itemDao.persist(integ1710Message);
			Map<String, Object> v = new HashMap<>();
			v.put(WorkflowService.WF_ARG_BEAN, integ1710Message);
			PreValidate1710MpClearIntegMsg oPreValidate1710MpClearIntegMsg = AppContext.getApplicationContext().getBean("PreValidate1710MpClearIntegMsg", PreValidate1710MpClearIntegMsg.class);
			oPreValidate1710MpClearIntegMsg.execute(v, null, null);
			if (v.get(Result.VALIDATION_RESULT).equals(Result.SUCCESSFUL)) {

				PostProcess1710MpClearIntegMsg oPostProcess1710MpClearIntegMsg = AppContext.getApplicationContext().getBean("PostProcess1710MpClearIntegMsg", PostProcess1710MpClearIntegMsg.class);
				oPostProcess1710MpClearIntegMsg.execute(v, null, null);
			}

		} catch (Exception e) {
			throw new WorkflowException(e);
		}

	}

	private MPAY_CustIntegMessage getOrigionalMessageByMessageID(String messageId) {
		MPAY_CustIntegMessage value = null;

		List<MPAY_CustIntegMessage> items = itemDao.getItems(MPAY_CustIntegMessage.class, null, null, "refMessage='" + messageId + "'", null);
		if (items != null && !items.isEmpty())
			value = items.get(0);

		return value;

	}

}
