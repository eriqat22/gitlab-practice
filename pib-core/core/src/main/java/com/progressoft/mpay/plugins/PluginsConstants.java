package com.progressoft.mpay.plugins;

public class PluginsConstants {
	public static final String MOBILE_SUSPENDED_STATUS_CODE = "100115";
	public static final String CUSTOMER_KEY = "customer";
	public static final String BANK_USER = "bankUser";

	private PluginsConstants() {
	}
}
