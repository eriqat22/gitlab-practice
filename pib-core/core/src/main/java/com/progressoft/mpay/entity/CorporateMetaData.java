package com.progressoft.mpay.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.jfw.model.bussinessobject.security.Tenant;
import com.progressoft.mpay.entities.MPAY_Corporate;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class CorporateMetaData extends ClientMetaData {
	private String name;
	private String description;
	private String clientRef;
	private Date registrationDate;

	private long prefLangId;
	private String cityCode;
	private String kycTemplate;

	private List<String> tenants;

	public CorporateMetaData() {
		tenants = new ArrayList<>();
	}

	public CorporateMetaData(MPAY_Corporate corporate) {
		if (corporate == null)
			throw new NullArgumentException("corporate");

		this.name = corporate.getName();
		this.description = corporate.getDescription();
		this.clientRef = corporate.getClientRef();
		this.registrationDate = corporate.getRegistrationDate();
		setPhoneOne(corporate.getPhoneOne());
        setPhoneTwo(corporate.getPhoneTwo());
		setEmail(corporate.getEmail());
		setPobox(corporate.getPobox());
		setZipCode(corporate.getZipCode());
		setBuildingNum(corporate.getBuildingNum());
		setStreetName(corporate.getStreetName());
		setNote(corporate.getNote());

		this.prefLangId = corporate.getPrefLang().getId();
		this.cityCode = corporate.getCity().getCode();
		this.tenants = getTenantNames(corporate.getTenants());
		this.kycTemplate = corporate.getKycTemplate().getName();
	}

	public void fillCorporateFromMetaData(MPAY_Corporate corporate) {
		if (corporate == null)
			throw new NullArgumentException("corporate");

		corporate.setName(this.name);
		corporate.setDescription(this.description);
		corporate.setClientRef(this.clientRef);
		corporate.setRegistrationDate(this.registrationDate);
		corporate.setPhoneOne(getPhoneOne());
		corporate.setPhoneTwo(getPhoneTwo());
		corporate.setEmail(getEmail());
		corporate.setPobox(getPobox());
		corporate.setZipCode(getZipCode());
		corporate.setBuildingNum(getBuildingNum());
		corporate.setStreetName(getStreetName());
		corporate.setNote(getNote());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getClientRef() {
		return clientRef;
	}

	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public long getPrefLangId() {
		return prefLangId;
	}

	public void setPrefLangId(long prefLangId) {
		this.prefLangId = prefLangId;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public List<String> getTenants() {
		return tenants;
	}

	public void setTenants(List<String> tenants) {
		this.tenants = tenants;
	}

	private List<String> getTenantNames(List<Tenant> tenants) {
		if (tenants == null)
			return new ArrayList<>();
		return tenants.stream().map(Tenant::getName).collect(Collectors.toList());
	}

	@Override
	public String toString() {
		JSONSerializer serializer = new JSONSerializer().exclude("*.class");
		return serializer.deepSerialize(this);
	}

	public static CorporateMetaData fromJson(String data) {
		JSONDeserializer<CorporateMetaData> deserializer = new JSONDeserializer<>();
		return deserializer.deserialize(data, CorporateMetaData.class);
	}

	public String getKycTemplate() {
		return kycTemplate;
	}
}
