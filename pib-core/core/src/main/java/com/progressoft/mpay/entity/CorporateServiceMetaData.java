package com.progressoft.mpay.entity;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_CorpoarteService;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class CorporateServiceMetaData {
    private String name;
    private String description;
//    private String notificationReceiver;
    private long serviceCategoryId;
    private String serviceTypeCode;

//    private String notificationChannelCode;

    public CorporateServiceMetaData() {
    	//Default
    }

    public CorporateServiceMetaData(MPAY_CorpoarteService service) {
        if (service == null)
            throw new NullArgumentException("service");

        this.name = service.getName();
        this.description = service.getDescription();
//        this.notificationReceiver = service.getNotificationReceiver();
        this.serviceCategoryId = service.getServiceCategory().getId();
        this.serviceTypeCode = service.getServiceType().getCode();
//        this.notificationChannelCode = service.getNotificationChannel().getCode();
    }

    public static CorporateServiceMetaData fromJson(String data) {
        return new JSONDeserializer<CorporateServiceMetaData>().deserialize(data, CorporateServiceMetaData.class);
    }

    public void fillCorporateServiceFromMetaData(MPAY_CorpoarteService service) {
        if (service == null)
            throw new NullArgumentException("service");

        service.setName(this.name);
        service.setDescription(this.description);
//        service.setNotificationReceiver(this.notificationReceiver);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public String getNotificationReceiver() {
//        return notificationReceiver;
//    }
//
//    public void setNotificationReceiver(String notificationReceiver) {
//        this.notificationReceiver = notificationReceiver;
//    }

    public long getServiceCategoryId() {
        return serviceCategoryId;
    }

    public void setServiceCategoryId(long serviceCategoryId) {
        this.serviceCategoryId = serviceCategoryId;
    }

    public String getServiceTypeCode() {
        return serviceTypeCode;
    }

    public void setServiceTypeCode(String serviceTypeCode) {
        this.serviceTypeCode = serviceTypeCode;
    }

//    public String getNotificationChannelCode() {
//        return notificationChannelCode;
//    }

//    public void setNotificationChannelId(String notificationChannelCode) {
//        this.notificationChannelCode = notificationChannelCode;
//    }

    @Override
    public String toString() {
        JSONSerializer serializer = new JSONSerializer().exclude("*.class");
        return serializer.serialize(this);
    }
}
