package com.progressoft.mpay.banksintegration;

import com.progressoft.mpay.entities.MPAY_BankIntegMessage;

public interface BankIntegrationProcessor {
	BankIntegrationResult processMessage(BankIntegrationContext context);
	BankIntegrationResult reverseMessage(BankIntegrationContext context);
	BankIntegrationResult processBalanceInquiryMessage(BankIntegrationContext context);
	MPAY_BankIntegMessage createBalanceInquiryMessage(BankIntegrationContext context);
	MPAY_BankIntegMessage createMessage(BankIntegrationContext context);
	MPAY_BankIntegMessage createReversalMessage(BankIntegrationContext context);
	MPAY_BankIntegMessage createChargeMessage(BankIntegrationContext context);
}
