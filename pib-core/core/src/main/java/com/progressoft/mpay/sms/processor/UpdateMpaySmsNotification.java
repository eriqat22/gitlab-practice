package com.progressoft.mpay.sms.processor;

import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.jaxb.JaxbHandler;
import com.progressoft.mpay.sms.NotificationResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.List;

public class UpdateMpaySmsNotification implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateMpaySmsNotification.class);
    private static final String MPAY_NOTIFICATION_ID = "mpayNotificationId";
    @Autowired
    private ItemDao itemDao;

    @Override
    public void process(Exchange exchange) throws Exception {
        try {
            LOGGER.info("SMS Response" + exchange.getIn().getBody());
            IntegrationUtils.enableServiceUser((String) exchange.getProperty("tenantId"));
            Unmarshaller unmarshaller = JaxbHandler.getJaxbContextInstance().createUnmarshaller();
            NotificationResponse notificationResponse = (NotificationResponse) unmarshaller.unmarshal(new StringReader((String) exchange.getIn().getBody()));
            Long refMessageId = Long.parseLong((String) exchange.getProperty(MPAY_NOTIFICATION_ID));
            List<MPAY_Notification> mpayNotifications = MPayContext.getInstance().getDataProvider().getMPAYNotificationByRefMessageId(refMessageId);
            for(MPAY_Notification mpayNotification:mpayNotifications) {
                mpayNotification.setSuccess(getSuccessFromResponse(notificationResponse));
                if (!mpayNotification.getSuccess()) {
                    mpayNotification.setErrorDesc(notificationResponse.getErrorDescription());
                }
                itemDao.merge(mpayNotification);
            }
        } catch (Exception e) {
            throw new BusinessException("An error occurred while updating sms notification ", e);
        }
    }

    private boolean getSuccessFromResponse(NotificationResponse notificationResponse) {
        return "success".equalsIgnoreCase(notificationResponse.getStatus());
    }

}
