package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.*;
import com.progressoft.mpay.common.DateUtils;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.*;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.registration.AccountsHelper;
import com.progressoft.mpay.registration.ClientManagement;
import com.progressoft.mpay.registration.customers.CustomerRegistrationNotificationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import static com.progressoft.jfw.model.sample.JFWCurrencyAmountSample_.time;
import static java.util.Objects.nonNull;

public class PostServiceAccountRegistrationApproval implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostServiceAccountRegistrationApproval.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute--------------------------");

        MPAY_ServiceAccount account = (MPAY_ServiceAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);

        if (account.getApprovedData() != null)
            applyApprovedData(account);
        if (nonNull(account.getIsRegistered()) && account.getIsRegistered()) {
            return;
        }
        account.setIsRegistered(true);
        createWalletAccount(account.getService(), account);
        try {
            notifyCorporate(account);
        } catch (Exception e) {
            logger.error("Error while trying to notify customer", e);
            throw new WorkflowException(e);
        }
    }

    private void applyApprovedData(MPAY_ServiceAccount account) {

        account.setRefProfile(account.getNewProfile());
        account.setExternalAcc(account.getNewExternalAcc());
        account.setNewExternalAcc(null);
        account.setNewProfile(null);
        account.setApprovedData(null);

    }

    private void createWalletAccount(MPAY_CorpoarteService service, MPAY_ServiceAccount serviceAccount)
            throws WorkflowException {
        String accountTypeCode = serviceAccount.getCategory().getId().toString().equals(AccountCategory.COMMISSION) ? AccountTypes.COMMISSION : AccountTypes.WALLET;
        MPAY_AccountType accountType = LookupsLoader.getInstance().getAccountTypes(accountTypeCode);
        MPAY_Account account = AccountsHelper.createAccount(
                service.getName() + " " + ClientManagement.getRegistrationId(serviceAccount),
                SystemParameters.getInstance().getDefaultCurrency(), new BigDecimal(0), accountType,
                serviceAccount.getExternalAcc(),
                MPayContext.getInstance().getLookupsLoader().getBalanceType(BalanceTypes.DEBIT),
                serviceAccount.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));

        DataProvider.instance().persistAccount(account);
        serviceAccount.setRefAccount(account);

        logger.debug("\n\r===Saving Account....");
        DataProvider.instance().mergeCorporateService(service);
    }

    public void notifyCorporate(MPAY_ServiceAccount account) throws WorkflowException {
        try {
            MPAY_CorpoarteService service = account.getService();
            boolean sendActivationCodeOrNot = SystemParameters.getInstance().getSendActivationCodeUponCustomerMobileRegistration();
            boolean sendPinCode = SystemParameters.getInstance().getSendPinCodeUponCorporateRegistration();

            MPAY_EndPointOperation operation = LookupsLoader.getInstance().getEndPointOperation("corporateReg");
            CustomerRegistrationNotificationContext context = new CustomerRegistrationNotificationContext();
            MPAY_Language language = service.getRefCorporate().getPrefLang();

            if (language.getCode().equals(LanguageCode.ENGLISH))
                context.setAppName(SystemParameters.getInstance().getApplicationNameEn());
            else
                context.setAppName(SystemParameters.getInstance().getApplicationNameAr());
            context.setReference(service.getRefCorporate().getClientRef());
            context.setAccountNumber(ClientManagement.getRegistrationId(account));
            context.setExtraData2(String.valueOf(language.getId()));
            if (sendActivationCodeOrNot) {
                int activationCodeLength = SystemParameters.getInstance().getActivationCodeLength();
                String activationCode = OTPGenerator.generateCode(activationCodeLength);
                context.setActivationCode(activationCode);
                context.setHasActivationCode(true);
                service.setActivationCode(SHA1Hashing.sha1hashing(activationCode));
                service.setActivationCodeValidy(DateUtils.getValidityTime(SystemParameters.getInstance().getActivationCodeValidity()));
                context.setExtraData3(getValidityTime(service.getActivationCodeValidy()));
            }

//			NotificationHelper.getInstance().createRegistrationNotification(null, context,
//					NotificationType.ACCEPTED_SENDER, operation.getId(), language.getId(),
//					service.getName(), service.getNotificationChannel().getCode());


            if (sendPinCode) {
                int length = SystemParameters.getInstance().getPinCodeLength();
                String pinCode = OTPGenerator.generateCode(length);
                String hashedPinCode = HashingAlgorithm.hash(SystemParameters.getInstance(), pinCode);
                service.setPin(hashedPinCode);
                service.setPinLastChanged(SystemHelper.getSystemTimestamp());
                String encryptedOTP = MPayCryptographer.DefaultCryptographer.INSTANCE.get().encrypt(pinCode, false);
                context.setPinCode("\"" + encryptedOTP + "\"");
                NotificationHelper.getInstance().createRegistrationNotification(null, context,
                        NotificationType.ACCEPTED_RECEIVER, operation.getId(), language.getId(),
                        service.getName(), NotificationChannelsCode.SMS);
            }

        } catch (Exception e) {
            logger.error(
                    "error create notification for activation code when register new corporate serivce account ...."
                            + e);
            throw new WorkflowException(e);
        }
    }

    private String getValidityTime(Timestamp activationCodeValidy) {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(activationCodeValidy);
    }
}