package com.progressoft.mpay.entity;




import java.util.Date;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_Customer;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

public class CustomerMetaData extends ClientMetaData {
    private String fullName;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date dob;
    private String clientRef;

    private String rejectionNote;

    private long prefLangId;
    private String cityCode;
    private String kycTemplate;

    public CustomerMetaData() {
        //Default

    }

    public CustomerMetaData(MPAY_Customer customer) {
        if (customer == null)
            throw new NullArgumentException("customer");

        this.fullName = customer.getFullName();
        this.firstName = customer.getFirstName();
        this.middleName = customer.getMiddleName();
        this.lastName = customer.getLastName();
        this.dob = customer.getDob();
        setPhoneOne(customer.getPhoneOne());
        setPhoneTwo(customer.getPhoneTwo());
        setEmail(customer.getEmail());
		setPobox(customer.getPobox());
		setZipCode(customer.getZipCode());
		setBuildingNum(customer.getBuildingNum());
		setStreetName(customer.getStreetName());
		setNote(customer.getNote());
        this.rejectionNote = customer.getRejectionNote();
        this.clientRef = customer.getClientRef();

        this.prefLangId = customer.getPrefLang().getId();
        this.cityCode = customer.getCity().getCode();
        this.kycTemplate = customer.getKycTemplate().getName();
    }

    public static CustomerMetaData fromJson(String data) {
        return new JSONDeserializer<CustomerMetaData>().deserialize(data, CustomerMetaData.class);
    }

    public void fillCustomerFromMetaData(MPAY_Customer customer) {
        if (customer == null)
            throw new NullArgumentException("customer");

        customer.setFullName(this.fullName);
        customer.setFirstName(this.firstName);
        customer.setMiddleName(this.middleName);
        customer.setLastName(this.lastName);
        customer.setClientRef(this.clientRef);
        customer.setDob(this.dob);
        customer.setPhoneOne(getPhoneOne());
        customer.setPhoneTwo(getPhoneTwo());
        customer.setEmail(getEmail());
        customer.setPobox(getPobox());
        customer.setZipCode(getZipCode());
		customer.setBuildingNum(getBuildingNum());
		customer.setStreetName(getStreetName());
		customer.setNote(getNote());
        customer.setRejectionNote(this.rejectionNote);
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getClientRef() {
        return clientRef;
    }

    public void setClientRef(String clientRef) {
        this.clientRef = clientRef;
    }

    public String getRejectionNote() {
        return rejectionNote;
    }

    public void setRejectionNote(String rejectionNote) {
        this.rejectionNote = rejectionNote;
    }

    public long getPrefLangId() {
        return prefLangId;
    }

    public void setPrefLangId(long prefLangId) {
        this.prefLangId = prefLangId;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    @Override
    public String toString() {
        JSONSerializer serializer = new JSONSerializer().exclude("*.class");
        return serializer.serialize(this);
    }

    public String getKycTemplate() {
        return kycTemplate;
    }

    public void setKycTemplate(String kycTemplate) {
        this.kycTemplate = kycTemplate;
    }
}
