package com.progressoft.mpay;

import java.math.BigDecimal;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.mpay.email.EmailSender;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.sms.NotificationRequest;

public class CashCapNotificationProcessor implements Processor {
	private static final Logger logger = LoggerFactory.getLogger(CashCapNotificationProcessor.class);

	@Override
	public void process(Exchange exchange) throws Exception {

		boolean enabledByMe = false;
		try {
			enabledByMe = IntegrationUtils.enableServiceUser((String) exchange.getProperty("tenantId"));
			BigDecimal cashCasp = MPayContext.getInstance().getSystemParameters().getCashCap();
			BigDecimal watermark = new BigDecimal(MPayContext.getInstance().getSystemParameters().getCashCapWatermark());
			if (MPayContext.getInstance().getLookupsLoader().getPSP() == null)
				return;
			MPAY_Account cashInAccount = MPayContext.getInstance().getLookupsLoader().getPSP().getPspCashInAccount();
			MPAY_Account cashOutAccount = MPayContext.getInstance().getLookupsLoader().getPSP().getPspCashOutAccount();
			if (cashInAccount == null || cashOutAccount == null)
				return;
			BigDecimal cashInAccountBalance = MPayContext.getInstance().getLookupsLoader().getPSP().getPspCashInAccount().getBalance();
			BigDecimal cashOutAccountBalance = MPayContext.getInstance().getLookupsLoader().getPSP().getPspCashInAccount().getBalance();
			BigDecimal currentBalance = cashInAccountBalance.add(cashOutAccountBalance).multiply(new BigDecimal(-1));
			BigDecimal watermarkBalance = cashCasp.multiply(watermark).divide(new BigDecimal(100));
			if (currentBalance.compareTo(watermarkBalance) >= 0)
				sendEmail(cashCasp, currentBalance);

		} catch (Exception ex) {
			logger.error(ex.toString(), ex);
		} finally {
			if (enabledByMe) {
				IntegrationUtils.disableServiceUser();
			}
		}
	}

	private void sendEmail(BigDecimal cashCap, BigDecimal currentBalance) {
		try {
			String content = "Cash Cap Watermark Reached CAP: " + cashCap + ", current balance is: " + currentBalance;
			logger.info(content);
			if (MPayContext.getInstance().getSystemParameters().getCashCAPNotificationSent()) {
				logger.info("Notification already sent, change system configuration 'Cash CAP Notification Sent' = 0 to enable sending another one");
				return;
			}
			EmailSender sender = new EmailSender();
			NotificationRequest notificationRequest = new NotificationRequest();
			notificationRequest.setContent(content);
			notificationRequest.setReciever(MPayContext.getInstance().getSystemParameters().getCashCAPNotificationEmail());
			notificationRequest.setSender(SystemParameters.getInstance().getEmailFrom());
			sender.send(notificationRequest);
			MPayContext.getInstance().getDataProvider().updateSystemParameter(SysConfigKeys.CASH_CAP_NOTIFICATION_SENT, "1");
		} catch (Exception e) {
			logger.error("Failed to send Cash Cap Notification", e);
		}
	}
}