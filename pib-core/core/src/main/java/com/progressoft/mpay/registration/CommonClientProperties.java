package com.progressoft.mpay.registration;

public class CommonClientProperties {
	public static final String EXTERNAL_ACC_PROP = "externalAcc";
	public static final String REF_PROFILE_PROP = "refProfile";
	public static final String BANKED_UNBANKED_PROP = "bankedUnbanked";
	public static final String ALIAS = "alias";
	public static final String BANK = "bank";
	public static final String MAS = "mas";
	public static final String IS_REGISTERED = "isRegistered";
	public static final String RETRY_COUNT = "retryCount";
	public static final String IS_BLOCKED = "isBlocked";
	public static final String IS_DEFAULT = "isDefault";
	public static final String IS_SWITCH_DEFAULT = "isSwitchDefault";
	public static final String REF_ACCOUNT = "refAccount";
	public static final String WALLET_TYPE = "walletType";
	public static final String TRANSACTION_SIZE = "transactionSize";
	public static final String CHARGE_AMOUNT_NUMBER = "chargeAmountNumber";
	public static final String CHARGE_AMOUNT_CHARS = "chargeAmountCharacters";
	public static final String WALLET_ACCOUNT = "walletAccount";
	public static final String CHARGE_DURATION = "chargeDuration";
	public static final String IBAN = "iban";
	public static final String MOBILE_NUMBER = "mobileNumber";

	private CommonClientProperties() {

	}
}
