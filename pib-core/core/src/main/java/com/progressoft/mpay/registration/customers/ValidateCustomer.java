package com.progressoft.mpay.registration.customers;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemNetworkStatus;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_Customer;

public class ValidateCustomer implements Validator {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ValidateCustomer.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map transientVar, Map arg1, PropertySet arg2) throws InvalidInputException {
		logger.debug("Inside validate ==========");
		MPAY_Customer customer = (MPAY_Customer) transientVar.get(WorkflowService.WF_ARG_BEAN);
		validateCustomerDateOfBirth(customer);
		validateCustomerMobileCount(customer);
		validateSystemLoggedIn();
	}

	private void validateCustomerDateOfBirth(MPAY_Customer customer) throws InvalidInputException {
		logger.debug("Inside  ValidateCustomerDateOfBirth ==========");
		long minAge = Integer.parseInt(MPayContext.getInstance().getLookupsLoader()
				.getSystemConfigurations(SysConfigKeys.MIN_AGE).getConfigValue());
		InvalidInputException iie = new InvalidInputException();
		StringBuilder sb = new StringBuilder();

		if (customer.getDob().after(SystemHelper.getCurrentDateTime()))
			sb.append(AppContext.getMsg(MPayErrorMessages.CUSTOMER_INVALID_DOB_FUTURE));

		if (DateUtils.addYears(customer.getDob(), (int) minAge).after(SystemHelper.getCurrentDateTime()))
			sb.append(AppContext.getMsg(MPayErrorMessages.CUSTOMER_INVALID_AGE) + ": " + minAge);

		if (!sb.toString().isEmpty())
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, sb.toString());

		if (iie.getErrors().size() > 0)
			throw iie;
	}

	private void validateSystemLoggedIn() throws InvalidInputException {
		logger.debug("Inside ValidateSystemLoggedIn ==========");
		InvalidInputException iie = new InvalidInputException();
		if (!SystemNetworkStatus.isSystemOnline(MPayContext.getInstance().getDataProvider())) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("SystemIsLoggedOut"));
			throw iie;
		}
	}

	private void validateCustomerMobileCount(MPAY_Customer customer) throws InvalidInputException {
		logger.debug("Inside ValidateCustomerMobileCount ===========");

		boolean creationMobileFromInteg = customer.getExternalAcc() != null && !customer.getExternalAcc().isEmpty()
				&& customer.getMobileNumber() != null && !customer.getMobileNumber().isEmpty();
		if (!creationMobileFromInteg && customer.getMobileNumber() == null
				&& CollectionUtils.isEmpty(customer.getRefCustomerCustomerMobiles())) {
			InvalidInputException cause = new InvalidInputException();
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS,
					AppContext.getMsg("customer.invalid.shouldHaveOneService"));
			throw cause;
		}

		Map<String, Object> props = new HashMap<>();
		props.put("idType", customer.getIdType());
		props.put("idNum", customer.getIdNum());
		props.put("nationality", customer.getNationality());
		InvalidInputException cause = Unique.validate(itemDao, customer, props,
				MPayErrorMessages.DUPLICATE_CUSTOMER_ID);

		if (cause.getErrors().size() > 0)
			throw cause;
	}
}
