package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.MPAYView;

public class PostApproveCorporateSuspend implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostApproveCorporateSuspend.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_Corporate corporate = (MPAY_Corporate) transientVars.get(WorkflowService.WF_ARG_BEAN);
		corporate.setIsActive(false);
		handleServices(corporate);
	}

	private void handleServices(MPAY_Corporate corporate) throws WorkflowException {
		logger.debug("inside HandleServices--------------------------");
		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId());
		if (services == null || services.isEmpty())
			return;
		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			if (!service.getStatusId().getCode().equals(CorporateServiceWorkflowStatuses.APPROVED.toString())) {
				InvalidInputException iie = new InvalidInputException();
				iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.SERVICE_UNDER_MODIFICATION));
				throw iie;
			}
			JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.SUSPEND, new WorkflowException());
		} while (servicesIterator.hasNext());
	}
}
