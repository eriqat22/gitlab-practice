package com.progressoft.mpay.registration.corporates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.shared.DynamicField;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.ServiceAccountMetaData;
import com.progressoft.mpay.registration.customers.MobileAccountMetaDataTranslationKeys;

public class DisplayServiceAccountChanges implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(DisplayServiceAccountChanges.class);
	private static final String MODIFIED_COLOR = "blue";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handle--------------------------");
		MPAY_ServiceAccount account = (MPAY_ServiceAccount) changeHandlerContext.getEntity();
		try {
			Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
			for (UIPortletAttributes ui : portletsAttributes) {
				ui.setVisible(true);
			}
			renderFields(changeHandlerContext, account, ServiceAccountMetaData.fromJson(account.getApprovedData()));
		} catch (Exception e) {
			throw new InvalidInputException(e);
		}

	}

	private void renderFields(ChangeHandlerContext changeHandlerContext, MPAY_ServiceAccount serviceAccount, ServiceAccountMetaData approvedServiceAccount) {
		logger.debug("inside RenderFields--------------------------");
		List<DynamicField> dynamicFields = new ArrayList<>();

		DynamicField serviceField = new DynamicField();
		serviceField.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.FIELD));
		setBold(serviceField);
		dynamicFields.add(serviceField);

		serviceField = new DynamicField();
		serviceField.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.OLD_DATA));
		setBold(serviceField);
		dynamicFields.add(serviceField);

		serviceField = new DynamicField();
		serviceField.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.NEW_DATA));
		setBold(serviceField);
		dynamicFields.add(serviceField);

		serviceField = new DynamicField();
		serviceField.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.EXTERNAL_ACCOUNT));
		setBold(serviceField);
		dynamicFields.add(serviceField);

		serviceField = new DynamicField();
		serviceField.setLabel(approvedServiceAccount.getExternalAcc());
		dynamicFields.add(serviceField);

		serviceField = new DynamicField();
		serviceField.setLabel(serviceAccount.getNewExternalAcc());
		if (!StringUtils.equals(approvedServiceAccount.getExternalAcc(), serviceAccount.getNewExternalAcc()))
			serviceField.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(serviceField);

		MPAY_Profile profile = LookupsLoader.getInstance().getProfile(approvedServiceAccount.getProfileId());

		serviceField = new DynamicField();
		serviceField.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.PROFILE));
		serviceField.setLabelBold(true);
		dynamicFields.add(serviceField);

		serviceField = new DynamicField();
		serviceField.setLabel(profile.getName());
		dynamicFields.add(serviceField);

		serviceField = new DynamicField();
		serviceField.setLabel(serviceAccount.getNewProfile().getName());
		if (approvedServiceAccount.getProfileId() != serviceAccount.getRefProfile().getId())
			serviceField.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(serviceField);

		changeHandlerContext.getPortletsAttributes().get("Info").setVisible(true);
		changeHandlerContext.getPortletsAttributes().get("Info").setDynamicPortlet(true);
		changeHandlerContext.getPortletsAttributes().get("Info").setDynamicFields(dynamicFields);
	}

	private void setBold(DynamicField field) {
		field.setLabelBold(true);
	}
}
