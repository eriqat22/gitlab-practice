package com.progressoft.mpay;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.shared.JfwCurrencyInfo;
import com.progressoft.mpay.entities.MPAY_ChargesScheme;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;
import com.progressoft.mpay.entities.MPAY_ChargesSlice;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entity.ChargeTypes;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.payments.ChargeSliceAmountComparer;
import com.progressoft.mpay.plugins.ProcessingContext;

public final class ChargesCalculator {
	private static final BigDecimal HUNDRED = new BigDecimal("100");

	private ChargesCalculator() {
	}

	public static void calculate(ProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");

		if (context.getTransactionConfig() == null)
			return;

		if (context.getSender().getProfile() != null) {
			setChargeScale(context, context.getSender().getProfile().getChargesScheme().getCurrency());
			calculateSenderCharge(context.getTransactionConfig().getMessageType().getCode(), context, context.getSender().getProfile());
		}

		if (context.getReceiver().getProfile() != null) {
			setChargeScale(context, context.getReceiver().getProfile().getChargesScheme().getCurrency());
			calculateReceiverCharge(context.getTransactionConfig().getMessageType().getCode(), context, context.getReceiver().getProfile());
		}
	}

	public static void calculateSenderCharge(String messageTypeCode, ProcessingContext context, MPAY_Profile profile) {
		if (context == null)
			return;
		if(profile == null)
			throw new NullArgumentException("profile");
		context.getSender().setCharge(calculateCharge(context.getDataProvider(), context.getAmount(), messageTypeCode, profile, true));
	}

	public static void calculateReceiverCharge(String messageTypeCode, ProcessingContext context, MPAY_Profile profile) {
		if (context == null)
			return;
		context.getReceiver().setCharge(calculateCharge(context.getDataProvider(), context.getAmount(), messageTypeCode, profile, false));
	}

	public static BigDecimal calculateCharge(IDataProvider dataProvider, BigDecimal amount, String messageTypeCode, MPAY_Profile profile, boolean isSender) {
		try {
			MPAY_ChargesScheme chargeScheme = checkChargeScheme(profile);
			if (chargeScheme == null)
				return BigDecimal.ZERO;

			MPAY_ChargesSchemeDetail chargeSchemeDetails = checkChargeSchemeDetails(dataProvider, messageTypeCode, chargeScheme);
			if (chargeSchemeDetails == null)
				return BigDecimal.ZERO;

			List<MPAY_ChargesSlice> chargeSlices = getChargeSlice(chargeSchemeDetails, amount);
			if (chargeSlices.isEmpty())
				return BigDecimal.ZERO;

			BigDecimal chargeAmount = BigDecimal.ZERO;

			JfwCurrencyInfo currency = dataProvider.getCurrencyInfo(chargeScheme.getCurrency());
			BigDecimal percentage;
			if (isSender)
				percentage = new BigDecimal(chargeSchemeDetails.getSendChargeBearerPercent());
			else
				percentage = new BigDecimal(chargeSchemeDetails.getReceiveChargeBearerPercent());

			Iterator<MPAY_ChargesSlice> slicesIterator = chargeSlices.iterator();
			do {
				MPAY_ChargesSlice slice = slicesIterator.next();
				chargeAmount = chargeAmount.add(calculateSliceCharge(slice, percentage, amount, currency));
			} while (slicesIterator.hasNext());

			return chargeAmount;
		} catch (Exception ex) {
			throw new MPayGenericException("Error While calculateCharges", ex);
		}
	}

	private static MPAY_ChargesSchemeDetail checkChargeSchemeDetails(IDataProvider dataProvider, String messageTypeCode, MPAY_ChargesScheme chargeScheme) {
		MPAY_ChargesSchemeDetail chargeSchemeDetails = getChargeSchemeDetails(dataProvider, messageTypeCode, chargeScheme);
		if (chargeSchemeDetails == null || !chargeSchemeDetails.getIsActive())
			return null;
		return chargeSchemeDetails;
	}

	private static MPAY_ChargesScheme checkChargeScheme(MPAY_Profile profile) {
		MPAY_ChargesScheme chargeScheme = profile.getChargesScheme();
		if (!chargeScheme.getIsActive())
			return null;
		return chargeScheme;
	}

	private static BigDecimal calculateSliceCharge(MPAY_ChargesSlice chargeSlice, BigDecimal bearerPercentage, BigDecimal amount, JfwCurrencyInfo currency) {
		BigDecimal chargeAmount;
		BigDecimal totalChargeAmount;

		if (chargeSlice.getChargeType().equalsIgnoreCase(ChargeTypes.FIXED)) {
			chargeAmount = (chargeSlice.getChargeAmount().multiply(bearerPercentage)).divide(HUNDRED);
			chargeAmount = chargeAmount.setScale(currency.getFractionDigits(), RoundingMode.HALF_DOWN);
		} else {
			totalChargeAmount = (amount.multiply(chargeSlice.getChargePercent())).divide(HUNDRED);
			totalChargeAmount = totalChargeAmount.compareTo(chargeSlice.getMinAmount()) < 0 ? chargeSlice.getMinAmount() : totalChargeAmount;
			totalChargeAmount = totalChargeAmount.compareTo(chargeSlice.getMaxAmount()) > 0 ? chargeSlice.getMaxAmount() : totalChargeAmount;

			chargeAmount = (totalChargeAmount.multiply(bearerPercentage)).divide(HUNDRED);
			chargeAmount = chargeAmount.setScale(currency.getFractionDigits(), RoundingMode.HALF_DOWN);
		}

		return chargeAmount;
	}

	private static List<MPAY_ChargesSlice> getChargeSlice(MPAY_ChargesSchemeDetail chargeSchemeDetails, BigDecimal amount) {
		List<MPAY_ChargesSlice> fliteredSlices = new ArrayList<>();
		List<MPAY_ChargesSlice> resultSlices = new ArrayList<>();
		List<MPAY_ChargesSlice> allSlices = chargeSchemeDetails.getRefChargesDetailsChargesSlices();
		if (allSlices == null)
			return resultSlices;
		fliteredSlices.addAll(allSlices.stream().filter(mpayChargesSlicePredicate -> mpayChargesSlicePredicate.getTxAmountLimit().compareTo(amount) >= 0 && (mpayChargesSlicePredicate.getDeletedFlag() == null || !mpayChargesSlicePredicate.getDeletedFlag())).collect(Collectors.toList()));
		if (fliteredSlices.isEmpty())
			return fliteredSlices;
		ChargeSliceAmountComparer comparor = new ChargeSliceAmountComparer();
		Collections.sort(fliteredSlices, comparor);
		MPAY_ChargesSlice minSlice = fliteredSlices.get(0);
		resultSlices.addAll(fliteredSlices.stream().filter(slice -> slice.getTxAmountLimit().compareTo(minSlice.getTxAmountLimit()) == 0).collect(Collectors.toList()));
		return resultSlices;
	}

	private static MPAY_ChargesSchemeDetail getChargeSchemeDetails(IDataProvider dataProvider, String msgTypeCode, MPAY_ChargesScheme chargeScheme) {
		return dataProvider.getChargeScheme(msgTypeCode, chargeScheme.getId());
	}

	private static void setChargeScale(ProcessingContext context, JFWCurrency currency) {
		JfwCurrencyInfo currencyInfo = context.getDataProvider().getCurrencyInfo(currency);
		context.getSender().setCharge(context.getSender().getCharge().setScale(currencyInfo.getFractionDigits(), RoundingMode.HALF_DOWN));
		context.getReceiver().setCharge(context.getReceiver().getCharge().setScale(currencyInfo.getFractionDigits(), RoundingMode.HALF_DOWN));
	}
}
