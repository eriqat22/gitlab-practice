package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.registration.ValidateAccountDeletion;
import com.progressoft.mpay.registration.ValidateNoServicesAreOnModifiedState;
import com.progressoft.mpay.registration.customers.ValidateDeleteCustomer;

public class ValidateDeleteCorporate implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteCustomer.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside validate =======================");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);

		EmptyChildCorproatesValidator.validate(corporate);

		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId());

		if (services == null || services.isEmpty())
			return;

		ValidateNoServicesAreOnModifiedState.validate(corporate);

		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			ValidateAccountDeletion.validate(service);
		} while (servicesIterator.hasNext());

	}
}