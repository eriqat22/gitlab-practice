
package com.progressoft.mpay.webclient.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.progressoft.mpay.messages.ExtraData;

import java.util.List;

public class RequestModel {
    @SerializedName("operation")
    private String operation;
    @SerializedName("sender")
    private String sender;
    @SerializedName("senderType")
    private String senderType;
    @SerializedName("deviceId")
    private String deviceId;
    @SerializedName("lang")
    private int lang;
    @SerializedName("msgId")
    private String msgId;
    @SerializedName("pin")
    private String pin;
    @SerializedName("tenant")
    private String tenant;
    @SerializedName("amount")
    private String amount;
    @SerializedName("checksum")
    private String CheckSum;
    @SerializedName("extraData")
    private List<ExtraData> extraData;
    @SerializedName("requestedId")
    private String requestedId;
    @SerializedName("shopId")
    private String shopId;
    @SerializedName("channelId")
    private String channelId;
    @SerializedName("walletId")
    private String walletId;
    @SerializedName("isAuthorized")
    private Integer isAuthorized;
    @SerializedName("senderAccount")
    private String senderAccount;
    @SerializedName("receiverAccount")
    private String receiverAccount;
    @SerializedName("notes")
    private String notes;
    @SerializedName("receiver")
    protected String receiver;
    @SerializedName("receiverType")
    protected String receiverType;

    public String getCheckSum() {
        return this.CheckSum;
    }

    public void setCheckSum(String checkSum) {
        this.CheckSum = checkSum;
    }

    public String getTenant() {
        return this.tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getOperation() {
        return this.operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getSender() {
        return this.sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderType() {
        return this.senderType;
    }

    public void setSenderType(String senderType) {
        this.senderType = senderType;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getLang() {
        return this.lang;
    }

    public void setLang(int lang) {
        this.lang = lang;
    }

    public String getMsgId() {
        return this.msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getPin() {
        return this.pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public List<ExtraData> getExtraData() {
        return this.extraData;
    }

    public void setExtraData(List<ExtraData> extraData) {
        this.extraData = extraData;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRequestedId() {
        return requestedId;
    }

    public void setRequestedId(String requestedId) {
        this.requestedId = requestedId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public Integer getIsAuthorized() {
        return isAuthorized;
    }

    public void setIsAuthorized(Integer isAuthorized) {
        this.isAuthorized = isAuthorized;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    public String getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public String toString() {
        Gson gson = (new GsonBuilder()).create();
        return gson.toJson(this);
    }

    public String isStringNull(String value) {
        return value == null ? "" : value;
    }



}
