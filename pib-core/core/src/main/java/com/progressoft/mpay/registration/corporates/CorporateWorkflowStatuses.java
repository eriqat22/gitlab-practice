package com.progressoft.mpay.registration.corporates;

public class CorporateWorkflowStatuses {
	public static final String APPROVED = "200005";
	public static final String DELETED = "200006";
	public static final String INTEGRATION = "200007";

	private CorporateWorkflowStatuses() {

	}
}
