package com.progressoft.mpay.psp;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_PspDetail;
import com.progressoft.mpay.entity.MPAYView;

public class PostApprovePSPDraftDetails implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostApprovePSPDraftDetails.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("\n\r===inside PostApprovePSPDraftDetails....");

		try {
			MPAY_Corporate psp = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
			psp.setIsRegistered(true);
//			approvePSPDetails(psp);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new WorkflowException("An error occured PostApprovePSPDraftDetails.", ex);
		}
	}

//	private void approvePSPDetails(MPAY_Corporate psp) throws WorkflowException {
//		MPAY_PspDetail detail = psp.getRefPSPPspDetails().get(0);
//
//		if (detail.getJfwDraft() != null) {
//			Map<Option, Object> options = new EnumMap<>(Option.class);
//			options.put(Option.ACTION_NAME, "SVC_Approve");
//			JfwHelper.executeAction(MPAYView.PSP_DETAILS, detail, options);
//		}
//	}
}
