package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.MPAYView;

public class HandleCustomerMobileCancel implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(HandleCustomerMobileCancel.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute ============");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (mobile.getIsRegistered())
			JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, CustomerMobileWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, CustomerMobileWorkflowServiceActions.REJECT, new WorkflowException());
	}
}
