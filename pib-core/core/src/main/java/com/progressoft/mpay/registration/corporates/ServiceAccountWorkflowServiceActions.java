package com.progressoft.mpay.registration.corporates;

public class ServiceAccountWorkflowServiceActions {
	public static final String APPROVE = "SVC_Approve";
	public static final String DIRECT_APPROVE = "SVC_DirectApprove";
	public static final String REJECT = "SVC_Reject";
	public static final String DELETE = "SVC_Delete";
	public static final String DELETE_APPROVAL = "SVC_DeleteApproval";
	public static final String SEND_APPROVAL = "SVC_SendApproval";
	public static final String DEFAULT_REJECT = "SVC_DefaultReject";
	public static final String SUSPEND = "SVC_Suspend";
	public static final String REACTIVATE = "SVC_Reactivate";

	private ServiceAccountWorkflowServiceActions() {

	}
}
