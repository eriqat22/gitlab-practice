package com.progressoft.mpay.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MPayResponseEnvelope {
	private static final Logger logger = LoggerFactory.getLogger(MPayResponseEnvelope.class);
private MPayResponse response;
	private String token;

	public MPayResponse getResponse() {
		return response;
	}
	public void setResponse(MPayResponse response) {
		this.response = response;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}

	public static MPayResponseEnvelope fromJson(String json) {
		try {
			Gson gson = new GsonBuilder().serializeNulls().create();
			return gson.fromJson(json, MPayResponseEnvelope.class);
		} catch (Exception ex) {
			logger.debug("Error while fromJson", ex);
			return null;
		}
	}
}
