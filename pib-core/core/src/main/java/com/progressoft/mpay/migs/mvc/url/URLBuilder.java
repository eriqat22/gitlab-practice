package com.progressoft.mpay.migs.mvc.url;

public class URLBuilder {

	private String baseUrl;
	private StringBuilder url;
	private boolean firstParam;

	public URLBuilder(String baseUrl) {
		this.baseUrl = baseUrl;
		url = new StringBuilder();
		url.append(baseUrl);
		setFirstParam(true);
	}

	public void addParameter(String name , String value) {
		if(isFirstParam()) {
			setFirstParam(false);
			url.append("?").append(name).append("=").append(value);
		} else {
			url.append("&").append(name).append("=").append(value);
		}
	}

	public String getDomain() {
		return baseUrl;
	}

	public void setFirstParam(boolean firstParam) {
		this.firstParam = firstParam;
	}

	public boolean isFirstParam() {
		return firstParam;
	}

	@Override
	public String toString() {
		return url.toString();
	}
}
