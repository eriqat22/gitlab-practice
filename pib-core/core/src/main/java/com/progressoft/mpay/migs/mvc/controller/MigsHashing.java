package com.progressoft.mpay.migs.mvc.controller;

import com.progressoft.mpay.migs.request.MigsRequest;

public abstract class MigsHashing {

	protected MigsContext context;

	public MigsHashing(MigsContext context) {
		this.context = context;
	}

	public abstract String generateHasing(MigsRequest migsRequest);
}
