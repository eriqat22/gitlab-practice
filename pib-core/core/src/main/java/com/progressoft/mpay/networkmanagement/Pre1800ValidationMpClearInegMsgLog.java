package com.progressoft.mpay.networkmanagement;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegRjctReason;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.MpClearIntegRjctReasonCodes;

public class Pre1800ValidationMpClearInegMsgLog implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(Pre1800ValidationMpClearInegMsgLog.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside Pre1800ValidationMpClearInegMsgLog -----------------------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		MPAY_MpClearIntegRjctReason rsn = technicalValidation(msg);
		if (rsn.getCode().equals(MpClearIntegRjctReasonCodes.VALID)) {
			transientVars.put(Result.VALIDATION_RESULT, Result.SUCCESSFUL);
		} else {
			transientVars.put(Result.VALIDATION_RESULT, Result.FAILED);
			rsn = LookupsLoader.getInstance().getMpClearIntegrationReasons(rsn.getCode());
			msg.setReason(rsn);
		}
	}

	private MPAY_MpClearIntegRjctReason technicalValidation(MPAY_MpClearIntegMsgLog msg) throws WorkflowException {
		MPAY_MpClearIntegRjctReason rsn = new MPAY_MpClearIntegRjctReason();
		rsn.setCode(MpClearIntegRjctReasonCodes.VALID);
		if (!msg.getSource().equalsIgnoreCase(IntegMessagesSource.SYSTEM)) {
			rsn.setCode(MpClearIntegRjctReasonCodes.INVALID_SOURCE);
		}
		if (Integer.parseInt(msg.getRefSender()) == LookupsLoader.getInstance().getPSP().getId()) {
			rsn.setCode(MpClearIntegRjctReasonCodes.INVALID_SENDER_ID);
		}

		return rsn;
	}

}
