package com.progressoft.mpay.mpclearinteg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entities.MPAY_TransactionConfig;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.mpclear.ClientInfo;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.mpclear.plugins.MPClearMessageProcessor;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingResult;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ValidationResult;

public class MPClearFinancialProcessor extends MPClearMessageProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MPClearFinancialProcessor.class);

	@Override
	public MPClearProcessingResult processMessage(MPClearProcessingContext context) {
		LOGGER.debug("Inside MPClearFinancialProcessor....");
		validateContext(context);

		String reasonCode;
		String reasonDescription;
		ValidationResult validationResult = loadOperation(context);
		if (!validationResult.isValid())
			return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());

		MessageProcessor processor = MessageProcessorHelper.getProcessor(context.getOperation());
		if (processor == null) {
			reasonCode = MPClearReasons.NOT_SUPPORTED_OPERATION;
			reasonDescription = LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(reasonCode).getDescription();
			return rejectMessage(context, reasonCode, reasonDescription);
		}

		IntegrationProcessingResult result = processor.processIntegration(new IntegrationProcessingContext(context));
		if (result == null) {
			reasonCode = ReasonCodes.INTERNAL_SYSTEM_ERROR;
			reasonDescription = LookupsLoader.getInstance().getReason(reasonCode).getDescription();
			return rejectMessage(context, reasonCode, reasonDescription);
		}

		return new MPClearProcessingResult(result);
	}

	private ValidationResult loadOperation(MPClearProcessingContext context) {
		LOGGER.debug("Inside LoadOperation....");
		if (context.getMessageType().getIsRequest())
			return loadRequestOperation(context);
		else
			return loadResponseOperation(context);
	}

	private MPClearProcessingResult rejectMessage(MPClearProcessingContext context, String reasonCode, String reasonDescription) {
		LOGGER.debug("Inside RejectMessage....");
		LOGGER.debug("reasonCode: " + reasonCode);
		LOGGER.debug("reasonDescription: " + reasonDescription);
		String originalMessageId = null;
		if (context.getIsoMessage() != null)
			originalMessageId = context.getIsoMessage().getField(MPClearCommonFields.MESSAGE_ID).getValue().toString();
		MPClearProcessingResult result = new MPClearProcessingResult();
		result.setMpClearMessage(context.getMessage());
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		result.setReasonCode(MPClearReasons.REJECTED_BY_RECEIVER_PSP);
		result.setReasonDescription(reason.getCode() + " - " + reason.getDescription());
		if (context.getMessageType() != null && context.getMessageType().getIsRequest()) {
			String processingCode = null;
			if (context.getIsoMessage().getField(MPClearCommonFields.PROCESSING_CODE) != null)
				processingCode = context.getIsoMessage().getField(MPClearCommonFields.PROCESSING_CODE).getValue().toString();
			result.setResponse(MPClearHelper.createMPClearResponse(new IntegrationProcessingContext(context), originalMessageId, Integer.parseInt(context.getMessageType().getResponseCode(), 16), reasonCode, reasonDescription, processingCode));
		}
		return result;
	}

	private ValidationResult loadResponseOperation(MPClearProcessingContext context) {
		LOGGER.debug("Inside LoadResponseOperation....");
		String originalMessageId = context.getIsoMessage().getField(116).getValue().toString();
		context.setOriginalMessage(DataProvider.instance().getMPClearMessage(originalMessageId));
		if (context.getOriginalMessage() == null) {
			LOGGER.debug("Original Message not found, messageId: " + originalMessageId);
			return new ValidationResult(MPClearReasons.ORIGINAL_MESSAGE_NOT_FOUND, LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(MPClearReasons.ORIGINAL_MESSAGE_NOT_FOUND).getDescription(), false);
		}
		if(context.getOriginalMessage().getResponseReceived()) {
			LOGGER.debug("Original Message not found, messageId: " + originalMessageId);
			return new ValidationResult(MPClearReasons.REQUEST_IS_NOT_PENDING, LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(MPClearReasons.REQUEST_IS_NOT_PENDING).getDescription(), false);
		}

		MPClearClientHelper.getClient(context.getSystemParameters().getMPClearProcessor()).updateResponseReceived(originalMessageId);
		context.setOperation(context.getOriginalMessage().getRefMessage().getRefOperation());
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private ValidationResult loadRequestOperation(MPClearProcessingContext context) {
		LOGGER.debug("Inside LoadRequestOperation....");
		int registrationIdLength = context.getSystemParameters().getMobileAccountSelectorLength() + context.getSystemParameters().getRoutingCodeLength();
		ClientInfo receiverInfo = getClientInfo(MPClearHelper.getEncodedString(context.getIsoMessage(), 63), registrationIdLength, context);
		if (receiverInfo == null) {
			LOGGER.debug("Receiver not found");
			return createResult(context.getLookupsLoader().getReason(ReasonCodes.RECEIVER_NOT_REGISTERED), false, false);
		}
		MPAY_MessageType messageType = context.getLookupsLoader().getMessageTypeByMPClearCode(context.getIsoMessage().getField(MPClearCommonFields.PAYMENT_TYPE).getValue().toString());
		if (messageType == null) {
			LOGGER.debug("Payment Type not supported");
			return createResult(context.getLookupsLoader().getReason(ReasonCodes.PAYMENT_TYPE_NOT_SUPPORTED), false, false);
		}

		String transactionTypeCode = TransactionTypeCodes.DIRECT_CREDIT;
		if (context.getIsoMessage().getField(MPClearCommonFields.PROCESSING_CODE) != null) {
			transactionTypeCode = getTransactionCodeType(context.getIsoMessage().getField(MPClearCommonFields.PROCESSING_CODE).getValue().toString());
			if (transactionTypeCode == null) {
				return createResult(context.getLookupsLoader().getReason(ReasonCodes.INVALID_PROCESSING_CODE), false, false);
			}
		}

		MPAY_TransactionConfig transactionConfig = context.getLookupsLoader().getTransactionConfig(context.getLookupsLoader().getMPClear().getClientType(), false, receiverInfo.getClientTypeId(), receiverInfo.isBanked(), messageType.getId(), transactionTypeCode);
		if (transactionConfig == null) {
			return createResult(context.getLookupsLoader().getReason(ReasonCodes.UNSUPPORTED_REQUEST), false, false);
		}

		context.setTransactionConfig(transactionConfig);
		context.setOperation(transactionConfig.getRefOperation());
		context.setTransactionNature(transactionTypeCode);
		return createResult(context.getLookupsLoader().getReason(ReasonCodes.VALID), true, true);
	}

	private String getTransactionCodeType(String processingCode) {
		LOGGER.debug("Inside GetTransactionCodeType....");
		LOGGER.debug("processingCode: " + processingCode);
		if (processingCode == null || processingCode.trim().isEmpty())
			return TransactionTypeCodes.DIRECT_CREDIT;
		try {
			Integer value = Integer.valueOf(processingCode);
			if (value >= ProcessingCodeRanges.DEBIT_START_VALUE && value < ProcessingCodeRanges.DEBIT_END_VALUE)
				return TransactionTypeCodes.DIRECT_DEBIT;
			else if (value >= ProcessingCodeRanges.CREDIT_START_VALUE && value < ProcessingCodeRanges.CREDIT_END_VALUE)
				return TransactionTypeCodes.DIRECT_CREDIT;
			else {
				return null;
			}
		} catch (Exception ex) {
			LOGGER.debug("getTransactionCodeType returning null", ex);
			return null;
		}
	}

	private ClientInfo getClientInfo(String fullAccount, int registrationIdLength, MPClearProcessingContext context) {
		LOGGER.debug("Inside GetClientInfo....");
		LOGGER.debug("fullAccount: " + fullAccount);
		LOGGER.debug("registrationIdLength: " + registrationIdLength);
		ClientInfo info = new ClientInfo();
		String clientInfo = fullAccount.substring(registrationIdLength);
		String clientType = clientInfo.substring(0, 1);
		clientInfo = clientInfo.substring(1);

		if (clientType.equalsIgnoreCase(ReceiverInfoType.MOBILE))
			return getMobileInfo(fullAccount, registrationIdLength, context, info, clientInfo, clientType);
		else if (clientType.equalsIgnoreCase(ReceiverInfoType.CORPORATE))
			return getServiceInfo(fullAccount, registrationIdLength, context, info, clientInfo, clientType);
		else if (clientType.equalsIgnoreCase(ReceiverInfoType.ALIAS))
			return getInfoByAlias(fullAccount, registrationIdLength, context, info, clientInfo, clientType);
		else
			throw new NotSupportedException("clientType: " + clientType);
		}

	private ClientInfo getInfoByAlias(String fullAccount, int registrationIdLength, MPClearProcessingContext context, ClientInfo info, String clientInfo, String clientType) {
		MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(clientInfo, clientType);
		if (mobile != null) {
			MPAY_MobileAccount mobileAccount = SystemHelper.getMobileAccount(context.getSystemParameters(), mobile, fullAccount.substring(0, registrationIdLength));
			if (mobileAccount == null)
				return null;
			info.setBanked(mobileAccount.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
			info.setClientTypeId(LookupsLoader.getInstance().getCustomerClientType().getId());
		} else {
			MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(clientInfo, clientType);
			if (service != null) {
				info.setBanked(service.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				info.setClientTypeId(service.getRefCorporate().getClientType().getId());
			} else {
				info.setBanked(false);
				info.setClientTypeId(context.getLookupsLoader().getMPClear().getClientType());
			}
		}
		return info;
	}

	private ClientInfo getServiceInfo(String fullAccount, int registrationIdLength, MPClearProcessingContext context, ClientInfo info, String clientInfo, String clientType) {
		MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(clientInfo, clientType);
		if (service == null)
			return null;

		MPAY_ServiceAccount serviceAccount = SystemHelper.getServiceAccount(context.getSystemParameters(), service, fullAccount.substring(0, registrationIdLength));
		if (serviceAccount == null)
			return null;
		info.setBanked(serviceAccount.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
		info.setClientTypeId(service.getRefCorporate().getClientType().getId());

		return info;
	}

	private ClientInfo getMobileInfo(String fullAccount, int registrationIdLength, MPClearProcessingContext context, ClientInfo info, String clientInfo, String clientType) {
		MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(clientInfo, clientType);
		if (mobile == null)
			return null;
		MPAY_MobileAccount mobileAccount = SystemHelper.getMobileAccount(context.getSystemParameters(), mobile, fullAccount.substring(0, registrationIdLength));
		if (mobileAccount == null)
			return null;
		info.setBanked(mobileAccount.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
		info.setClientTypeId(context.getLookupsLoader().getCustomerClientType().getId());

		return info;
	}

	private ValidationResult createResult(MPAY_Reason reason, boolean isValid, boolean isSystemReason) {
		LOGGER.debug("Inside CreateResult....");
		LOGGER.debug("isValid: " + isValid);
		LOGGER.debug("isSystemReason: " + isSystemReason);
		ValidationResult result = new ValidationResult();
		result.setValid(isValid);
		result.setReasonCode(reason.getCode());
		result.setReasonDescription(reason.getDescription());
		return result;
	}
}
