package com.progressoft.mpay.migs.mvc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MigsRequestHandler implements MigsHandler {

	private static final Logger logger = LoggerFactory.getLogger(MigsRequestHandler.class);

	@Override
	public String handle(MigsContext context) {
		boolean enableByMe = false;
		try {
			enableByMe = enableServicUserTenant(context);
			if (validToken(context))
				return processSuccessRequest(context);
			else
				return errorRequestURL(context);
		} catch (Exception e) {
			logger.error("Error while handle", e);
			return errorRequestURL(context);
		} finally {
			if(enableByMe)
				context.getServiceUserManager().disableServiceUser();
		}
	}

	private boolean enableServicUserTenant(MigsContext context) {
		boolean enableByMe;
		enableByMe =  context.getServiceUserManager().enableServiceUser(context.getServletRequest().getParameter(MIGSParameters.TENANT_ID));
		return enableByMe;
	}

	private String processSuccessRequest(MigsContext context) {
		updateMigsRequest(context);
		insertBankIntegrationMessage(context);
		return successRequestURL(context);
	}

	private void updateMigsRequest(MigsContext context) {
		context.setMigsRequest(MigsRequestObjectFactory.getMigsRequestObjectBuilder(context).buildRequestObject(context.getServletRequest()));
	}

	private void insertBankIntegrationMessage(MigsContext context) {
		new MigsBankIntegrationMessage().insert(context);
	}

	private String successRequestURL(MigsContext context) {
		return new MigsRequestURLBuilder().buildMigsURL(context);
	}

	private String errorRequestURL(MigsContext context) {
		return MigsHandlerFactory.getMigsHandler(MigsHandlerFactory.ERROR).handle(context);
	}

	private boolean validToken(MigsContext context) {
		return MigsTokenFactory.getTokenValidator(context).isValidToken(context.getServletRequest());
	}
}