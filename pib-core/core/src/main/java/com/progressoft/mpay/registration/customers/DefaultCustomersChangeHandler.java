package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;

public class DefaultCustomersChangeHandler extends CustomersChangeHandler {

	@Override
	protected void handleExtraCustomerFields(ChangeHandlerContext changeHandlerContext, MPAY_Customer customer) {
		//
	}

	@Override
	protected void handleExtraMobileFields(ChangeHandlerContext changeHandlerContext, MPAY_CustomerMobile mobile) {
		//
	}

	@Override
	protected void handleExtraAccountFields(ChangeHandlerContext changeHandlerContext, MPAY_MobileAccount account) {
		//
	}
}
