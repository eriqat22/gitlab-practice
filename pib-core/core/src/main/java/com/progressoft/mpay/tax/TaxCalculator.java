package com.progressoft.mpay.tax;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.shared.JfwCurrencyInfo;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_TaxScheme;
import com.progressoft.mpay.entities.MPAY_TaxSchemeDetail;
import com.progressoft.mpay.entities.MPAY_TaxSlice;
import com.progressoft.mpay.entity.TaxTypes;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;

public class TaxCalculator {
	private static final BigDecimal HUNDRED = new BigDecimal("100");

	private TaxCalculator() {

	}

	public static final void claculate(ProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");
		if (context.getAmount() == null)
			return;

		calculateTaxForSide(context, context.getSender());
		calculateTaxForSide(context, context.getReceiver());
	}

	public static BigDecimal calculateTax(IDataProvider dataProvider, BigDecimal amount, String messageTypeCode, MPAY_Profile profile, JFWCurrency currency) {
		try {
			if(amount.equals(BigDecimal.ZERO))
				return BigDecimal.ZERO;
			MPAY_TaxScheme taxScheme = checkTaxScheme(profile);
			if (taxScheme == null)
				return BigDecimal.ZERO;

			List<MPAY_TaxSlice> slices = getTaxSlices(taxScheme, messageTypeCode, amount);
			if (slices.isEmpty())
				return BigDecimal.ZERO;

			JfwCurrencyInfo currencyInfo = dataProvider.getCurrencyInfo(currency);
			BigDecimal tax = BigDecimal.ZERO;
			for (MPAY_TaxSlice slice : slices) {
				tax = tax.add(calculateTax(slice, amount, currencyInfo));
			}
			return tax;
		} catch (Exception ex) {
			throw new MPayGenericException("Error While calculateTax", ex);
		}
	}

	private static MPAY_TaxScheme checkTaxScheme(MPAY_Profile profile) {
		MPAY_TaxScheme taxScheme = profile.getTaxScheme();
		if (!taxScheme.getIsActive())
			return null;
		return taxScheme;
	}

	private static void calculateTaxForSide(ProcessingContext context, ProcessingContextSide side) {

		if (side.getCharge().compareTo(BigDecimal.ZERO) == 0)
			return;

		if (side.getProfile() == null || side.getProfile().getTaxScheme() == null || !side.getProfile().getTaxScheme().getIsActive())
			return;

		List<MPAY_TaxSlice> slices = getTaxSlices(side.getProfile().getTaxScheme(), context.getOperation().getMessageType().getId(), side.getCharge());
		if (slices.isEmpty())
			return;

		JfwCurrencyInfo currencyInfo = context.getDataProvider().getCurrencyInfo(context.getSystemParameters().getDefaultCurrency());
		BigDecimal tax = BigDecimal.ZERO;
		for (MPAY_TaxSlice slice : slices) {
			tax = tax.add(calculateTax(slice, side.getCharge(), currencyInfo));
		}
		side.setTax(tax);
	}

	private static BigDecimal calculateTax(MPAY_TaxSlice slice, BigDecimal charge, JfwCurrencyInfo currency) {
		if (slice.getTaxType().equalsIgnoreCase(TaxTypes.FIXED)) {
			BigDecimal taxAmount = slice.getTaxAmount();
			if (taxAmount.compareTo(charge) > 0)
				return charge;
			return taxAmount;
		} else {
			BigDecimal taxAmount = charge.multiply(slice.getTaxPercent()).divide(HUNDRED);
			taxAmount = taxAmount.compareTo(slice.getMinAmount()) < 0 ? slice.getMinAmount() : taxAmount;
			taxAmount = taxAmount.compareTo(slice.getMaxAmount()) > 0 ? slice.getMaxAmount() : taxAmount;
			return taxAmount.setScale(currency.getFractionDigits(), RoundingMode.HALF_DOWN);
		}
	}

	private static List<MPAY_TaxSlice> getTaxSlices(MPAY_TaxScheme scheme, String messageTypeCode, BigDecimal amount) {
		Optional<MPAY_TaxSchemeDetail> detail = scheme.getRefSchemeTaxSchemeDetails().stream().filter(d -> d.getMsgType().getCode().equals(messageTypeCode)).findFirst();
		if (!detail.isPresent())
			return new ArrayList<>();
		return getTaxSlices(detail.get(), amount);
	}

	private static List<MPAY_TaxSlice> getTaxSlices(MPAY_TaxScheme scheme, long messageTypeId, BigDecimal amount) {
		Optional<MPAY_TaxSchemeDetail> detail = scheme.getRefSchemeTaxSchemeDetails().stream().filter(d -> d.getMsgType().getId() == messageTypeId).findFirst();
		if (!detail.isPresent())
			return new ArrayList<>();
		return getTaxSlices(detail.get(), amount);
	}

	private static List<MPAY_TaxSlice> getTaxSlices(MPAY_TaxSchemeDetail detail, BigDecimal amount) {

		List<MPAY_TaxSlice> fliteredSlices = new ArrayList<>();
		List<MPAY_TaxSlice> resultSlices = new ArrayList<>();
		List<MPAY_TaxSlice> allSlices = detail.getRefTaxDetailsTaxSlices();
		if (allSlices == null)
			return resultSlices;
		fliteredSlices.addAll(allSlices.stream().filter(mpayChargesSlicePredicate -> mpayChargesSlicePredicate.getTxAmountLimit().compareTo(amount) >= 0
				&& (mpayChargesSlicePredicate.getDeletedFlag() == null || !mpayChargesSlicePredicate.getDeletedFlag())).collect(Collectors.toList()));
		if (fliteredSlices.isEmpty())
			return fliteredSlices;
		TaxSliceAmountComparer comparor = new TaxSliceAmountComparer();
		Collections.sort(fliteredSlices, comparor);
		MPAY_TaxSlice minSlice = fliteredSlices.get(0);
		resultSlices.addAll(fliteredSlices.stream().filter(slice -> slice.getTxAmountLimit().compareTo(minSlice.getTxAmountLimit()) == 0).collect(Collectors.toList()));
		return resultSlices;
	}
}
