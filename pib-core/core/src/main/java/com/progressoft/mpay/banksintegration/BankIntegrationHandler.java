package com.progressoft.mpay.banksintegration;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_BankIntegMessage;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.BankType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.BankReversalException;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.TransactionDirection;

public class BankIntegrationHandler {
    private static final String FAILED_TO_CREATE_BANK_INTEGRATION_MESSAGE_FOR_BANK = "Failed to create Bank Integration Message for bank: ";
    private static final String ERROR_WHILE_CREATING_PROCESSOR_FOR_BANK = "Error while creating processor for bank: ";

    private BankIntegrationHandler() {

    }

    public static BankIntegrationResult handleIntegrations(ProcessingContext context) {
    	if(context == null)
    		throw new NullArgumentException("context");
        BankIntegrationResult result;
        if (context.getDirection() == TransactionDirection.ONUS) {
            result = handleTransfer(context);
        } else if (context.getDirection() == TransactionDirection.INWARD) {
            result = handleDeposit(context);
        } else if (context.getDirection() == TransactionDirection.OUTWARD) {
            result = handleWithdrawal(context);
        } else
            throw new InvalidArgumentException("context.getDirection() == " + context.getDirection());
        return result;
    }

    private static BankIntegrationResult handleWithdrawal(ProcessingContext context) {
        BankIntegrationResult result = new BankIntegrationResult();
        if (!context.getSender().isBanked()) {
            result.setReasonCode(ReasonCodes.VALID);
            result.setStatus(BankIntegrationStatus.ACCEPTED);
            return result;
        }
        BankIntegrationProcessor processor = BankIntegrationHelper.getProcessor(context.getSender().getBank());
        if (processor == null) {
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription(ERROR_WHILE_CREATING_PROCESSOR_FOR_BANK + context.getSender().getBank().getName());
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setReasonDescription(result.getStatusDescription());
            return result;
        }
        BankIntegrationContext bankIntegrationContext = new BankIntegrationContext(context.getSender().getBank(), context, BankIntegrationMethod.ACCOUNT_WITHDRAWAL, context.getTransaction());
        MPAY_BankIntegMessage integMessage = createIntegrationMessage(context.getDataProvider(), processor, bankIntegrationContext, false);
        if (integMessage == null) {
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription(FAILED_TO_CREATE_BANK_INTEGRATION_MESSAGE_FOR_BANK + context.getSender().getBank().getName());
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setReasonDescription(result.getStatusDescription());
            return result;
        }

        result = processor.processMessage(bankIntegrationContext);
        context.getDataProvider().updateBankIntegMessage(result.getIntegMessage());
        if (result.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
            MPAY_BankIntegMessage chargeMessage = processor.createChargeMessage(bankIntegrationContext);
            if (chargeMessage == null)
                return result;
            BankIntegrationResult chargesResult = processor.processMessage(bankIntegrationContext);
            if (chargesResult.getStatus().equals(BankIntegrationStatus.ACCEPTED))
                handleReversal(context.getSender().getBank(), context, result);
        }
        return result;
    }

    private static BankIntegrationResult handleDeposit(ProcessingContext context) {
        BankIntegrationResult result = new BankIntegrationResult();
        if (!context.getReceiver().isBanked()) {
            result.setReasonCode(ReasonCodes.VALID);
            result.setStatus(BankIntegrationStatus.ACCEPTED);
            return result;
        }
        BankIntegrationProcessor processor = BankIntegrationHelper.getProcessor(context.getReceiver().getBank());
        if (processor == null) {
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription(ERROR_WHILE_CREATING_PROCESSOR_FOR_BANK + context.getReceiver().getBank().getName());
            return result;
        }
        BankIntegrationContext bankIntegrationContext = new BankIntegrationContext(context.getReceiver().getBank(), context, BankIntegrationMethod.ACCOUNT_DEPOSIT, context.getTransaction());
        MPAY_BankIntegMessage integMessage = createIntegrationMessage(context.getDataProvider(), processor, bankIntegrationContext, false);
        if (integMessage == null) {
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription(FAILED_TO_CREATE_BANK_INTEGRATION_MESSAGE_FOR_BANK + context.getReceiver().getBank().getName());
            return result;
        }
        bankIntegrationContext.setIntegMessage(integMessage);
        result = processor.processMessage(bankIntegrationContext);
        if (result.getIntegMessage() != null)
            context.getDataProvider().updateBankIntegMessage(result.getIntegMessage());
        return result;
    }

    private static BankIntegrationResult handleTransfer(ProcessingContext context) {
        BankIntegrationResult result = new BankIntegrationResult();
        boolean isOuns = false;
        if (!context.getSender().isBanked() && !context.getReceiver().isBanked()) {
            result.setStatus(BankIntegrationStatus.ACCEPTED);
            result.setReasonCode(ReasonCodes.VALID);
            return result;
        }
        if (context.getSender().isBanked() && context.getReceiver().isBanked() && context.getSender().getBank().getId() == context.getReceiver().getBank().getId())
            isOuns = true;
        if (isOuns)
            return handleInterBank(context);

        if (!context.getReceiver().isBanked()) {
            return handleWithdrawal(context);
        } else if (!context.getSender().isBanked()) {
            return handleDeposit(context);
        } else
            return handleCrossBank(context);
    }

    public static BankIntegrationResult handleBalanceInquiry(ProcessingContext context) {
        BankIntegrationResult result = new BankIntegrationResult();
        if (!context.getSender().isBanked()) {
            result.setStatus(BankIntegrationStatus.ACCEPTED);
            result.setReasonCode(ReasonCodes.VALID);
            return result;
        }
        return handleBalanceInquiryIntegration(context);
    }

    private static BankIntegrationResult handleBalanceInquiryIntegration(ProcessingContext context) {
    	BankIntegrationResult result = new BankIntegrationResult();
        MPAY_Bank bank = context.getSender().getBank();
        BankIntegrationProcessor processor = BankIntegrationHelper.getProcessor(bank);
        if (processor == null) {
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription(ERROR_WHILE_CREATING_PROCESSOR_FOR_BANK + context.getReceiver().getBank().getName());
            return result;
        }
        BankIntegrationContext bankIntegrationContext = new BankIntegrationContext(bank, context, BankIntegrationMethod.BALANCE_INQUIRY, null);
        MPAY_BankIntegMessage integMessage = createBalanceInquiryIntegrationMessage(context.getDataProvider(), processor, bankIntegrationContext, false);
        if (integMessage == null) {
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription(FAILED_TO_CREATE_BANK_INTEGRATION_MESSAGE_FOR_BANK + context.getReceiver().getBank().getName());
            return result;
        }
        result = processor.processBalanceInquiryMessage(bankIntegrationContext);
        if (result.getIntegMessage() != null)
            context.getDataProvider().updateBankIntegMessage(result.getIntegMessage());
        return result;
    }

    private static BankIntegrationResult handleInterBank(ProcessingContext context) {
        BankIntegrationResult result = new BankIntegrationResult();
        MPAY_Bank bank = context.getSender().getBank();
        BankIntegrationProcessor processor = BankIntegrationHelper.getProcessor(bank);
        if (processor == null) {
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription(ERROR_WHILE_CREATING_PROCESSOR_FOR_BANK + context.getReceiver().getBank().getName());
            return result;
        }
        BankIntegrationContext bankIntegrationContext = new BankIntegrationContext(bank, context, BankIntegrationMethod.ACCOUNT_TRANSFER, context.getTransaction());
        MPAY_BankIntegMessage integMessage = createIntegrationMessage(context.getDataProvider(), processor, bankIntegrationContext, false);
        if (integMessage == null) {
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription(FAILED_TO_CREATE_BANK_INTEGRATION_MESSAGE_FOR_BANK + context.getReceiver().getBank().getName());
            return result;
        }
        result = processor.processMessage(bankIntegrationContext);
        if (result.getIntegMessage() != null)
            context.getDataProvider().updateBankIntegMessage(result.getIntegMessage());
        if (result.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
            MPAY_BankIntegMessage chargeMessage = processor.createChargeMessage(bankIntegrationContext);
            if (chargeMessage == null)
                return result;
            BankIntegrationResult chargesResult = processor.processMessage(bankIntegrationContext);
            if (!chargesResult.getStatus().equals(BankIntegrationStatus.ACCEPTED))
                handleReversal(context.getSender().getBank(), context, result);
        }

        return result;
    }

    private static BankIntegrationResult handleCrossBank(ProcessingContext context) {
        BankIntegrationResult withrdawalResult = handleWithdrawal(context);

        if (!withrdawalResult.getStatus().equals(BankIntegrationStatus.ACCEPTED))
            return withrdawalResult;

        BankIntegrationResult depositResult = handleDeposit(context);
        if (!depositResult.getStatus().equals(BankIntegrationStatus.ACCEPTED))
            handleReversal(context.getSender().getBank(), context, withrdawalResult);
        return depositResult;
    }

    private static void handleReversal(MPAY_Bank bank, ProcessingContext context, BankIntegrationResult withrdawalResult) {
        if (bank.getSettlementParticipant().equals(BankType.SETTLEMENT))
            return;
        BankIntegrationProcessor processor = BankIntegrationHelper.getProcessor(bank);
        BankIntegrationContext bankIntegrationContext = new BankIntegrationContext(bank, context, BankIntegrationMethod.REVERSAL, context.getTransaction());
        bankIntegrationContext.setOriginalIntegMessage(withrdawalResult.getIntegMessage());
        MPAY_BankIntegMessage reversalMessage = processor.createReversalMessage(bankIntegrationContext);
        if (reversalMessage == null) {
            return;
        }
        DataProvider.instance().persistBankIntegMessage(reversalMessage);
        bankIntegrationContext.setIntegMessage(reversalMessage);
        BankIntegrationResult reversalResult = processor.reverseMessage(bankIntegrationContext);
        DataProvider.instance().updateBankIntegMessage(reversalMessage);
        if (reversalResult.getIntegMessage().getRefStatus().getCode().equals(BankIntegrationStatus.ACCEPTED)) {
            bankIntegrationContext.getOriginalIntegMessage().setIsReversed(true);
            DataProvider.instance().updateBankIntegMessage(bankIntegrationContext.getOriginalIntegMessage());
        }
    }

    private static MPAY_BankIntegMessage createIntegrationMessage(IDataProvider dataProvider, BankIntegrationProcessor processor, BankIntegrationContext context, boolean isReversal) {
        MPAY_BankIntegMessage integMessage;
        if (isReversal)
            integMessage = processor.createReversalMessage(context);
        else
            integMessage = processor.createMessage(context);
        if (integMessage == null)
            return null;
        if (integMessage.getRequestID() == null)
            return integMessage;
        if (context.getTransaction().getRefTransactionBankIntegMessages() == null)
            context.getTransaction().setRefTransactionBankIntegMessages(new ArrayList<>());
        context.getTransaction().getRefTransactionBankIntegMessages().add(integMessage);
        dataProvider.persistBankIntegMessage(integMessage);
        context.setIntegMessage(integMessage);
        return integMessage;
    }

    private static MPAY_BankIntegMessage createBalanceInquiryIntegrationMessage(IDataProvider dataProvider, BankIntegrationProcessor processor, BankIntegrationContext context, boolean isReversal) {
        MPAY_BankIntegMessage integMessage;
        integMessage = processor.createBalanceInquiryMessage(context);
        if (integMessage == null)
            return null;
        if (integMessage.getRequestID() == null)
            return integMessage;
        dataProvider.persistBankIntegMessage(integMessage);
        context.setIntegMessage(integMessage);
        return integMessage;
    }

    public static BankIntegrationResult reverse(ProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");
        BankIntegrationResult result = new BankIntegrationResult();
        MPAY_Transaction transaction = context.getTransaction();
        if (transaction == null)
            return null;

        if (processEmptyTransactions(result, transaction))
            return result;

        boolean hasFailure = false;

        List<MPAY_BankIntegMessage> integrationMessages = transaction.getRefTransactionBankIntegMessages();

        for (MPAY_BankIntegMessage integrationMessage : integrationMessages) {
            if (integrationMessage.getRefStatus().getCode().equals(BankIntegrationStatus.ACCEPTED) && !integrationMessage.getIsReversed() && !integrationMessage.getIsReversal()) {
                result = reverseMessage(context, integrationMessage);
                if (result == null)
                    throw new BankReversalException("Bank Reversal Failed");
                if (result.getStatus().equals(BankIntegrationStatus.ACCEPTED))
                    integrationMessage.setIsReversed(true);
                else
                    hasFailure = true;
            }
        }
        processResult(result, hasFailure);
        return result;
    }

    private static void processResult(BankIntegrationResult result, boolean hasFailure) {
        if (hasFailure) {
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription("Reversal incomplete");
        } else {
            result.setStatus(BankIntegrationStatus.ACCEPTED);
        }
    }

    private static boolean processEmptyTransactions(BankIntegrationResult result, MPAY_Transaction transaction) {
        if (transaction.getRefTransactionBankIntegMessages() == null || transaction.getRefTransactionBankIntegMessages().isEmpty()) {
            result.setReasonCode(ReasonCodes.VALID);
            result.setStatus(BankIntegrationStatus.ACCEPTED);
            return true;
        }
        return false;
    }

    private static BankIntegrationResult reverseMessage(ProcessingContext context, MPAY_BankIntegMessage integrationMessage) {

        MPAY_Bank bank = integrationMessage.getRefBank();
        BankIntegrationProcessor processor = BankIntegrationHelper.getProcessor(bank);
        BankIntegrationContext bankIntegrationContext = new BankIntegrationContext(bank, context, BankIntegrationMethod.REVERSAL, context.getTransaction());
        bankIntegrationContext.setOriginalIntegMessage(integrationMessage);
        MPAY_BankIntegMessage reversalMessage = createIntegrationMessage(context.getDataProvider(), processor, bankIntegrationContext, true);
        if (reversalMessage == null) {
            BankIntegrationResult result = new BankIntegrationResult();
            result.setReasonCode(ReasonCodes.BANK_INTEGRATION_FAILURE);
            result.setStatus(BankIntegrationStatus.FAILED);
            result.setStatusDescription("Failed to create bank integration message");
            return result;
        }
        return processor.reverseMessage(bankIntegrationContext);
    }
}
