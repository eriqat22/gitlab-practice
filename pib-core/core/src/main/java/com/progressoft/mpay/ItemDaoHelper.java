package com.progressoft.mpay;

import java.util.List;
import java.util.stream.Collectors;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.bussinessobject.core.JfwDraft;
import com.progressoft.jfw.model.dao.item.ItemDao;

public class ItemDaoHelper {
    private ItemDaoHelper() {
    }

    public static <T> T getOneItem(ItemDao itemDao, Class<T> entity, String whereClause) {

        List<T> list = itemDao.getItems(entity, null, null, whereClause, null);
        if (list.isEmpty())
            return null;

        return list.get(0);
    }

    static <T extends JFWEntity> List<T> filterUndeleted(List<T> list) {
        return list.stream().filter(i -> !SystemHelper.toPremitiveBoolean(i.getDeletedFlag())).map(i -> (T) i).collect(Collectors.toList());
    }

    static JFWEntity getUndeleted(JFWEntity entity) {
        if (entity == null)
            return null;
        if (SystemHelper.toPremitiveBoolean(entity.getDeletedFlag()))
            return null;
        else
            return entity;
    }

    public static JfwDraft getDraftObject(JFWEntity reference, ItemDao itemDao) throws WorkflowException {
        if (reference == null)
            return null;
        if (null == reference.getJfwDraft())
            throw new WorkflowException("Object has null draft");
        return itemDao.getItem(JfwDraft.class, reference.getJfwDraft().getId());
    }
}
