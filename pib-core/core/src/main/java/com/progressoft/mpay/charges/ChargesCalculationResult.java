package com.progressoft.mpay.charges;

import java.math.BigDecimal;

public class ChargesCalculationResult {
	private BigDecimal senderChargesAmount;
	private BigDecimal receiverChargesAmount;

	public BigDecimal getSenderChargesAmount() {
		return senderChargesAmount;
	}
	public void setSenderChargesAmount(BigDecimal senderChargesAmount) {
		this.senderChargesAmount = senderChargesAmount;
	}
	public BigDecimal getReceiverChargesAmount() {
		return receiverChargesAmount;
	}
	public void setReceiverChargesAmount(BigDecimal receiverChargesAmount) {
		this.receiverChargesAmount = receiverChargesAmount;
	}


}
