package com.progressoft.mpay.registration.corporates;


import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_Corporate;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

import static com.progressoft.jfw.labels.Labels.description;
import static java.util.Objects.isNull;

@Component
public class AllowedCustomerProfileValidator implements Validator {
    @Override
    public void validate(Map map, Map map1, PropertySet propertySet) throws WorkflowException {
        InvalidInputException iie = new InvalidInputException();
        MPAY_Corporate corporate = (MPAY_Corporate) map.get(WorkflowService.WF_ARG_BEAN);
        if ("50".equals(corporate.getClientType().getCode()) && isNull(corporate.getAllowedProfile())) {
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, description(MPayErrorMessages.ALLOWED_PROFILE_IS_REQUIRED));
        }
        if (iie.getErrors().size() != 0) {
            throw iie;
        }
    }
}
