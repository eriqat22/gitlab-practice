package com.progressoft.mpay.messages;

import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.mpay.*;
import com.progressoft.mpay.common.RequestProcessor;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.notifications.INotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MPayRequestProcessor extends RequestProcessor {
    private static final String MERGE_PROCESSING_RESULT = "MergeProcessingResult";
    private static final Logger LOGGER = LoggerFactory.getLogger(MPayMessageHandler.class);
    private static final String KEY = "key";

    public MPayRequestProcessor(IDataProvider dataProvider, ISystemParameters systemParameters,
                                ILookupsLoader lookupsLoader, ICryptographer cryptographer, INotificationHelper notificationHelper,
                                Communicator communicator) {
        super(dataProvider, systemParameters, lookupsLoader, cryptographer, notificationHelper, communicator);
    }

    public MPayResponseEnvelope processMessage(String body, String token, IServiceUserManager serviceUserManager) {
        boolean enabledByMe = false;
        MPayRequest request = null;
        MessageProcessingContext context = null;
        MPayResponseEnvelope responseEnvelope;
        try {
            LOGGER.debug("Request Received: ", body);
            LOGGER.info("token:. ", token);
            request = MPayRequest.fromJson(body);
            if (request == null)
                return generateResponseEnvelop(body, token, ReasonCodes.FAILED_TO_PARSE_MESSAGE, null);
            enabledByMe = serviceUserManager.enableServiceUser(request.getTenant());

            MPAY_EndPointOperation operation = lookupsLoader.getEndPointOperation(request.getOperation());
            if (operation == null || operation.getIsSystem())
                return generateResponseEnvelop(body, token, ReasonCodes.UNSUPPORTED_REQUEST, request);
            if (!operation.getIsActive())
                return generateResponseEnvelop(body, token, ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE, request);
            MessageProcessor processor = MessageProcessorHelper.getProcessor(operation);
            if (processor == null)
                return generateResponseEnvelop(body, token, ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE, request);


            responseEnvelope = validateSecurityRequirements(body, token, request, processor);
            if (responseEnvelope != null)
                return responseEnvelope;

            context = new MessageProcessingContext(dataProvider, lookupsLoader, systemParameters, cryptographer,
                    notificationHelper);
            context.setOperation(operation);
            context.setRequest(request);
            context.setOriginalRequest(body);
            context.setToken(token);
            context.setLanguage(getLanguage(request));
            MPAY_BulkPayment bulkPayment = dataProvider.getBulkPayment(systemParameters.getBulkPaymentDefaultId());
            if (bulkPayment == null)
                throw new InvalidOperationException("Default Bulk Payment ID Not Found");
            MPAY_MPayMessage message = MessageProcessorHelper.createMessage(serviceUserManager, context,
                    ProcessingStatusCodes.PENDING, null, null, bulkPayment);
            responseEnvelope = tryPersistMessage(body, token, message, request);
            if (responseEnvelope != null)
                return responseEnvelope;
            context.setMessage(message);
            return process(body, token, request, context, processor);
        } catch (Exception ex) {
            LOGGER.error(MERGE_PROCESSING_RESULT, ex);
            if (context != null) {
                return rejectMessage(context, request, body, token, ex.getMessage());
            } else
                return generateResponseEnvelop(body, token, ReasonCodes.INTERNAL_SYSTEM_ERROR, request);
        } finally {
            if (enabledByMe) {
                serviceUserManager.disableServiceUser();
            }
        }
    }

    private MPayResponseEnvelope validateSecurityRequirements(String body, String token, MPayRequest request, MessageProcessor processor) {
        String checkSumValue = null;
        if (systemParameters.getEnableAppsChecksums()) {
            if (request.getChecksum() == null || request.getChecksum().trim().length() == 0)
                return generateResponseEnvelop(body, token, ReasonCodes.INVALID_REQUEST, request);
            MPAY_AppsChecksum checksum = lookupsLoader.getAppChecksum(request.getChecksum());
            if (checksum == null || !checksum.getIsActive())
                return generateResponseEnvelop(body, token, ReasonCodes.INVALID_REQUEST, request);
            checkSumValue = checksum.getChecksum();
        }

        if (StringUtils.isNotBlank(request.getValue(KEY)) && !processor.isNeedToSessionId()) {
            if (validatePublicKey(token, request, checkSumValue))
                return generateResponseEnvelop(body, token, ReasonCodes.INVALID_TOKEN, request);
        } else if (request.getSenderType().equals(ReceiverInfoType.MOBILE)) {
            MPAY_CustomerDevice customerDevice = DataProvider.instance().getCustomerDevice(request.getDeviceId());
            if (customerDevice != null) {
                boolean isSameSign = cryptographer.verifySign(token, HashingAlgorithm.hash(systemParameters, request.toValues(checkSumValue)), customerDevice.getPublicKey());
                if (!isSameSign)
                    return generateResponseEnvelop(body, token, ReasonCodes.INVALID_TOKEN, request);
            }

        } else if (request.getSenderType().equals(ReceiverInfoType.CORPORATE)) {
            MPAY_CorporateDevice corporateDevice = DataProvider.instance().getCorporateDevice(request.getDeviceId());
            if (corporateDevice != null) {
                boolean isSameSign = cryptographer.verifySign(token, HashingAlgorithm.hash(systemParameters, request.toValues(checkSumValue)), corporateDevice.getPublicKey());
                if (!isSameSign)
                    return generateResponseEnvelop(body, token, ReasonCodes.INVALID_TOKEN, request);
            }
        } else
            return generateResponseEnvelop(body, token, ReasonCodes.INVALID_TOKEN, request);
    	
        return null;
    }

    private boolean validatePublicKey(String token, MPayRequest request, String checkSumValue) {
        boolean isSameSign = cryptographer.verifySign(token, HashingAlgorithm.hash(systemParameters, request.toValues(checkSumValue)), request.getValue(KEY));
        if (!isSameSign)
            return true;
        return false;
    }

    private MPayResponseEnvelope generateResponseEnvelop(String body, String token, String reasonCode,
                                                         MPayRequest request) {
        MPayResponseEnvelope responseEnvelope;
        responseEnvelope = new MPayResponseEnvelope();
        responseEnvelope.setResponse(
                rejectMessage(null, body, token, lookupsLoader.getReason(reasonCode), getLanguage(request)));
        responseEnvelope
                .setToken(cryptographer.createJsonToken(systemParameters, responseEnvelope.getResponse().toValues()));
        return responseEnvelope;
    }

    private MPayResponseEnvelope process(String body, String token, MPayRequest request,
                                         MessageProcessingContext context, MessageProcessor processor) {
        MPayResponseEnvelope responseEnvelope;
        MessageProcessingResult result;
        try {
            result = processor.processMessage(context);
            if (result.getResponse() == null) {
                responseEnvelope = new MPayResponseEnvelope();
                responseEnvelope.setResponse(generateResponse(result.getMessage().getReference(),
                        result.getMessage().getProcessingStatus().getCode(), result.getMessage().getReason(),
                        context.getLanguage()));
                responseEnvelope.setToken(
                        cryptographer.createJsonToken(systemParameters, responseEnvelope.getResponse().toValues()));
                result.setResponse(responseEnvelope.toString());
            } else {
                responseEnvelope = new MPayResponseEnvelope();
                responseEnvelope.setResponse(MPayResponse.fromJson(result.getResponse()));
                responseEnvelope.setToken(
                        cryptographer.createJsonToken(systemParameters, responseEnvelope.getResponse().toValues()));
                result.setResponse(responseEnvelope.toString());
            }
            if (result.getMessage().getResponseContent() == null)
                result.getMessage().setResponseContent(result.getResponse());
            dataProvider.persistProcessingResult(result);
            if (result.getMpClearMessage() != null && !result.isHandleMPClearOffline())
                MPClearHelper.sendToActiveMQ(result.getMpClearMessage(), communicator);
            notificationHelper.sendNotifications(result);
            return MPayResponseEnvelope.fromJson(result.getResponse());
        } catch (Exception ex) {
            LOGGER.error(MERGE_PROCESSING_RESULT, ex);
            return rejectMessage(context, request, body, token, ex.toString());
        }
    }

    private MPayResponseEnvelope tryPersistMessage(String body, String token, MPAY_MPayMessage message,
                                                   MPayRequest request) {
        try {
            dataProvider.persistMPayMessage(message);
            return null;
        } catch (Exception e) {
            LOGGER.debug(MERGE_PROCESSING_RESULT, e);
            return generateResponseEnvelop(body, token, ReasonCodes.MESSAGE_IS_DUPLICATED, request);
        }
    }

    private MPayResponseEnvelope rejectMessage(MessageProcessingContext context, MPayRequest request, String body,
                                               String token, String reasonDescription) {
        MPAY_Reason reason = lookupsLoader.getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
        String reference = null;
        if (context.getMessage() != null) {
            reference = context.getMessage().getReference();
            context.getMessage().setReason(reason);
            context.getMessage().setProcessingStatus(lookupsLoader.getProcessingStatus(ProcessingStatusCodes.FAILED));
            context.getMessage().setReasonDesc(reasonDescription);
            if (context.getTransaction() != null) {
                context.getTransaction().setReason(reason);
                context.getTransaction().setProcessingStatus(context.getMessage().getProcessingStatus());
                context.getTransaction().setReasonDesc(context.getMessage().getReasonDesc());
            }
            dataProvider.mergeProcessingContext(context);
        }
        MPayResponse response = rejectMessage(reference, body, token, reason, getLanguage(request));
        MPayResponseEnvelope responseEnvelope = new MPayResponseEnvelope();
        responseEnvelope.setResponse(response);
        responseEnvelope
                .setToken(cryptographer.createJsonToken(systemParameters, responseEnvelope.getResponse().toValues()));
        return responseEnvelope;
    }

    private MPayResponse rejectMessage(String reference, String requset, String token, MPAY_Reason reason,
                                       MPAY_Language language) {
        LOGGER.debug("Invalid Message Received.");
        LOGGER.debug("Receiving Date: " + SystemHelper.getSystemTimestamp().toString());
        LOGGER.debug("Request: " + requset);
        LOGGER.debug("token: " + token);
        MPAY_Language actualLanguage = language;
        MPayResponse response = new MPayResponse();
        long ref = 0;
        if (reference != null)
            ref = Long.parseLong(reference);
        response.setRef(ref);
        response.setErrorCd(reason.getCode());
        response.setStatusCode(ProcessingStatusCodes.REJECTED);
        if (actualLanguage == null)
            actualLanguage = systemParameters.getSystemLanguage();
        String description;
        MPAY_Reasons_NLS nls = lookupsLoader.getReasonNLS(reason.getId(), actualLanguage.getCode());
        if (nls == null)
            description = lookupsLoader.getReasonNLS(reason.getId(), systemParameters.getSystemLanguage().getCode())
                    .getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        return response;
    }

    private MPayResponse generateResponse(String reference, String processingCode, MPAY_Reason reason,
                                          MPAY_Language language) {
        MPayResponse response = new MPayResponse();
        response.setErrorCd(reason.getCode());
        MPAY_Language actualLanguage = language;
        if (actualLanguage == null)
            actualLanguage = systemParameters.getSystemLanguage();
        MPAY_Reasons_NLS nls = lookupsLoader.getReasonNLS(reason.getId(), actualLanguage.getCode());
        String description;
        if (nls == null)
            description = lookupsLoader.getReasonNLS(reason.getId(), systemParameters.getSystemLanguage().getCode())
                    .getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setRef(Long.parseLong(reference));
        response.setStatusCode(processingCode);
        return response;
    }

    private MPAY_Language getLanguage(MPayRequest request) {
        LOGGER.debug("Inside getLanguage ....");
        if (request == null)
            return systemParameters.getSystemLanguage();
        return lookupsLoader.getLanguageByCode(LanguageMapper.getInstance().getLanguageLocaleCode(String.valueOf(request.getLang())));
    }
}
