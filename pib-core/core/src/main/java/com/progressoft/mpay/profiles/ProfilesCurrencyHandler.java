package com.progressoft.mpay.profiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_Profile;

public class ProfilesCurrencyHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(ProfilesCurrencyHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("***inside ProfilesCurrencyHandler: " + changeHandlerContext.getSource());

		MPAY_Profile profile = (MPAY_Profile) changeHandlerContext.getEntity();
		profile.setCurrency(SystemParameters.getInstance().getDefaultCurrency());
		try {

			if (profile.getCurrency() == null) {
				changeHandlerContext.getFieldsAttributes().get("chargesScheme").setEnabled(false);
				changeHandlerContext.getFieldsAttributes().get("limitsScheme").setEnabled(false);
			} else {
				changeHandlerContext.getFieldsAttributes().get("chargesScheme").setEnabled(true);
				changeHandlerContext.getFieldsAttributes().get("limitsScheme").setEnabled(true);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new BusinessException("An error occured in the ProfilesCurrencyHandler", ex);
		}
	}

}
