package com.progressoft.mpay.exceptions;

public class MPayGenericException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MPayGenericException(String message, Exception e) {
		super(message, e);
	}
}
