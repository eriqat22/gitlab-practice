package com.progressoft.mpay.valueprovider;

import com.progressoft.jfw.model.bussinessobject.security.Tenant;
import com.progressoft.jfw.model.service.utils.AppContext;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TenantsCustomValueProvider {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public Map<String, String> getTenants() {
		Map<String, String> tenants = new HashMap<>();
		Query query = em.createQuery("FROM Tenant");

		List<Tenant> dbTenants = query.getResultList();
		for (Tenant tenant : dbTenants) {
			if (!tenant.getName().equals(AppContext.getCurrentTenant()) && !"SYSTEM".equals(tenant.getName())) {
				tenants.put(String.valueOf(tenant.getId()), tenant.getName());
			}
		}

		return tenants;
	}

}
