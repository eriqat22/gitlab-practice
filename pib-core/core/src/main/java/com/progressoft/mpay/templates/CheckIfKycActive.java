package com.progressoft.mpay.templates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.WF_Generic_Edit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

public class CheckIfKycActive implements Condition {

    private static final Logger logger = LoggerFactory.getLogger(CheckIfKycActive.class);

    @SuppressWarnings("rawtypes")
    @Override
    public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside passesCondition ------------");
        JFWEntity kycEntity = (JFWEntity) transientVar.get(WorkflowService.WF_ARG_BEAN);
        return Objects.nonNull(kycEntity) && kycEntity.getStatusId().getCode().equals(WF_Generic_Edit.STEP_ActiveStep);
    }
}