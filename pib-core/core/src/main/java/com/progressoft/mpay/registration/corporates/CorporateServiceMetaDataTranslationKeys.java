package com.progressoft.mpay.registration.corporates;

import com.progressoft.mpay.registration.customers.CommonMetaDataTranslationKeys;

public class CorporateServiceMetaDataTranslationKeys extends CommonMetaDataTranslationKeys {
	public static final String NOTIFICATION_RECEIVER = "ChangesNotificationReceiver";
	public static final String SERVICE_CATEGORY = "ChangesServiceCategory";
	public static final String SERVICE_TYPE = "ChangesServiceType";
	public static final String NOTIFICATION_CHANNEL = "ChangesNotificationChannel";
}
