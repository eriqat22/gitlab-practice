package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_BanksUser;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;

public class UserValidator {

	private static final Logger logger = LoggerFactory.getLogger(UserValidator.class);

	private UserValidator() {

	}

	public static ValidationResult validate(MPAY_BanksUser user) {
		logger.debug("Inside Validate ...");
		if (user == null)
			return new ValidationResult(ReasonCodes.INCORRECT_USERNAME_OR_PASSWORD, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
