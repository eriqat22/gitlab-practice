package com.progressoft.mpay.registration.customers;

public class MobileAccountWorkflowServiceActions {
	public static final String APPROVE = "SVC_Approve";
	public static final String REJECT = "SVC_Reject";
	public static final String DELETE = "SVC_Delete";
	public static final String DELETE_APPROVAL = "SVC_DeleteApproval";
	public static final String SEND_APPROVAL = "SVC_SendApproval";
	public static final String DEFAULT_REJECT = "SVC_DefaultReject";
	public static final String SUSPEND = "SVC_Suspend";
	public static final String REACTIVATE = "SVC_Reactivate";
	public static final String RETURN_APPROVED = "SVC_ReturnApproved";

	private MobileAccountWorkflowServiceActions() {

	}
}
