package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.mpclear.MPClearClientHelper;

public class PostSetMobileAccountSwitchDefaultApproval implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostSetMobileAccountSwitchDefaultApproval.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute--------------------------");
        MPAY_MobileAccount account = (MPAY_MobileAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
        MPAY_CustomerMobile mobile = account.getMobile();
        MPAY_Customer customer = mobile.getRefCustomer();
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).setDefaultMobileAccount(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), customer, mobile, account);

    }
}
