package com.progressoft.mpay.registration.customers;

public class CustomerDeviceWorkflowServiceActions {
	public static final String DELETE = "SVC_Delete";
	public static final String REACTIVATE = "SVC_Reactivate";

	private CustomerDeviceWorkflowServiceActions() {

	}
}
