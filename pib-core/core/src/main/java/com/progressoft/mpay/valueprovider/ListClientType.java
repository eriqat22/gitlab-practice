package com.progressoft.mpay.valueprovider;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_ClientType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListClientType {

	public Map<Long, String> getExposedClientType() {
		Map<Long, String> map = new HashMap<>();

		List<MPAY_ClientType> clientTypes = LookupsLoader.getInstance().listClientTypes();
		String[] exposedCorporatesCodes = (LookupsLoader.getInstance().getSystemConfigurations("Exposed Corporates Codes").getConfigValue()).split(";");

		if (clientTypes != null) {
			for (MPAY_ClientType clientType : clientTypes ) {
				if(Arrays.asList(exposedCorporatesCodes).contains(clientType.getCode()))
					map.put(clientType.getId(),clientType.getCodeNamePair());
			}
		}
		return map;
	}

}
