package com.progressoft.mpay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.mpay.entities.MPAY_SysConfig;

public class SystemConfigHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(SystemConfigHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		try {
			MPAY_SysConfig sysConfig = (MPAY_SysConfig) changeHandlerContext.getEntity();
			changeHandlerContext.getFieldsAttributes().get("configValue").setRegex(sysConfig.getRegex());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new BusinessException("An error occured in the SystemConfigHandler  Handler", ex);
		}
	}

}
