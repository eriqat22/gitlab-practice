package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;

public class LanguageValidator {
	private static final Logger logger = LoggerFactory.getLogger(LanguageValidator.class);
	private static final String ACTIVE_STATUS = "104605";

	private LanguageValidator() {

	}

	public static ValidationResult validate(MPAY_Language language) {
		logger.debug("Inside Validate ...");

		if (language == null || language.getStatusId() == null || !language.getStatusId().getCode().equals(ACTIVE_STATUS))
			return new ValidationResult(ReasonCodes.LANGUAGE_NOT_DEFINED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
