package com.progressoft.mpay.mpclearinteg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.plugins.MPClearMessageProcessor;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingResult;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ValidationResult;
import com.solab.iso8583.IsoMessage;

public class MPClearReversalProcessor extends MPClearMessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(MPClearFinancialProcessor.class);

    @Override
    public MPClearProcessingResult processMessage(MPClearProcessingContext context) {
        logger.debug("Inside MPClearFinancialProcessor....");
        validateContext(context);

        String reasonCode;
        String reasonDescription;
        ValidationResult validationResult = loadOperation(context);
        if (!validationResult.isValid())
            return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());

        MessageProcessingContext processingContext = createProcessingCotext(context);
        if (processingContext == null) {
            reasonCode = ReasonCodes.TRANSACTION_NOT_FOUND;
            reasonDescription = LookupsLoader.getInstance().getReason(reasonCode).getDescription();
            return rejectMessage(context, reasonCode, reasonDescription);
        }

        MessageProcessor processor = MessageProcessorHelper.getProcessor(processingContext.getOperation());
        if (processor == null) {
            reasonCode = MPClearReasons.NOT_SUPPORTED_OPERATION;
            reasonDescription = LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(reasonCode).getDescription();
            return rejectMessage(context, reasonCode, reasonDescription);
        }
        MessageProcessingResult result = processor.reverse(processingContext);
        if (result == null) {
            reasonCode = ReasonCodes.INTERNAL_SYSTEM_ERROR;
            reasonDescription = LookupsLoader.getInstance().getReason(reasonCode).getDescription();
            return rejectMessage(context, reasonCode, reasonDescription);
        }
        updateTransaction(processingContext.getTransaction(), context.getIsoMessage());
        MPClearProcessingResult mpClearProcessingResult = new MPClearProcessingResult(result);
        mpClearProcessingResult.setResponse(MPClearHelper.createMPClearReversalResponse(new IntegrationProcessingContext(context), context.getIsoMessage().getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(),
                Integer.parseInt(context.getMessageType().getResponseCode(), 16), MPClearReasons.SUCCESS, null));
        return mpClearProcessingResult;
    }

    private MPClearProcessingResult rejectMessage(MPClearProcessingContext context, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage....");
        logger.debug("reasonCode: " + reasonCode);
        logger.debug("reasonDescription: " + reasonDescription);
        String originalMessageId = null;
        if (context.getIsoMessage() != null)
            originalMessageId = context.getIsoMessage().getField(MPClearCommonFields.MESSAGE_ID).getValue().toString();
        MPClearProcessingResult result = new MPClearProcessingResult();
        MPAY_Reason reason = LookupsLoader.getInstance().getReason(reasonCode);
        result.setReasonCode(MPClearReasons.REJECTED_BY_RECEIVER_PSP);
        result.setReasonDescription(reason.getCode() + " - " + reason.getDescription());
        if (context.getMessageType() != null) {
            result.setResponse(MPClearHelper.createMPClearReversalResponse(new IntegrationProcessingContext(context), originalMessageId, Integer.parseInt(context.getMessageType().getResponseCode(), 16), reasonCode, reasonDescription));
        }
        return result;
    }

    private ValidationResult loadOperation(MPClearProcessingContext context) {
        logger.debug("Inside LoadResponseOperation....");
        String originalMessageId = context.getIsoMessage().getField(MPClearCommonFields.ORIGINAL_MESSAGE_ID).getValue().toString();
        context.setOriginalMessage(DataProvider.instance().getMPClearMessage(originalMessageId));
        if (context.getOriginalMessage() == null) {
            logger.debug("Original Message not found, messageId: " + originalMessageId);
            return new ValidationResult(MPClearReasons.ORIGINAL_MESSAGE_NOT_FOUND,
                    LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(MPClearReasons.ORIGINAL_MESSAGE_NOT_FOUND).getDescription(), false);
        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private MessageProcessingContext createProcessingCotext(MPClearProcessingContext mpClearProcessingContext) {
        MessageProcessingContext processingContext = new MessageProcessingContext(mpClearProcessingContext);
        IsoMessage originalMessage = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(mpClearProcessingContext.getOriginalMessage().getContent());
        MPAY_MpClearIntegMsgLog originalRequest = mpClearProcessingContext.getDataProvider().getMPClearMessage(originalMessage.getField(MPClearCommonFields.ORIGINAL_MESSAGE_ID).getValue().toString());
        if (originalRequest == null)
            return null;
        processingContext.setTransaction(originalRequest.getInwardTransaction());
        if (processingContext.getTransaction() == null)
            return null;
        processingContext.setOperation(processingContext.getTransaction().getRefOperation());
        return processingContext;
    }

    private void updateTransaction(MPAY_Transaction transaction, IsoMessage isoMessage) {
        String reversalReason = null;
        if (isoMessage.getField(MPClearCommonFields.REASON_DESCRIPTION) != null)
            reversalReason = isoMessage.getField(MPClearCommonFields.REASON_DESCRIPTION).getValue().toString();
        if (reversalReason != null)
            reversalReason = isoMessage.getField(MPClearCommonFields.REASON_CODE).getValue().toString() + " - " + reversalReason;
        else
            reversalReason = isoMessage.getField(MPClearCommonFields.REASON_CODE).getValue().toString();
        transaction.setReversalReason(reversalReason);
        transaction.setReversedBy(isoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString());
    }
}
