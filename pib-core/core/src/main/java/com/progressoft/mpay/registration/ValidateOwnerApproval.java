package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class ValidateOwnerApproval implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(ValidateIsClientRegistered.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object sender = transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (sender instanceof MPAY_CustomerMobile)
			return checkCustomerMobile((MPAY_CustomerMobile)sender);
		else if (sender instanceof MPAY_CorpoarteService)
			return checkCorporateService((MPAY_CorpoarteService) sender);
		if (sender instanceof MPAY_MobileAccount)
			return checkMobileAccount((MPAY_MobileAccount) sender);
		else if (sender instanceof MPAY_ServiceAccount)
			return checkServiceAccount((MPAY_ServiceAccount) sender);
		else
			throw new WorkflowException("Invalid Object Received");
	}

	private boolean checkServiceAccount(MPAY_ServiceAccount account) {
		MPAY_CorpoarteService owner = account.getService();
		boolean isEnabled = owner.getIsRegistered() && owner.getApprovedData() == null;
		return isEnabled && checkCorporateService(owner);

	}

	private boolean checkCorporateService(MPAY_CorpoarteService service) {
		return service.getRefCorporate().getIsRegistered() && service.getRefCorporate().getApprovedData() == null;
	}

	private boolean checkCustomerMobile(MPAY_CustomerMobile mobile){
		return mobile.getRefCustomer().getIsRegistered() && mobile.getRefCustomer().getApprovedData() == null;
	}

	private boolean checkMobileAccount(MPAY_MobileAccount account) {
		MPAY_CustomerMobile owner = account.getMobile();
		boolean isEnabled = owner.getIsRegistered() && owner.getApprovedData() == null;
		return isEnabled && checkCustomerMobile(owner);

	}
}