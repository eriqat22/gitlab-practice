package com.progressoft.mpay.charges;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.common.TranslationHelper;
import com.progressoft.mpay.entities.MPAY_ChargesScheme;
import com.progressoft.mpay.entities.MPAY_Profile;

public class ValidateDeleteChargesScheme implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateDeleteChargesScheme.class);

	@Autowired
	private ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside validate --------------------- ");
		MPAY_ChargesScheme scheme = (MPAY_ChargesScheme) arg0.get(WorkflowService.WF_ARG_BEAN);
		Long linkedProfiles = itemDao.getItemCount(MPAY_Profile.class, getFilters(scheme), null);

		if (linkedProfiles != 0) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, TranslationHelper.getTranslation("charges.invalid.attachedToProfile"));
			throw iie;
		}
	}

	private List<Filter> getFilters(MPAY_ChargesScheme scheme) {
		List<Filter> filters = new ArrayList<>();
		Filter profileFilter = new Filter();
		profileFilter.setProperty("chargesScheme.id");
		profileFilter.setOperator(Operator.EQ);
		profileFilter.setFirstOperand(scheme.getId());
		filters.add(profileFilter);
		return filters;
	}
}
