package com.progressoft.mpay;

import java.util.Random;

import org.springframework.util.DigestUtils;

/**
 * @author U484
 */

public class OTPGenerator {

	private OTPGenerator() {

	}

    public static String generateCode(int maxLength) {
        Random random = new Random();
        StringBuilder result = new StringBuilder("");
        for (int i = 0; i < maxLength; i++) {
            result.append(String.valueOf(random.nextInt(10)));
        }
        return result.toString();
    }

    public static String getHashedCode(String code) {
        return DigestUtils.md5DigestAsHex(code.getBytes());
    }
}
