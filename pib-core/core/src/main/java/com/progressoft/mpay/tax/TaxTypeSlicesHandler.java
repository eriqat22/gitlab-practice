package com.progressoft.mpay.tax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_TaxSlice;

public class TaxTypeSlicesHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(TaxTypeSlicesHandler.class);
	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside TaxTypeSlicesHandler --------------------");

		MPAY_TaxSlice slice = (MPAY_TaxSlice) changeHandlerContext.getEntity();

		TaxTypeSliceHandlerHelper.handleTaxType(changeHandlerContext.getFieldsAttributes(), slice);
	}
}
