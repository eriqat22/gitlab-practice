package com.progressoft.mpay;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import com.progressoft.jfw.shared.JFWCurrencyInfoProvider;
import com.progressoft.jfw.shared.JfwCurrencyInfo;
import com.progressoft.mpay.exceptions.InvalidOperationException;

class CurrencyHelper {
	private CurrencyHelper() {
	}

	public static String formatAmount(String currencyCode, BigDecimal amount) {
		Locale currencyLocale = getLocaleByCurrencyCode(currencyCode);
		String code = null;
		if (currencyLocale == null) {
			currencyLocale = Locale.getDefault();
			code = Currency.getInstance(currencyLocale).getCurrencyCode();
		}

		return formatCurrency(currencyLocale.toLanguageTag(), code, amount.toString()).replace(",", "");
	}

	private static String formatCurrency(String localeLanguage, String currencyCode, String value) {
		JfwCurrencyInfo reportCurrency;
		try {
			reportCurrency = JFWCurrencyInfoProvider.getCurrency(localeLanguage, currencyCode);
			DecimalFormat decimalFormat = new DecimalFormat();
			decimalFormat.setMaximumFractionDigits(reportCurrency.getFractionDigits());
			decimalFormat.setMinimumFractionDigits(reportCurrency.getFractionDigits());
			return decimalFormat.format(new BigDecimal(value));
		} catch (Exception e) {
			throw new InvalidOperationException("Error while formatCurrency", e);
		}

	}

	private static Locale getLocaleByCurrencyCode(String currencyCode) {
		for (Locale locale : NumberFormat.getAvailableLocales()) {
			String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
			if (currencyCode.equals(code))
				return locale;
		}
		return null;
	}

}
