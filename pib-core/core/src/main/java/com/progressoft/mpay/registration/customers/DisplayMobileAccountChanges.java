package com.progressoft.mpay.registration.customers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.shared.DynamicField;
import com.progressoft.jfw.shared.UIPortletAttributes;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Profile;

public class DisplayMobileAccountChanges implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(DisplayMobileAccountChanges.class);
	private static final String MODIFIED_COLOR = "blue";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handle--------------------------");
		MPAY_MobileAccount account = (MPAY_MobileAccount) changeHandlerContext.getEntity();
		try {
			Collection<UIPortletAttributes> portletsAttributes = changeHandlerContext.getPortletsAttributes().values();
			for (UIPortletAttributes ui : portletsAttributes) {
				ui.setVisible(true);
			}
			renderFields(changeHandlerContext, account, MobileAccountMetaData.fromJson(account.getApprovedData()));
		} catch (Exception e) {
			throw new InvalidInputException(e);
		}

	}

	private void renderFields(ChangeHandlerContext changeHandlerContext, MPAY_MobileAccount account, MobileAccountMetaData approvedAccount) {
		logger.debug("inside RenderFields--------------------------");
		List<DynamicField> dynamicFields = new ArrayList<>();

		DynamicField field = new DynamicField();
		field.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.FIELD));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.OLD_DATA));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.NEW_DATA));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.EXTERNAL_ACCOUNT));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(approvedAccount.getExternalAcc());
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(account.getNewExternalAcc());
		if (!StringUtils.equals(approvedAccount.getExternalAcc(), account.getNewExternalAcc()))
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		MPAY_Profile profile = LookupsLoader.getInstance().getProfile(approvedAccount.getProfileId());

		field = new DynamicField();
		field.setLabel(AppContext.getMsg(MobileAccountMetaDataTranslationKeys.PROFILE));
		field.setLabelBold(true);
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(profile.getName());
		dynamicFields.add(field);

		field = new DynamicField();
		field.setLabel(account.getNewProfile().getName());
		if (approvedAccount.getProfileId() != account.getRefProfile().getId())
			field.setLabelColor(MODIFIED_COLOR);
		dynamicFields.add(field);

		changeHandlerContext.getPortletsAttributes().get("Info").setVisible(true);
		changeHandlerContext.getPortletsAttributes().get("Info").setDynamicPortlet(true);
		changeHandlerContext.getPortletsAttributes().get("Info").setDynamicFields(dynamicFields);
	}
}
