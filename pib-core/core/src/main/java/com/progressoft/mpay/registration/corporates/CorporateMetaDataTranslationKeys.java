package com.progressoft.mpay.registration.corporates;

import com.progressoft.mpay.registration.customers.CommonMetaDataTranslationKeys;

public class CorporateMetaDataTranslationKeys extends CommonMetaDataTranslationKeys {
	public static final String REGISTRATION_DATE = "CorporateRegistrationDate";
	public static final String TENANTS = "CorporateTenants";
	public static final String KYC_TEMPLATES = "CustomerKycTemplate";
}
