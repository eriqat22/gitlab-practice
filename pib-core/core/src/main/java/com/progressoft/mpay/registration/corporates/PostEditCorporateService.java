package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entity.CorporateServiceMetaData;

public class PostEditCorporateService implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostEditCorporateService.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("execute************");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN);
		if (!service.getIsRegistered())
			return;
		MPAY_CorpoarteService originalService = (MPAY_CorpoarteService) transientVars.get(WorkflowService.WF_ARG_BEAN_ORIGINAL);
		service.setNewName(service.getName());
		service.setNewDescription(service.getDescription());
		service.setNewAlias(service.getAlias());
		service.setName(originalService.getName());
		service.setDescription(originalService.getDescription());
		service.setAlias(originalService.getAlias());
		service.setApprovedData(new CorporateServiceMetaData(originalService).toString());
	}
}