package com.progressoft.mpay.common;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class WebClient {
    private final HttpClient httpClient;

    public WebClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }
    public String post(HttpPost postRequest) throws IOException {
        HttpResponse response = httpClient.execute(postRequest);
        HttpEntity entity = response.getEntity();
        return EntityUtils.toString(entity, "UTF-8");
    }

    public HttpResponse postRequest(HttpPost postRequest) throws IOException {
        return httpClient.execute(postRequest);
    }

    public String get(HttpGet getRequest) throws IOException {
        HttpResponse response = httpClient.execute(getRequest);
        HttpEntity entity = response.getEntity();
        return EntityUtils.toString(entity, "UTF-8");
    }
}