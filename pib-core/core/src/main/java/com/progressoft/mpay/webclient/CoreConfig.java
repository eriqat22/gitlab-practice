
package com.progressoft.mpay.webclient;

public final class CoreConfig {

    public static final String PREF_CHECKSUM_CODE = "checksumcode";
    public static final String IS_DEVELOP_MODE_ENABLED = "developMode";
    public static final String ENABLE_TOKEN_ENCRYPTION = "ENABLE_TOKEN_ENCRYPTION";
    public static final String PREF_PINNING_CER_KEY = "pinning";
    public static final String PREF_SECURE_HASH_CER_KEY = "secureHash";

    public static final String PREF_LINK = "link_pref";

    private CoreConfig() {
    }
}
