package com.progressoft.mpay.psp;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;

public class AddPSPServicesCondition implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(AddPSPServicesCondition.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transVars, Map argument, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside AddPSPServicesCondition Condition.....");

		return Boolean.FALSE;
	}

}
