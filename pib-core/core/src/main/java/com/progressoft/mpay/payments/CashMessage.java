package com.progressoft.mpay.payments;

import java.math.BigDecimal;

import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;

import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class CashMessage extends MPayRequest {

	public static final String RECEIVER_KEY = "receiver";
	public static final String RECEIVER_TYPE_KEY = "receiverType";
	public static final String AMOUNT_KEY = "amount";
	public static final String CHARGE_AMOUNT_KEY = "charges";
	public static final String TAX_AMOUNT_KEY = "taxes";
	public static final String NOTES_KEY = "notes";
	public static final String SENDER_ACCOUNT_KEY = "senderAccount";
	public static final String RECEIVER_ACCOUNT_KEY = "receiverAccount";

	private String receiver;
	private String receiverType;
	private BigDecimal chargesAmount;
	private BigDecimal taxesAmount;
	private String notes;
	private String senderAccount;
	private String receiverAccount;

	public CashMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiverMobile) {
		this.receiver = receiverMobile;
	}

	public String getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(String receiverType) {
		this.receiverType = receiverType;
	}

	public BigDecimal getChargesAmount() {
		return chargesAmount;
	}

	public void setChargesAmount(BigDecimal chargesAmount) {
		this.chargesAmount = chargesAmount;
	}

	public BigDecimal getTaxesAmount() {
		return taxesAmount;
	}

	public void setTaxesAmount(BigDecimal taxesAmount) {
		this.taxesAmount = taxesAmount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public String getReceiverAccount() {
		return receiverAccount;
	}

	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	public static CashMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		CashMessage message = new CashMessage(request);
		if (StringUtils.isEmpty(message.getSender()))
			throw new MessageParsingException(SENDER_KEY);
		if (message.getSenderType() == null || message.getSenderType().trim().length() == 0)
			throw new MessageParsingException(SENDER_TYPE_KEY);
		String amount = message.getValue(AMOUNT_KEY);
		if (amount == null)
			throw new MessageParsingException(AMOUNT_KEY);
		try {
			message.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
		} catch (NumberFormatException ex) {
			throw new MessageParsingException(AMOUNT_KEY);
		}
		String charges = message.getValue(CHARGE_AMOUNT_KEY);
		if (charges == null)
			throw new MessageParsingException(CHARGE_AMOUNT_KEY);
		try {
			message.setChargesAmount(BigDecimal.valueOf(Double.valueOf(charges)));
		} catch (NumberFormatException ex) {
			throw new MessageParsingException(CHARGE_AMOUNT_KEY);
		}
		String taxes = message.getValue(TAX_AMOUNT_KEY);
		if (taxes == null)
			throw new MessageParsingException(TAX_AMOUNT_KEY);
		try {
			message.setTaxesAmount(BigDecimal.valueOf(Double.valueOf(taxes)));
		} catch (NumberFormatException ex) {
			throw new MessageParsingException(TAX_AMOUNT_KEY);
		}
		message.setReceiver(message.getValue(RECEIVER_KEY));
		if (message.getReceiver() == null)
			throw new MessageParsingException(RECEIVER_KEY);
		message.setReceiverType(message.getValue(RECEIVER_TYPE_KEY));
		if (message.getReceiverType() == null)
			throw new MessageParsingException(RECEIVER_TYPE_KEY);
		message.setSenderAccount(message.getValue(SENDER_ACCOUNT_KEY));
		message.setReceiverAccount(message.getValue(RECEIVER_ACCOUNT_KEY));
		message.setNotes(message.getValue(NOTES_KEY));
		return message;
	}
}
