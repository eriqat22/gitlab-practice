package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CorpIntegMessage;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_CustIntegActionType;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgType;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoValue;

public class Post1700CorpProcessMpClearInegMsgLog implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(Post1700CorpProcessMpClearInegMsgLog.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside Post1700CorpProcessMpClearInegMsgLog----------------*---------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		MPAY_CorpIntegMessage integrationMessage = new MPAY_CorpIntegMessage();
		integrationMessage.setRefMsgLog(msg);
		MPAY_Corporate corporate;
		MPAY_CorpoarteService service;
		String[] tokens = msg.getHint().split(";");
		if (msg.getHint().endsWith("CC")) {
			corporate = DataProvider.instance().getCorporate(Long.valueOf(tokens[0]));
			service = corporate.getRefCorporateCorpoarteServices().get(0);
		} else {
			service = DataProvider.instance().getCorporateService(Long.valueOf(tokens[0]));
			corporate = service.getRefCorporate();
		}

		integrationMessage.setRefService(service);
		integrationMessage.setRefCorporate(corporate);
		MPAY_MpClearIntegMsgType integType = LookupsLoader.getInstance().getMpClearIntegrationMessageTypes(IntegMessageTypes.TYPE_1700);
		integrationMessage.setIntegMsgType(integType);

		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());
		integrationMessage.setRefMessage(isomsg.getField(115).toString());
		integrationMessage.setActionType(setActionType(isomsg));
		corporate.setRefLastMsgLog(msg);
		try {
			DataProvider.instance().persistCorporateIntegMessage(integrationMessage);
			DataProvider.instance().updateCorporate(corporate);
			JfwHelper.sendToActiveMQ(integrationMessage, integrationMessage.getTenantId(),
					MPAYView.CORP_1700_REG_MSGS.viewName, "mpay.mpclear.register");
		} catch (Exception e) {
			throw new WorkflowException(e);
		}

	}

	private MPAY_CustIntegActionType setActionType(IsoMessage m) {
		IsoValue<Object> at = m.getField(48);
		return LookupsLoader.getInstance().getCustomerIntegrationActionTypes(at.toString());
	}
}
