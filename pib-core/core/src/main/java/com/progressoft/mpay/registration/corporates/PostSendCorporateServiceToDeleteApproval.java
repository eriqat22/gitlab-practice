package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostSendCorporateServiceToDeleteApproval implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostSendCorporateServiceToDeleteApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside execute =========");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
		service.setIsRegistered(false);
		service.setIsActive(false);
		handleAccounts(service);
	}

	private void handleAccounts(MPAY_CorpoarteService service) throws WorkflowException {
		logger.debug("inside HandleAccounts =========");
		List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accountsIterator = accounts.listIterator();
		if (accountsIterator.hasNext()) {
			MPAY_ServiceAccount account = accountsIterator.next();
			if (account.getDeletedFlag() == null || !account.getDeletedFlag())
				JfwHelper.executeAction(MPAYView.SERVICE_ACCOUNTS_VIEW, account, ServiceAccountWorkflowServiceActions.DELETE_APPROVAL, new WorkflowException());
		}
	}
}
