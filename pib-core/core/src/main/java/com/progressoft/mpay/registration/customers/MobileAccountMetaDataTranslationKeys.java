package com.progressoft.mpay.registration.customers;

public class MobileAccountMetaDataTranslationKeys extends CommonMetaDataTranslationKeys {
	public static final String EXTERNAL_ACCOUNT = "AccountExternalAccount";
	public static final String PROFILE = "AccountProfile";
}
