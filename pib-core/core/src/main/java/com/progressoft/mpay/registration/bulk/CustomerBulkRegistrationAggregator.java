package com.progressoft.mpay.registration.bulk;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class CustomerBulkRegistrationAggregator implements AggregationStrategy {

	@SuppressWarnings("unchecked")
	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		CustomerRegistraionLine registrationLine = newExchange.getIn().getBody(CustomerRegistraionLine.class);
		if(oldExchange == null) {
			List<CustomerRegistraionLine> registraionLines = new ArrayList<>();
			registraionLines.add(registrationLine);
			newExchange.getIn().setBody(registraionLines);
			return newExchange;
		} else {
			List<CustomerRegistraionLine> registraionLines = (List<CustomerRegistraionLine>) oldExchange.getIn().getBody();
			registraionLines.add(registrationLine);
			oldExchange.getIn().setBody(registraionLines);
			return oldExchange;
		}
	}

}
