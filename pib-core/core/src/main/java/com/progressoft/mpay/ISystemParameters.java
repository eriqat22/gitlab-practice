package com.progressoft.mpay;

import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.mpay.entities.MPAY_AppsChecksum;
import com.progressoft.mpay.entities.MPAY_Language;

import java.math.BigDecimal;

public interface ISystemParameters {

    JFWCurrency getDefaultCurrency();

    int getPinCodeLength();

    int getMnoIntegrationTimeout();

    String getSigningDateFormat();

    String getPersonClienttype();

    String getDefaultEncoding();

    String getMPClearCertificateAlias();

    String getEPayCertificateAlias();

    String getCertificatePSP();

    String getEfawatercomCertificateAlias();

    String getKeyStorePath();

    String getKeyStorePass();

    int getRoutingCodeLength();

    int getTxCount();

    int getMobileAccountSelectorLength();

    int getMpClearGracePeriod();

    String getAmountFormat();

    String getAccountRegex();

    String getChargesAmountFormat();

    boolean getSendActivationCodeUponCustomerMobileRegistration();

    String getDefaultProfile();

    int getCorporateRegistrationIdType();

    int getMpClearLoginValidtyHours();

    int getATMCodeValidityMinutes();

    int getATMCodeLength();

    int getActivationCodeLength();

    boolean getForceFailed1644();

    boolean getForceFailed56();

    String getCountryCode();

    int getMPClearResendWaitingTime();

    MPAY_Language getSystemLanguage();

    boolean getOfflineModeEnabled();

    int getPinCodeMaxRetryCount();

    String getHashingAlogorithm();

    int getDecimalCount();

    String getAliasChangeEndPointOperation();

    String getApplicationNameEn();

    String getApplicationNameAr();

    String getRegistrationBankCode();

    String getRegistrationProfileCode();

    String getDefaultBankCode();

    String getMigsVpcVersion();

    String getMigsVpcCurrency();

    String getMigsVpcAccessCode();

    String getMigsVpcCommand();

    String getMigsVpcMershant();

    String getMigsVpcRedirectUrl();

    String getMigsVpcTitle();

    String getMigsSZ();

    String getMigsAmountKind();

    String getMigsUrl();

    String getMigsSecureSecret();

    String getAirTimeServiceTypeCode();

    String getDonationsServiceTypeCode();

    String getEncryptionCertificateAlias();

    boolean enableTokenEncryption();

    int getOtpValidityInMinutes();

    int getMobileSignInValidityInMin();

    int getServiceSignInValidityInMin();

    int getPasswordMaxRetryCount();

    boolean getEnableAppsChecksums();

    String getAgentCode();

    BigDecimal getCashCap();

    String getMobileNumberRegex();

    String getEmailRegex();

    String getSMTPHost();

    int getSMTPPort();

    String getEmailSubject();

    String getEmailFrom();

    String getPushNotificationURL();

    String getAPIKey();

    String getISO8583ConfigFilePath();

    String getMPClearProcessor();

    String getGAMParkingProductId();

    long getBulkRegistrationDefaultId();

    long getBulkPaymentDefaultId();

    String getPinExpiryTimeInHours();

    String getPasswordExpiryTimeInHours();

    String getMEPSServiceName();

    String getMEPSPaymentEndpoint();

    String getMEPSReversalEndpoint();

    String getMEPSISO8583ConfigFilePath();

    String getBulkPaymentOperations();

    boolean isSystemStandAlone();

    String getCustomersChangeHandler();

    String getCorporatesChangeHandler();

    String getEmailUsername();

    String getEmailPassword();

    String getAgentTypeCode();

    String getBanksUsersLicense();

    String getEnableRegistrationOTP();

    long getCashCapWatermark();

    String getCashCAPNotificationEmail();

    boolean getCashCAPNotificationSent();

    boolean allowYaqeenAddressToBeNull();

    String getDefaultCity();

    Long getNumberOfAllowsOtpRequest();

    String getVIbanCheckDigits();

    int getActivationCodeValidity();

    String getPushNotificationService();

    String middleWareUrl();

    String getMobileNumber();

    String getDeploymentLocation();

    String getServiceAdminMessageTypeCode();

    boolean getSendPinCodeUponRegistration();

    int getAmlLowerScoreHit();

    int getAmlUpperScoreHit();

    int getAmlNumberOfHits();

    String getATMPublicKey();

    String getAmlEndPointOperation();

    boolean isSmsNotificationEnabledOnCustomerRejection();

    boolean isSmsNotificationEnabledOnCashinExceededLimits();

    int getDormantMobileAccountTimeoutInDays();

    int getDormantServiceAccountTimeoutInDays();

    String getATMMessageTypeCode();

    boolean getMaskMobileNumberInNotification();

    BigDecimal getATMMultiplies();

    boolean getIsCustomerSetActiveOrNot();

    String getOperationVersion();

    String getClientId();

    String getClearingSystemId();

    String getMpClearBicCode();

    String getSendingPSPBicCode();

    String getSendingAgentShortName();

    String getOperators();

    String getMobileNotificationChannels();

    String getTollFreeNumber();

    String getInterNationalNumber();

    String getComplainsLinkURL();

    String getATMlocationsURL();

    String getHomeNotificationsCount();

    String getPageNotificationsCount();

    boolean createServiceVirtualIbanOrNot();

    boolean getSendPinCodeUponCorporateRegistration();

    String getImagePath();

    String getCoreAppCheckSumCode();

    String getReferralCommissionAmount();

    String getRefereeCommissionAmount();

    int getRefundPeriodInDays();

    String getSmppUrl();

    String getSmsServiceId();

    Long getKycWalletId();

    Long getKycFullWalletId();
    
    String getOperationsAllowedToBerefunded();
}
