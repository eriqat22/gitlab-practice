package com.progressoft.mpay.templates;

import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerKyc;

import java.util.Objects;

import static com.progressoft.mpay.templates.TemplateBehaviour.*;


public class CustomerKycTemplateChangeHandler implements ChangeHandler {

    @Override
    public void handle(ChangeHandlerContext context) {
        /*MPAY_Customer customer = (MPAY_Customer) context.getEntity();
        MPAY_CustomerKyc kycTemplate = customer.getKycTemplate();

        if (Objects.isNull(kycTemplate))
            return;

        KycTemplateHandler handler = new KycTemplateHandler(customer);
        handler.handleTemplateOf(kycTemplate, VISIBLE, context);
        handler.handleTemplateOf(kycTemplate, ENABLED, context);
//        handler.handleTemplateOf(kycTemplate, REQ, context);

        setTemplateDefaultValues(customer, kycTemplate);*/
    }

    private void setTemplateDefaultValues(MPAY_Customer customer, MPAY_CustomerKyc kycTemplate) {
        customer.setEnglishFullName(kycTemplate.getEnglishFullNameDefault());
        customer.setArabicFullName(kycTemplate.getArabicFullNameDefault());
        customer.setIdentificationReference(kycTemplate.getIdentificationReferenceDefault());
        customer.setIdentificationCard(kycTemplate.getIdentificationCardDefault());
        customer.setIdCardIssuanceDate(kycTemplate.getIdCardIssuanceDateDefault());
        customer.setPassportId(kycTemplate.getPassportIdDefault());
        customer.setPassportIssuanceDate(kycTemplate.getPassportIssuanceDateDefault());
        customer.setPhoneOne(kycTemplate.getPhoneOneDefault());
        customer.setPhoneTwo(kycTemplate.getPhoneTwoDefault());
        customer.setArea(kycTemplate.getAreaDefault());
        customer.setPobox(kycTemplate.getPoboxDefault());
        customer.setZipCode(kycTemplate.getZipCodeDefault());
        customer.setBuildingNum(kycTemplate.getBuildingNumDefault());
        customer.setStreetName(kycTemplate.getStreetNameDefault());
        customer.setAddress(kycTemplate.getAddressDefault());
        customer.setNote(kycTemplate.getNoteDefault());
        customer.setOccupation(kycTemplate.getOccupationDefault());
        customer.setIdExpiryDate(kycTemplate.getIdExpiryDateDefault());
    }


}
