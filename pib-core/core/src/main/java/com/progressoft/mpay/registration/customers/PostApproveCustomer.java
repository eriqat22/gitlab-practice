package com.progressoft.mpay.registration.customers;

import com.google.gson.Gson;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.*;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.*;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.psp.PSP;
import com.progressoft.mpay.webclient.model.ResponseModel;
import com.progressoft.mpay.webclient.request.RequestManager;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static ch.lambdaj.Lambda.*;
import static java.lang.Long.parseLong;

public class PostApproveCustomer implements FunctionProvider {

    private static final String REFERRAL_KEY_LENGTH = "Referral Code Length";

    private static final Logger logger = LoggerFactory.getLogger(PostApproveCustomer.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("Inside execute ======");
        MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
        MPAY_CustomerMobile customerMobile = customer.getRefCustomerCustomerMobiles().get(0);
        MPAY_MobileAccount mobileAccount = customerMobile.getMobileMobileAccounts().stream().filter(m -> m.getCategory().getId().equals(parseLong(AccountCategory.CUSTOMER))).findFirst().orElse(null);

        if (!customer.getIsRegistered()) {
            MPAY_CustomerMobile mobile = getPendingMobile(customer);
            if (mobile == null)
                throw new WorkflowException("Cutomer doesn't have pending mobile");
            JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, CustomerMobileWorkflowServiceActions.APPROVE, new WorkflowException());
        } else {
            boolean hasPendingMobile = true;
            MPAY_CustomerMobile mobile = getPendingMobile(customer);
            if (mobile == null) {
                hasPendingMobile = false;
                mobile = getFirstRegisteredMobile(customer);
            }
            if (mobile == null)
                throw new WorkflowException("Cutomer doesn't have registered mobile");
            if (hasPendingMobile)
                JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, CustomerMobileWorkflowServiceActions.APPROVE, new WorkflowException());
            MPAY_MobileAccount account = handlePendingAccounts(customer);
            if (!hasPendingMobile)
                handleUpdateClient(customer, mobile, account);
            try {
                boolean isAutoRegistered = customer.getIsAutoRegistered();
                Boolean isLight = customer.getIsLight();

                applyApprovedData(mobileAccount, customer);

                System.out.println("IsAutoRegistered : " + isAutoRegistered);
                System.out.println("isLight : " + isLight);
                if (customer.getRegAgent() != null)
                    System.out.println("RegAgent is : " + customer.getRegAgent().getName());
//                if ((isAutoRegistered || isLight) && customer.getRegAgent() != null)
//                    callApi(buildRequest(customer));
            } catch (Exception e) {
                com.opensymphony.workflow.InvalidInputException iie = new com.opensymphony.workflow.InvalidInputException();
                iie.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS, e.getMessage());
                throw iie;
            }
        }
    }

    private String getOperator(String customerOperatorValue) {
        try {
            JSONObject json = new JSONObject(customerOperatorValue);
            if (json.isNull("key"))
                return customerOperatorValue;
            return json.getString("key");
        } catch (Exception e) {
            return customerOperatorValue;
        }
    }

    private void handleUpdateClient(MPAY_Customer customer, MPAY_CustomerMobile mobile, MPAY_MobileAccount account) throws WorkflowException {
        logger.debug("inside handleUpdateClient ============");
        MPAY_MobileAccount actualAccount = account;
        if (actualAccount == null)
            actualAccount = getFirstRegisteredAccount(mobile);
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).updateCustomer(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), customer, mobile, actualAccount);
    }

    private MPAY_MobileAccount getFirstRegisteredAccount(MPAY_CustomerMobile mobile) {
        logger.debug("inside getFirstRegisteredAccount ============");
        return selectFirst(mobile.getMobileMobileAccounts(), having(on(MPAY_MobileAccount.class).getIsRegistered(), Matchers.equalTo(true)));
    }

    private MPAY_MobileAccount handlePendingAccounts(MPAY_Customer customer) throws WorkflowException {
        logger.debug("inside handlePendingAccounts ============");
        List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId());
        if (mobiles == null || mobiles.isEmpty())
            return null;
        Iterator<MPAY_CustomerMobile> mobilesIterator = mobiles.iterator();

        do {
            MPAY_CustomerMobile mobile = mobilesIterator.next();
            List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
            MPAY_MobileAccount account = selectFirst(accounts, having(on(MPAY_MobileAccount.class).getStatusId().getCode(), Matchers.equalTo(MobileAccountWorkflowStatuses.APPROVAL.toString())));
            if (account != null) {
                JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.APPROVE, new WorkflowException());
                return account;
            }
        } while (mobilesIterator.hasNext());
        return null;
    }

    private MPAY_CustomerMobile getPendingMobile(MPAY_Customer customer) throws WorkflowException {
        logger.debug("inside getPendingMobile ============");
        List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId(), CustomerMobileWorkflowStatuses.APPROVAL);
        if (mobiles == null || mobiles.isEmpty())
            return null;
        return mobiles.get(0);
    }

    private MPAY_CustomerMobile getFirstRegisteredMobile(MPAY_Customer customer) throws WorkflowException {
        logger.debug("inside getFirstRegisteredMobile ============");
        List<MPAY_CustomerMobile> mobiles = DataProvider.instance().listCustomerMobiles(customer.getId());
        if (mobiles == null || mobiles.isEmpty())
            return null;

        return selectFirst(mobiles, having(on(MPAY_CustomerMobile.class).getIsRegistered(), Matchers.equalTo(true)));
    }

    private void applyApprovedData(MPAY_MobileAccount account, MPAY_Customer customer) throws Exception {
        String customerId = String.valueOf(customer.getId());
        customer.setIsActive(true);
        customer.setIsAutoRegistered(false);
        customer.setIsLight(false);
        customer.setExpiryDate(null);
        customer.setReferralkey(fillReferralKey(customerId));
        customer.setWalletUpgraded(true);

        if (account.getNewProfile() != null) {
            account.setRefProfile(account.getNewProfile());
            account.setNewProfile(null);
        }
    }

    private String fillReferralKey(String customerId) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        String configValue = LookupsLoader.getInstance().getSystemConfigurations(REFERRAL_KEY_LENGTH).getConfigValue();
        byte[] hash = digest.digest(customerId.getBytes(StandardCharsets.UTF_8));
        String base64String = new BigInteger(1, hash).toString(36);

        return base64String.substring(0, Integer.parseInt(configValue));
    }

    private MPayRequest buildRequest(MPAY_Customer customer) throws WorkflowException {
        MPayRequest request = new MPayRequest();
        request.setOperation("agentcommission");
        request.setSender(getPSPCommissionService().getName());
        request.setSenderType(ReceiverInfoType.CORPORATE);
        request.setDeviceId(null);
        request.setLang(getSysLanguage());
        request.setMsgId(UUID.randomUUID().toString());
        request.setTenant(customer.getTenantId());
        request.setAmount(BigDecimal.ZERO);
        request.setReceiver(customer.getRegAgent().getName());
        request.setReceiverType(ReceiverInfoType.CORPORATE);
        request.setChecksum(MPayContext.getInstance().getSystemParameters().getCoreAppCheckSumCode());
        return request;
    }

    private long getSysLanguage() {
        return LanguageMapper.getInstance().getLanguageLongByLocaleCode(SystemParameters.getInstance().getSystemLanguage().getCode());
    }

//    private void callApi(MPayRequest request) throws Exception {
//        try {
//            String mpaySpringUrl = LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.INTERNAL_INTEGRATION_URL).getConfigValue();
//            logger.info("********** Agent has to be set commission , Calling agent commission spring url : " + mpaySpringUrl);
//            RequestManager requestManager = new RequestManager(mpaySpringUrl);
//            String response = requestManager.post(request);
//            logger.info("********** Agent commission set for receiver agent :" + request.getReceiver());
//
//            ResponseModel responseModel = new Gson().fromJson(response, ResponseModel.class);
//            if (!responseModel.getResponse().getErrorCd().equals(ReasonCodes.VALID))
//                throw new Exception(responseModel.getResponse().getDesc());
//        } catch (Exception e) {
//            throw new Exception(e.getMessage());
//        }
//    }

    private MPAY_CorpoarteService getPSPCommissionService() throws WorkflowException {
        MPAY_CorpoarteService pspCashInService = new MPAY_CorpoarteService();
        MPAY_Corporate psp = PSP.get();
        Optional<MPAY_CorpoarteService> service = psp.getRefCorporateCorpoarteServices()
                .stream()
                .filter(c -> MessageCodes.COMMISSIONS.toString().equals(c.getPaymentType().getCode()))
                .findFirst();
        return service.orElse(pspCashInService);
    }
}