package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class ValidateEditServiceAccount implements Validator {
	private static final Logger logger = LoggerFactory.getLogger(ValidateEditServiceAccount.class);

	@Autowired
	ItemDao itemDao;

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("validate =======");

		MPAY_ServiceAccount currentAccount = (MPAY_ServiceAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
		validateMasLength(currentAccount);
		MPAY_CorpoarteService service = currentAccount.getService();
		List<MPAY_ServiceAccount> allAccounts = DataProvider.instance().listServiceAccounts(service.getId());
		if (allAccounts == null || allAccounts.isEmpty())
			return;
		Iterator<MPAY_ServiceAccount> accounts = allAccounts.iterator();
		do {
			MPAY_ServiceAccount account = accounts.next();
			if (account.getId() == currentAccount.getId())
				continue;
			validateEditAccount(account, currentAccount);

		} while (accounts.hasNext());
	}

	private void validateEditAccount(MPAY_ServiceAccount account, MPAY_ServiceAccount currentAccount) throws InvalidInputException {
		InvalidInputException cause = new InvalidInputException();
		String storedRegistrationId = account.getBank().getCode() + String.valueOf(account.getMas());
		if (storedRegistrationId.equals(currentAccount.getBank().getCode() + String.valueOf(currentAccount.getMas()))) {
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_MAS));
			throw cause;
		}
		if (currentAccount.getExternalAcc() != null && currentAccount.getExternalAcc().equals(account.getExternalAcc())) {
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_EXTERNAL_ACCOUNT));
			throw cause;
		}
		if (currentAccount.getIsDefault() && account.getIsDefault()) {
			cause.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.DUPLICATE_ISDEFUALT));
			throw cause;
		}
	}

	private void validateMasLength(MPAY_ServiceAccount serviceAccount) throws InvalidInputException {
		if (String.valueOf(serviceAccount.getMas()).length() > SystemParameters.getInstance().getMobileAccountSelectorLength()) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, String.format(AppContext.getMsg(MPayErrorMessages.INVALID_MAS_LENGTH), SystemParameters.getInstance().getMobileAccountSelectorLength()));
			throw iie;
		}
	}
}