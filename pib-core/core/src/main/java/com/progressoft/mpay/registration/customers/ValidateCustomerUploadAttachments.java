package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerAtt;
import com.progressoft.mpay.registration.corporates.ValidateCorpServiceLogoAttached;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.progressoft.mpay.MPayErrorMessages.NO_FILE_UPLOADED;

@Component
public class ValidateCustomerUploadAttachments implements Validator {

    @Autowired
    ItemDao itemDao;
    private static final Logger logger = LoggerFactory.getLogger(ValidateCorpServiceLogoAttached.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside ValidateCreateCorporateService-----");
        MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
        String where = "recordId=" + customer.getId();
        Long itemCount = itemDao.getItemCount(MPAY_CustomerAtt.class, null, where);
        
        if (itemCount == 0) {
            InvalidInputException iie = new InvalidInputException();
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(NO_FILE_UPLOADED));
            throw iie;
        }
    }
}
