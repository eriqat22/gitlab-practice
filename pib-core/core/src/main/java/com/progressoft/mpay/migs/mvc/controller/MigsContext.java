package com.progressoft.mpay.migs.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.IServiceUserManager;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.migs.request.MigsRequest;
import com.progressoft.mpay.migs.response.MigsResponse;

public class MigsContext {

	private HttpServletRequest request;
	private MigsRequest migsRequest;
	private ItemDao itemDao;
	private IServiceUserManager serviceUserManager;
	private ISystemParameters systemParameters;
	private ILookupsLoader lookupLoader;
	private ItemDao itemDaoWithoutTenant;
	private MigsResponse migsResponse;

	public void setHttpServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletRequest getServletRequest() {
		return request;
	}

	public void setMigsRequest(MigsRequest migsRequest) {
		this.migsRequest = migsRequest;
	}

	public MigsRequest getMigsRequest() {
		return migsRequest;
	}

	public void setItemDao(ItemDao itemDao) {
		this.itemDao = itemDao;
	}

	public ItemDao getItemDao() {
		return itemDao;
	}

	public void setServiceUserManager(IServiceUserManager serviceUserManager) {
		this.serviceUserManager = serviceUserManager;
	}

	public IServiceUserManager getServiceUserManager() {
		return serviceUserManager;
	}

	public void setSystemParameters(ISystemParameters systemParameters) {
		this.systemParameters = systemParameters;
	}

	public ISystemParameters getSystemParameters() {
		return systemParameters;
	}

	public void setLookupLoader(ILookupsLoader lookupLoader) {
		this.lookupLoader = lookupLoader;
	}

	public ILookupsLoader getLookupLoader() {
		return lookupLoader;
	}

	public void setItemDaoWithoutTenant(ItemDao itemDaoWithoutTenant) {
		this.itemDaoWithoutTenant =  itemDaoWithoutTenant;
	}

	public ItemDao getItemDaoWithoutTenant() {
		return itemDaoWithoutTenant;
	}

	public void setMigsResponse(MigsResponse migsResponse) {
		this.migsResponse = migsResponse;
	}

	public MigsResponse getMigsResponse() {
		return migsResponse;
	}
}
