package com.progressoft.mpay;

import java.math.BigDecimal;
import java.util.Date;

public class AccountLimits {
	private long accountId;
	private long messageTypeId;
	private Date dailyDate;
	private int weekNumber;
	private int month;
	private int year;
	private int dailyCount;
	private BigDecimal dailyAmount;
	private int weeklyCount;
	private BigDecimal weeklyAmount;
	private int monthlyCount;
	private BigDecimal monthlyAmount;
	private int yearlyCount;
	private BigDecimal yearlyAmount;
	private int totalDailyCount;
	private BigDecimal totalDailyAmount;
	private int totalWeeklyCount;
	private BigDecimal totalWeeklyAmount;
	private int totalMonthlyCount;
	private BigDecimal totalMonthlyAmount;
	private int totalYearlyCount;
	private BigDecimal totalYearlyAmount;

	public AccountLimits() {
		this.dailyAmount = BigDecimal.ZERO;
		this.weeklyAmount = BigDecimal.ZERO;
		this.monthlyAmount = BigDecimal.ZERO;
		this.yearlyAmount = BigDecimal.ZERO;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getMessageTypeId() {
		return messageTypeId;
	}

	public void setMessageTypeId(long messageTypeId) {
		this.messageTypeId = messageTypeId;
	}

	public Date getDailyDate() {
		return dailyDate;
	}

	public void setDailyDate(Date dailyDate) {
		this.dailyDate = dailyDate;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getDailyCount() {
		return dailyCount;
	}

	public void setDailyCount(int dailyCount) {
		this.dailyCount = dailyCount;
	}

	public BigDecimal getDailyAmount() {
		return dailyAmount;
	}

	public void setDailyAmount(BigDecimal dailyAmount) {
		this.dailyAmount = dailyAmount;
	}

	public int getWeeklyCount() {
		return weeklyCount;
	}

	public void setWeeklyCount(int weeklyCount) {
		this.weeklyCount = weeklyCount;
	}

	public BigDecimal getWeeklyAmount() {
		return weeklyAmount;
	}

	public void setWeeklyAmount(BigDecimal weeklyAmount) {
		this.weeklyAmount = weeklyAmount;
	}

	public int getMonthlyCount() {
		return monthlyCount;
	}

	public void setMonthlyCount(int monthlyCount) {
		this.monthlyCount = monthlyCount;
	}

	public BigDecimal getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(BigDecimal monthyAmount) {
		this.monthlyAmount = monthyAmount;
	}

	public int getYearlyCount() {
		return yearlyCount;
	}

	public void setYearlyCount(int yearlyCount) {
		this.yearlyCount = yearlyCount;
	}

	public BigDecimal getYearlyAmount() {
		return yearlyAmount;
	}

	public void setYearlyAmount(BigDecimal yearlyAmount) {
		this.yearlyAmount = yearlyAmount;
	}

	public void setTotalDailyCount(int totalDailyCount) {
		this.totalDailyCount = totalDailyCount;
	}

	public int getTotalDailyCount() {
		return totalDailyCount;
	}

	public void setTotalDailyAmount(BigDecimal totalDailyAmount) {
		this.totalDailyAmount = totalDailyAmount;
	}

	public BigDecimal getTotalDailyAmount() {
		return totalDailyAmount;
	}

	public void setTotalWeeklyCount(int totalWeeklyCount) {
		this.totalWeeklyCount = totalWeeklyCount;
	}

	public int getTotalWeeklyCount() {
		return totalWeeklyCount;
	}

	public void setTotalWeeklyAmount(BigDecimal totalWeeklyAmount) {
		this.totalWeeklyAmount = totalWeeklyAmount;
	}

	public BigDecimal getTotalWeeklyAmount() {
		return totalWeeklyAmount;
	}

	public void setTotalMonthlyCount(int totalMonthlyCount) {
		this.totalMonthlyCount = totalMonthlyCount;
	}

	public int getTotalMonthlyCount() {
		return totalMonthlyCount;
	}

	public void setTotalMonthlyAmount(BigDecimal totalMonthlyAmount) {
		this.totalMonthlyAmount = totalMonthlyAmount;
	}

	public BigDecimal getTotalMonthlyAmount() {
		return totalMonthlyAmount;
	}

	public void setTotalYearlyCount(int totalYearlyCount) {
		this.totalYearlyCount = totalYearlyCount;
	}

	public int getTotalYearlyCount() {
		return totalYearlyCount;
	}

	public void setTotalYearlyAmount(BigDecimal totalYearlyAmount) {
		this.totalYearlyAmount = totalYearlyAmount;
	}

	public BigDecimal getTotalYearlyAmount() {
		return totalYearlyAmount;
	}
}
