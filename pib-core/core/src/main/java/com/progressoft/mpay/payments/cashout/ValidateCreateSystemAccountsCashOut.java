package com.progressoft.mpay.payments.cashout;

import java.math.BigDecimal;
import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_SystemAccountsCashOut;
import com.progressoft.mpay.entity.BalanceTypes;

public class ValidateCreateSystemAccountsCashOut implements Validator {

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		MPAY_SystemAccountsCashOut cashOut = (MPAY_SystemAccountsCashOut) arg0.get(WorkflowService.WF_ARG_BEAN);
		MPAY_Account sourceAccount = cashOut.getSourceService().getServiceServiceAccounts().get(0).getRefAccount();
		if (sourceAccount.getBalanceType().getCode().equals(BalanceTypes.CREDIT) && (sourceAccount.getBalance().add(cashOut.getAmount()).compareTo(BigDecimal.ZERO) > 0)
				|| sourceAccount.getBalanceType().getCode().equals(BalanceTypes.DEBIT) && (sourceAccount.getBalance().subtract(cashOut.getAmount()).compareTo(BigDecimal.ZERO) < 0)) {
			InvalidInputException iie = new InvalidInputException();
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.INSUFFICIENT_FUNDS));
			throw iie;
		}
	}
}
