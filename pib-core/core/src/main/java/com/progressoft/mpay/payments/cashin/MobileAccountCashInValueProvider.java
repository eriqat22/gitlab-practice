package com.progressoft.mpay.payments.cashin;

import java.util.HashMap;
import java.util.Map;

import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.registration.ClientManagement;

public class MobileAccountCashInValueProvider {

	public Map<Object, Object> getMobileAccounts(String mobileId) {
		Map<Object, Object> accounts = new HashMap<>();
		if (mobileId != null && mobileId.length() > 0) {
			MPAY_CustomerMobile mobile = MPayContext.getInstance().getDataProvider().getCustomerMobile(Long.parseLong(mobileId));
			for (MPAY_MobileAccount account : mobile.getMobileMobileAccounts()) {
				if (account.getBankedUnbanked().equals(BankedUnbankedFlag.UNBANKED) && account.getIsRegistered()) {
					accounts.put(ClientManagement.getRegistrationId(account), ClientManagement.getRegistrationId(account));
				}
			}
		}

		return accounts;
	}
}