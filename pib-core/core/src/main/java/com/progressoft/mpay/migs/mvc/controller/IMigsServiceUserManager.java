package com.progressoft.mpay.migs.mvc.controller;

public interface IMigsServiceUserManager {

	boolean enableServiceUser(String tenantName);

	void disableServiceUser(boolean enabledServiceUser);
}
