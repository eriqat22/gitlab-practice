package com.progressoft.mpay;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.integration.camel.types.ComponentOption;
import com.progressoft.jfw.integration.comm.CommunicationConfig;
import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.jfw.integration.comm.JfwMessage;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.utils.ItemUtils;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.jaxb.JaxbHandler;
import com.progressoft.mpay.mpclear.MpClearIntegMsg;
import com.progressoft.mpay.sms.NotificationRequest;
import flexjson.JSONDeserializer;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class JfwHelper {
	private static final String JFW_COMMUNICATOR = "jfwCommunicator";
	private static final String MPAY_REF_MESSAGE_ID = "mpayNotificationId";
	private static final String CHANNEL_CODE = "channelCode";
	private static final String TENANT_ID = "tenantId";

	private JfwHelper() {
	}

	public static void sendToActiveMQ(MPAY_FinancialIntegMsg financialIntegMsg, String tenantID, String viewName, String queueName) throws WorkflowException {

		Map<String, String> headers = createGenericHeader(tenantID, viewName);

		MpClearIntegMsg oMpClearIntegMsg = new MpClearIntegMsg();
		oMpClearIntegMsg.setContent(financialIntegMsg.getRefMsgLog().getContent());
		oMpClearIntegMsg.setRefSender(financialIntegMsg.getRefMsgLog().getRefSender());
		oMpClearIntegMsg.setSigningStamp(financialIntegMsg.getRefMsgLog().getSigningStamp());
		oMpClearIntegMsg.setToken(financialIntegMsg.getRefMsgLog().getToken());
		String messageBody = MessageSerializer.serialize(oMpClearIntegMsg);

		sendJMSMessage(messageBody, headers, queueName);
	}

	private static Map<String, String> createGenericHeader(String tenantID, String viewName) {
		Map<String, String> headers = new HashMap<>();
		headers.put(ComponentOption.TENANT_ID.getName(), tenantID);
		headers.put(ComponentOption.VIEW_NAME.getName(), viewName);
		return headers;
	}

	public static void sendToActiveMQ(MPAY_Notification notification, String tenantID, String viewName, String queueName) {

		Map<String, String> headers = createGenericHeader(tenantID, viewName);

		headers.put(MPAY_REF_MESSAGE_ID, String.valueOf(notification.getRefMessage()==null?null:notification.getRefMessage().getId()));
		headers.put(CHANNEL_CODE, notification.getRefChannel().getCode());
		headers.put(TENANT_ID, AppContext.getCurrentTenant());

		NotificationRequest notificationRequest = new NotificationRequest();
		notificationRequest.setContent(notification.getContent());
		notificationRequest.setReciever(notification.getReceiver());
		notificationRequest.setSender("");
		notificationRequest.setExtra1(notification.getExtraData1());
		notificationRequest.setExtra2(notification.getExtraData2());
		notificationRequest.setExtra3(notification.getExtraData3());
		try {
			Marshaller marshaller = JaxbHandler.getJaxbContextInstance().createMarshaller();
			StringWriter writer = new StringWriter();
			marshaller.marshal(notificationRequest, writer);
			sendJMSMessage(writer.toString(), headers, queueName);
		} catch (Exception e) {
			throw new InvalidOperationException("Error while sendToActiveMQ", e);
		}
	}

	public static void sendToActiveMQ(MPAY_NetworkManIntegMsg mpayNetworkManIntegMsg, String tenantID, String viewName, String queueName) throws WorkflowException {

		Map<String, String> headers = createGenericHeader(tenantID, viewName);

		MpClearIntegMsg oMpClearIntegMsg = new MpClearIntegMsg();
		oMpClearIntegMsg.setContent(mpayNetworkManIntegMsg.getRefMsgLog().getContent());
		oMpClearIntegMsg.setRefSender(mpayNetworkManIntegMsg.getRefMsgLog().getRefSender());
		oMpClearIntegMsg.setSigningStamp(mpayNetworkManIntegMsg.getRefMsgLog().getSigningStamp());
		oMpClearIntegMsg.setToken(mpayNetworkManIntegMsg.getRefMsgLog().getToken());
		String messageBody = MessageSerializer.serialize(oMpClearIntegMsg);

		sendJMSMessage(messageBody, headers, queueName);
	}

	public static void sendToActiveMQ(MPAY_CorpIntegMessage corpIntegMessage, String tenantID, String viewName, String queueName) throws WorkflowException {

		Map<String, String> headers = createGenericHeader(tenantID, viewName);

		MpClearIntegMsg oMpClearIntegMsg = new MpClearIntegMsg();
		oMpClearIntegMsg.setContent(corpIntegMessage.getRefMsgLog().getContent());
		oMpClearIntegMsg.setRefSender(corpIntegMessage.getRefMsgLog().getRefSender());
		oMpClearIntegMsg.setSigningStamp(corpIntegMessage.getRefMsgLog().getSigningStamp());
		oMpClearIntegMsg.setToken(corpIntegMessage.getRefMsgLog().getToken());
		String messageBody = MessageSerializer.serialize(oMpClearIntegMsg);

		sendJMSMessage(messageBody, headers, queueName);
	}

	public static void sendToActiveMQ(MPAY_CustIntegMessage mpayCustIntegMessage, String tenantID, String viewName, String queueName) throws WorkflowException {

		Map<String, String> headers = createGenericHeader(tenantID, viewName);

		MpClearIntegMsg oMpClearIntegMsg = new MpClearIntegMsg();
		oMpClearIntegMsg.setContent(mpayCustIntegMessage.getRefMsgLog().getContent());
		oMpClearIntegMsg.setRefSender(mpayCustIntegMessage.getRefMsgLog().getRefSender());
		oMpClearIntegMsg.setSigningStamp(mpayCustIntegMessage.getRefMsgLog().getSigningStamp());
		oMpClearIntegMsg.setToken(mpayCustIntegMessage.getRefMsgLog().getToken());
		String messageBody = MessageSerializer.serialize(oMpClearIntegMsg);

		sendJMSMessage(messageBody, headers, queueName);
	}

	public static void sendToActiveMQ(MPAY_Intg_Reg_File file, boolean isFileLevel) {
		String messageBody = Long.toString(file.getId());
		Map<String, String> headers = createGenericHeader(file.getTenantId(), MPAYView.FILE_INTG_REGISTRATION.viewName);
		headers.put(ComponentOption.TOKEN.getName(), Boolean.toString(isFileLevel));

		sendJMSMessage(messageBody, headers, "mpay.corpregistration.file");
	}

	private static void sendJMSMessage(String messageBody, Map<String, String> headers, String requestQueueName) {
		JfwMessage message = new JfwMessage();
		message.setBody(messageBody);
		message.setHeaders(headers);

		CommunicationConfig commConfig = new CommunicationConfig();
		commConfig.setRequestQueueName(requestQueueName);

		Communicator communicator = AppContext.getApplicationContext().getBean(JFW_COMMUNICATOR, Communicator.class);
		communicator.asyncSend(commConfig, message);
	}

	private static JfwFacade getJfwFacade() {
		return AppContext.getApplicationContext().getBean("defaultJfwFacade", JfwFacade.class);
	}

	public static void createEntityWithServiceUser(MPAYView view, JFWEntity entity) throws InvalidActionException {
		createEntity(view, entity);
	}

	public static void createEntity(MPAYView view, JFWEntity entity) throws InvalidActionException {
		Map<Option, Object> options = new EnumMap<>(Option.class);
		if (AppContext.getCurrentRootOrg() != null)
			options.put(Option.ORG_SHORT_NAME, AppContext.getCurrentRootOrg().getShortName());
		createEntity(view, entity, new HashMap<>(), options);
	}

	private static void createEntity(MPAYView view, JFWEntity entity, Map<String, Object> arguments, Map<Option, Object> options) throws InvalidActionException {
		if (AppContext.getCurrentRootOrg() != null)
			options.put(Option.ORG_SHORT_NAME, AppContext.getCurrentRootOrg().getShortName());
		JfwFacade facade = getJfwFacade();
		facade.createEntity(view.viewName, entity, arguments, options);
	}

	public static void createEntity(MPAYView view, JFWEntity entity, Map<String, Object> workFlowInputs) throws InvalidActionException {
		Map<Option, Object> options = new EnumMap<>(Option.class);
		if (AppContext.getCurrentRootOrg() != null)
			options.put(Option.ORG_SHORT_NAME, AppContext.getCurrentRootOrg().getShortName());
		JfwFacade facade = getJfwFacade();
		facade.createEntity(view.viewName, entity, workFlowInputs, options);
	}

	public static void executeActionWithServiceUser(MPAYView view, Long id, String actionName) throws WorkflowException {
		executeAction(view, id, actionName);
	}

	public static void executeAction(MPAYView view, Long id, String actionName) throws WorkflowException {
		executeAction(view, id, actionName, false);
	}

	public static void executeAction(MPAYView view, Long id, String actionName, boolean includeDraft) throws WorkflowException {
		JfwFacade facade = getJfwFacade();

		try {
			List<Filter> filters = new ArrayList<>();
			filters.add(Filter.get("id", Operator.EQ, id));
			Map<Option, Object> options = new EnumMap<>(Option.class);
			options.put(Option.ACTION_NAME, actionName);
			options.put(Option.INCLUDE_DRAFTS, includeDraft);
			facade.executeAction(view.viewName, null, filters, new Hashtable<>(), options);
		} catch (BusinessException | SecurityException | IllegalArgumentException e) {
			throw new WorkflowException(e);
		}
	}

	public static void executeAction(MPAYView view, Long id, String actionName, Map<String, Object> transientVars) throws WorkflowException {
		JfwFacade facade = getJfwFacade();
		try {
			List<Filter> filters = new ArrayList<>();
			filters.add(Filter.get("id", Operator.EQ, id));
			Map<Option, Object> options = new EnumMap<>(Option.class);
			options.put(Option.ACTION_NAME, actionName);
			facade.executeAction(view.viewName, null, filters, transientVars, options);
		} catch (BusinessException | SecurityException | IllegalArgumentException e) {
			throw new WorkflowException(e);
		}
	}

	public static <T extends Exception> void executeActionWithServiceUser(MPAYView view, JFWEntity entity, String actionName, T ex) throws T {
		executeAction(view, entity, actionName, ex);
	}

	public static <T extends Exception> void executeAction(MPAYView view, JFWEntity entity, String actionName, T ex) throws T {
		executeAction(view, entity, actionName, null, null, ex);
	}

	public static void executeActionWithServiceUser(MPAYView view, JFWEntity entity, Map<Option, Object> options) throws WorkflowException {
		executeAction(view, entity, options);
	}

	public static void executeAction(MPAYView view, JFWEntity entity, Map<Option, Object> options) throws WorkflowException {
		JfwFacade facade = getJfwFacade();
		try {
			facade.executeAction(view.viewName, null, entity, new HashMap<>(), options);
		} catch (BusinessException e) {
			throw new WorkflowException(e);
		}
	}

	public static <T extends Exception> void executeActionWithServiceUser(MPAYView view, JFWEntity entity, String actionName, Map<String, Object> arguments, Map<Option, Object> options, T ex) throws T {
		executeAction(view, entity, actionName, arguments, options, ex);
	}

	public static <T extends Exception> void executeAction(MPAYView view, JFWEntity entity, String actionName, Map<String, Object> arguments, Map<Option, Object> options, T ex) throws T {
		JfwFacade facade = getJfwFacade();
		try {
			Map<Option, Object> innerOptions = options;
			Map<String, Object> innerArguments = arguments;
			Method method = entity.getClass().getMethod("getId");
			Long id = (Long) method.invoke(entity);
			List<Filter> filters = new ArrayList<>();
			filters.add(Filter.get("id", Operator.EQ, id));
			if (null == innerOptions)
				innerOptions = new Hashtable<>();
			if (null == innerArguments)
				innerArguments = new HashMap<>();

			innerOptions.put(Option.ACTION_NAME, actionName);
			innerOptions.put(Option.INCLUDE_DRAFTS, Boolean.TRUE);
			facade.executeAction(view.viewName, null, filters, innerArguments, innerOptions);

		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | BusinessException ex1) {
			throw createExp(ex1, ex);
		}
	}

	public static <T extends Exception> void createEntityWithServiceUser(MPAYView view, JFWEntity entity, T ex) throws T {
		createEntity(view, entity, ex);
	}

	public static <T extends Exception> void createEntity(MPAYView view, JFWEntity entity, T ex) throws T {
		try {
			createEntity(view, entity, new HashMap<>(), new HashMap<>());
		} catch (BusinessException | InvalidActionException ex1) {
			throw createExp(ex1, ex);
		}
	}

	@SuppressWarnings("unchecked")
	private static <T extends Exception> T createExp(Exception ex, T exType) {
		try {
			Constructor<T> c = (Constructor<T>) exType.getClass().getConstructor(String.class);
			T exp = c.newInstance(((exType.getMessage() == null || exType.getMessage().isEmpty()) ? "" : (exType.getMessage() + " : ")) + ex.getMessage());
			exp.setStackTrace(ex.getStackTrace());
			return exp;
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new InvalidOperationException("Error while createExp", e);
		}
	}

	public static Object createObjectFromDraft(String draftData, Object targetObjet, ItemDao itemDao) {
		JSONDeserializer<Map<String, Object>> deserilazer = new JSONDeserializer<>();
		Map<String, Object> propertiesMap = deserilazer.deserialize(draftData);
		Object newJpaEntity;
		try {
			Class<?> entity = Class.forName(targetObjet.getClass().getName());
			newJpaEntity = entity.newInstance();
			ItemUtils.applyProperties(newJpaEntity, propertiesMap, itemDao);
		} catch (NoSuchMethodException | InstantiationException | InvocationTargetException | NoSuchFieldException | IllegalAccessException | ClassNotFoundException e) {
			throw new InvalidOperationException("Error while createObjectFromDraft", e);
		}
		return newJpaEntity;

	}

	public static void updateWorkflowStep(String stepKey, long workflowId, EntityManager entityManager) {
		String updateStatement = "UPDATE JPACurrentStep s SET s.stepId = " + stepKey + " WHERE s.workflowEntry.id = :workflowId";
		Query updateQuery = entityManager.createQuery(updateStatement);
		updateQuery.setParameter("workflowId", workflowId);
		updateQuery.executeUpdate();
	}
}
