package com.progressoft.mpay.limits;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;
import com.progressoft.mpay.registration.customers.CustomerMobileHandler;

public class CheckLimitSchemeModification implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(CustomerMobileHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside CheckLimitSchemeModification*******************-----------------");

		Object entity = changeHandlerContext.getEntity();
		if (entity instanceof MPAY_Limit) {
			MPAY_Limit limit = (MPAY_Limit) changeHandlerContext.getEntity();
			if (limit.getRefScheme() != null && "102105".equals(limit.getRefScheme().getStatusId().getCode())) {
				changeHandlerContext.getFieldsAttributes().get("isActive").setEnabled(false);
				changeHandlerContext.getFieldsAttributes().get("description").setEnabled(false);
				changeHandlerContext.getFieldsAttributes().get("refType").setEnabled(false);
			}
		} else if (entity instanceof MPAY_LimitsDetail) {
			MPAY_LimitsDetail details = (MPAY_LimitsDetail) changeHandlerContext.getEntity();
			if (details.getRefLimit() != null && details.getRefLimit().getRefScheme() != null && "102105".equals(details.getRefLimit().getRefScheme().getStatusId().getCode())) {
				changeHandlerContext.getFieldsAttributes().get("isActive").setEnabled(false);
				changeHandlerContext.getFieldsAttributes().get("msgType").setEnabled(false);
				changeHandlerContext.getFieldsAttributes().get("txCountLimit").setEnabled(false);
				changeHandlerContext.getFieldsAttributes().get("txAmountLimit").setEnabled(false);
			}
		}
	}
}
