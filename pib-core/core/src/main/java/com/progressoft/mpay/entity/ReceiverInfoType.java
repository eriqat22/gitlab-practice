package com.progressoft.mpay.entity;

public final class ReceiverInfoType {
    public static final String MOBILE = "M";
    public static final String ALIAS = "A";
    public static final String CORPORATE = "C";
    public static final String MP_CLEAR_ALIAS = "MA";
    public static final String IBAN = "I";

    private ReceiverInfoType() {
    }
}
