package com.progressoft.mpay.registration.corporates;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.MPAYView;

public class PostDeleteCorporateMPClearApproval implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostDeleteCorporateMPClearApproval.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =====");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		corporate.setIsRegistered(false);
		corporate.setIsActive(false);
		deleteCorporateServices(corporate);
	}

	private void deleteCorporateServices(MPAY_Corporate corporate) throws WorkflowException {
		logger.debug("Inside DeleteCorporateServices =====");

		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId());
		if (services == null || services.isEmpty())
			return;
		for (MPAY_CorpoarteService service : services) {
			if (service.getDeletedFlag() == null || !service.getDeletedFlag()) {
				JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.DELETE, new WorkflowException());
			}
		}
	}
}