package com.progressoft.mpay.webclient;


import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.webclient.model.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;


public interface RestAPI {

    @POST
    Call<ResponseModel> getResponse(@Url String url, @Query("token") String token, @Body MPayRequest body);
}
