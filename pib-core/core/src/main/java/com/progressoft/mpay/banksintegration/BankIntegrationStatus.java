package com.progressoft.mpay.banksintegration;

public class BankIntegrationStatus {
    public static final String NONE = "0";
    public static final String ACCEPTED = "1";
    public static final String REJECTED = "2";
    public static final String PENDING = "3";
    public static final String FAILED = "4";

    private BankIntegrationStatus() {
    }
}
