package com.progressoft.mpay.mpclear;

import java.util.List;

import javax.xml.transform.Source;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.notifications.NotificationHelper;

public class MPClearMessageHandler implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(MPClearMessageHandler.class);

	public static final String SERVICE_USER = "SERVICE_USER";

	@SuppressWarnings("rawtypes")
	@Override
	public void process(Exchange exchange) throws Exception {
		boolean serviceUserEnabled = false;
		try {
			logger.debug("Inside MPClearMessageHandler ....");
			serviceUserEnabled = IntegrationUtils.enableServiceUser((String) exchange.getProperty("tenantId"));
			CxfPayload requestPayload = exchange.getIn().getBody(CxfPayload.class);
			if (requestPayload == null)
				throw new InvalidArgumentException("exchange.getIn().getBody(CxfPayload.class) == null");
			List inElements = requestPayload.getBodySources();

			Element messageElements = new XmlConverter().toDOMElement((Source) inElements.get(0));

			if ((messageElements.getChildNodes() == null) || (messageElements.getChildNodes().getLength() != 4)) {
				throw new InvalidArgumentException("Invalid argument input for send operation!");
			}

			logger.debug("msgEl.getChildNodes().item(0).getTextContent() {}", messageElements.getChildNodes().item(0).getTextContent());
			logger.debug("msgEl.getChildNodes().item(1).getTextContent() {}", messageElements.getChildNodes().item(1).getTextContent());
			logger.debug("msgEl.getChildNodes().item(2).getTextContent() {}", messageElements.getChildNodes().item(2).getTextContent());
			logger.debug("msgEl.getChildNodes().item(3).getTextContent() {}", messageElements.getChildNodes().item(3).getTextContent());

			MPClearRequestProcessor processor = new MPClearRequestProcessor(
					DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance(), new MPayCryptographer(), new NotificationHelper(), MPayCommunicatorHelper.getCommunicator());
			processor.processRequest(messageElements.getChildNodes().item(0).getTextContent(),
							messageElements.getChildNodes().item(1)
				.getTextContent(), messageElements.getChildNodes().item(2).getTextContent(),
							messageElements.getChildNodes().item(3).getTextContent());
		} catch (Exception ex) {
			logger.error("Error while ProcessMPClearMessage message", ex);
		} finally {
			if (serviceUserEnabled) {
				IntegrationUtils.disableServiceUser();
			}
		}
	}
}
