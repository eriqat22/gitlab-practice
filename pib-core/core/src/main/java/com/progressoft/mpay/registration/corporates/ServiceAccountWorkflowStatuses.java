package com.progressoft.mpay.registration.corporates;

public enum ServiceAccountWorkflowStatuses {
	APPROVAL("201002"), REJECTED("201003"), MODIFICATION("201004"), APPROVED("201005"), DELETED("201006"), INTEGRATION("201007"), DELETING("201008"), MODIFIED("201010"), CANCELING("201011"), SUSPENDED("201014"),DORMANT("101017");

	private final String status;

	ServiceAccountWorkflowStatuses(String value) {
		this.status = value;
	}

	@Override
	public String toString() {
		return this.status;
	}
}
