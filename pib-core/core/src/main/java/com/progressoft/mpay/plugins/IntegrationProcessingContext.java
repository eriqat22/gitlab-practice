package com.progressoft.mpay.plugins;

import com.progressoft.mpay.ICryptographer;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.notifications.INotificationHelper;
import com.solab.iso8583.IsoMessage;

public class IntegrationProcessingContext extends ProcessingContext {
	private MPAY_MpClearIntegMsgLog mpClearMessage;
	private MPAY_MpClearIntegMsgLog originalMPClearMessage;
	private IsoMessage mpClearIsoMessage;
	private MPAY_MpClearIntegMsgLog mpClearResponse;
	private boolean isRequest;

	public IntegrationProcessingContext(IDataProvider dataProvider, ILookupsLoader lookupsLoader, ISystemParameters systemParameters, ICryptographer cryptographer, INotificationHelper notificationHelper) {
		super(dataProvider, lookupsLoader, systemParameters, cryptographer, notificationHelper);
	}

	public IntegrationProcessingContext(MPClearProcessingContext mpClearContext) {
		super(mpClearContext.getDataProvider(), mpClearContext.getLookupsLoader(), mpClearContext.getSystemParameters(), mpClearContext.getCryptographer(), mpClearContext.getNotificationHelper());
		this.mpClearMessage = mpClearContext.getMessage();
		this.mpClearIsoMessage = mpClearContext.getIsoMessage();
		this.originalMPClearMessage = mpClearContext.getOriginalMessage();
		this.isRequest = mpClearContext.getMessageType().getIsRequest();
		setTransactionConfig(mpClearContext.getTransactionConfig());
		setOperation(mpClearContext.getOperation());
		setTransactionNature(mpClearContext.getTransactionNature());
		if (mpClearContext.getOriginalMessage() != null)
			setMessage(mpClearContext.getOriginalMessage().getRefMessage());
	}

	public MPAY_MpClearIntegMsgLog getMpClearMessage() {
		return mpClearMessage;
	}

	public void setMpClearMessage(MPAY_MpClearIntegMsgLog mpClearMessage) {
		this.mpClearMessage = mpClearMessage;
	}

	public MPAY_MpClearIntegMsgLog getOriginalMPClearMessage() {
		return originalMPClearMessage;
	}

	public void setOriginalMPClearMessage(MPAY_MpClearIntegMsgLog originalMPClearMessage) {
		this.originalMPClearMessage = originalMPClearMessage;
	}

	public IsoMessage getMpClearIsoMessage() {
		return mpClearIsoMessage;
	}

	public void setMpClearIsoMessage(IsoMessage mpClearIsoMessage) {
		this.mpClearIsoMessage = mpClearIsoMessage;
	}

	public MPAY_MpClearIntegMsgLog getMpClearResponse() {
		return mpClearResponse;
	}

	public void setMpClearResponse(MPAY_MpClearIntegMsgLog mpClearResponse) {
		this.mpClearResponse = mpClearResponse;
	}

	public boolean isRequest() {
		return isRequest;
	}

	public void setRequest(boolean isRequest) {
		this.isRequest = isRequest;
	}
}
