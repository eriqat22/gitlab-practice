package com.progressoft.mpay;

import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.jfw.job.context.JobsContextHolder;
import com.progressoft.jfw.model.bussinessobject.security.Tenant;
import com.progressoft.jfw.model.service.utils.AppContext;

public class ServiceUserManager implements IServiceUserManager {

	@Override
	public boolean enableServiceUser(String tenantId) {
		return IntegrationUtils.enableServiceUser(tenantId);
	}

	@Override
	public void disableServiceUser() {
		IntegrationUtils.disableServiceUser();
	}

	@Override
	public String getUserName() {
		String userName;
		Tenant serviceTanant = getServiceTenant();
		if (serviceTanant == null) {
			userName = AppContext.getCurrentUser().getUsername();
		} else {
			userName = JobsContextHolder.getContext().getUserName();
		}
		return userName;
	}

	@Override
	public Tenant getServiceTenant() {
		Tenant serviceTenant = JobsContextHolder.getContext().getTenant();
		if (serviceTenant == null) {
			serviceTenant = AppContext.getCurrentTenantObject();
		}
		return serviceTenant;
	}
}
