package com.progressoft.mpay.payments.cashout;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CashOut;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.tax.TaxCalculator;

public class CashOutIsChargeIncluded implements ChangeHandler {

	private static final String REF_MOBILE_ACCOUNT = "refMobileAccount";
	private static final Logger logger = LoggerFactory.getLogger(CashOutIsChargeIncluded.class);
	public static final String RECALCULATESTEP = "100606";

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("***inside CashOutIsChargeIncluded : " + changeHandlerContext.getSource());

		MPAY_CashOut cashOut = (MPAY_CashOut) changeHandlerContext.getEntity();
		if (cashOut.getAmount() != null && cashOut.getAmount().doubleValue() > 0 && cashOut.getRefCustMob() != null) {
			MPAY_MobileAccount account = SystemHelper.getMobileAccount(MPayContext.getInstance().getSystemParameters(), cashOut.getRefCustMob(), cashOut.getRefMobileAccount());
			MPAY_Profile profile = account.getRefProfile();
			BigDecimal chargeAmount = ChargesCalculator.calculateCharge(MPayContext.getInstance().getDataProvider(), cashOut.getAmount(), MessageCodes.CO.toString(), profile, true);
			BigDecimal taxAmount = TaxCalculator.calculateTax(MPayContext.getInstance().getDataProvider(), chargeAmount, MessageCodes.CO.toString(), profile, MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
			cashOut.setTransAmount(cashOut.getAmount());
			cashOut.setChargeAmount(chargeAmount);
			cashOut.setTaxAmount(taxAmount);
			cashOut.setTotalAmount(cashOut.getAmount());
		}
		if (cashOut.getRefCustMob() != null && cashOut.getStatusId() !=null &&cashOut.getStatusId().getCode().equals(RECALCULATESTEP) ) {
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setRequired(false);
		}else if (cashOut.getRefCustMob() != null) {
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setEnabled(true);
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setRequired(true);
		} else {
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(REF_MOBILE_ACCOUNT).setRequired(false);
			cashOut.setRefMobileAccount(null);
		}
	}

}