package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;

@SuppressWarnings("rawtypes")
public class SetCorporateDeviceFlagToFalse implements FunctionProvider {
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		MPAY_CorporateDevice corporateDevice = (MPAY_CorporateDevice) transientVars.get(WorkflowService.WF_ARG_BEAN);
		corporateDevice.setActiveDevice(false);
	}
}
