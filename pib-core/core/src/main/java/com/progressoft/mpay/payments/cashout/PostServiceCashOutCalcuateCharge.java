package com.progressoft.mpay.payments.cashout;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entities.MPAY_ServiceCashOut;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.payments.CashRequestHelper;
import com.progressoft.mpay.tax.TaxCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Map;

public class PostServiceCashOutCalcuateCharge implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostServiceCashOutCalcuateCharge.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside execute =====================");
        MPAY_ServiceCashOut serviceCashOut = (MPAY_ServiceCashOut) arg0.get(WorkflowService.WF_ARG_BEAN);
        if (serviceCashOut.getAmount() != null && serviceCashOut.getAmount().doubleValue() > 0 && serviceCashOut.getRefCorporateService() != null && serviceCashOut.getRefServiceAccount() != null) {
            String refAccount = CashRequestHelper.prepareRefMobileAccount(serviceCashOut.getRefServiceAccount());
            serviceCashOut.setRefServiceAccount(refAccount);
            MPAY_ServiceAccount account = SystemHelper.getServiceAccount(MPayContext.getInstance().getSystemParameters(), serviceCashOut.getRefCorporateService(), refAccount);
            if (account == null)
                return;
            MPAY_Profile profile = account.getRefProfile();
            BigDecimal chargeAmount = ChargesCalculator.calculateCharge(MPayContext.getInstance().getDataProvider(), serviceCashOut.getAmount(), MessageCodes.CO.toString(), profile, true);
            BigDecimal taxAmount = TaxCalculator.calculateTax(MPayContext.getInstance().getDataProvider(), chargeAmount, MessageCodes.CO.toString(), profile, MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
            serviceCashOut.setTransAmount(serviceCashOut.getAmount());
            serviceCashOut.setChargeAmount(chargeAmount);
            serviceCashOut.setTaxAmount(taxAmount);
            serviceCashOut.setTotalAmount(serviceCashOut.getAmount().add(chargeAmount).add(taxAmount));
        }
    }
}