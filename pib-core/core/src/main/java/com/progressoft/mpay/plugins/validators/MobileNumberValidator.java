package com.progressoft.mpay.plugins.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class MobileNumberValidator {
	private static final Logger logger = LoggerFactory.getLogger(MobileNumberValidator.class);

	private MobileNumberValidator() {

	}

	public static ValidationResult validate(ProcessingContext context, String mobileNumber) {
		logger.debug("Inside Validate ...");
		if(context == null)
			throw new NullArgumentException("context");

		if (!SystemHelper.validateMobielNumber(context.getLookupsLoader(), mobileNumber))
			return new ValidationResult(ReasonCodes.INVALID_MOBILE_NUMBER, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
