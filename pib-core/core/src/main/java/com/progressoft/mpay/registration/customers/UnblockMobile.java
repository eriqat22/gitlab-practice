package com.progressoft.mpay.registration.customers;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class UnblockMobile implements FunctionProvider {

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVar, Map arg1, PropertySet arg2) throws WorkflowException {
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVar.get(WorkflowService.WF_ARG_BEAN);
		mobile.setIsBlocked(false);
		mobile.setRetryCount(0L);
	}
}
