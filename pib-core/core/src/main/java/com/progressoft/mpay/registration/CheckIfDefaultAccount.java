package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class CheckIfDefaultAccount implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(CheckIfDefaultAccount.class);
	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object account = transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (account instanceof MPAY_MobileAccount)
			return !((MPAY_MobileAccount) account).getIsDefault();
		else
			return !((MPAY_ServiceAccount) account).getIsDefault();
	}
}