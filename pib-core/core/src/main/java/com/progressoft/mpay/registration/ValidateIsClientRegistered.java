package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class ValidateIsClientRegistered implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(ValidateIsClientRegistered.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object sender = transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (sender instanceof MPAY_CustomerMobile)
			return ((MPAY_CustomerMobile) sender).getRefCustomer().getIsRegistered();
		else if (sender instanceof MPAY_CorpoarteService)
			return ((MPAY_CorpoarteService) sender).getRefCorporate().getIsRegistered();
		if (sender instanceof MPAY_Customer)
			return ((MPAY_Customer) sender).getIsRegistered();
		else if (sender instanceof MPAY_Corporate)
			return ((MPAY_Corporate) sender).getIsRegistered();
		else
			throw new WorkflowException("Invalid Object Received");
	}
}