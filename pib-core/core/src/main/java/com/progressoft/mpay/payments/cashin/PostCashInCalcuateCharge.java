package com.progressoft.mpay.payments.cashin;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.payments.CashRequestHelper;
import com.progressoft.mpay.tax.TaxCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Map;

public class PostCashInCalcuateCharge implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCashInCalcuateCharge.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("***inside execute");
        MPAY_CashIn cashIn = (MPAY_CashIn) arg0.get(WorkflowService.WF_ARG_BEAN);

        if (cashIn.getAmount() != null && cashIn.getAmount().doubleValue() > 0 && cashIn.getRefCustMob() != null) {
            String refAccount = CashRequestHelper.prepareRefMobileAccount(cashIn.getRefMobileAccount());
            cashIn.setRefMobileAccount(refAccount);
            MPAY_MobileAccount account = SystemHelper.getMobileAccount(MPayContext.getInstance().getSystemParameters(), cashIn.getRefCustMob(), refAccount);
            if (account == null)
                return;
            MPAY_Profile profile = account.getRefProfile();
            BigDecimal chargeAmount = ChargesCalculator.calculateCharge(MPayContext.getInstance().getDataProvider(), cashIn.getAmount(), MessageCodes.CI.toString(), profile, false);
            BigDecimal taxAmount = TaxCalculator.calculateTax(MPayContext.getInstance().getDataProvider(), chargeAmount, MessageCodes.CI.toString(), profile, MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
            cashIn.setTransAmount(cashIn.getAmount());
            cashIn.setChargeAmount(chargeAmount);
            cashIn.setTaxAmount(taxAmount);
            cashIn.setTotalAmount(cashIn.getAmount().add(chargeAmount).add(taxAmount));
        }

    }

    private String prepareRefMobileAccount(String refMobileAccount) {
        if (refMobileAccount.contains("{value={value=")) {
            String s = refMobileAccount.split(",")[0];
            return s.replace("{value={value=", "");
        }
        if (refMobileAccount.contains("key") || refMobileAccount.contains("value")) {
            String[] split = refMobileAccount.split("=");
            return split[1].replace(",", "").replace("key", "");
        }
        return refMobileAccount;
    }
}