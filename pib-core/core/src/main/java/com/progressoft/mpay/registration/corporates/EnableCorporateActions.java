package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class EnableCorporateActions implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(EnableCorporateActions.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_Corporate corporate = (MPAY_Corporate) transientVar.get(WorkflowService.WF_ARG_BEAN);
		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId());
		if (services.size() > 1) {
			MPAY_CorpoarteService newService = selectFirst(services, having(on(MPAY_CorpoarteService.class).getIsRegistered(), Matchers.equalTo(false)));
			if (newService != null)
				return false;
		}
		Iterator<MPAY_CorpoarteService> servicesIterator = services.iterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			List<MPAY_ServiceAccount> accounts = DataProvider.instance().listServiceAccounts(service.getId());
			if(accounts.size() == 1)
				continue;
			MPAY_ServiceAccount newAccount = selectFirst(accounts, having(on(MPAY_ServiceAccount.class).getIsRegistered(), Matchers.equalTo(false)));
			if (newAccount != null)
				return false;
		} while (servicesIterator.hasNext());

		return true;
	}
}
