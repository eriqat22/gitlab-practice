package com.progressoft.mpay.migs.mvc.controller;

import com.progressoft.mpay.migs.mvc.url.URLBuilder;

public class MigsRequestURLBuilder {

	public String buildMigsURL(MigsContext context) {
		return MIGSParameters.REDIRECT + prepareUrlMigsRequest(context);
	}

	private String prepareUrlMigsRequest(MigsContext context) {
		URLBuilder urlBuilder = new URLBuilder(context.getSystemParameters().getMigsUrl());
		urlBuilder.addParameter(MIGSParameters.VPC_ACCESS_CODE, context.getMigsRequest().getServiceType().getVpcAccessCode());
		urlBuilder.addParameter(MIGSParameters.VPC_VERSION, context.getSystemParameters().getMigsVpcVersion());
		urlBuilder.addParameter(MIGSParameters.VPC_ORDER_INFO, context.getMigsRequest().getVpcOrderInfo());
		urlBuilder.addParameter(MIGSParameters.VPC_COMMAND, context.getSystemParameters().getMigsVpcCommand());
		urlBuilder.addParameter(MIGSParameters.VPC_LOCALE, context.getMigsRequest().getVpcLocale());
		urlBuilder.addParameter(MIGSParameters.VPC_MERCHANT, context.getMigsRequest().getServiceType().getVpcMerchant());
		urlBuilder.addParameter(MIGSParameters.VPC_AMOUNT, context.getMigsRequest().getVpcAmount());
		urlBuilder.addParameter(MIGSParameters.VPC_SECURE_HASH, context.getMigsRequest().getVpcSecureHash());
		urlBuilder.addParameter(MIGSParameters.VPC_CURRENCY, MIGSParameters.EGP);
		urlBuilder.addParameter(MIGSParameters.VPC_RETURN_URL, context.getSystemParameters().getMigsVpcRedirectUrl());
		urlBuilder.addParameter(MIGSParameters.VPC_SECURE_HASH_TYPE, MIGSParameters.SHA256);
		urlBuilder.addParameter(MIGSParameters.VPC_MERCH_TXN_REF, context.getMigsRequest().getVpcMerchTxnRef());
		return urlBuilder.toString();
	}
}
