package com.progressoft.mpay.psp;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Corporate;

public class ValidatePSPDetailsCount implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidatePSPDetailsCount.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("***inside ValidatePSPDetailsCount");

		StringBuilder sbError = new StringBuilder();

		MPAY_Corporate psp = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
//		validatePSPDetails(sbError, psp);

		if (sbError.length() > 0) {
			InvalidInputException iie = new InvalidInputException();
			iie.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS, sbError.toString());
			throw iie;
		}
	}

//	private void validatePSPDetails(StringBuilder sbError, MPAY_Corporate psp) {
//		if (psp.getRefPSPPspDetails() == null || psp.getRefPSPPspDetails().isEmpty()) {
//			sbError.append("PSP should have Details defined.");
//		}
//
//		if (psp.getRefPSPPspDetails() != null && psp.getRefPSPPspDetails().size() > 1) {
//				sbError.append("PSP can only have one detail defined.");
//		}
//	}
}
