package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entity.MPAYView;

public class HandleCustomerCancel implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(HandleCustomerCancel.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute ====================");
		MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);
		if (customer.getIsRegistered())
			JfwHelper.executeAction(MPAYView.CUSTOMER, customer, CustomerWorkflowServiceActions.APPROVE, new WorkflowException());
		else
			JfwHelper.executeAction(MPAYView.CUSTOMER, customer, CustomerWorkflowServiceActions.REJECT, new WorkflowException());
	}
}
