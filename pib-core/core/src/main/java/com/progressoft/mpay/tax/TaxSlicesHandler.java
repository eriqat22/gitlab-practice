package com.progressoft.mpay.tax;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_TaxSlice;

public class TaxSlicesHandler implements ChangeHandler {

    private static final Logger logger = LoggerFactory.getLogger(TaxSlicesHandler.class);
    private static final String TX_AMOUNT_LIMIT = "txAmountLimit";
    private static final String TAX_TYPE = "taxType";
    private static final String TAX_PERCENT = "taxPercent";
    private static final String MIN_AMOUNT = "minAmount";
    private static final String MAX_AMOUNT = "maxAmount";

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        MPAY_TaxSlice slice = (MPAY_TaxSlice) changeHandlerContext.getEntity();
        logger.debug("inside TaxSlicesHandler ----------------------------");

        if (slice.getRefTaxDetails().getMsgType().getIsFinancial()) {
        	changeHandlerContext.getFieldsAttributes().get(TX_AMOUNT_LIMIT).setVisible(true);
            changeHandlerContext.getFieldsAttributes().get(TX_AMOUNT_LIMIT).setRequired(true);
            changeHandlerContext.getFieldsAttributes().get(TAX_TYPE).setVisible(true);
            changeHandlerContext.getFieldsAttributes().get(TAX_PERCENT).setRequired(true);
            changeHandlerContext.getFieldsAttributes().get(MIN_AMOUNT).setRequired(true);
            changeHandlerContext.getFieldsAttributes().get(MAX_AMOUNT).setRequired(true);
        } else {
        	changeHandlerContext.getFieldsAttributes().get(TX_AMOUNT_LIMIT).setVisible(false);
            changeHandlerContext.getFieldsAttributes().get(TX_AMOUNT_LIMIT).setRequired(false);
            changeHandlerContext.getFieldsAttributes().get(TAX_TYPE).setVisible(false);
            changeHandlerContext.getFieldsAttributes().get(TAX_PERCENT).setRequired(false);
            changeHandlerContext.getFieldsAttributes().get(MIN_AMOUNT).setRequired(false);
            changeHandlerContext.getFieldsAttributes().get(MAX_AMOUNT).setRequired(false);
            slice.setTaxPercent(BigDecimal.ZERO);
            slice.setMinAmount(BigDecimal.ZERO);
            slice.setMaxAmount(BigDecimal.ZERO);
            slice.setTxAmountLimit(BigDecimal.ZERO);
        }
        TaxTypeSliceHandlerHelper.handleTaxType(changeHandlerContext.getFieldsAttributes(), slice);
    }
}
