package com.progressoft.mpay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.Filter;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.Operator;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;

public class Unique {
	private Unique() {
	}

	public static InvalidInputException validate(ItemDao itemDao, JFWEntity entity, String property, Object value,
												 String errorMsgKey) {
		InvalidInputException iie = new InvalidInputException();
		return validate(itemDao, iie, entity, property, value, errorMsgKey);
	}

	public static InvalidInputException validate(ItemDao itemDao, InvalidInputException iie, JFWEntity entity, String property, Object value,
	        String errorMsgKey) {
		Map<String, Object> properties = new HashMap<>();
		properties.put(property, value);
		return validate(itemDao, iie, entity, properties, errorMsgKey);
	}

	public static InvalidInputException validate(ItemDao itemDao, JFWEntity entity, Map<String, Object> properties,
												 String errorMsgKey) {
		InvalidInputException iie = new InvalidInputException();
		return validate(itemDao, iie, entity, properties, errorMsgKey);
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static InvalidInputException validate(ItemDao itemDao, InvalidInputException iie, JFWEntity entity, Map<String, Object> properties,
	        String errorMsgKey) {
		InvalidInputException exception = new InvalidInputException();
		List<Filter> filters = new ArrayList<>();
		filters.add(Filter.get("deletedFlag", Operator.NE, "1"));
		filters.addAll(properties.keySet().stream().map(key -> Filter.get(key, Operator.EQ, properties.get(key)))
		        .collect(Collectors.toList()));

		List<JFWEntity> dbEntities = (List<JFWEntity>) ItemDaoHelper
		        .filterUndeleted(itemDao.getItems(entity.getClass(), null, filters, null, null));
		for (JFWEntity dbEntity : dbEntities) {
			if (!itemDao.getIdentifier(entity).equals(itemDao.getIdentifier(dbEntity))) {
				if (iie == null)
					iie = new InvalidInputException();
				iie.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(errorMsgKey));
				return iie;
			}
		}
		return iie;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static InvalidInputException validateMobileNumer(ItemDao itemDao, String errorMsgKey, String value,
	        String property) {
		InvalidInputException exception = new InvalidInputException();
		Map<String, Object> properties = new HashMap<>();
		properties.put(property, value);
		List<Filter> filters = new ArrayList<>();
		filters.add(Filter.get("deletedFlag", Operator.NE, "1"));
		filters.addAll(properties.keySet().stream().map(key -> Filter.get(key, Operator.EQ, properties.get(key)))
		        .collect(Collectors.toList()));

		List<MPAY_CustomerMobile> dbEntities = ItemDaoHelper
		        .filterUndeleted(itemDao.getItems(MPAY_CustomerMobile.class, null, filters, null, null));
		if (dbEntities.size() > 0) {
			exception.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(errorMsgKey));
			return exception;
		}
		return exception;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static InvalidInputException validateNationality(ItemDao itemDao, String errorMsgKey, String value,
	        String property) {
		InvalidInputException exception = new InvalidInputException();
		Map<String, Object> properties = new HashMap<>();
		properties.put(property, value);
		List<Filter> filters = new ArrayList<>();
		filters.add(Filter.get("deletedFlag", Operator.NE, "1"));
		filters.addAll(properties.keySet().stream().map(key -> Filter.get(key, Operator.EQ, properties.get(key)))
		        .collect(Collectors.toList()));

		List<JfwCountry> dbEntities = ItemDaoHelper
		        .filterUndeleted(itemDao.getItems(JfwCountry.class, null, filters, null, null));
		if (dbEntities.size() > 0) {
			exception.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(errorMsgKey));
			return exception;
		}
		return exception;
	}
}
