package com.progressoft.mpay.registration.customers;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.BankType;
import com.progressoft.mpay.entity.BankedUnbankedFlag;

public class MobileBankedUnbankedHandler implements ChangeHandler {

	@Autowired
	ItemDao itemDao;

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) changeHandlerContext.getEntity();
		if (mobile.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED)) {
			MPAY_Bank settlementBank = itemDao.getItem(MPAY_Bank.class, "settlementParticipant", BankType.SETTLEMENT);
			mobile.setBank(settlementBank);
		}
	}

}
