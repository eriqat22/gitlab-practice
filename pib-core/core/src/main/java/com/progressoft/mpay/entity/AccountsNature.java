package com.progressoft.mpay.entity;

public class AccountsNature {
    public static final String ASSETS = "1";
    public static final String LIABILITIES = "2";
    private AccountsNature() {
    }
}
