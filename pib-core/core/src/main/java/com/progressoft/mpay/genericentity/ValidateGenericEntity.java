package com.progressoft.mpay.genericentity;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_CustomerKyc;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component
public class ValidateGenericEntity implements Validator {
    public static final String DELETESTATUSCODE = "104609";
    private static final Logger logger = LoggerFactory.getLogger(ValidateGenericEntity.class);


    @Override
    public void validate(Map arg0, Map map1, PropertySet propertySet) throws WorkflowException {
        logger.debug("Inside execute =====");
        Object master = arg0.get(WorkflowService.WF_ARG_BEAN);
        Object draft = arg0.get(WorkflowService.WF_ARG_BEAN_DRAFT);
        if (master instanceof MPAY_MessageType) {
            MPAY_MessageType messageType;
            if (draft != null)
                messageType = (MPAY_MessageType) draft;
            else
                messageType = (MPAY_MessageType) master;

            IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();
            List<MPAY_MessageType> messageTypes = dataProvider.getMessageTypesByCodeOrName(messageType.getCode(), messageType.getName());
            if (!canAddNewMessageType(messageTypes)) {
                throw new WorkflowException("WorkflowException");
            }
        }
    }



    private boolean canAddNewMessageType(List<MPAY_MessageType> messageTypes) {
        int i = 0;
        for (MPAY_MessageType messageType : messageTypes) {
            if (messageType.getDeletedFlag() == null && messageType.getStatusId() == null)
                i++;
            else if (!messageType.getStatusId().getCode().equals(DELETESTATUSCODE))
                i++;
        }
        return i <= 1;
    }

}
