package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;

public class UnblockDevice implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(UnblockDevice.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVar, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		Object device = transientVar.get(WorkflowService.WF_ARG_BEAN);
		if (device instanceof MPAY_CustomerDevice) {
			((MPAY_CustomerDevice) device).setIsBlocked(false);
			((MPAY_CustomerDevice) device).setRetryCount(0L);
		} else {
			((MPAY_CorporateDevice) device).setIsBlocked(false);
			((MPAY_CorporateDevice) device).setRetryCount(0L);
		}
	}

}
