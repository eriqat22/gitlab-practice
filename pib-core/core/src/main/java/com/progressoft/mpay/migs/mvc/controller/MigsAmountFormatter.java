package com.progressoft.mpay.migs.mvc.controller;

import java.text.DecimalFormat;

public class MigsAmountFormatter {

	private static DecimalFormat decimalFormat = null;
	private static final String MIGS_DECIMAL_FORMAT = "#000000000000000";

	private MigsAmountFormatter() {

	}

	public static String formatAmount(String amountString) {
		if (decimalFormat == null)
			decimalFormat = new DecimalFormat(MIGS_DECIMAL_FORMAT);
		Double amount = Double.parseDouble(amountString) * 100;
		return decimalFormat.format(amount);
	}
}
