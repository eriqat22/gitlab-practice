package com.progressoft.mpay.registration.customers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.progressoft.jfw.labels.LocaleHolder;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_SysConfig;
import com.progressoft.mpay.entity.SysConfigListItem;

public class SaibCustomersChangeHandler extends CustomersChangeHandler {
	private static final Logger logger = LoggerFactory.getLogger(SaibCustomersChangeHandler.class);

	private static final String MONTHLY_INCOME_SYS_CONFIG_KEY = "Monthly Income";
	private static final String SOURCE_OF_INCOME_SYS_CONFIG_KEY = "Source of Income";
	private static final String EMPLOYMENT_STATUS_SYS_CONFIG_KEY = "Employment status";

	@Override
	protected void handleExtraCustomerFields(ChangeHandlerContext changeHandlerContext, MPAY_Customer customer) {
		boolean isVisible = customer.getStatusId() != null && !customer.getStatusId().getCode().equals(CustomerWorkflowStatuses.MODIFICATION);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.FIRST_TEXT_FIELD).setVisible(isVisible);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.FIRST_TEXT_FIELD).setEnabled(false);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.SECOND_TEXT_FIELD).setVisible(isVisible);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.SECOND_TEXT_FIELD).setEnabled(false);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.THIRD_TEXT_FIELD).setVisible(isVisible);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.THIRD_TEXT_FIELD).setEnabled(false);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.FOURTH_TEXT_FIELD).setVisible(isVisible);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.FOURTH_TEXT_FIELD).setEnabled(false);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.FIFTH_TEXT_FIELD).setVisible(isVisible);
		changeHandlerContext.getFieldsAttributes().get(CustomerProperties.FIFTH_TEXT_FIELD).setEnabled(false);


	}

	@Override
	protected void handleExtraMobileFields(ChangeHandlerContext changeHandlerContext, MPAY_CustomerMobile mobile) {
		//
	}

	@Override
	protected void handleExtraAccountFields(ChangeHandlerContext changeHandlerContext, MPAY_MobileAccount account) {
		//
	}

	private String getValue(String code, String key) {
		if (code == null || code.trim().length() == 0)
			return null;
		List<SysConfigListItem> items = getListItem(key);
		if (items.isEmpty())
			return code;
		Optional<SysConfigListItem> value = items.stream().filter(i -> i.getCode() != null && i.getCode().equalsIgnoreCase(code)).findFirst();
		if (!value.isPresent())
			return code;
		if (LocaleHolder.currentLocale().getLanguage().equals("ar"))
			return value.get().getAr();
		else
			return value.get().getEn();
	}

	private List<SysConfigListItem> getListItem(String key) {
		MPAY_SysConfig sysConfig = MPayContext.getInstance().getLookupsLoader().getSystemConfigurations(key);
		if (sysConfig == null)
			return new ArrayList<>();
		try {
			Type listType = new TypeToken<ArrayList<SysConfigListItem>>() {
			}.getType();
			Gson gson = new GsonBuilder().create();
			return gson.fromJson(sysConfig.getConfigValue(), listType);
		} catch (Exception e) {
			logger.error("Failed to deserialize sysConfig with key: " + key, e);
			return new ArrayList<>();
		}
	}
}
