package com.progressoft.mpay.networkmanagement;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;

public class ValidatePSP implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidatePSP.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside ValidatePSP ==========");
		InvalidInputException iie = new InvalidInputException();
				StringBuilder sb = new StringBuilder();

		if(LookupsLoader.getInstance().getPSP() == null)
		{
			sb.append(AppContext.getMsg("networkManagement.pspnotdefined"));
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, sb.toString());
		}

		if (iie.getErrors().size() > 0) {
			throw iie;
		}

	}

}
