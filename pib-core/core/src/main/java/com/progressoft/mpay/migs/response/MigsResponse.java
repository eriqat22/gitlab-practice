package com.progressoft.mpay.migs.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="MigsResponse")
public class MigsResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	private String vpcAVSRequestCode;
	private String vpcAVSResultCode;
	private String vpcAcqAVSRespCode;
	private String vpcAcqCSCRespCode;
	private String vpcAcqResponseCode;
	private String vpcAmount;
	private String vpcAuthorizeId;
	private String vpcBatchNo;
	private String vpcCSCResultCode;
	private String vpcCard;
	private String vpcCardNum;
	private String vpcCommand;
	private String vpcLocale;
	private String vpcMerchTxnRef;
	private String vpcMerchant;
	private String vpcMessage;
	private String vpcOrderInfo;
	private String vpcReceiptNo;
	private String vpcSecureHash;
	private String vpcTransactionNo;
	private String vpcTxnResponseCode;
	private String vpcVersion;

	@XmlElement(name="vpc_AVSRequestCode")
	public String getVpcAVSRequestCode() {
		return vpcAVSRequestCode;
	}

	public void setVpcAVSRequestCode(String vpcAVSRequestCode) {
		this.vpcAVSRequestCode = vpcAVSRequestCode;
	}

	@XmlElement(name="vpc_AVSResultCode")
	public String getVpcAVSResultCode() {
		return vpcAVSResultCode;
	}

	public void setVpcAVSResultCode(String vpcAVSResultCode) {
		this.vpcAVSResultCode = vpcAVSResultCode;
	}

	@XmlElement(name="vpc_AcqAVSRespCode")
	public String getVpcAcqAVSRespCode() {
		return vpcAcqAVSRespCode;
	}

	public void setVpcAcqAVSRespCode(String vpcAcqAVSRespCode) {
		this.vpcAcqAVSRespCode = vpcAcqAVSRespCode;
	}

	@XmlElement(name="vpc_AcqCSCRespCode")
	public String getVpcAcqCSCRespCode() {
		return vpcAcqCSCRespCode;
	}

	public void setVpcAcqCSCRespCode(String vpcAcqCSCRespCode) {
		this.vpcAcqCSCRespCode = vpcAcqCSCRespCode;
	}

	@XmlElement(name="vpc_AcqResponseCode")
	public String getVpcAcqResponseCode() {
		return vpcAcqResponseCode;
	}

	public void setVpcAcqResponseCode(String vpcAcqResponseCode) {
		this.vpcAcqResponseCode = vpcAcqResponseCode;
	}

	@XmlElement(name="vpc_Amount")
	public String getVpcAmount() {
		return vpcAmount;
	}

	public void setVpcAmount(String vpcAmount) {
		this.vpcAmount = vpcAmount;
	}

	@XmlElement(name="vpc_AuthorizeId")
	public String getVpcAuthorizeId() {
		return vpcAuthorizeId;
	}

	public void setVpcAuthorizeId(String vpcAuthorizeId) {
		this.vpcAuthorizeId = vpcAuthorizeId;
	}

	@XmlElement(name="vpc_BatchNo")
	public String getVpcBatchNo() {
		return vpcBatchNo;
	}

	public void setVpcBatchNo(String vpcBatchNo) {
		this.vpcBatchNo = vpcBatchNo;
	}

	@XmlElement(name="vpc_CSCResultCode")
	public String getVpcCSCResultCode() {
		return vpcCSCResultCode;
	}

	public void setVpcCSCResultCode(String vpcCSCResultCode) {
		this.vpcCSCResultCode = vpcCSCResultCode;
	}

	@XmlElement(name="vpc_Card")
	public String getVpcCard() {
		return vpcCard;
	}

	public void setVpcCard(String vpcCard) {
		this.vpcCard = vpcCard;
	}

	@XmlElement(name="vpc_CardNum")
	public String getVpcCardNum() {
		return vpcCardNum;
	}

	public void setVpcCardNum(String vpcCardNum) {
		this.vpcCardNum = vpcCardNum;
	}

	@XmlElement(name="vpc_Command")
	public String getVpcCommand() {
		return vpcCommand;
	}

	public void setVpcCommand(String vpcCommand) {
		this.vpcCommand = vpcCommand;
	}

	@XmlElement(name="vpc_Locale")
	public String getVpcLocale() {
		return vpcLocale;
	}

	public void setVpcLocale(String vpcLocale) {
		this.vpcLocale = vpcLocale;
	}

	@XmlElement(name="vpc_MerchTxnRef")
	public String getVpcMerchTxnRef() {
		return vpcMerchTxnRef;
	}

	public void setVpcMerchTxnRef(String vpcMerchTxnRef) {
		this.vpcMerchTxnRef = vpcMerchTxnRef;
	}

	@XmlElement(name="vpc_Merchant")
	public String getVpcMerchant() {
		return vpcMerchant;
	}

	public void setVpcMerchant(String vpcMerchant) {
		this.vpcMerchant = vpcMerchant;
	}

	@XmlElement(name="vpc_Message")
	public String getVpcMessage() {
		return vpcMessage;
	}

	public void setVpcMessage(String vpcMessage) {
		this.vpcMessage = vpcMessage;
	}

	@XmlElement(name="vpc_OrderInfo")
	public String getVpcOrderInfo() {
		return vpcOrderInfo;
	}

	public void setVpcOrderInfo(String vpcOrderInfo) {
		this.vpcOrderInfo = vpcOrderInfo;
	}

	@XmlElement(name="vpc_ReceiptNo")
	public String getVpcReceiptNo() {
		return vpcReceiptNo;
	}

	public void setVpcReceiptNo(String vpcReceiptNo) {
		this.vpcReceiptNo = vpcReceiptNo;
	}

	@XmlElement(name="vpc_SecureHash")
	public String getVpcSecureHash() {
		return vpcSecureHash;
	}

	public void setVpcSecureHash(String vpcSecureHash) {
		this.vpcSecureHash = vpcSecureHash;
	}

	@XmlElement(name="vpc_TransactionNo")
	public String getVpcTransactionNo() {
		return vpcTransactionNo;
	}

	public void setVpcTransactionNo(String vpcTransactionNo) {
		this.vpcTransactionNo = vpcTransactionNo;
	}

	@XmlElement(name="vpc_TxnResponseCode")
	public String getVpcTxnResponseCode() {
		return vpcTxnResponseCode;
	}

	public void setVpcTxnResponseCode(String vpcTxnResponseCode) {
		this.vpcTxnResponseCode = vpcTxnResponseCode;
	}

	@XmlElement(name="vpc_Version")
	public String getVpcVersion() {
		return vpcVersion;
	}

	public void setVpcVersion(String vpcVersion) {
		this.vpcVersion = vpcVersion;
	}

}