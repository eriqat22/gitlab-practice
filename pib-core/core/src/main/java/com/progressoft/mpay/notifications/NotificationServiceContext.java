package com.progressoft.mpay.notifications;

import com.progressoft.mpay.entities.MPAY_NotificationService;
import com.progressoft.mpay.sms.NotificationRequest;

public class NotificationServiceContext {
	private MPAY_NotificationService notificationService;
	private NotificationRequest request;

	public NotificationRequest getRequest() {
		return request;
	}

	public void setRequest(NotificationRequest request) {
		this.request = request;
	}

	public MPAY_NotificationService getNotificationService() {
		return notificationService;
	}

	public void setNotificationService(MPAY_NotificationService notificationService) {
		this.notificationService = notificationService;
	}
}
