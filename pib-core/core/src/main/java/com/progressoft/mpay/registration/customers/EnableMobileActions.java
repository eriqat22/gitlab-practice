package com.progressoft.mpay.registration.customers;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;

public class EnableMobileActions implements Condition {

	private static final Logger logger = LoggerFactory.getLogger(EnableMobileActions.class);

	@SuppressWarnings("rawtypes")
	@Override
	public boolean passesCondition(Map transientVar, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside passesCondition ------------");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVar.get(WorkflowService.WF_ARG_BEAN);
		List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
		if (accounts.size() == 1)
			return true;

		MPAY_MobileAccount newAccount = selectFirst(mobile.getMobileMobileAccounts(), having(on(MPAY_MobileAccount.class).getIsRegistered(), Matchers.equalTo(false)));
		return newAccount == null;
	}
}
