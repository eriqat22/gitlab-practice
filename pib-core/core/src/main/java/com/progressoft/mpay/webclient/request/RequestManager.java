package com.progressoft.mpay.webclient.request;

import com.progressoft.mpay.common.HttpHelper;
import com.progressoft.mpay.common.WebClient;
import com.progressoft.mpay.messages.MPayRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;

import static com.progressoft.mpay.webclient.security.SecurityUtils.createToken;

public class RequestManager {

    private String apiUrl;

    public RequestManager(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String post(MPayRequest request) throws Exception {
        try {
            String token = createToken(request);
            HttpPost httpRequest = new HttpHelper().createPostRequest(apiUrl, token, request.toString());
            WebClient client = new WebClient(HttpClients.createDefault());
            return client.post(httpRequest);
        } catch (Exception e) {
            throw new Exception(e);
        }

    }
}
