package com.progressoft.mpay.common;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Map;

public class HttpHelper {

    public HttpPost createPostRequest(String uri,String token, String body) throws URISyntaxException, UnsupportedEncodingException {
        URIBuilder builder = new URIBuilder(uri);
        builder.setParameter("token", token);
        HttpPost httpRequest = new HttpPost(builder.build());
        httpRequest.setHeader("Content-Type", "application/json");
        httpRequest.setEntity(new StringEntity(body));
        return httpRequest;
    }

    public HttpGet createGetRequest(String uri, Map<String, String> header) {
        HttpGet httpResponse = new HttpGet(uri);
        httpResponse.setHeader("Content-Type", "application/json");
        header.forEach(httpResponse::setHeader);
        return httpResponse;
    }
}