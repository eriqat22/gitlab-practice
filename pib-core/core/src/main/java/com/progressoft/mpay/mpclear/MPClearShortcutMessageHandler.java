package com.progressoft.mpay.mpclear;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.apache.cxf.binding.soap.SoapHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayCryptographer;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgType;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class MPClearShortcutMessageHandler implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(MPClearShortcutMessageHandler.class);

	public static final String SERVICE_USER = "SERVICE_USER";

	@SuppressWarnings("rawtypes")
	@Override
	public void process(Exchange exchange) throws Exception {
		boolean serviceUserEnabled = false;
		try {
			logger.debug("Inside MPClearMessageHandler ....");
			serviceUserEnabled = IntegrationUtils.enableServiceUser((String) exchange.getProperty("tenantId"));

			CxfPayload requestPayload = exchange.getIn().getBody(CxfPayload.class);
			if (requestPayload == null)
				throw new InvalidArgumentException("exchange.getIn().getBody(CxfPayload.class) == null");
			List inElements = requestPayload.getBodySources();

			Element messageElements = new XmlConverter().toDOMElement((Source) inElements.get(0));

			if ((messageElements.getChildNodes() == null) || (messageElements.getChildNodes().getLength() != 4)) {
				throw new InvalidArgumentException("Invalid argument input for send operation!");
			}

			IsoMessage isoMessage = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath())
					.parseMessage(messageElements.getChildNodes().item(0).getTextContent());
			if (isoMessage == null)
				throw new NotSupportedException("Invalid request received: " + messageElements.getChildNodes().item(0).getTextContent());
			MPAY_MpClearIntegMsgType messageType = LookupsLoader.getInstance().getMpClearIntegrationMessageTypes(Integer.toHexString(isoMessage.getType()));

			IntegrationProcessingContext context = new IntegrationProcessingContext(DataProvider.instance(), LookupsLoader.getInstance(), SystemParameters.getInstance(),
					new MPayCryptographer(), NotificationHelper.getInstance());
			String processingCodeValue = null;
			IsoValue processingCode = isoMessage.getField(MPClearCommonFields.PROCESSING_CODE);
			if(processingCode != null)
				processingCodeValue = processingCode.getValue().toString();
			MPAY_MpClearIntegMsgLog response = MPClearHelper.createMPClearResponse(context, isoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(), messageType, ReasonCodes.VALID, "",
					processingCodeValue);
			sendResponse(exchange, response);
		} catch (Exception ex) {
			logger.error("Error while ProcessMPClearMessage message", ex);
		} finally {
			if (serviceUserEnabled) {
				IntegrationUtils.disableServiceUser();
			}
		}
	}

	private void sendResponse(Exchange exchange, MPAY_MpClearIntegMsgLog msgLog) throws IOException, SAXException, ParserConfigurationException {
		exchange.setProperty("integMsgLog", msgLog);

        IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msgLog.getContent());
        isomsg.setField(
                7,
                new IsoValue<>(IsoType.DATE10, SystemHelper.formatDate(
                        SystemHelper.getCalendar().getTime(), "MMddHHmmss")));
        String contents = new String(isomsg.writeData());
        msgLog.setContent(contents);

        Calendar date = getSigningTimestamp();
        String signingDate = SystemHelper.formatDate(date.getTime(),
                SystemParameters.getInstance().getSigningDateFormat());

        String token = new MPayCryptographer().generateToken(SystemParameters.getInstance(), msgLog.getContent(),
                signingDate, IntegMessagesSource.SYSTEM);
        msgLog.setToken(token);

        msgLog.setSigningStamp(new Timestamp(date.getTimeInMillis()));

        exchange.setProperty("msgtype", msgLog.getContent().substring(0, 5));

        String request = "<ProcessMessage xmlns=\"http://www.progressoft.com/mpclear\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">"
                + "<message>"
                + msgLog.getContent()
                + "</message>"
                + "<agentId>"
                + msgLog.getRefSender()
                + "</agentId>"
                + "<token>"
                + msgLog.getToken()
                + "</token>"
                + "<date>"
                + SystemHelper.formatDate(msgLog.getSigningStamp(),
                SystemParameters.getInstance().getSigningDateFormat())
                + "</date>" + "</ProcessMessage>";

        logger.debug("*** Request");
        logger.debug(request);
        XmlConverter converter = new XmlConverter();
        List<Source> outElements = new ArrayList<>();

        @SuppressWarnings("deprecation")
        Document outDocument = converter.toDOMDocument(request);
        outElements.add(new DOMSource(outDocument.getDocumentElement()));
        CxfPayload<SoapHeader> responsePayload = new CxfPayload<>(
                null, outElements, null);
        exchange.getOut().setBody(responsePayload);
	}

	private Calendar getSigningTimestamp() {
        Calendar date = SystemHelper.getCalendar();
        date.set(Calendar.MILLISECOND, 0);
        return date;
    }
}
