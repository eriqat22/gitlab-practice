package com.progressoft.mpay.charges;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_ChargesSlice;
import com.progressoft.mpay.entity.ChargeTypes;
public class ChargeSlicesHandler implements ChangeHandler {

    private static final Logger logger = LoggerFactory.getLogger(ChargeSlicesHandler.class);
    private static final String TX_AMOUNT_LIMIT = "txAmountLimit";
    private static final String TX_COUNT_LIMIT = "txCountLimit";
    private static final String CHARGE_TYPE = "chargeType";
    private static final String CHARGE_PERCENT = "chargePercent";
    private static final String MIN_AMOUNT = "minAmount";
    private static final String MAX_AMOUNT = "maxAmount";

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        MPAY_ChargesSlice slice = (MPAY_ChargesSlice) changeHandlerContext.getEntity();
        logger.debug("inside ChargeSlicesHandler ----------------------------");

        if (slice.getRefChargesDetails().getMsgType().getIsFinancial()) {
            changeHandlerContext.getFieldsAttributes().get(TX_AMOUNT_LIMIT).setVisible(true);
            changeHandlerContext.getFieldsAttributes().get(TX_AMOUNT_LIMIT).setRequired(true);
            changeHandlerContext.getFieldsAttributes().get(TX_COUNT_LIMIT).setRequired(false);
            changeHandlerContext.getFieldsAttributes().get(TX_COUNT_LIMIT).setVisible(false);
            slice.setTxCountLimit(0L);

            changeHandlerContext.getFieldsAttributes().get(CHARGE_TYPE).setVisible(true);
            changeHandlerContext.getFieldsAttributes().get(CHARGE_PERCENT).setRequired(true);
            changeHandlerContext.getFieldsAttributes().get(MIN_AMOUNT).setRequired(true);
            changeHandlerContext.getFieldsAttributes().get(MAX_AMOUNT).setRequired(true);
        } else {
            changeHandlerContext.getFieldsAttributes().get(TX_COUNT_LIMIT).setRequired(true);
            changeHandlerContext.getFieldsAttributes().get(TX_COUNT_LIMIT).setVisible(true);
            changeHandlerContext.getFieldsAttributes().get(TX_AMOUNT_LIMIT).setVisible(false);
            changeHandlerContext.getFieldsAttributes().get(TX_AMOUNT_LIMIT).setRequired(false);
            slice.setTxAmountLimit(BigDecimal.ZERO);
            slice.setChargeType(ChargeTypes.FIXED);
            changeHandlerContext.getFieldsAttributes().get(CHARGE_TYPE).setVisible(false);
            changeHandlerContext.getFieldsAttributes().get(CHARGE_PERCENT).setRequired(false);
            changeHandlerContext.getFieldsAttributes().get(MIN_AMOUNT).setRequired(false);
            changeHandlerContext.getFieldsAttributes().get(MAX_AMOUNT).setRequired(false);
            slice.setChargePercent(BigDecimal.ZERO);
            slice.setMinAmount(BigDecimal.ZERO);
            slice.setMaxAmount(BigDecimal.ZERO);
        }
        ChargeHandlersHelper.handleChargeType(changeHandlerContext.getFieldsAttributes(), slice);

    }
}
