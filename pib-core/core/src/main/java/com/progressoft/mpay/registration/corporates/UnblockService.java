package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class UnblockService implements FunctionProvider {

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVar, Map arg1, PropertySet arg2) throws WorkflowException {
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) transientVar.get(WorkflowService.WF_ARG_BEAN);
		service.setIsBlocked(false);
		service.setRetryCount(0L);
	}

}
