package com.progressoft.mpay.messages;

import java.util.Comparator;

public class ExtraDataComparer implements Comparator<ExtraData> {

	@Override
	public int compare(ExtraData extraData1, ExtraData extraData2) {
		if(extraData1 == null)
			return -1;
		if(extraData2 == null)
			return 1;
		return extraData1.getKey().compareToIgnoreCase(extraData2.getKey());
	}

}
