package com.progressoft.mpay.payments.cashin;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entities.MPAY_ServiceCashIn;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.tax.TaxCalculator;

public class ServiceCashInIsChargeIncluded implements ChangeHandler {

	private static final String REF_SERVICE_ACCOUNT = "refServiceAccount";
	private static final Logger logger = LoggerFactory.getLogger(ServiceCashInIsChargeIncluded.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside handle ========================");

		MPAY_ServiceCashIn serviceCashIn = (MPAY_ServiceCashIn) changeHandlerContext.getEntity();
		if (serviceCashIn.getAmount() != null && serviceCashIn.getAmount().doubleValue() > 0 && serviceCashIn.getRefCorporateService() != null) {
			MPAY_ServiceAccount account = SystemHelper.getServiceAccount(MPayContext.getInstance().getSystemParameters(), serviceCashIn.getRefCorporateService(), serviceCashIn.getRefServiceAccount());
			MPAY_Profile profile = account.getRefProfile();
			BigDecimal chargeAmount = ChargesCalculator.calculateCharge(MPayContext.getInstance().getDataProvider(), serviceCashIn.getAmount(), MessageCodes.SRVCI.toString(), profile, false);
			BigDecimal taxAmount = TaxCalculator.calculateTax(MPayContext.getInstance().getDataProvider(), chargeAmount, MessageCodes.SRVCI.toString(), profile, MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
			serviceCashIn.setTransAmount(serviceCashIn.getAmount());
			serviceCashIn.setChargeAmount(chargeAmount);
			serviceCashIn.setTaxAmount(taxAmount);
			serviceCashIn.setTotalAmount(serviceCashIn.getAmount());
		}

		if (serviceCashIn.getRefCorporateService() != null) {
			changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setRequired(true);
			changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setEnabled(true);

		} else {
			changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setEnabled(false);
			changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setRequired(false);
			serviceCashIn.setRefServiceAccount(null);
		}

	}
}