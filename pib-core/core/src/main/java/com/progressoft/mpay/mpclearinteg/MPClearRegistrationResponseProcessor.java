package com.progressoft.mpay.mpclearinteg;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.mpclear.plugins.MPClearMessageProcessor;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingContext;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingResult;
import com.progressoft.mpay.registration.Post1710CorpProcessMpClearInegMsgLog;
import com.progressoft.mpay.registration.Post1710ProcessMpClearInegMsgLog;
import com.progressoft.mpay.registration.Pre1710CorpValidationMpClearInegMsgLog;
import com.progressoft.mpay.registration.Pre1710EvaluationMpClearInegMsgLog;
import com.progressoft.mpay.registration.Pre1710ValidationMpClearInegMsgLog;

public class MPClearRegistrationResponseProcessor extends MPClearMessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(MPClearRegistrationResponseProcessor.class);

    @Override
    public MPClearProcessingResult processMessage(MPClearProcessingContext context) {
        try {
            logger.debug("inside MPClearRegistrationResponseProcessor...");
            if (context == null)
                throw new NullArgumentException("context");

            DataProvider.instance().persistMPClearIntegMessageLog(context.getMessage());
            Map<String, Object> v = new HashMap<>();
            v.put(WorkflowService.WF_ARG_BEAN, context.getMessage());
            PreParseMpClearInegMsgLog oPreParseMpClearInegMsgLog = AppContext.getApplicationContext().getBean("PreParseMpClearInegMsgLog", PreParseMpClearInegMsgLog.class);
            oPreParseMpClearInegMsgLog.execute(v, null, null);
            Pre1710EvaluationMpClearInegMsgLog oPre1710EvaluationMpClearInegMsgLog = AppContext.getApplicationContext().getBean("Pre1710EvaluationMpClearInegMsgLog",
                    Pre1710EvaluationMpClearInegMsgLog.class);
            oPre1710EvaluationMpClearInegMsgLog.execute(v, null, null);

            if ("M".equals(v.get("clientType"))) {

                Pre1710ValidationMpClearInegMsgLog oPre1710ValidationMpClearInegMsgLog = AppContext.getApplicationContext().getBean("Pre1710ValidationMpClearInegMsgLog",
                        Pre1710ValidationMpClearInegMsgLog.class);
                oPre1710ValidationMpClearInegMsgLog.execute(v, null, null);

                if (v.get(Result.VALIDATION_RESULT).equals(Result.SUCCESSFUL)) {
                    Post1710ProcessMpClearInegMsgLog oPost1710ProcessMpClearInegMsgLog = AppContext.getApplicationContext().getBean("Post1710ProcessMpClearInegMsgLog",
                            Post1710ProcessMpClearInegMsgLog.class);
                    oPost1710ProcessMpClearInegMsgLog.execute(v, null, null);
                } else {
                    PreRejectMpClearIntegMsgLog oPreRejectMpClearIntegMsgLog = AppContext.getApplicationContext().getBean("PreRejectMpClearIntegMsgLog", PreRejectMpClearIntegMsgLog.class);
                    oPreRejectMpClearIntegMsgLog.execute(v, null, null);
                }
            } else if ("C".equals(v.get("clientType"))) {
                Pre1710CorpValidationMpClearInegMsgLog oPre1710CorpValidationMpClearInegMsgLog = AppContext.getApplicationContext().getBean("Pre1710CorpValidationMpClearInegMsgLog",
                        Pre1710CorpValidationMpClearInegMsgLog.class);
                oPre1710CorpValidationMpClearInegMsgLog.execute(v, null, null);

                if (v.get(Result.VALIDATION_RESULT).equals(Result.SUCCESSFUL)) {
                    Post1710CorpProcessMpClearInegMsgLog oPost1710CorpProcessMpClearInegMsgLog = AppContext.getApplicationContext().getBean("Post1710CorpProcessMpClearInegMsgLog",
                            Post1710CorpProcessMpClearInegMsgLog.class);
                    oPost1710CorpProcessMpClearInegMsgLog.execute(v, null, null);
                } else {
                    PreRejectMpClearIntegMsgLog oPreRejectMpClearIntegMsgLog = AppContext.getApplicationContext().getBean("PreRejectMpClearIntegMsgLog", PreRejectMpClearIntegMsgLog.class);
                    oPreRejectMpClearIntegMsgLog.execute(v, null, null);
                }
            }

        } catch (Exception ex) {
            logger.error("Error while Process1710MPClear message", ex);
        }
        return null;
    }
}
