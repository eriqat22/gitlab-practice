package com.progressoft.mpay.plugins.validators;

import com.progressoft.jfw.integration.JfwFacade;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.registration.corporates.ServiceAccountWorkflowStatuses;
import com.progressoft.mpay.registration.customers.MobileAccountWorkflowStatuses;
import org.joda.time.DurationFieldType;
import org.joda.time.LocalDateTime;

import java.util.Date;
import java.util.HashMap;

public class DormantAccountValidator {

    public static final int MAKE_DORMANT_MOBILE_ACCOUNT_ACTION_KEY = 409;
    public static final int MAKE_DORMANT_SERVICE_ACCOUNT_ACTION_KEY = 409;

    public static ValidationResult validateMobileAccountAgainstDormantCase(MPAY_MobileAccount mobileAccount)
    {
        return validateMobileOrServiceAccountAgainstDormantCase(mobileAccount);
    }

    public static ValidationResult validateServiceAccountAgainstDormantCase(MPAY_ServiceAccount serviceAccount)
    {
        return validateMobileOrServiceAccountAgainstDormantCase(serviceAccount);
    }

    private static ValidationResult validateMobileOrServiceAccountAgainstDormantCase(JFWEntity account)
    {
        DormantStatus dormantStatus = getDormantStatus(account);
        if(dormantStatus == DormantStatus.None)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        else if(dormantStatus == DormantStatus.AlreadyDormant)
            return new ValidationResult(getDormantReasonCode(account), null, false);
        else {
            convertToDormantAccount(account);
            return new ValidationResult(getDormantReasonCode(account), null, false);
        }
    }

    private static String  getDormantReasonCode(JFWEntity account)
    {
        return (account instanceof  MPAY_MobileAccount)?
                ReasonCodes.MOBILE_ACCOUNT_DORMANT:
                ReasonCodes.SERVICE_ACCOUNT_DORMANT;
    }

    private static String  getViewName(JFWEntity account)
    {
        return (account instanceof  MPAY_MobileAccount)?
            MPAYView.MOBILE_ACCOUNTS_VIEW.viewName:
            MPAYView.SERVICE_ACCOUNTS_VIEW.viewName;
    }

    private static int  getMakeDormantActionKey(JFWEntity account)
    {
        return (account instanceof  MPAY_MobileAccount)?
            MAKE_DORMANT_MOBILE_ACCOUNT_ACTION_KEY:
            MAKE_DORMANT_SERVICE_ACCOUNT_ACTION_KEY;
    }

    private static Date  getLastTransactionDate(JFWEntity account)
    {
        return (account instanceof  MPAY_MobileAccount)?
                ((MPAY_MobileAccount) account).getLastTransactionDate():
                ((MPAY_ServiceAccount) account).getLastTransactionDate();
    }

    private static int  getDormantTimeout(JFWEntity account) {
        return (account instanceof MPAY_MobileAccount) ?
                SystemParameters.getInstance().getDormantMobileAccountTimeoutInDays() :
                SystemParameters.getInstance().getDormantServiceAccountTimeoutInDays();
    }

    private static DormantStatus getDormantStatus(JFWEntity account)
    {
        DormantStatus dormantStatus = DormantStatus.None;
        if(isCurrentlyDormantMobileAccount(account.getStatusId().getCode()))
            dormantStatus = DormantStatus.AlreadyDormant;
        else if(hasExceededNonDormantDuration(getLastTransactionDate(account), getDormantTimeout(account)))
            dormantStatus = DormantStatus.ExceededNonDormantDuration;
        return dormantStatus;
    }

    private static void convertToDormantAccount(JFWEntity account) {
        JfwFacade facade = (JfwFacade) AppContext.getApplicationContext().getBean("defaultJfwFacade");
        facade.executeAction(getViewName(account), getMakeDormantActionKey(account), account, new HashMap<>(), new HashMap<>());
    }

    private static boolean hasExceededNonDormantDuration(Date lastTransactionDate, int timeoutInDays ) {
        if( timeoutInDays > 0 ) {
            LocalDateTime theMaxPossibleDateToBeNonDormant = new LocalDateTime(lastTransactionDate).withFieldAdded(DurationFieldType.days(), timeoutInDays);
            if (theMaxPossibleDateToBeNonDormant.toDateTime().isBeforeNow()) {
                return true;
            }
        }
        return false;
    }

    private static boolean isCurrentlyDormantMobileAccount(String workFlowStatus) {
        return  (workFlowStatus.equals(MobileAccountWorkflowStatuses.DORMANT.toString()) |
                workFlowStatus.equals(ServiceAccountWorkflowStatuses.DORMANT.toString()));
    }

    private enum DormantStatus
    {
        None,
        AlreadyDormant,
        ExceededNonDormantDuration
    }
}