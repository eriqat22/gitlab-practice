package com.progressoft.mpay.pushnotificationservices.onesignal;

import com.progressoft.mpay.entities.MPAY_NotificationService;
import com.progressoft.mpay.entities.MPAY_NotificationServiceArg;
import com.progressoft.mpay.entity.Languages;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.notifications.INotificationService;
import com.progressoft.mpay.notifications.NotificationServiceContext;
import com.progressoft.mpay.notifications.NotificationServiceResult;
import com.progressoft.mpay.sms.NotificationRequest;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;
import java.util.Scanner;

/**
 * Created by user on 29/11/18.
 */
public class OneSignalPushNotificationService implements INotificationService {
    private static final Logger logger = LoggerFactory.getLogger(OneSignalPushNotificationService.class);
    private static final String UTF_8 = "UTF-8";
    private static final String APP_ID_KEY = "APP_ID";
    private static final String AUTHORIZATION_KEY = "AUTHORIZATION";
    private static final String URL_KEY = "URL";
    private static final String SERVICE_TAG_KEY = "SERVICE_TAG";
    private static final String DEVICE_TAG_KEY = "DEVICE_TAG";
    private static final String NOTIFICATION_TITLE_EN_KEY = "NOTIFICATION_TITLE_EN";
    private static final String NOTIFICATION_TITLE_AR_KEY = "NOTIFICATION_TITLE_AR";
    private String appId;
    private String authorization;
    private String serviceUrl;
    private String serviceTag;
    private String deviceTag;
    private String notificationTitleEn;
    private String notificationTitleAr;


    private void initialize(MPAY_NotificationService service) {
        if (service == null)
            throw new NullArgumentException("service");
        appId = getArgValue(service, APP_ID_KEY);
        authorization = getArgValue(service, AUTHORIZATION_KEY);
        serviceUrl = getArgValue(service, URL_KEY);
        serviceTag = getArgValue(service, SERVICE_TAG_KEY);
        deviceTag = getArgValue(service, DEVICE_TAG_KEY);
        notificationTitleEn = getArgValue(service, NOTIFICATION_TITLE_EN_KEY);
        notificationTitleAr = getArgValue(service, NOTIFICATION_TITLE_AR_KEY);
    }

    private String getArgValue(MPAY_NotificationService service, String name) {
        Optional<MPAY_NotificationServiceArg> arg = service.getRefServiceNotificationServiceArgs().stream().filter(a -> a.getName().equals(name)).findFirst();
        if (!arg.isPresent())
            throw new InvalidOperationException("Notification Argument not found, name: " + name);
        return arg.get().getValue();
    }

    @Override
    public NotificationServiceResult sendNotification(NotificationServiceContext context) {
        logger.info(" ****************** push notification ");
        if (context == null)
            throw new NullArgumentException("context");
        NotificationRequest request = context.getRequest();
        initialize(context.getNotificationService());
        try {
            String title = notificationTitleEn;
            if (request.getExtra2() != null && Long.parseLong(request.getExtra2()) == Languages.ARBIC) {
                title = notificationTitleAr;
            }

            OkHttpClient client = new OkHttpClient();
            PushNotificationBody psPushNotificationBody = new PushNotificationBody();
            Notification notification = new Notification();
            notification.setTitle(title);
            notification.setBody(request.getContent());
            psPushNotificationBody.setTo(request.getReciever());
            psPushNotificationBody.setNotification(notification);
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, psPushNotificationBody.toString());
            logger.info("****************** push notification body "+psPushNotificationBody.toString());
            Request jsonRequest = new Request.Builder()
                    .url(serviceUrl)
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", authorization)
                    .build();
            Response response = client.newCall(jsonRequest).execute();
            logger.info("****************** push notification respnse" + response);
            response.close();
            return new NotificationServiceResult();
        } catch (Exception e) {
            throw new MPayGenericException("Failed to send push notification", e);
        }
    }
}
