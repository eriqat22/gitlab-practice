package com.progressoft.mpay.registration.corporates;

import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;

public class DefaultCorporatesChangeHandler extends CorporatesChangeHandler {

	@Override
	protected void handleExtraCorporateFields(ChangeHandlerContext changeHandlerContext, MPAY_Corporate corporate) {
		//
	}

	@Override
	protected void handleExtraServiceFields(ChangeHandlerContext changeHandlerContext, MPAY_CorpoarteService service) {
		//
	}

	@Override
	protected void handleExtraAccountFields(ChangeHandlerContext changeHandlerContext, MPAY_ServiceAccount account) {
		//
	}
}
