package com.progressoft.mpay.registration;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.persistence.EntityManager;

import org.hibernate.internal.SessionImpl;

import com.progressoft.mpay.entities.MPAY_Intg_Reg_File;

public final class FileRegistrationHelper {

	private FileRegistrationHelper() {

	}

	public static int callUpdateProcessedCountSP(MPAY_Intg_Reg_File file,	EntityManager em) throws SQLException {
		Connection connection = ((SessionImpl) em.getDelegate()).connection();
		CallableStatement statement = null;
		try {
			statement = connection.prepareCall("{call UpdateRegFileRecordsCount(?,?,?)}");
			statement.setLong(1, file.getId());
			statement.registerOutParameter(2, Types.INTEGER);
			statement.registerOutParameter(3, Types.INTEGER);
			statement.executeUpdate();
			int rowsCount = statement.getInt(3);
			statement.close();
			em.flush();
			return rowsCount;
		} finally {
			if (statement != null) {
				statement.close();
			}
		}
	}
}