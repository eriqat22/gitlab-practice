package com.progressoft.mpay.registration.corporates;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;

import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.MPAY_SysConfig;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class PostCreateCorporateService implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCreateCorporateService.class);
	private static final String VIRTUAL_IBAN = "SA0065998";

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
		service.setRetryCount(0L);
		service.setIsBlocked(false);
		service.setIsRegistered(false);
		service.setIsActive(true);
		try {
			createServiceAccount(service);
			createCommissionServiceAccount(service);
		} catch (Exception e) {
			logger.error("Error while creating service account", e);
			throw new WorkflowException(e);
		}
	}

	private void createServiceAccount(MPAY_CorpoarteService service) {
		logger.debug("inside createServiceAccount--------------------------");
		MPAY_ServiceAccount account = new MPAY_ServiceAccount();
		account.setService(service);
		account.setCategory(service.getCategory());
		account.setBank(service.getBank());
		account.setBankedUnbanked(service.getBankedUnbanked());
		account.setExternalAcc(service.getExternalAcc());
		account.setMas(service.getMas());
		account.setIsRegistered(service.getIsRegistered());
		account.setIsActive(service.getIsActive());
		account.setRefProfile(service.getRefProfile());
		account.setIsDefault(true);
		account.setIsSwitchDefault(true);
		account.setIban(service.getIban());
		service.setServiceServiceAccounts(new ArrayList<>());
		service.getServiceServiceAccounts().add(account);
		account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
		account.setCategory(service.getCategory());
		try {
			JfwHelper.createEntity(MPAYView.SERVICE_ACCOUNTS, account);
		} catch (InvalidActionException e) {
			throw new BusinessException(e);
		}
	}

	private void createCommissionServiceAccount(MPAY_CorpoarteService service) {
		logger.debug("inside createServiceAccount--------------------------");
		MPAY_ServiceAccount account = new MPAY_ServiceAccount();
		account.setService(service);
		account.setBankedUnbanked(service.getBankedUnbanked());
		account.setBank(service.getBank());
		account.setMas(service.getMas() + 1);
		account.setIsActive(service.getIsActive());
		account.setIsRegistered(service.getIsRegistered());
		account.setRefProfile(service.getRefProfile());

		service.setServiceServiceAccounts(new ArrayList<>());
		service.getServiceServiceAccounts().add(account);
		account.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));

		account.setIban(null);
		account.setIsDefault(false);
		account.setIsSwitchDefault(false);
		account.setExternalAcc(null);

		ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();
		MPAY_SysConfig commissionCategoryConfig = lookupsLoader.getSystemConfigurations(SysConfigKeys.COMMISSION_ACCOUNT_CATEGORY);
		MPAY_SysConfig commissionProfileConfig = lookupsLoader.getSystemConfigurations(SysConfigKeys.COMMISSION_PROFILE);
		account.setCategory(lookupsLoader.getAccountCategory(Long.parseLong(commissionCategoryConfig.getConfigValue())));
		account.setRefProfile(lookupsLoader.getProfile(commissionProfileConfig.getConfigValue()));


		try {
			JfwHelper.createEntity(MPAYView.SERVICE_ACCOUNTS, account);
		} catch (InvalidActionException e) {
			throw new BusinessException(e);
		}
	}
}