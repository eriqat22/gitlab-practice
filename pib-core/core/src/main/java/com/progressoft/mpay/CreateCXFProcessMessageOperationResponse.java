package com.progressoft.mpay;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.apache.cxf.binding.soap.SoapHeader;
import org.w3c.dom.Document;

public class CreateCXFProcessMessageOperationResponse implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        XmlConverter converter = new XmlConverter();
        List<Source> outElements = new ArrayList<>();
        String responseXML = "<ProcessMessageResponse xmlns=\"http://www.progressoft.com/mpclear\"></ProcessMessageResponse>";
        @SuppressWarnings("deprecation")
        Document outDocument = converter.toDOMDocument(responseXML);
        outElements.add(new DOMSource(outDocument.getDocumentElement()));
        CxfPayload<SoapHeader> responsePayload = new CxfPayload<>(null, outElements, null);
        exchange.getOut().setBody(responsePayload);
    }

}
