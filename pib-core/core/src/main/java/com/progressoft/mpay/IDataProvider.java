package com.progressoft.mpay;

import com.progressoft.jfw.model.bussinessobject.attachments.AttachmentItem;
import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.bussinessobject.core.JfwDraft;
import com.progressoft.jfw.model.bussinessobject.security.Tenant;
import com.progressoft.jfw.model.bussinessobject.security.User;
import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.jfw.shared.JfwCurrencyInfo;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.BalancePostingResult;
import com.progressoft.mpay.entity.JVsDetailsFilter;
import com.progressoft.mpay.entity.MessageInquiryFilter;
import com.progressoft.mpay.entity.TransactionDetailsFilter;
import com.progressoft.mpay.mpclear.plugins.MPClearProcessingResult;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public interface IDataProvider {
	MPAY_CustomerMobile getCustomerMobile(long id);

	MPAY_CustomerMobile getCustomerMobile(String mobileInfo);

	MPAY_CustomerMobile getCustomerMobile(String mobileInfo, String infoType);

	MPAY_CorpoarteService getCorporateService(String serviceInfo, String serviceInfoType);

	MPAY_CorpoarteService getCorporateServiceByMPClearAlias(String mpClearAlias);

	MPAY_CorpoarteService getCorporateService(long id);

	MPAY_Corporate getCorporate(String name);

	void persistNotification(MPAY_Notification notification);

	void persistAccount(MPAY_Account account);

	void persistMPClearIntegMessageLog(MPAY_MpClearIntegMsgLog messageLog);

	void persistFinancialIntegMessage(MPAY_FinancialIntegMsg message);

	MPAY_MPayMessage getMPayMessage(long id);

	MPAY_MPayMessage getMPayMessage(String reference);

	MPAY_MPayMessage getMPayMessageByMessageId(String messageId);

	MPAY_ChargesSchemeDetail getChargeScheme(String messageTypeCode, long chargeSchemeId);

	long getProfileLimitID(long profileId, long messageTypeId);

	MPAY_Corporate getCorporate(long id);

	MPAY_Customer getCustomer(long id);

	MPAY_NetworkManagement getNetworkManagement(long id);

	MPAY_CashIn getCashIn(long id);

	MPAY_CashOut getCashOut(long id);

	void presistCustomerIntegMessage(MPAY_CustIntegMessage message);

	void persistCorporateIntegMessage(MPAY_CorpIntegMessage message);

	void persistNetowkrIntegMessage(MPAY_NetworkManIntegMsg message);

	void persistBankIntegMessage(MPAY_BankIntegMessage message);

	void updateCustomer(MPAY_Customer customer);

	void updateCustomerMobile(MPAY_CustomerMobile customerMobile);

	void updateCorporate(MPAY_Corporate corporate);

	void updateNetworkManagement(MPAY_NetworkManagement networkManagement);

	void updateCashIn(MPAY_CashIn cashIn);

	void updateCashOut(MPAY_CashOut cashOut);

	void updateFinancialIntegMessage(MPAY_FinancialIntegMsg message);

	void updateBankIntegMessage(MPAY_BankIntegMessage message);

	BigDecimal getBalance(long accountId);

	MPAY_FinancialIntegMsg getFinancialMessageByRefMessage(String messageId);

	long getNextReferenceNumberSequence();

	void persistMPayMessage(MPAY_MPayMessage message);

	void persistProcessingResult(MessageProcessingResult result);

	void mergeProcessingResult(MessageProcessingResult result);

	void persistMPClearProcessingResult(MPClearProcessingResult result);

	void mergeMPClearMessageLog(MPAY_MpClearIntegMsgLog messageLog);

	MPAY_MpClearIntegMsgLog getMPClearMessage(String messageId);

	void updateMpclearMegLogToResponseReceived(String messageId);

	List<MPAY_MpClearIntegMsgLog> getPendingMPClearMessages(String messageId);

	void mergeIntegrationResult(IntegrationProcessingResult result);

	void persistIntegrationResult(IntegrationProcessingResult result);

	void persistCustomerDevice(MPAY_CustomerDevice device);

	void persistCorporateDevice(MPAY_CorporateDevice device);

	void mergeCustomerMobile(MPAY_CustomerMobile mobile);

	MPAY_CustomerDevice getCustomerDevice(String deviceId);

	void updateNotification(MPAY_Notification notification);

	void mergeProcessingContext(MessageProcessingContext context);

	MPAY_Transaction getTransactionByMessageId(long messageId);

	MPAY_Transaction getTransactionByTransactionRef(String transactionRef);

	void persistTransaction(MPAY_Transaction transaction);

	void mergeTransaction(MPAY_Transaction transaction);

	void mergeCashIn(MPAY_CashIn cashIn);

	void mergeCashOut(MPAY_CashOut cashOut);

	void mergeServiceCashOut(MPAY_ServiceCashOut serviceCashOut);

	MPAY_Account getAccount(long accountId);

	void mergeCustomerDevice(MPAY_CustomerDevice device);

	void mergeCorporateDevice(MPAY_CorporateDevice device);

	void mergeMPayMessage(MPAY_MPayMessage message);

	List<MPAY_Transaction> listMiniTransactions(MPAY_CustomerMobile mobile, int maxCount);

	List<MPAY_Transaction> listMiniTransactions(MPAY_CustomerMobile mobile, Timestamp fromTime, Timestamp toTime);

	List<MPAY_Transaction> listMiniTransactions(MPAY_CorpoarteService service, int maxCount);

	List<MPAY_Transaction> listMiniTransactions(MPAY_CorpoarteService service, Timestamp fromTime, Timestamp toTime);

	Boolean isAliasUsed(String alias);

	void persistServiceIntegrationMessage(MPAY_ServiceIntegMessage message);

	void updateServiceIntegrationMessage(MPAY_ServiceIntegMessage message);

	MPAY_ServiceIntegMessage getServiceIntegrationMessageByRequestId(String requestId);

	void persistExternalIntegrationMessage(MPAY_ExternalIntegMessage message);

	void updateExternalIntegrationMessage(MPAY_ExternalIntegMessage message);

	MPAY_ExternalIntegMessage getExternalIntegrationMessageByRequestId(String requestId);

	List<MPAY_Corporate> listActiveCorporates();

	MPAY_Customer getCustomer(String idType, String idValue);

	void refreshEntity(Object entity);

	MPAY_BankIntegMessage getBankIntegrationMessage(String requestID);

	void mergeCorporateService(MPAY_CorpoarteService service);

	MPAY_CorporateDevice getCorporateDevice(String deviceId);

	MPAY_ClientsOTP getOtpClientByCode(String code);

	MPAY_RegistrationOTP getRegOtpByCode(String code);

	void updateMobileAccount(MPAY_MobileAccount account);

	void updateServiceAccount(MPAY_ServiceAccount account);

	void updateAccount(MPAY_Account account);

	boolean updateAccountLimit(long accountId, long messageTypeId, BigDecimal amount, int count, Date date)
			throws SQLException;

	AccountLimits getAccountLimits(long accountId, long messageTypeId) throws SQLException;

	void persistOTP(MPAY_ClientsOTP otp);

	void persistRegOTP(MPAY_RegistrationOTP regOtp);

	boolean checkIfBankHasActiveAccounts(long bankId, long paymentServiceProviderId);

	MPAY_CorpoarteService getCorporateServiceByTypeWithoutTenant(String serviceInfo, String serviceInfoType);

	MPAY_MpClearIntegMsgLog getMPClearMessageLog(long id);

	BigDecimal getTotalCashBalance();

	void updateSystemParameter(String configKey, String configValue);

	WorkflowStatus getWorkflowStatus(String code);

	MPAY_NetworkManagement getLastNetworkManagement();

	BalancePostingResult callUpdateBalanceSP(long accountId, BigDecimal jvAmount, boolean isDebit) throws SQLException;

	void updateWorkflowStep(String stepKey, long workflowId);

	String getNextMPClearMessageId();

	List<MPAY_CustomerMobile> listCustomerMobiles(long customerId);

	List<MPAY_CustomerMobile> listCustomerMobiles(long customerId, String statusCode);

	List<MPAY_MobileAccount> listMobileAccounts(long mobileId);

	List<MPAY_MobileAccount> listMobileAccounts(long mobileId, String statusCode);

	List<MPAY_CustomerDevice> listMobileDevices(long mobileId);

	List<MPAY_CorpoarteService> listCorporateServices(long corporateId);

	List<MPAY_CorpoarteService> listCorporateServices(long corporateId, String statusCode);

	List<MPAY_ServiceAccount> listServiceAccounts(long serviceId);

	List<MPAY_ServiceAccount> listServiceAccounts(long serviceId, String statusCode);

	List<MPAY_CorporateDevice> listServiceDevices(long serviceId);

	List<Tenant> listTenants();

	MPAY_ServiceType getMpayServiceTypeByCode(String code);

	void deleteClientOTP(MPAY_ClientsOTP otp);

	List<MPAY_Transaction> searchTransactions(TransactionDetailsFilter filter);

	List<MPAY_JvDetail> searchJVs(JVsDetailsFilter filter);

	JfwCurrencyInfo getCurrencyInfo(JFWCurrency currency);

	long getNewAccountNumberSequence();

	MPAY_IDType getIdTypeByCode(String code);

	MPAY_BulkRegistrationAtt getBulkRegistrationAttachment(String recordId);

	void persistCustomerAtt(MPAY_BulkRegistrationAtt registrationAtt);

	MPAY_BulkPaymentAtt getBulkPaymentFileAttachment(String valueOf);

	void persistPaymentAttachment(MPAY_BulkPaymentAtt bulkPaymentAtt);

	JfwDraft getJFWDraft(long id);

	MPAY_BulkRegistration getBulkRegistration(long id);

	MPAY_BulkPayment getBulkPayment(long id);

	boolean uniqueNationalId(MPAY_Customer customer);

	boolean uniqueAlias(String value);

	boolean uniqueMobile(String value);

	void persistMEPSProcessingResult(MessageProcessingResult result);

	void mergeMEPSProcessingResult(MessageProcessingResult result);

	void mergeMEPSTransaction(MPAY_Transaction transaction);

	void mergeMEPSMPayMessage(MPAY_MPayMessage message);

	MPAY_ServiceIntegReason getSeviceIntegReasonByMappingCode(String mappingReasonCode, String processorCode);

	List<MPAY_ChargesScheme> listChargeSchemes();

	List<MPAY_LimitsScheme> listLimitSchemes();

	List<MPAY_RequestTypesScheme> listRequestTypeSchemes();

	void reverseTransaction(long id, String reversedBy);

	long getNumberOfCustomerMobiles(long customerId);

	long getNumberOfCorporateServices(long corporateId);

	User getUserByUserNameAndPassword(String userName, String password);

	List<MPAY_BanksUser> listBanksUsers();

	MPAY_BanksUser getBankUser(String userName, String password);

	void updateBankUser(MPAY_BanksUser bankUser);

	MPAY_Corporate getCorporateByIdTypeAndRegistrationId(String idTypeCode, String registrationId);

	MPAY_Corporate getCorporateByRegistrationId(String registrationId);

	void deleteRegOTP(MPAY_RegistrationOTP regOTP);

	MPAY_ClientsCommission getClientCommission(long messageTypeId, long accountId);

	void updateClientCommission(MPAY_ClientsCommission commission);

	MPAY_Customer getCustomerByNationalId(String nationalId);

	MPAY_Customer getCustomerByMobileNumber(String mobileNumber);

	void persistAmlCase(MPAY_AmlCase amlCase);

	MPAY_ExternalIntegMessage getSaudiPostIntegrationMessage(String mobileNumber, String nationalId, String reasonCode);

	MPAY_CorpoarteService getCorporateServiceByName(String serviceName);

	void persistATMVoucher(MPAY_ATM_Voucher voucher);

	void updateATMVoucher(MPAY_ATM_Voucher voucher);

	long getNextATMVoucherSequence();

	List<MPAY_ATM_Voucher> listMobileATMVouchers(long mobileId, Timestamp fromTime, Timestamp toTime, String voucher);

	List<MPAY_TaxScheme> listTaxSchemes();

	List<MPAY_CommissionScheme> listCommissionScheme();

	List<MPAY_ATM_Voucher> listPendingMobileATMVouchers(long mobileId);

	MPAY_ATM_Voucher getVoucher(String value);

	List<MPAY_Customer> getCustomersByExternalAccount(String mobileNumber, String excternalAccount);

	List<MPAY_Notification> getMPAYNotificationByRefMessageId(Long refMessageId);

	void updateVoucherToUser(MPAY_ATM_Voucher voucher);

	List<MPAY_MessageType> getMessageTypesByCodeOrName(String code, String name);

	void updateLanguageStatus(MPAY_Language language);

	void updateServiceCategoriesStatus(MPAY_ServicesCategory category);

	void updateEntity(Object o);

	MPAY_LimitsScheme getLimitSchemeByCode(String code);

	MPAY_TaxScheme getTaxSchemeByCode(String code);

	MPAY_CommissionScheme getCommissionScheneByCode(String code);

	MPAY_ChargesScheme getChargeSchemeByCode(String code);

	MPAY_RequestTypesScheme getRequestTypeScheme(String code);

	List<MPAY_ServiceType> getAllServiceTypesCategory();

	List<MPAY_Customer> getCustomersByExternalAccount(String excternalAccount);

	MPAY_Customer getCustomerByIdNumber(String idNumber);

	MPAY_Customer getCustomerByFullName(String fullName);

	MPAY_CustomerMobile getCustomerMobileByAlias(String alias);

    MPAY_CustomerMobile getCustomerMobileByNfcSerial(String nfcSerial);

    MPAY_ClientsOTP getOtpClientByFullData(String pinCode, Long messageTypeId, Long senderMobileId,
                                           Long senderServiceId);

	MPAY_ClientsOTP getExistingOtpByFullData(Long messageTypeId, Long senderMobileId,
			Long senderServiceId);

	void updateClientOtp(MPAY_ClientsOTP clientOtp);

    MPAY_Corporate getCorporateByServiceName(String serviceName);

    List<MPAY_MPayMessage> getMPayMessages(MessageInquiryFilter filter);

    List<MPAY_Notification> listOfNotificationsByReceiverMobileNumber(String receiverMobileNumber);

    String getNextCityCodeSequence();

	void persistObject(Object obj);

	<T extends JFWEntity> List<T> getEntitiesWhere(Class<T> entity, String where);

	<T extends AttachmentItem> List<T> getAttachmentsWhere(Class<T> entity, String where);

	void mergeEntity(JFWEntity entity);

    List<MPAY_CorpoarteService> getCorporatesByParentCorporateId(Long parentCorporateId);

    MPAY_Customer getCustomerByNfcSerial(String nfcSerial);

	void updateCorporateService(MPAY_CorpoarteService updateCorporateServiceFromRequest);

	MPAY_CorpoarteService getCorporateServiceByAlias(String alias);

	MPAY_Corporate getCorporateByAlias(String alias);

	void persistCustomerAtt(MPAY_CustomerAtt customerAtt);

	void persistInstallment(MPAY_Installment installment);

	List<MPAY_CorpoarteService> listServiceByFirstThreeLetters(String firstThreeLetters);

	boolean isOperationAllowedToRefund(Long id);
}