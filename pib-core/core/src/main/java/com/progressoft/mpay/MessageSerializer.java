package com.progressoft.mpay;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.FSTObjectInput;
import org.nustaq.serialization.FSTObjectOutput;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.mpclear.MpClearIntegMsg;
import com.solab.iso8583.IsoMessage;

public final class MessageSerializer {

	static FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();

	private MessageSerializer() {

	}

	public static String serialize(Notification notification) throws WorkflowException {
		String result = null;
		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			FSTObjectOutput out = conf.getObjectOutput(baos);
			out.writeObject(notification, MPAY_Notification.class);
			out.flush();
			result = Base64.encodeBase64String(baos.toByteArray());
			baos.close();

		} catch (IOException e) {
			throw new WorkflowException(e);
		}
		return result;
	}

	public static String serialize(MpClearIntegMsg message) {
		String result = null;
		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			FSTObjectOutput out = conf.getObjectOutput(baos);
			out.writeObject(message, MpClearIntegMsg.class);
			out.flush();
			result = Base64.encodeBase64String(baos.toByteArray());
			baos.close();

		} catch (IOException e) {
			throw new MPayGenericException("Error while serialze", e);
		}
		return result;
	}

	public static byte[] serialize(IsoMessage object) throws WorkflowException {
		byte[] value = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			FSTObjectOutput out = conf.getObjectOutput(baos);
			out.writeObject(object, IsoMessage.class);
			out.flush();
			baos.close();
			value = baos.toByteArray();

		} catch (IOException ex) {
			throw new WorkflowException(ex);
		}
		return value;
	}

	public static Notification deSerializeNotification(String strMessage) throws WorkflowException {
		Notification oNotification = null;
		try {
			ByteArrayInputStream baos = new ByteArrayInputStream(Base64.decodeBase64(strMessage));
			FSTObjectInput in = conf.getObjectInput(baos);
			oNotification = (Notification) in.readObject(Notification.class);
			baos.close();

		} catch (Exception e) {
			throw new WorkflowException(e);
		}
		return oNotification;
	}

	public static MpClearIntegMsg deSerializeIntegMsg(String strMessage) throws WorkflowException {
		MpClearIntegMsg oMessage = null;
		try {
			ByteArrayInputStream baos = new ByteArrayInputStream(Base64.decodeBase64(strMessage));
			FSTObjectInput in = conf.getObjectInput(baos);
			oMessage = (MpClearIntegMsg) in.readObject(MpClearIntegMsg.class);
			baos.close();

		} catch (Exception e) {
			throw new WorkflowException(e);
		}
		return oMessage;
	}
}
