package com.progressoft.mpay.registration.corporates;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_CorporateKyc;
import com.progressoft.mpay.registration.customers.CustomerHandler;
import com.progressoft.mpay.templates.KycTemplateHandler;
import com.progressoft.mpay.templates.TemplateBehaviour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

public class CorporateHandler implements ChangeHandler {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(CustomerHandler.class);

    @Override
    public void handle(ChangeHandlerContext context) throws InvalidInputException {
        logger.debug("inside CorporateHandler*******************-----------------");
        try {
            CorporatesChangeHandler handler = (CorporatesChangeHandler) Class.forName(MPayContext.getInstance().getSystemParameters().getCorporatesChangeHandler()).newInstance();
            handler.handleCorporate(context);

            MPAY_Corporate corporate = (MPAY_Corporate) context.getEntity();
            initEnablesChannels(context, corporate);
            MPAY_CorporateKyc kycTemplate = corporate.getKycTemplate();
            if (Objects.nonNull(kycTemplate)) {
                KycTemplateHandler kycTemplateHandler = new KycTemplateHandler(corporate);
                kycTemplateHandler.handleTemplateOf(kycTemplate, TemplateBehaviour.VISIBLE, context);
                kycTemplateHandler.handleTemplateOf(kycTemplate, TemplateBehaviour.ENABLED, context);
                kycTemplateHandler.handleTemplateOf(kycTemplate, TemplateBehaviour.REQ, context);
            }

            MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "code", MPayContext.getInstance().getSystemParameters().getDefaultBankCode());
            corporate.setBank(defaultBank);
        } catch (Exception e) {
            logger.error("Failed to create change handler instance", e);
            throw new InvalidInputException(e.getMessage());
        }
    }

    private void initEnablesChannels(ChangeHandlerContext context, MPAY_Corporate corporate) {
        if (corporate.getStatusId() == null) {
            corporate.setEnableEmail(false);
            corporate.setEnablePushNotification(false);
            corporate.setEnableSMS(false);
            context.getFieldsAttributes().get(MPAY_Corporate.EMAIL).setRequired(corporate.getEnableEmail());
            context.getFieldsAttributes().get(MPAY_Corporate.MOBILE_NUMBER).setRequired(corporate.getEnableSMS());
        }
    }
}
