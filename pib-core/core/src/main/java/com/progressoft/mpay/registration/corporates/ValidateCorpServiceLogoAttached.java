package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.attachment.AttachmentContext;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CorpoarteServiceAtt;
import com.progressoft.mpay.entities.MPAY_CorpoarteServicesViewAtt;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.progressoft.mpay.MPayErrorMessages.SERVICE_LOGO_NOT_FOUND;

@Component
public class ValidateCorpServiceLogoAttached implements Validator {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(ValidateCorpServiceLogoAttached.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside ValidateCreateCorporateService-----");
//        MPAY_CorpoarteService corpoarteService = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);
//        String where = "recordId=" + corpoarteService.getId().toString() + " AND imageType='1'";
//        String viewName = arg0.get(WorkflowService.WF_ARG_VIEW_NAME).toString();
//        Long itemCount;
//
//        if (viewName.equals(MPAYView.CORPORATE_SERVICES.viewName))
//            itemCount = itemDao.getItemCount(MPAY_CorpoarteServiceAtt.class, null, where);
//        else
//            itemCount = itemDao.getItemCount(MPAY_CorpoarteServicesViewAtt.class, null, where);
//
//        if (itemCount == 0) {
//            InvalidInputException iie = new InvalidInputException();
//            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(SERVICE_LOGO_NOT_FOUND));
//            throw iie;
//        }
    }
}
