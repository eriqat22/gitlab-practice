package com.progressoft.mpay.registration.customers;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class PreCreateCustomerMobile implements FunctionProvider {

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) transientVars.get(WorkflowService.WF_ARG_BEAN);
        mobile.setIsActive(false);
        mobile.setIsBlocked(false);
        mobile.setRetryCount(0L);
        mobile.setEnableSMS(true);
    }
}

