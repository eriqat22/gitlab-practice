package com.progressoft.mpay.profiles;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_Profile;

public class Profile {

	private Profile() {

	}

	public static MPAY_Profile getDefault() throws InvalidInputException {
		ItemDao itemDao = (ItemDao) AppContext.getApplicationContext().getBean("itemDao");
		String defaultProfileSysConfigValue = SystemParameters.getInstance().getDefaultProfile();
		MPAY_Profile profile = itemDao.getItem(MPAY_Profile.class, "code", defaultProfileSysConfigValue);

		if (profile == null)
			throw new InvalidInputException("Default profile is not defined");
		return profile;
	}
}
