package com.progressoft.mpay.registration.corporates;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.MPAYView;

public class PostApproveCorporateReactivate implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostApproveCorporateReactivate.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_Corporate corporate = (MPAY_Corporate) transientVars.get(WorkflowService.WF_ARG_BEAN);
		corporate.setIsActive(true);
		handleServices(corporate);
	}

	private void handleServices(MPAY_Corporate corporate) throws WorkflowException {
		logger.debug("inside HandleServices--------------------------");
		List<MPAY_CorpoarteService> services = DataProvider.instance().listCorporateServices(corporate.getId(), CorporateServiceWorkflowStatuses.SUSPENDED.toString());
		if (services == null || services.isEmpty())
			return;
		Iterator<MPAY_CorpoarteService> servicesIterator = services.listIterator();
		do {
			MPAY_CorpoarteService service = servicesIterator.next();
			JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, CorporateServiceWorkflowServiceActions.REACTIVATE, new WorkflowException());
		} while (servicesIterator.hasNext());
	}
}
