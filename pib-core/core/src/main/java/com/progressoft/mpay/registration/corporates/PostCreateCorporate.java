package com.progressoft.mpay.registration.corporates;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.AbstractJFWEntity;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_CorporateAtt;
import com.progressoft.mpay.entity.MPAYView;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class PostCreateCorporate implements FunctionProvider {

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
        corporate.setIsRegistered(false);
        corporate.setIsActive(true);
        try {
//            saveLogo(corporate);
            MPAY_CorpoarteService service = fillService(corporate);
            JfwHelper.createEntity(MPAYView.CORPORATE_SERVICES, service);
        } catch (InvalidActionException ex) {
            throw new WorkflowException(getError(ex), ex);
        } catch (BusinessException ex) {
            if (ex.getCause() instanceof InvalidInputException)
                throw (InvalidInputException) ex.getCause();
            else
                throw new WorkflowException(getError(ex), ex);
        }
    }

    private void saveLogo(MPAY_Corporate corporate) throws IOException {
        List<MPAY_CorporateAtt> attachments = MPayContext.getInstance().getDataProvider()
                .getAttachmentsWhere(MPAY_CorporateAtt.class, "recordId = '" + corporate.getId() + "'");
        String imagesPath = MPayContext.getInstance().getSystemParameters().getImagePath();
        MPAY_CorporateAtt attachment = Collections.max(attachments, Comparator.comparing(AbstractJFWEntity::getCreationDate));
        if (attachment != null && imagesPath != null)
            FileUtils.writeByteArrayToFile(new File(Paths.get(imagesPath).resolve(attachment.getName()).toUri()), attachment.getAttFile());
    }

    private String getError(Throwable exceptionCause) {
        String error = exceptionCause.getMessage();
        if (InvalidInputException.class.isInstance(exceptionCause)) {
            InvalidInputException iie = (InvalidInputException) exceptionCause;
            return (String) iie.getErrors().values().toArray()[0];
        }

        if (exceptionCause.getCause() != null)
            return getError(exceptionCause.getCause());
        return error;
    }

    private MPAY_CorpoarteService fillService(MPAY_Corporate corporate) {
        MPAY_CorpoarteService service = new MPAY_CorpoarteService();

        service.setName(corporate.getServiceName());
        service.setDescription(corporate.getServiceDescription());
        service.setAlias(corporate.getAlias());
        service.setCategory(corporate.getCategory());
        service.setMpclearAlias(service.getMpclearAlias());
        service.setBankedUnbanked(corporate.getBankedUnbanked());
        service.setBank(corporate.getBank());
        service.setExternalAcc(corporate.getExternalAcc());
        service.setMas(Long.parseLong(corporate.getMas()));
        service.setPaymentType(corporate.getPaymentType());
        service.setIsActive(corporate.getIsActive());
//        service.setNotificationChannel(corporate.getServiceNotificationChannel());
//        service.setNotificationReceiver(corporate.getServiceNotificationReceiver());
        service.setRefProfile(corporate.getRefProfile());
        service.setServiceCategory(corporate.getServiceCategory());
        service.setServiceType(corporate.getServiceType());
        service.setRefCorporate(corporate);
        service.setIban(corporate.getIban());
        service.setMaxNumberOfDevices(corporate.getMaxNumberOfDevices());
        service.setIsBlocked(false);
        service.setRetryCount(0L);
        if (corporate.getRefCorporateCorpoarteServices() == null)
            corporate.setRefCorporateCorpoarteServices(new ArrayList<>());
        corporate.getRefCorporateCorpoarteServices().add(service);
        service.setEmail(corporate.getEmail());
        service.setMobileNumber(corporate.getMobileNumber());
        service.setEnableEmail(true);
        service.setEnableSMS(true);
        service.setEnablePushNotification(true);
        service.setCity(corporate.getCity());
        service.setArea(corporate.getArea());
        service.setRating("0");
        service.setRateCount(0L);
        service.setNotificationChannel(MPayContext.getInstance().getLookupsLoader().getNotificationChannel(NotificationChannelsCode.SMS));
        service.setEnableEmail(corporate.getEnableEmail());
        service.setEnableSMS(corporate.getEnableSMS());
        service.setEnablePushNotification(corporate.getEnablePushNotification());
        return service;
    }
}
