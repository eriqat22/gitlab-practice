package com.progressoft.mpay.payments.cashout;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entities.MPAY_ServiceCashOut;
import com.progressoft.mpay.entity.MessageCodes;
import com.progressoft.mpay.tax.TaxCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class ServiceCashOutIsChargeIncluded implements ChangeHandler {

    private static final String REF_SERVICE_ACCOUNT = "refServiceAccount";
    private static final Logger logger = LoggerFactory.getLogger(ServiceCashOutIsChargeIncluded.class);

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside handle ==================");

        MPAY_ServiceCashOut cashOut = (MPAY_ServiceCashOut) changeHandlerContext.getEntity();
        if (cashOut.getAmount() != null && cashOut.getAmount().doubleValue() > 0 && cashOut.getRefCorporateService() != null) {
            MPAY_ServiceAccount account = SystemHelper.getServiceAccount(MPayContext.getInstance().getSystemParameters(), cashOut.getRefCorporateService(), cashOut.getRefServiceAccount());
            MPAY_Profile profile = account.getRefProfile();
            BigDecimal chargeAmount = ChargesCalculator.calculateCharge(MPayContext.getInstance().getDataProvider(), cashOut.getAmount(), MessageCodes.SRVCO.toString(), profile, true);
            BigDecimal taxAmount = TaxCalculator.calculateTax(MPayContext.getInstance().getDataProvider(), chargeAmount, MessageCodes.SRVCO.toString(), profile, MPayContext.getInstance().getSystemParameters().getDefaultCurrency());
            cashOut.setTransAmount(cashOut.getAmount());
            cashOut.setChargeAmount(chargeAmount);
            cashOut.setTaxAmount(taxAmount);
            cashOut.setTotalAmount(cashOut.getAmount());
        }

        if (cashOut.getRefCorporateService() != null) {
            changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setEnabled(true);
            changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setRequired(true);
        } else {
            changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setEnabled(false);
            changeHandlerContext.getFieldsAttributes().get(REF_SERVICE_ACCOUNT).setRequired(false);
            cashOut.setRefServiceAccount(null);
        }
    }

}