package com.progressoft.mpay.charges;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_CommissionParameter;
import com.progressoft.mpay.entities.MPAY_CommissionSlice;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class PostCreateCommissionSlice implements FunctionProvider {

    private static Map<String, String> maps = new HashMap<>();

    static {
        maps.put("TOTAL_COUNT", "-1");
        maps.put("TOTAL_AMOUNT", "-1");
        maps.put("COMMISSION_AMOUNT", "0");
        maps.put("COMMISSION_TYPE", "FIXED");
        maps.put("DIRECTION", "SENDER");
        maps.put("SENDER_PERCENTAGE", "0");
        maps.put("RECEIVER_PERCENTAGE", "0");
        maps.put("PERCENTAGE_MIN_VALUE", "0");
        maps.put("PERCENTAGE_MAX_VALUE", "0");
        maps.put("COMMISSION_MIN_AMOUNT", "0");
        maps.put("COMMISSION_MAX_AMOUNT", "0");
        maps.put("PARENT_PERCENTAGE", "0");
    }

    private static final Logger logger = LoggerFactory.getLogger(PostApproveCommissionSlice.class);

    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PostCreateCommissionSlice ----------------");

        try {
            MPAY_CommissionSlice slice = (MPAY_CommissionSlice) arg0.get(WorkflowService.WF_ARG_BEAN);

            for (Map.Entry<String, String> entry : maps.entrySet()) {
                MPAY_CommissionParameter parameter = new MPAY_CommissionParameter();
                parameter.setRefSlice(slice);
                parameter.setName(entry.getKey());
                parameter.setValue(entry.getValue());
                parameter.setOrgId(slice.getOrgId());
                parameter.setTenantId(slice.getTenantId());

                JfwHelper.createEntity(MPAYView.COMMISSIONS_PARAMETERS, parameter);
            }
        } catch (InvalidActionException e) {
            logger.error(e.getMessage());
            throw new WorkflowException(e);
        }
    }

}
