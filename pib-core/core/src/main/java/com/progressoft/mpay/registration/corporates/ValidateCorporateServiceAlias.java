package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class ValidateCorporateServiceAlias implements Validator {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ValidateCorporateServiceAlias.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("ValidateCorporateServiceAlias-+-+-+*-+-+-+-");
		MPAY_CorpoarteService corpService = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN_DRAFT);
		InvalidInputException cause = Unique.validate(itemDao, corpService, "alias", corpService.getAlias(), MPayErrorMessages.SERVICE_INVALID_ALIAS_IS_USED);
		if (cause.getErrors().size() > 0)
			throw cause;
	}
}
