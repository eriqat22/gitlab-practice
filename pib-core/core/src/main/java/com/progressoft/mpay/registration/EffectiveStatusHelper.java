package com.progressoft.mpay.registration;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import com.progressoft.jfw.model.bussinessobject.core.JfwDraft;
import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.jfw.model.dao.item.ItemDao;

public class EffectiveStatusHelper {

	private EffectiveStatusHelper() {

	}

	public static WorkflowStatus getEffectiveStatus(JFWEntity entity) throws WorkflowException {

		if (entity == null)
			throw new WorkflowException("GetEffectiveStatus: Entity is null");
		WorkflowStatus status = entity.getStatusId();

		if (entity.getJfwDraft() != null) {
			status = entity.getJfwDraft().getStatusId();
		}
		return status;
	}

	public static WorkflowStatus getEffectiveStatus(JFWEntity entity, ItemDao itemDao) {

		WorkflowStatus status;

		if (entity.getJfwDraft() != null) {
			status = entity.getJfwDraft().getStatusId();
		} else {
			JfwDraft draft = itemDao.getItem(JfwDraft.class, entity.getJfwDraft().getId());
			status = draft.getStatusId();
		}
		return status;
	}

}