package com.progressoft.mpay.entity;

public class BalanceTypes {
	public static final String DEBIT = "1";
	public static final String CREDIT = "2";
	public static final String NORMAL = "3";

	private BalanceTypes() {

	}
}
