package com.progressoft.mpay.registration.customers;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.jfw.model.bussinessobject.core.IJFWEntity;
import com.progressoft.mpay.entity.BankedUnbankedFlag;

public class CustomerMobileBankedUnbankedChangeHandler implements ChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(CustomerMobileBankedUnbankedChangeHandler.class);

	@Override
	public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
		logger.debug("inside CustomerMobileBankedUnbankedChangeHandler ------------");

		IJFWEntity entity = changeHandlerContext.getEntity();
		String value;
		try {
			value = (String) PropertyUtils.getProperty(entity, "bankedUnbanked");
			if (value == null)
				return;

			if (BankedUnbankedFlag.BANKED.equals(value))
				changeHandlerContext.getFieldsAttributes().get("externalAcc").setRequired(true);
			else if (BankedUnbankedFlag.UNBANKED.equals(value))
				changeHandlerContext.getFieldsAttributes().get("externalAcc").setRequired(false);

			if (BankedUnbankedFlag.UNBANKED.equals(value) || BankedUnbankedFlag.BANKED.equals(value))
				changeHandlerContext.getFieldsAttributes().get("bank").setClearSelections(true);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			logger.error("error while changing the radio group values banked or unbanked", e);
		}
	}
}
