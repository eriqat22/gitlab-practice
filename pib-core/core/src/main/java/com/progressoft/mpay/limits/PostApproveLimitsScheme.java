package com.progressoft.mpay.limits;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;
import com.progressoft.mpay.entity.MPAYView;

public class PostApproveLimitsScheme implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostApproveLimitsScheme.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside PostApproveLimitScheme.java-------");
		MPAY_LimitsScheme limitScheme = (MPAY_LimitsScheme) arg0.get(WorkflowService.WF_ARG_BEAN);

		for (MPAY_Limit limit : limitScheme.getRefSchemeLimits()) {
			Map<Option, Object> options = new EnumMap<>(Option.class);
			options.put(Option.ACTION_NAME, "SVC_Approve");
			JfwHelper.executeAction(MPAYView.LIMITS, limit, options);
		}
	}
}
