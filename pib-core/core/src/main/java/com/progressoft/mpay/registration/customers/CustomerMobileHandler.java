package com.progressoft.mpay.registration.customers;

import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.registration.CommonClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.bussinessobject.ChangeHandler;
import com.progressoft.jfw.model.bussinessobject.ChangeHandlerContext;
import com.progressoft.mpay.MPayContext;
import org.springframework.beans.factory.annotation.Autowired;

public class CustomerMobileHandler implements ChangeHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomerMobileHandler.class);

    @Autowired
    ItemDao itemDao;

    @Override
    public void handle(ChangeHandlerContext changeHandlerContext) throws InvalidInputException {
        logger.debug("inside CustomerMobileHandler*******************-----------------");

        MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) changeHandlerContext.getEntity();

        if (mobile.getStatusId() == null || mobile.getNewMobileNumber() == null)
            changeHandlerContext.getFieldsAttributes().get("newMobileNumber").setVisible(false);
        else
            changeHandlerContext.getFieldsAttributes().get("newMobileNumber").setVisible(true);

        if (mobile.getStatusId() == null || mobile.getNewAlias() == null)
            changeHandlerContext.getFieldsAttributes().get("newAlias").setVisible(false);
        else
            changeHandlerContext.getFieldsAttributes().get("newAlias").setVisible(true);

        try {
            CustomersChangeHandler handler = (CustomersChangeHandler) Class.forName(MPayContext.getInstance().getSystemParameters().getCustomersChangeHandler()).newInstance();
            handler.handleCustomerMobile(changeHandlerContext);

            MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "code", MPayContext.getInstance().getSystemParameters().getDefaultBankCode());
            mobile.setBank(defaultBank);

        } catch (Exception e) {
            logger.error("Failed to create change handler instance", e);
            throw new InvalidInputException(e.getMessage());
        }
    }
}
