package com.progressoft.mpay.plugins;

public interface MessageProcessor {
	MessageProcessingResult processMessage(MessageProcessingContext context);
	IntegrationProcessingResult processIntegration(IntegrationProcessingContext context);
	MessageProcessingResult reverse(MessageProcessingContext context);
	MessageProcessingResult accept(MessageProcessingContext context);
	MessageProcessingResult reject(MessageProcessingContext context);

    default boolean isNeedToSessionId(){
    	return true;
	}
}