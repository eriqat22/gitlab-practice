package com.progressoft.mpay.migs.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.IServiceUserManager;
import com.progressoft.mpay.ISystemParameters;

@Controller
@RequestMapping("/migs")
public class MigsController {

//	@Autowired
	// @Qualifier("itemDaoWIthoutTenant")
//	private ItemDao itemDaoWithoutTenant;

//	@Autowired
//	private ItemDao itemDao;
//
//	@Autowired
//	private SystemParameters systemParameters;
//
//	@Autowired
//	private LookupsLoader lookupsLoader;
//
//	@Autowired
//	private ServiceUserManager serviceManager;


	private MigsContext migsContext;

	public MigsController() {
		intiatiateMigsContext();
	}

	public MigsController(IServiceUserManager serviceUserManager , ItemDao itemDao , ISystemParameters systemParameters , ILookupsLoader lookupsLoader , ItemDao itemDaoWithoutTenant) {
		intiatiateMigsContext();
		migsContext.setServiceUserManager(serviceUserManager);
		migsContext.setItemDao(itemDao);
		migsContext.setSystemParameters(systemParameters);
		migsContext.setLookupLoader(lookupsLoader);
		migsContext.setItemDaoWithoutTenant(itemDaoWithoutTenant);
	}

	private void intiatiateMigsContext() {
		migsContext = new MigsContext();
//		migsContext.setServiceUserManager(serviceManager);
//		migsContext.setLookupLoader(lookupsLoader);
//		migsContext.setSystemParameters(systemParameters);
//		migsContext.setItemDao(itemDao);
	}

	@RequestMapping("/request.migs")
	public String sendRequestToMigs(HttpServletRequest request) {
		migsContext.setHttpServletRequest(request);
		return MigsHandlerFactory.getMigsHandler(MigsHandlerFactory.REQUEST).handle(migsContext);

	}

	@ResponseBody
	@RequestMapping("/response.migs")
	public String getResponseFromMigs(HttpServletRequest response) {
		migsContext.setHttpServletRequest(response);
		return MigsHandlerFactory.getMigsHandler(MigsHandlerFactory.RESPONSE).handle(migsContext);
	}

	@RequestMapping("/error.migs")
	@ResponseBody
	public String generateMigsErrorPage(HttpServletRequest request) {
		return MigsHandlerFactory.getMigsHandler(MigsHandlerFactory.ERROR).handle(migsContext);
	}
}