package com.progressoft.mpay.networkmanagement;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_NetworkManagement;

public class SwitchAutoLogin implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(SwitchAutoLogin.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Switch AutoLogin.");
		MPAY_NetworkManagement ntw = (MPAY_NetworkManagement) arg0.get(WorkflowService.WF_ARG_BEAN);
		ntw.setIsAutoLogin(!ntw.getIsAutoLogin());
	}
}
