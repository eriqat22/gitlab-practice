package com.progressoft.mpay;

import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;
import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.exceptions.NullArgumentException;

public class MPayHelper {
	private static final Logger logger = LoggerFactory.getLogger(MPayHelper.class);

	private MPayHelper() {

	}

	public static MPAY_CustomerDevice getCustomerDevice(IDataProvider dataProvider, String deviceId, long mobileId) {
		logger.debug("Inside getCustomerDevice ==========");
		if (dataProvider == null)
			throw new NullArgumentException("dataProvider");
		return dataProvider.getCustomerDevice(deviceId);
	}

	public static MPAY_CorporateDevice getCorporateDevice(IDataProvider dataProvider, String deviceId, long serviceId) {
		logger.debug("Inside getCustomerDevice ==========");
		if (dataProvider == null)
			throw new NullArgumentException("dataProvider");
		return dataProvider.getCorporateDevice(deviceId);
	}

	public static MPAY_Reasons_NLS getReasonNLS(MPAY_Reason reason, MPAY_Language language)
	{
		Optional<MPAY_Reasons_NLS> reasonNLS = (new ArrayList<MPAY_Reasons_NLS>(reason.getReasonsNLS().values()))
				.stream().filter(c -> c.getLanguageCode().equals(language.getCode())).findFirst();
		if(!reasonNLS.isPresent())
			return null;
		return reasonNLS.get();
	}
}
