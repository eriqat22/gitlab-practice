package com.progressoft.mpay.payments.cashin;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.entities.MPAY_ServiceCashIn;

public class ValidateServiceCashInRequest implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(ValidateServiceCashInRequest.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside validate ======================");

		InvalidInputException iie = new InvalidInputException();

		MPAY_ServiceCashIn cashin = (MPAY_ServiceCashIn) arg0.get(WorkflowService.WF_ARG_BEAN);

		if (cashin.getRefCorporateService() == null) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.CORPORATE_SERVICE_ISREQUIRED));
		}

		if (!cashin.getRefCorporateService().getRefCorporate().getIsActive()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.CORPORATE_ISNOT_ACTIVE));
		}

		if (!cashin.getRefCorporateService().getIsActive()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.CORPORATE_SERVICE_ISNOT_ACTIVE));
		}

		if (cashin.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.AMOUNT_SHOULD_BE_GREATER_THAN_ZERO));
		}

		if (iie.getErrors().size() > 0) {
			throw iie;
		}
	}
}