package com.progressoft.mpay.charges;

import java.math.BigDecimal;
import java.util.Map;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_ChargesSlice;
import com.progressoft.mpay.entity.ChargeTypes;

public class ValidateChargesSlice implements Validator {

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        MPAY_ChargesSlice slice = (MPAY_ChargesSlice) arg0.get(WorkflowService.WF_ARG_BEAN);
        processFinMessages(slice);
        processChargeType(slice);
    }

    private void processChargeType(MPAY_ChargesSlice slice) throws InvalidInputException {
        InvalidInputException iie = new InvalidInputException();
        if (slice.getChargeType().contains(ChargeTypes.FIXED)) {
            if (slice.getChargeAmount().compareTo(BigDecimal.ZERO) <= 0) {
                iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("transaction.invalid.chargeamount"));
                throw iie;
            }
        } else {
            if (slice.getChargePercent().compareTo(BigDecimal.ZERO) <= 0) {
                iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("transaction.invalid.chargepercentage"));
                throw iie;
            }
            if (slice.getMinAmount().compareTo(BigDecimal.ZERO) <= 0) {
                iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("transaction.invalid.minamount"));
                throw iie;
            }
            if (slice.getMaxAmount().compareTo(BigDecimal.ZERO) <= 0) {
                iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("transaction.invalid.maxamount"));
                throw iie;
            }
        }
    }

    private void processFinMessages(MPAY_ChargesSlice slice) throws InvalidInputException {
        InvalidInputException iie = new InvalidInputException();
        if (slice.getRefChargesDetails().getMsgType().getIsFinancial()) {
            if (slice.getTxAmountLimit().compareTo(BigDecimal.ZERO) <= 0) {
                iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("transaction.invalid.amountlimit"));
                throw iie;
            }

        } else {
            if (slice.getTxCountLimit() <= 0) {
                iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("transaction.invalid.countlimit"));
                throw iie;
            }
        }
    }

}
