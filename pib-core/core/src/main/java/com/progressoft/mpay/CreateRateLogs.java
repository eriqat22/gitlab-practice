package com.progressoft.mpay;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Rate;
import com.progressoft.mpay.entities.MPAY_RatesLogsRpt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

@Component
public class CreateRateLogs implements FunctionProvider {

    @Autowired
    private ItemDao itemDao;

    @Override
    public void execute(Map map, Map map1, PropertySet propertySet) throws WorkflowException {

        MPAY_Rate rate = (MPAY_Rate) map.get(WorkflowService.WF_ARG_BEAN);

        MPAY_RatesLogsRpt ratesLog = new MPAY_RatesLogsRpt();
        ratesLog.setFromDate(new Timestamp(new Date().getTime()));
        ratesLog.setToDate(new Timestamp(new Date().getTime()));
        ratesLog.setBaseCurrency(rate.getBaseCurrency());
        ratesLog.setForeignCurrency(rate.getForeignCurrency());
        ratesLog.setRate(rate.getRate());
        ratesLog.setMargin(rate.getMargin());
        ratesLog.setTenantId(rate.getTenantId());
        ratesLog.setOrgId(rate.getOrgId());

        itemDao.persist(ratesLog);
    }
}
