package com.progressoft.mpay.exceptions;

/**
 * Created by user on 27/02/17.
 */
public class WorkflowExecutionException extends RuntimeException {
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public WorkflowExecutionException(Exception ex) {
        super(ex);
    }
}
