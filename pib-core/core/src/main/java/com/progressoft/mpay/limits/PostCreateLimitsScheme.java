package com.progressoft.mpay.limits;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;
import com.progressoft.mpay.entities.MPAY_LimitsType;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entity.MPAYView;

public class PostCreateLimitsScheme implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCreateLimitsScheme.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		try {
			logger.debug("Inside PostCreateLimitsScheme----------------");
			MPAY_LimitsScheme limitScheme = (MPAY_LimitsScheme) arg0.get(WorkflowService.WF_ARG_BEAN);
			List<MPAY_LimitsType> limitsTypes = LookupsLoader.getInstance().listLimitsTypes();
			processLimits(limitScheme, limitsTypes);
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}

	private void processLimits(MPAY_LimitsScheme limitScheme, List<MPAY_LimitsType> limitsTypes) throws InvalidActionException {
		List<MPAY_MessageType> msgTypes = LookupsLoader.getInstance().listMessageTypes();
		for (MPAY_LimitsType type : limitsTypes) {
            MPAY_Limit limit = new MPAY_Limit();
            limit.setDescription(type.getDescription() + " " + limitScheme.getName());
            limit.setIsActive(false);
            limit.setRefType(type);
            limit.setRefScheme(limitScheme);
			limit.setTotalAmount(BigDecimal.ZERO);
			limit.setTotalCount(0L);
			limit.setCreditorTotalAmount(BigDecimal.ZERO);
            JfwHelper.createEntity(MPAYView.LIMITS, limit);
            processMessageTypes(msgTypes, limit);
        }
	}

	private void processMessageTypes(List<MPAY_MessageType> msgTypes, MPAY_Limit limit) throws InvalidActionException {
		for (MPAY_MessageType messageType : msgTypes) {
            if (!messageType.getIsFinancial())
                continue;
            MPAY_LimitsDetail limitDetails = new MPAY_LimitsDetail();
            limitDetails.setIsActive(false);
            limitDetails.setTxAmountLimit(BigDecimal.ZERO);
            limitDetails.setTxCountLimit(0L);
            limitDetails.setRefLimit(limit);
            limitDetails.setMsgType(messageType);
            limitDetails.setCheckTotalAmount(true);
            logger.debug("Setting limit details for message " + messageType.getDescription());
            JfwHelper.createEntity(MPAYView.LIMITS_DETAILS, limitDetails);
        }
	}
}