package com.progressoft.mpay.registration.customers;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_BulkRegistration;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.MPAYView;

public class PostCreateCustomer implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostCreateCustomer.class);
	private static final String REFERRAL_KEY_LENGTH = "Referral Code Length";

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("inside PostCreateCustomer====");

		MPAY_Customer customer = (MPAY_Customer) arg0.get(WorkflowService.WF_ARG_BEAN);

		customer.setFullName(customer.getFirstName() + " " + customer.getMiddleName() + " " + customer.getLastName());
		customer.setIsRegistered(false);
//		customer.setIsActive(true);
		try {
			customer.setReferralkey(fillReferralKey(customer));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		if (customer.getMobileNumber() == null || customer.getMobileNumber().trim().isEmpty())
			return;
		try {
			MPAY_CustomerMobile mobile = fillMobile(customer);
			if (customer.getBulkRegistration() == null) {
				MPAY_BulkRegistration bulkRegistration = MPayContext.getInstance().getDataProvider()
						.getBulkRegistration(
								MPayContext.getInstance().getSystemParameters().getBulkRegistrationDefaultId());
				if (bulkRegistration == null)
					throw new InvalidInputException("Default Bulk Registration Not Found");
				customer.setBulkRegistration(bulkRegistration);
			}
			JfwHelper.createEntity(MPAYView.CUSTOMER_MOBILES, mobile);
		} catch (InvalidActionException e) {
			throw new WorkflowException(getError(e), e);
		} catch (BusinessException be) {
			if (be.getCause() instanceof InvalidInputException) {
				throw (InvalidInputException) be.getCause();
			} else {
				throw new WorkflowException(getError(be), be);
			}
		}
	}

	private String getError(Throwable cause) {
		String error = cause.getMessage();

		if (InvalidInputException.class.isInstance(cause)) {
			InvalidInputException iie = (InvalidInputException) cause;
			error = (String) iie.getErrors().values().toArray()[0];
			return error;
		}

		if (cause.getCause() != null)
			error = getError(cause.getCause());

		return error;
	}

	private MPAY_CustomerMobile fillMobile(MPAY_Customer customer) {
		MPAY_CustomerMobile mobile = new MPAY_CustomerMobile();

		mobile.setNfcSerial(customer.getNfcSerial());
		mobile.setNotificationShowType(customer.getNotificationShowType());
		mobile.setBankedUnbanked(customer.getBankedUnbanked());
		mobile.setBank(customer.getBank());
		mobile.setExternalAcc(customer.getExternalAcc());
		mobile.setChannelType(LookupsLoader.getInstance().listChannelTypes().get(0));
		mobile.setDailyBalanceTime("0");
		mobile.setExternalAcc(customer.getExternalAcc());
		mobile.setIsActive(customer.getIsActive());
		mobile.setMobileNumber(customer.getMobileNumber());
		mobile.setRefProfile(customer.getRefProfile());
		mobile.setRefCustomer(customer);
		mobile.setNotificationShowType(customer.getNotificationShowType());
		mobile.setBalanceThreshold(1000000F);
		mobile.setAlias(customer.getAlias());
		mobile.setHint(customer.getHint());
		mobile.setRetryCount(0L);
		mobile.setIsBlocked(false);
		mobile.setMas(customer.getMas());
		mobile.setIban(customer.getIban());
		mobile.setMaxNumberOfDevices(customer.getMaxNumberOfDevices());
		mobile.setEmail(customer.getEmail());
		mobile.setEnableEmail(customer.getEnableEmail());
		mobile.setEnableSMS(customer.getEnableSMS());
		mobile.setEnablePushNotification(customer.getEnablePushNotification());


		if (customer.getRefCustomerCustomerMobiles() == null)
			customer.setRefCustomerCustomerMobiles(new ArrayList<MPAY_CustomerMobile>());
		customer.getRefCustomerCustomerMobiles().add(mobile);

		return mobile;
	}

	private String fillReferralKey(MPAY_Customer customer) throws NoSuchAlgorithmException {

		String configValue = LookupsLoader.getInstance().getSystemConfigurations(REFERRAL_KEY_LENGTH).getConfigValue();
		String customerId = String.valueOf(customer.getId());
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(customerId.getBytes(StandardCharsets.UTF_8));
		String base64String = new BigInteger(1, hash).toString(36);

		return base64String.substring(0, Integer.parseInt(configValue));
	}
}
