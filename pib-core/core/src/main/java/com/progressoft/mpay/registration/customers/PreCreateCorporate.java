package com.progressoft.mpay.registration.customers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.entities.MPAY_Corporate;

@Component
public class PreCreateCorporate implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PreCreateCorporate.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute ============");
		MPAY_Corporate corporate = (MPAY_Corporate) arg0.get(WorkflowService.WF_ARG_BEAN);
		corporate.setIsActive(true);
	}

}
