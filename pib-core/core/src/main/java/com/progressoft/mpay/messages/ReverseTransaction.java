package com.progressoft.mpay.messages;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidInputException;
import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class ReverseTransaction implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(ReverseTransaction.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("Inside ReverseTransaction ==========");
        MPAY_Transaction transaction = (MPAY_Transaction) arg0.get(WorkflowService.WF_ARG_BEAN);
        transaction.setReversedBy(AppContext.getCurrentUser().getUsername());
        MessageProcessor processor = MessageProcessorHelper.getProcessor(transaction.getRefOperation());
        if (processor == null)
            throw new WorkflowException("Service Unavailable, no available processor to handle");

        try {
            MessageProcessingContext context = new MessageProcessingContext(DataProvider.instance(), LookupsLoader.getInstance(), SystemParameters.getInstance(), new MPayCryptographer(), NotificationHelper.getInstance());
            context.setTransaction(transaction);
            context.setMessage(transaction.getRefMessage());
            if (context.getMessage() != null) {
                context.setRequest(MPayRequest.fromJson(context.getMessage().getRequestContent()));
                context.setLanguage(LookupsLoader.getInstance().getLanguageByCode(String.valueOf(context.getRequest().getLang())));
            }
            ProcessingContextSide sender = new ProcessingContextSide();
            ProcessingContextSide receiver = new ProcessingContextSide();
            sender.setMobile(transaction.getSenderMobile());
            sender.setService(transaction.getSenderService());
            receiver.setMobile(transaction.getReceiverMobile());
            receiver.setService(transaction.getReceiverService());

            context.setSender(sender);
            context.setReceiver(receiver);
            MessageProcessingResult result = processor.reverse(context);
            if (result == null)
                throw new WorkflowException("Reverse not applicable in this kind of transactions");
            if (result.getReasonCode().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                throw new InvalidInputException("Insufficient funds");
            }
            sendNotifications(result.getNotifications());
        } catch (Exception e) {
            if (e instanceof com.progressoft.jfw.shared.exception.InvalidInputException) {
                com.opensymphony.workflow.InvalidInputException iie = new com.opensymphony.workflow.InvalidInputException();
                iie.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS, e.getMessage());
                throw iie;
            }
            throw new WorkflowException(e.getMessage(), e);
        }
    }

    private void sendNotifications(List<MPAY_Notification> notifications) {
        if (notifications == null || notifications.isEmpty())
            return;
        for (MPAY_Notification notification : notifications) {
            try {
                DataProvider.instance().persistNotification(notification);
                NotificationHelper.getInstance().sendToActiveMQ(notification, SmsNotificationType.GENERAL);
            } catch (Exception e) {
                logger.error("An error while sending notification ", e);
            }
        }
    }
}
