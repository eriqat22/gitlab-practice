package com.progressoft.mpay.messages;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static ch.lambdaj.Lambda.selectFirst;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_JvDetail;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.BalancePostingResult;
import com.progressoft.mpay.entity.JournalVoucherFlags;
import com.progressoft.mpay.entity.JvDetailsTypes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.transactions.TransactionCreator;
import com.progressoft.mpay.transactions.TransactionCreatorFactory;

public class TransactionHelper {

	private static final String REVERSAL_FOR_ACCOUNT = "Reversal for account: ";
	private static final Logger logger = LoggerFactory.getLogger(TransactionHelper.class);

	private TransactionHelper() {

	}

	public static MPAY_Transaction createTransaction(ProcessingContext context, String typeCode,
			BigDecimal externalFees, String description) {
		logger.debug("Inside CreateTransaction for MessageProcessor ...");
		if (context == null)
			throw new NullArgumentException("context");
		if (externalFees == null)
			throw new NullArgumentException("externalFees");
		return TransactionCreatorFactory.create(context.getTransactionNature()).createTransaction(context, typeCode,
				externalFees, description);
	}

	public static JVPostingResult reverseTransaction(ProcessingContext context) {

		logger.debug("Inside ReverseTransaction ...");
		MPAY_Transaction transaction = context.getTransaction();
		return reverseTransaction(transaction, context);
	}

	public static JVPostingResult reverseTransaction(MPAY_Transaction transaction, ProcessingContext context) {
		logger.debug("Inside ReverseTransaction ...");
		if (!isTransactionReversable(transaction))
			return JVPostingResult.success();
		if (transaction.getIsReversed())
			throw new InvalidOperationException("Transaction already reversed");
		if (transaction.getProcessingStatus().getCode().equals(ProcessingStatusCodes.REJECTED)
				|| transaction.getProcessingStatus().getCode().equals(ProcessingStatusCodes.FAILED))
			throw new NotSupportedException("Invalid transation status");
		if (transaction.getRefTrsansactionJvDetails().isEmpty())
			return JVPostingResult.success();

		List<MPAY_JvDetail> postedJVs = new ArrayList<>();
		MPAY_Account reversalCreaditorAccount = getTransactionCreditor(transaction);
		MPAY_JvDetail reversalCreaditorJV = createTransferReversalJV(context.getLookupsLoader(), transaction,
				reversalCreaditorAccount, JournalVoucherFlags.DEBIT);
		if (!postReversalJV(context, reversalCreaditorAccount, reversalCreaditorJV, postedJVs))
			return cancelReversal(context, transaction, postedJVs);

		MPAY_Account reversalDebitorAccount = getTransactionDebitor(transaction);
		MPAY_JvDetail reversalDebitorJV = createTransferReversalJV(context.getLookupsLoader(), transaction,
				reversalDebitorAccount, JournalVoucherFlags.CREDIT);
		if (!postReversalJV(context, reversalDebitorAccount, reversalDebitorJV, postedJVs))
			return cancelReversal(context, transaction, postedJVs);

		if (!reverseTransactionCharges(context, transaction, postedJVs))
			return cancelReversal(context, transaction, postedJVs);

		if (!reverseTransactionTaxes(context, transaction, postedJVs))
			return cancelReversal(context, transaction, postedJVs);

		transaction.setIsReversed(true);
		return JVPostingResult.success();
	}

	private static JVPostingResult cancelReversal(ProcessingContext context, MPAY_Transaction transaction,
			List<MPAY_JvDetail> postedJVs) {
		for (MPAY_JvDetail postedJV : postedJVs) {
			boolean isDebit = postedJV.getDebitCreditFlag().equals(JournalVoucherFlags.CREDIT);
			try {
				context.getDataProvider().callUpdateBalanceSP(postedJV.getRefAccount().getId(), postedJV.getAmount(),
						isDebit);
			} catch (SQLException e) {
				throw new MPayGenericException("Failed to recover reversal JV, no sufficient funds available", e);
			}
		}
		transaction.getRefTrsansactionJvDetails().removeAll(postedJVs);
		return new JVPostingResult(false, ReasonCodes.INSUFFICIENT_FUNDS);
	}

	private static boolean reverseTransactionTaxes(ProcessingContext context, MPAY_Transaction transaction,
			List<MPAY_JvDetail> postedJVs) {
		if (!transaction.getRefOperation().getIsChargeReversalEnabled())
			return true;
		if (!transaction.getRefOperation().getIsTaxReversalEnabled()
				&& transaction.getProcessingStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return true;

		MPAY_Account dibtorAccount = getTransactionDebitor(transaction);
		return reverseTax(dibtorAccount, context, transaction,
				transaction.getSenderTax().add(transaction.getReceiverTax()), postedJVs);
	}

	private static boolean reverseTax(MPAY_Account account, ProcessingContext context, MPAY_Transaction transaction,
			BigDecimal amount, List<MPAY_JvDetail> postedJVs) {
		if (amount.compareTo(BigDecimal.ZERO) == 0)
			return true;
		MPAY_Account taxesAccount = context.getLookupsLoader().getPSP().getPspTaxesAccount();
		MPAY_JvDetail debitReversal = TransactionCreator.createJVDetail(transaction, JournalVoucherFlags.DEBIT,
				taxesAccount, amount, context.getLookupsLoader().getJvTypes(JvDetailsTypes.REVERSAL.toString()),
				REVERSAL_FOR_ACCOUNT + taxesAccount.getAccNumber());
		if (!postReversalJV(context, taxesAccount, debitReversal, postedJVs))
			return false;

		MPAY_JvDetail creditReversal = TransactionCreator.createJVDetail(transaction, JournalVoucherFlags.CREDIT,
				account, amount, context.getLookupsLoader().getJvTypes(JvDetailsTypes.REVERSAL.toString()),
				REVERSAL_FOR_ACCOUNT + account.getAccNumber());
		return postReversalJV(context, account, creditReversal, postedJVs);
	}

	private static boolean isTransactionReversable(MPAY_Transaction transaction) {
		if (transaction.getProcessingStatus().getCode().equals(ProcessingStatusCodes.PENDING)) {
			List<MPAY_JvDetail> postedJVs = transaction.getRefTrsansactionJvDetails().stream()
					.filter(j -> j.getIsPosted()).collect(Collectors.toList());
			if (!postedJVs.isEmpty())
				return true;
			return false;
		}
		return true;
	}

	private static MPAY_JvDetail createTransferReversalJV(ILookupsLoader lookupsLoader, MPAY_Transaction transaction,
			MPAY_Account account, String flag) {
		BigDecimal totalCharges = transaction.getSenderCharge().add(transaction.getReceiverCharge());
		BigDecimal totalTaxes = transaction.getSenderTax().add(transaction.getReceiverTax());
		BigDecimal transferAmount = transaction.getTotalAmount().subtract(totalCharges.add(totalTaxes));
		return TransactionCreator.createJVDetail(transaction, flag, account, transferAmount,
				lookupsLoader.getJvTypes(JvDetailsTypes.REVERSAL.toString()),
				REVERSAL_FOR_ACCOUNT + account.getAccNumber());
	}

	private static boolean reverseTransactionCharges(ProcessingContext context, MPAY_Transaction transaction,
			List<MPAY_JvDetail> postedJVs) {
		if (!transaction.getRefOperation().getIsChargeReversalEnabled()
				&& transaction.getProcessingStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return true;

		MPAY_Account dibtorAccount = getTransactionDebitor(transaction);
		return reverseCharge(dibtorAccount, context, transaction,
				transaction.getSenderCharge().add(transaction.getReceiverCharge()), postedJVs);
	}

	private static boolean reverseCharge(MPAY_Account account, ProcessingContext context, MPAY_Transaction transaction,
			BigDecimal amount, List<MPAY_JvDetail> postedJVs) {
		if (amount.compareTo(BigDecimal.ZERO) == 0)
			return true;
		MPAY_Account chargesAccount = context.getLookupsLoader().getPSP().getPspChargeAccount();
		MPAY_JvDetail debitReversal = TransactionCreator.createJVDetail(transaction, JournalVoucherFlags.DEBIT,
				chargesAccount, amount, context.getLookupsLoader().getJvTypes(JvDetailsTypes.REVERSAL.toString()),
				REVERSAL_FOR_ACCOUNT + chargesAccount.getAccNumber());
		if (!postReversalJV(context, chargesAccount, debitReversal, postedJVs))
			return false;

		MPAY_JvDetail creditReversal = TransactionCreator.createJVDetail(transaction, JournalVoucherFlags.CREDIT,
				account, amount, context.getLookupsLoader().getJvTypes(JvDetailsTypes.REVERSAL.toString()),
				REVERSAL_FOR_ACCOUNT + account.getAccNumber());
		return postReversalJV(context, account, creditReversal, postedJVs);
	}

	private static MPAY_Account getTransactionCreditor(MPAY_Transaction transaction) {
		if (transaction.getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT))
			return transaction.getReceiverMobileAccount() == null
					? transaction.getReceiverServiceAccount().getRefAccount()
					: transaction.getReceiverMobileAccount().getRefAccount();
		else
			return transaction.getSenderMobileAccount() == null ? transaction.getSenderServiceAccount().getRefAccount()
					: transaction.getSenderMobileAccount().getRefAccount();
	}

	private static MPAY_Account getTransactionDebitor(MPAY_Transaction transaction) {
		if (transaction.getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT))
			return transaction.getSenderMobileAccount() == null ? transaction.getSenderServiceAccount().getRefAccount()
					: transaction.getSenderMobileAccount().getRefAccount();
		else
			return transaction.getReceiverMobileAccount() == null
					? transaction.getReceiverServiceAccount().getRefAccount()
					: transaction.getReceiverMobileAccount().getRefAccount();
	}

	private static boolean postReversalJV(ProcessingContext context, MPAY_Account account, MPAY_JvDetail reversalJV,
			List<MPAY_JvDetail> postedJVs) {
		try {
			BalancePostingResult postingResult = context.getDataProvider().callUpdateBalanceSP(
					reversalJV.getRefAccount().getId(), reversalJV.getAmount(),
					reversalJV.getDebitCreditFlag().equals(JournalVoucherFlags.DEBIT));
			if (!postingResult.isSuccess()) {
				reversalJV.setBalanceBefore(account.getBalance());
				reversalJV.setBalanceAfter(account.getBalance());
				return false;
			} else {
				reversalJV.setIsPosted(true);
				reversalJV.setBalanceBefore(postingResult.getBalanceBefore());
				reversalJV.setBalanceAfter(postingResult.getBalanceAfter());
				postedJVs.add(reversalJV);
			}
		} catch (SQLException e) {
			logger.debug("Error while reverseTransaction", e);
			return false;
		}
		return true;
	}

	public static BigDecimal getAccountDeductedChargeValue(MPAY_Transaction transaction, String accountNo) {
		logger.debug("Inside getAccountDeductedChargeValue ...");
		MPAY_JvDetail details = selectFirst(transaction.getRefTrsansactionJvDetails(), having(
				on(MPAY_JvDetail.class).getJvType().getCode(), Matchers.equalTo(JvDetailsTypes.CHARGES.toString())).and(
						having(on(MPAY_JvDetail.class).getRefAccount().getAccNumber(), Matchers.equalTo(accountNo))));
		if (details == null)
			return BigDecimal.ZERO;
		return details.getAmount();
	}

	public static MPAY_JvDetail getAccountDeductedChargeJvDetails(MPAY_Transaction transaction, String accountNo) {
		logger.debug("Inside getAccountDeductedChargeJvDetails ...");
		return selectFirst(transaction.getRefTrsansactionJvDetails(), having(
				on(MPAY_JvDetail.class).getJvType().getCode(), Matchers.equalTo(JvDetailsTypes.CHARGES.toString())).and(
						having(on(MPAY_JvDetail.class).getRefAccount().getAccNumber(), Matchers.equalTo(accountNo))));
	}

	public static List<MPAY_JvDetail> listPostedJVs(MPAY_Transaction transaction) {
		logger.debug("Inside listPostedJVs ...");
		return select(transaction.getRefTrsansactionJvDetails(),
				having(on(MPAY_JvDetail.class).getIsPosted(), Matchers.equalTo(true))
						.and(having(on(MPAY_JvDetail.class).getJvType().getCode(),
								Matchers.not(Matchers.equalTo(JvDetailsTypes.REVERSAL.toString())))));
	}

	public static BigDecimal getTransactionChargeValue(MPAY_Transaction transaction) {
		logger.debug("Inside getTransactionChargeValue ...");
		BigDecimal chargeAmount = BigDecimal.ZERO;
		for (MPAY_JvDetail detail : transaction.getRefTrsansactionJvDetails()) {
			if (detail.getJvType().getCode().equals(JvDetailsTypes.CHARGES.toString())
					&& detail.getDebitCreditFlag().equals(JournalVoucherFlags.DEBIT))
				chargeAmount = chargeAmount.add(detail.getAmount());
		}
		return chargeAmount;
	}

	public static long prepareSenderAccountId(MPAY_Transaction transactionContext) {
		MPAY_MobileAccount senderMobileAccount;
		senderMobileAccount = transactionContext.getSenderMobileAccount();
		if (senderMobileAccount != null)
			return senderMobileAccount.getRefAccount().getId();
		return transactionContext.getSenderServiceAccount().getRefAccount().getId();

	}

	public static long prepareReciverAccountId(MPAY_Transaction transactionContext) {
		MPAY_MobileAccount reciverMobileAccount;
		reciverMobileAccount = transactionContext.getReceiverMobileAccount();
		if (reciverMobileAccount != null)
			return reciverMobileAccount.getRefAccount().getId();
		return transactionContext.getReceiverServiceAccount().getRefAccount().getId();

	}

	public static MPAY_Transaction createRefundTransaction(MessageProcessingContext context, String directCredit,
			BigDecimal zero, MPAY_Transaction originalTransaction) {
		MPAY_Transaction refundTransaction = createTransaction(context, directCredit, zero, "");
		List<MPAY_JvDetail> chargesDetails = originalTransaction.getRefTrsansactionJvDetails().stream()
				.filter(jv -> jv.getJvType().getCode().equals(JvDetailsTypes.CHARGES.toString()))
				.collect(Collectors.toList());
		if (!chargesDetails.isEmpty()) {
			for (MPAY_JvDetail jv : chargesDetails) {
				TransactionCreator.createJVDetail(refundTransaction, oppositeFlag(jv.getDebitCreditFlag()),
						jv.getRefAccount(), jv.getAmount(),
						context.getLookupsLoader().getJvTypes(JvDetailsTypes.REVERSAL.toString()),
						REVERSAL_FOR_ACCOUNT + jv.getRefAccount().getAccNumber());
			}
		}

		reorderingJVsToPreventInsufficiantFundsOnceMerhcantBalanceIsZeroByRefundChargesFirst(refundTransaction);

		return refundTransaction;
	}

	private static void reorderingJVsToPreventInsufficiantFundsOnceMerhcantBalanceIsZeroByRefundChargesFirst(MPAY_Transaction refundTransaction) {
		long size = refundTransaction.getRefTrsansactionJvDetails().size();
		long serial = 1;
		for (Long i = size; i >= 1; i--) {
			refundTransaction.getRefTrsansactionJvDetails().get(i.intValue()-1).setSerial(serial);
			++serial;
		}
	}

	private static String oppositeFlag(String debitCreditFlag) {
		return debitCreditFlag.equals(JournalVoucherFlags.CREDIT.toString()) ? JournalVoucherFlags.DEBIT.toString()
				: JournalVoucherFlags.CREDIT.toString();
	}
}