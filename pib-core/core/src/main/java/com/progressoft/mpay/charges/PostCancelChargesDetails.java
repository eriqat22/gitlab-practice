package com.progressoft.mpay.charges;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;
import com.progressoft.mpay.entities.MPAY_ChargesSlice;
import com.progressoft.mpay.entity.ChargesSlicesStatuses;
import com.progressoft.mpay.entity.MPAYView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

@Component
public class PostCancelChargesDetails implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCancelChargesDetails.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PostApproveChargesDetails ----------------");
        MPAY_ChargesSchemeDetail schemeDetails = (MPAY_ChargesSchemeDetail) arg0.get(WorkflowService.WF_ARG_BEAN);
        if(schemeDetails.getRefChargesDetailsChargesSlices() == null)
        	return;
        for (MPAY_ChargesSlice slice : schemeDetails.getRefChargesDetailsChargesSlices()) {
            if (!ChargesSlicesStatuses.DELETED.toString().equals(slice.getStatusId().getCode())) {
                logger.debug("Approving slice with TX amount" + slice.getTxAmountLimit() + ". . . . .");
                Map<Option, Object> options = new EnumMap<>(Option.class);
                options.put(Option.ACTION_NAME, "SVC_Cancel");
                JfwHelper.executeAction(MPAYView.CHARGES_SLICES, slice, options);
            }
        }
    }

}
