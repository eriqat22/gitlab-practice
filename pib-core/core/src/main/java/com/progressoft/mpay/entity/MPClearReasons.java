package com.progressoft.mpay.entity;

public class MPClearReasons extends ResponseCodesBase {


    public static final String DEBTOR_PARTICIPANT_ACCOUNT_DEBIT_CAB_REACHED = "26";

    public static final String INVALID_TRANSACTION_CONFIGURATION = "37";

    public static final String ALIAS_IS_DEFINED = "63";
    public static final String ALIAS_IS_NOT_DEFINED = "64";
    public static final String OWNER_IS_NOT_DEFINED = "65";
    public static final String ACCOUNT_NOT_DEFINED = "66";
    public static final String PSP_DOES_NOT_OWN_CLIENT = "67";
    public static final String MAX_OWNERS_REACHED = "68";
    public static final String DEBTOR_CLIENT_INVALID_CURRENCY = "69";
    public static final String INVALID_CASH_BALANCE = "70";
    public static final String INVALID_DEALING_BALANCE = "71";
    public static final String CASH_OPERATIONS_INHOUSE_ONLY = "72";
    public static final String CREDITOR_BALANCE_NOT_DEFINED = "73";
    public static final String DEBTOR_BALANCE_NOT_DEFINED = "74";
    public static final String INVALID_PROCESSING_CODE = "75";
    public static final String PROCESSING_CODE_NOT_MATCH = "76";
    public static final String INVALID_PIN_CODE_ENCRYPTION = "77";
    public static final String REJECTED_BY_RECEIVER_PSP = "97";
    public static final String SECURITY_ERROR = "98";
    public static final String RECEIPT_CONFIRMATION = "99";


}
