package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;

public class CancelMPClearRequest implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(CancelMPClearRequest.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside CancelMPClearRequest ==========");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) arg0.get(WorkflowService.WF_ARG_BEAN);
		msg.setIsCanceled(true);
		DataProvider.instance().mergeMPClearMessageLog(msg);

	}

}
