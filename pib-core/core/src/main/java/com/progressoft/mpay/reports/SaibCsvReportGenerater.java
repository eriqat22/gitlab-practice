package com.progressoft.mpay.reports;

import com.progressoft.jfw.model.dao.item.ItemDao;

import com.progressoft.mpay.entities.*;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.List;

public class SaibCsvReportGenerater implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(SaibCsvReportGenerater.class);
    private static final String CSV_SEPARATOR = ",";

    @Autowired
    ItemDao itemDao;
    private String fileLocation;
    private String fileDate =(new Date().toGMTString()).replace(":"," ");


    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("in SaibCsvReportGenerater ............");
        fileLocation = exchange.getProperty("fileLocation").toString();
        createCustomerDetailsCsvReport();
        createCustomerTransactionsCsvReport();
        createMerchantDetailsCsvReport();
        createServiceDetailsCsvReport();
        createServiceTransactioncsvReport();
        createP2PTransactioncsvReport();
    }

    private void createCustomerDetailsCsvReport() throws Exception {
        LOGGER.info("in createCustomerDetailsCsvReport ............");
        List<MPAY_Customer_Trans_Rep> transactions = itemDao.getItems(MPAY_Customer_Trans_Rep.class);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileLocation + "/customerDetails"+fileDate+".csv"), "UTF-8"))) {
            for (MPAY_Customer_Trans_Rep transaction : transactions) {
                StringBuffer oneLine = new StringBuffer();
                oneLine.append(transaction.getExtractStamp())
                        .append(CSV_SEPARATOR).append(transaction.getExtractStamp())
                        .append(CSV_SEPARATOR).append(transaction.getTransactionreference())
                        .append(CSV_SEPARATOR).append(transaction.getTrnDate())
                        .append(CSV_SEPARATOR).append(transaction.getTrnTime())
                        .append(CSV_SEPARATOR).append(transaction.getTrnStatus())
                        .append(CSV_SEPARATOR).append(transaction.getTrnsactionSource())
                        .append(CSV_SEPARATOR).append(transaction.getTrnType())
                        .append(CSV_SEPARATOR).append(transaction.getTrnCCY())
                        .append(CSV_SEPARATOR).append(transaction.getTrnAmount())
                        .append(CSV_SEPARATOR).append(transaction.getSenderIBAN())
                        .append(CSV_SEPARATOR).append(transaction.getSenderNIN())
                        .append(CSV_SEPARATOR).append(transaction.getSenderName())
                        .append(CSV_SEPARATOR).append(transaction.getSenderBankAccountNumber())
                        .append(CSV_SEPARATOR).append(transaction.getSenderAccountNumber())
                        .append(CSV_SEPARATOR).append(transaction.getSenderMobileNumber())
                        .append(CSV_SEPARATOR).append(transaction.getSenderChargesAmount())
                        .append(CSV_SEPARATOR).append(transaction.getSenderBalanceBefore())
                        .append(CSV_SEPARATOR).append(transaction.getSenderBalanceAfter())
                        .append(CSV_SEPARATOR).append(transaction.getSenderBank())
                        .append(CSV_SEPARATOR).append(transaction.getSenderVATAmount())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverIBAN())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverName())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverNIN())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverBankAccountNumber())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverAccountNumber())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverMobileNumber())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverChargesAmount())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverBank())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverVATAmount())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverNetAmount())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverBalanceBefore())
                        .append(CSV_SEPARATOR).append(transaction.getReceiverBalanceAfter())
                        .append(CSV_SEPARATOR).append(transaction.getMerchantname());

                bw.write(oneLine.toString());
                bw.newLine();
            }
        }

    }


    private void createCustomerTransactionsCsvReport() throws Exception {
        LOGGER.info("in createCustomerTransactionsCsvReport ............");
        List<MPAY_Customer_Rep> customersDetails = itemDao.getItems(MPAY_Customer_Rep.class);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileLocation + "/customerTransactions "+fileDate+".csv"), "UTF-8"))) {
            for (MPAY_Customer_Rep customer : customersDetails) {
                StringBuffer oneLine = new StringBuffer();
                oneLine.append(customer.getExtractStamp())
                        .append(CSV_SEPARATOR).append(customer.getCustomerName())
                        .append(CSV_SEPARATOR).append(customer.getCustomerType())
                        .append(CSV_SEPARATOR).append(customer.getStatus())
                        .append(CSV_SEPARATOR).append(customer.getActive())
                        .append(CSV_SEPARATOR).append(customer.getNin())
                        .append(CSV_SEPARATOR).append(customer.getNationality())
                        .append(CSV_SEPARATOR).append(customer.getIdexpirydate())
                        .append(CSV_SEPARATOR).append(customer.getIDType())
                        .append(CSV_SEPARATOR).append(customer.getDob())
                        .append(CSV_SEPARATOR).append(customer.getCustomerMobile())
                        .append(CSV_SEPARATOR).append(customer.getIban())
                        .append(CSV_SEPARATOR).append(customer.getAccountNumber())
                        .append(CSV_SEPARATOR).append(customer.getBankAccNumber())
                        .append(CSV_SEPARATOR).append(customer.getBalances())
                        .append(CSV_SEPARATOR).append(customer.getLanguageindicator())
                        .append(CSV_SEPARATOR).append(customer.getNotificationChannel())
                        .append(CSV_SEPARATOR).append(customer.getDeviceId())
                        .append(CSV_SEPARATOR).append(customer.getDeviceName())
                        .append(CSV_SEPARATOR).append(customer.getDeviceStatus())
                        .append(CSV_SEPARATOR).append(customer.getDeviceActive())
                        .append(CSV_SEPARATOR).append(customer.getDeviceStolen())
                        .append(CSV_SEPARATOR).append(customer.getDeviceRetry())
                        .append(CSV_SEPARATOR).append(customer.getMaximumNumberOfDevices())
                        .append(CSV_SEPARATOR).append(customer.getLastLogonDate())
                        .append(CSV_SEPARATOR).append(customer.getCustomerAlladdressDetails())
                        .append(CSV_SEPARATOR).append(customer.getGender())
                        .append(CSV_SEPARATOR).append(customer.getEmailAddress())
                        .append(CSV_SEPARATOR).append(customer.getDormancyFlag())
                        .append(CSV_SEPARATOR).append(customer.getLastTransactionDate())
                        .append(CSV_SEPARATOR).append(customer.getLastTransactionType())
                        .append(CSV_SEPARATOR).append(customer.getLastTransactionAmount())
                        .append(CSV_SEPARATOR).append(customer.getCreatedDate())
                        .append(CSV_SEPARATOR).append(customer.getCreatedUser())
                        .append(CSV_SEPARATOR).append(customer.getModifiedDate())
                        .append(CSV_SEPARATOR).append(customer.getModifiedUser())
                        .append(CSV_SEPARATOR).append(customer.getDeletedDate())
                        .append(CSV_SEPARATOR).append(customer.getDeletedUser());
                bw.write(oneLine.toString());
                bw.newLine();
            }
        }
    }




    private void createMerchantDetailsCsvReport() throws Exception {
        LOGGER.info("in createMerchantDetailsCsvReport ............");
        List<MPAY_Corporate_Detail_Rep> merchantssDetails = itemDao.getItems(MPAY_Corporate_Detail_Rep.class);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileLocation + "/merchantDetails "+fileDate+".csv"), "UTF-8"))) {
            for (MPAY_Corporate_Detail_Rep merchant : merchantssDetails) {
                StringBuffer oneLine = new StringBuffer();
                oneLine.append(merchant.getExtratStamp())
                        .append(CSV_SEPARATOR).append(merchant.getShortName())
                        .append(CSV_SEPARATOR).append(merchant.getFullName())
                        .append(CSV_SEPARATOR).append(merchant.getRegistrationDate())
                        .append(CSV_SEPARATOR).append(merchant.getIdType())
                        .append(CSV_SEPARATOR).append(merchant.getRegistrationID())
                        .append(CSV_SEPARATOR).append(merchant.getStatus())
                        .append(CSV_SEPARATOR).append(merchant.getRegistered())
                        .append(CSV_SEPARATOR).append(merchant.getActive())
                        .append(CSV_SEPARATOR).append(merchant.getNotificationsLanguage())
                        .append(CSV_SEPARATOR).append(merchant.getPoBox())
                        .append(CSV_SEPARATOR).append(merchant.getZipCode())
                        .append(CSV_SEPARATOR).append(merchant.getStreet())
                        .append(CSV_SEPARATOR).append(merchant.getCity())
                        .append(CSV_SEPARATOR).append(merchant.getCountry())
                        .append(CSV_SEPARATOR).append(merchant.getPhone1())
                        .append(CSV_SEPARATOR).append(merchant.getMobileNo())
                        .append(CSV_SEPARATOR).append(merchant.getBankAccountNumber())
                        .append(CSV_SEPARATOR).append(merchant.getIban())
                        .append(CSV_SEPARATOR).append(merchant.getPaymentType())
                        .append(CSV_SEPARATOR).append(merchant.getNoOfServices())
                        .append(CSV_SEPARATOR).append(merchant.getDeletedDate())
                        .append(CSV_SEPARATOR).append(merchant.getAddedBy())
                        .append(CSV_SEPARATOR).append(merchant.getDeletedBY())
                        .append(CSV_SEPARATOR).append(merchant.getApprovalNote())
                        .append(CSV_SEPARATOR).append(merchant.getRegistrationDate())
                        .append(CSV_SEPARATOR).append(merchant.getCreatedDate())
                        .append(CSV_SEPARATOR).append(merchant.getCreatedUser())
                        .append(CSV_SEPARATOR).append(merchant.getModifiedDate())
                        .append(CSV_SEPARATOR).append(merchant.getModifiedUser())
                        .append(CSV_SEPARATOR).append(merchant.getDeletedDate())
                        .append(CSV_SEPARATOR).append(merchant.getDeletedUser());
                        bw.write(oneLine.toString());
                bw.newLine();
            }
        }
    }

    private void createServiceDetailsCsvReport() throws Exception {
        LOGGER.info("in createServiceDetailsCsvReport ............");
        List<MPAY_Service_Detail_Rep> services = itemDao.getItems(MPAY_Service_Detail_Rep.class);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileLocation + "/serviceDetails "+fileDate+".csv"), "UTF-8"))) {
            for (MPAY_Service_Detail_Rep service : services) {
                StringBuffer oneLine = new StringBuffer();
                oneLine.append(service.getExtractStamp())
                        .append(CSV_SEPARATOR).append(service.getCorporateName())
                        .append(CSV_SEPARATOR).append(service.getServiceName())
                        .append(CSV_SEPARATOR).append(service.getStatus())
                        .append(CSV_SEPARATOR).append(service.getServiceDescription())
                        .append(CSV_SEPARATOR).append(service.getServiceAlias())
                        .append(CSV_SEPARATOR).append(service.getServiceType())
                        .append(CSV_SEPARATOR).append(service.getBlocked())
                        .append(CSV_SEPARATOR).append(service.getServiceBankName())
                        .append(CSV_SEPARATOR).append(service.getServierBankNumber())
                        .append(CSV_SEPARATOR).append(service.getServiceIBAN())
                        .append(CSV_SEPARATOR).append(service.getServiceMPAYAccount())
                        .append(CSV_SEPARATOR).append(service.getServiceBalance())
                        .append(CSV_SEPARATOR).append(service.getPaymentType())
                        .append(CSV_SEPARATOR).append(service.getRegistered())
                        .append(CSV_SEPARATOR).append(service.getActive())
                        .append(CSV_SEPARATOR).append(service.getNotificationChannel())
                        .append(CSV_SEPARATOR).append(service.getNotificationReceiver())
                        .append(CSV_SEPARATOR).append(service.getProfile())
                        .append(CSV_SEPARATOR).append(service.getServiceCategory())
                        .append(CSV_SEPARATOR).append(service.getServiceType())
                        .append(CSV_SEPARATOR).append(service.getServiceType())
                        .append(CSV_SEPARATOR).append(service.getPinLastChanges())
                        .append(CSV_SEPARATOR).append(service.getRetryCount())
                        .append(CSV_SEPARATOR).append(service.getLastTransactiondate())
                        .append(CSV_SEPARATOR).append(service.getLastTransactionType())
                        .append(CSV_SEPARATOR).append(service.getLastTransactionAmount())
                        .append(CSV_SEPARATOR).append(service.getCreatedDate())
                        .append(CSV_SEPARATOR).append(service.getCreatedUser())
                        .append(CSV_SEPARATOR).append(service.getModifiedDate())
                        .append(CSV_SEPARATOR).append(service.getModifiedUser())
                        .append(CSV_SEPARATOR).append(service.getDeletedDate())
                        .append(CSV_SEPARATOR).append(service.getDeletedUser());
                bw.write(oneLine.toString());
                bw.newLine();
            }
        }
    }

    private void createServiceTransactioncsvReport() throws Exception {
        LOGGER.info("in createServiceTransactioncsvReport ............");
        List<MPAY_Service_Trans_Rep> transactions = itemDao.getItems(MPAY_Service_Trans_Rep.class);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileLocation + "/serviceTransactions "+fileDate+".csv"), "UTF-8"))) {
            for (MPAY_Service_Trans_Rep transaction : transactions) {
                StringBuffer oneLine = new StringBuffer();
                oneLine.append(transaction.getExtractStamp())
                        .append(CSV_SEPARATOR).append(transaction.getCorporateName())
                        .append(CSV_SEPARATOR).append(transaction.getServiceName())
                        .append(CSV_SEPARATOR).append(transaction.getServiceStatus())
                        .append(CSV_SEPARATOR).append(transaction.getServiceBankName())
                        .append(CSV_SEPARATOR).append(transaction.getServiceBankNumber())
                        .append(CSV_SEPARATOR).append(transaction.getServiceIBAN())
                        .append(CSV_SEPARATOR).append(transaction.getServiceMPAYAccount())
                        .append(CSV_SEPARATOR).append(transaction.getTrnDate())
                        .append(CSV_SEPARATOR).append(transaction.getTrnTime())
                        .append(CSV_SEPARATOR).append(transaction.getTrnType())
                        .append(CSV_SEPARATOR).append(transaction.getTrnSide())
                        .append(CSV_SEPARATOR).append(transaction.getTrnAmount())
                        .append(CSV_SEPARATOR).append(transaction.getTrnCharges())
                        .append(CSV_SEPARATOR).append(transaction.getTrnVAT())
                        .append(CSV_SEPARATOR).append(transaction.getTrnNetAmount())
                        .append(CSV_SEPARATOR).append(transaction.getTrnRefernceNo())
                        .append(CSV_SEPARATOR).append(transaction.getTrnDescription())
                        .append(CSV_SEPARATOR).append(transaction.getServiceBalanceBeforeTrn())
                        .append(CSV_SEPARATOR).append(transaction.getServiceBalanceAfterTrn())
                        .append(CSV_SEPARATOR).append(transaction.getOtherPartyMobileNo())
                        .append(CSV_SEPARATOR).append(transaction.getOtherPartyAmount());

                bw.write(oneLine.toString());
                bw.newLine();
            }
        }
    }

    private void createP2PTransactioncsvReport() throws Exception {
        LOGGER.info("in createP2PTransactioncsvReport ............");
        List<MPAY_P2p_Trans_Rep> p2ps = itemDao.getItems(MPAY_P2p_Trans_Rep.class);
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileLocation + "/p2pTransactions "+fileDate+".csv"), "UTF-8"))) {
            for (MPAY_P2p_Trans_Rep p2p : p2ps) {
                StringBuffer oneLine = new StringBuffer();
                oneLine.append(p2p.getExtractDate())
                        .append(CSV_SEPARATOR).append(p2p.getTrnDate())
                        .append(CSV_SEPARATOR).append(p2p.getTrnTime())
                        .append(CSV_SEPARATOR).append(p2p.getTrnStatus())
                        .append(CSV_SEPARATOR).append(p2p.getTrnRefernceNo())
                        .append(CSV_SEPARATOR).append(p2p.getTrnAmount())
                        .append(CSV_SEPARATOR).append(p2p.getTrnDescription())
                        .append(CSV_SEPARATOR).append(p2p.getSenderIBAN())
                        .append(CSV_SEPARATOR).append(p2p.getSenderNIN())
                        .append(CSV_SEPARATOR).append(p2p.getSenderName())
                        .append(CSV_SEPARATOR).append(p2p.getSenderBankAccountNumber())
                        .append(CSV_SEPARATOR).append(p2p.getSenderAccountNumber())
                        .append(CSV_SEPARATOR).append(p2p.getSenderMobileNumber())
                        .append(CSV_SEPARATOR).append(p2p.getSenderChargesAmount())
                        .append(CSV_SEPARATOR).append(p2p.getSenderAmount())
                        .append(CSV_SEPARATOR).append(p2p.getSenderBalanceBefore())
                        .append(CSV_SEPARATOR).append(p2p.getSenderBalanceAfter())
                        .append(CSV_SEPARATOR).append(p2p.getSenderBank())
                        .append(CSV_SEPARATOR).append(p2p.getSenderVATAmount())
                        .append(CSV_SEPARATOR).append(p2p.getReceiverIBAN())
                        .append(CSV_SEPARATOR).append(p2p.getReceiverName())
                        .append(CSV_SEPARATOR).append(p2p.getReceiverNIN())
                        .append(CSV_SEPARATOR).append(p2p.getReceiverBankAccountNumber())
                        .append(CSV_SEPARATOR).append(p2p.getReceiverAccountNumber())
                        .append(CSV_SEPARATOR).append(p2p.getReceiverMobileNumber())
                        .append(CSV_SEPARATOR).append(p2p.getReceiverChargesAmount());
                bw.write(oneLine.toString());
                bw.newLine();
            }
        }
    }
}
