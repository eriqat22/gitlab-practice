package com.progressoft.mpay.email;

import com.progressoft.mpay.MPayCryptographer.DefaultCryptographer;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.sms.NotificationRequest;
import com.progressoft.mpay.sms.NotificationResponse;
import org.apache.camel.Body;
import org.apache.camel.Exchange;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;

public class EmailSender {
    private boolean isAuth;

    public void sendEmailMessage(@Body String body, Exchange exchange) {
        NotificationRequest notificationRequest = getNotificationRequestFromBody(body);
        send(notificationRequest);
        NotificationResponse notificationResponse = new NotificationResponse();
        notificationResponse.setStatus("success");
        exchange.getIn().setBody(createXML(notificationResponse));
    }

    public void send(NotificationRequest notificationRequest) {
        try {
            Properties properties;
            if (getEmailAuthenticatorUsernameFromConfiguration().equalsIgnoreCase("--")
                    && getEmailAuthenticatorPasswordFromConfiguration().equalsIgnoreCase("--")) {
                properties = prepareEmailProperties("false");
            } else {

                isAuth = true;
                properties = prepareEmailProperties("true");
            }
            Session session = prepareMailSession(properties);
            Message message = prepareMailMessage(notificationRequest, session);
            Transport.send(message);
        } catch (MessagingException e) {
            throw new MPayGenericException("Error while send", e);
        }
    }

    private Message prepareMailMessage(NotificationRequest notificationRequest, Session session)
            throws MessagingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(getFromEmailFromConfiguration()));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(notificationRequest.getReciever()));
        message.setSubject(getEmailSubjectFromConfiguration());
        message.setText(getNotificationText(notificationRequest.getContent()));
        return message;
    }

    private String getNotificationText(String content) {
        if (content.indexOf("\"") < 0)
            return content;
        String encryptedOtp = content.substring(content.indexOf("\"") + 1,
                content.lastIndexOf("\""));

        String decreyptedOtp = DefaultCryptographer.INSTANCE.get().decrypt(encryptedOtp);

        return content.replace("\"" + encryptedOtp + "\"", decreyptedOtp);
    }

    private Session prepareMailSession(Properties properties) {
        if (isAuth)
            return Session.getDefaultInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(getEmailAuthenticatorUsernameFromConfiguration(),
                            getEmailAuthenticatorPasswordFromConfiguration());
                }
            });
        else
            return Session.getDefaultInstance(properties);
    }

    private Properties prepareEmailProperties(String isEnableAuth) {
        Properties properties = new Properties();
        properties.put("mail.smtp.starttls.enable", "false");
        properties.put("mail.smtp.port", getSMTPPortFromConfiguration());
        properties.put("mail.smtp.host", getSMTPHostFromConfiguration());
        properties.put("mail.smtp.auth", isEnableAuth);
        return properties;
    }

    private NotificationRequest getNotificationRequestFromBody(String body) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(new Class[]{NotificationRequest.class});
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (NotificationRequest) unmarshaller.unmarshal(new StringReader(body));
        } catch (JAXBException e) {
            throw new MPayGenericException("Error while getNotificationRequestFromBody", e);
        }
    }

    private String getSMTPHostFromConfiguration() {
        return SystemParameters.getInstance().getSMTPHost();
    }

    private int getSMTPPortFromConfiguration() {
        return SystemParameters.getInstance().getSMTPPort();
    }

    private String getFromEmailFromConfiguration() {
        return SystemParameters.getInstance().getEmailFrom();
    }

    private String getEmailSubjectFromConfiguration() {
        return SystemParameters.getInstance().getEmailSubject();
    }

    private String getEmailAuthenticatorUsernameFromConfiguration() {
        return SystemParameters.getInstance().getEmailUsername();
    }

    private String getEmailAuthenticatorPasswordFromConfiguration() {
        return SystemParameters.getInstance().getEmailPassword();
    }

    public String createXML(Object object) {
        try {
            StringWriter writer = new StringWriter();
            JAXBContext JAXB_CONTEXT = JAXBContext.newInstance(NotificationResponse.class);
            JAXB_CONTEXT.createMarshaller().marshal(object, writer);
            return writer.toString();
        } catch (Exception e) {
            return null;
        }
    }
}