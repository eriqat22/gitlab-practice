package com.progressoft.mpay.registration;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CustIntegMessage;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Post1700ProcessMpClearInegMsgLog implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(Post1700ProcessMpClearInegMsgLog.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside Post1700ProcessMpClearInegMsgLog----------------*---------");
		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		MPAY_CustIntegMessage integrationMessage = new MPAY_CustIntegMessage();
		integrationMessage.setRefMsgLog(msg);
		MPAY_Customer customer;
		MPAY_CustomerMobile mobile;
		String[] tokens = msg.getHint().split(";");
		if (msg.getHint().endsWith("MM")) {
			customer = DataProvider.instance().getCustomer(Long.valueOf(tokens[0]));
			mobile = customer.getRefCustomerCustomerMobiles().get(0);
		} else {
			mobile = DataProvider.instance().getCustomerMobile(Long.valueOf(tokens[0]));
			customer = mobile.getRefCustomer();
		}

		integrationMessage.setRefMobile(mobile);
		integrationMessage.setRefCustomer(customer);
		integrationMessage.setIntegMsgType(LookupsLoader.getInstance().getMpClearIntegrationMessageTypes(IntegMessageTypes.TYPE_1700));
		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());
		integrationMessage.setRefMessage(isomsg.getField(115).toString());
		integrationMessage.setActionType(LookupsLoader.getInstance().getCustomerIntegrationActionTypes(isomsg.getField(48).toString()));
		customer.setRefLastMsgLog(msg);
		try {
			DataProvider.instance().presistCustomerIntegMessage(integrationMessage);
			DataProvider.instance().updateCustomer(customer);
			JfwHelper.sendToActiveMQ(integrationMessage, integrationMessage.getTenantId(), MPAYView.CUST_1700_REG_MSGS.viewName, "mpay.mpclear.register");
		} catch (Exception e) {
			throw new WorkflowException(e);
		}
	}
}
