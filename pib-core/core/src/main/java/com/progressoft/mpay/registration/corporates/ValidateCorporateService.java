package com.progressoft.mpay.registration.corporates;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.progressoft.mpay.DataProvider;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.MPayErrorMessages;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;

public class ValidateCorporateService implements Validator {

    @Autowired
    ItemDao itemDao;

    private static final Logger logger = LoggerFactory.getLogger(ValidateCorporateService.class);
    private static final String ADMIN_CODE = "ADMIN";

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside ValidateCreateCorporateService-----");
        MPAY_CorpoarteService service = (MPAY_CorpoarteService) arg0.get(WorkflowService.WF_ARG_BEAN);

        if (service.getName() != null)
            service.setName(service.getName().toUpperCase());
        if (service.getAlias() != null)
            service.setAlias(service.getAlias().toUpperCase());
        if (service.getMpclearAlias() != null)
            service.setMpclearAlias(service.getMpclearAlias().toUpperCase());

        validateMaxNumberOfServices(service);
        validateServiceName(service);
        validateServiceAlias(service);
        if (service.getPaymentType().getCode().equals(ADMIN_CODE))
            validateAdminService(service.getRefCorporate().getId());
    }

    private void validateAdminService(Long refCorporateId) throws InvalidInputException {
        List<MPAY_CorpoarteService> allServices = DataProvider.instance().listCorporateServices(refCorporateId);

        if (allServices.stream().filter(s -> s.getPaymentType().getCode().equals(ADMIN_CODE)).count() > 1) {
            InvalidInputException iie = new InvalidInputException();
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, "An admin service is already defined for this corporate");
            throw iie;
        }
    }

    private void validateMaxNumberOfServices(MPAY_CorpoarteService service) throws InvalidInputException {
        List<MPAY_CorpoarteService> undeletedMobiles = select(
                service.getRefCorporate().getRefCorporateCorpoarteServices(),
                (having(on(MPAY_CorpoarteService.class).getDeletedFlag(), Matchers.nullValue()).or(having(on(MPAY_CorpoarteService.class).getDeletedFlag(), Matchers.equalTo(false)))).and(having(
                        on(MPAY_CorpoarteService.class).getName(), Matchers.not(service.getName()))));
        MPAY_ClientType type = service.getRefCorporate().getClientType();
        if (type.getMaxNumberOfOwners() == 0)
            return;
        if (undeletedMobiles.size() >= type.getMaxNumberOfOwners()) {
            InvalidInputException iie = new InvalidInputException();
            iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg(MPayErrorMessages.SERVICE_MAX_REACHED));
            throw iie;
        }
    }

    private void validateServiceName(MPAY_CorpoarteService service) throws InvalidInputException {
        Map<String, Object> nameProperties = new HashMap<>();
        nameProperties.put("name", service.getName());
        InvalidInputException cause = Unique.validate(itemDao, service, nameProperties, MPayErrorMessages.SERVICE_INVALID_NAME_IS_USED);
        if (cause.getErrors().size() > 0)
            throw cause;
    }

    private void validateServiceAlias(MPAY_CorpoarteService service) throws InvalidInputException {
        Map<String, Object> aliasProperties = new HashMap<>();
        aliasProperties.put("alias", service.getAlias());
        aliasProperties.put("deletedFlag", false);
        InvalidInputException cause = Unique.validate(itemDao, service, aliasProperties, MPayErrorMessages.SERVICE_INVALID_ALIAS_IS_USED);
        if (cause.getErrors().size() > 0)
            throw cause;
    }
}
