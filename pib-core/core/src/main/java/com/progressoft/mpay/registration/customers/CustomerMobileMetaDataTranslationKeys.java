package com.progressoft.mpay.registration.customers;

class CustomerMobileMetaDataTranslationKeys extends CommonMetaDataTranslationKeys {
    static final String MOBILE_NUMBER = "MobileNumber";
    static final String OPERATOR = "MobileOperator";
    static final String NFC_SERIAL = "MobileNFCSerial";
    static final String NOTIFICATION_SHOW_TYPE = "MobileNotificationShowType";
    static final String ALIAS = "MobileAlias";
}
