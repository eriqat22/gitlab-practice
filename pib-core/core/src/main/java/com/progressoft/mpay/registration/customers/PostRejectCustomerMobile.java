package com.progressoft.mpay.registration.customers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;

public class PostRejectCustomerMobile implements FunctionProvider {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(PostRejectCustomerMobile.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
		logger.debug("Inside execute =============");
		MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) arg0.get(WorkflowService.WF_ARG_BEAN);
		mobile.setIsActive(false);
		rejectAccounts(mobile);
		handleRequestMessageIfExist(mobile);
	}

	private void rejectAccounts(MPAY_CustomerMobile mobile) throws WorkflowException {
		logger.debug("Inside RejectAccounts =============");
		List<MPAY_MobileAccount> accounts = DataProvider.instance().listMobileAccounts(mobile.getId());
		if (accounts == null || accounts.isEmpty())
			return;
		Iterator<MPAY_MobileAccount> accountsIterator = accounts.listIterator();
		do {
			MPAY_MobileAccount account = accountsIterator.next();
			if (account.getStatusId() != null && account.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.APPROVAL.toString())
					|| account.getStatusId().getCode().equals(MobileAccountWorkflowStatuses.INTEGRATION.toString()))
				JfwHelper.executeAction(MPAYView.MOBILE_ACCOUNTS_VIEW, account, MobileAccountWorkflowServiceActions.REJECT, new WorkflowException());
		} while (accountsIterator.hasNext());
	}
	private void handleRequestMessageIfExist(MPAY_CustomerMobile mobile) {
		if (mobile.getIsRegistered())
			return;
		if (mobile.getHint() != null && mobile.getHint().trim().length() > 0) {
			MPAY_MPayMessage registrationMessage = DataProvider.instance().getMPayMessage(Long.parseLong(mobile.getHint()));
			if (registrationMessage != null) {
				if (!registrationMessage.getProcessingStatus().getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED))
					return;
				registrationMessage.setReason(LookupsLoader.getInstance().getReason(ReasonCodes.REJECTED_BY_NATIONAL_SWITCH));
				registrationMessage.setReasonDesc(mobile.getRejectionNote());
				registrationMessage.setProcessingStatus(LookupsLoader.getInstance().getProcessingStatus(ProcessingStatusCodes.REJECTED));
				DataProvider.instance().mergeMPayMessage(registrationMessage);
			}
		}
	}
}
