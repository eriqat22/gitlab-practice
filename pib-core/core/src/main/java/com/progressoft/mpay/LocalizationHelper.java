package com.progressoft.mpay;

import com.progressoft.jfw.model.service.utils.AppContext;

public class LocalizationHelper {
	public String localize(String key) {
		return AppContext.getMsg(key);
	}
}
