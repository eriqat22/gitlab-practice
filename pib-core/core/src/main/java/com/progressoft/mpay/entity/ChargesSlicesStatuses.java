package com.progressoft.mpay.entity;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;

public enum ChargesSlicesStatuses {

	DELETED("102003");

	private String value;

	ChargesSlicesStatuses(String value) {
		this.value = value;
	}

	public boolean compare(WorkflowStatus status) {
		return status != null && status.getCode().equals(this.value);
	}

	@Override
	public String toString() {
		return value;
	}
}
