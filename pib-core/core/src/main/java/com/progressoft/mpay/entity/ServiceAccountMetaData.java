package com.progressoft.mpay.entity;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_ServiceAccount;

import flexjson.JSONDeserializer;

public class ServiceAccountMetaData extends AccountMetaData {

    public ServiceAccountMetaData() {
    	//Default
    }

    public ServiceAccountMetaData(MPAY_ServiceAccount account) {
        if (account == null)
            throw new NullArgumentException("account");

        this.externalAcc = account.getExternalAcc();
        this.profileId = account.getRefProfile().getId();
    }

    public static ServiceAccountMetaData fromJson(String data) {
        JSONDeserializer<ServiceAccountMetaData> deserializer = new JSONDeserializer<>();
		return deserializer.deserialize(data, ServiceAccountMetaData.class);
	}
}
