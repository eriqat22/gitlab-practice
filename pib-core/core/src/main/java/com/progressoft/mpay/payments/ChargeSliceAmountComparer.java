package com.progressoft.mpay.payments;

import java.util.Comparator;

import com.progressoft.mpay.entities.MPAY_ChargesSlice;

public class ChargeSliceAmountComparer implements Comparator<MPAY_ChargesSlice> {

	@Override
	public int compare(MPAY_ChargesSlice o1, MPAY_ChargesSlice o2) {
		if (o1 == null) {
			throw new IllegalArgumentException("o1 is null");
		}
		if (o2 == null) {
			throw new IllegalArgumentException("o2 is null");
		}

		return o1.getTxAmountLimit().compareTo(o2.getTxAmountLimit());
	}
}
