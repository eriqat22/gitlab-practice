package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.mpclear.MPClearClientHelper;

public class PostSetServiceAccountSwitchDefaultApproval implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostSetServiceAccountSwitchDefaultApproval.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        logger.debug("inside execute--------------------------");
        MPAY_ServiceAccount account = (MPAY_ServiceAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
        MPAY_CorpoarteService service = account.getService();
        MPAY_Corporate corporate = service.getRefCorporate();
        MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).setDefaultServiceAccount(new CoreComponents(DataProvider.instance(), SystemParameters.getInstance(), LookupsLoader.getInstance()), corporate, service, account);
    }
}
