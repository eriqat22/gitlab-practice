package com.progressoft.mpay.psp;

import com.opensymphony.workflow.InvalidInputException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.ClientTypes;

public class PSP {
	private PSP() {

	}

	public static MPAY_Corporate get() throws InvalidInputException {
		ItemDao itemDao = (ItemDao) AppContext.getApplicationContext().getBean("itemDao");
		MPAY_Corporate psp = itemDao
				.getItem(MPAY_Corporate.class, "clientType.code", ClientTypes.SERVICE_PROVIDER_CODE);
		if (psp == null)
			throw new InvalidInputException("PSP is not defined");
		return psp;
	}
}