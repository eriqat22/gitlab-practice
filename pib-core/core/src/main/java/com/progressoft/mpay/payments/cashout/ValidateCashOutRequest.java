package com.progressoft.mpay.payments.cashout;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CashOut;

public class ValidateCashOutRequest implements Validator {

	@Autowired
	ItemDao itemDao;

	private static final Logger logger = LoggerFactory.getLogger(ValidateCashOutRequest.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void validate(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {

		logger.debug("\n\r===inside ValidateCashOutRequest....");

		InvalidInputException iie = new InvalidInputException();

		MPAY_CashOut cashout = (MPAY_CashOut) arg0.get(WorkflowService.WF_ARG_BEAN);

		if (!cashout.getRefCustMob().getRefCustomer().getIsActive()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("Customer.isnotActive"));
			throw iie;
		}

		if (!cashout.getRefCustMob().getIsActive()) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("Customer.mobile.isnotActive"));
			throw iie;
		}

		if (cashout.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("Amount.shouldBeGreaterThanZero"));
			throw iie;
		}

		BigDecimal balance = SystemHelper.getDefaultWalletAccount(cashout.getRefCustMob()).getMinBalance();
		BigDecimal amount = cashout.getAmount();
		BigDecimal result = balance.subtract(amount);

		if (cashout.getAmount().compareTo(result) < 1) {
			iie.addError(WorkflowService.WF_ARG_ERROR_MSGS, AppContext.getMsg("Amount.shouldBeGreaterThanMinBalance"));
			throw iie;
		}
	}
}