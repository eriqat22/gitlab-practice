package com.progressoft.mpay.charges;

import java.util.EnumMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.entities.MPAY_ChargesSchemeDetail;
import com.progressoft.mpay.entities.MPAY_ChargesSlice;
import com.progressoft.mpay.entity.MPAYView;

public class PreDeleteChargesDetails implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PreDeleteChargesDetails.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PreDeleteChargesDetails ----------------");
        MPAY_ChargesSchemeDetail schemeDetails = (MPAY_ChargesSchemeDetail) arg0.get(WorkflowService.WF_ARG_BEAN);
        schemeDetails.setIsActive(Boolean.FALSE);
        for (MPAY_ChargesSlice slice : schemeDetails.getRefChargesDetailsChargesSlices()) {
            logger.debug("DELETING slice with TX amount" + slice.getTxAmountLimit() + ". . . . .");
            Map<Option, Object> options = new EnumMap<>(Option.class);
            options.put(Option.ACTION_NAME, "Delete");
            JfwHelper.executeAction(MPAYView.CHARGES_SLICES, slice, options);
        }
    }

}
