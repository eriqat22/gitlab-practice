package com.progressoft.mpay.plugins;

public class NotificationContext {
	private String sender;
	private String operation;
	private String reference;
	private String reasonCode;
	private String reasonDescription;
	private String notes;
	private boolean hasNotes;
	private String extraData1;
	private String extraData2;
	private String extraData3;

	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getReasonDescription() {
		return reasonDescription;
	}
	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public boolean isHasNotes() {
		return this.hasNotes;
	}
	public void setHasNotes(boolean hasNotes) {
		this.hasNotes = hasNotes;
	}
	public String getExtraData1() {
		return extraData1;
	}
	public void setExtraData1(String extraData1) {
		this.extraData1 = extraData1;
	}
	public String getExtraData2() {
		return extraData2;
	}
	public void setExtraData2(String extraData2) {
		this.extraData2 = extraData2;
	}
	public String getExtraData3() {
		return extraData3;
	}
	public void setExtraData3(String extraData3) {
		this.extraData3 = extraData3;
	}
}
