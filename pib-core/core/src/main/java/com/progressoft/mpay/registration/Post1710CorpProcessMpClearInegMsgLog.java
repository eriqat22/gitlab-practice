package com.progressoft.mpay.registration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.openmbean.InvalidOpenTypeException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.Result;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CorpIntegMessage;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entity.IntegMessageTypes;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.solab.iso8583.IsoMessage;

public class Post1710CorpProcessMpClearInegMsgLog implements FunctionProvider {

	@Autowired
	private ItemDao itemDao;
	private static final Logger logger = LoggerFactory.getLogger(Post1710CorpProcessMpClearInegMsgLog.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("Inside Post1710CorpProcessMpClearInegMsgLog-------------------------");

		MPAY_MpClearIntegMsgLog msg = (MPAY_MpClearIntegMsgLog) transientVars.get(WorkflowService.WF_ARG_BEAN);
		MPAY_CorpIntegMessage integrationMessage = new MPAY_CorpIntegMessage();

		IsoMessage isomsg = ISO8583Parser.getInstance(SystemParameters.getInstance().getISO8583ConfigFilePath()).parseMessage(msg.getContent());

		integrationMessage.setRefMsgLog(msg);
		MPAY_CorpIntegMessage orgMessage = getOrigionalMessageByMessageID(isomsg.getField(116).toString());
		if (orgMessage == null)
			throw new InvalidOpenTypeException("original message not found with id = " + isomsg.getField(116).toString());
		MPClearClientHelper.getClient(SystemParameters.getInstance().getMPClearProcessor()).updateResponseReceived(isomsg.getField(116).toString());
		integrationMessage.setRefCorporate(orgMessage.getRefCorporate());
		integrationMessage.setIntegMsgType(LookupsLoader.getInstance().getMpClearIntegrationMessageTypes(IntegMessageTypes.TYPE_1710));
		integrationMessage.setRefService(orgMessage.getRefService());
		integrationMessage.setRefOriginalMsg(orgMessage);
		integrationMessage.setRefMessage(isomsg.getField(115).toString());
		orgMessage.setRefMessage(isomsg.getField(116).toString());
		integrationMessage.setResponse(LookupsLoader.getInstance().getMpClearIntegrationResponseCodes(isomsg.getField(44).toString()));
		integrationMessage.setActionType(orgMessage.getActionType());
		if (isomsg.getField(47) != null) {
			integrationMessage.setNarration(isomsg.getField(47).toString());
		}
		try {
			itemDao.persist(integrationMessage);
			Map<String, Object> v = new HashMap<>();
			v.put(WorkflowService.WF_ARG_BEAN, integrationMessage);
			PreValidateCorp1710MpClearIntegMsg oPreValidateCorp1710MpClearIntegMsg = AppContext.getApplicationContext().getBean("PreValidateCorp1710MpClearIntegMsg", PreValidateCorp1710MpClearIntegMsg.class);
			oPreValidateCorp1710MpClearIntegMsg.execute(v, null, null);
			if (v.get(Result.VALIDATION_RESULT).equals(Result.SUCCESSFUL)) {

				PostProcessCorp1710MpClearIntegMsg oPostProcessCorp1710MpClearIntegMsg = AppContext.getApplicationContext().getBean("PostProcessCorp1710MpClearIntegMsg", PostProcessCorp1710MpClearIntegMsg.class);
				oPostProcessCorp1710MpClearIntegMsg.execute(v, null, null);
			}

		} catch (Exception e) {

			throw new WorkflowException(e);
		}

	}

	private MPAY_CorpIntegMessage getOrigionalMessageByMessageID(String messageId) {
		MPAY_CorpIntegMessage value = null;

		List<MPAY_CorpIntegMessage> items = itemDao.getItems(MPAY_CorpIntegMessage.class, null, null, "refMessage='" + messageId + "'", null);
		if (items != null && !items.isEmpty())
			value = items.get(0);

		return value;

	}

}
