package com.progressoft.mpay.valueprovider;


import com.progressoft.mpay.entities.MPAY_Customer;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CustomersValueProvider {

    @PersistenceContext
    private EntityManager em;


    public Map<String, String> getCustomersReferralKeys() {
        Map<String, String> customers = new HashMap<>();
        Query query = em.createQuery("FROM MPAY_Customer");

        List<MPAY_Customer> dbCustomers = query.getResultList();
        for (MPAY_Customer customer : dbCustomers) {
                customers.put(String.valueOf(customer.getId()), customer.getReferralkey() + " - " +customer.getFullName());

        }
        return customers;
    }

}
