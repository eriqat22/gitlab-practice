package com.progressoft.mpay.banks;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.HashingAlgorithm;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_BanksUser;

public class PostCreateBankUser implements FunctionProvider {

    private static final Logger logger = LoggerFactory.getLogger(PostCreateBankUser.class);

    @SuppressWarnings("rawtypes")
    @Override
    public void execute(Map arg0, Map arg1, PropertySet arg2) throws WorkflowException {
        logger.debug("inside PostCreateBankUser ----------------");
        MPAY_BanksUser bankUser = (MPAY_BanksUser) arg0.get(WorkflowService.WF_ARG_BEAN);
        bankUser.setUserName(bankUser.getUserName().toUpperCase());
        bankUser.setPassword(HashingAlgorithm.hash(MPayContext.getInstance().getSystemParameters(), bankUser.getPassword()));
        MPayContext.getInstance().getDataProvider().updateBankUser(bankUser);
    }
}
