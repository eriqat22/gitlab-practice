package com.progressoft.mpay.registration.corporates;

public class CorporateDeviceWorkflowServiceActions {
	public static final String DELETE = "SVC_Delete";
	public static final String REACTIVATE = "SVC_Reactivate";

	private CorporateDeviceWorkflowServiceActions() {

	}
}
