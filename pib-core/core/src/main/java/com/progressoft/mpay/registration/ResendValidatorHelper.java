package com.progressoft.mpay.registration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.MPAYView;

public class ResendValidatorHelper {
	private static final Logger logger = LoggerFactory.getLogger(ResendValidatorHelper.class);

	private ResendValidatorHelper() {

	}

	public static MPAY_MpClearIntegMsgLog tryGetCustomer(String viewName, Object bean) {
		logger.debug("Inside tryGetCustomer ======================");
		MPAY_MpClearIntegMsgLog msg = null;
		if (viewName.equals(MPAYView.CUSTOMER.viewName)) {
			msg = ((MPAY_Customer) bean).getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.CUSTOMER_MOBILES.viewName) || viewName.equals(MPAYView.CUSTOMER_MOBILE_VIEWS.viewName)) {
			msg = ((MPAY_CustomerMobile) bean).getRefCustomer().getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.MOBILE_ACCOUNTS.viewName) || viewName.equals(MPAYView.MOBILE_ACCOUNTS_VIEW.viewName)) {
			msg = ((MPAY_MobileAccount) bean).getMobile().getRefCustomer().getRefLastMsgLog();
		}
		return msg;
	}

	public static MPAY_MpClearIntegMsgLog tryGetCorporate(String viewName, Object bean) {
		logger.debug("Inside tryGetCorporate ======================");
		MPAY_MpClearIntegMsgLog msg = null;
		if (viewName.equals(MPAYView.CORPORATE.viewName)) {
			msg = ((MPAY_Corporate) bean).getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.CORPORATE_SERVICES.viewName) || viewName.equals(MPAYView.CORPORATE_SERVICE_VIEWS.viewName)) {
			msg = ((MPAY_CorpoarteService) bean).getRefCorporate().getRefLastMsgLog();
		} else if (viewName.equals(MPAYView.SERVICE_ACCOUNTS.viewName) || viewName.equals(MPAYView.SERVICE_ACCOUNTS_VIEW.viewName)) {
			msg = ((MPAY_ServiceAccount) bean).getService().getRefCorporate().getRefLastMsgLog();
		}
		return msg;
	}
}
