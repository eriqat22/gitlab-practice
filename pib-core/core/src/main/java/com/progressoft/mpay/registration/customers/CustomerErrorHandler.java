package com.progressoft.mpay.registration.customers;

import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.dao.item.ItemDao;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.ErrorHandler;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.util.simpleview.SimpleView;
import com.progressoft.mpay.Unique;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerErrorHandler extends ErrorHandler {

    @Autowired
    private ItemDao itemDao;

    @Override
    public InvalidInputException handleError(Exception ex, SimpleView simpleView, Object entity, int actionKey) {
        InvalidInputException cause = null;
        if (ex instanceof DataIntegrityViolationException || isTransactionSystemException(ex)
                || ex.getCause() instanceof DataIntegrityViolationException
                || ex instanceof WorkflowException) {

            MPAY_Customer customer = (MPAY_Customer) entity;

            cause = Unique.validate(itemDao, customer, "clientRef", customer.getClientRef(),
                    "duplicat.customer.item.clientref");

            if (cause != null)
                return cause;

            cause = Unique.validate(itemDao, customer, "externalAcc", customer.getExternalAcc(),
                    "duplicat.customer.item.externalacc");

            if (cause != null)
                return cause;

            Map<String, Object> props = new HashMap<>();
            props.put("idType", customer.getIdType());
            props.put("idNum", customer.getIdNum());
            cause = Unique.validate(itemDao, customer, props, "duplicat.customer.item.id");
            if (cause != null)
                return cause;

            cause = handleMobiles(ex, customer);
        }

        return cause;
    }

    @SuppressWarnings("unchecked")
    private InvalidInputException handleMobiles(Exception ex, MPAY_Customer customer) {
        InvalidInputException cause = null;
        List<MPAY_CustomerMobile> mobiles = customer.getRefCustomerCustomerMobiles();
        if (mobiles != null) {
            for (MPAY_CustomerMobile mobile : mobiles) {
                cause = Unique.validate(itemDao, mobile, "mobileNumber", mobile.getMobileNumber(),
                        "duplicat.customer.item.mobile.number");
                if (cause != null)
                    return cause;

                cause = Unique.validate(itemDao, mobile, "externalAcc", mobile.getExternalAcc(),
                        "duplicat.customer.item.mobile.externalacc");
                if (cause != null)
                    return cause;
            }
        }

        if (ex.getMessage().contains("[name : mobileNumber, label : Mobile Number] does not match the regex")) {
            cause = new InvalidInputException();
            cause.getErrors().put(WorkflowService.WF_ARG_ERROR_MSGS,
                    AppContext.getMsg("regex.customer.item.mobilenumber"));
        }
        return cause;
    }

    private boolean isTransactionSystemException(Exception ex) {
        return ex instanceof TransactionSystemException || ex.getCause() instanceof TransactionSystemException;
    }
}
