package com.progressoft.mpay.registration.corporates;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.entities.MPAY_ServiceAccount;
import com.progressoft.mpay.entity.ServiceAccountMetaData;

public class PostEditServiceAccount implements FunctionProvider {

	private static final Logger logger = LoggerFactory.getLogger(PostEditServiceAccount.class);

	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		logger.debug("inside execute--------------------------");
		MPAY_ServiceAccount account = (MPAY_ServiceAccount) transientVars.get(WorkflowService.WF_ARG_BEAN);
		if(!account.getIsRegistered())
			return;
		MPAY_ServiceAccount originalAccount = (MPAY_ServiceAccount) transientVars.get(WorkflowService.WF_ARG_BEAN_ORIGINAL);
		account.setNewProfile(account.getRefProfile());
		account.setRefProfile(originalAccount.getRefProfile());
		account.setNewExternalAcc(account.getExternalAcc());
		account.setExternalAcc(originalAccount.getExternalAcc());
		account.setApprovedData(new ServiceAccountMetaData(originalAccount).toString());
		account.setExternalAcc(account.getIban());
		DataProvider.instance().updateServiceAccount(account);
	}
}
