package com.progressoft.mpay;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.apache.cxf.binding.soap.SoapHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

public class HandleMPAYRequest implements Processor {

    private static final Logger logger = LoggerFactory.getLogger(HandleMPAYRequest.class);

    @SuppressWarnings("deprecation")
    @Override
    public void process(Exchange exchange) throws Exception {

        @SuppressWarnings("unchecked")
        CxfPayload<SoapHeader> requestPayload = exchange.getIn().getBody(CxfPayload.class);
        List<Source> inElements = requestPayload.getBodySources();
        org.w3c.dom.Element msgEl = new XmlConverter().toDOMElement(inElements.get(0));

        for (int i = 0; i < msgEl.getChildNodes().getLength(); i++) {
            logger.debug("param: " + msgEl.getChildNodes().item(i).getNodeName() + ". value: "
                    + msgEl.getChildNodes().item(i).getTextContent());
        }

        // return response

        XmlConverter converter = new XmlConverter();
        List<Source> outElements = new ArrayList<>();
        String responseXML = "<processMessageResponse xmlns=\"http://www.progressoft.com/MPClear/\"><result xmlns=\"\">fofo</result></processMessageResponse>";
        Document outDocument = converter.toDOMDocument(responseXML);
        outElements.add(new DOMSource(outDocument.getDocumentElement()));

        CxfPayload<SoapHeader> responsePayload = new CxfPayload<>(null, outElements, null);
        exchange.getOut().setBody(responsePayload);
    }

}
