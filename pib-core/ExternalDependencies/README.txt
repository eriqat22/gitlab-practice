To add external dependencies:
1- copy the 'dependency-file.jar' file into 'ExternalDependencies' directory temporary ...
2- open the terminal in 'ExternalDependencies' directory.
2- install the 'dependency-file.jar' into local-repositoy as follows:

    mvn install:install-file -Dfile='dependency-file.jar' -DgroupId={group-id} -DartifactId={artificat-id} -Dversion={version} -Dpackaging=jar -DlocalRepositoryPath=../local-repo

3- add the repository to your POM file and set the <url>-path correctly to point to the 'local-repo' directory:

    <repositories>
    		<repository>
    			<id>mpay-local</id>
    			<url>file://${project.basedir}/../local-repo</url>
    		</repository>
    	</repositories>

4- add the dependency to your project as a regualr dependency:

    <dependency>
    		<groupId>{group-id}</groupId>
    		<artifactId>{artificat-id}</artifactId>
    		<version>{version}</version>
    </dependency>


5- commit all changes to local-repo.

6. to update dependency in the 'local-repo' directory:
         1- delete the old dependency from ".m2/repository/com/progressoft/babylon/"
         Then, you have 2 options:

            A- you can directly rename 'dependency-file.jar' file into  'dependency-file-1.0.0.jar'
            and replace the one in the 'local-repo' folder.
                ==> e.g. mpay.jar is copied always and replace the one in the 'local-repo' directory(see the top level mpay/POM file)
                    to keep the latest one ...
            OR
            B-delete the dependency from 'local-repo' directory and reinstall it again.

======================================================================================

Example:

    mvn install:install-file -Dfile='mpay.jar' -DgroupId=com.progressoft.babylon.mpay -DartifactId=mpay-entities -Dversion=1.0.0 -Dpackaging=jar -DlocalRepositoryPath=../local-repo

    ==> in POM:

    <dependency>
    		<groupId>com.progressoft.mars.pib.pib-core</groupId>
    		<artifactId>mpay-entities</artifactId>
    		<version>1.0.0</version>
    </dependency>



    mvn install:install-file -Dfile=PIN_Encryptor_1.0.jar -DgroupId=com.efinance.pension.ivrservices -DartifactId=PIN_Encryptor -Dversion=1.0.0 -Dpackaging=jar -DlocalRepositoryPath=../local-repo
    mvn install:install-file -Dfile=bcprov-ext-jdk14-1.45.jar -DgroupId=org.bouncycastle -DartifactId=bcprov-ext-jdk14 -Dversion=1.0.0 -Dpackaging=jar -DlocalRepositoryPath=../local-repo

