package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_PspDetails")
@XmlRootElement(name="PspDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_PspDetail extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_PspDetail(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String PSP_NATIONAL_I_D = "pspNationalID";
@Column(name="PSPNATIONALID", nullable=false, length=20)
private String pspNationalID;
public String getPspNationalID(){
return this.pspNationalID;
}
public void setPspNationalID(String pspNationalID){
this.pspNationalID = pspNationalID;
}

public static final String REF_P_S_P = "refPSP";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFPSPID", nullable=true)
private MPAY_Corporate refPSP;
@flexjson.JSON(include = false)
public MPAY_Corporate getRefPSP(){
return this.refPSP;
}
public void setRefPSP(MPAY_Corporate refPSP){
this.refPSP = refPSP;
}

public static final String REF_CLEARING_ACCOUNT = "refClearingAccount";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCLEARINGACCOUNTID", nullable=true)
private MPAY_Account refClearingAccount;
public MPAY_Account getRefClearingAccount(){
return this.refClearingAccount;
}
public void setRefClearingAccount(MPAY_Account refClearingAccount){
this.refClearingAccount = refClearingAccount;
}

public static final String REF_DIFFERENCES_ACCOUNT = "refDifferencesAccount";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFDIFFERENCESACCOUNTID", nullable=true)
private MPAY_Account refDifferencesAccount;
public MPAY_Account getRefDifferencesAccount(){
return this.refDifferencesAccount;
}
public void setRefDifferencesAccount(MPAY_Account refDifferencesAccount){
this.refDifferencesAccount = refDifferencesAccount;
}

@Override
public String toString() {
return "MPAY_PspDetail [id= " + getId() + ", pspNationalID= " + getPspNationalID() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getPspNationalID() == null) ? 0 : getPspNationalID().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_PspDetail))
return false;
else {MPAY_PspDetail other = (MPAY_PspDetail) obj;
return this.hashCode() == other.hashCode();}
}


}