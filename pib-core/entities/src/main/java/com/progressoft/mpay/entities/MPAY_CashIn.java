package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_CashIn")
@XmlRootElement(name="CashIn")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CashIn extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CashIn(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REF_MOBILE_ACCOUNT = "refMobileAccount";
@Column(name="REFMOBILEACCOUNT", nullable=true, length=100)
private String refMobileAccount;
public String getRefMobileAccount(){
return this.refMobileAccount;
}
public void setRefMobileAccount(String refMobileAccount){
this.refMobileAccount = refMobileAccount;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String IS_CHARGE_INCLUDED = "isChargeIncluded";
@Column(name="ISCHARGEINCLUDED", nullable=true, length=1)
private Boolean isChargeIncluded;
public Boolean getIsChargeIncluded(){
return this.isChargeIncluded;
}
public void setIsChargeIncluded(Boolean isChargeIncluded){
this.isChargeIncluded = isChargeIncluded;
}

public static final String TRANS_AMOUNT = "transAmount";
@Column(name="TRANSAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal transAmount;
public java.math.BigDecimal getTransAmount(){
return this.transAmount;
}
public void setTransAmount(java.math.BigDecimal transAmount){
this.transAmount = transAmount;
}

public static final String CHARGE_AMOUNT = "chargeAmount";
@Column(name="CHARGEAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal chargeAmount;
public java.math.BigDecimal getChargeAmount(){
return this.chargeAmount;
}
public void setChargeAmount(java.math.BigDecimal chargeAmount){
this.chargeAmount = chargeAmount;
}

public static final String TAX_AMOUNT = "taxAmount";
@Column(name="TAXAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal taxAmount;
public java.math.BigDecimal getTaxAmount(){
return this.taxAmount;
}
public void setTaxAmount(java.math.BigDecimal taxAmount){
this.taxAmount = taxAmount;
}

public static final String TOTAL_AMOUNT = "totalAmount";
@Column(name="TOTALAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal totalAmount;
public java.math.BigDecimal getTotalAmount(){
return this.totalAmount;
}
public void setTotalAmount(java.math.BigDecimal totalAmount){
this.totalAmount = totalAmount;
}

public static final String CUSTOMER_TYPE = "customerType";
@Column(name="CUSTOMERTYPE", nullable=true, length=19)
private String customerType;
public String getCustomerType(){
return this.customerType;
}
public void setCustomerType(String customerType){
this.customerType = customerType;
}

public static final String BANK_BRANCH = "bankBranch";
@Column(name="BANKBRANCH", nullable=true, length=100)
private String bankBranch;
public String getBankBranch(){
return this.bankBranch;
}
public void setBankBranch(String bankBranch){
this.bankBranch = bankBranch;
}

public static final String CHEQUE_NUMBER = "chequeNumber";
@Column(name="CHEQUENUMBER", nullable=true, length=256)
private String chequeNumber;
public String getChequeNumber(){
return this.chequeNumber;
}
public void setChequeNumber(String chequeNumber){
this.chequeNumber = chequeNumber;
}

public static final String REASON_DESC = "reasonDesc";
@Column(name="REASONDESC", nullable=true, length=4000)
private String reasonDesc;
public String getReasonDesc(){
return this.reasonDesc;
}
public void setReasonDesc(String reasonDesc){
this.reasonDesc = reasonDesc;
}

public static final String COMMENTS = "comments";
@Column(name="COMMENTS", nullable=true, length=255)
private String comments;
public String getComments(){
return this.comments;
}
public void setComments(String comments){
this.comments = comments;
}

public static final String REF_CUST_MOB = "refCustMob";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCUSTMOBID", nullable=false)
private MPAY_CustomerMobile refCustMob;
public MPAY_CustomerMobile getRefCustMob(){
return this.refCustMob;
}
public void setRefCustMob(MPAY_CustomerMobile refCustMob){
this.refCustMob = refCustMob;
}

public static final String REF_LAST_MSG_LOG = "refLastMsgLog";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFLASTMSGLOGID", nullable=true)
private MPAY_MpClearIntegMsgLog refLastMsgLog;
public MPAY_MpClearIntegMsgLog getRefLastMsgLog(){
return this.refLastMsgLog;
}
public void setRefLastMsgLog(MPAY_MpClearIntegMsgLog refLastMsgLog){
this.refLastMsgLog = refLastMsgLog;
}

public static final String REF_MESSAGE = "refMessage";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMESSAGEID", nullable=true)
private MPAY_MPayMessage refMessage;
public MPAY_MPayMessage getRefMessage(){
return this.refMessage;
}
public void setRefMessage(MPAY_MPayMessage refMessage){
this.refMessage = refMessage;
}

public static final String PROCESSING_STATUS = "processingStatus";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PROCESSINGSTATUSID", nullable=true)
private MPAY_ProcessingStatus processingStatus;
public MPAY_ProcessingStatus getProcessingStatus(){
return this.processingStatus;
}
public void setProcessingStatus(MPAY_ProcessingStatus processingStatus){
this.processingStatus = processingStatus;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=true)
private MPAY_Reason reason;
public MPAY_Reason getReason(){
return this.reason;
}
public void setReason(MPAY_Reason reason){
this.reason = reason;
}

public static final String BANK = "bank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BANKID", nullable=true)
private MPAY_Bank bank;
public MPAY_Bank getBank(){
return this.bank;
}
public void setBank(MPAY_Bank bank){
this.bank = bank;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_CashIn [id= " + getId() + ", refMobileAccount= " + getRefMobileAccount() + ", amount= " + getAmount() + ", isChargeIncluded= " + getIsChargeIncluded() + ", transAmount= " + getTransAmount() + ", chargeAmount= " + getChargeAmount() + ", taxAmount= " + getTaxAmount() + ", totalAmount= " + getTotalAmount() + ", customerType= " + getCustomerType() + ", bankBranch= " + getBankBranch() + ", chequeNumber= " + getChequeNumber() + ", reasonDesc= " + getReasonDesc() + ", comments= " + getComments() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRefMobileAccount() == null) ? 0 : getRefMobileAccount().hashCode());
result = prime * result + ((getBankBranch() == null) ? 0 : getBankBranch().hashCode());
result = prime * result + ((getChequeNumber() == null) ? 0 : getChequeNumber().hashCode());
result = prime * result + ((getReasonDesc() == null) ? 0 : getReasonDesc().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CashIn))
return false;
else {MPAY_CashIn other = (MPAY_CashIn) obj;
return this.hashCode() == other.hashCode();}
}


}