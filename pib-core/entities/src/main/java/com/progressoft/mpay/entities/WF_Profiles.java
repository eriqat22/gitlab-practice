package com.progressoft.mpay.entities;

public class WF_Profiles
 {
    // Steps
    public static final String STEP_Initialization = "101701";
    public static final String STEP_CreationApproval = "101702";
    public static final String STEP_RepairNew = "101703";
    public static final String STEP_EditNew = "101704";
    public static final String STEP_ViewApproved = "101705";
    public static final String STEP_EditApproved = "101706";
    public static final String STEP_ModificationApproval = "101707";
    public static final String STEP_RepairRejected = "101708";
    public static final String STEP_EditRejected = "101709";
    public static final String STEP_DeletionApproval = "101710";
    public static final String STEP_End = "101711";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_CreationApproval = "New Authorization";
    public static final String STEP_STATUS_RepairNew = "Repair New";
    public static final String STEP_STATUS_EditNew = "Edit New";
    public static final String STEP_STATUS_ViewApproved = "Approved";
    public static final String STEP_STATUS_EditApproved = "Modify";
    public static final String STEP_STATUS_ModificationApproval = "Authorization";
    public static final String STEP_STATUS_RepairRejected = "Repair Rejected";
    public static final String STEP_STATUS_EditRejected = "Edit Rejected";
    public static final String STEP_STATUS_DeletionApproval = "Deletion Authorization";
    public static final String STEP_STATUS_End = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Cancel = "Cancel";
    public static final String ACTION_NAME_Reset = "Reset";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveCreationApproval = "3";
    public static final String ACTION_CODE_RejectCreationApproval = "4";
    public static final String ACTION_CODE_EditRepairNew = "5";
    public static final String ACTION_CODE_DeleteRepairNew = "6";
    public static final String ACTION_CODE_SaveEditNew = "7";
    public static final String ACTION_CODE_CancelEditNew = "8";
    public static final String ACTION_CODE_EditViewApproved = "9";
    public static final String ACTION_CODE_DeleteViewApproved = "10";
    public static final String ACTION_CODE_SaveEditApproved = "11";
    public static final String ACTION_CODE_CancelEditApproved = "12";
    public static final String ACTION_CODE_ApproveModificationApproval = "13";
    public static final String ACTION_CODE_RejectModificationApproval = "14";
    public static final String ACTION_CODE_EditRepairRejected = "15";
    public static final String ACTION_CODE_ResetRepairRejected = "16";
    public static final String ACTION_CODE_SaveEditRejected = "17";
    public static final String ACTION_CODE_CancelEditRejected = "18";
    public static final String ACTION_CODE_ApproveDeletionApproval = "19";
    public static final String ACTION_CODE_RejectDeletionApproval = "20";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveCreationApproval = "724820467";
    public static final String ACTION_KEY_RejectCreationApproval = "1151288789";
    public static final String ACTION_KEY_EditRepairNew = "599902584";
    public static final String ACTION_KEY_DeleteRepairNew = "91783412";
    public static final String ACTION_KEY_SaveEditNew = "181692868";
    public static final String ACTION_KEY_CancelEditNew = "1609561768";
    public static final String ACTION_KEY_EditViewApproved = "14054304";
    public static final String ACTION_KEY_DeleteViewApproved = "1944415477";
    public static final String ACTION_KEY_SaveEditApproved = "313605449";
    public static final String ACTION_KEY_CancelEditApproved = "1010326143";
    public static final String ACTION_KEY_ApproveModificationApproval = "963171467";
    public static final String ACTION_KEY_RejectModificationApproval = "1398026141";
    public static final String ACTION_KEY_EditRepairRejected = "1967039721";
    public static final String ACTION_KEY_ResetRepairRejected = "1398103386";
    public static final String ACTION_KEY_SaveEditRejected = "868982123";
    public static final String ACTION_KEY_CancelEditRejected = "1047594295";
    public static final String ACTION_KEY_ApproveDeletionApproval = "2104038619";
    public static final String ACTION_KEY_RejectDeletionApproval = "1785711542";

}