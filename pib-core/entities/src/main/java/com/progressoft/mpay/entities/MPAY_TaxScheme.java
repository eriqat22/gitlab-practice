package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_TaxSchemes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="TaxSchemes")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_TaxScheme extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_TaxScheme(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String IS_INLUDED_INSIDE_FEES = "isInludedInsideFees";
@Column(name="ISINLUDEDINSIDEFEES", nullable=false, length=1)
private Boolean isInludedInsideFees;
public Boolean getIsInludedInsideFees(){
return this.isInludedInsideFees;
}
public void setIsInludedInsideFees(Boolean isInludedInsideFees){
this.isInludedInsideFees = isInludedInsideFees;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

public static final String REF_SCHEME_TAX_SCHEME_DETAILS = "refSchemeTaxSchemeDetails";
@OneToMany(mappedBy = "refScheme")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_TaxSchemeDetail> refSchemeTaxSchemeDetails;
public List<MPAY_TaxSchemeDetail> getRefSchemeTaxSchemeDetails(){
return this.refSchemeTaxSchemeDetails;
}
public void setRefSchemeTaxSchemeDetails(List<MPAY_TaxSchemeDetail> refSchemeTaxSchemeDetails){
this.refSchemeTaxSchemeDetails = refSchemeTaxSchemeDetails;
}

@Override
public String toString() {
return "MPAY_TaxScheme [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", isActive= " + getIsActive() + ", isInludedInsideFees= " + getIsInludedInsideFees() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_TaxScheme))
return false;
else {MPAY_TaxScheme other = (MPAY_TaxScheme) obj;
return this.hashCode() == other.hashCode();}
}


}