package com.progressoft.mpay.entities;

public class WF_NetworkManagement
 {
    // Steps
    public static final String STEP_Initialization = "102801";
    public static final String STEP_LoggedOut = "102802";
    public static final String STEP_LoggedIn = "102803";
    public static final String STEP_PendingReply = "102804";
    public static final String STEP_Failed = "102805";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_LoggedOut = "Logged Out";
    public static final String STEP_STATUS_LoggedIn = "Logged In";
    public static final String STEP_STATUS_PendingReply = "Pending Reply";
    public static final String STEP_STATUS_Failed = "Failed";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Login = "Login";
    public static final String ACTION_NAME_Logout = "Logout";
    public static final String ACTION_NAME_SVC_AutoLogin = "SVC_AutoLogin";
    public static final String ACTION_NAME_SVC_Force_Failed_1644 = "SVC_Force_Failed_1644";
    public static final String ACTION_NAME_SVC_Force_Failed_56 = "SVC_Force_Failed_56";
    public static final String ACTION_NAME_SVC_AcceptLogin = "SVC_AcceptLogin";
    public static final String ACTION_NAME_SVC_AcceptLogout = "SVC_AcceptLogout";
    public static final String ACTION_NAME_SVC_Reject = "SVC_Reject";
    public static final String ACTION_NAME_Resend = "Resend";
    public static final String ACTION_NAME_MoveToLoggedIn = "Move To  Logged-In";
    public static final String ACTION_NAME_MoveToLoggedOut = "Move To  Logged-Out";
    public static final String ACTION_NAME_SwitchAutoLogin = "Switch AutoLogin";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_LoginLoggedOut = "3";
    public static final String ACTION_CODE_LogoutLoggedIn = "4";
    public static final String ACTION_CODE_SVC_AutoLoginLoggedIn = "5";
    public static final String ACTION_CODE_SVC_Force_Failed_1644LoggedIn = "12";
    public static final String ACTION_CODE_SVC_Force_Failed_56LoggedIn = "13";
    public static final String ACTION_CODE_SVC_AcceptLoginPendingReply = "6";
    public static final String ACTION_CODE_SVC_AcceptLogoutPendingReply = "7";
    public static final String ACTION_CODE_SVC_RejectPendingReply = "8";
    public static final String ACTION_CODE_ResendPendingReply = "14";
    public static final String ACTION_CODE_MoveToLoggedInFailed = "9";
    public static final String ACTION_CODE_MoveToLoggedOutFailed = "10";
    public static final String ACTION_CODE_SwitchAutoLoginFailed = "11";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_LoginLoggedOut = "1402114885";
    public static final String ACTION_KEY_LogoutLoggedIn = "1583687308";
    public static final String ACTION_KEY_SVC_AutoLoginLoggedIn = "641828410";
    public static final String ACTION_KEY_SVC_Force_Failed_1644LoggedIn = "416455841";
    public static final String ACTION_KEY_SVC_Force_Failed_56LoggedIn = "1971411208";
    public static final String ACTION_KEY_SVC_AcceptLoginPendingReply = "1401260838";
    public static final String ACTION_KEY_SVC_AcceptLogoutPendingReply = "1674956133";
    public static final String ACTION_KEY_SVC_RejectPendingReply = "320670979";
    public static final String ACTION_KEY_ResendPendingReply = "1254070410";
    public static final String ACTION_KEY_MoveToLoggedInFailed = "603743847";
    public static final String ACTION_KEY_MoveToLoggedOutFailed = "1474967476";
    public static final String ACTION_KEY_SwitchAutoLoginFailed = "1899579731";

}