package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_TaxSlices")
@XmlRootElement(name="TaxSlices")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_TaxSlice extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_TaxSlice(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String TX_AMOUNT_LIMIT = "txAmountLimit";
@Column(name="TXAMOUNTLIMIT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal txAmountLimit;
public java.math.BigDecimal getTxAmountLimit(){
return this.txAmountLimit;
}
public void setTxAmountLimit(java.math.BigDecimal txAmountLimit){
this.txAmountLimit = txAmountLimit;
}

public static final String TAX_TYPE = "taxType";
@Column(name="TAXTYPE", nullable=false, length=1)
private String taxType;
public String getTaxType(){
return this.taxType;
}
public void setTaxType(String taxType){
this.taxType = taxType;
}

public static final String TAX_AMOUNT = "taxAmount";
@Column(name="TAXAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal taxAmount;
public java.math.BigDecimal getTaxAmount(){
return this.taxAmount;
}
public void setTaxAmount(java.math.BigDecimal taxAmount){
this.taxAmount = taxAmount;
}

public static final String TAX_PERCENT = "taxPercent";
@Column(name="TAXPERCENT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal taxPercent;
public java.math.BigDecimal getTaxPercent(){
return this.taxPercent;
}
public void setTaxPercent(java.math.BigDecimal taxPercent){
this.taxPercent = taxPercent;
}

public static final String MIN_AMOUNT = "minAmount";
@Column(name="MINAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal minAmount;
public java.math.BigDecimal getMinAmount(){
return this.minAmount;
}
public void setMinAmount(java.math.BigDecimal minAmount){
this.minAmount = minAmount;
}

public static final String MAX_AMOUNT = "maxAmount";
@Column(name="MAXAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal maxAmount;
public java.math.BigDecimal getMaxAmount(){
return this.maxAmount;
}
public void setMaxAmount(java.math.BigDecimal maxAmount){
this.maxAmount = maxAmount;
}

public static final String REF_TAX_DETAILS = "refTaxDetails";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTAXDETAILSID", nullable=true)
private MPAY_TaxSchemeDetail refTaxDetails;
public MPAY_TaxSchemeDetail getRefTaxDetails(){
return this.refTaxDetails;
}
public void setRefTaxDetails(MPAY_TaxSchemeDetail refTaxDetails){
this.refTaxDetails = refTaxDetails;
}

@Override
public String toString() {
return "MPAY_TaxSlice [id= " + getId() + ", txAmountLimit= " + getTxAmountLimit() + ", taxType= " + getTaxType() + ", taxAmount= " + getTaxAmount() + ", taxPercent= " + getTaxPercent() + ", minAmount= " + getMinAmount() + ", maxAmount= " + getMaxAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_TaxSlice))
return false;
else {MPAY_TaxSlice other = (MPAY_TaxSlice) obj;
return this.hashCode() == other.hashCode();}
}


}