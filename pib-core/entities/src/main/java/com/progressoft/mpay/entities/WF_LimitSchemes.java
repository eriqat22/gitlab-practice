package com.progressoft.mpay.entities;

public class WF_LimitSchemes
 {
    // Steps
    public static final String STEP_Initialization = "102101";
    public static final String STEP_CreationApproval = "102102";
    public static final String STEP_RepairNew = "102103";
    public static final String STEP_EditNew = "102104";
    public static final String STEP_ViewApproved = "102105";
    public static final String STEP_EditApproved = "102106";
    public static final String STEP_ModificationApproval = "102107";
    public static final String STEP_RepairRejected = "102108";
    public static final String STEP_EditRejected = "102109";
    public static final String STEP_DeletionApproval = "102110";
    public static final String STEP_End = "102111";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_CreationApproval = "New Authorization";
    public static final String STEP_STATUS_RepairNew = "Repair New";
    public static final String STEP_STATUS_EditNew = "Edit New";
    public static final String STEP_STATUS_ViewApproved = "Approved";
    public static final String STEP_STATUS_EditApproved = "Modify";
    public static final String STEP_STATUS_ModificationApproval = "Authorization";
    public static final String STEP_STATUS_RepairRejected = "Repair Rejected";
    public static final String STEP_STATUS_EditRejected = "Edit Rejected";
    public static final String STEP_STATUS_DeletionApproval = "Deletion Authorization";
    public static final String STEP_STATUS_End = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Cancel = "Cancel";
    public static final String ACTION_NAME_Reset = "Reset";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveCreationApproval = "3";
    public static final String ACTION_CODE_RejectCreationApproval = "4";
    public static final String ACTION_CODE_EditRepairNew = "5";
    public static final String ACTION_CODE_DeleteRepairNew = "6";
    public static final String ACTION_CODE_SaveEditNew = "7";
    public static final String ACTION_CODE_CancelEditNew = "8";
    public static final String ACTION_CODE_EditViewApproved = "9";
    public static final String ACTION_CODE_DeleteViewApproved = "10";
    public static final String ACTION_CODE_SaveEditApproved = "11";
    public static final String ACTION_CODE_CancelEditApproved = "12";
    public static final String ACTION_CODE_ApproveModificationApproval = "13";
    public static final String ACTION_CODE_RejectModificationApproval = "14";
    public static final String ACTION_CODE_EditRepairRejected = "15";
    public static final String ACTION_CODE_ResetRepairRejected = "16";
    public static final String ACTION_CODE_SaveEditRejected = "17";
    public static final String ACTION_CODE_CancelEditRejected = "18";
    public static final String ACTION_CODE_ApproveDeletionApproval = "19";
    public static final String ACTION_CODE_RejectDeletionApproval = "20";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveCreationApproval = "1659218179";
    public static final String ACTION_KEY_RejectCreationApproval = "1043964665";
    public static final String ACTION_KEY_EditRepairNew = "1214101784";
    public static final String ACTION_KEY_DeleteRepairNew = "430298206";
    public static final String ACTION_KEY_SaveEditNew = "2123369713";
    public static final String ACTION_KEY_CancelEditNew = "351270192";
    public static final String ACTION_KEY_EditViewApproved = "1928405569";
    public static final String ACTION_KEY_DeleteViewApproved = "994624676";
    public static final String ACTION_KEY_SaveEditApproved = "589847449";
    public static final String ACTION_KEY_CancelEditApproved = "787411898";
    public static final String ACTION_KEY_ApproveModificationApproval = "1975946584";
    public static final String ACTION_KEY_RejectModificationApproval = "1243259822";
    public static final String ACTION_KEY_EditRepairRejected = "1716967063";
    public static final String ACTION_KEY_ResetRepairRejected = "1390638119";
    public static final String ACTION_KEY_SaveEditRejected = "1097487321";
    public static final String ACTION_KEY_CancelEditRejected = "2111435252";
    public static final String ACTION_KEY_ApproveDeletionApproval = "1592869276";
    public static final String ACTION_KEY_RejectDeletionApproval = "1269314809";

}