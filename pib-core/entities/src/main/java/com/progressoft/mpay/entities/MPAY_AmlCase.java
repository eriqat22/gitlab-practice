package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_AmlCases")
@XmlRootElement(name="AmlCases")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_AmlCase extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_AmlCase(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String NOTES = "notes";
@Column(name="NOTES", nullable=true, length=4000)
private String notes;
public String getNotes(){
return this.notes;
}
public void setNotes(String notes){
this.notes = notes;
}

public static final String ENGLISH_NAME = "englishName";
@Column(name="ENGLISHNAME", nullable=true, length=1000)
private String englishName;
public String getEnglishName(){
return this.englishName;
}
public void setEnglishName(String englishName){
this.englishName = englishName;
}

public static final String ARABIC_NAME = "arabicName";
@Column(name="ARABICNAME", nullable=true, length=1000)
private String arabicName;
public String getArabicName(){
return this.arabicName;
}
public void setArabicName(String arabicName){
this.arabicName = arabicName;
}

public static final String NATIONAL_ID = "nationalId";
@Column(name="NATIONALID", nullable=true, length=255)
private String nationalId;
public String getNationalId(){
return this.nationalId;
}
public void setNationalId(String nationalId){
this.nationalId = nationalId;
}

public static final String MOBILE_NUMBER = "mobileNumber";
@Column(name="MOBILENUMBER", nullable=true, length=100)
private String mobileNumber;
public String getMobileNumber(){
return this.mobileNumber;
}
public void setMobileNumber(String mobileNumber){
this.mobileNumber = mobileNumber;
}

public static final String DATE_OF_BIRTH = "dateOfBirth";
@Column(name="DATEOFBIRTH", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date dateOfBirth;
public java.util.Date getDateOfBirth(){
return this.dateOfBirth;
}
public void setDateOfBirth(java.util.Date dateOfBirth){
this.dateOfBirth = dateOfBirth;
}

public static final String ADDRESS = "address";
@Column(name="ADDRESS", nullable=true, length=4000)
private String address;
public String getAddress(){
return this.address;
}
public void setAddress(String address){
this.address = address;
}

public static final String GENDER = "gender";
@Column(name="GENDER", nullable=true, length=10)
private String gender;
public String getGender(){
return this.gender;
}
public void setGender(String gender){
this.gender = gender;
}

public static final String ID_EXPIRY_DATE = "idExpiryDate";
@Column(name="IDEXPIRYDATE", nullable=true, length=100)
private String idExpiryDate;
public String getIdExpiryDate(){
return this.idExpiryDate;
}
public void setIdExpiryDate(String idExpiryDate){
this.idExpiryDate = idExpiryDate;
}

public static final String REF_CUSTOMER = "refCustomer";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCUSTOMERID", nullable=true)
private MPAY_Customer refCustomer;
public MPAY_Customer getRefCustomer(){
return this.refCustomer;
}
public void setRefCustomer(MPAY_Customer refCustomer){
this.refCustomer = refCustomer;
}

public static final String NATIONALITY = "nationality";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="NATIONALITYID", nullable=true)
private com.progressoft.jfw.model.bussinessobject.core.JfwCountry nationality;
public com.progressoft.jfw.model.bussinessobject.core.JfwCountry getNationality(){
return this.nationality;
}
public void setNationality(com.progressoft.jfw.model.bussinessobject.core.JfwCountry nationality){
this.nationality = nationality;
}

public static final String REF_CASE_AML_HITS = "refCaseAmlHits";
@OneToMany(mappedBy = "refCase")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_AmlHit> refCaseAmlHits;
public List<MPAY_AmlHit> getRefCaseAmlHits(){
return this.refCaseAmlHits;
}
public void setRefCaseAmlHits(List<MPAY_AmlHit> refCaseAmlHits){
this.refCaseAmlHits = refCaseAmlHits;
}

@Override
public String toString() {
return "MPAY_AmlCase [id= " + getId() + ", notes= " + getNotes() + ", englishName= " + getEnglishName() + ", arabicName= " + getArabicName() + ", nationalId= " + getNationalId() + ", mobileNumber= " + getMobileNumber() + ", dateOfBirth= " + getDateOfBirth() + ", address= " + getAddress() + ", gender= " + getGender() + ", idExpiryDate= " + getIdExpiryDate() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getNotes() == null) ? 0 : getNotes().hashCode());
result = prime * result + ((getEnglishName() == null) ? 0 : getEnglishName().hashCode());
result = prime * result + ((getArabicName() == null) ? 0 : getArabicName().hashCode());
result = prime * result + ((getNationalId() == null) ? 0 : getNationalId().hashCode());
result = prime * result + ((getMobileNumber() == null) ? 0 : getMobileNumber().hashCode());
result = prime * result + ((getDateOfBirth() == null) ? 0 : getDateOfBirth().hashCode());
result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
result = prime * result + ((getIdExpiryDate() == null) ? 0 : getIdExpiryDate().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_AmlCase))
return false;
else {MPAY_AmlCase other = (MPAY_AmlCase) obj;
return this.hashCode() == other.hashCode();}
}


}