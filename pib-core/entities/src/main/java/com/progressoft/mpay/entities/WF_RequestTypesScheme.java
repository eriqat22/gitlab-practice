package com.progressoft.mpay.entities;

public class WF_RequestTypesScheme
 {
    // Steps
    public static final String STEP_Initialization = "904601";
    public static final String STEP_ApprovalNewStep = "904602";
    public static final String STEP_RepairStep = "904603";
    public static final String STEP_ApprovalRepairStep = "904604";
    public static final String STEP_ActiveStep = "904605";
    public static final String STEP_EditStep = "904606";
    public static final String STEP_ApprovalEditStep = "904607";
    public static final String STEP_ApprovalDeleteStep = "904608";
    public static final String STEP_DeletedStep = "904609";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_ApprovalNewStep = "Approval New";
    public static final String STEP_STATUS_RepairStep = "Repair";
    public static final String STEP_STATUS_ApprovalRepairStep = "Approval Repair";
    public static final String STEP_STATUS_ActiveStep = "Active";
    public static final String STEP_STATUS_EditStep = "Edit";
    public static final String STEP_STATUS_ApprovalEditStep = "Approval Edit";
    public static final String STEP_STATUS_ApprovalDeleteStep = "Approval Delete";
    public static final String STEP_STATUS_DeletedStep = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveApprovalNewStep = "3";
    public static final String ACTION_CODE_RejectApprovalNewStep = "4";
    public static final String ACTION_CODE_SubmitRepairStep = "5";
    public static final String ACTION_CODE_DeleteRepairStep = "6";
    public static final String ACTION_CODE_ApproveApprovalRepairStep = "7";
    public static final String ACTION_CODE_RejectApprovalRepairStep = "8";
    public static final String ACTION_CODE_EditActiveStep = "9";
    public static final String ACTION_CODE_DeleteActiveStep = "12";
    public static final String ACTION_CODE_SubmitEditStep = "10";
    public static final String ACTION_CODE_CancelEditStep = "11";
    public static final String ACTION_CODE_ApproveApprovalEditStep = "13";
    public static final String ACTION_CODE_RejectApprovalEditStep = "14";
    public static final String ACTION_CODE_ApproveApprovalDeleteStep = "15";
    public static final String ACTION_CODE_RejectApprovalDeleteStep = "16";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveApprovalNewStep = "385891785";
    public static final String ACTION_KEY_RejectApprovalNewStep = "850291755";
    public static final String ACTION_KEY_SubmitRepairStep = "1384202976";
    public static final String ACTION_KEY_DeleteRepairStep = "1619398476";
    public static final String ACTION_KEY_ApproveApprovalRepairStep = "1753774974";
    public static final String ACTION_KEY_RejectApprovalRepairStep = "1425705265";
    public static final String ACTION_KEY_EditActiveStep = "1538279077";
    public static final String ACTION_KEY_DeleteActiveStep = "1033985813";
    public static final String ACTION_KEY_SubmitEditStep = "1498475734";
    public static final String ACTION_KEY_CancelEditStep = "1093953310";
    public static final String ACTION_KEY_ApproveApprovalEditStep = "1329311576";
    public static final String ACTION_KEY_RejectApprovalEditStep = "892888841";
    public static final String ACTION_KEY_ApproveApprovalDeleteStep = "890573466";
    public static final String ACTION_KEY_RejectApprovalDeleteStep = "1383317632";

}