package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_EndPointOperations",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"OPERATION","Z_TENANT_ID"})
})
@XmlRootElement(name="EndPointOperations")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_EndPointOperation extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_EndPointOperation(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String NAME = "name";
@Column(name="NAME", nullable=false, length=500)
private String name;
public String getName(){
return this.name;
}
public void setName(String name){
this.name = name;
}

public static final String CODE = "code";
@Column(name="CODE", nullable=false, length=4)
private String code;
public String getCode(){
return this.code;
}
public void setCode(String code){
this.code = code;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=1000)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String OPERATION = "operation";
@Column(name="OPERATION", nullable=false, length=100)
private String operation;
public String getOperation(){
return this.operation;
}
public void setOperation(String operation){
this.operation = operation;
}

public static final String PROCESSOR = "processor";
@Column(name="PROCESSOR", nullable=false, length=1000)
private String processor;
public String getProcessor(){
return this.processor;
}
public void setProcessor(String processor){
this.processor = processor;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String IS_SYSTEM = "isSystem";
@Column(name="ISSYSTEM", nullable=false, length=1)
private Boolean isSystem;
public Boolean getIsSystem(){
return this.isSystem;
}
public void setIsSystem(Boolean isSystem){
this.isSystem = isSystem;
}

public static final String IS_CHARGE_REVERSAL_ENABLED = "isChargeReversalEnabled";
@Column(name="ISCHARGEREVERSALENABLED", nullable=false, length=1)
private Boolean isChargeReversalEnabled;
public Boolean getIsChargeReversalEnabled(){
return this.isChargeReversalEnabled;
}
public void setIsChargeReversalEnabled(Boolean isChargeReversalEnabled){
this.isChargeReversalEnabled = isChargeReversalEnabled;
}

public static final String IS_TAX_REVERSAL_ENABLED = "isTaxReversalEnabled";
@Column(name="ISTAXREVERSALENABLED", nullable=false, length=1)
private Boolean isTaxReversalEnabled;
public Boolean getIsTaxReversalEnabled(){
return this.isTaxReversalEnabled;
}
public void setIsTaxReversalEnabled(Boolean isTaxReversalEnabled){
this.isTaxReversalEnabled = isTaxReversalEnabled;
}

public static final String IS_PERSISTABLE = "isPersistable";
@Column(name="ISPERSISTABLE", nullable=true, length=1)
private Boolean isPersistable;
public Boolean getIsPersistable(){
return this.isPersistable;
}
public void setIsPersistable(Boolean isPersistable){
this.isPersistable = isPersistable;
}

public static final String ENABLE_S_M_S = "enableSMS";
@Column(name="ENABLESMS", nullable=true, length=1)
private Boolean enableSMS;
public Boolean getEnableSMS(){
return this.enableSMS;
}
public void setEnableSMS(Boolean enableSMS){
this.enableSMS = enableSMS;
}

public static final String ENABLE_EMAIL = "enableEmail";
@Column(name="ENABLEEMAIL", nullable=true, length=1)
private Boolean enableEmail;
public Boolean getEnableEmail(){
return this.enableEmail;
}
public void setEnableEmail(Boolean enableEmail){
this.enableEmail = enableEmail;
}

public static final String ENABLE_PUSH_NOTIFICATION = "enablePushNotification";
@Column(name="ENABLEPUSHNOTIFICATION", nullable=true, length=1)
private Boolean enablePushNotification;
public Boolean getEnablePushNotification(){
return this.enablePushNotification;
}
public void setEnablePushNotification(Boolean enablePushNotification){
this.enablePushNotification = enablePushNotification;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=false)
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

public static final String REF_OPERATION_TRANSACTIONS = "refOperationTransactions";
@OneToMany(mappedBy = "refOperation")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> refOperationTransactions;
public List<MPAY_Transaction> getRefOperationTransactions(){
return this.refOperationTransactions;
}
public void setRefOperationTransactions(List<MPAY_Transaction> refOperationTransactions){
this.refOperationTransactions = refOperationTransactions;
}

public static final String REF_OPERATION_NOTIFICATION_TEMPLATES = "refOperationNotificationTemplates";
@OneToMany(mappedBy = "refOperation")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_NotificationTemplate> refOperationNotificationTemplates;
public List<MPAY_NotificationTemplate> getRefOperationNotificationTemplates(){
return this.refOperationNotificationTemplates;
}
public void setRefOperationNotificationTemplates(List<MPAY_NotificationTemplate> refOperationNotificationTemplates){
this.refOperationNotificationTemplates = refOperationNotificationTemplates;
}

public static final String REF_OPERATION_TRANSACTION_CONFIGS = "refOperationTransactionConfigs";
@OneToMany(mappedBy = "refOperation")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_TransactionConfig> refOperationTransactionConfigs;
public List<MPAY_TransactionConfig> getRefOperationTransactionConfigs(){
return this.refOperationTransactionConfigs;
}
public void setRefOperationTransactionConfigs(List<MPAY_TransactionConfig> refOperationTransactionConfigs){
this.refOperationTransactionConfigs = refOperationTransactionConfigs;
}

public static final String REF_OPERATION_M_PAY_MESSAGES = "refOperationMPayMessages";
@OneToMany(mappedBy = "refOperation")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_MPayMessage> refOperationMPayMessages;
public List<MPAY_MPayMessage> getRefOperationMPayMessages(){
return this.refOperationMPayMessages;
}
public void setRefOperationMPayMessages(List<MPAY_MPayMessage> refOperationMPayMessages){
this.refOperationMPayMessages = refOperationMPayMessages;
}

@Override
public String toString() {
return "MPAY_EndPointOperation [id= " + getId() + ", name= " + getName() + ", code= " + getCode() + ", description= " + getDescription() + ", operation= " + getOperation() + ", processor= " + getProcessor() + ", isActive= " + getIsActive() + ", isSystem= " + getIsSystem() + ", isChargeReversalEnabled= " + getIsChargeReversalEnabled() + ", isTaxReversalEnabled= " + getIsTaxReversalEnabled() + ", isPersistable= " + getIsPersistable() + ", enableSMS= " + getEnableSMS() + ", enableEmail= " + getEnableEmail() + ", enablePushNotification= " + getEnablePushNotification() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getOperation() == null) ? 0 : getOperation().hashCode());
result = prime * result + ((getProcessor() == null) ? 0 : getProcessor().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_EndPointOperation))
return false;
else {MPAY_EndPointOperation other = (MPAY_EndPointOperation) obj;
return this.hashCode() == other.hashCode();}
}


}