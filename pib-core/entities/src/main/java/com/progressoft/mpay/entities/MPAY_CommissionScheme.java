package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_CommissionSchemes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="CommissionSchemes")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CommissionScheme extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CommissionScheme(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String PROCESSOR = "processor";
@Column(name="PROCESSOR", nullable=false, length=255)
private String processor;
public String getProcessor(){
return this.processor;
}
public void setProcessor(String processor){
this.processor = processor;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String REF_COMMISSION_SCHEME_COMMISSIONS = "refCommissionSchemeCommissions";
@OneToMany(mappedBy = "refCommissionScheme")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Commission> refCommissionSchemeCommissions;
public List<MPAY_Commission> getRefCommissionSchemeCommissions(){
return this.refCommissionSchemeCommissions;
}
public void setRefCommissionSchemeCommissions(List<MPAY_Commission> refCommissionSchemeCommissions){
this.refCommissionSchemeCommissions = refCommissionSchemeCommissions;
}

@Override
public String toString() {
return "MPAY_CommissionScheme [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", processor= " + getProcessor() + ", isActive= " + getIsActive() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getProcessor() == null) ? 0 : getProcessor().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CommissionScheme))
return false;
else {MPAY_CommissionScheme other = (MPAY_CommissionScheme) obj;
return this.hashCode() == other.hashCode();}
}


}