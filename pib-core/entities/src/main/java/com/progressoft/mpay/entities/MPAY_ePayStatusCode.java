package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_ePayStatusCodes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="ePayStatusCodes")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_ePayStatusCode extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ePayStatusCode(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String EPAY_MESSAGE_CODE = "epayMessageCode";
@Column(name="EPAYMESSAGECODE", nullable=true, length=12)
private String epayMessageCode;
public String getEpayMessageCode(){
return this.epayMessageCode;
}
public void setEpayMessageCode(String epayMessageCode){
this.epayMessageCode = epayMessageCode;
}

public static final String E_PAY_STATUS_CODES_N_L_S = "ePayStatusCodesNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "sttsId")
@MapKey(name="languageCode")
private Map<String, MPAY_ePayStatusCodes_NLS> ePayStatusCodesNLS;
public Map<String, MPAY_ePayStatusCodes_NLS> getEPayStatusCodesNLS(){
return this.ePayStatusCodesNLS;
}
public void setEPayStatusCodesNLS(Map<String, MPAY_ePayStatusCodes_NLS> ePayStatusCodesNLS){
this.ePayStatusCodesNLS = ePayStatusCodesNLS;
}

@Override
public String toString() {
return "MPAY_ePayStatusCode [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", epayMessageCode= " + getEpayMessageCode() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getEpayMessageCode() == null) ? 0 : getEpayMessageCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ePayStatusCode))
return false;
else {MPAY_ePayStatusCode other = (MPAY_ePayStatusCode) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.ePayStatusCodesNLS;
}
}