package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_SaibOtp")
@XmlRootElement(name="SaibOtp")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_SaibOtp extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_SaibOtp(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String MOBILE_NUMBER = "mobileNumber";
@Column(name="MOBILENUMBER", nullable=false, length=100)
private String mobileNumber;
public String getMobileNumber(){
return this.mobileNumber;
}
public void setMobileNumber(String mobileNumber){
this.mobileNumber = mobileNumber;
}

public static final String ATTEMPTS_NUM = "attemptsNum";
@Column(name="ATTEMPTSNUM", nullable=false, length=100)
private Long attemptsNum;
public Long getAttemptsNum(){
return this.attemptsNum;
}
public void setAttemptsNum(Long attemptsNum){
this.attemptsNum = attemptsNum;
}

public static final String OTP = "otp";
@Column(name="OTP", nullable=false, length=255)
private String otp;
public String getOtp(){
return this.otp;
}
public void setOtp(String otp){
this.otp = otp;
}

public static final String ID_NUM = "idNum";
@Column(name="IDNUM", nullable=false, length=100)
private String idNum;
public String getIdNum(){
return this.idNum;
}
public void setIdNum(String idNum){
this.idNum = idNum;
}

public static final String NATIONALITY = "nationality";
@Column(name="NATIONALITY", nullable=true, length=100)
private String nationality;
public String getNationality(){
return this.nationality;
}
public void setNationality(String nationality){
this.nationality = nationality;
}

public static final String DOB = "dob";
@Column(name="DOB", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date dob;
public java.util.Date getDob(){
return this.dob;
}
public void setDob(java.util.Date dob){
this.dob = dob;
}

public static final String REQUEST_RETRY_NUMBER = "requestRetryNumber";
@Column(name="REQUESTRETRYNUMBER", nullable=false, length=100)
private Long requestRetryNumber;
public Long getRequestRetryNumber(){
return this.requestRetryNumber;
}
public void setRequestRetryNumber(Long requestRetryNumber){
this.requestRetryNumber = requestRetryNumber;
}

@Override
public String toString() {
return "MPAY_SaibOtp [id= " + getId() + ", mobileNumber= " + getMobileNumber() + ", attemptsNum= " + getAttemptsNum() + ", otp= " + getOtp() + ", idNum= " + getIdNum() + ", nationality= " + getNationality() + ", dob= " + getDob() + ", requestRetryNumber= " + getRequestRetryNumber() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getMobileNumber() == null) ? 0 : getMobileNumber().hashCode());
result = prime * result + ((getAttemptsNum() == null) ? 0 : getAttemptsNum().hashCode());
result = prime * result + ((getOtp() == null) ? 0 : getOtp().hashCode());
result = prime * result + ((getIdNum() == null) ? 0 : getIdNum().hashCode());
result = prime * result + ((getNationality() == null) ? 0 : getNationality().hashCode());
result = prime * result + ((getDob() == null) ? 0 : getDob().hashCode());
result = prime * result + ((getRequestRetryNumber() == null) ? 0 : getRequestRetryNumber().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_SaibOtp))
return false;
else {MPAY_SaibOtp other = (MPAY_SaibOtp) obj;
return this.hashCode() == other.hashCode();}
}


}