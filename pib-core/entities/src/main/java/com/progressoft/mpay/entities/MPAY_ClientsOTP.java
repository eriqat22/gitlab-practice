package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_ClientsOTPs")
@XmlRootElement(name="ClientsOTPs")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ClientsOTP extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ClientsOTP(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CODE = "code";
@Column(name="CODE", nullable=false, length=255)
private String code;
public String getCode(){
return this.code;
}
public void setCode(String code){
this.code = code;
}

public static final String REQUEST_TIME = "requestTime";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REQUESTTIME", nullable=false, length=12)
private java.sql.Timestamp requestTime;
public java.sql.Timestamp getRequestTime(){
return this.requestTime;
}
public void setRequestTime(java.sql.Timestamp requestTime){
this.requestTime = requestTime;
}

public static final String MAX_AMOUNT = "maxAmount";
@Column(name="MAXAMOUNT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal maxAmount;
public java.math.BigDecimal getMaxAmount(){
return this.maxAmount;
}
public void setMaxAmount(java.math.BigDecimal maxAmount){
this.maxAmount = maxAmount;
}

public static final String SENDER_MOBILE = "senderMobile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SENDERMOBILEID", nullable=true)
private MPAY_CustomerMobile senderMobile;
public MPAY_CustomerMobile getSenderMobile(){
return this.senderMobile;
}
public void setSenderMobile(MPAY_CustomerMobile senderMobile){
this.senderMobile = senderMobile;
}

public static final String SENDER_SERVICE = "senderService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SENDERSERVICEID", nullable=true)
private MPAY_CorpoarteService senderService;
public MPAY_CorpoarteService getSenderService(){
return this.senderService;
}
public void setSenderService(MPAY_CorpoarteService senderService){
this.senderService = senderService;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=false)
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_ClientsOTP [id= " + getId() + ", code= " + getCode() + ", requestTime= " + getRequestTime() + ", maxAmount= " + getMaxAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ClientsOTP))
return false;
else {MPAY_ClientsOTP other = (MPAY_ClientsOTP) obj;
return this.hashCode() == other.hashCode();}
}


}