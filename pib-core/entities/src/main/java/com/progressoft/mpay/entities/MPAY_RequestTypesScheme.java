package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_RequestTypesScheme")
@XmlRootElement(name="RequestTypesScheme")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_RequestTypesScheme extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_RequestTypesScheme(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REF_SCHEME_REQUEST_TYPES_DETAILS = "refSchemeRequestTypesDetails";
@OneToMany(mappedBy = "refScheme")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_RequestTypesDetail> refSchemeRequestTypesDetails;
public List<MPAY_RequestTypesDetail> getRefSchemeRequestTypesDetails(){
return this.refSchemeRequestTypesDetails;
}
public void setRefSchemeRequestTypesDetails(List<MPAY_RequestTypesDetail> refSchemeRequestTypesDetails){
this.refSchemeRequestTypesDetails = refSchemeRequestTypesDetails;
}

@Override
public String toString() {
return "MPAY_RequestTypesScheme [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_RequestTypesScheme))
return false;
else {MPAY_RequestTypesScheme other = (MPAY_RequestTypesScheme) obj;
return this.hashCode() == other.hashCode();}
}


}