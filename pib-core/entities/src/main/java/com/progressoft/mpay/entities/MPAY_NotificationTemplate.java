package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_NotificationTemplates",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"REFOPERATIONID","REFTYPEID","REFLANGUAGEID","Z_TENANT_ID"})
})
@XmlRootElement(name="NotificationTemplates")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_NotificationTemplate extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_NotificationTemplate(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String TEMPLATE = "template";
@Column(name="TEMPLATE", nullable=false, length=4000)
private String template;
public String getTemplate(){
return this.template;
}
public void setTemplate(String template){
this.template = template;
}

public static final String REF_OPERATION = "refOperation";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFOPERATIONID", nullable=false)
private MPAY_EndPointOperation refOperation;
public MPAY_EndPointOperation getRefOperation(){
return this.refOperation;
}
public void setRefOperation(MPAY_EndPointOperation refOperation){
this.refOperation = refOperation;
}

public static final String REF_TYPE = "refType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTYPEID", nullable=false)
private MPAY_NotificationType refType;
public MPAY_NotificationType getRefType(){
return this.refType;
}
public void setRefType(MPAY_NotificationType refType){
this.refType = refType;
}

public static final String REF_LANGUAGE = "refLanguage";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFLANGUAGEID", nullable=false)
private MPAY_Language refLanguage;
public MPAY_Language getRefLanguage(){
return this.refLanguage;
}
public void setRefLanguage(MPAY_Language refLanguage){
this.refLanguage = refLanguage;
}

public static final String NOTIFICATION_CHANNEL = "notificationChannel";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="NOTIFICATIONCHANNELID", nullable=false)
private MPAY_NotificationChannel notificationChannel;
public MPAY_NotificationChannel getNotificationChannel(){
return this.notificationChannel;
}
public void setNotificationChannel(MPAY_NotificationChannel notificationChannel){
this.notificationChannel = notificationChannel;
}

@Override
public String toString() {
return "MPAY_NotificationTemplate [id= " + getId() + ", template= " + getTemplate() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getTemplate() == null) ? 0 : getTemplate().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_NotificationTemplate))
return false;
else {MPAY_NotificationTemplate other = (MPAY_NotificationTemplate) obj;
return this.hashCode() == other.hashCode();}
}


}