package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_Service_Trans_Rep")
@XmlRootElement(name="Service_Trans_Rep")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Service_Trans_Rep extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Service_Trans_Rep(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String EXTRACT_STAMP = "extractStamp";
@Column(name="EXTRACTSTAMP", nullable=true, length=250)
private String extractStamp;
public String getExtractStamp(){
return this.extractStamp;
}
public void setExtractStamp(String extractStamp){
this.extractStamp = extractStamp;
}

public static final String CORPORATE_NAME = "corporateName";
@Column(name="CORPORATENAME", nullable=true, length=250)
private String corporateName;
public String getCorporateName(){
return this.corporateName;
}
public void setCorporateName(String corporateName){
this.corporateName = corporateName;
}

public static final String SERVICE_NAME = "serviceName";
@Column(name="SERVICENAME", nullable=true, length=250)
private String serviceName;
public String getServiceName(){
return this.serviceName;
}
public void setServiceName(String serviceName){
this.serviceName = serviceName;
}

public static final String SERVICE_STATUS = "serviceStatus";
@Column(name="SERVICESTATUS", nullable=true, length=250)
private String serviceStatus;
public String getServiceStatus(){
return this.serviceStatus;
}
public void setServiceStatus(String serviceStatus){
this.serviceStatus = serviceStatus;
}

public static final String SERVICE_BANK_NAME = "serviceBankName";
@Column(name="SERVICEBANKNAME", nullable=true, length=250)
private String serviceBankName;
public String getServiceBankName(){
return this.serviceBankName;
}
public void setServiceBankName(String serviceBankName){
this.serviceBankName = serviceBankName;
}

public static final String SERVICE_BANK_NUMBER = "serviceBankNumber";
@Column(name="SERVICEBANKNUMBER", nullable=true, length=250)
private String serviceBankNumber;
public String getServiceBankNumber(){
return this.serviceBankNumber;
}
public void setServiceBankNumber(String serviceBankNumber){
this.serviceBankNumber = serviceBankNumber;
}

public static final String SERVICE_I_B_A_N = "serviceIBAN";
@Column(name="SERVICEIBAN", nullable=true, length=250)
private String serviceIBAN;
public String getServiceIBAN(){
return this.serviceIBAN;
}
public void setServiceIBAN(String serviceIBAN){
this.serviceIBAN = serviceIBAN;
}

public static final String SERVICE_M_P_A_Y_ACCOUNT = "serviceMPAYAccount";
@Column(name="SERVICEMPAYACCOUNT", nullable=true, length=250)
private String serviceMPAYAccount;
public String getServiceMPAYAccount(){
return this.serviceMPAYAccount;
}
public void setServiceMPAYAccount(String serviceMPAYAccount){
this.serviceMPAYAccount = serviceMPAYAccount;
}

public static final String TRN_DATE = "trnDate";
@Column(name="TRNDATE", nullable=true, length=250)
private String trnDate;
public String getTrnDate(){
return this.trnDate;
}
public void setTrnDate(String trnDate){
this.trnDate = trnDate;
}

public static final String TRN_TIME = "trnTime";
@Column(name="TRNTIME", nullable=true, length=250)
private String trnTime;
public String getTrnTime(){
return this.trnTime;
}
public void setTrnTime(String trnTime){
this.trnTime = trnTime;
}

public static final String TRN_TYPE = "trnType";
@Column(name="TRNTYPE", nullable=true, length=250)
private String trnType;
public String getTrnType(){
return this.trnType;
}
public void setTrnType(String trnType){
this.trnType = trnType;
}

public static final String TRN_SIDE = "trnSide";
@Column(name="TRNSIDE", nullable=true, length=250)
private String trnSide;
public String getTrnSide(){
return this.trnSide;
}
public void setTrnSide(String trnSide){
this.trnSide = trnSide;
}

public static final String TRN_AMOUNT = "trnAmount";
@Column(name="TRNAMOUNT", nullable=true, length=250)
private String trnAmount;
public String getTrnAmount(){
return this.trnAmount;
}
public void setTrnAmount(String trnAmount){
this.trnAmount = trnAmount;
}

public static final String TRN_CHARGES = "trnCharges";
@Column(name="TRNCHARGES", nullable=true, length=250)
private String trnCharges;
public String getTrnCharges(){
return this.trnCharges;
}
public void setTrnCharges(String trnCharges){
this.trnCharges = trnCharges;
}

public static final String TRN_V_A_T = "trnVAT";
@Column(name="TRNVAT", nullable=true, length=250)
private String trnVAT;
public String getTrnVAT(){
return this.trnVAT;
}
public void setTrnVAT(String trnVAT){
this.trnVAT = trnVAT;
}

public static final String TRN_NET_AMOUNT = "trnNetAmount";
@Column(name="TRNNETAMOUNT", nullable=true, length=250)
private String trnNetAmount;
public String getTrnNetAmount(){
return this.trnNetAmount;
}
public void setTrnNetAmount(String trnNetAmount){
this.trnNetAmount = trnNetAmount;
}

public static final String TRN_REFERNCE_NO = "trnRefernceNo";
@Column(name="TRNREFERNCENO", nullable=true, length=250)
private String trnRefernceNo;
public String getTrnRefernceNo(){
return this.trnRefernceNo;
}
public void setTrnRefernceNo(String trnRefernceNo){
this.trnRefernceNo = trnRefernceNo;
}

public static final String TRN_DESCRIPTION = "trnDescription";
@Column(name="TRNDESCRIPTION", nullable=true, length=250)
private String trnDescription;
public String getTrnDescription(){
return this.trnDescription;
}
public void setTrnDescription(String trnDescription){
this.trnDescription = trnDescription;
}

public static final String SERVICE_BALANCE_BEFORE_TRN = "serviceBalanceBeforeTrn";
@Column(name="SERVICEBALANCEBEFORETRN", nullable=true, length=250)
private String serviceBalanceBeforeTrn;
public String getServiceBalanceBeforeTrn(){
return this.serviceBalanceBeforeTrn;
}
public void setServiceBalanceBeforeTrn(String serviceBalanceBeforeTrn){
this.serviceBalanceBeforeTrn = serviceBalanceBeforeTrn;
}

public static final String SERVICE_BALANCE_AFTER_TRN = "serviceBalanceAfterTrn";
@Column(name="SERVICEBALANCEAFTERTRN", nullable=true, length=250)
private String serviceBalanceAfterTrn;
public String getServiceBalanceAfterTrn(){
return this.serviceBalanceAfterTrn;
}
public void setServiceBalanceAfterTrn(String serviceBalanceAfterTrn){
this.serviceBalanceAfterTrn = serviceBalanceAfterTrn;
}

public static final String OTHER_PARTY_MOBILE_NO = "otherPartyMobileNo";
@Column(name="OTHERPARTYMOBILENO", nullable=true, length=250)
private String otherPartyMobileNo;
public String getOtherPartyMobileNo(){
return this.otherPartyMobileNo;
}
public void setOtherPartyMobileNo(String otherPartyMobileNo){
this.otherPartyMobileNo = otherPartyMobileNo;
}

public static final String OTHER_PARTY_AMOUNT = "otherPartyAmount";
@Column(name="OTHERPARTYAMOUNT", nullable=true, length=250)
private String otherPartyAmount;
public String getOtherPartyAmount(){
return this.otherPartyAmount;
}
public void setOtherPartyAmount(String otherPartyAmount){
this.otherPartyAmount = otherPartyAmount;
}

@Override
public String toString() {
return "MPAY_Service_Trans_Rep [id= " + getId() + ", extractStamp= " + getExtractStamp() + ", corporateName= " + getCorporateName() + ", serviceName= " + getServiceName() + ", serviceStatus= " + getServiceStatus() + ", serviceBankName= " + getServiceBankName() + ", serviceBankNumber= " + getServiceBankNumber() + ", serviceIBAN= " + getServiceIBAN() + ", serviceMPAYAccount= " + getServiceMPAYAccount() + ", trnDate= " + getTrnDate() + ", trnTime= " + getTrnTime() + ", trnType= " + getTrnType() + ", trnSide= " + getTrnSide() + ", trnAmount= " + getTrnAmount() + ", trnCharges= " + getTrnCharges() + ", trnVAT= " + getTrnVAT() + ", trnNetAmount= " + getTrnNetAmount() + ", trnRefernceNo= " + getTrnRefernceNo() + ", trnDescription= " + getTrnDescription() + ", serviceBalanceBeforeTrn= " + getServiceBalanceBeforeTrn() + ", serviceBalanceAfterTrn= " + getServiceBalanceAfterTrn() + ", otherPartyMobileNo= " + getOtherPartyMobileNo() + ", otherPartyAmount= " + getOtherPartyAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getExtractStamp() == null) ? 0 : getExtractStamp().hashCode());
result = prime * result + ((getCorporateName() == null) ? 0 : getCorporateName().hashCode());
result = prime * result + ((getServiceName() == null) ? 0 : getServiceName().hashCode());
result = prime * result + ((getServiceStatus() == null) ? 0 : getServiceStatus().hashCode());
result = prime * result + ((getServiceBankName() == null) ? 0 : getServiceBankName().hashCode());
result = prime * result + ((getServiceBankNumber() == null) ? 0 : getServiceBankNumber().hashCode());
result = prime * result + ((getServiceIBAN() == null) ? 0 : getServiceIBAN().hashCode());
result = prime * result + ((getServiceMPAYAccount() == null) ? 0 : getServiceMPAYAccount().hashCode());
result = prime * result + ((getTrnDate() == null) ? 0 : getTrnDate().hashCode());
result = prime * result + ((getTrnTime() == null) ? 0 : getTrnTime().hashCode());
result = prime * result + ((getTrnType() == null) ? 0 : getTrnType().hashCode());
result = prime * result + ((getTrnSide() == null) ? 0 : getTrnSide().hashCode());
result = prime * result + ((getTrnAmount() == null) ? 0 : getTrnAmount().hashCode());
result = prime * result + ((getTrnCharges() == null) ? 0 : getTrnCharges().hashCode());
result = prime * result + ((getTrnVAT() == null) ? 0 : getTrnVAT().hashCode());
result = prime * result + ((getTrnNetAmount() == null) ? 0 : getTrnNetAmount().hashCode());
result = prime * result + ((getTrnRefernceNo() == null) ? 0 : getTrnRefernceNo().hashCode());
result = prime * result + ((getTrnDescription() == null) ? 0 : getTrnDescription().hashCode());
result = prime * result + ((getServiceBalanceBeforeTrn() == null) ? 0 : getServiceBalanceBeforeTrn().hashCode());
result = prime * result + ((getServiceBalanceAfterTrn() == null) ? 0 : getServiceBalanceAfterTrn().hashCode());
result = prime * result + ((getOtherPartyMobileNo() == null) ? 0 : getOtherPartyMobileNo().hashCode());
result = prime * result + ((getOtherPartyAmount() == null) ? 0 : getOtherPartyAmount().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Service_Trans_Rep))
return false;
else {MPAY_Service_Trans_Rep other = (MPAY_Service_Trans_Rep) obj;
return this.hashCode() == other.hashCode();}
}


}