package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_CorporateDevices")
@XmlRootElement(name="CorporateDevices")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CorporateDevice extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CorporateDevice(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String DEVICE_I_D = "deviceID";
@Column(name="DEVICEID", nullable=true, length=255)
private String deviceID;
public String getDeviceID(){
return this.deviceID;
}
public void setDeviceID(String deviceID){
this.deviceID = deviceID;
}

public static final String DEVICE_NAME = "deviceName";
@Column(name="DEVICENAME", nullable=true, length=100)
private String deviceName;
public String getDeviceName(){
return this.deviceName;
}
public void setDeviceName(String deviceName){
this.deviceName = deviceName;
}

public static final String IS_STOLEN = "isStolen";
@Column(name="ISSTOLEN", nullable=true, length=1)
private Boolean isStolen;
public Boolean getIsStolen(){
return this.isStolen;
}
public void setIsStolen(Boolean isStolen){
this.isStolen = isStolen;
}

public static final String PASSWORD = "password";
@Column(name="PASSWORD", nullable=true, length=255)
private String password;
public String getPassword(){
return this.password;
}
public void setPassword(String password){
this.password = password;
}

public static final String ACTIVE_DEVICE = "activeDevice";
@Column(name="ACTIVEDEVICE", nullable=true, length=1)
private Boolean activeDevice;
public Boolean getActiveDevice(){
return this.activeDevice;
}
public void setActiveDevice(Boolean activeDevice){
this.activeDevice = activeDevice;
}

public static final String SESSION_ID = "sessionId";
@Column(name="SESSIONID", nullable=true, length=1000)
private String sessionId;
public String getSessionId(){
return this.sessionId;
}
public void setSessionId(String sessionId){
this.sessionId = sessionId;
}

public static final String EXTRA_DATA = "extraData";
@Column(name="EXTRADATA", nullable=true, length=1000)
private String extraData;
public String getExtraData(){
return this.extraData;
}
public void setExtraData(String extraData){
this.extraData = extraData;
}

public static final String LAST_LOGIN_TIME = "lastLoginTime";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="LASTLOGINTIME", nullable=true, length=20)
private java.sql.Timestamp lastLoginTime;
public java.sql.Timestamp getLastLoginTime(){
return this.lastLoginTime;
}
public void setLastLoginTime(java.sql.Timestamp lastLoginTime){
this.lastLoginTime = lastLoginTime;
}

public static final String _IS_ONLINE = "IsOnline";
@Column(name="ISONLINE", nullable=false, length=1)
private Boolean IsOnline;
public Boolean getIsOnline(){
return this.IsOnline;
}
public void setIsOnline(Boolean IsOnline){
this.IsOnline = IsOnline;
}

public static final String _SIGN_IN_LOCATION = "SignInLocation";
@Column(name="SIGNINLOCATION", nullable=true, length=1000)
private String SignInLocation;
public String getSignInLocation(){
return this.SignInLocation;
}
public void setSignInLocation(String SignInLocation){
this.SignInLocation = SignInLocation;
}

public static final String _RETRY_COUNT = "RetryCount";
@Column(name="RETRYCOUNT", nullable=false, length=3)
private Long RetryCount;
public Long getRetryCount(){
return this.RetryCount;
}
public void setRetryCount(Long RetryCount){
this.RetryCount = RetryCount;
}

public static final String IS_BLOCKED = "isBlocked";
@Column(name="ISBLOCKED", nullable=false, length=1)
private Boolean isBlocked;
public Boolean getIsBlocked(){
return this.isBlocked;
}
public void setIsBlocked(Boolean isBlocked){
this.isBlocked = isBlocked;
}

public static final String PASSWORD_LAST_CHANGED = "passwordLastChanged";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="PASSWORDLASTCHANGED", nullable=true, length=11)
private java.sql.Timestamp passwordLastChanged;
public java.sql.Timestamp getPasswordLastChanged(){
return this.passwordLastChanged;
}
public void setPasswordLastChanged(java.sql.Timestamp passwordLastChanged){
this.passwordLastChanged = passwordLastChanged;
}

public static final String IS_DEFAULT = "isDefault";
@Column(name="ISDEFAULT", nullable=false, length=1)
private Boolean isDefault;
public Boolean getIsDefault(){
return this.isDefault;
}
public void setIsDefault(Boolean isDefault){
this.isDefault = isDefault;
}

public static final String PUBLIC_KEY = "publicKey";
@Column(name="PUBLICKEY", nullable=true, length=8000)
@Lob
private String publicKey;
public String getPublicKey(){
return this.publicKey;
}
public void setPublicKey(String publicKey){
this.publicKey = publicKey;
}

public static final String REF_CORPORATE_SERVICE = "refCorporateService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCORPORATESERVICEID", nullable=false)
private MPAY_CorpoarteService refCorporateService;
public MPAY_CorpoarteService getRefCorporateService(){
return this.refCorporateService;
}
public void setRefCorporateService(MPAY_CorpoarteService refCorporateService){
this.refCorporateService = refCorporateService;
}

public static final String CORPORATE_DEVICE_CORPORATE_DEVICE_TOKENS = "corporateDeviceCorporateDeviceTokens";
@OneToMany(mappedBy = "corporateDevice")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CorporateDeviceToken> corporateDeviceCorporateDeviceTokens;
public List<MPAY_CorporateDeviceToken> getCorporateDeviceCorporateDeviceTokens(){
return this.corporateDeviceCorporateDeviceTokens;
}
public void setCorporateDeviceCorporateDeviceTokens(List<MPAY_CorporateDeviceToken> corporateDeviceCorporateDeviceTokens){
this.corporateDeviceCorporateDeviceTokens = corporateDeviceCorporateDeviceTokens;
}

@Override
public String toString() {
return "MPAY_CorporateDevice [id= " + getId() + ", deviceID= " + getDeviceID() + ", deviceName= " + getDeviceName() + ", isStolen= " + getIsStolen() + ", password= " + getPassword() + ", activeDevice= " + getActiveDevice() + ", sessionId= " + getSessionId() + ", extraData= " + getExtraData() + ", lastLoginTime= " + getLastLoginTime() + ", IsOnline= " + getIsOnline() + ", SignInLocation= " + getSignInLocation() + ", RetryCount= " + getRetryCount() + ", isBlocked= " + getIsBlocked() + ", passwordLastChanged= " + getPasswordLastChanged() + ", isDefault= " + getIsDefault() + ", publicKey= " + getPublicKey() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDeviceID() == null) ? 0 : getDeviceID().hashCode());
result = prime * result + ((getDeviceName() == null) ? 0 : getDeviceName().hashCode());
result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
result = prime * result + ((getSessionId() == null) ? 0 : getSessionId().hashCode());
result = prime * result + ((getExtraData() == null) ? 0 : getExtraData().hashCode());
result = prime * result + ((getSignInLocation() == null) ? 0 : getSignInLocation().hashCode());
int RetryCount= new Long("null".equals(getRetryCount() + "") ? 0 : getRetryCount()).intValue();
result = prime * result + (int) (RetryCount ^ RetryCount >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CorporateDevice))
return false;
else {MPAY_CorporateDevice other = (MPAY_CorporateDevice) obj;
return this.hashCode() == other.hashCode();}
}


}