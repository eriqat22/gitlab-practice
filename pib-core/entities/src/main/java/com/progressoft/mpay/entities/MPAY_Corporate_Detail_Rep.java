package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_Corporate_Detail_Rep")
@XmlRootElement(name="Corporate_Detail_Rep")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Corporate_Detail_Rep extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Corporate_Detail_Rep(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String EXTRAT_STAMP = "extratStamp";
@Column(name="EXTRATSTAMP", nullable=true, length=250)
private String extratStamp;
public String getExtratStamp(){
return this.extratStamp;
}
public void setExtratStamp(String extratStamp){
this.extratStamp = extratStamp;
}

public static final String SHORT_NAME = "shortName";
@Column(name="SHORTNAME", nullable=true, length=250)
private String shortName;
public String getShortName(){
return this.shortName;
}
public void setShortName(String shortName){
this.shortName = shortName;
}

public static final String FULL_NAME = "fullName";
@Column(name="FULLNAME", nullable=true, length=250)
private String fullName;
public String getFullName(){
return this.fullName;
}
public void setFullName(String fullName){
this.fullName = fullName;
}

public static final String REGISTRATION_DATE = "registrationDate";
@Column(name="REGISTRATIONDATE", nullable=true, length=250)
private String registrationDate;
public String getRegistrationDate(){
return this.registrationDate;
}
public void setRegistrationDate(String registrationDate){
this.registrationDate = registrationDate;
}

public static final String ID_TYPE = "idType";
@Column(name="IDTYPE", nullable=true, length=250)
private String idType;
public String getIdType(){
return this.idType;
}
public void setIdType(String idType){
this.idType = idType;
}

public static final String REGISTRATION_I_D = "registrationID";
@Column(name="REGISTRATIONID", nullable=true, length=250)
private String registrationID;
public String getRegistrationID(){
return this.registrationID;
}
public void setRegistrationID(String registrationID){
this.registrationID = registrationID;
}

public static final String STATUS = "status";
@Column(name="STATUS", nullable=true, length=250)
private String status;
public String getStatus(){
return this.status;
}
public void setStatus(String status){
this.status = status;
}

public static final String REGISTERED = "registered";
@Column(name="REGISTERED", nullable=true, length=250)
private String registered;
public String getRegistered(){
return this.registered;
}
public void setRegistered(String registered){
this.registered = registered;
}

public static final String ACTIVE = "active";
@Column(name="ACTIVE", nullable=true, length=250)
private String active;
public String getActive(){
return this.active;
}
public void setActive(String active){
this.active = active;
}

public static final String NOTIFICATIONS_LANGUAGE = "notificationsLanguage";
@Column(name="NOTIFICATIONSLANGUAGE", nullable=true, length=250)
private String notificationsLanguage;
public String getNotificationsLanguage(){
return this.notificationsLanguage;
}
public void setNotificationsLanguage(String notificationsLanguage){
this.notificationsLanguage = notificationsLanguage;
}

public static final String PO_BOX = "poBox";
@Column(name="POBOX", nullable=true, length=250)
private String poBox;
public String getPoBox(){
return this.poBox;
}
public void setPoBox(String poBox){
this.poBox = poBox;
}

public static final String ZIP_CODE = "zipCode";
@Column(name="ZIPCODE", nullable=true, length=250)
private String zipCode;
public String getZipCode(){
return this.zipCode;
}
public void setZipCode(String zipCode){
this.zipCode = zipCode;
}

public static final String STREET = "street";
@Column(name="STREET", nullable=true, length=250)
private String street;
public String getStreet(){
return this.street;
}
public void setStreet(String street){
this.street = street;
}

public static final String CITY = "city";
@Column(name="CITY", nullable=true, length=250)
private String city;
public String getCity(){
return this.city;
}
public void setCity(String city){
this.city = city;
}

public static final String COUNTRY = "country";
@Column(name="COUNTRY", nullable=true, length=250)
private String country;
public String getCountry(){
return this.country;
}
public void setCountry(String country){
this.country = country;
}

public static final String PHONE1 = "phone1";
@Column(name="PHONE1", nullable=true, length=250)
private String phone1;
public String getPhone1(){
return this.phone1;
}
public void setPhone1(String phone1){
this.phone1 = phone1;
}

public static final String MOBILE_NO = "mobileNo";
@Column(name="MOBILENO", nullable=true, length=250)
private String mobileNo;
public String getMobileNo(){
return this.mobileNo;
}
public void setMobileNo(String mobileNo){
this.mobileNo = mobileNo;
}

public static final String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
@Column(name="BANKACCOUNTNUMBER", nullable=true, length=250)
private String bankAccountNumber;
public String getBankAccountNumber(){
return this.bankAccountNumber;
}
public void setBankAccountNumber(String bankAccountNumber){
this.bankAccountNumber = bankAccountNumber;
}

public static final String IBAN = "iban";
@Column(name="IBAN", nullable=true, length=250)
private String iban;
public String getIban(){
return this.iban;
}
public void setIban(String iban){
this.iban = iban;
}

public static final String PAYMENT_TYPE = "paymentType";
@Column(name="PAYMENTTYPE", nullable=true, length=250)
private String paymentType;
public String getPaymentType(){
return this.paymentType;
}
public void setPaymentType(String paymentType){
this.paymentType = paymentType;
}

public static final String NO_OF_SERVICES = "noOfServices";
@Column(name="NOOFSERVICES", nullable=true, length=250)
private String noOfServices;
public String getNoOfServices(){
return this.noOfServices;
}
public void setNoOfServices(String noOfServices){
this.noOfServices = noOfServices;
}

public static final String ADDED_BY = "addedBy";
@Column(name="ADDEDBY", nullable=true, length=250)
private String addedBy;
public String getAddedBy(){
return this.addedBy;
}
public void setAddedBy(String addedBy){
this.addedBy = addedBy;
}

public static final String APPROVAL_NOTE = "approvalNote";
@Column(name="APPROVALNOTE", nullable=true, length=250)
private String approvalNote;
public String getApprovalNote(){
return this.approvalNote;
}
public void setApprovalNote(String approvalNote){
this.approvalNote = approvalNote;
}

public static final String REJECTION_NOTE = "rejectionNote";
@Column(name="REJECTIONNOTE", nullable=true, length=250)
private String rejectionNote;
public String getRejectionNote(){
return this.rejectionNote;
}
public void setRejectionNote(String rejectionNote){
this.rejectionNote = rejectionNote;
}

public static final String CREATED_DATE = "createdDate";
@Column(name="CREATEDDATE", nullable=true, length=250)
private String createdDate;
public String getCreatedDate(){
return this.createdDate;
}
public void setCreatedDate(String createdDate){
this.createdDate = createdDate;
}

public static final String CREATED_USER = "createdUser";
@Column(name="CREATEDUSER", nullable=true, length=250)
private String createdUser;
public String getCreatedUser(){
return this.createdUser;
}
public void setCreatedUser(String createdUser){
this.createdUser = createdUser;
}

public static final String MODIFIED_DATE = "modifiedDate";
@Column(name="MODIFIEDDATE", nullable=true, length=250)
private String modifiedDate;
public String getModifiedDate(){
return this.modifiedDate;
}
public void setModifiedDate(String modifiedDate){
this.modifiedDate = modifiedDate;
}

public static final String MODIFIED_USER = "modifiedUser";
@Column(name="MODIFIEDUSER", nullable=true, length=250)
private String modifiedUser;
public String getModifiedUser(){
return this.modifiedUser;
}
public void setModifiedUser(String modifiedUser){
this.modifiedUser = modifiedUser;
}

public static final String DELETED_DATE = "deletedDate";
@Column(name="DELETEDDATE", nullable=true, length=250)
private String deletedDate;
public String getDeletedDate(){
return this.deletedDate;
}
public void setDeletedDate(String deletedDate){
this.deletedDate = deletedDate;
}

public static final String DELETED_USER = "deletedUser";
@Column(name="DELETEDUSER", nullable=true, length=250)
private String deletedUser;
public String getDeletedUser(){
return this.deletedUser;
}
public void setDeletedUser(String deletedUser){
this.deletedUser = deletedUser;
}

public static final String _DELETED_B_Y = "DeletedBY";
@Column(name="DELETEDBY", nullable=true, length=250)
private String DeletedBY;
public String getDeletedBY(){
return this.DeletedBY;
}
public void setDeletedBY(String DeletedBY){
this.DeletedBY = DeletedBY;
}

@Override
public String toString() {
return "MPAY_Corporate_Detail_Rep [id= " + getId() + ", extratStamp= " + getExtratStamp() + ", shortName= " + getShortName() + ", fullName= " + getFullName() + ", registrationDate= " + getRegistrationDate() + ", idType= " + getIdType() + ", registrationID= " + getRegistrationID() + ", status= " + getStatus() + ", registered= " + getRegistered() + ", active= " + getActive() + ", notificationsLanguage= " + getNotificationsLanguage() + ", poBox= " + getPoBox() + ", zipCode= " + getZipCode() + ", street= " + getStreet() + ", city= " + getCity() + ", country= " + getCountry() + ", phone1= " + getPhone1() + ", mobileNo= " + getMobileNo() + ", bankAccountNumber= " + getBankAccountNumber() + ", iban= " + getIban() + ", paymentType= " + getPaymentType() + ", noOfServices= " + getNoOfServices() + ", addedBy= " + getAddedBy() + ", approvalNote= " + getApprovalNote() + ", rejectionNote= " + getRejectionNote() + ", createdDate= " + getCreatedDate() + ", createdUser= " + getCreatedUser() + ", modifiedDate= " + getModifiedDate() + ", modifiedUser= " + getModifiedUser() + ", deletedDate= " + getDeletedDate() + ", deletedUser= " + getDeletedUser() + ", DeletedBY= " + getDeletedBY() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getExtratStamp() == null) ? 0 : getExtratStamp().hashCode());
result = prime * result + ((getShortName() == null) ? 0 : getShortName().hashCode());
result = prime * result + ((getFullName() == null) ? 0 : getFullName().hashCode());
result = prime * result + ((getRegistrationDate() == null) ? 0 : getRegistrationDate().hashCode());
result = prime * result + ((getIdType() == null) ? 0 : getIdType().hashCode());
result = prime * result + ((getRegistrationID() == null) ? 0 : getRegistrationID().hashCode());
result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
result = prime * result + ((getRegistered() == null) ? 0 : getRegistered().hashCode());
result = prime * result + ((getActive() == null) ? 0 : getActive().hashCode());
result = prime * result + ((getNotificationsLanguage() == null) ? 0 : getNotificationsLanguage().hashCode());
result = prime * result + ((getPoBox() == null) ? 0 : getPoBox().hashCode());
result = prime * result + ((getZipCode() == null) ? 0 : getZipCode().hashCode());
result = prime * result + ((getStreet() == null) ? 0 : getStreet().hashCode());
result = prime * result + ((getCity() == null) ? 0 : getCity().hashCode());
result = prime * result + ((getCountry() == null) ? 0 : getCountry().hashCode());
result = prime * result + ((getPhone1() == null) ? 0 : getPhone1().hashCode());
result = prime * result + ((getMobileNo() == null) ? 0 : getMobileNo().hashCode());
result = prime * result + ((getBankAccountNumber() == null) ? 0 : getBankAccountNumber().hashCode());
result = prime * result + ((getIban() == null) ? 0 : getIban().hashCode());
result = prime * result + ((getPaymentType() == null) ? 0 : getPaymentType().hashCode());
result = prime * result + ((getNoOfServices() == null) ? 0 : getNoOfServices().hashCode());
result = prime * result + ((getAddedBy() == null) ? 0 : getAddedBy().hashCode());
result = prime * result + ((getApprovalNote() == null) ? 0 : getApprovalNote().hashCode());
result = prime * result + ((getRejectionNote() == null) ? 0 : getRejectionNote().hashCode());
result = prime * result + ((getCreatedDate() == null) ? 0 : getCreatedDate().hashCode());
result = prime * result + ((getCreatedUser() == null) ? 0 : getCreatedUser().hashCode());
result = prime * result + ((getModifiedDate() == null) ? 0 : getModifiedDate().hashCode());
result = prime * result + ((getModifiedUser() == null) ? 0 : getModifiedUser().hashCode());
result = prime * result + ((getDeletedDate() == null) ? 0 : getDeletedDate().hashCode());
result = prime * result + ((getDeletedUser() == null) ? 0 : getDeletedUser().hashCode());
result = prime * result + ((getDeletedBY() == null) ? 0 : getDeletedBY().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Corporate_Detail_Rep))
return false;
else {MPAY_Corporate_Detail_Rep other = (MPAY_Corporate_Detail_Rep) obj;
return this.hashCode() == other.hashCode();}
}


}