package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_BulkRegistration")
@XmlRootElement(name="BulkRegistration")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_BulkRegistration extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_BulkRegistration(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String FILE_NAME = "fileName";
@Column(name="FILENAME", nullable=false, length=255)
private String fileName;
public String getFileName(){
return this.fileName;
}
public void setFileName(String fileName){
this.fileName = fileName;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String BULK_REGISTRATION_CUSTOMERS = "bulkRegistrationCustomers";
@OneToMany(mappedBy = "bulkRegistration")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Customer> bulkRegistrationCustomers;
public List<MPAY_Customer> getBulkRegistrationCustomers(){
return this.bulkRegistrationCustomers;
}
public void setBulkRegistrationCustomers(List<MPAY_Customer> bulkRegistrationCustomers){
this.bulkRegistrationCustomers = bulkRegistrationCustomers;
}

@Override
public String toString() {
return "MPAY_BulkRegistration [id= " + getId() + ", fileName= " + getFileName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getFileName() == null) ? 0 : getFileName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_BulkRegistration))
return false;
else {MPAY_BulkRegistration other = (MPAY_BulkRegistration) obj;
return this.hashCode() == other.hashCode();}
}


}