package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.List;

@Entity

@Table(name="MPAY_Corporates")
@XmlRootElement(name="Corporates")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Corporate extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Corporate(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String NAME = "name";
@Column(name="NAME", nullable=false, length=128)
private String name;
public String getName(){
return this.name;
}
public void setName(String name){
this.name = name;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=false, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String EXPECTED_ACTIVITIES = "expectedActivities";
@Column(name="EXPECTEDACTIVITIES", nullable=true, length=200)
private String expectedActivities;
public String getExpectedActivities(){
return this.expectedActivities;
}
public void setExpectedActivities(String expectedActivities){
this.expectedActivities = expectedActivities;
}

public static final String SECTOR_TYPE = "sectorType";
@Column(name="SECTORTYPE", nullable=true, length=50)
private String sectorType;
public String getSectorType(){
return this.sectorType;
}
public void setSectorType(String sectorType){
this.sectorType = sectorType;
}

public static final String CLIENT_REF = "clientRef";
@Column(name="CLIENTREF", nullable=true, length=50)
private String clientRef;
public String getClientRef(){
return this.clientRef;
}
public void setClientRef(String clientRef){
this.clientRef = clientRef;
}

public static final String REGISTRATION_AUTHORITY = "registrationAuthority";
@Column(name="REGISTRATIONAUTHORITY", nullable=true, length=100)
private String registrationAuthority;
public String getRegistrationAuthority(){
return this.registrationAuthority;
}
public void setRegistrationAuthority(String registrationAuthority){
this.registrationAuthority = registrationAuthority;
}

public static final String REGISTRATION_LOCATION = "registrationLocation";
@Column(name="REGISTRATIONLOCATION", nullable=true, length=100)
private String registrationLocation;
public String getRegistrationLocation(){
return this.registrationLocation;
}
public void setRegistrationLocation(String registrationLocation){
this.registrationLocation = registrationLocation;
}

public static final String REGISTRATION_DATE = "registrationDate";
@Column(name="REGISTRATIONDATE", nullable=false, length=32)
@Temporal(TemporalType.DATE)
private java.util.Date registrationDate;
public java.util.Date getRegistrationDate(){
return this.registrationDate;
}
public void setRegistrationDate(java.util.Date registrationDate){
this.registrationDate = registrationDate;
}

public static final String REGISTRATION_ID = "registrationId";
@Column(name="REGISTRATIONID", nullable=false, length=50)
private String registrationId;
public String getRegistrationId(){
return this.registrationId;
}
public void setRegistrationId(String registrationId){
this.registrationId = registrationId;
}

public static final String CHAMBER_ID = "chamberId";
@Column(name="CHAMBERID", nullable=true, length=50)
private String chamberId;
public String getChamberId(){
return this.chamberId;
}
public void setChamberId(String chamberId){
this.chamberId = chamberId;
}

public static final String IS_REGISTERED = "isRegistered";
@Column(name="ISREGISTERED", nullable=true, length=1)
private Boolean isRegistered;
public Boolean getIsRegistered(){
return this.isRegistered;
}
public void setIsRegistered(Boolean isRegistered){
this.isRegistered = isRegistered;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=5)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String OTHER_LEGAL_ENTITY = "otherLegalEntity";
@Column(name="OTHERLEGALENTITY", nullable=true, length=100)
private String otherLegalEntity;
public String getOtherLegalEntity(){
return this.otherLegalEntity;
}
public void setOtherLegalEntity(String otherLegalEntity){
this.otherLegalEntity = otherLegalEntity;
}

public static final String PHONE_ONE = "phoneOne";
@Column(name="PHONEONE", nullable=true, length=20)
private String phoneOne;
public String getPhoneOne(){
return this.phoneOne;
}
public void setPhoneOne(String phoneOne){
this.phoneOne = phoneOne;
}

public static final String PHONE_TWO = "phoneTwo";
@Column(name="PHONETWO", nullable=true, length=20)
private String phoneTwo;
public String getPhoneTwo(){
return this.phoneTwo;
}
public void setPhoneTwo(String phoneTwo){
this.phoneTwo = phoneTwo;
}

public static final String EMAIL = "email";
@Column(name="EMAIL", nullable=true, length=128)
private String email;
public String getEmail(){
return this.email;
}
public void setEmail(String email){
this.email = email;
}

public static final String MOBILE_NUMBER = "mobileNumber";
@Column(name="MOBILENUMBER", nullable=true, length=50)
private String mobileNumber;
public String getMobileNumber(){
return this.mobileNumber;
}
public void setMobileNumber(String mobileNumber){
this.mobileNumber = mobileNumber;
}

public static final String POBOX = "pobox";
@Column(name="POBOX", nullable=true, length=20)
private String pobox;
public String getPobox(){
return this.pobox;
}
public void setPobox(String pobox){
this.pobox = pobox;
}

public static final String ZIP_CODE = "zipCode";
@Column(name="ZIPCODE", nullable=true, length=20)
private String zipCode;
public String getZipCode(){
return this.zipCode;
}
public void setZipCode(String zipCode){
this.zipCode = zipCode;
}

public static final String BUILDING_NUM = "buildingNum";
@Column(name="BUILDINGNUM", nullable=true, length=20)
private String buildingNum;
public String getBuildingNum(){
return this.buildingNum;
}
public void setBuildingNum(String buildingNum){
this.buildingNum = buildingNum;
}

public static final String STREET_NAME = "streetName";
@Column(name="STREETNAME", nullable=true, length=255)
private String streetName;
public String getStreetName(){
return this.streetName;
}
public void setStreetName(String streetName){
this.streetName = streetName;
}

public static final String LOCATION = "location";
@Column(name="LOCATION", nullable=true, length=100)
private String location;
public String getLocation(){
return this.location;
}
public void setLocation(String location){
this.location = location;
}

public static final String NOTE = "note";
@Column(name="NOTE", nullable=true, length=255)
private String note;
public String getNote(){
return this.note;
}
public void setNote(String note){
this.note = note;
}

public static final String REJECTION_NOTE = "rejectionNote";
@Column(name="REJECTIONNOTE", nullable=true, length=255)
private String rejectionNote;
public String getRejectionNote(){
return this.rejectionNote;
}
public void setRejectionNote(String rejectionNote){
this.rejectionNote = rejectionNote;
}

public static final String APPROVAL_NOTE = "approvalNote";
@Column(name="APPROVALNOTE", nullable=true, length=255)
private String approvalNote;
public String getApprovalNote(){
return this.approvalNote;
}
public void setApprovalNote(String approvalNote){
this.approvalNote = approvalNote;
}

public static final String CONSIGNEE_ARABIC_NAME = "consigneeArabicName";
@Column(name="CONSIGNEEARABICNAME", nullable=true, length=255)
private String consigneeArabicName;
public String getConsigneeArabicName(){
return this.consigneeArabicName;
}
public void setConsigneeArabicName(String consigneeArabicName){
this.consigneeArabicName = consigneeArabicName;
}

public static final String CONSIGNEE_ENGLISH_NAME = "consigneeEnglishName";
@Column(name="CONSIGNEEENGLISHNAME", nullable=true, length=255)
private String consigneeEnglishName;
public String getConsigneeEnglishName(){
return this.consigneeEnglishName;
}
public void setConsigneeEnglishName(String consigneeEnglishName){
this.consigneeEnglishName = consigneeEnglishName;
}

public static final String GENDER = "gender";
@Column(name="GENDER", nullable=true, length=50)
private String gender;
public String getGender(){
return this.gender;
}
public void setGender(String gender){
this.gender = gender;
}

public static final String OTHER_NATIONALITY = "otherNationality";
@Column(name="OTHERNATIONALITY", nullable=true, length=100)
private String otherNationality;
public String getOtherNationality(){
return this.otherNationality;
}
public void setOtherNationality(String otherNationality){
this.otherNationality = otherNationality;
}

public static final String PLACE_OF_BIRTH = "placeOfBirth";
@Column(name="PLACEOFBIRTH", nullable=true, length=100)
private String placeOfBirth;
public String getPlaceOfBirth(){
return this.placeOfBirth;
}
public void setPlaceOfBirth(String placeOfBirth){
this.placeOfBirth = placeOfBirth;
}

public static final String DATE_OF_BIRTH = "dateOfBirth";
@Column(name="DATEOFBIRTH", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date dateOfBirth;
public java.util.Date getDateOfBirth(){
return this.dateOfBirth;
}
public void setDateOfBirth(java.util.Date dateOfBirth){
this.dateOfBirth = dateOfBirth;
}

public static final String COSIGNEE_E_MAIL = "cosigneeEMail";
@Column(name="COSIGNEEEMAIL", nullable=true, length=128)
private String cosigneeEMail;
public String getCosigneeEMail(){
return this.cosigneeEMail;
}
public void setCosigneeEMail(String cosigneeEMail){
this.cosigneeEMail = cosigneeEMail;
}

public static final String NATIONAL_I_D = "nationalID";
@Column(name="NATIONALID", nullable=true, length=50)
private String nationalID;
public String getNationalID(){
return this.nationalID;
}
public void setNationalID(String nationalID){
this.nationalID = nationalID;
}

public static final String IDENTIFICATION_REFERENCE = "identificationReference";
@Column(name="IDENTIFICATIONREFERENCE", nullable=true, length=50)
private String identificationReference;
public String getIdentificationReference(){
return this.identificationReference;
}
public void setIdentificationReference(String identificationReference){
this.identificationReference = identificationReference;
}

public static final String PASSPORT_I_D = "passportID";
@Column(name="PASSPORTID", nullable=true, length=50)
private String passportID;
public String getPassportID(){
return this.passportID;
}
public void setPassportID(String passportID){
this.passportID = passportID;
}

public static final String PASSPORT_ISSUANCE_DATE = "passportIssuanceDate";
@Column(name="PASSPORTISSUANCEDATE", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date passportIssuanceDate;
public java.util.Date getPassportIssuanceDate(){
return this.passportIssuanceDate;
}
public void setPassportIssuanceDate(java.util.Date passportIssuanceDate){
this.passportIssuanceDate = passportIssuanceDate;
}

public static final String IDENTIFICATION_CARD = "identificationCard";
@Column(name="IDENTIFICATIONCARD", nullable=true, length=50)
private String identificationCard;
public String getIdentificationCard(){
return this.identificationCard;
}
public void setIdentificationCard(String identificationCard){
this.identificationCard = identificationCard;
}

public static final String IDENTIFICATION_CARD_ISSUANCE_DATE = "identificationCardIssuanceDate";
@Column(name="IDENTIFICATIONCARDISSUANCEDATE", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date identificationCardIssuanceDate;
public java.util.Date getIdentificationCardIssuanceDate(){
return this.identificationCardIssuanceDate;
}
public void setIdentificationCardIssuanceDate(java.util.Date identificationCardIssuanceDate){
this.identificationCardIssuanceDate = identificationCardIssuanceDate;
}

public static final String ADDRESS = "address";
@Column(name="ADDRESS", nullable=true, length=500)
private String address;
public String getAddress(){
return this.address;
}
public void setAddress(String address){
this.address = address;
}

public static final String SERVICE_NAME = "serviceName";
@Column(name="SERVICENAME", nullable=true, length=128)
private String serviceName;
public String getServiceName(){
return this.serviceName;
}
public void setServiceName(String serviceName){
this.serviceName = serviceName;
}

public static final String SERVICE_DESCRIPTION = "serviceDescription";
@Column(name="SERVICEDESCRIPTION", nullable=true, length=128)
private String serviceDescription;
public String getServiceDescription(){
return this.serviceDescription;
}
public void setServiceDescription(String serviceDescription){
this.serviceDescription = serviceDescription;
}

public static final String ALIAS = "alias";
@Column(name="ALIAS", nullable=true, length=16)
private String alias;
public String getAlias(){
return this.alias;
}
public void setAlias(String alias){
this.alias = alias;
}

public static final String BANKED_UNBANKED = "bankedUnbanked";
@Column(name="BANKEDUNBANKED", nullable=true, length=20)
private String bankedUnbanked;
public String getBankedUnbanked(){
return this.bankedUnbanked;
}
public void setBankedUnbanked(String bankedUnbanked){
this.bankedUnbanked = bankedUnbanked;
}

public static final String BRANCH = "branch";
@Column(name="BRANCH", nullable=true, length=50)
private String branch;
public String getBranch(){
return this.branch;
}
public void setBranch(String branch){
this.branch = branch;
}

public static final String EXTERNAL_ACC = "externalAcc";
@Column(name="EXTERNALACC", nullable=true, length=100)
private String externalAcc;
public String getExternalAcc(){
return this.externalAcc;
}
public void setExternalAcc(String externalAcc){
this.externalAcc = externalAcc;
}

public static final String IBAN = "iban";
@Column(name="IBAN", nullable=true, length=100)
private String iban;
public String getIban(){
return this.iban;
}
public void setIban(String iban){
this.iban = iban;
}

public static final String MAS = "mas";
@Column(name="MAS", nullable=true, length=10)
private String mas;
public String getMas(){
return this.mas;
}
public void setMas(String mas){
this.mas = mas;
}

public static final String MAX_NUMBER_OF_DEVICES = "maxNumberOfDevices";
@Column(name="MAXNUMBEROFDEVICES", nullable=false, length=10)
private Long maxNumberOfDevices;
public Long getMaxNumberOfDevices(){
return this.maxNumberOfDevices;
}
public void setMaxNumberOfDevices(Long maxNumberOfDevices){
this.maxNumberOfDevices = maxNumberOfDevices;
}

public static final String ENABLE_S_M_S = "enableSMS";
@Column(name="ENABLESMS", nullable=true, length=10)
private Boolean enableSMS;
public Boolean getEnableSMS(){
return this.enableSMS;
}
public void setEnableSMS(Boolean enableSMS){
this.enableSMS = enableSMS;
}

public static final String ENABLE_EMAIL = "enableEmail";
@Column(name="ENABLEEMAIL", nullable=true, length=10)
private Boolean enableEmail;
public Boolean getEnableEmail(){
return this.enableEmail;
}
public void setEnableEmail(Boolean enableEmail){
this.enableEmail = enableEmail;
}

public static final String ENABLE_PUSH_NOTIFICATION = "enablePushNotification";
@Column(name="ENABLEPUSHNOTIFICATION", nullable=true, length=10)
private Boolean enablePushNotification;
public Boolean getEnablePushNotification(){
return this.enablePushNotification;
}
public void setEnablePushNotification(Boolean enablePushNotification){
this.enablePushNotification = enablePushNotification;
}

public static final String PSP_NATIONAL_I_D = "pspNationalID";
@Column(name="PSPNATIONALID", nullable=true, length=20)
private String pspNationalID;
public String getPspNationalID(){
return this.pspNationalID;
}
public void setPspNationalID(String pspNationalID){
this.pspNationalID = pspNationalID;
}

public static final String APPROVED_DATA = "approvedData";
@Column(name="APPROVEDDATA", nullable=true, length=4000)
private String approvedData;
public String getApprovedData(){
return this.approvedData;
}
public void setApprovedData(String approvedData){
this.approvedData = approvedData;
}

public static final String CHANGES = "changes";
@Column(name="CHANGES", nullable=true, length=1000)
@Transient
private String changes;
public String getChanges(){
return this.changes;
}
public void setChanges(String changes){
this.changes = changes;
}

public static final String HINT = "hint";
@Column(name="HINT", nullable=true, length=255)
private String hint;
public String getHint(){
return this.hint;
}
public void setHint(String hint){
this.hint = hint;
}

public static final String KYC_TEMPLATE = "kycTemplate";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="KYCTEMPLATEID", nullable=true)
private MPAY_CorporateKyc kycTemplate;
public MPAY_CorporateKyc getKycTemplate(){
return this.kycTemplate;
}
public void setKycTemplate(MPAY_CorporateKyc kycTemplate){
this.kycTemplate = kycTemplate;
}

public static final String CATEGORY = "category";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CATEGORYID", nullable=false)
private MPAY_AccountCategory category;
public MPAY_AccountCategory getCategory(){
return this.category;
}
public void setCategory(MPAY_AccountCategory category){
this.category = category;
}

public static final String CLIENT_TYPE = "clientType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CLIENTTYPEID", nullable=false)
private com.progressoft.mpay.entities.MPAY_ClientType clientType;
public com.progressoft.mpay.entities.MPAY_ClientType getClientType(){
return this.clientType;
}
public void setClientType(com.progressoft.mpay.entities.MPAY_ClientType clientType){
this.clientType = clientType;
}

public static final String ID_TYPE = "idType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="IDTYPEID", nullable=false)
private MPAY_IDType idType;
public MPAY_IDType getIdType(){
return this.idType;
}
public void setIdType(MPAY_IDType idType){
this.idType = idType;
}

public static final String PREF_LANG = "prefLang";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PREFLANGID", nullable=false)
private MPAY_Language prefLang;
public MPAY_Language getPrefLang(){
return this.prefLang;
}
public void setPrefLang(MPAY_Language prefLang){
this.prefLang = prefLang;
}

public static final String LEGAL_ENTITY = "legalEntity";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="LEGALENTITYID", nullable=true)
private MPAY_CorpLegalEntity legalEntity;
public MPAY_CorpLegalEntity getLegalEntity(){
return this.legalEntity;
}
public void setLegalEntity(MPAY_CorpLegalEntity legalEntity){
this.legalEntity = legalEntity;
}

public static final String CITY = "city";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CITYID", nullable=false)
private MPAY_City city;
public MPAY_City getCity(){
return this.city;
}
public void setCity(MPAY_City city){
this.city = city;
}

public static final String AREA = "area";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="AREAID", nullable=true)
private MPAY_CityArea area;
public MPAY_CityArea getArea(){
return this.area;
}
public void setArea(MPAY_CityArea area){
this.area = area;
}

public static final String REF_ACCOUNT = "refAccount";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFACCOUNTID", nullable=true)
private MPAY_Account refAccount;
public MPAY_Account getRefAccount(){
return this.refAccount;
}
public void setRefAccount(MPAY_Account refAccount){
this.refAccount = refAccount;
}

public static final String REF_CHARGE_ACCOUNT = "refChargeAccount";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCHARGEACCOUNTID", nullable=true)
private MPAY_Account refChargeAccount;
public MPAY_Account getRefChargeAccount(){
return this.refChargeAccount;
}
public void setRefChargeAccount(MPAY_Account refChargeAccount){
this.refChargeAccount = refChargeAccount;
}

public static final String INST_ID = "instId";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="INSTID", nullable=true)
private MPAY_Intg_Corp_Reg_Instr instId;
public MPAY_Intg_Corp_Reg_Instr getInstId(){
return this.instId;
}
public void setInstId(MPAY_Intg_Corp_Reg_Instr instId){
this.instId = instId;
}

public static final String REF_LAST_MSG_LOG = "refLastMsgLog";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFLASTMSGLOGID", nullable=true)
private MPAY_MpClearIntegMsgLog refLastMsgLog;
public MPAY_MpClearIntegMsgLog getRefLastMsgLog(){
return this.refLastMsgLog;
}
public void setRefLastMsgLog(MPAY_MpClearIntegMsgLog refLastMsgLog){
this.refLastMsgLog = refLastMsgLog;
}

public static final String BANK = "bank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BANKID", nullable=true)
private MPAY_Bank bank;
public MPAY_Bank getBank(){
return this.bank;
}
public void setBank(MPAY_Bank bank){
this.bank = bank;
}

public static final String PAYMENT_TYPE = "paymentType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PAYMENTTYPEID", nullable=true)
private MPAY_MessageType paymentType;
public MPAY_MessageType getPaymentType(){
return this.paymentType;
}
public void setPaymentType(MPAY_MessageType paymentType){
this.paymentType = paymentType;
}

public static final String REF_PROFILE = "refProfile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFPROFILEID", nullable=true)
private MPAY_Profile refProfile;
public MPAY_Profile getRefProfile(){
return this.refProfile;
}
public void setRefProfile(MPAY_Profile refProfile){
this.refProfile = refProfile;
}

public static final String SERVICE_CATEGORY = "serviceCategory";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SERVICECATEGORYID", nullable=true)
private MPAY_ServicesCategory serviceCategory;
public MPAY_ServicesCategory getServiceCategory(){
return this.serviceCategory;
}
public void setServiceCategory(MPAY_ServicesCategory serviceCategory){
this.serviceCategory = serviceCategory;
}

public static final String SERVICE_TYPE = "serviceType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SERVICETYPEID", nullable=true)
private MPAY_ServiceType serviceType;
public MPAY_ServiceType getServiceType(){
return this.serviceType;
}
public void setServiceType(MPAY_ServiceType serviceType){
this.serviceType = serviceType;
}

public static final String ALLOWED_PROFILE = "allowedProfile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="ALLOWEDPROFILEID", nullable=true)
private MPAY_CustomerProfile allowedProfile;
public MPAY_CustomerProfile getAllowedProfile(){
return this.allowedProfile;
}
public void setAllowedProfile(MPAY_CustomerProfile allowedProfile){
this.allowedProfile = allowedProfile;
}

public static final String TENANTS = "tenants";
@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.MERGE })
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinTable(name="MPAY_CorporatesTenants", joinColumns = @JoinColumn(name = "CORPORATES_ID"), inverseJoinColumns = @JoinColumn(name = "TENANTS_ID"))
private List<com.progressoft.jfw.model.bussinessobject.security.Tenant> tenants = new ArrayList<com.progressoft.jfw.model.bussinessobject.security.Tenant>();
public List<com.progressoft.jfw.model.bussinessobject.security.Tenant> getTenants(){
return this.tenants;
}
public void setTenants(List<com.progressoft.jfw.model.bussinessobject.security.Tenant> tenants){
this.tenants = tenants;
}

public static final String REF_COUNTRY = "refCountry";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCOUNTRYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JfwCountry refCountry;
public com.progressoft.jfw.model.bussinessobject.core.JfwCountry getRefCountry(){
return this.refCountry;
}
public void setRefCountry(com.progressoft.jfw.model.bussinessobject.core.JfwCountry refCountry){
this.refCountry = refCountry;
}

public static final String NATIONALITY = "nationality";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="NATIONALITYID", nullable=true)
private com.progressoft.jfw.model.bussinessobject.core.JfwCountry nationality;
public com.progressoft.jfw.model.bussinessobject.core.JfwCountry getNationality(){
return this.nationality;
}
public void setNationality(com.progressoft.jfw.model.bussinessobject.core.JfwCountry nationality){
this.nationality = nationality;
}

public static final String REF_CORPORATE_CORPOARTE_SERVICES = "refCorporateCorpoarteServices";
@OneToMany(mappedBy = "refCorporate")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CorpoarteService> refCorporateCorpoarteServices;
public List<MPAY_CorpoarteService> getRefCorporateCorpoarteServices(){
return this.refCorporateCorpoarteServices;
}
public void setRefCorporateCorpoarteServices(List<MPAY_CorpoarteService> refCorporateCorpoarteServices){
this.refCorporateCorpoarteServices = refCorporateCorpoarteServices;
}

public static final String REF_PARENT_CORPORATE_CORPOARTE_SERVICES = "refParentCorporateCorpoarteServices";
@OneToMany(mappedBy = "refParentCorporate")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CorpoarteService> refParentCorporateCorpoarteServices;
public List<MPAY_CorpoarteService> getRefParentCorporateCorpoarteServices(){
return this.refParentCorporateCorpoarteServices;
}
public void setRefParentCorporateCorpoarteServices(List<MPAY_CorpoarteService> refParentCorporateCorpoarteServices){
this.refParentCorporateCorpoarteServices = refParentCorporateCorpoarteServices;
}

public static final String REF_CORPORATE_CORP_INTEG_MESSAGES = "refCorporateCorpIntegMessages";
@OneToMany(mappedBy = "refCorporate")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CorpIntegMessage> refCorporateCorpIntegMessages;
public List<MPAY_CorpIntegMessage> getRefCorporateCorpIntegMessages(){
return this.refCorporateCorpIntegMessages;
}
public void setRefCorporateCorpIntegMessages(List<MPAY_CorpIntegMessage> refCorporateCorpIntegMessages){
this.refCorporateCorpIntegMessages = refCorporateCorpIntegMessages;
}

public static final String REF_P_S_P_PSP_DETAILS = "refPSPPspDetails";
@OneToMany(mappedBy = "refPSP")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_PspDetail> refPSPPspDetails;
public List<MPAY_PspDetail> getRefPSPPspDetails(){
return this.refPSPPspDetails;
}
public void setRefPSPPspDetails(List<MPAY_PspDetail> refPSPPspDetails){
this.refPSPPspDetails = refPSPPspDetails;
}

@Override
public String toString() {
return "MPAY_Corporate [id= " + getId() + ", name= " + getName() + ", description= " + getDescription() + ", expectedActivities= " + getExpectedActivities() + ", sectorType= " + getSectorType() + ", clientRef= " + getClientRef() + ", registrationAuthority= " + getRegistrationAuthority() + ", registrationLocation= " + getRegistrationLocation() + ", registrationDate= " + getRegistrationDate() + ", registrationId= " + getRegistrationId() + ", chamberId= " + getChamberId() + ", isRegistered= " + getIsRegistered() + ", isActive= " + getIsActive() + ", otherLegalEntity= " + getOtherLegalEntity() + ", phoneOne= " + getPhoneOne() + ", phoneTwo= " + getPhoneTwo() + ", email= " + getEmail() + ", mobileNumber= " + getMobileNumber() + ", pobox= " + getPobox() + ", zipCode= " + getZipCode() + ", buildingNum= " + getBuildingNum() + ", streetName= " + getStreetName() + ", location= " + getLocation() + ", note= " + getNote() + ", rejectionNote= " + getRejectionNote() + ", approvalNote= " + getApprovalNote() + ", consigneeArabicName= " + getConsigneeArabicName() + ", consigneeEnglishName= " + getConsigneeEnglishName() + ", gender= " + getGender() + ", otherNationality= " + getOtherNationality() + ", placeOfBirth= " + getPlaceOfBirth() + ", dateOfBirth= " + getDateOfBirth() + ", cosigneeEMail= " + getCosigneeEMail() + ", nationalID= " + getNationalID() + ", identificationReference= " + getIdentificationReference() + ", passportID= " + getPassportID() + ", passportIssuanceDate= " + getPassportIssuanceDate() + ", identificationCard= " + getIdentificationCard() + ", identificationCardIssuanceDate= " + getIdentificationCardIssuanceDate() + ", address= " + getAddress() + ", serviceName= " + getServiceName() + ", serviceDescription= " + getServiceDescription() + ", alias= " + getAlias() + ", bankedUnbanked= " + getBankedUnbanked() + ", branch= " + getBranch() + ", externalAcc= " + getExternalAcc() + ", iban= " + getIban() + ", mas= " + getMas() + ", maxNumberOfDevices= " + getMaxNumberOfDevices() + ", enableSMS= " + getEnableSMS() + ", enableEmail= " + getEnableEmail() + ", enablePushNotification= " + getEnablePushNotification() + ", pspNationalID= " + getPspNationalID() + ", approvedData= " + getApprovedData() + ", changes= " + getChanges() + ", hint= " + getHint() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getExpectedActivities() == null) ? 0 : getExpectedActivities().hashCode());
result = prime * result + ((getClientRef() == null) ? 0 : getClientRef().hashCode());
result = prime * result + ((getRegistrationAuthority() == null) ? 0 : getRegistrationAuthority().hashCode());
result = prime * result + ((getRegistrationLocation() == null) ? 0 : getRegistrationLocation().hashCode());
result = prime * result + ((getRegistrationDate() == null) ? 0 : getRegistrationDate().hashCode());
result = prime * result + ((getRegistrationId() == null) ? 0 : getRegistrationId().hashCode());
result = prime * result + ((getChamberId() == null) ? 0 : getChamberId().hashCode());
result = prime * result + ((getOtherLegalEntity() == null) ? 0 : getOtherLegalEntity().hashCode());
result = prime * result + ((getPhoneOne() == null) ? 0 : getPhoneOne().hashCode());
result = prime * result + ((getPhoneTwo() == null) ? 0 : getPhoneTwo().hashCode());
result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
result = prime * result + ((getMobileNumber() == null) ? 0 : getMobileNumber().hashCode());
result = prime * result + ((getPobox() == null) ? 0 : getPobox().hashCode());
result = prime * result + ((getZipCode() == null) ? 0 : getZipCode().hashCode());
result = prime * result + ((getBuildingNum() == null) ? 0 : getBuildingNum().hashCode());
result = prime * result + ((getStreetName() == null) ? 0 : getStreetName().hashCode());
result = prime * result + ((getConsigneeArabicName() == null) ? 0 : getConsigneeArabicName().hashCode());
result = prime * result + ((getConsigneeEnglishName() == null) ? 0 : getConsigneeEnglishName().hashCode());
result = prime * result + ((getOtherNationality() == null) ? 0 : getOtherNationality().hashCode());
result = prime * result + ((getPlaceOfBirth() == null) ? 0 : getPlaceOfBirth().hashCode());
result = prime * result + ((getDateOfBirth() == null) ? 0 : getDateOfBirth().hashCode());
result = prime * result + ((getCosigneeEMail() == null) ? 0 : getCosigneeEMail().hashCode());
result = prime * result + ((getNationalID() == null) ? 0 : getNationalID().hashCode());
result = prime * result + ((getIdentificationReference() == null) ? 0 : getIdentificationReference().hashCode());
result = prime * result + ((getPassportID() == null) ? 0 : getPassportID().hashCode());
result = prime * result + ((getPassportIssuanceDate() == null) ? 0 : getPassportIssuanceDate().hashCode());
result = prime * result + ((getIdentificationCard() == null) ? 0 : getIdentificationCard().hashCode());
result = prime * result + ((getIdentificationCardIssuanceDate() == null) ? 0 : getIdentificationCardIssuanceDate().hashCode());
result = prime * result + ((getServiceName() == null) ? 0 : getServiceName().hashCode());
result = prime * result + ((getServiceDescription() == null) ? 0 : getServiceDescription().hashCode());
result = prime * result + ((getAlias() == null) ? 0 : getAlias().hashCode());
result = prime * result + ((getBranch() == null) ? 0 : getBranch().hashCode());
result = prime * result + ((getExternalAcc() == null) ? 0 : getExternalAcc().hashCode());
result = prime * result + ((getIban() == null) ? 0 : getIban().hashCode());
result = prime * result + ((getMas() == null) ? 0 : getMas().hashCode());
int MaxNumberOfDevices= new Long("null".equals(getMaxNumberOfDevices() + "") ? 0 : getMaxNumberOfDevices()).intValue();
result = prime * result + (int) (MaxNumberOfDevices ^ MaxNumberOfDevices >>> 32);
result = prime * result + ((getPspNationalID() == null) ? 0 : getPspNationalID().hashCode());
result = prime * result + ((getChanges() == null) ? 0 : getChanges().hashCode());
result = prime * result + ((getHint() == null) ? 0 : getHint().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Corporate))
return false;
else {MPAY_Corporate other = (MPAY_Corporate) obj;
return this.hashCode() == other.hashCode();}
}


}