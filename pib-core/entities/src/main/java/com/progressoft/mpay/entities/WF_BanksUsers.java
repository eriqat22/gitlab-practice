package com.progressoft.mpay.entities;

public class WF_BanksUsers
 {
    // Steps
    public static final String STEP_Initialization = "11204601";
    public static final String STEP_ApprovalNewStep = "11204602";
    public static final String STEP_RepairStep = "11204603";
    public static final String STEP_ApprovalRepairStep = "11204604";
    public static final String STEP_ActiveStep = "11204605";
    public static final String STEP_EditStep = "11204606";
    public static final String STEP_ApprovalEditStep = "11204607";
    public static final String STEP_ApprovalDeleteStep = "11204608";
    public static final String STEP_DeletedStep = "11204609";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_ApprovalNewStep = "Approval New";
    public static final String STEP_STATUS_RepairStep = "Repair";
    public static final String STEP_STATUS_ApprovalRepairStep = "Approval Repair";
    public static final String STEP_STATUS_ActiveStep = "Active";
    public static final String STEP_STATUS_EditStep = "Edit";
    public static final String STEP_STATUS_ApprovalEditStep = "Approval Edit";
    public static final String STEP_STATUS_ApprovalDeleteStep = "Approval Delete";
    public static final String STEP_STATUS_DeletedStep = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveApprovalNewStep = "3";
    public static final String ACTION_CODE_RejectApprovalNewStep = "4";
    public static final String ACTION_CODE_SubmitRepairStep = "5";
    public static final String ACTION_CODE_DeleteRepairStep = "6";
    public static final String ACTION_CODE_ApproveApprovalRepairStep = "7";
    public static final String ACTION_CODE_RejectApprovalRepairStep = "8";
    public static final String ACTION_CODE_EditActiveStep = "9";
    public static final String ACTION_CODE_DeleteActiveStep = "12";
    public static final String ACTION_CODE_SubmitEditStep = "10";
    public static final String ACTION_CODE_CancelEditStep = "11";
    public static final String ACTION_CODE_ApproveApprovalEditStep = "13";
    public static final String ACTION_CODE_RejectApprovalEditStep = "14";
    public static final String ACTION_CODE_ApproveApprovalDeleteStep = "15";
    public static final String ACTION_CODE_RejectApprovalDeleteStep = "16";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveApprovalNewStep = "961647865";
    public static final String ACTION_KEY_RejectApprovalNewStep = "546882862";
    public static final String ACTION_KEY_SubmitRepairStep = "622956348";
    public static final String ACTION_KEY_DeleteRepairStep = "986301820";
    public static final String ACTION_KEY_ApproveApprovalRepairStep = "1109040656";
    public static final String ACTION_KEY_RejectApprovalRepairStep = "1064201711";
    public static final String ACTION_KEY_EditActiveStep = "404956102";
    public static final String ACTION_KEY_DeleteActiveStep = "706847872";
    public static final String ACTION_KEY_SubmitEditStep = "1445029547";
    public static final String ACTION_KEY_CancelEditStep = "807435156";
    public static final String ACTION_KEY_ApproveApprovalEditStep = "1672219536";
    public static final String ACTION_KEY_RejectApprovalEditStep = "596533328";
    public static final String ACTION_KEY_ApproveApprovalDeleteStep = "896004151";
    public static final String ACTION_KEY_RejectApprovalDeleteStep = "111023855";

}