package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_ClientsCommissions",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"ACCOUNTID","MESSAGETYPEID","Z_TENANT_ID"})
})
@XmlRootElement(name="ClientsCommissions")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ClientsCommission extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ClientsCommission(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String TOTAL_COUNT = "totalCount";
@Column(name="TOTALCOUNT", nullable=false, length=8)
private Long totalCount;
public Long getTotalCount(){
return this.totalCount;
}
public void setTotalCount(Long totalCount){
this.totalCount = totalCount;
}

public static final String TOTAL_AMOUNT = "totalAmount";
@Column(name="TOTALAMOUNT", nullable=false, length=19)
private java.math.BigDecimal totalAmount;
public java.math.BigDecimal getTotalAmount(){
return this.totalAmount;
}
public void setTotalAmount(java.math.BigDecimal totalAmount){
this.totalAmount = totalAmount;
}

public static final String ACCOUNT = "account";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="ACCOUNTID", nullable=false)
private MPAY_Account account;
public MPAY_Account getAccount(){
return this.account;
}
public void setAccount(MPAY_Account account){
this.account = account;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=false)
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

@Override
public String toString() {
return "MPAY_ClientsCommission [id= " + getId() + ", totalCount= " + getTotalCount() + ", totalAmount= " + getTotalAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getTotalCount() == null) ? 0 : getTotalCount().hashCode());
result = prime * result + ((getTotalAmount() == null) ? 0 : getTotalAmount().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ClientsCommission))
return false;
else {MPAY_ClientsCommission other = (MPAY_ClientsCommission) obj;
return this.hashCode() == other.hashCode();}
}


}