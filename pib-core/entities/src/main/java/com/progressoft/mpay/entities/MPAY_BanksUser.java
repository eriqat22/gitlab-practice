package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_BanksUsers")
@XmlRootElement(name="BanksUsers")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_BanksUser extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_BanksUser(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String USER_NAME = "userName";
@Column(name="USERNAME", nullable=false, length=200)
private String userName;
public String getUserName(){
return this.userName;
}
public void setUserName(String userName){
this.userName = userName;
}

public static final String PASSWORD = "password";
@Column(name="PASSWORD", nullable=false, length=1000)
private String password;
public String getPassword(){
return this.password;
}
public void setPassword(String password){
this.password = password;
}

public static final String CONFIRM = "confirm";
@Column(name="CONFIRM", nullable=true, length=1000)
private String confirm;
public String getConfirm(){
return this.confirm;
}
public void setConfirm(String confirm){
this.confirm = confirm;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String BANK = "bank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BANKID", nullable=false)
private MPAY_Bank bank;
public MPAY_Bank getBank(){
return this.bank;
}
public void setBank(MPAY_Bank bank){
this.bank = bank;
}

@Override
public String toString() {
return "MPAY_BanksUser [id= " + getId() + ", userName= " + getUserName() + ", password= " + getPassword() + ", confirm= " + getConfirm() + ", isActive= " + getIsActive() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
result = prime * result + ((getConfirm() == null) ? 0 : getConfirm().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_BanksUser))
return false;
else {MPAY_BanksUser other = (MPAY_BanksUser) obj;
return this.hashCode() == other.hashCode();}
}


}