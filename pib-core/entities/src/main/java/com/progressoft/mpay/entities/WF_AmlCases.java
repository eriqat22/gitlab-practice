package com.progressoft.mpay.entities;

public class WF_AmlCases
 {
    // Steps
    public static final String STEP_Initialization = "91294651";
    public static final String STEP_Waiting = "91294652";
    public static final String STEP_Accepted = "91294653";
    public static final String STEP_Rejected = "91294654";
    public static final String STEP_Closed = "91294655";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_Waiting = "Waiting";
    public static final String STEP_STATUS_Accepted = "Accepted";
    public static final String STEP_STATUS_Rejected = "Rejected";
    public static final String STEP_STATUS_Closed = "Closed";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_TrueMatch = "True Match";
    public static final String ACTION_NAME_FalseMatch = "False Match";
    public static final String ACTION_NAME_Resend = "Resend";
    public static final String ACTION_NAME_Close = "Close";
    public static final String ACTION_NAME_SVC_Close = "SVC_Close";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_TrueMatchWaiting = "3";
    public static final String ACTION_CODE_FalseMatchWaiting = "4";
    public static final String ACTION_CODE_ResendRejected = "6";
    public static final String ACTION_CODE_CloseRejected = "7";
    public static final String ACTION_CODE_SVC_CloseRejected = "8";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_TrueMatchWaiting = "1419456802";
    public static final String ACTION_KEY_FalseMatchWaiting = "1936137834";
    public static final String ACTION_KEY_ResendRejected = "591598447";
    public static final String ACTION_KEY_CloseRejected = "872763027";
    public static final String ACTION_KEY_SVC_CloseRejected = "74753899";

}