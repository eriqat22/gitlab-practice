package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_Intg_Corp_Reg_Instrs")
@XmlRootElement(name="Intg_Corp_Reg_Instrs")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Intg_Corp_Reg_Instr extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Intg_Corp_Reg_Instr(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REG_INSTR_CREATION_DT = "regInstrCreationDt";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REGINSTRCREATIONDT", nullable=false, length=100)
private java.sql.Timestamp regInstrCreationDt;
public java.sql.Timestamp getRegInstrCreationDt(){
return this.regInstrCreationDt;
}
public void setRegInstrCreationDt(java.sql.Timestamp regInstrCreationDt){
this.regInstrCreationDt = regInstrCreationDt;
}

public static final String REG_INSTR_RECORD_ID = "regInstrRecordId";
@Column(name="REGINSTRRECORDID", nullable=false, length=5000)
@Lob
private String regInstrRecordId;
public String getRegInstrRecordId(){
return this.regInstrRecordId;
}
public void setRegInstrRecordId(String regInstrRecordId){
this.regInstrRecordId = regInstrRecordId;
}

public static final String CORPORATE_NAME = "corporateName";
@Column(name="CORPORATENAME", nullable=true, length=5000)
@Lob
private String corporateName;
public String getCorporateName(){
return this.corporateName;
}
public void setCorporateName(String corporateName){
this.corporateName = corporateName;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=5000)
@Lob
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String CLIENT_TYPE = "clientType";
@Column(name="CLIENTTYPE", nullable=true, length=5000)
@Lob
private String clientType;
public String getClientType(){
return this.clientType;
}
public void setClientType(String clientType){
this.clientType = clientType;
}

public static final String REGISTRATION_DATE = "registrationDate";
@Column(name="REGISTRATIONDATE", nullable=true, length=5000)
@Lob
private String registrationDate;
public String getRegistrationDate(){
return this.registrationDate;
}
public void setRegistrationDate(String registrationDate){
this.registrationDate = registrationDate;
}

public static final String REGISTRATION_ID = "registrationId";
@Column(name="REGISTRATIONID", nullable=true, length=5000)
@Lob
private String registrationId;
public String getRegistrationId(){
return this.registrationId;
}
public void setRegistrationId(String registrationId){
this.registrationId = registrationId;
}

public static final String PREF_LANG = "prefLang";
@Column(name="PREFLANG", nullable=true, length=5000)
@Lob
private String prefLang;
public String getPrefLang(){
return this.prefLang;
}
public void setPrefLang(String prefLang){
this.prefLang = prefLang;
}

public static final String PHONE_ONE = "phoneOne";
@Column(name="PHONEONE", nullable=true, length=5000)
@Lob
private String phoneOne;
public String getPhoneOne(){
return this.phoneOne;
}
public void setPhoneOne(String phoneOne){
this.phoneOne = phoneOne;
}

public static final String PHONE_TWO = "phoneTwo";
@Column(name="PHONETWO", nullable=true, length=5000)
@Lob
private String phoneTwo;
public String getPhoneTwo(){
return this.phoneTwo;
}
public void setPhoneTwo(String phoneTwo){
this.phoneTwo = phoneTwo;
}

public static final String EMAIL = "email";
@Column(name="EMAIL", nullable=true, length=5000)
@Lob
private String email;
public String getEmail(){
return this.email;
}
public void setEmail(String email){
this.email = email;
}

public static final String POBOX = "pobox";
@Column(name="POBOX", nullable=true, length=5000)
@Lob
private String pobox;
public String getPobox(){
return this.pobox;
}
public void setPobox(String pobox){
this.pobox = pobox;
}

public static final String ZIP_CODE = "zipCode";
@Column(name="ZIPCODE", nullable=true, length=5000)
@Lob
private String zipCode;
public String getZipCode(){
return this.zipCode;
}
public void setZipCode(String zipCode){
this.zipCode = zipCode;
}

public static final String BUILDING_NUM = "buildingNum";
@Column(name="BUILDINGNUM", nullable=true, length=5000)
@Lob
private String buildingNum;
public String getBuildingNum(){
return this.buildingNum;
}
public void setBuildingNum(String buildingNum){
this.buildingNum = buildingNum;
}

public static final String STREET_NAME = "streetName";
@Column(name="STREETNAME", nullable=true, length=5000)
@Lob
private String streetName;
public String getStreetName(){
return this.streetName;
}
public void setStreetName(String streetName){
this.streetName = streetName;
}

public static final String NATIONALITY = "nationality";
@Column(name="NATIONALITY", nullable=true, length=5000)
@Lob
private String nationality;
public String getNationality(){
return this.nationality;
}
public void setNationality(String nationality){
this.nationality = nationality;
}

public static final String CITY = "city";
@Column(name="CITY", nullable=true, length=5000)
@Lob
private String city;
public String getCity(){
return this.city;
}
public void setCity(String city){
this.city = city;
}

public static final String CLIENT_REF = "clientRef";
@Column(name="CLIENTREF", nullable=true, length=5000)
@Lob
private String clientRef;
public String getClientRef(){
return this.clientRef;
}
public void setClientRef(String clientRef){
this.clientRef = clientRef;
}

public static final String NOTE = "note";
@Column(name="NOTE", nullable=true, length=5000)
@Lob
private String note;
public String getNote(){
return this.note;
}
public void setNote(String note){
this.note = note;
}

public static final String BANK_SHORT_NAME = "bankShortName";
@Column(name="BANKSHORTNAME", nullable=true, length=5000)
@Lob
private String bankShortName;
public String getBankShortName(){
return this.bankShortName;
}
public void setBankShortName(String bankShortName){
this.bankShortName = bankShortName;
}

public static final String SERVICE_NAME = "serviceName";
@Column(name="SERVICENAME", nullable=true, length=5000)
@Lob
private String serviceName;
public String getServiceName(){
return this.serviceName;
}
public void setServiceName(String serviceName){
this.serviceName = serviceName;
}

public static final String ALIAS = "alias";
@Column(name="ALIAS", nullable=true, length=5000)
@Lob
private String alias;
public String getAlias(){
return this.alias;
}
public void setAlias(String alias){
this.alias = alias;
}

public static final String EXTERNAL_ACC = "externalAcc";
@Column(name="EXTERNALACC", nullable=true, length=5000)
@Lob
private String externalAcc;
public String getExternalAcc(){
return this.externalAcc;
}
public void setExternalAcc(String externalAcc){
this.externalAcc = externalAcc;
}

public static final String MOBILE_ACC_SELECTOR = "mobileAccSelector";
@Column(name="MOBILEACCSELECTOR", nullable=true, length=5000)
@Lob
private String mobileAccSelector;
public String getMobileAccSelector(){
return this.mobileAccSelector;
}
public void setMobileAccSelector(String mobileAccSelector){
this.mobileAccSelector = mobileAccSelector;
}

public static final String PAYMENT_TYPE = "paymentType";
@Column(name="PAYMENTTYPE", nullable=true, length=5000)
@Lob
private String paymentType;
public String getPaymentType(){
return this.paymentType;
}
public void setPaymentType(String paymentType){
this.paymentType = paymentType;
}

public static final String REASON_DESCRIPTION = "reasonDescription";
@Column(name="REASONDESCRIPTION", nullable=true, length=5000)
@Lob
private String reasonDescription;
public String getReasonDescription(){
return this.reasonDescription;
}
public void setReasonDescription(String reasonDescription){
this.reasonDescription = reasonDescription;
}

public static final String SERVICE_DESCRIPTION = "serviceDescription";
@Column(name="SERVICEDESCRIPTION", nullable=true, length=255)
private String serviceDescription;
public String getServiceDescription(){
return this.serviceDescription;
}
public void setServiceDescription(String serviceDescription){
this.serviceDescription = serviceDescription;
}

public static final String REF_REG_FILE = "refRegFile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFREGFILEID", nullable=false)
private MPAY_Intg_Reg_File refRegFile;
public MPAY_Intg_Reg_File getRefRegFile(){
return this.refRegFile;
}
public void setRefRegFile(MPAY_Intg_Reg_File refRegFile){
this.refRegFile = refRegFile;
}

public static final String REG_INSTR_PROCESSING_STTS = "regInstrProcessingStts";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REGINSTRPROCESSINGSTTSID", nullable=false)
private MPAY_Reg_Instrs_Stt regInstrProcessingStts;
public MPAY_Reg_Instrs_Stt getRegInstrProcessingStts(){
return this.regInstrProcessingStts;
}
public void setRegInstrProcessingStts(MPAY_Reg_Instrs_Stt regInstrProcessingStts){
this.regInstrProcessingStts = regInstrProcessingStts;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=true)
@Lob
private MPAY_Reason reason;
public MPAY_Reason getReason(){
return this.reason;
}
public void setReason(MPAY_Reason reason){
this.reason = reason;
}

public static final String SERVICE_CATEGORY = "serviceCategory";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SERVICECATEGORYID", nullable=true)
private MPAY_ServicesCategory serviceCategory;
public MPAY_ServicesCategory getServiceCategory(){
return this.serviceCategory;
}
public void setServiceCategory(MPAY_ServicesCategory serviceCategory){
this.serviceCategory = serviceCategory;
}

public static final String SERVICE_TYPE = "serviceType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SERVICETYPEID", nullable=true)
private MPAY_ServiceType serviceType;
public MPAY_ServiceType getServiceType(){
return this.serviceType;
}
public void setServiceType(MPAY_ServiceType serviceType){
this.serviceType = serviceType;
}

public static final String INST_ID_CORPORATES = "instIdCorporates";
@OneToMany(mappedBy = "instId")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Corporate> instIdCorporates;
public List<MPAY_Corporate> getInstIdCorporates(){
return this.instIdCorporates;
}
public void setInstIdCorporates(List<MPAY_Corporate> instIdCorporates){
this.instIdCorporates = instIdCorporates;
}

@Override
public String toString() {
return "MPAY_Intg_Corp_Reg_Instr [id= " + getId() + ", regInstrCreationDt= " + getRegInstrCreationDt() + ", regInstrRecordId= " + getRegInstrRecordId() + ", corporateName= " + getCorporateName() + ", description= " + getDescription() + ", clientType= " + getClientType() + ", registrationDate= " + getRegistrationDate() + ", registrationId= " + getRegistrationId() + ", prefLang= " + getPrefLang() + ", phoneOne= " + getPhoneOne() + ", phoneTwo= " + getPhoneTwo() + ", email= " + getEmail() + ", pobox= " + getPobox() + ", zipCode= " + getZipCode() + ", buildingNum= " + getBuildingNum() + ", streetName= " + getStreetName() + ", nationality= " + getNationality() + ", city= " + getCity() + ", clientRef= " + getClientRef() + ", note= " + getNote() + ", bankShortName= " + getBankShortName() + ", serviceName= " + getServiceName() + ", alias= " + getAlias() + ", externalAcc= " + getExternalAcc() + ", mobileAccSelector= " + getMobileAccSelector() + ", paymentType= " + getPaymentType() + ", reasonDescription= " + getReasonDescription() + ", serviceDescription= " + getServiceDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRegInstrRecordId() == null) ? 0 : getRegInstrRecordId().hashCode());
result = prime * result + ((getCorporateName() == null) ? 0 : getCorporateName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getClientType() == null) ? 0 : getClientType().hashCode());
result = prime * result + ((getRegistrationDate() == null) ? 0 : getRegistrationDate().hashCode());
result = prime * result + ((getRegistrationId() == null) ? 0 : getRegistrationId().hashCode());
result = prime * result + ((getPrefLang() == null) ? 0 : getPrefLang().hashCode());
result = prime * result + ((getPhoneOne() == null) ? 0 : getPhoneOne().hashCode());
result = prime * result + ((getPhoneTwo() == null) ? 0 : getPhoneTwo().hashCode());
result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
result = prime * result + ((getPobox() == null) ? 0 : getPobox().hashCode());
result = prime * result + ((getZipCode() == null) ? 0 : getZipCode().hashCode());
result = prime * result + ((getBuildingNum() == null) ? 0 : getBuildingNum().hashCode());
result = prime * result + ((getStreetName() == null) ? 0 : getStreetName().hashCode());
result = prime * result + ((getNationality() == null) ? 0 : getNationality().hashCode());
result = prime * result + ((getCity() == null) ? 0 : getCity().hashCode());
result = prime * result + ((getClientRef() == null) ? 0 : getClientRef().hashCode());
result = prime * result + ((getBankShortName() == null) ? 0 : getBankShortName().hashCode());
result = prime * result + ((getServiceName() == null) ? 0 : getServiceName().hashCode());
result = prime * result + ((getAlias() == null) ? 0 : getAlias().hashCode());
result = prime * result + ((getExternalAcc() == null) ? 0 : getExternalAcc().hashCode());
result = prime * result + ((getMobileAccSelector() == null) ? 0 : getMobileAccSelector().hashCode());
result = prime * result + ((getPaymentType() == null) ? 0 : getPaymentType().hashCode());
result = prime * result + ((getServiceDescription() == null) ? 0 : getServiceDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Intg_Corp_Reg_Instr))
return false;
else {MPAY_Intg_Corp_Reg_Instr other = (MPAY_Intg_Corp_Reg_Instr) obj;
return this.hashCode() == other.hashCode();}
}


}