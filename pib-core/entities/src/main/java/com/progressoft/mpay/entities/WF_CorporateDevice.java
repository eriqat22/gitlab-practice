package com.progressoft.mpay.entities;

public class WF_CorporateDevice
 {
    // Steps
    public static final String STEP_Initialization = "1100101";
    public static final String STEP_CreationApproval = "1100102";
    public static final String STEP_RepairNew = "1100103";
    public static final String STEP_EditNew = "1100104";
    public static final String STEP_ViewApproved = "1100105";
    public static final String STEP_ModificationApproval = "1100107";
    public static final String STEP_RepairRejected = "1100108";
    public static final String STEP_EditRejected = "1100109";
    public static final String STEP_DeletionApproval = "1100110";
    public static final String STEP_End = "1100111";
    public static final String STEP_Stolen = "1100112";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_CreationApproval = "New Authorization";
    public static final String STEP_STATUS_RepairNew = "Repair New";
    public static final String STEP_STATUS_EditNew = "Edit New";
    public static final String STEP_STATUS_ViewApproved = "Approved";
    public static final String STEP_STATUS_ModificationApproval = "Authorization";
    public static final String STEP_STATUS_RepairRejected = "Repair Rejected";
    public static final String STEP_STATUS_EditRejected = "Edit Rejected";
    public static final String STEP_STATUS_DeletionApproval = "Deletion Authorization";
    public static final String STEP_STATUS_End = "Deleted";
    public static final String STEP_STATUS_Stolen = "Stolen";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_SVC_Approve = "SVC_Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Cancel = "Cancel";
    public static final String ACTION_NAME_SVC_Delete = "SVC_Delete";
    public static final String ACTION_NAME_Unblock = "Unblock";
    public static final String ACTION_NAME_Stolen = "Stolen";
    public static final String ACTION_NAME_Reset = "Reset";
    public static final String ACTION_NAME_Reactivate = "Reactivate";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveCreationApproval = "3";
    public static final String ACTION_CODE_SVC_ApproveCreationApproval = "4";
    public static final String ACTION_CODE_RejectCreationApproval = "5";
    public static final String ACTION_CODE_EditRepairNew = "6";
    public static final String ACTION_CODE_DeleteRepairNew = "7";
    public static final String ACTION_CODE_SaveEditNew = "8";
    public static final String ACTION_CODE_CancelEditNew = "9";
    public static final String ACTION_CODE_DeleteViewApproved = "11";
    public static final String ACTION_CODE_SVC_DeleteViewApproved = "12";
    public static final String ACTION_CODE_UnblockViewApproved = "23";
    public static final String ACTION_CODE_StolenViewApproved = "24";
    public static final String ACTION_CODE_ApproveModificationApproval = "15";
    public static final String ACTION_CODE_RejectModificationApproval = "16";
    public static final String ACTION_CODE_EditRepairRejected = "17";
    public static final String ACTION_CODE_ResetRepairRejected = "18";
    public static final String ACTION_CODE_SaveEditRejected = "19";
    public static final String ACTION_CODE_CancelEditRejected = "20";
    public static final String ACTION_CODE_ApproveDeletionApproval = "21";
    public static final String ACTION_CODE_RejectDeletionApproval = "22";
    public static final String ACTION_CODE_ReactivateStolen = "25";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveCreationApproval = "1653800329";
    public static final String ACTION_KEY_SVC_ApproveCreationApproval = "1821860163";
    public static final String ACTION_KEY_RejectCreationApproval = "959231703";
    public static final String ACTION_KEY_EditRepairNew = "195345174";
    public static final String ACTION_KEY_DeleteRepairNew = "209323395";
    public static final String ACTION_KEY_SaveEditNew = "708775918";
    public static final String ACTION_KEY_CancelEditNew = "2051391874";
    public static final String ACTION_KEY_DeleteViewApproved = "1318694537";
    public static final String ACTION_KEY_SVC_DeleteViewApproved = "1413355027";
    public static final String ACTION_KEY_UnblockViewApproved = "2051455359";
    public static final String ACTION_KEY_StolenViewApproved = "618773169";
    public static final String ACTION_KEY_ApproveModificationApproval = "75773915";
    public static final String ACTION_KEY_RejectModificationApproval = "1806465975";
    public static final String ACTION_KEY_EditRepairRejected = "2058060516";
    public static final String ACTION_KEY_ResetRepairRejected = "260977568";
    public static final String ACTION_KEY_SaveEditRejected = "761106099";
    public static final String ACTION_KEY_CancelEditRejected = "2034613300";
    public static final String ACTION_KEY_ApproveDeletionApproval = "1438801554";
    public static final String ACTION_KEY_RejectDeletionApproval = "1953600476";
    public static final String ACTION_KEY_ReactivateStolen = "1786023210";

}