package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_ServiceIntegSettings")
@XmlRootElement(name="ServiceIntegSettings")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ServiceIntegSetting extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ServiceIntegSetting(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String KEY = "key";
@Column(name="KEY", nullable=false, length=255)
private String key;
public String getKey(){
return this.key;
}
public void setKey(String key){
this.key = key;
}

public static final String VALUE = "value";
@Column(name="VALUE", nullable=false, length=1000)
private String value;
public String getValue(){
return this.value;
}
public void setValue(String value){
this.value = value;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=1000)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String REF_SERVICE = "refService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFSERVICEID", nullable=false)
private MPAY_CorpoarteService refService;
public MPAY_CorpoarteService getRefService(){
return this.refService;
}
public void setRefService(MPAY_CorpoarteService refService){
this.refService = refService;
}

@Override
public String toString() {
return "MPAY_ServiceIntegSetting [id= " + getId() + ", key= " + getKey() + ", value= " + getValue() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getKey() == null) ? 0 : getKey().hashCode());
result = prime * result + ((getValue() == null) ? 0 : getValue().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ServiceIntegSetting))
return false;
else {MPAY_ServiceIntegSetting other = (MPAY_ServiceIntegSetting) obj;
return this.hashCode() == other.hashCode();}
}


}