package com.progressoft.mpay.entities;

public class WF_CashOut
 {
    // Steps
    public static final String STEP_Initialization = "100601";
    public static final String STEP_CalcuationStep = "100606";
    public static final String STEP_PendingReply = "100602";
    public static final String STEP_Accepted = "100603";
    public static final String STEP_Rejected = "100604";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_CalcuationStep = "Calculate Charges";
    public static final String STEP_STATUS_PendingReply = "Pending Reply";
    public static final String STEP_STATUS_Accepted = "Accepted";
    public static final String STEP_STATUS_Rejected = "Rejected";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_SaveandSubmit = "Save and Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Recalculate = "Recalculate";
    public static final String ACTION_NAME_SVC_Accept = "SVC_Accept";
    public static final String ACTION_NAME_SVC_Reject = "SVC_Reject";
    public static final String ACTION_NAME_Resend = "Resend";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SaveandSubmitCalcuationStep = "5";
    public static final String ACTION_CODE_DeleteCalcuationStep = "6";
    public static final String ACTION_CODE_RecalculateCalcuationStep = "8";
    public static final String ACTION_CODE_SVC_AcceptPendingReply = "3";
    public static final String ACTION_CODE_SVC_RejectPendingReply = "4";
    public static final String ACTION_CODE_ResendPendingReply = "7";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SaveandSubmitCalcuationStep = "2115089605";
    public static final String ACTION_KEY_DeleteCalcuationStep = "757580642";
    public static final String ACTION_KEY_RecalculateCalcuationStep = "1267123227";
    public static final String ACTION_KEY_SVC_AcceptPendingReply = "1805932798";
    public static final String ACTION_KEY_SVC_RejectPendingReply = "1745579117";
    public static final String ACTION_KEY_ResendPendingReply = "526458127";

}