package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_ServicesCategories",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="ServicesCategories")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ServicesCategory extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ServicesCategory(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String SERVICES_CATEGORIES_N_L_S = "servicesCategoriesNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "serviceCategoryId")
@MapKey(name="languageCode")
private Map<String, MPAY_ServicesCategories_NLS> servicesCategoriesNLS;
public Map<String, MPAY_ServicesCategories_NLS> getServicesCategoriesNLS(){
return this.servicesCategoriesNLS;
}
public void setServicesCategoriesNLS(Map<String, MPAY_ServicesCategories_NLS> servicesCategoriesNLS){
this.servicesCategoriesNLS = servicesCategoriesNLS;
}

@Override
public String toString() {
return "MPAY_ServicesCategory [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ServicesCategory))
return false;
else {MPAY_ServicesCategory other = (MPAY_ServicesCategory) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.servicesCategoriesNLS;
}
}