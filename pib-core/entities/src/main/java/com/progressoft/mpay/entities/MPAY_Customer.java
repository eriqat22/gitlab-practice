package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_Customers")
@XmlRootElement(name="Customers")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Customer extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Customer(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String FULL_NAME = "fullName";
@Column(name="FULLNAME", nullable=true, length=200)
private String fullName;
public String getFullName(){
return this.fullName;
}
public void setFullName(String fullName){
this.fullName = fullName;
}

public static final String FIRST_NAME = "firstName";
@Column(name="FIRSTNAME", nullable=false, length=60)
private String firstName;
public String getFirstName(){
return this.firstName;
}
public void setFirstName(String firstName){
this.firstName = firstName;
}

public static final String MIDDLE_NAME = "middleName";
@Column(name="MIDDLENAME", nullable=false, length=60)
private String middleName;
public String getMiddleName(){
return this.middleName;
}
public void setMiddleName(String middleName){
this.middleName = middleName;
}

public static final String LAST_NAME = "lastName";
@Column(name="LASTNAME", nullable=false, length=60)
private String lastName;
public String getLastName(){
return this.lastName;
}
public void setLastName(String lastName){
this.lastName = lastName;
}

public static final String AR_FIRST_NAME = "arFirstName";
@Column(name="ARFIRSTNAME", nullable=true, length=60)
private String arFirstName;
public String getArFirstName(){
return this.arFirstName;
}
public void setArFirstName(String arFirstName){
this.arFirstName = arFirstName;
}

public static final String AR_MIDDLE_NAME = "arMiddleName";
@Column(name="ARMIDDLENAME", nullable=true, length=60)
private String arMiddleName;
public String getArMiddleName(){
return this.arMiddleName;
}
public void setArMiddleName(String arMiddleName){
this.arMiddleName = arMiddleName;
}

public static final String AR_LAST_NAME = "arLastName";
@Column(name="ARLASTNAME", nullable=true, length=60)
private String arLastName;
public String getArLastName(){
return this.arLastName;
}
public void setArLastName(String arLastName){
this.arLastName = arLastName;
}

public static final String ENGLISH_FULL_NAME = "englishFullName";
@Column(name="ENGLISHFULLNAME", nullable=true, length=200)
private String englishFullName;
public String getEnglishFullName(){
return this.englishFullName;
}
public void setEnglishFullName(String englishFullName){
this.englishFullName = englishFullName;
}

public static final String ARABIC_FULL_NAME = "arabicFullName";
@Column(name="ARABICFULLNAME", nullable=true, length=1000)
private String arabicFullName;
public String getArabicFullName(){
return this.arabicFullName;
}
public void setArabicFullName(String arabicFullName){
this.arabicFullName = arabicFullName;
}

public static final String GENDER = "gender";
@Column(name="GENDER", nullable=true, length=20)
private String gender;
public String getGender(){
return this.gender;
}
public void setGender(String gender){
this.gender = gender;
}

public static final String DOB = "dob";
@Column(name="DOB", nullable=false, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date dob;
public java.util.Date getDob(){
return this.dob;
}
public void setDob(java.util.Date dob){
this.dob = dob;
}

public static final String OCCUPATION = "occupation";
@Column(name="OCCUPATION", nullable=true, length=50)
private String occupation;
public String getOccupation(){
return this.occupation;
}
public void setOccupation(String occupation){
this.occupation = occupation;
}

public static final String ID_NUM = "idNum";
@Column(name="IDNUM", nullable=false, length=84)
private String idNum;
public String getIdNum(){
return this.idNum;
}
public void setIdNum(String idNum){
this.idNum = idNum;
}

public static final String ID_EXPIRY_DATE = "idExpiryDate";
@Column(name="IDEXPIRYDATE", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date idExpiryDate;
public java.util.Date getIdExpiryDate(){
return this.idExpiryDate;
}
public void setIdExpiryDate(java.util.Date idExpiryDate){
this.idExpiryDate = idExpiryDate;
}

public static final String IDENTIFICATION_REFERENCE = "identificationReference";
@Column(name="IDENTIFICATIONREFERENCE", nullable=true, length=50)
private String identificationReference;
public String getIdentificationReference(){
return this.identificationReference;
}
public void setIdentificationReference(String identificationReference){
this.identificationReference = identificationReference;
}

public static final String IDENTIFICATION_CARD = "identificationCard";
@Column(name="IDENTIFICATIONCARD", nullable=true, length=100)
private String identificationCard;
public String getIdentificationCard(){
return this.identificationCard;
}
public void setIdentificationCard(String identificationCard){
this.identificationCard = identificationCard;
}

public static final String ID_CARD_ISSUANCE_DATE = "idCardIssuanceDate";
@Column(name="IDCARDISSUANCEDATE", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date idCardIssuanceDate;
public java.util.Date getIdCardIssuanceDate(){
return this.idCardIssuanceDate;
}
public void setIdCardIssuanceDate(java.util.Date idCardIssuanceDate){
this.idCardIssuanceDate = idCardIssuanceDate;
}

public static final String PASSPORT_ID = "passportId";
@Column(name="PASSPORTID", nullable=true, length=100)
private String passportId;
public String getPassportId(){
return this.passportId;
}
public void setPassportId(String passportId){
this.passportId = passportId;
}

public static final String PASSPORT_ISSUANCE_DATE = "passportIssuanceDate";
@Column(name="PASSPORTISSUANCEDATE", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date passportIssuanceDate;
public java.util.Date getPassportIssuanceDate(){
return this.passportIssuanceDate;
}
public void setPassportIssuanceDate(java.util.Date passportIssuanceDate){
this.passportIssuanceDate = passportIssuanceDate;
}

public static final String CLIENT_REF = "clientRef";
@Column(name="CLIENTREF", nullable=false, length=50)
private String clientRef;
public String getClientRef(){
return this.clientRef;
}
public void setClientRef(String clientRef){
this.clientRef = clientRef;
}

public static final String REFERRALKEY = "referralkey";
@Column(name="REFERRALKEY", nullable=true, length=50)
private String referralkey;
public String getReferralkey(){
return this.referralkey;
}
public void setReferralkey(String referralkey){
this.referralkey = referralkey;
}

public static final String PHONE_ONE = "phoneOne";
@Column(name="PHONEONE", nullable=true, length=20)
private String phoneOne;
public String getPhoneOne(){
return this.phoneOne;
}
public void setPhoneOne(String phoneOne){
this.phoneOne = phoneOne;
}

public static final String PHONE_TWO = "phoneTwo";
@Column(name="PHONETWO", nullable=true, length=20)
private String phoneTwo;
public String getPhoneTwo(){
return this.phoneTwo;
}
public void setPhoneTwo(String phoneTwo){
this.phoneTwo = phoneTwo;
}

public static final String EMAIL = "email";
@Column(name="EMAIL", nullable=true, length=250)
private String email;
public String getEmail(){
return this.email;
}
public void setEmail(String email){
this.email = email;
}

public static final String POBOX = "pobox";
@Column(name="POBOX", nullable=true, length=20)
private String pobox;
public String getPobox(){
return this.pobox;
}
public void setPobox(String pobox){
this.pobox = pobox;
}

public static final String ZIP_CODE = "zipCode";
@Column(name="ZIPCODE", nullable=true, length=20)
private String zipCode;
public String getZipCode(){
return this.zipCode;
}
public void setZipCode(String zipCode){
this.zipCode = zipCode;
}

public static final String BUILDING_NUM = "buildingNum";
@Column(name="BUILDINGNUM", nullable=true, length=20)
private String buildingNum;
public String getBuildingNum(){
return this.buildingNum;
}
public void setBuildingNum(String buildingNum){
this.buildingNum = buildingNum;
}

public static final String STREET_NAME = "streetName";
@Column(name="STREETNAME", nullable=true, length=255)
private String streetName;
public String getStreetName(){
return this.streetName;
}
public void setStreetName(String streetName){
this.streetName = streetName;
}

public static final String ADDRESS = "address";
@Column(name="ADDRESS", nullable=true, length=1000)
private String address;
public String getAddress(){
return this.address;
}
public void setAddress(String address){
this.address = address;
}

public static final String NOTE = "note";
@Column(name="NOTE", nullable=true, length=255)
private String note;
public String getNote(){
return this.note;
}
public void setNote(String note){
this.note = note;
}

public static final String IS_LIGHT = "isLight";
@Column(name="ISLIGHT", nullable=false, length=1)
private Boolean isLight;
public Boolean getIsLight(){
return this.isLight;
}
public void setIsLight(Boolean isLight){
this.isLight = isLight;
}

public static final String IS_AUTO_REGISTERED = "isAutoRegistered";
@Column(name="ISAUTOREGISTERED", nullable=false, length=1)
private Boolean isAutoRegistered;
public Boolean getIsAutoRegistered(){
return this.isAutoRegistered;
}
public void setIsAutoRegistered(Boolean isAutoRegistered){
this.isAutoRegistered = isAutoRegistered;
}

public static final String EXPIRY_DATE = "expiryDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="EXPIRYDATE", nullable=true, length=12)
private java.sql.Timestamp expiryDate;
public java.sql.Timestamp getExpiryDate(){
return this.expiryDate;
}
public void setExpiryDate(java.sql.Timestamp expiryDate){
this.expiryDate = expiryDate;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=5)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String IS_REGISTERED = "isRegistered";
@Column(name="ISREGISTERED", nullable=true, length=1)
private Boolean isRegistered;
public Boolean getIsRegistered(){
return this.isRegistered;
}
public void setIsRegistered(Boolean isRegistered){
this.isRegistered = isRegistered;
}

public static final String IS_BLACK_LISTED = "isBlackListed";
@Column(name="ISBLACKLISTED", nullable=true, length=1)
private Boolean isBlackListed;
public Boolean getIsBlackListed(){
return this.isBlackListed;
}
public void setIsBlackListed(Boolean isBlackListed){
this.isBlackListed = isBlackListed;
}

public static final String REJECTION_NOTE = "rejectionNote";
@Column(name="REJECTIONNOTE", nullable=true, length=255)
private String rejectionNote;
public String getRejectionNote(){
return this.rejectionNote;
}
public void setRejectionNote(String rejectionNote){
this.rejectionNote = rejectionNote;
}

public static final String APPROVAL_NOTE = "approvalNote";
@Column(name="APPROVALNOTE", nullable=true, length=255)
private String approvalNote;
public String getApprovalNote(){
return this.approvalNote;
}
public void setApprovalNote(String approvalNote){
this.approvalNote = approvalNote;
}

public static final String MOBILE_NUMBER = "mobileNumber";
@Column(name="MOBILENUMBER", nullable=true, length=20)
private String mobileNumber;
public String getMobileNumber(){
return this.mobileNumber;
}
public void setMobileNumber(String mobileNumber){
this.mobileNumber = mobileNumber;
}

public static final String ALIAS = "alias";
@Column(name="ALIAS", nullable=true, length=25)
private String alias;
public String getAlias(){
return this.alias;
}
public void setAlias(String alias){
this.alias = alias;
}

public static final String NOTIFICATION_SHOW_TYPE = "notificationShowType";
@Column(name="NOTIFICATIONSHOWTYPE", nullable=true, length=50)
private String notificationShowType;
public String getNotificationShowType(){
return this.notificationShowType;
}
public void setNotificationShowType(String notificationShowType){
this.notificationShowType = notificationShowType;
}

public static final String MAX_NUMBER_OF_DEVICES = "maxNumberOfDevices";
@Column(name="MAXNUMBEROFDEVICES", nullable=false, length=10)
private Long maxNumberOfDevices;
public Long getMaxNumberOfDevices(){
return this.maxNumberOfDevices;
}
public void setMaxNumberOfDevices(Long maxNumberOfDevices){
this.maxNumberOfDevices = maxNumberOfDevices;
}

public static final String NFC_SERIAL = "nfcSerial";
@Column(name="NFCSERIAL", nullable=true, length=255)
private String nfcSerial;
public String getNfcSerial(){
return this.nfcSerial;
}
public void setNfcSerial(String nfcSerial){
this.nfcSerial = nfcSerial;
}

public static final String ENABLE_S_M_S = "enableSMS";
@Column(name="ENABLESMS", nullable=true, length=10)
private Boolean enableSMS;
public Boolean getEnableSMS(){
return this.enableSMS;
}
public void setEnableSMS(Boolean enableSMS){
this.enableSMS = enableSMS;
}

public static final String ENABLE_EMAIL = "enableEmail";
@Column(name="ENABLEEMAIL", nullable=true, length=10)
private Boolean enableEmail;
public Boolean getEnableEmail(){
return this.enableEmail;
}
public void setEnableEmail(Boolean enableEmail){
this.enableEmail = enableEmail;
}

public static final String ENABLE_PUSH_NOTIFICATION = "enablePushNotification";
@Column(name="ENABLEPUSHNOTIFICATION", nullable=true, length=10)
private Boolean enablePushNotification;
public Boolean getEnablePushNotification(){
return this.enablePushNotification;
}
public void setEnablePushNotification(Boolean enablePushNotification){
this.enablePushNotification = enablePushNotification;
}

public static final String BANKED_UNBANKED = "bankedUnbanked";
@Column(name="BANKEDUNBANKED", nullable=true, length=20)
private String bankedUnbanked;
public String getBankedUnbanked(){
return this.bankedUnbanked;
}
public void setBankedUnbanked(String bankedUnbanked){
this.bankedUnbanked = bankedUnbanked;
}

public static final String BANK_BRANCH = "bankBranch";
@Column(name="BANKBRANCH", nullable=true, length=50)
private String bankBranch;
public String getBankBranch(){
return this.bankBranch;
}
public void setBankBranch(String bankBranch){
this.bankBranch = bankBranch;
}

public static final String EXTERNAL_ACC = "externalAcc";
@Column(name="EXTERNALACC", nullable=true, length=100)
private String externalAcc;
public String getExternalAcc(){
return this.externalAcc;
}
public void setExternalAcc(String externalAcc){
this.externalAcc = externalAcc;
}

public static final String IBAN = "iban";
@Column(name="IBAN", nullable=true, length=100)
private String iban;
public String getIban(){
return this.iban;
}
public void setIban(String iban){
this.iban = iban;
}

public static final String MAS = "mas";
@Column(name="MAS", nullable=true, length=10)
private Long mas;
public Long getMas(){
return this.mas;
}
public void setMas(Long mas){
this.mas = mas;
}

public static final String HINT = "hint";
@Column(name="HINT", nullable=true, length=255)
private String hint;
public String getHint(){
return this.hint;
}
public void setHint(String hint){
this.hint = hint;
}

public static final String APPROVED_DATA = "approvedData";
@Column(name="APPROVEDDATA", nullable=true, length=4000)
private String approvedData;
public String getApprovedData(){
return this.approvedData;
}
public void setApprovedData(String approvedData){
this.approvedData = approvedData;
}

public static final String CHANGES = "changes";
@Column(name="CHANGES", nullable=true, length=1000)
@Transient
private String changes;
public String getChanges(){
return this.changes;
}
public void setChanges(String changes){
this.changes = changes;
}

public static final String KYCID = "kycid";
@Column(name="KYCID", nullable=true, length=20)
private String kycid;
public String getKycid(){
return this.kycid;
}
public void setKycid(String kycid){
this.kycid = kycid;
}

public static final String REASON_FOR_RESIDENCE = "reasonForResidence";
@Column(name="REASONFORRESIDENCE", nullable=true, length=300)
private String reasonForResidence;
public String getReasonForResidence(){
return this.reasonForResidence;
}
public void setReasonForResidence(String reasonForResidence){
this.reasonForResidence = reasonForResidence;
}

public static final String IS_EXPIRED = "isExpired";
@Column(name="ISEXPIRED", nullable=false, length=1)
private Boolean isExpired;
public Boolean getIsExpired(){
return this.isExpired;
}
public void setIsExpired(Boolean isExpired){
this.isExpired = isExpired;
}

public static final String AVATAR = "avatar";
@Column(name="AVATAR", nullable=true, length=4000)
@Lob
private String avatar;
public String getAvatar(){
return this.avatar;
}
public void setAvatar(String avatar){
this.avatar = avatar;
}

public static final String COMMISION_PAYED = "commisionPayed";
@Column(name="COMMISIONPAYED", nullable=true, length=1)
private Boolean commisionPayed;
public Boolean getCommisionPayed(){
return this.commisionPayed;
}
public void setCommisionPayed(Boolean commisionPayed){
this.commisionPayed = commisionPayed;
}

public static final String WALLET_UPGRADED = "walletUpgraded";
@Column(name="WALLETUPGRADED", nullable=true, length=1)
private Boolean walletUpgraded;
public Boolean getWalletUpgraded(){
return this.walletUpgraded;
}
public void setWalletUpgraded(Boolean walletUpgraded){
this.walletUpgraded = walletUpgraded;
}

public static final String KYC_TEMPLATE = "kycTemplate";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="KYCTEMPLATEID", nullable=true)
private MPAY_CustomerKyc kycTemplate;
public MPAY_CustomerKyc getKycTemplate(){
return this.kycTemplate;
}
public void setKycTemplate(MPAY_CustomerKyc kycTemplate){
this.kycTemplate = kycTemplate;
}

public static final String ID_TYPE = "idType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="IDTYPEID", nullable=false)
private MPAY_IDType idType;
public MPAY_IDType getIdType(){
return this.idType;
}
public void setIdType(MPAY_IDType idType){
this.idType = idType;
}

public static final String CLIENT_TYPE = "clientType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CLIENTTYPEID", nullable=false)
private MPAY_ClientType clientType;
public MPAY_ClientType getClientType(){
return this.clientType;
}
public void setClientType(MPAY_ClientType clientType){
this.clientType = clientType;
}

public static final String REFEREE_CUSTOMER_KEY = "refereeCustomerKey";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFEREECUSTOMERKEYID", nullable=true)
private com.progressoft.mpay.entities.MPAY_Customer refereeCustomerKey;
public com.progressoft.mpay.entities.MPAY_Customer getRefereeCustomerKey(){
return this.refereeCustomerKey;
}
public void setRefereeCustomerKey(com.progressoft.mpay.entities.MPAY_Customer refereeCustomerKey){
this.refereeCustomerKey = refereeCustomerKey;
}

public static final String CITY = "city";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CITYID", nullable=false)
private MPAY_City city;
public MPAY_City getCity(){
return this.city;
}
public void setCity(MPAY_City city){
this.city = city;
}

public static final String AREA = "area";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="AREAID", nullable=true)
private MPAY_CityArea area;
public MPAY_CityArea getArea(){
return this.area;
}
public void setArea(MPAY_CityArea area){
this.area = area;
}

public static final String PREF_LANG = "prefLang";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PREFLANGID", nullable=false)
private MPAY_Language prefLang;
public MPAY_Language getPrefLang(){
return this.prefLang;
}
public void setPrefLang(MPAY_Language prefLang){
this.prefLang = prefLang;
}

public static final String REG_AGENT = "regAgent";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REGAGENTID", nullable=true)
private MPAY_CorpoarteService regAgent;
@flexjson.JSON(include = false)
public MPAY_CorpoarteService getRegAgent(){
return this.regAgent;
}
public void setRegAgent(MPAY_CorpoarteService regAgent){
this.regAgent = regAgent;
}

public static final String BANK = "bank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BANKID", nullable=true)
private MPAY_Bank bank;
public MPAY_Bank getBank(){
return this.bank;
}
public void setBank(MPAY_Bank bank){
this.bank = bank;
}

public static final String REF_PROFILE = "refProfile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFPROFILEID", nullable=true)
private MPAY_Profile refProfile;
public MPAY_Profile getRefProfile(){
return this.refProfile;
}
public void setRefProfile(MPAY_Profile refProfile){
this.refProfile = refProfile;
}

public static final String INST_ID = "instId";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="INSTID", nullable=true)
private MPAY_Intg_Cust_Reg_Instr instId;
public MPAY_Intg_Cust_Reg_Instr getInstId(){
return this.instId;
}
public void setInstId(MPAY_Intg_Cust_Reg_Instr instId){
this.instId = instId;
}

public static final String REF_LAST_MSG_LOG = "refLastMsgLog";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFLASTMSGLOGID", nullable=true)
private MPAY_MpClearIntegMsgLog refLastMsgLog;
public MPAY_MpClearIntegMsgLog getRefLastMsgLog(){
return this.refLastMsgLog;
}
public void setRefLastMsgLog(MPAY_MpClearIntegMsgLog refLastMsgLog){
this.refLastMsgLog = refLastMsgLog;
}

public static final String BULK_REGISTRATION = "bulkRegistration";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BULKREGISTRATIONID", nullable=true)
private MPAY_BulkRegistration bulkRegistration;
public MPAY_BulkRegistration getBulkRegistration(){
return this.bulkRegistration;
}
public void setBulkRegistration(MPAY_BulkRegistration bulkRegistration){
this.bulkRegistration = bulkRegistration;
}

public static final String NATIONALITY = "nationality";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="NATIONALITYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JfwCountry nationality;
public com.progressoft.jfw.model.bussinessobject.core.JfwCountry getNationality(){
return this.nationality;
}
public void setNationality(com.progressoft.jfw.model.bussinessobject.core.JfwCountry nationality){
this.nationality = nationality;
}

public static final String REF_CUSTOMER_AML_CASES = "refCustomerAmlCases";
@OneToMany(mappedBy = "refCustomer")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_AmlCase> refCustomerAmlCases;
public List<MPAY_AmlCase> getRefCustomerAmlCases(){
return this.refCustomerAmlCases;
}
public void setRefCustomerAmlCases(List<MPAY_AmlCase> refCustomerAmlCases){
this.refCustomerAmlCases = refCustomerAmlCases;
}

public static final String REF_CUSTOMER_CUSTOMER_MOBILES = "refCustomerCustomerMobiles";
@OneToMany(mappedBy = "refCustomer")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CustomerMobile> refCustomerCustomerMobiles;
public List<MPAY_CustomerMobile> getRefCustomerCustomerMobiles(){
return this.refCustomerCustomerMobiles;
}
public void setRefCustomerCustomerMobiles(List<MPAY_CustomerMobile> refCustomerCustomerMobiles){
this.refCustomerCustomerMobiles = refCustomerCustomerMobiles;
}

public static final String REF_CUSTOMER_CUST_INTEG_MESSAGES = "refCustomerCustIntegMessages";
@OneToMany(mappedBy = "refCustomer")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CustIntegMessage> refCustomerCustIntegMessages;
public List<MPAY_CustIntegMessage> getRefCustomerCustIntegMessages(){
return this.refCustomerCustIntegMessages;
}
public void setRefCustomerCustIntegMessages(List<MPAY_CustIntegMessage> refCustomerCustIntegMessages){
this.refCustomerCustIntegMessages = refCustomerCustIntegMessages;
}

@Override
public String toString() {
return "MPAY_Customer [id= " + getId() + ", fullName= " + getFullName() + ", firstName= " + getFirstName() + ", middleName= " + getMiddleName() + ", lastName= " + getLastName() + ", arFirstName= " + getArFirstName() + ", arMiddleName= " + getArMiddleName() + ", arLastName= " + getArLastName() + ", englishFullName= " + getEnglishFullName() + ", arabicFullName= " + getArabicFullName() + ", gender= " + getGender() + ", dob= " + getDob() + ", occupation= " + getOccupation() + ", idNum= " + getIdNum() + ", idExpiryDate= " + getIdExpiryDate() + ", identificationReference= " + getIdentificationReference() + ", identificationCard= " + getIdentificationCard() + ", idCardIssuanceDate= " + getIdCardIssuanceDate() + ", passportId= " + getPassportId() + ", passportIssuanceDate= " + getPassportIssuanceDate() + ", clientRef= " + getClientRef() + ", referralkey= " + getReferralkey() + ", phoneOne= " + getPhoneOne() + ", phoneTwo= " + getPhoneTwo() + ", email= " + getEmail() + ", pobox= " + getPobox() + ", zipCode= " + getZipCode() + ", buildingNum= " + getBuildingNum() + ", streetName= " + getStreetName() + ", address= " + getAddress() + ", note= " + getNote() + ", isLight= " + getIsLight() + ", isAutoRegistered= " + getIsAutoRegistered() + ", expiryDate= " + getExpiryDate() + ", isActive= " + getIsActive() + ", isRegistered= " + getIsRegistered() + ", isBlackListed= " + getIsBlackListed() + ", rejectionNote= " + getRejectionNote() + ", approvalNote= " + getApprovalNote() + ", mobileNumber= " + getMobileNumber() + ", alias= " + getAlias() + ", notificationShowType= " + getNotificationShowType() + ", maxNumberOfDevices= " + getMaxNumberOfDevices() + ", nfcSerial= " + getNfcSerial() + ", enableSMS= " + getEnableSMS() + ", enableEmail= " + getEnableEmail() + ", enablePushNotification= " + getEnablePushNotification() + ", bankedUnbanked= " + getBankedUnbanked() + ", bankBranch= " + getBankBranch() + ", externalAcc= " + getExternalAcc() + ", iban= " + getIban() + ", mas= " + getMas() + ", hint= " + getHint() + ", approvedData= " + getApprovedData() + ", changes= " + getChanges() + ", kycid= " + getKycid() + ", reasonForResidence= " + getReasonForResidence() + ", isExpired= " + getIsExpired() + ", avatar= " + getAvatar() + ", commisionPayed= " + getCommisionPayed() + ", walletUpgraded= " + getWalletUpgraded() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getFullName() == null) ? 0 : getFullName().hashCode());
result = prime * result + ((getFirstName() == null) ? 0 : getFirstName().hashCode());
result = prime * result + ((getMiddleName() == null) ? 0 : getMiddleName().hashCode());
result = prime * result + ((getLastName() == null) ? 0 : getLastName().hashCode());
result = prime * result + ((getArFirstName() == null) ? 0 : getArFirstName().hashCode());
result = prime * result + ((getArMiddleName() == null) ? 0 : getArMiddleName().hashCode());
result = prime * result + ((getArLastName() == null) ? 0 : getArLastName().hashCode());
result = prime * result + ((getEnglishFullName() == null) ? 0 : getEnglishFullName().hashCode());
result = prime * result + ((getArabicFullName() == null) ? 0 : getArabicFullName().hashCode());
result = prime * result + ((getDob() == null) ? 0 : getDob().hashCode());
result = prime * result + ((getOccupation() == null) ? 0 : getOccupation().hashCode());
result = prime * result + ((getIdNum() == null) ? 0 : getIdNum().hashCode());
result = prime * result + ((getIdExpiryDate() == null) ? 0 : getIdExpiryDate().hashCode());
result = prime * result + ((getIdentificationReference() == null) ? 0 : getIdentificationReference().hashCode());
result = prime * result + ((getIdentificationCard() == null) ? 0 : getIdentificationCard().hashCode());
result = prime * result + ((getIdCardIssuanceDate() == null) ? 0 : getIdCardIssuanceDate().hashCode());
result = prime * result + ((getPassportId() == null) ? 0 : getPassportId().hashCode());
result = prime * result + ((getPassportIssuanceDate() == null) ? 0 : getPassportIssuanceDate().hashCode());
result = prime * result + ((getClientRef() == null) ? 0 : getClientRef().hashCode());
result = prime * result + ((getReferralkey() == null) ? 0 : getReferralkey().hashCode());
result = prime * result + ((getPhoneOne() == null) ? 0 : getPhoneOne().hashCode());
result = prime * result + ((getPhoneTwo() == null) ? 0 : getPhoneTwo().hashCode());
result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
result = prime * result + ((getPobox() == null) ? 0 : getPobox().hashCode());
result = prime * result + ((getZipCode() == null) ? 0 : getZipCode().hashCode());
result = prime * result + ((getBuildingNum() == null) ? 0 : getBuildingNum().hashCode());
result = prime * result + ((getStreetName() == null) ? 0 : getStreetName().hashCode());
result = prime * result + ((getMobileNumber() == null) ? 0 : getMobileNumber().hashCode());
result = prime * result + ((getAlias() == null) ? 0 : getAlias().hashCode());
int MaxNumberOfDevices= new Long("null".equals(getMaxNumberOfDevices() + "") ? 0 : getMaxNumberOfDevices()).intValue();
result = prime * result + (int) (MaxNumberOfDevices ^ MaxNumberOfDevices >>> 32);
result = prime * result + ((getNfcSerial() == null) ? 0 : getNfcSerial().hashCode());
result = prime * result + ((getBankBranch() == null) ? 0 : getBankBranch().hashCode());
result = prime * result + ((getExternalAcc() == null) ? 0 : getExternalAcc().hashCode());
result = prime * result + ((getIban() == null) ? 0 : getIban().hashCode());
int Mas= new Long("null".equals(getMas() + "") ? 0 : getMas()).intValue();
result = prime * result + (int) (Mas ^ Mas >>> 32);
result = prime * result + ((getHint() == null) ? 0 : getHint().hashCode());
result = prime * result + ((getChanges() == null) ? 0 : getChanges().hashCode());
result = prime * result + ((getKycid() == null) ? 0 : getKycid().hashCode());
result = prime * result + ((getReasonForResidence() == null) ? 0 : getReasonForResidence().hashCode());
result = prime * result + ((getAvatar() == null) ? 0 : getAvatar().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Customer))
return false;
else {MPAY_Customer other = (MPAY_Customer) obj;
return this.hashCode() == other.hashCode();}
}


}