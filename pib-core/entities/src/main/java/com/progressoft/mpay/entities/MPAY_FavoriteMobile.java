package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_FavoriteMobiles")
@XmlRootElement(name="FavoriteMobiles")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_FavoriteMobile extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_FavoriteMobile(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String FAVORITE_MOBILE = "favoriteMobile";
@Column(name="FAVORITEMOBILE", nullable=true, length=255)
private String favoriteMobile;
public String getFavoriteMobile(){
return this.favoriteMobile;
}
public void setFavoriteMobile(String favoriteMobile){
this.favoriteMobile = favoriteMobile;
}

public static final String FAVORITE_NAME = "favoriteName";
@Column(name="FAVORITENAME", nullable=true, length=255)
private String favoriteName;
public String getFavoriteName(){
return this.favoriteName;
}
public void setFavoriteName(String favoriteName){
this.favoriteName = favoriteName;
}

public static final String REF_MOBILE = "refMobile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMOBILEID", nullable=false)
private MPAY_CustomerMobile refMobile;
public MPAY_CustomerMobile getRefMobile(){
return this.refMobile;
}
public void setRefMobile(MPAY_CustomerMobile refMobile){
this.refMobile = refMobile;
}

@Override
public String toString() {
return "MPAY_FavoriteMobile [id= " + getId() + ", favoriteMobile= " + getFavoriteMobile() + ", favoriteName= " + getFavoriteName() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getFavoriteMobile() == null) ? 0 : getFavoriteMobile().hashCode());
result = prime * result + ((getFavoriteName() == null) ? 0 : getFavoriteName().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_FavoriteMobile))
return false;
else {MPAY_FavoriteMobile other = (MPAY_FavoriteMobile) obj;
return this.hashCode() == other.hashCode();}
}


}