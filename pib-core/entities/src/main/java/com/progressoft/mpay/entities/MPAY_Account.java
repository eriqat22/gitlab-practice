package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_Accounts",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"ACCNUMBER","Z_TENANT_ID"})
})
@XmlRootElement(name="Accounts")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Account extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Account(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String ACC_NUMBER = "accNumber";
@Column(name="ACCNUMBER", nullable=false, length=35)
private String accNumber;
public String getAccNumber(){
return this.accNumber;
}
public void setAccNumber(String accNumber){
this.accNumber = accNumber;
}

public static final String EXTERNAL_ACC = "externalAcc";
@Column(name="EXTERNALACC", nullable=true, length=100)
private String externalAcc;
public String getExternalAcc(){
return this.externalAcc;
}
public void setExternalAcc(String externalAcc){
this.externalAcc = externalAcc;
}

public static final String ACC_NAME = "accName";
@Column(name="ACCNAME", nullable=true, length=255)
private String accName;
public String getAccName(){
return this.accName;
}
public void setAccName(String accName){
this.accName = accName;
}

public static final String MIN_BALANCE = "minBalance";
@Column(name="MINBALANCE", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal minBalance;
public java.math.BigDecimal getMinBalance(){
return this.minBalance;
}
public void setMinBalance(java.math.BigDecimal minBalance){
this.minBalance = minBalance;
}

public static final String LEVEL1 = "level1";
@Column(name="LEVEL1", nullable=true, length=1)
private String level1;
public String getLevel1(){
return this.level1;
}
public void setLevel1(String level1){
this.level1 = level1;
}

public static final String LEVEL2 = "level2";
@Column(name="LEVEL2", nullable=true, length=2)
private String level2;
public String getLevel2(){
return this.level2;
}
public void setLevel2(String level2){
this.level2 = level2;
}

public static final String LEVEL3 = "level3";
@Column(name="LEVEL3", nullable=true, length=2)
private String level3;
public String getLevel3(){
return this.level3;
}
public void setLevel3(String level3){
this.level3 = level3;
}

public static final String LEVEL4 = "level4";
@Column(name="LEVEL4", nullable=true, length=5)
private String level4;
public String getLevel4(){
return this.level4;
}
public void setLevel4(String level4){
this.level4 = level4;
}

public static final String LEVEL5 = "level5";
@Column(name="LEVEL5", nullable=true, length=25)
private String level5;
public String getLevel5(){
return this.level5;
}
public void setLevel5(String level5){
this.level5 = level5;
}

public static final String ACC_LEVEL = "accLevel";
@Column(name="ACCLEVEL", nullable=true, length=1)
private String accLevel;
public String getAccLevel(){
return this.accLevel;
}
public void setAccLevel(String accLevel){
this.accLevel = accLevel;
}

public static final String ACC_COUNT = "accCount";
@Column(name="ACCCOUNT", nullable=true, length=5)
private Long accCount;
public Long getAccCount(){
return this.accCount;
}
public void setAccCount(Long accCount){
this.accCount = accCount;
}

public static final String NATURE = "nature";
@Column(name="NATURE", nullable=true, length=1)
private String nature;
public String getNature(){
return this.nature;
}
public void setNature(String nature){
this.nature = nature;
}

public static final String BALANCE = "balance";
@Column(name="BALANCE", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal balance;
public java.math.BigDecimal getBalance(){
return this.balance;
}
public void setBalance(java.math.BigDecimal balance){
this.balance = balance;
}

public static final String IS_BANKED = "isBanked";
@Column(name="ISBANKED", nullable=false, length=1)
private Boolean isBanked;
public Boolean getIsBanked(){
return this.isBanked;
}
public void setIsBanked(Boolean isBanked){
this.isBanked = isBanked;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=true, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String ACC_PARENT = "accParent";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="ACCPARENTID", nullable=true)
private MPAY_Account accParent;
public MPAY_Account getAccParent(){
return this.accParent;
}
public void setAccParent(MPAY_Account accParent){
this.accParent = accParent;
}

public static final String ACCOUNT_TYPE = "accountType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="ACCOUNTTYPEID", nullable=false)
private MPAY_AccountType accountType;
public MPAY_AccountType getAccountType(){
return this.accountType;
}
public void setAccountType(MPAY_AccountType accountType){
this.accountType = accountType;
}

public static final String BALANCE_TYPE = "balanceType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BALANCETYPEID", nullable=true)
private MPAY_BalanceType balanceType;
public MPAY_BalanceType getBalanceType(){
return this.balanceType;
}
public void setBalanceType(MPAY_BalanceType balanceType){
this.balanceType = balanceType;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

public static final String ACCOUNT_CLIENTS_COMMISSIONS = "accountClientsCommissions";
@OneToMany(mappedBy = "account")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ClientsCommission> accountClientsCommissions;
public List<MPAY_ClientsCommission> getAccountClientsCommissions(){
return this.accountClientsCommissions;
}
public void setAccountClientsCommissions(List<MPAY_ClientsCommission> accountClientsCommissions){
this.accountClientsCommissions = accountClientsCommissions;
}

public static final String ACCOUNT_CLIENTS_LIMITS = "accountClientsLimits";
@OneToMany(mappedBy = "account")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ClientsLimit> accountClientsLimits;
public List<MPAY_ClientsLimit> getAccountClientsLimits(){
return this.accountClientsLimits;
}
public void setAccountClientsLimits(List<MPAY_ClientsLimit> accountClientsLimits){
this.accountClientsLimits = accountClientsLimits;
}

public static final String REF_ACCOUNT_JV_DETAILS = "refAccountJvDetails";
@OneToMany(mappedBy = "refAccount")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_JvDetail> refAccountJvDetails;
public List<MPAY_JvDetail> getRefAccountJvDetails(){
return this.refAccountJvDetails;
}
public void setRefAccountJvDetails(List<MPAY_JvDetail> refAccountJvDetails){
this.refAccountJvDetails = refAccountJvDetails;
}

public static final String ACC_PARENT_ACCOUNTS = "accParentAccounts";
@OneToMany(mappedBy = "accParent")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Account> accParentAccounts;
public List<MPAY_Account> getAccParentAccounts(){
return this.accParentAccounts;
}
public void setAccParentAccounts(List<MPAY_Account> accParentAccounts){
this.accParentAccounts = accParentAccounts;
}

@Override
public String toString() {
return "MPAY_Account [id= " + getId() + ", accNumber= " + getAccNumber() + ", externalAcc= " + getExternalAcc() + ", accName= " + getAccName() + ", minBalance= " + getMinBalance() + ", level1= " + getLevel1() + ", level2= " + getLevel2() + ", level3= " + getLevel3() + ", level4= " + getLevel4() + ", level5= " + getLevel5() + ", accLevel= " + getAccLevel() + ", accCount= " + getAccCount() + ", nature= " + getNature() + ", balance= " + getBalance() + ", isBanked= " + getIsBanked() + ", isActive= " + getIsActive() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getAccNumber() == null) ? 0 : getAccNumber().hashCode());
result = prime * result + ((getExternalAcc() == null) ? 0 : getExternalAcc().hashCode());
result = prime * result + ((getAccName() == null) ? 0 : getAccName().hashCode());
result = prime * result + ((getLevel1() == null) ? 0 : getLevel1().hashCode());
result = prime * result + ((getLevel2() == null) ? 0 : getLevel2().hashCode());
result = prime * result + ((getLevel3() == null) ? 0 : getLevel3().hashCode());
result = prime * result + ((getLevel4() == null) ? 0 : getLevel4().hashCode());
result = prime * result + ((getLevel5() == null) ? 0 : getLevel5().hashCode());
result = prime * result + ((getAccLevel() == null) ? 0 : getAccLevel().hashCode());
int AccCount= new Long("null".equals(getAccCount() + "") ? 0 : getAccCount()).intValue();
result = prime * result + (int) (AccCount ^ AccCount >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Account))
return false;
else {MPAY_Account other = (MPAY_Account) obj;
return this.hashCode() == other.hashCode();}
}


}