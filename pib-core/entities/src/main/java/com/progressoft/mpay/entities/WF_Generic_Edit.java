package com.progressoft.mpay.entities;

public class WF_Generic_Edit
 {
    // Steps
    public static final String STEP_Initialization = "104601";
    public static final String STEP_ApprovalNewStep = "104602";
    public static final String STEP_RepairStep = "104603";
    public static final String STEP_ApprovalRepairStep = "104604";
    public static final String STEP_ActiveStep = "104605";
    public static final String STEP_EditStep = "104606";
    public static final String STEP_ApprovalEditStep = "104607";
    public static final String STEP_ApprovalDeleteStep = "104608";
    public static final String STEP_DeletedStep = "104609";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_ApprovalNewStep = "Approval New";
    public static final String STEP_STATUS_RepairStep = "Repair";
    public static final String STEP_STATUS_ApprovalRepairStep = "Approval Repair";
    public static final String STEP_STATUS_ActiveStep = "Active";
    public static final String STEP_STATUS_EditStep = "Edit";
    public static final String STEP_STATUS_ApprovalEditStep = "Approval Edit";
    public static final String STEP_STATUS_ApprovalDeleteStep = "Approval Delete";
    public static final String STEP_STATUS_DeletedStep = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveApprovalNewStep = "3";
    public static final String ACTION_CODE_RejectApprovalNewStep = "4";
    public static final String ACTION_CODE_SubmitRepairStep = "5";
    public static final String ACTION_CODE_DeleteRepairStep = "6";
    public static final String ACTION_CODE_ApproveApprovalRepairStep = "7";
    public static final String ACTION_CODE_RejectApprovalRepairStep = "8";
    public static final String ACTION_CODE_EditActiveStep = "9";
    public static final String ACTION_CODE_DeleteActiveStep = "12";
    public static final String ACTION_CODE_SubmitEditStep = "10";
    public static final String ACTION_CODE_CancelEditStep = "11";
    public static final String ACTION_CODE_ApproveApprovalEditStep = "13";
    public static final String ACTION_CODE_RejectApprovalEditStep = "14";
    public static final String ACTION_CODE_ApproveApprovalDeleteStep = "15";
    public static final String ACTION_CODE_RejectApprovalDeleteStep = "16";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveApprovalNewStep = "1899702617";
    public static final String ACTION_KEY_RejectApprovalNewStep = "1239138802";
    public static final String ACTION_KEY_SubmitRepairStep = "1727220900";
    public static final String ACTION_KEY_DeleteRepairStep = "1705735742";
    public static final String ACTION_KEY_ApproveApprovalRepairStep = "2096725858";
    public static final String ACTION_KEY_RejectApprovalRepairStep = "2075395720";
    public static final String ACTION_KEY_EditActiveStep = "832745776";
    public static final String ACTION_KEY_DeleteActiveStep = "165555144";
    public static final String ACTION_KEY_SubmitEditStep = "336674074";
    public static final String ACTION_KEY_CancelEditStep = "1670997506";
    public static final String ACTION_KEY_ApproveApprovalEditStep = "1102654505";
    public static final String ACTION_KEY_RejectApprovalEditStep = "1157221452";
    public static final String ACTION_KEY_ApproveApprovalDeleteStep = "678803230";
    public static final String ACTION_KEY_RejectApprovalDeleteStep = "1058119117";

}