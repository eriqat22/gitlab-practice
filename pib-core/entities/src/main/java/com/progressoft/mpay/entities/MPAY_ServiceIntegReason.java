package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_ServiceIntegReasons",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","PROCESSORCODE","MPAYMAPPINGREASONID","Z_TENANT_ID"})
})
@XmlRootElement(name="ServiceIntegReasons")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ServiceIntegReason extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ServiceIntegReason(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String PROCESSOR_CODE = "processorCode";
@Column(name="PROCESSORCODE", nullable=true, length=12)
private String processorCode;
public String getProcessorCode(){
return this.processorCode;
}
public void setProcessorCode(String processorCode){
this.processorCode = processorCode;
}

public static final String MPAY_MAPPING_REASON = "mpayMappingReason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MPAYMAPPINGREASONID", nullable=true)
private MPAY_Reason mpayMappingReason;
public MPAY_Reason getMpayMappingReason(){
return this.mpayMappingReason;
}
public void setMpayMappingReason(MPAY_Reason mpayMappingReason){
this.mpayMappingReason = mpayMappingReason;
}

public static final String SERVICE_INTEG_REASONS_N_L_S = "serviceIntegReasonsNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "rsnId")
@MapKey(name="languageCode")
private Map<String, MPAY_ServiceIntegReasons_NLS> serviceIntegReasonsNLS;
public Map<String, MPAY_ServiceIntegReasons_NLS> getServiceIntegReasonsNLS(){
return this.serviceIntegReasonsNLS;
}
public void setServiceIntegReasonsNLS(Map<String, MPAY_ServiceIntegReasons_NLS> serviceIntegReasonsNLS){
this.serviceIntegReasonsNLS = serviceIntegReasonsNLS;
}

@Override
public String toString() {
return "MPAY_ServiceIntegReason [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", processorCode= " + getProcessorCode() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getProcessorCode() == null) ? 0 : getProcessorCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ServiceIntegReason))
return false;
else {MPAY_ServiceIntegReason other = (MPAY_ServiceIntegReason) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.serviceIntegReasonsNLS;
}
}