package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_SysConfigs",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"SECTION","CONFIGKEY","Z_TENANT_ID"})
})
@XmlRootElement(name="SysConfigs")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_SysConfig extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_SysConfig(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String SECTION = "section";
@Column(name="SECTION", nullable=false, length=255)
private String section;
public String getSection(){
return this.section;
}
public void setSection(String section){
this.section = section;
}

public static final String CONFIG_KEY = "configKey";
@Column(name="CONFIGKEY", nullable=false, length=255)
private String configKey;
public String getConfigKey(){
return this.configKey;
}
public void setConfigKey(String configKey){
this.configKey = configKey;
}

public static final String CONFIG_VALUE = "configValue";
@Column(name="CONFIGVALUE", nullable=false, length=4000)
private String configValue;
public String getConfigValue(){
return this.configValue;
}
public void setConfigValue(String configValue){
this.configValue = configValue;
}

public static final String REGEX = "regex";
@Column(name="REGEX", nullable=true, length=255)
private String regex;
public String getRegex(){
return this.regex;
}
public void setRegex(String regex){
this.regex = regex;
}

public static final String HINT = "hint";
@Column(name="HINT", nullable=true, length=255)
private String hint;
public String getHint(){
return this.hint;
}
public void setHint(String hint){
this.hint = hint;
}

public static final String IS_DOWNLOADABLE = "isDownloadable";
@Column(name="ISDOWNLOADABLE", nullable=true, length=1)
private Boolean isDownloadable;
public Boolean getIsDownloadable(){
return this.isDownloadable;
}
public void setIsDownloadable(Boolean isDownloadable){
this.isDownloadable = isDownloadable;
}

@Override
public String toString() {
return "MPAY_SysConfig [id= " + getId() + ", section= " + getSection() + ", configKey= " + getConfigKey() + ", configValue= " + getConfigValue() + ", regex= " + getRegex() + ", hint= " + getHint() + ", isDownloadable= " + getIsDownloadable() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getSection() == null) ? 0 : getSection().hashCode());
result = prime * result + ((getConfigKey() == null) ? 0 : getConfigKey().hashCode());
result = prime * result + ((getConfigValue() == null) ? 0 : getConfigValue().hashCode());
result = prime * result + ((getRegex() == null) ? 0 : getRegex().hashCode());
result = prime * result + ((getHint() == null) ? 0 : getHint().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_SysConfig))
return false;
else {MPAY_SysConfig other = (MPAY_SysConfig) obj;
return this.hashCode() == other.hashCode();}
}


}