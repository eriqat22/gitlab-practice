package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_Customer_Trans_Rep")
@XmlRootElement(name="Customer_Trans_Rep")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Customer_Trans_Rep extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Customer_Trans_Rep(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String EXTRACT_STAMP = "extractStamp";
@Column(name="EXTRACTSTAMP", nullable=true, length=250)
private String extractStamp;
public String getExtractStamp(){
return this.extractStamp;
}
public void setExtractStamp(String extractStamp){
this.extractStamp = extractStamp;
}

public static final String TRANSACTIONREFERENCE = "transactionreference";
@Column(name="TRANSACTIONREFERENCE", nullable=true, length=250)
private String transactionreference;
public String getTransactionreference(){
return this.transactionreference;
}
public void setTransactionreference(String transactionreference){
this.transactionreference = transactionreference;
}

public static final String TRN_DATE = "trnDate";
@Column(name="TRNDATE", nullable=true, length=250)
private String trnDate;
public String getTrnDate(){
return this.trnDate;
}
public void setTrnDate(String trnDate){
this.trnDate = trnDate;
}

public static final String TRN_TIME = "trnTime";
@Column(name="TRNTIME", nullable=true, length=250)
private String trnTime;
public String getTrnTime(){
return this.trnTime;
}
public void setTrnTime(String trnTime){
this.trnTime = trnTime;
}

public static final String TRN_STATUS = "trnStatus";
@Column(name="TRNSTATUS", nullable=true, length=250)
private String trnStatus;
public String getTrnStatus(){
return this.trnStatus;
}
public void setTrnStatus(String trnStatus){
this.trnStatus = trnStatus;
}

public static final String TRNSACTION_SOURCE = "trnsactionSource";
@Column(name="TRNSACTIONSOURCE", nullable=true, length=250)
private String trnsactionSource;
public String getTrnsactionSource(){
return this.trnsactionSource;
}
public void setTrnsactionSource(String trnsactionSource){
this.trnsactionSource = trnsactionSource;
}

public static final String TRN_TYPE = "trnType";
@Column(name="TRNTYPE", nullable=true, length=250)
private String trnType;
public String getTrnType(){
return this.trnType;
}
public void setTrnType(String trnType){
this.trnType = trnType;
}

public static final String TRN_C_C_Y = "trnCCY";
@Column(name="TRNCCY", nullable=true, length=250)
private String trnCCY;
public String getTrnCCY(){
return this.trnCCY;
}
public void setTrnCCY(String trnCCY){
this.trnCCY = trnCCY;
}

public static final String TRN_AMOUNT = "trnAmount";
@Column(name="TRNAMOUNT", nullable=true, length=250)
private String trnAmount;
public String getTrnAmount(){
return this.trnAmount;
}
public void setTrnAmount(String trnAmount){
this.trnAmount = trnAmount;
}

public static final String SENDER_I_B_A_N = "senderIBAN";
@Column(name="SENDERIBAN", nullable=true, length=250)
private String senderIBAN;
public String getSenderIBAN(){
return this.senderIBAN;
}
public void setSenderIBAN(String senderIBAN){
this.senderIBAN = senderIBAN;
}

public static final String SENDER_N_I_N = "senderNIN";
@Column(name="SENDERNIN", nullable=true, length=250)
private String senderNIN;
public String getSenderNIN(){
return this.senderNIN;
}
public void setSenderNIN(String senderNIN){
this.senderNIN = senderNIN;
}

public static final String SENDER_NAME = "senderName";
@Column(name="SENDERNAME", nullable=true, length=250)
private String senderName;
public String getSenderName(){
return this.senderName;
}
public void setSenderName(String senderName){
this.senderName = senderName;
}

public static final String SENDER_BANK_ACCOUNT_NUMBER = "senderBankAccountNumber";
@Column(name="SENDERBANKACCOUNTNUMBER", nullable=true, length=250)
private String senderBankAccountNumber;
public String getSenderBankAccountNumber(){
return this.senderBankAccountNumber;
}
public void setSenderBankAccountNumber(String senderBankAccountNumber){
this.senderBankAccountNumber = senderBankAccountNumber;
}

public static final String SENDER_ACCOUNT_NUMBER = "senderAccountNumber";
@Column(name="SENDERACCOUNTNUMBER", nullable=true, length=250)
private String senderAccountNumber;
public String getSenderAccountNumber(){
return this.senderAccountNumber;
}
public void setSenderAccountNumber(String senderAccountNumber){
this.senderAccountNumber = senderAccountNumber;
}

public static final String SENDER_MOBILE_NUMBER = "senderMobileNumber";
@Column(name="SENDERMOBILENUMBER", nullable=true, length=250)
private String senderMobileNumber;
public String getSenderMobileNumber(){
return this.senderMobileNumber;
}
public void setSenderMobileNumber(String senderMobileNumber){
this.senderMobileNumber = senderMobileNumber;
}

public static final String SENDER_CHARGES_AMOUNT = "senderChargesAmount";
@Column(name="SENDERCHARGESAMOUNT", nullable=true, length=250)
private String senderChargesAmount;
public String getSenderChargesAmount(){
return this.senderChargesAmount;
}
public void setSenderChargesAmount(String senderChargesAmount){
this.senderChargesAmount = senderChargesAmount;
}

public static final String SENDER_AMOUNT = "senderAmount";
@Column(name="SENDERAMOUNT", nullable=true, length=250)
private String senderAmount;
public String getSenderAmount(){
return this.senderAmount;
}
public void setSenderAmount(String senderAmount){
this.senderAmount = senderAmount;
}

public static final String SENDER_BALANCE_BEFORE = "senderBalanceBefore";
@Column(name="SENDERBALANCEBEFORE", nullable=true, length=250)
private String senderBalanceBefore;
public String getSenderBalanceBefore(){
return this.senderBalanceBefore;
}
public void setSenderBalanceBefore(String senderBalanceBefore){
this.senderBalanceBefore = senderBalanceBefore;
}

public static final String SENDER_BALANCE_AFTER = "senderBalanceAfter";
@Column(name="SENDERBALANCEAFTER", nullable=true, length=250)
private String senderBalanceAfter;
public String getSenderBalanceAfter(){
return this.senderBalanceAfter;
}
public void setSenderBalanceAfter(String senderBalanceAfter){
this.senderBalanceAfter = senderBalanceAfter;
}

public static final String SENDER_BANK = "senderBank";
@Column(name="SENDERBANK", nullable=true, length=250)
private String senderBank;
public String getSenderBank(){
return this.senderBank;
}
public void setSenderBank(String senderBank){
this.senderBank = senderBank;
}

public static final String SENDER_V_A_T_AMOUNT = "senderVATAmount";
@Column(name="SENDERVATAMOUNT", nullable=true, length=250)
private String senderVATAmount;
public String getSenderVATAmount(){
return this.senderVATAmount;
}
public void setSenderVATAmount(String senderVATAmount){
this.senderVATAmount = senderVATAmount;
}

public static final String RECEIVER_I_B_A_N = "receiverIBAN";
@Column(name="RECEIVERIBAN", nullable=true, length=250)
private String receiverIBAN;
public String getReceiverIBAN(){
return this.receiverIBAN;
}
public void setReceiverIBAN(String receiverIBAN){
this.receiverIBAN = receiverIBAN;
}

public static final String RECEIVER_NAME = "receiverName";
@Column(name="RECEIVERNAME", nullable=true, length=250)
private String receiverName;
public String getReceiverName(){
return this.receiverName;
}
public void setReceiverName(String receiverName){
this.receiverName = receiverName;
}

public static final String RECEIVER_N_I_N = "receiverNIN";
@Column(name="RECEIVERNIN", nullable=true, length=250)
private String receiverNIN;
public String getReceiverNIN(){
return this.receiverNIN;
}
public void setReceiverNIN(String receiverNIN){
this.receiverNIN = receiverNIN;
}

public static final String RECEIVER_BANK_ACCOUNT_NUMBER = "receiverBankAccountNumber";
@Column(name="RECEIVERBANKACCOUNTNUMBER", nullable=true, length=250)
private String receiverBankAccountNumber;
public String getReceiverBankAccountNumber(){
return this.receiverBankAccountNumber;
}
public void setReceiverBankAccountNumber(String receiverBankAccountNumber){
this.receiverBankAccountNumber = receiverBankAccountNumber;
}

public static final String RECEIVER_ACCOUNT_NUMBER = "receiverAccountNumber";
@Column(name="RECEIVERACCOUNTNUMBER", nullable=true, length=250)
private String receiverAccountNumber;
public String getReceiverAccountNumber(){
return this.receiverAccountNumber;
}
public void setReceiverAccountNumber(String receiverAccountNumber){
this.receiverAccountNumber = receiverAccountNumber;
}

public static final String RECEIVER_MOBILE_NUMBER = "receiverMobileNumber";
@Column(name="RECEIVERMOBILENUMBER", nullable=true, length=250)
private String receiverMobileNumber;
public String getReceiverMobileNumber(){
return this.receiverMobileNumber;
}
public void setReceiverMobileNumber(String receiverMobileNumber){
this.receiverMobileNumber = receiverMobileNumber;
}

public static final String RECEIVER_CHARGES_AMOUNT = "receiverChargesAmount";
@Column(name="RECEIVERCHARGESAMOUNT", nullable=true, length=250)
private String receiverChargesAmount;
public String getReceiverChargesAmount(){
return this.receiverChargesAmount;
}
public void setReceiverChargesAmount(String receiverChargesAmount){
this.receiverChargesAmount = receiverChargesAmount;
}

public static final String RECEIVER_BANK = "receiverBank";
@Column(name="RECEIVERBANK", nullable=true, length=250)
private String receiverBank;
public String getReceiverBank(){
return this.receiverBank;
}
public void setReceiverBank(String receiverBank){
this.receiverBank = receiverBank;
}

public static final String RECEIVER_V_A_T_AMOUNT = "receiverVATAmount";
@Column(name="RECEIVERVATAMOUNT", nullable=true, length=250)
private String receiverVATAmount;
public String getReceiverVATAmount(){
return this.receiverVATAmount;
}
public void setReceiverVATAmount(String receiverVATAmount){
this.receiverVATAmount = receiverVATAmount;
}

public static final String RECEIVER_NET_AMOUNT = "receiverNetAmount";
@Column(name="RECEIVERNETAMOUNT", nullable=true, length=250)
private String receiverNetAmount;
public String getReceiverNetAmount(){
return this.receiverNetAmount;
}
public void setReceiverNetAmount(String receiverNetAmount){
this.receiverNetAmount = receiverNetAmount;
}

public static final String RECEIVER_BALANCE_BEFORE = "receiverBalanceBefore";
@Column(name="RECEIVERBALANCEBEFORE", nullable=true, length=250)
private String receiverBalanceBefore;
public String getReceiverBalanceBefore(){
return this.receiverBalanceBefore;
}
public void setReceiverBalanceBefore(String receiverBalanceBefore){
this.receiverBalanceBefore = receiverBalanceBefore;
}

public static final String RECEIVER_BALANCE_AFTER = "receiverBalanceAfter";
@Column(name="RECEIVERBALANCEAFTER", nullable=true, length=250)
private String receiverBalanceAfter;
public String getReceiverBalanceAfter(){
return this.receiverBalanceAfter;
}
public void setReceiverBalanceAfter(String receiverBalanceAfter){
this.receiverBalanceAfter = receiverBalanceAfter;
}

public static final String MERCHANTNAME = "merchantname";
@Column(name="MERCHANTNAME", nullable=true, length=250)
private String merchantname;
public String getMerchantname(){
return this.merchantname;
}
public void setMerchantname(String merchantname){
this.merchantname = merchantname;
}

@Override
public String toString() {
return "MPAY_Customer_Trans_Rep [id= " + getId() + ", extractStamp= " + getExtractStamp() + ", transactionreference= " + getTransactionreference() + ", trnDate= " + getTrnDate() + ", trnTime= " + getTrnTime() + ", trnStatus= " + getTrnStatus() + ", trnsactionSource= " + getTrnsactionSource() + ", trnType= " + getTrnType() + ", trnCCY= " + getTrnCCY() + ", trnAmount= " + getTrnAmount() + ", senderIBAN= " + getSenderIBAN() + ", senderNIN= " + getSenderNIN() + ", senderName= " + getSenderName() + ", senderBankAccountNumber= " + getSenderBankAccountNumber() + ", senderAccountNumber= " + getSenderAccountNumber() + ", senderMobileNumber= " + getSenderMobileNumber() + ", senderChargesAmount= " + getSenderChargesAmount() + ", senderAmount= " + getSenderAmount() + ", senderBalanceBefore= " + getSenderBalanceBefore() + ", senderBalanceAfter= " + getSenderBalanceAfter() + ", senderBank= " + getSenderBank() + ", senderVATAmount= " + getSenderVATAmount() + ", receiverIBAN= " + getReceiverIBAN() + ", receiverName= " + getReceiverName() + ", receiverNIN= " + getReceiverNIN() + ", receiverBankAccountNumber= " + getReceiverBankAccountNumber() + ", receiverAccountNumber= " + getReceiverAccountNumber() + ", receiverMobileNumber= " + getReceiverMobileNumber() + ", receiverChargesAmount= " + getReceiverChargesAmount() + ", receiverBank= " + getReceiverBank() + ", receiverVATAmount= " + getReceiverVATAmount() + ", receiverNetAmount= " + getReceiverNetAmount() + ", receiverBalanceBefore= " + getReceiverBalanceBefore() + ", receiverBalanceAfter= " + getReceiverBalanceAfter() + ", merchantname= " + getMerchantname() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getExtractStamp() == null) ? 0 : getExtractStamp().hashCode());
result = prime * result + ((getTransactionreference() == null) ? 0 : getTransactionreference().hashCode());
result = prime * result + ((getTrnDate() == null) ? 0 : getTrnDate().hashCode());
result = prime * result + ((getTrnTime() == null) ? 0 : getTrnTime().hashCode());
result = prime * result + ((getTrnStatus() == null) ? 0 : getTrnStatus().hashCode());
result = prime * result + ((getTrnsactionSource() == null) ? 0 : getTrnsactionSource().hashCode());
result = prime * result + ((getTrnType() == null) ? 0 : getTrnType().hashCode());
result = prime * result + ((getTrnCCY() == null) ? 0 : getTrnCCY().hashCode());
result = prime * result + ((getTrnAmount() == null) ? 0 : getTrnAmount().hashCode());
result = prime * result + ((getSenderIBAN() == null) ? 0 : getSenderIBAN().hashCode());
result = prime * result + ((getSenderNIN() == null) ? 0 : getSenderNIN().hashCode());
result = prime * result + ((getSenderName() == null) ? 0 : getSenderName().hashCode());
result = prime * result + ((getSenderBankAccountNumber() == null) ? 0 : getSenderBankAccountNumber().hashCode());
result = prime * result + ((getSenderAccountNumber() == null) ? 0 : getSenderAccountNumber().hashCode());
result = prime * result + ((getSenderMobileNumber() == null) ? 0 : getSenderMobileNumber().hashCode());
result = prime * result + ((getSenderChargesAmount() == null) ? 0 : getSenderChargesAmount().hashCode());
result = prime * result + ((getSenderAmount() == null) ? 0 : getSenderAmount().hashCode());
result = prime * result + ((getSenderBalanceBefore() == null) ? 0 : getSenderBalanceBefore().hashCode());
result = prime * result + ((getSenderBalanceAfter() == null) ? 0 : getSenderBalanceAfter().hashCode());
result = prime * result + ((getSenderBank() == null) ? 0 : getSenderBank().hashCode());
result = prime * result + ((getSenderVATAmount() == null) ? 0 : getSenderVATAmount().hashCode());
result = prime * result + ((getReceiverIBAN() == null) ? 0 : getReceiverIBAN().hashCode());
result = prime * result + ((getReceiverName() == null) ? 0 : getReceiverName().hashCode());
result = prime * result + ((getReceiverNIN() == null) ? 0 : getReceiverNIN().hashCode());
result = prime * result + ((getReceiverBankAccountNumber() == null) ? 0 : getReceiverBankAccountNumber().hashCode());
result = prime * result + ((getReceiverAccountNumber() == null) ? 0 : getReceiverAccountNumber().hashCode());
result = prime * result + ((getReceiverMobileNumber() == null) ? 0 : getReceiverMobileNumber().hashCode());
result = prime * result + ((getReceiverChargesAmount() == null) ? 0 : getReceiverChargesAmount().hashCode());
result = prime * result + ((getReceiverBank() == null) ? 0 : getReceiverBank().hashCode());
result = prime * result + ((getReceiverVATAmount() == null) ? 0 : getReceiverVATAmount().hashCode());
result = prime * result + ((getReceiverNetAmount() == null) ? 0 : getReceiverNetAmount().hashCode());
result = prime * result + ((getReceiverBalanceBefore() == null) ? 0 : getReceiverBalanceBefore().hashCode());
result = prime * result + ((getReceiverBalanceAfter() == null) ? 0 : getReceiverBalanceAfter().hashCode());
result = prime * result + ((getMerchantname() == null) ? 0 : getMerchantname().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Customer_Trans_Rep))
return false;
else {MPAY_Customer_Trans_Rep other = (MPAY_Customer_Trans_Rep) obj;
return this.hashCode() == other.hashCode();}
}


}