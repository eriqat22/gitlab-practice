package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_SystemAccountsCashOut")
@XmlRootElement(name="SystemAccountsCashOut")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_SystemAccountsCashOut extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_SystemAccountsCashOut(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String SOURCE_SERVICE_BALANCE = "sourceServiceBalance";
@Column(name="SOURCESERVICEBALANCE", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal sourceServiceBalance;
public java.math.BigDecimal getSourceServiceBalance(){
return this.sourceServiceBalance;
}
public void setSourceServiceBalance(java.math.BigDecimal sourceServiceBalance){
this.sourceServiceBalance = sourceServiceBalance;
}

public static final String DESTINATION_SERVICE_BALANCE = "destinationServiceBalance";
@Column(name="DESTINATIONSERVICEBALANCE", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal destinationServiceBalance;
public java.math.BigDecimal getDestinationServiceBalance(){
return this.destinationServiceBalance;
}
public void setDestinationServiceBalance(java.math.BigDecimal destinationServiceBalance){
this.destinationServiceBalance = destinationServiceBalance;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String COMMENTS = "comments";
@Column(name="COMMENTS", nullable=true, length=255)
private String comments;
public String getComments(){
return this.comments;
}
public void setComments(String comments){
this.comments = comments;
}

public static final String SOURCE_SERVICE = "sourceService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SOURCESERVICEID", nullable=false)
private MPAY_CorpoarteService sourceService;
public MPAY_CorpoarteService getSourceService(){
return this.sourceService;
}
public void setSourceService(MPAY_CorpoarteService sourceService){
this.sourceService = sourceService;
}

public static final String DESTINATION_SERVICE = "destinationService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="DESTINATIONSERVICEID", nullable=true)
private MPAY_CorpoarteService destinationService;
public MPAY_CorpoarteService getDestinationService(){
return this.destinationService;
}
public void setDestinationService(MPAY_CorpoarteService destinationService){
this.destinationService = destinationService;
}

public static final String REF_TRANSACTION = "refTransaction";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTRANSACTIONID", nullable=true)
private MPAY_Transaction refTransaction;
public MPAY_Transaction getRefTransaction(){
return this.refTransaction;
}
public void setRefTransaction(MPAY_Transaction refTransaction){
this.refTransaction = refTransaction;
}

public static final String PROCESSING_STATUS = "processingStatus";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PROCESSINGSTATUSID", nullable=true)
private MPAY_ProcessingStatus processingStatus;
public MPAY_ProcessingStatus getProcessingStatus(){
return this.processingStatus;
}
public void setProcessingStatus(MPAY_ProcessingStatus processingStatus){
this.processingStatus = processingStatus;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=true)
private MPAY_Reason reason;
public MPAY_Reason getReason(){
return this.reason;
}
public void setReason(MPAY_Reason reason){
this.reason = reason;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_SystemAccountsCashOut [id= " + getId() + ", sourceServiceBalance= " + getSourceServiceBalance() + ", destinationServiceBalance= " + getDestinationServiceBalance() + ", amount= " + getAmount() + ", comments= " + getComments() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_SystemAccountsCashOut))
return false;
else {MPAY_SystemAccountsCashOut other = (MPAY_SystemAccountsCashOut) obj;
return this.hashCode() == other.hashCode();}
}


}