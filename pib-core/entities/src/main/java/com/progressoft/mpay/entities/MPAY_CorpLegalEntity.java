package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_CorpLegalEntities",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="CorpLegalEntities")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CorpLegalEntity extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CorpLegalEntity(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CORP_LEGAL_ENTITIES_N_L_S = "corpLegalEntitiesNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "legalEntityId")
@MapKey(name="languageCode")
private Map<String, MPAY_CorpLegalEntities_NLS> corpLegalEntitiesNLS;
public Map<String, MPAY_CorpLegalEntities_NLS> getCorpLegalEntitiesNLS(){
return this.corpLegalEntitiesNLS;
}
public void setCorpLegalEntitiesNLS(Map<String, MPAY_CorpLegalEntities_NLS> corpLegalEntitiesNLS){
this.corpLegalEntitiesNLS = corpLegalEntitiesNLS;
}

@Override
public String toString() {
return "MPAY_CorpLegalEntity [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CorpLegalEntity))
return false;
else {MPAY_CorpLegalEntity other = (MPAY_CorpLegalEntity) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.corpLegalEntitiesNLS;
}
}