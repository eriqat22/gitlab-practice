package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_Transactions",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"REFERENCE","Z_TENANT_ID"})
})
@XmlRootElement(name="Transactions")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Transaction extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Transaction(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String COMMENTS = "comments";
@Column(name="COMMENTS", nullable=true, length=255)
private String comments;
public String getComments(){
return this.comments;
}
public void setComments(String comments){
this.comments = comments;
}

public static final String REFERENCE = "reference";
@Column(name="REFERENCE", nullable=true, length=1000)
private String reference;
public String getReference(){
return this.reference;
}
public void setReference(String reference){
this.reference = reference;
}

public static final String EXTERNAL_REFERENCE = "externalReference";
@Column(name="EXTERNALREFERENCE", nullable=true, length=250)
private String externalReference;
public String getExternalReference(){
return this.externalReference;
}
public void setExternalReference(String externalReference){
this.externalReference = externalReference;
}

public static final String TRANS_DATE = "transDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="TRANSDATE", nullable=false, length=12)
private java.sql.Timestamp transDate;
public java.sql.Timestamp getTransDate(){
return this.transDate;
}
public void setTransDate(java.sql.Timestamp transDate){
this.transDate = transDate;
}

public static final String SENDER_INFO = "senderInfo";
@Column(name="SENDERINFO", nullable=true, length=1000)
private String senderInfo;
public String getSenderInfo(){
return this.senderInfo;
}
public void setSenderInfo(String senderInfo){
this.senderInfo = senderInfo;
}

public static final String RECEIVER_INFO = "receiverInfo";
@Column(name="RECEIVERINFO", nullable=true, length=1000)
private String receiverInfo;
public String getReceiverInfo(){
return this.receiverInfo;
}
public void setReceiverInfo(String receiverInfo){
this.receiverInfo = receiverInfo;
}

public static final String TOTAL_CHARGE = "totalCharge";
@Column(name="TOTALCHARGE", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal totalCharge;
public java.math.BigDecimal getTotalCharge(){
return this.totalCharge;
}
public void setTotalCharge(java.math.BigDecimal totalCharge){
this.totalCharge = totalCharge;
}

public static final String ORIGINAL_AMOUNT = "originalAmount";
@Column(name="ORIGINALAMOUNT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal originalAmount;
public java.math.BigDecimal getOriginalAmount(){
return this.originalAmount;
}
public void setOriginalAmount(java.math.BigDecimal originalAmount){
this.originalAmount = originalAmount;
}

public static final String TOTAL_AMOUNT = "totalAmount";
@Column(name="TOTALAMOUNT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal totalAmount;
public java.math.BigDecimal getTotalAmount(){
return this.totalAmount;
}
public void setTotalAmount(java.math.BigDecimal totalAmount){
this.totalAmount = totalAmount;
}

public static final String SENDER_CHARGE = "senderCharge";
@Column(name="SENDERCHARGE", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal senderCharge;
public java.math.BigDecimal getSenderCharge(){
return this.senderCharge;
}
public void setSenderCharge(java.math.BigDecimal senderCharge){
this.senderCharge = senderCharge;
}

public static final String SENDER_TAX = "senderTax";
@Column(name="SENDERTAX", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal senderTax;
public java.math.BigDecimal getSenderTax(){
return this.senderTax;
}
public void setSenderTax(java.math.BigDecimal senderTax){
this.senderTax = senderTax;
}

public static final String RECEIVER_CHARGE = "receiverCharge";
@Column(name="RECEIVERCHARGE", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal receiverCharge;
public java.math.BigDecimal getReceiverCharge(){
return this.receiverCharge;
}
public void setReceiverCharge(java.math.BigDecimal receiverCharge){
this.receiverCharge = receiverCharge;
}

public static final String RECEIVER_TAX = "receiverTax";
@Column(name="RECEIVERTAX", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal receiverTax;
public java.math.BigDecimal getReceiverTax(){
return this.receiverTax;
}
public void setReceiverTax(java.math.BigDecimal receiverTax){
this.receiverTax = receiverTax;
}

public static final String REASON_DESC = "reasonDesc";
@Column(name="REASONDESC", nullable=true, length=4000)
private String reasonDesc;
public String getReasonDesc(){
return this.reasonDesc;
}
public void setReasonDesc(String reasonDesc){
this.reasonDesc = reasonDesc;
}

public static final String IS_REVERSED = "isReversed";
@Column(name="ISREVERSED", nullable=false, length=1)
private Boolean isReversed;
public Boolean getIsReversed(){
return this.isReversed;
}
public void setIsReversed(Boolean isReversed){
this.isReversed = isReversed;
}

public static final String REVERSED_BY = "reversedBy";
@Column(name="REVERSEDBY", nullable=true, length=1000)
private String reversedBy;
public String getReversedBy(){
return this.reversedBy;
}
public void setReversedBy(String reversedBy){
this.reversedBy = reversedBy;
}

public static final String REVERSAL_REASON = "reversalReason";
@Column(name="REVERSALREASON", nullable=true, length=1000)
private String reversalReason;
public String getReversalReason(){
return this.reversalReason;
}
public void setReversalReason(String reversalReason){
this.reversalReason = reversalReason;
}

public static final String EXTERNAL_FEES = "externalFees";
@Column(name="EXTERNALFEES", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal externalFees;
public java.math.BigDecimal getExternalFees(){
return this.externalFees;
}
public void setExternalFees(java.math.BigDecimal externalFees){
this.externalFees = externalFees;
}

public static final String REVERSABLE = "reversable";
@Column(name="REVERSABLE", nullable=true, length=7)
private Boolean reversable;
public Boolean getReversable(){
return this.reversable;
}
public void setReversable(Boolean reversable){
this.reversable = reversable;
}

public static final String REQUESTED_ID = "requestedId";
@Column(name="REQUESTEDID", nullable=true, length=1000)
private String requestedId;
public String getRequestedId(){
return this.requestedId;
}
public void setRequestedId(String requestedId){
this.requestedId = requestedId;
}

public static final String SHOP_ID = "shopId";
@Column(name="SHOPID", nullable=true, length=1000)
private String shopId;
public String getShopId(){
return this.shopId;
}
public void setShopId(String shopId){
this.shopId = shopId;
}

public static final String CHANNEL_ID = "channelId";
@Column(name="CHANNELID", nullable=true, length=1000)
private String channelId;
public String getChannelId(){
return this.channelId;
}
public void setChannelId(String channelId){
this.channelId = channelId;
}

public static final String WALLET_ID = "walletId";
@Column(name="WALLETID", nullable=true, length=1000)
private String walletId;
public String getWalletId(){
return this.walletId;
}
public void setWalletId(String walletId){
this.walletId = walletId;
}

public static final String EXPIRY_DATE = "expiryDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="EXPIRYDATE", nullable=true, length=12)
private java.sql.Timestamp expiryDate;
public java.sql.Timestamp getExpiryDate(){
return this.expiryDate;
}
public void setExpiryDate(java.sql.Timestamp expiryDate){
this.expiryDate = expiryDate;
}

public static final String REF_MESSAGE = "refMessage";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMESSAGEID", nullable=true)
private MPAY_MPayMessage refMessage;
public MPAY_MPayMessage getRefMessage(){
return this.refMessage;
}
public void setRefMessage(MPAY_MPayMessage refMessage){
this.refMessage = refMessage;
}

public static final String DIRECTION = "direction";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="DIRECTIONID", nullable=true)
private MPAY_TransactionDirection direction;
public MPAY_TransactionDirection getDirection(){
return this.direction;
}
public void setDirection(MPAY_TransactionDirection direction){
this.direction = direction;
}

public static final String REF_TYPE = "refType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTYPEID", nullable=false)
private MPAY_TransactionType refType;
public MPAY_TransactionType getRefType(){
return this.refType;
}
public void setRefType(MPAY_TransactionType refType){
this.refType = refType;
}

public static final String SENDER_MOBILE = "senderMobile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SENDERMOBILEID", nullable=true)
private MPAY_CustomerMobile senderMobile;
public MPAY_CustomerMobile getSenderMobile(){
return this.senderMobile;
}
public void setSenderMobile(MPAY_CustomerMobile senderMobile){
this.senderMobile = senderMobile;
}

public static final String SENDER_MOBILE_ACCOUNT = "senderMobileAccount";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SENDERMOBILEACCOUNTID", nullable=true)
private MPAY_MobileAccount senderMobileAccount;
public MPAY_MobileAccount getSenderMobileAccount(){
return this.senderMobileAccount;
}
public void setSenderMobileAccount(MPAY_MobileAccount senderMobileAccount){
this.senderMobileAccount = senderMobileAccount;
}

public static final String SENDER_SERVICE = "senderService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SENDERSERVICEID", nullable=true)
private MPAY_CorpoarteService senderService;
public MPAY_CorpoarteService getSenderService(){
return this.senderService;
}
public void setSenderService(MPAY_CorpoarteService senderService){
this.senderService = senderService;
}

public static final String SENDER_SERVICE_ACCOUNT = "senderServiceAccount";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SENDERSERVICEACCOUNTID", nullable=true)
private MPAY_ServiceAccount senderServiceAccount;
public MPAY_ServiceAccount getSenderServiceAccount(){
return this.senderServiceAccount;
}
public void setSenderServiceAccount(MPAY_ServiceAccount senderServiceAccount){
this.senderServiceAccount = senderServiceAccount;
}

public static final String RECEIVER_MOBILE = "receiverMobile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="RECEIVERMOBILEID", nullable=true)
private MPAY_CustomerMobile receiverMobile;
public MPAY_CustomerMobile getReceiverMobile(){
return this.receiverMobile;
}
public void setReceiverMobile(MPAY_CustomerMobile receiverMobile){
this.receiverMobile = receiverMobile;
}

public static final String RECEIVER_MOBILE_ACCOUNT = "receiverMobileAccount";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="RECEIVERMOBILEACCOUNTID", nullable=true)
private MPAY_MobileAccount receiverMobileAccount;
public MPAY_MobileAccount getReceiverMobileAccount(){
return this.receiverMobileAccount;
}
public void setReceiverMobileAccount(MPAY_MobileAccount receiverMobileAccount){
this.receiverMobileAccount = receiverMobileAccount;
}

public static final String RECEIVER_SERVICE = "receiverService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="RECEIVERSERVICEID", nullable=true)
private MPAY_CorpoarteService receiverService;
public MPAY_CorpoarteService getReceiverService(){
return this.receiverService;
}
public void setReceiverService(MPAY_CorpoarteService receiverService){
this.receiverService = receiverService;
}

public static final String RECEIVER_SERVICE_ACCOUNT = "receiverServiceAccount";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="RECEIVERSERVICEACCOUNTID", nullable=true)
private MPAY_ServiceAccount receiverServiceAccount;
public MPAY_ServiceAccount getReceiverServiceAccount(){
return this.receiverServiceAccount;
}
public void setReceiverServiceAccount(MPAY_ServiceAccount receiverServiceAccount){
this.receiverServiceAccount = receiverServiceAccount;
}

public static final String SENDER_BANK = "senderBank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SENDERBANKID", nullable=true)
private MPAY_Bank senderBank;
public MPAY_Bank getSenderBank(){
return this.senderBank;
}
public void setSenderBank(MPAY_Bank senderBank){
this.senderBank = senderBank;
}

public static final String RECEIVER_BANK = "receiverBank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="RECEIVERBANKID", nullable=true)
private MPAY_Bank receiverBank;
public MPAY_Bank getReceiverBank(){
return this.receiverBank;
}
public void setReceiverBank(MPAY_Bank receiverBank){
this.receiverBank = receiverBank;
}

public static final String PROCESSING_STATUS = "processingStatus";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PROCESSINGSTATUSID", nullable=false)
private MPAY_ProcessingStatus processingStatus;
public MPAY_ProcessingStatus getProcessingStatus(){
return this.processingStatus;
}
public void setProcessingStatus(MPAY_ProcessingStatus processingStatus){
this.processingStatus = processingStatus;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=false)
private MPAY_Reason reason;
public MPAY_Reason getReason(){
return this.reason;
}
public void setReason(MPAY_Reason reason){
this.reason = reason;
}

public static final String REF_OPERATION = "refOperation";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFOPERATIONID", nullable=true)
private MPAY_EndPointOperation refOperation;
public MPAY_EndPointOperation getRefOperation(){
return this.refOperation;
}
public void setRefOperation(MPAY_EndPointOperation refOperation){
this.refOperation = refOperation;
}

public static final String INWARD_MESSAGE = "inwardMessage";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="INWARDMESSAGEID", nullable=true)
private MPAY_MpClearIntegMsgLog inwardMessage;
public MPAY_MpClearIntegMsgLog getInwardMessage(){
return this.inwardMessage;
}
public void setInwardMessage(MPAY_MpClearIntegMsgLog inwardMessage){
this.inwardMessage = inwardMessage;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

public static final String REF_TRANSACTION_SYSTEM_ACCOUNTS_CASH_OUT = "refTransactionSystemAccountsCashOut";
@OneToMany(mappedBy = "refTransaction")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_SystemAccountsCashOut> refTransactionSystemAccountsCashOut;
public List<MPAY_SystemAccountsCashOut> getRefTransactionSystemAccountsCashOut(){
return this.refTransactionSystemAccountsCashOut;
}
public void setRefTransactionSystemAccountsCashOut(List<MPAY_SystemAccountsCashOut> refTransactionSystemAccountsCashOut){
this.refTransactionSystemAccountsCashOut = refTransactionSystemAccountsCashOut;
}

public static final String REF_TRSANSACTION_JV_DETAILS = "refTrsansactionJvDetails";
@OneToMany(mappedBy = "refTrsansaction")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_JvDetail> refTrsansactionJvDetails;
public List<MPAY_JvDetail> getRefTrsansactionJvDetails(){
return this.refTrsansactionJvDetails;
}
public void setRefTrsansactionJvDetails(List<MPAY_JvDetail> refTrsansactionJvDetails){
this.refTrsansactionJvDetails = refTrsansactionJvDetails;
}

public static final String REF_TRANSACTION_BANK_INTEG_MESSAGES = "refTransactionBankIntegMessages";
@OneToMany(mappedBy = "refTransaction")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_BankIntegMessage> refTransactionBankIntegMessages;
public List<MPAY_BankIntegMessage> getRefTransactionBankIntegMessages(){
return this.refTransactionBankIntegMessages;
}
public void setRefTransactionBankIntegMessages(List<MPAY_BankIntegMessage> refTransactionBankIntegMessages){
this.refTransactionBankIntegMessages = refTransactionBankIntegMessages;
}

@Override
public String toString() {
return "MPAY_Transaction [id= " + getId() + ", comments= " + getComments() + ", reference= " + getReference() + ", externalReference= " + getExternalReference() + ", transDate= " + getTransDate() + ", senderInfo= " + getSenderInfo() + ", receiverInfo= " + getReceiverInfo() + ", totalCharge= " + getTotalCharge() + ", originalAmount= " + getOriginalAmount() + ", totalAmount= " + getTotalAmount() + ", senderCharge= " + getSenderCharge() + ", senderTax= " + getSenderTax() + ", receiverCharge= " + getReceiverCharge() + ", receiverTax= " + getReceiverTax() + ", reasonDesc= " + getReasonDesc() + ", isReversed= " + getIsReversed() + ", reversedBy= " + getReversedBy() + ", reversalReason= " + getReversalReason() + ", externalFees= " + getExternalFees() + ", reversable= " + getReversable() + ", requestedId= " + getRequestedId() + ", shopId= " + getShopId() + ", channelId= " + getChannelId() + ", walletId= " + getWalletId() + ", expiryDate= " + getExpiryDate() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getComments() == null) ? 0 : getComments().hashCode());
result = prime * result + ((getReference() == null) ? 0 : getReference().hashCode());
result = prime * result + ((getExternalReference() == null) ? 0 : getExternalReference().hashCode());
result = prime * result + ((getSenderInfo() == null) ? 0 : getSenderInfo().hashCode());
result = prime * result + ((getReceiverInfo() == null) ? 0 : getReceiverInfo().hashCode());
result = prime * result + ((getReversedBy() == null) ? 0 : getReversedBy().hashCode());
result = prime * result + ((getRequestedId() == null) ? 0 : getRequestedId().hashCode());
result = prime * result + ((getShopId() == null) ? 0 : getShopId().hashCode());
result = prime * result + ((getChannelId() == null) ? 0 : getChannelId().hashCode());
result = prime * result + ((getWalletId() == null) ? 0 : getWalletId().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Transaction))
return false;
else {MPAY_Transaction other = (MPAY_Transaction) obj;
return this.hashCode() == other.hashCode();}
}


}