package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_JvDetails")
@XmlRootElement(name="JvDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_JvDetail extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_JvDetail(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String SERIAL = "serial";
@Column(name="SERIAL", nullable=true, length=4)
private Long serial;
public Long getSerial(){
return this.serial;
}
public void setSerial(Long serial){
this.serial = serial;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String DEBIT_CREDIT_FLAG = "debitCreditFlag";
@Column(name="DEBITCREDITFLAG", nullable=false, length=1)
private String debitCreditFlag;
public String getDebitCreditFlag(){
return this.debitCreditFlag;
}
public void setDebitCreditFlag(String debitCreditFlag){
this.debitCreditFlag = debitCreditFlag;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String IS_POSTED = "isPosted";
@Column(name="ISPOSTED", nullable=false, length=1)
private Boolean isPosted;
public Boolean getIsPosted(){
return this.isPosted;
}
public void setIsPosted(Boolean isPosted){
this.isPosted = isPosted;
}

public static final String BALANCE_BEFORE = "balanceBefore";
@Column(name="BALANCEBEFORE", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal balanceBefore;
public java.math.BigDecimal getBalanceBefore(){
return this.balanceBefore;
}
public void setBalanceBefore(java.math.BigDecimal balanceBefore){
this.balanceBefore = balanceBefore;
}

public static final String BALANCE_AFTER = "balanceAfter";
@Column(name="BALANCEAFTER", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal balanceAfter;
public java.math.BigDecimal getBalanceAfter(){
return this.balanceAfter;
}
public void setBalanceAfter(java.math.BigDecimal balanceAfter){
this.balanceAfter = balanceAfter;
}

public static final String REF_TRSANSACTION = "refTrsansaction";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTRSANSACTIONID", nullable=true)
private MPAY_Transaction refTrsansaction;
public MPAY_Transaction getRefTrsansaction(){
return this.refTrsansaction;
}
public void setRefTrsansaction(MPAY_Transaction refTrsansaction){
this.refTrsansaction = refTrsansaction;
}

public static final String JV_TYPE = "jvType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="JVTYPEID", nullable=false)
private MPAY_JvType jvType;
public MPAY_JvType getJvType(){
return this.jvType;
}
public void setJvType(MPAY_JvType jvType){
this.jvType = jvType;
}

public static final String REF_ACCOUNT = "refAccount";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFACCOUNTID", nullable=false)
private MPAY_Account refAccount;
public MPAY_Account getRefAccount(){
return this.refAccount;
}
public void setRefAccount(MPAY_Account refAccount){
this.refAccount = refAccount;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_JvDetail [id= " + getId() + ", serial= " + getSerial() + ", description= " + getDescription() + ", debitCreditFlag= " + getDebitCreditFlag() + ", amount= " + getAmount() + ", isPosted= " + getIsPosted() + ", balanceBefore= " + getBalanceBefore() + ", balanceAfter= " + getBalanceAfter() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
int Serial= new Long("null".equals(getSerial() + "") ? 0 : getSerial()).intValue();
result = prime * result + (int) (Serial ^ Serial >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_JvDetail))
return false;
else {MPAY_JvDetail other = (MPAY_JvDetail) obj;
return this.hashCode() == other.hashCode();}
}


}