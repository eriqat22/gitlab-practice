package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_BankIntegMessages")
@XmlRootElement(name="BankIntegMessages")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_BankIntegMessage extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_BankIntegMessage(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REQUEST_I_D = "requestID";
@Column(name="REQUESTID", nullable=false, length=255)
private String requestID;
public String getRequestID(){
return this.requestID;
}
public void setRequestID(String requestID){
this.requestID = requestID;
}

public static final String REQUEST_CONTENT = "requestContent";
@Column(name="REQUESTCONTENT", nullable=false, length=8000)
@Lob
private String requestContent;
public String getRequestContent(){
return this.requestContent;
}
public void setRequestContent(String requestContent){
this.requestContent = requestContent;
}

public static final String REQUEST_DATE = "requestDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REQUESTDATE", nullable=false, length=32)
private java.sql.Timestamp requestDate;
public java.sql.Timestamp getRequestDate(){
return this.requestDate;
}
public void setRequestDate(java.sql.Timestamp requestDate){
this.requestDate = requestDate;
}

public static final String REQUEST_TOKEN = "requestToken";
@Column(name="REQUESTTOKEN", nullable=true, length=4000)
private String requestToken;
public String getRequestToken(){
return this.requestToken;
}
public void setRequestToken(String requestToken){
this.requestToken = requestToken;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String RESPONSE_I_D = "responseID";
@Column(name="RESPONSEID", nullable=true, length=255)
private String responseID;
public String getResponseID(){
return this.responseID;
}
public void setResponseID(String responseID){
this.responseID = responseID;
}

public static final String RESPONSE_CONTENT = "responseContent";
@Column(name="RESPONSECONTENT", nullable=true, length=8000)
@Lob
private String responseContent;
public String getResponseContent(){
return this.responseContent;
}
public void setResponseContent(String responseContent){
this.responseContent = responseContent;
}

public static final String RESPONSE_DATE = "responseDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="RESPONSEDATE", nullable=true, length=32)
private java.sql.Timestamp responseDate;
public java.sql.Timestamp getResponseDate(){
return this.responseDate;
}
public void setResponseDate(java.sql.Timestamp responseDate){
this.responseDate = responseDate;
}

public static final String RESPONSE_TOKEN = "responseToken";
@Column(name="RESPONSETOKEN", nullable=true, length=4000)
private String responseToken;
public String getResponseToken(){
return this.responseToken;
}
public void setResponseToken(String responseToken){
this.responseToken = responseToken;
}

public static final String STATUS_DESCRIPTION = "statusDescription";
@Column(name="STATUSDESCRIPTION", nullable=true, length=4000)
private String statusDescription;
public String getStatusDescription(){
return this.statusDescription;
}
public void setStatusDescription(String statusDescription){
this.statusDescription = statusDescription;
}

public static final String IS_REVERSAL = "isReversal";
@Column(name="ISREVERSAL", nullable=false, length=1)
private Boolean isReversal;
public Boolean getIsReversal(){
return this.isReversal;
}
public void setIsReversal(Boolean isReversal){
this.isReversal = isReversal;
}

public static final String IS_REVERSED = "isReversed";
@Column(name="ISREVERSED", nullable=false, length=1)
private Boolean isReversed;
public Boolean getIsReversed(){
return this.isReversed;
}
public void setIsReversed(Boolean isReversed){
this.isReversed = isReversed;
}

public static final String REF_STATUS = "refStatus";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFSTATUSID", nullable=true)
private MPAY_BankIntegStatus refStatus;
public MPAY_BankIntegStatus getRefStatus(){
return this.refStatus;
}
public void setRefStatus(MPAY_BankIntegStatus refStatus){
this.refStatus = refStatus;
}

public static final String REF_BANK = "refBank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFBANKID", nullable=false)
private MPAY_Bank refBank;
public MPAY_Bank getRefBank(){
return this.refBank;
}
public void setRefBank(MPAY_Bank refBank){
this.refBank = refBank;
}

public static final String REF_TRANSACTION = "refTransaction";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTRANSACTIONID", nullable=true)
private MPAY_Transaction refTransaction;
public MPAY_Transaction getRefTransaction(){
return this.refTransaction;
}
public void setRefTransaction(MPAY_Transaction refTransaction){
this.refTransaction = refTransaction;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=true)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_BankIntegMessage [id= " + getId() + ", requestID= " + getRequestID() + ", requestContent= " + getRequestContent() + ", requestDate= " + getRequestDate() + ", requestToken= " + getRequestToken() + ", amount= " + getAmount() + ", responseID= " + getResponseID() + ", responseContent= " + getResponseContent() + ", responseDate= " + getResponseDate() + ", responseToken= " + getResponseToken() + ", statusDescription= " + getStatusDescription() + ", isReversal= " + getIsReversal() + ", isReversed= " + getIsReversed() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRequestID() == null) ? 0 : getRequestID().hashCode());
result = prime * result + ((getRequestToken() == null) ? 0 : getRequestToken().hashCode());
result = prime * result + ((getResponseID() == null) ? 0 : getResponseID().hashCode());
result = prime * result + ((getResponseToken() == null) ? 0 : getResponseToken().hashCode());
result = prime * result + ((getStatusDescription() == null) ? 0 : getStatusDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_BankIntegMessage))
return false;
else {MPAY_BankIntegMessage other = (MPAY_BankIntegMessage) obj;
return this.hashCode() == other.hashCode();}
}


}