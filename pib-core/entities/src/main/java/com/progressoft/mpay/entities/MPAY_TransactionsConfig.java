package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_TransactionsConfig",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CLIENTTYPEID","ISSENDER","BANKEDUNBANKED","MESSAGETYPEID","Z_TENANT_ID"})
})
@XmlRootElement(name="TransactionsConfig")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_TransactionsConfig extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_TransactionsConfig(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=false, length=500)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String IS_SENDER = "isSender";
@Column(name="ISSENDER", nullable=false, length=500)
private Boolean isSender;
public Boolean getIsSender(){
return this.isSender;
}
public void setIsSender(Boolean isSender){
this.isSender = isSender;
}

public static final String BANKED_UNBANKED = "bankedUnbanked";
@Column(name="BANKEDUNBANKED", nullable=false, length=500)
private String bankedUnbanked;
public String getBankedUnbanked(){
return this.bankedUnbanked;
}
public void setBankedUnbanked(String bankedUnbanked){
this.bankedUnbanked = bankedUnbanked;
}

public static final String MIN_AMOUNT = "minAmount";
@Column(name="MINAMOUNT", nullable=false, length=500)
private Float minAmount;
public Float getMinAmount(){
return this.minAmount;
}
public void setMinAmount(Float minAmount){
this.minAmount = minAmount;
}

public static final String MAX_AMOUNT = "maxAmount";
@Column(name="MAXAMOUNT", nullable=false, length=500)
private Float maxAmount;
public Float getMaxAmount(){
return this.maxAmount;
}
public void setMaxAmount(Float maxAmount){
this.maxAmount = maxAmount;
}

public static final String REVERSABLE = "reversable";
@Column(name="REVERSABLE", nullable=false, length=7)
private Boolean reversable;
public Boolean getReversable(){
return this.reversable;
}
public void setReversable(Boolean reversable){
this.reversable = reversable;
}

public static final String CLIENT_TYPE = "clientType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CLIENTTYPEID", nullable=false)
private MPAY_ClientType clientType;
public MPAY_ClientType getClientType(){
return this.clientType;
}
public void setClientType(MPAY_ClientType clientType){
this.clientType = clientType;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=false)
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

@Override
public String toString() {
return "MPAY_TransactionsConfig [id= " + getId() + ", description= " + getDescription() + ", isSender= " + getIsSender() + ", bankedUnbanked= " + getBankedUnbanked() + ", minAmount= " + getMinAmount() + ", maxAmount= " + getMaxAmount() + ", reversable= " + getReversable() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + Float.floatToIntBits(getMinAmount());
result = prime * result + Float.floatToIntBits(getMaxAmount());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_TransactionsConfig))
return false;
else {MPAY_TransactionsConfig other = (MPAY_TransactionsConfig) obj;
return this.hashCode() == other.hashCode();}
}


}