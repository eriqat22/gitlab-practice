package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_CitiesNLS")
@XmlRootElement(name="CitiesNLS")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CitiesNL extends JFWTranslation implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CitiesNL(){/*Default Constructor*/}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=false, length=100)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String CITY_ID = "cityId";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CITYID", nullable=false)
private MPAY_City cityId;
@flexjson.JSON(include = false)
public MPAY_City getCityId(){
return this.cityId;
}
public void setCityId(MPAY_City cityId){
this.cityId = cityId;
}

public static final String CLANG = "clang";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CLANGID", nullable=false)
private MPAY_Language clang;
public MPAY_Language getClang(){
return this.clang;
}
public void setClang(MPAY_Language clang){
this.clang= clang;  this.languageCode = clang.getCode();
}

@Override
public String toString() {
return "MPAY_CitiesNL [id= " + getId() + ", translation= " + getTranslation() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getTranslation() == null) ? 0 : getTranslation().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CitiesNL))
return false;
else {MPAY_CitiesNL other = (MPAY_CitiesNL) obj;
return this.hashCode() == other.hashCode();}
}


}