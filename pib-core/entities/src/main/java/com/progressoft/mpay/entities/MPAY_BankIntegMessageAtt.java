package com.progressoft.mpay.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import com.progressoft.jfw.model.bussinessobject.attachments.AttachmentItem;

@Entity
@Table(name="MPAY_BankIntegMessageATT")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_BankIntegMessageAtt extends AttachmentItem implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_BankIntegMessageAtt(){/*Default Constructor*/}


}