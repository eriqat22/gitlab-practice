package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_NotificationTypes_NLS")
@XmlRootElement(name="NotificationTypes_NLS")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_NotificationTypes_NLS extends JFWTranslation implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_NotificationTypes_NLS(){/*Default Constructor*/}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=false, length=100)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String TYPE_ID = "typeId";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="TYPEID", nullable=false)
private MPAY_NotificationType typeId;
@flexjson.JSON(include = false)
public MPAY_NotificationType getTypeId(){
return this.typeId;
}
public void setTypeId(MPAY_NotificationType typeId){
this.typeId = typeId;
}

@Override
public String toString() {
return "MPAY_NotificationTypes_NLS [id= " + getId() + ", translation= " + getTranslation() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getTranslation() == null) ? 0 : getTranslation().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_NotificationTypes_NLS))
return false;
else {MPAY_NotificationTypes_NLS other = (MPAY_NotificationTypes_NLS) obj;
return this.hashCode() == other.hashCode();}
}


}