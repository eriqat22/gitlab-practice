package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_Rates",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"BASECURRENCYID","FOREIGNCURRENCYID","Z_TENANT_ID"})
})
@XmlRootElement(name="Rates")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_Rate extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Rate(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String RATE = "rate";
@Column(name="RATE", nullable=false, length=50)
private java.math.BigDecimal rate;
public java.math.BigDecimal getRate(){
return this.rate;
}
public void setRate(java.math.BigDecimal rate){
this.rate = rate;
}

public static final String MARGIN = "margin";
@Column(name="MARGIN", nullable=true, length=50)
private java.math.BigDecimal margin;
public java.math.BigDecimal getMargin(){
return this.margin;
}
public void setMargin(java.math.BigDecimal margin){
this.margin = margin;
}

public static final String BASE_CURRENCY = "baseCurrency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BASECURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency baseCurrency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getBaseCurrency(){
return this.baseCurrency;
}
public void setBaseCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency baseCurrency){
this.baseCurrency = baseCurrency;
}

public static final String FOREIGN_CURRENCY = "foreignCurrency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="FOREIGNCURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency foreignCurrency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getForeignCurrency(){
return this.foreignCurrency;
}
public void setForeignCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency foreignCurrency){
this.foreignCurrency = foreignCurrency;
}

@Override
public String toString() {
return "MPAY_Rate [id= " + getId() + ", rate= " + getRate() + ", margin= " + getMargin() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRate() == null) ? 0 : getRate().hashCode());
result = prime * result + ((getMargin() == null) ? 0 : getMargin().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Rate))
return false;
else {MPAY_Rate other = (MPAY_Rate) obj;
return this.hashCode() == other.hashCode();}
}


}