package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_LimitsSchemes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="LimitsSchemes")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_LimitsScheme extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_LimitsScheme(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String PINLESS_AMOUNT = "pinlessAmount";
@Column(name="PINLESSAMOUNT", nullable=false, precision=21, scale=8, length=19)
private java.math.BigDecimal pinlessAmount;
public java.math.BigDecimal getPinlessAmount(){
return this.pinlessAmount;
}
public void setPinlessAmount(java.math.BigDecimal pinlessAmount){
this.pinlessAmount = pinlessAmount;
}

public static final String WALLET_CAP = "walletCap";
@Column(name="WALLETCAP", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal walletCap;
public java.math.BigDecimal getWalletCap(){
return this.walletCap;
}
public void setWalletCap(java.math.BigDecimal walletCap){
this.walletCap = walletCap;
}

public static final String INITIAL_COUNT = "initialCount";
@Column(name="INITIALCOUNT", nullable=true, length=20)
private Long initialCount;
public Long getInitialCount(){
return this.initialCount;
}
public void setInitialCount(Long initialCount){
this.initialCount = initialCount;
}

public static final String INITIAL_AMOUNT = "initialAmount";
@Column(name="INITIALAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal initialAmount;
public java.math.BigDecimal getInitialAmount(){
return this.initialAmount;
}
public void setInitialAmount(java.math.BigDecimal initialAmount){
this.initialAmount = initialAmount;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

public static final String REF_SCHEME_LIMITS = "refSchemeLimits";
@OneToMany(mappedBy = "refScheme")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Limit> refSchemeLimits;
public List<MPAY_Limit> getRefSchemeLimits(){
return this.refSchemeLimits;
}
public void setRefSchemeLimits(List<MPAY_Limit> refSchemeLimits){
this.refSchemeLimits = refSchemeLimits;
}

@Override
public String toString() {
return "MPAY_LimitsScheme [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", isActive= " + getIsActive() + ", pinlessAmount= " + getPinlessAmount() + ", walletCap= " + getWalletCap() + ", initialCount= " + getInitialCount() + ", initialAmount= " + getInitialAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
int InitialCount= new Long("null".equals(getInitialCount() + "") ? 0 : getInitialCount()).intValue();
result = prime * result + (int) (InitialCount ^ InitialCount >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_LimitsScheme))
return false;
else {MPAY_LimitsScheme other = (MPAY_LimitsScheme) obj;
return this.hashCode() == other.hashCode();}
}


}