package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_RatesLogsRpt")
@XmlRootElement(name="RatesLogsRpt")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_RatesLogsRpt extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_RatesLogsRpt(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String FROM_DATE = "fromDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="FROMDATE", nullable=false, length=50)
private java.sql.Timestamp fromDate;
public java.sql.Timestamp getFromDate(){
return this.fromDate;
}
public void setFromDate(java.sql.Timestamp fromDate){
this.fromDate = fromDate;
}

public static final String TO_DATE = "toDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="TODATE", nullable=false, length=50)
private java.sql.Timestamp toDate;
public java.sql.Timestamp getToDate(){
return this.toDate;
}
public void setToDate(java.sql.Timestamp toDate){
this.toDate = toDate;
}

public static final String RATE = "rate";
@Column(name="RATE", nullable=false, length=50)
private java.math.BigDecimal rate;
public java.math.BigDecimal getRate(){
return this.rate;
}
public void setRate(java.math.BigDecimal rate){
this.rate = rate;
}

public static final String MARGIN = "margin";
@Column(name="MARGIN", nullable=true, length=50)
private java.math.BigDecimal margin;
public java.math.BigDecimal getMargin(){
return this.margin;
}
public void setMargin(java.math.BigDecimal margin){
this.margin = margin;
}

public static final String BASE_CURRENCY = "baseCurrency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BASECURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency baseCurrency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getBaseCurrency(){
return this.baseCurrency;
}
public void setBaseCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency baseCurrency){
this.baseCurrency = baseCurrency;
}

public static final String FOREIGN_CURRENCY = "foreignCurrency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="FOREIGNCURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency foreignCurrency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getForeignCurrency(){
return this.foreignCurrency;
}
public void setForeignCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency foreignCurrency){
this.foreignCurrency = foreignCurrency;
}

@Override
public String toString() {
return "MPAY_RatesLogsRpt [id= " + getId() + ", fromDate= " + getFromDate() + ", toDate= " + getToDate() + ", rate= " + getRate() + ", margin= " + getMargin() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRate() == null) ? 0 : getRate().hashCode());
result = prime * result + ((getMargin() == null) ? 0 : getMargin().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_RatesLogsRpt))
return false;
else {MPAY_RatesLogsRpt other = (MPAY_RatesLogsRpt) obj;
return this.hashCode() == other.hashCode();}
}


}