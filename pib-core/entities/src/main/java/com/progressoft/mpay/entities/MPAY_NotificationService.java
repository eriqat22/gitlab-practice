package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_NotificationServices",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","PROCESSOR","Z_TENANT_ID"})
})
@XmlRootElement(name="NotificationServices")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_NotificationService extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_NotificationService(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String PROCESSOR = "processor";
@Column(name="PROCESSOR", nullable=false, length=1000)
private String processor;
public String getProcessor(){
return this.processor;
}
public void setProcessor(String processor){
this.processor = processor;
}

public static final String REF_SERVICE_NOTIFICATION_SERVICE_ARGS = "refServiceNotificationServiceArgs";
@OneToMany(mappedBy = "refService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_NotificationServiceArg> refServiceNotificationServiceArgs;
public List<MPAY_NotificationServiceArg> getRefServiceNotificationServiceArgs(){
return this.refServiceNotificationServiceArgs;
}
public void setRefServiceNotificationServiceArgs(List<MPAY_NotificationServiceArg> refServiceNotificationServiceArgs){
this.refServiceNotificationServiceArgs = refServiceNotificationServiceArgs;
}

@Override
public String toString() {
return "MPAY_NotificationService [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", processor= " + getProcessor() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getProcessor() == null) ? 0 : getProcessor().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_NotificationService))
return false;
else {MPAY_NotificationService other = (MPAY_NotificationService) obj;
return this.hashCode() == other.hashCode();}
}


}