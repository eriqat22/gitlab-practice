package com.progressoft.mpay.entities;

public class WF_SystemAccountsCashOut
 {
    // Steps
    public static final String STEP_Initialization = "1016091";
    public static final String STEP_ConfirmationStep = "1016092";
    public static final String STEP_Processing = "1016093";
    public static final String STEP_Accepted = "1016094";
    public static final String STEP_Rejected = "1016095";
    public static final String STEP_End = "1016096";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_ConfirmationStep = "Confirmation Step";
    public static final String STEP_STATUS_Processing = "Processing";
    public static final String STEP_STATUS_Accepted = "Accepted";
    public static final String STEP_STATUS_Rejected = "Rejected";
    public static final String STEP_STATUS_End = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_SVC_Accept = "SVC_Accept";
    public static final String ACTION_NAME_SVC_Reject = "SVC_Reject";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SubmitConfirmationStep = "3";
    public static final String ACTION_CODE_DeleteConfirmationStep = "4";
    public static final String ACTION_CODE_SVC_AcceptProcessing = "5";
    public static final String ACTION_CODE_SVC_RejectProcessing = "6";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SubmitConfirmationStep = "2024198575";
    public static final String ACTION_KEY_DeleteConfirmationStep = "29168601";
    public static final String ACTION_KEY_SVC_AcceptProcessing = "659613586";
    public static final String ACTION_KEY_SVC_RejectProcessing = "1825828494";

}