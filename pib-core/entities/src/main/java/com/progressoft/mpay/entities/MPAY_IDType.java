package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_IDTypes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="IDTypes")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_IDType extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_IDType(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_CUSTOMER = "isCustomer";
@Column(name="ISCUSTOMER", nullable=true, length=255)
private Boolean isCustomer;
public Boolean getIsCustomer(){
return this.isCustomer;
}
public void setIsCustomer(Boolean isCustomer){
this.isCustomer = isCustomer;
}

public static final String IS_SYSTEM = "isSystem";
@Column(name="ISSYSTEM", nullable=true, length=255)
private Boolean isSystem;
public Boolean getIsSystem(){
return this.isSystem;
}
public void setIsSystem(Boolean isSystem){
this.isSystem = isSystem;
}

public static final String I_D_TYPES_N_L_S = "iDTypesNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "idTypeId")
@MapKey(name="languageCode")
private Map<String, MPAY_IDTypes_NLS> iDTypesNLS;
public Map<String, MPAY_IDTypes_NLS> getIDTypesNLS(){
return this.iDTypesNLS;
}
public void setIDTypesNLS(Map<String, MPAY_IDTypes_NLS> iDTypesNLS){
this.iDTypesNLS = iDTypesNLS;
}

@Override
public String toString() {
return "MPAY_IDType [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", isCustomer= " + getIsCustomer() + ", isSystem= " + getIsSystem() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_IDType))
return false;
else {MPAY_IDType other = (MPAY_IDType) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.iDTypesNLS;
}
}