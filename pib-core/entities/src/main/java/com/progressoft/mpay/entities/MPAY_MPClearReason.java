package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_MPClearReasons",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="MPClearReasons")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_MPClearReason extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_MPClearReason(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

@Override
public String toString() {
return "MPAY_MPClearReason [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_MPClearReason))
return false;
else {MPAY_MPClearReason other = (MPAY_MPClearReason) obj;
return this.hashCode() == other.hashCode();}
}


}