package com.progressoft.mpay.entities;

public class WF_PSP
 {
    // Steps
    public static final String STEP_Initialization = "101501";
    public static final String STEP_CreationApproval = "101502";
    public static final String STEP_RepairNew = "101503";
    public static final String STEP_EditNew = "101504";
    public static final String STEP_ViewApproved = "101505";
    public static final String STEP_EditApproved = "101506";
    public static final String STEP_ModificationApproval = "101507";
    public static final String STEP_RepairRejected = "101508";
    public static final String STEP_EditRejected = "101509";
    public static final String STEP_DeletionApproval = "101510";
    public static final String STEP_End = "101511";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_CreationApproval = "New Authorization";
    public static final String STEP_STATUS_RepairNew = "Repair New";
    public static final String STEP_STATUS_EditNew = "Edit New";
    public static final String STEP_STATUS_ViewApproved = "Approved";
    public static final String STEP_STATUS_EditApproved = "Modify";
    public static final String STEP_STATUS_ModificationApproval = "Authorization";
    public static final String STEP_STATUS_RepairRejected = "Repair Rejected";
    public static final String STEP_STATUS_EditRejected = "Edit Rejected";
    public static final String STEP_STATUS_DeletionApproval = "Deletion Authorization";
    public static final String STEP_STATUS_End = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Cancel = "Cancel";
    public static final String ACTION_NAME_Reset = "Reset";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveCreationApproval = "3";
    public static final String ACTION_CODE_RejectCreationApproval = "4";
    public static final String ACTION_CODE_EditRepairNew = "5";
    public static final String ACTION_CODE_DeleteRepairNew = "6";
    public static final String ACTION_CODE_SaveEditNew = "7";
    public static final String ACTION_CODE_CancelEditNew = "8";
    public static final String ACTION_CODE_EditViewApproved = "9";
    public static final String ACTION_CODE_DeleteViewApproved = "10";
    public static final String ACTION_CODE_SaveEditApproved = "11";
    public static final String ACTION_CODE_CancelEditApproved = "12";
    public static final String ACTION_CODE_ApproveModificationApproval = "13";
    public static final String ACTION_CODE_RejectModificationApproval = "14";
    public static final String ACTION_CODE_EditRepairRejected = "15";
    public static final String ACTION_CODE_ResetRepairRejected = "16";
    public static final String ACTION_CODE_SaveEditRejected = "17";
    public static final String ACTION_CODE_CancelEditRejected = "18";
    public static final String ACTION_CODE_ApproveDeletionApproval = "19";
    public static final String ACTION_CODE_RejectDeletionApproval = "20";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveCreationApproval = "1006191834";
    public static final String ACTION_KEY_RejectCreationApproval = "2019205986";
    public static final String ACTION_KEY_EditRepairNew = "1675177248";
    public static final String ACTION_KEY_DeleteRepairNew = "486944921";
    public static final String ACTION_KEY_SaveEditNew = "527872933";
    public static final String ACTION_KEY_CancelEditNew = "1313920827";
    public static final String ACTION_KEY_EditViewApproved = "310693120";
    public static final String ACTION_KEY_DeleteViewApproved = "747767372";
    public static final String ACTION_KEY_SaveEditApproved = "285537469";
    public static final String ACTION_KEY_CancelEditApproved = "1202806071";
    public static final String ACTION_KEY_ApproveModificationApproval = "838677015";
    public static final String ACTION_KEY_RejectModificationApproval = "424162558";
    public static final String ACTION_KEY_EditRepairRejected = "1120324988";
    public static final String ACTION_KEY_ResetRepairRejected = "168174155";
    public static final String ACTION_KEY_SaveEditRejected = "2147423606";
    public static final String ACTION_KEY_CancelEditRejected = "165545455";
    public static final String ACTION_KEY_ApproveDeletionApproval = "1187498060";
    public static final String ACTION_KEY_RejectDeletionApproval = "1704049170";

}