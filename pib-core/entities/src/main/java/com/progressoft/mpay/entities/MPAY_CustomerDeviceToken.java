package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_CustomerDeviceTokens",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CUSTOMERDEVICEID","Z_TENANT_ID"})
})
@XmlRootElement(name="CustomerDeviceTokens")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CustomerDeviceToken extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CustomerDeviceToken(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String TOKEN = "token";
@Column(name="TOKEN", nullable=false, length=4000)
private String token;
public String getToken(){
return this.token;
}
public void setToken(String token){
this.token = token;
}

public static final String _EXPIRATION_DATE = "ExpirationDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="EXPIRATIONDATE", nullable=false, length=19)
private java.sql.Timestamp ExpirationDate;
public java.sql.Timestamp getExpirationDate(){
return this.ExpirationDate;
}
public void setExpirationDate(java.sql.Timestamp ExpirationDate){
this.ExpirationDate = ExpirationDate;
}

public static final String CUSTOMER_DEVICE = "customerDevice";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CUSTOMERDEVICEID", nullable=true)
private MPAY_CustomerDevice customerDevice;
public MPAY_CustomerDevice getCustomerDevice(){
return this.customerDevice;
}
public void setCustomerDevice(MPAY_CustomerDevice customerDevice){
this.customerDevice = customerDevice;
}

@Override
public String toString() {
return "MPAY_CustomerDeviceToken [id= " + getId() + ", token= " + getToken() + ", ExpirationDate= " + getExpirationDate() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getToken() == null) ? 0 : getToken().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CustomerDeviceToken))
return false;
else {MPAY_CustomerDeviceToken other = (MPAY_CustomerDeviceToken) obj;
return this.hashCode() == other.hashCode();}
}


}