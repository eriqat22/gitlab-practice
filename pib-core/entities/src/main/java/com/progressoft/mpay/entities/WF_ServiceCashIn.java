package com.progressoft.mpay.entities;

public class WF_ServiceCashIn
 {
    // Steps
    public static final String STEP_Initialization = "101501";
    public static final String STEP_CalcuationStep = "101506";
    public static final String STEP_Processing = "101502";
    public static final String STEP_Accepted = "101503";
    public static final String STEP_Rejected = "101504";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_CalcuationStep = "Calculate Charges";
    public static final String STEP_STATUS_Processing = "Processing";
    public static final String STEP_STATUS_Accepted = "Accepted";
    public static final String STEP_STATUS_Rejected = "Rejected";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_SaveandSubmit = "Save and Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Recalculate = "Recalculate";
    public static final String ACTION_NAME_SVC_Accept = "SVC_Accept";
    public static final String ACTION_NAME_SVC_Reject = "SVC_Reject";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SaveandSubmitCalcuationStep = "5";
    public static final String ACTION_CODE_DeleteCalcuationStep = "6";
    public static final String ACTION_CODE_RecalculateCalcuationStep = "8";
    public static final String ACTION_CODE_SVC_AcceptProcessing = "3";
    public static final String ACTION_CODE_SVC_RejectProcessing = "4";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SaveandSubmitCalcuationStep = "1388720816";
    public static final String ACTION_KEY_DeleteCalcuationStep = "1475226447";
    public static final String ACTION_KEY_RecalculateCalcuationStep = "829440645";
    public static final String ACTION_KEY_SVC_AcceptProcessing = "1016712247";
    public static final String ACTION_KEY_SVC_RejectProcessing = "320647892";

}