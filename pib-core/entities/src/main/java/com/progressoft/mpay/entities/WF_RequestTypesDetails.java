package com.progressoft.mpay.entities;

public class WF_RequestTypesDetails
 {
    // Steps
    public static final String STEP_Initialization = "901901";
    public static final String STEP_View = "901902";
    public static final String STEP_Deleted = "901903";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_View = "View";
    public static final String STEP_STATUS_Deleted = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_SVC_Approve = "SVC_Approve";
    public static final String ACTION_NAME_SVC_Cancel = "SVC_Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SaveView = "3";
    public static final String ACTION_CODE_DeleteView = "4";
    public static final String ACTION_CODE_SVC_ApproveView = "5";
    public static final String ACTION_CODE_SVC_CancelView = "6";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SaveView = "1530421955";
    public static final String ACTION_KEY_DeleteView = "291332742";
    public static final String ACTION_KEY_SVC_ApproveView = "400240919";
    public static final String ACTION_KEY_SVC_CancelView = "1971919237";

}