package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_ClientsLimits",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"ACCOUNTID","MESSAGETYPEID","Z_TENANT_ID"})
})
@XmlRootElement(name="ClientsLimits")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ClientsLimit extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ClientsLimit(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String YEAR = "year";
@Column(name="YEAR", nullable=false, length=8)
private Long year;
public Long getYear(){
return this.year;
}
public void setYear(Long year){
this.year = year;
}

public static final String DAILY_DATE = "dailyDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="DAILYDATE", nullable=false, length=12)
private java.sql.Timestamp dailyDate;
public java.sql.Timestamp getDailyDate(){
return this.dailyDate;
}
public void setDailyDate(java.sql.Timestamp dailyDate){
this.dailyDate = dailyDate;
}

public static final String DAILY_COUNT = "dailyCount";
@Column(name="DAILYCOUNT", nullable=false, length=8)
private Long dailyCount;
public Long getDailyCount(){
return this.dailyCount;
}
public void setDailyCount(Long dailyCount){
this.dailyCount = dailyCount;
}

public static final String DAILY_AMOUNT = "dailyAmount";
@Column(name="DAILYAMOUNT", nullable=false, length=19)
private java.math.BigDecimal dailyAmount;
public java.math.BigDecimal getDailyAmount(){
return this.dailyAmount;
}
public void setDailyAmount(java.math.BigDecimal dailyAmount){
this.dailyAmount = dailyAmount;
}

public static final String WEEK_NUMBER = "weekNumber";
@Column(name="WEEKNUMBER", nullable=false, length=4)
private Long weekNumber;
public Long getWeekNumber(){
return this.weekNumber;
}
public void setWeekNumber(Long weekNumber){
this.weekNumber = weekNumber;
}

public static final String WEEKLY_COUNT = "weeklyCount";
@Column(name="WEEKLYCOUNT", nullable=false, length=8)
private Long weeklyCount;
public Long getWeeklyCount(){
return this.weeklyCount;
}
public void setWeeklyCount(Long weeklyCount){
this.weeklyCount = weeklyCount;
}

public static final String WEEKLY_AMOUNT = "weeklyAmount";
@Column(name="WEEKLYAMOUNT", nullable=false, length=19)
private java.math.BigDecimal weeklyAmount;
public java.math.BigDecimal getWeeklyAmount(){
return this.weeklyAmount;
}
public void setWeeklyAmount(java.math.BigDecimal weeklyAmount){
this.weeklyAmount = weeklyAmount;
}

public static final String MONTH = "month";
@Column(name="MONTH", nullable=false, length=4)
private Long month;
public Long getMonth(){
return this.month;
}
public void setMonth(Long month){
this.month = month;
}

public static final String MONTHLY_COUNT = "monthlyCount";
@Column(name="MONTHLYCOUNT", nullable=false, length=8)
private Long monthlyCount;
public Long getMonthlyCount(){
return this.monthlyCount;
}
public void setMonthlyCount(Long monthlyCount){
this.monthlyCount = monthlyCount;
}

public static final String MONTHLY_AMOUNT = "monthlyAmount";
@Column(name="MONTHLYAMOUNT", nullable=false, length=19)
private java.math.BigDecimal monthlyAmount;
public java.math.BigDecimal getMonthlyAmount(){
return this.monthlyAmount;
}
public void setMonthlyAmount(java.math.BigDecimal monthlyAmount){
this.monthlyAmount = monthlyAmount;
}

public static final String YEARLY_COUNT = "yearlyCount";
@Column(name="YEARLYCOUNT", nullable=false, length=8)
private Long yearlyCount;
public Long getYearlyCount(){
return this.yearlyCount;
}
public void setYearlyCount(Long yearlyCount){
this.yearlyCount = yearlyCount;
}

public static final String YEARLY_AMOUNT = "yearlyAmount";
@Column(name="YEARLYAMOUNT", nullable=false, length=19)
private java.math.BigDecimal yearlyAmount;
public java.math.BigDecimal getYearlyAmount(){
return this.yearlyAmount;
}
public void setYearlyAmount(java.math.BigDecimal yearlyAmount){
this.yearlyAmount = yearlyAmount;
}

public static final String DAILY_CREDIT_AMOUNT = "dailyCreditAmount";
@Column(name="DAILYCREDITAMOUNT", nullable=true, length=19)
private java.math.BigDecimal dailyCreditAmount;
public java.math.BigDecimal getDailyCreditAmount(){
return this.dailyCreditAmount;
}
public void setDailyCreditAmount(java.math.BigDecimal dailyCreditAmount){
this.dailyCreditAmount = dailyCreditAmount;
}

public static final String MONTHLY_CREDIT_AMOUNT = "monthlyCreditAmount";
@Column(name="MONTHLYCREDITAMOUNT", nullable=true, length=19)
private java.math.BigDecimal monthlyCreditAmount;
public java.math.BigDecimal getMonthlyCreditAmount(){
return this.monthlyCreditAmount;
}
public void setMonthlyCreditAmount(java.math.BigDecimal monthlyCreditAmount){
this.monthlyCreditAmount = monthlyCreditAmount;
}

public static final String WEEKLY_CREDIT_AMOUNT = "weeklyCreditAmount";
@Column(name="WEEKLYCREDITAMOUNT", nullable=true, length=19)
private java.math.BigDecimal weeklyCreditAmount;
public java.math.BigDecimal getWeeklyCreditAmount(){
return this.weeklyCreditAmount;
}
public void setWeeklyCreditAmount(java.math.BigDecimal weeklyCreditAmount){
this.weeklyCreditAmount = weeklyCreditAmount;
}

public static final String YEARLY_CREDIT_AMOUNT = "yearlyCreditAmount";
@Column(name="YEARLYCREDITAMOUNT", nullable=true, length=19)
private java.math.BigDecimal yearlyCreditAmount;
public java.math.BigDecimal getYearlyCreditAmount(){
return this.yearlyCreditAmount;
}
public void setYearlyCreditAmount(java.math.BigDecimal yearlyCreditAmount){
this.yearlyCreditAmount = yearlyCreditAmount;
}

public static final String ACCOUNT = "account";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="ACCOUNTID", nullable=false)
private MPAY_Account account;
public MPAY_Account getAccount(){
return this.account;
}
public void setAccount(MPAY_Account account){
this.account = account;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=false)
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

@Override
public String toString() {
return "MPAY_ClientsLimit [id= " + getId() + ", year= " + getYear() + ", dailyDate= " + getDailyDate() + ", dailyCount= " + getDailyCount() + ", dailyAmount= " + getDailyAmount() + ", weekNumber= " + getWeekNumber() + ", weeklyCount= " + getWeeklyCount() + ", weeklyAmount= " + getWeeklyAmount() + ", month= " + getMonth() + ", monthlyCount= " + getMonthlyCount() + ", monthlyAmount= " + getMonthlyAmount() + ", yearlyCount= " + getYearlyCount() + ", yearlyAmount= " + getYearlyAmount() + ", dailyCreditAmount= " + getDailyCreditAmount() + ", monthlyCreditAmount= " + getMonthlyCreditAmount() + ", weeklyCreditAmount= " + getWeeklyCreditAmount() + ", yearlyCreditAmount= " + getYearlyCreditAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getYear() == null) ? 0 : getYear().hashCode());
result = prime * result + ((getDailyCount() == null) ? 0 : getDailyCount().hashCode());
result = prime * result + ((getDailyAmount() == null) ? 0 : getDailyAmount().hashCode());
result = prime * result + ((getWeekNumber() == null) ? 0 : getWeekNumber().hashCode());
result = prime * result + ((getWeeklyCount() == null) ? 0 : getWeeklyCount().hashCode());
result = prime * result + ((getWeeklyAmount() == null) ? 0 : getWeeklyAmount().hashCode());
result = prime * result + ((getMonth() == null) ? 0 : getMonth().hashCode());
result = prime * result + ((getMonthlyCount() == null) ? 0 : getMonthlyCount().hashCode());
result = prime * result + ((getMonthlyAmount() == null) ? 0 : getMonthlyAmount().hashCode());
result = prime * result + ((getYearlyCount() == null) ? 0 : getYearlyCount().hashCode());
result = prime * result + ((getYearlyAmount() == null) ? 0 : getYearlyAmount().hashCode());
result = prime * result + ((getDailyCreditAmount() == null) ? 0 : getDailyCreditAmount().hashCode());
result = prime * result + ((getMonthlyCreditAmount() == null) ? 0 : getMonthlyCreditAmount().hashCode());
result = prime * result + ((getWeeklyCreditAmount() == null) ? 0 : getWeeklyCreditAmount().hashCode());
result = prime * result + ((getYearlyCreditAmount() == null) ? 0 : getYearlyCreditAmount().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ClientsLimit))
return false;
else {MPAY_ClientsLimit other = (MPAY_ClientsLimit) obj;
return this.hashCode() == other.hashCode();}
}


}