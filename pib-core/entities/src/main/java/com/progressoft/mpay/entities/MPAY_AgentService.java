package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.List;

@Entity

@Table(name="MPAY_AgentServices",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="AgentServices")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_AgentService extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_AgentService(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String SERVICES = "services";
@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.MERGE })
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinTable(name="MPAY_AgentServicesServices", joinColumns = @JoinColumn(name = "AGENTSERVICES_ID"), inverseJoinColumns = @JoinColumn(name = "SERVICES_ID"))
private List<MPAY_AgentProvidedService> services = new ArrayList<MPAY_AgentProvidedService>();
public List<MPAY_AgentProvidedService> getServices(){
return this.services;
}
public void setServices(List<MPAY_AgentProvidedService> services){
this.services = services;
}

@Override
public String toString() {
return "MPAY_AgentService [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_AgentService))
return false;
else {MPAY_AgentService other = (MPAY_AgentService) obj;
return this.hashCode() == other.hashCode();}
}


}