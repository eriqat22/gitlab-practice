package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_ServiceAccounts")
@XmlRootElement(name="ServiceAccounts")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ServiceAccount extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ServiceAccount(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String BANKED_UNBANKED = "bankedUnbanked";
@Column(name="BANKEDUNBANKED", nullable=false, length=20)
private String bankedUnbanked;
public String getBankedUnbanked(){
return this.bankedUnbanked;
}
public void setBankedUnbanked(String bankedUnbanked){
this.bankedUnbanked = bankedUnbanked;
}

public static final String BRANCH = "branch";
@Column(name="BRANCH", nullable=true, length=50)
private String branch;
public String getBranch(){
return this.branch;
}
public void setBranch(String branch){
this.branch = branch;
}

public static final String EXTERNAL_ACC = "externalAcc";
@Column(name="EXTERNALACC", nullable=true, length=100)
private String externalAcc;
public String getExternalAcc(){
return this.externalAcc;
}
public void setExternalAcc(String externalAcc){
this.externalAcc = externalAcc;
}

public static final String IBAN = "iban";
@Column(name="IBAN", nullable=true, length=100)
private String iban;
public String getIban(){
return this.iban;
}
public void setIban(String iban){
this.iban = iban;
}

public static final String MAS = "mas";
@Column(name="MAS", nullable=false, length=10)
private Long mas;
public Long getMas(){
return this.mas;
}
public void setMas(Long mas){
this.mas = mas;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=true, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String IS_REGISTERED = "isRegistered";
@Column(name="ISREGISTERED", nullable=true, length=1)
private Boolean isRegistered;
public Boolean getIsRegistered(){
return this.isRegistered;
}
public void setIsRegistered(Boolean isRegistered){
this.isRegistered = isRegistered;
}

public static final String IS_DEFAULT = "isDefault";
@Column(name="ISDEFAULT", nullable=false, length=1)
private Boolean isDefault;
public Boolean getIsDefault(){
return this.isDefault;
}
public void setIsDefault(Boolean isDefault){
this.isDefault = isDefault;
}

public static final String IS_SWITCH_DEFAULT = "isSwitchDefault";
@Column(name="ISSWITCHDEFAULT", nullable=false, length=1)
private Boolean isSwitchDefault;
public Boolean getIsSwitchDefault(){
return this.isSwitchDefault;
}
public void setIsSwitchDefault(Boolean isSwitchDefault){
this.isSwitchDefault = isSwitchDefault;
}

public static final String EXTRA_DATA = "extraData";
@Column(name="EXTRADATA", nullable=true, length=1000)
private String extraData;
public String getExtraData(){
return this.extraData;
}
public void setExtraData(String extraData){
this.extraData = extraData;
}

public static final String NEW_EXTERNAL_ACC = "newExternalAcc";
@Column(name="NEWEXTERNALACC", nullable=true, length=100)
private String newExternalAcc;
public String getNewExternalAcc(){
return this.newExternalAcc;
}
public void setNewExternalAcc(String newExternalAcc){
this.newExternalAcc = newExternalAcc;
}

public static final String APPROVED_DATA = "approvedData";
@Column(name="APPROVEDDATA", nullable=true, length=4000)
private String approvedData;
public String getApprovedData(){
return this.approvedData;
}
public void setApprovedData(String approvedData){
this.approvedData = approvedData;
}

public static final String REJECTION_NOTE = "rejectionNote";
@Column(name="REJECTIONNOTE", nullable=true, length=255)
private String rejectionNote;
public String getRejectionNote(){
return this.rejectionNote;
}
public void setRejectionNote(String rejectionNote){
this.rejectionNote = rejectionNote;
}

public static final String APPROVAL_NOTE = "approvalNote";
@Column(name="APPROVALNOTE", nullable=true, length=255)
private String approvalNote;
public String getApprovalNote(){
return this.approvalNote;
}
public void setApprovalNote(String approvalNote){
this.approvalNote = approvalNote;
}

public static final String LIMITS = "limits";
@Column(name="LIMITS", nullable=true, length=255)
@Transient
private String limits;
public String getLimits(){
return this.limits;
}
public void setLimits(String limits){
this.limits = limits;
}

public static final String CHANGES = "changes";
@Column(name="CHANGES", nullable=true, length=1000)
@Transient
private String changes;
public String getChanges(){
return this.changes;
}
public void setChanges(String changes){
this.changes = changes;
}

public static final String LAST_TRANSACTION_DATE = "lastTransactionDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="LASTTRANSACTIONDATE", nullable=true, length=20)
private java.sql.Timestamp lastTransactionDate;
public java.sql.Timestamp getLastTransactionDate(){
return this.lastTransactionDate;
}
public void setLastTransactionDate(java.sql.Timestamp lastTransactionDate){
this.lastTransactionDate = lastTransactionDate;
}

public static final String SERVICE = "service";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SERVICEID", nullable=false)
private MPAY_CorpoarteService service;
public MPAY_CorpoarteService getService(){
return this.service;
}
public void setService(MPAY_CorpoarteService service){
this.service = service;
}

public static final String CATEGORY = "category";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CATEGORYID", nullable=false)
private MPAY_AccountCategory category;
public MPAY_AccountCategory getCategory(){
return this.category;
}
public void setCategory(MPAY_AccountCategory category){
this.category = category;
}

public static final String BANK = "bank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BANKID", nullable=false)
private MPAY_Bank bank;
public MPAY_Bank getBank(){
return this.bank;
}
public void setBank(MPAY_Bank bank){
this.bank = bank;
}

public static final String REF_PROFILE = "refProfile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFPROFILEID", nullable=false)
private MPAY_Profile refProfile;
public MPAY_Profile getRefProfile(){
return this.refProfile;
}
public void setRefProfile(MPAY_Profile refProfile){
this.refProfile = refProfile;
}

public static final String REF_ACCOUNT = "refAccount";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFACCOUNTID", nullable=true)
private MPAY_Account refAccount;
public MPAY_Account getRefAccount(){
return this.refAccount;
}
public void setRefAccount(MPAY_Account refAccount){
this.refAccount = refAccount;
}

public static final String NEW_PROFILE = "newProfile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="NEWPROFILEID", nullable=true)
private MPAY_Profile newProfile;
public MPAY_Profile getNewProfile(){
return this.newProfile;
}
public void setNewProfile(MPAY_Profile newProfile){
this.newProfile = newProfile;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=true)
@Transient
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

public static final String SENDER_SERVICE_ACCOUNT_TRANSACTIONS = "senderServiceAccountTransactions";
@OneToMany(mappedBy = "senderServiceAccount")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> senderServiceAccountTransactions;
public List<MPAY_Transaction> getSenderServiceAccountTransactions(){
return this.senderServiceAccountTransactions;
}
public void setSenderServiceAccountTransactions(List<MPAY_Transaction> senderServiceAccountTransactions){
this.senderServiceAccountTransactions = senderServiceAccountTransactions;
}

public static final String RECEIVER_SERVICE_ACCOUNT_TRANSACTIONS = "receiverServiceAccountTransactions";
@OneToMany(mappedBy = "receiverServiceAccount")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> receiverServiceAccountTransactions;
public List<MPAY_Transaction> getReceiverServiceAccountTransactions(){
return this.receiverServiceAccountTransactions;
}
public void setReceiverServiceAccountTransactions(List<MPAY_Transaction> receiverServiceAccountTransactions){
this.receiverServiceAccountTransactions = receiverServiceAccountTransactions;
}

@Override
public String toString() {
return "MPAY_ServiceAccount [id= " + getId() + ", bankedUnbanked= " + getBankedUnbanked() + ", branch= " + getBranch() + ", externalAcc= " + getExternalAcc() + ", iban= " + getIban() + ", mas= " + getMas() + ", isActive= " + getIsActive() + ", isRegistered= " + getIsRegistered() + ", isDefault= " + getIsDefault() + ", isSwitchDefault= " + getIsSwitchDefault() + ", extraData= " + getExtraData() + ", newExternalAcc= " + getNewExternalAcc() + ", approvedData= " + getApprovedData() + ", rejectionNote= " + getRejectionNote() + ", approvalNote= " + getApprovalNote() + ", limits= " + getLimits() + ", changes= " + getChanges() + ", lastTransactionDate= " + getLastTransactionDate() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getBranch() == null) ? 0 : getBranch().hashCode());
result = prime * result + ((getExternalAcc() == null) ? 0 : getExternalAcc().hashCode());
result = prime * result + ((getIban() == null) ? 0 : getIban().hashCode());
int Mas= new Long("null".equals(getMas() + "") ? 0 : getMas()).intValue();
result = prime * result + (int) (Mas ^ Mas >>> 32);
result = prime * result + ((getExtraData() == null) ? 0 : getExtraData().hashCode());
result = prime * result + ((getNewExternalAcc() == null) ? 0 : getNewExternalAcc().hashCode());
result = prime * result + ((getApprovedData() == null) ? 0 : getApprovedData().hashCode());
result = prime * result + ((getLimits() == null) ? 0 : getLimits().hashCode());
result = prime * result + ((getChanges() == null) ? 0 : getChanges().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ServiceAccount))
return false;
else {MPAY_ServiceAccount other = (MPAY_ServiceAccount) obj;
return this.hashCode() == other.hashCode();}
}


}