package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_ServiceCashIn")
@XmlRootElement(name="ServiceCashIn")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ServiceCashIn extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ServiceCashIn(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REF_SERVICE_ACCOUNT = "refServiceAccount";
@Column(name="REFSERVICEACCOUNT", nullable=true, length=50)
private String refServiceAccount;
public String getRefServiceAccount(){
return this.refServiceAccount;
}
public void setRefServiceAccount(String refServiceAccount){
this.refServiceAccount = refServiceAccount;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String IS_CHARGE_INCLUDED = "isChargeIncluded";
@Column(name="ISCHARGEINCLUDED", nullable=true, length=1)
private Boolean isChargeIncluded;
public Boolean getIsChargeIncluded(){
return this.isChargeIncluded;
}
public void setIsChargeIncluded(Boolean isChargeIncluded){
this.isChargeIncluded = isChargeIncluded;
}

public static final String TRANS_AMOUNT = "transAmount";
@Column(name="TRANSAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal transAmount;
public java.math.BigDecimal getTransAmount(){
return this.transAmount;
}
public void setTransAmount(java.math.BigDecimal transAmount){
this.transAmount = transAmount;
}

public static final String CHARGE_AMOUNT = "chargeAmount";
@Column(name="CHARGEAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal chargeAmount;
public java.math.BigDecimal getChargeAmount(){
return this.chargeAmount;
}
public void setChargeAmount(java.math.BigDecimal chargeAmount){
this.chargeAmount = chargeAmount;
}

public static final String TAX_AMOUNT = "taxAmount";
@Column(name="TAXAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal taxAmount;
public java.math.BigDecimal getTaxAmount(){
return this.taxAmount;
}
public void setTaxAmount(java.math.BigDecimal taxAmount){
this.taxAmount = taxAmount;
}

public static final String TOTAL_AMOUNT = "totalAmount";
@Column(name="TOTALAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal totalAmount;
public java.math.BigDecimal getTotalAmount(){
return this.totalAmount;
}
public void setTotalAmount(java.math.BigDecimal totalAmount){
this.totalAmount = totalAmount;
}

public static final String REASON_DESC = "reasonDesc";
@Column(name="REASONDESC", nullable=true, length=4000)
private String reasonDesc;
public String getReasonDesc(){
return this.reasonDesc;
}
public void setReasonDesc(String reasonDesc){
this.reasonDesc = reasonDesc;
}

public static final String COMMENTS = "comments";
@Column(name="COMMENTS", nullable=true, length=255)
private String comments;
public String getComments(){
return this.comments;
}
public void setComments(String comments){
this.comments = comments;
}

public static final String REF_CORPORATE_SERVICE = "refCorporateService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCORPORATESERVICEID", nullable=false)
private MPAY_CorpoarteService refCorporateService;
@flexjson.JSON(include = false)
public MPAY_CorpoarteService getRefCorporateService(){
return this.refCorporateService;
}
public void setRefCorporateService(MPAY_CorpoarteService refCorporateService){
this.refCorporateService = refCorporateService;
}

public static final String REF_LAST_MSG_LOG = "refLastMsgLog";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFLASTMSGLOGID", nullable=true)
private MPAY_MpClearIntegMsgLog refLastMsgLog;
public MPAY_MpClearIntegMsgLog getRefLastMsgLog(){
return this.refLastMsgLog;
}
public void setRefLastMsgLog(MPAY_MpClearIntegMsgLog refLastMsgLog){
this.refLastMsgLog = refLastMsgLog;
}

public static final String REF_MESSAGE = "refMessage";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMESSAGEID", nullable=true)
private MPAY_MPayMessage refMessage;
public MPAY_MPayMessage getRefMessage(){
return this.refMessage;
}
public void setRefMessage(MPAY_MPayMessage refMessage){
this.refMessage = refMessage;
}

public static final String PROCESSING_STATUS = "processingStatus";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PROCESSINGSTATUSID", nullable=true)
private MPAY_ProcessingStatus processingStatus;
public MPAY_ProcessingStatus getProcessingStatus(){
return this.processingStatus;
}
public void setProcessingStatus(MPAY_ProcessingStatus processingStatus){
this.processingStatus = processingStatus;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=true)
private MPAY_Reason reason;
public MPAY_Reason getReason(){
return this.reason;
}
public void setReason(MPAY_Reason reason){
this.reason = reason;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_ServiceCashIn [id= " + getId() + ", refServiceAccount= " + getRefServiceAccount() + ", amount= " + getAmount() + ", isChargeIncluded= " + getIsChargeIncluded() + ", transAmount= " + getTransAmount() + ", chargeAmount= " + getChargeAmount() + ", taxAmount= " + getTaxAmount() + ", totalAmount= " + getTotalAmount() + ", reasonDesc= " + getReasonDesc() + ", comments= " + getComments() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRefServiceAccount() == null) ? 0 : getRefServiceAccount().hashCode());
result = prime * result + ((getReasonDesc() == null) ? 0 : getReasonDesc().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ServiceCashIn))
return false;
else {MPAY_ServiceCashIn other = (MPAY_ServiceCashIn) obj;
return this.hashCode() == other.hashCode();}
}


}