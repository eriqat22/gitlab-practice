package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_NetworkManagements")
@XmlRootElement(name="NetworkManagements")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_NetworkManagement extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_NetworkManagement(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_AUTO_LOGIN = "isAutoLogin";
@Column(name="ISAUTOLOGIN", nullable=false, length=20)
private Boolean isAutoLogin;
public Boolean getIsAutoLogin(){
return this.isAutoLogin;
}
public void setIsAutoLogin(Boolean isAutoLogin){
this.isAutoLogin = isAutoLogin;
}

public static final String LOGIN_EXPIRATION = "loginExpiration";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="LOGINEXPIRATION", nullable=true, length=20)
private java.sql.Timestamp loginExpiration;
public java.sql.Timestamp getLoginExpiration(){
return this.loginExpiration;
}
public void setLoginExpiration(java.sql.Timestamp loginExpiration){
this.loginExpiration = loginExpiration;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String OPERATION_TYPE = "operationType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="OPERATIONTYPEID", nullable=false)
private MPAY_NetworkManActionType operationType;
public MPAY_NetworkManActionType getOperationType(){
return this.operationType;
}
public void setOperationType(MPAY_NetworkManActionType operationType){
this.operationType = operationType;
}

public static final String REF_LAST_MSG_LOG = "refLastMsgLog";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFLASTMSGLOGID", nullable=true)
private MPAY_MpClearIntegMsgLog refLastMsgLog;
public MPAY_MpClearIntegMsgLog getRefLastMsgLog(){
return this.refLastMsgLog;
}
public void setRefLastMsgLog(MPAY_MpClearIntegMsgLog refLastMsgLog){
this.refLastMsgLog = refLastMsgLog;
}

@Override
public String toString() {
return "MPAY_NetworkManagement [id= " + getId() + ", isAutoLogin= " + getIsAutoLogin() + ", loginExpiration= " + getLoginExpiration() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_NetworkManagement))
return false;
else {MPAY_NetworkManagement other = (MPAY_NetworkManagement) obj;
return this.hashCode() == other.hashCode();}
}


}