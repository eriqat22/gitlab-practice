package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_ClientTypes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="ClientTypes")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_ClientType extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ClientType(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_CUSTOMER = "isCustomer";
@Column(name="ISCUSTOMER", nullable=true, length=16)
private Boolean isCustomer;
public Boolean getIsCustomer(){
return this.isCustomer;
}
public void setIsCustomer(Boolean isCustomer){
this.isCustomer = isCustomer;
}

public static final String IS_SYSTEM = "isSystem";
@Column(name="ISSYSTEM", nullable=true, length=16)
private Boolean isSystem;
public Boolean getIsSystem(){
return this.isSystem;
}
public void setIsSystem(Boolean isSystem){
this.isSystem = isSystem;
}

public static final String MAX_NUMBER_OF_OWNERS = "maxNumberOfOwners";
@Column(name="MAXNUMBEROFOWNERS", nullable=false, length=20)
private Long maxNumberOfOwners;
public Long getMaxNumberOfOwners(){
return this.maxNumberOfOwners;
}
public void setMaxNumberOfOwners(Long maxNumberOfOwners){
this.maxNumberOfOwners = maxNumberOfOwners;
}

public static final String MAX_NUMBER_OF_ACCOUNT = "maxNumberOfAccount";
@Column(name="MAXNUMBEROFACCOUNT", nullable=false, length=20)
private Long maxNumberOfAccount;
public Long getMaxNumberOfAccount(){
return this.maxNumberOfAccount;
}
public void setMaxNumberOfAccount(Long maxNumberOfAccount){
this.maxNumberOfAccount = maxNumberOfAccount;
}

public static final String CLIENT_TYPE_N_L_S = "clientTypeNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "clientTypeId")
@MapKey(name="languageCode")
private Map<String, MPAY_ClientType_NLS> clientTypeNLS;
public Map<String, MPAY_ClientType_NLS> getClientTypeNLS(){
return this.clientTypeNLS;
}
public void setClientTypeNLS(Map<String, MPAY_ClientType_NLS> clientTypeNLS){
this.clientTypeNLS = clientTypeNLS;
}

@Override
public String toString() {
return "MPAY_ClientType [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", isCustomer= " + getIsCustomer() + ", isSystem= " + getIsSystem() + ", maxNumberOfOwners= " + getMaxNumberOfOwners() + ", maxNumberOfAccount= " + getMaxNumberOfAccount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
int MaxNumberOfOwners= new Long("null".equals(getMaxNumberOfOwners() + "") ? 0 : getMaxNumberOfOwners()).intValue();
result = prime * result + (int) (MaxNumberOfOwners ^ MaxNumberOfOwners >>> 32);
int MaxNumberOfAccount= new Long("null".equals(getMaxNumberOfAccount() + "") ? 0 : getMaxNumberOfAccount()).intValue();
result = prime * result + (int) (MaxNumberOfAccount ^ MaxNumberOfAccount >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ClientType))
return false;
else {MPAY_ClientType other = (MPAY_ClientType) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.clientTypeNLS;
}
}