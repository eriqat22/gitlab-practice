package com.progressoft.mpay.entities;

public class WF_CashIn
 {
    // Steps
    public static final String STEP_Initialization = "100501";
    public static final String STEP_CalcuationStep = "100506";
    public static final String STEP_PendingReply = "100502";
    public static final String STEP_Accepted = "100503";
    public static final String STEP_Rejected = "100504";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_CalcuationStep = "Calculate Charges";
    public static final String STEP_STATUS_PendingReply = "Pending Reply";
    public static final String STEP_STATUS_Accepted = "Accepted";
    public static final String STEP_STATUS_Rejected = "Rejected";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_SaveandSubmit = "Save and Submit";
    public static final String ACTION_NAME_SVC_Accept = "SVC_Accept";
    public static final String ACTION_NAME_SVC_Reject = "SVC_Reject";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Recalculate = "Recalculate";
    public static final String ACTION_NAME_Resend = "Resend";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SaveandSubmitCalcuationStep = "5";
    public static final String ACTION_CODE_SVC_AcceptCalcuationStep = "9";
    public static final String ACTION_CODE_SVC_RejectCalcuationStep = "10";
    public static final String ACTION_CODE_DeleteCalcuationStep = "6";
    public static final String ACTION_CODE_RecalculateCalcuationStep = "8";
    public static final String ACTION_CODE_SVC_AcceptPendingReply = "3";
    public static final String ACTION_CODE_SVC_RejectPendingReply = "4";
    public static final String ACTION_CODE_ResendPendingReply = "7";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SaveandSubmitCalcuationStep = "572256281";
    public static final String ACTION_KEY_SVC_AcceptCalcuationStep = "1535104178";
    public static final String ACTION_KEY_SVC_RejectCalcuationStep = "205685426";
    public static final String ACTION_KEY_DeleteCalcuationStep = "664254175";
    public static final String ACTION_KEY_RecalculateCalcuationStep = "191583882";
    public static final String ACTION_KEY_SVC_AcceptPendingReply = "1266067784";
    public static final String ACTION_KEY_SVC_RejectPendingReply = "760318567";
    public static final String ACTION_KEY_ResendPendingReply = "36163333";

}