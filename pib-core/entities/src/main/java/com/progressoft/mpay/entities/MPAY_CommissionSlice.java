package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_CommissionSlices",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"REFCOMMISSIONID","NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="CommissionSlices")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CommissionSlice extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CommissionSlice(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String NAME = "name";
@Column(name="NAME", nullable=false, length=255)
private String name;
public String getName(){
return this.name;
}
public void setName(String name){
this.name = name;
}

public static final String REF_COMMISSION = "refCommission";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCOMMISSIONID", nullable=true)
private MPAY_Commission refCommission;
public MPAY_Commission getRefCommission(){
return this.refCommission;
}
public void setRefCommission(MPAY_Commission refCommission){
this.refCommission = refCommission;
}

public static final String REF_SLICE_COMMISSION_PARAMETERS = "refSliceCommissionParameters";
@OneToMany(mappedBy = "refSlice")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CommissionParameter> refSliceCommissionParameters;
public List<MPAY_CommissionParameter> getRefSliceCommissionParameters(){
return this.refSliceCommissionParameters;
}
public void setRefSliceCommissionParameters(List<MPAY_CommissionParameter> refSliceCommissionParameters){
this.refSliceCommissionParameters = refSliceCommissionParameters;
}

@Override
public String toString() {
return "MPAY_CommissionSlice [id= " + getId() + ", name= " + getName() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CommissionSlice))
return false;
else {MPAY_CommissionSlice other = (MPAY_CommissionSlice) obj;
return this.hashCode() == other.hashCode();}
}


}