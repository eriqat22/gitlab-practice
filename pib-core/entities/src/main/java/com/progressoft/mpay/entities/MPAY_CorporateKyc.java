package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.List;

@Entity

@Table(name="MPAY_CorporateKyc",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="CorporateKyc")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CorporateKyc extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CorporateKyc(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String EXPECTED_ACTIVITIES_REQ = "expectedActivitiesReq";
@Column(name="EXPECTEDACTIVITIESREQ", nullable=true, length=20)
private Boolean expectedActivitiesReq;
public Boolean getExpectedActivitiesReq(){
return this.expectedActivitiesReq;
}
public void setExpectedActivitiesReq(Boolean expectedActivitiesReq){
this.expectedActivitiesReq = expectedActivitiesReq;
}

public static final String EXPECTED_ACTIVITIES_VISIBLE = "expectedActivitiesVisible";
@Column(name="EXPECTEDACTIVITIESVISIBLE", nullable=true, length=20)
private Boolean expectedActivitiesVisible;
public Boolean getExpectedActivitiesVisible(){
return this.expectedActivitiesVisible;
}
public void setExpectedActivitiesVisible(Boolean expectedActivitiesVisible){
this.expectedActivitiesVisible = expectedActivitiesVisible;
}

public static final String EXPECTED_ACTIVITIES_ENABLED = "expectedActivitiesEnabled";
@Column(name="EXPECTEDACTIVITIESENABLED", nullable=true, length=20)
private Boolean expectedActivitiesEnabled;
public Boolean getExpectedActivitiesEnabled(){
return this.expectedActivitiesEnabled;
}
public void setExpectedActivitiesEnabled(Boolean expectedActivitiesEnabled){
this.expectedActivitiesEnabled = expectedActivitiesEnabled;
}

public static final String EXPECTED_ACTIVITIES_DEFAULT = "expectedActivitiesDefault";
@Column(name="EXPECTEDACTIVITIESDEFAULT", nullable=true, length=255)
private String expectedActivitiesDefault;
public String getExpectedActivitiesDefault(){
return this.expectedActivitiesDefault;
}
public void setExpectedActivitiesDefault(String expectedActivitiesDefault){
this.expectedActivitiesDefault = expectedActivitiesDefault;
}

public static final String CLIENT_REF_REQ = "clientRefReq";
@Column(name="CLIENTREFREQ", nullable=true, length=20)
private Boolean clientRefReq;
public Boolean getClientRefReq(){
return this.clientRefReq;
}
public void setClientRefReq(Boolean clientRefReq){
this.clientRefReq = clientRefReq;
}

public static final String CLIENT_REF_VISIBLE = "clientRefVisible";
@Column(name="CLIENTREFVISIBLE", nullable=true, length=20)
private Boolean clientRefVisible;
public Boolean getClientRefVisible(){
return this.clientRefVisible;
}
public void setClientRefVisible(Boolean clientRefVisible){
this.clientRefVisible = clientRefVisible;
}

public static final String CLIENT_REF_ENABLED = "clientRefEnabled";
@Column(name="CLIENTREFENABLED", nullable=true, length=20)
private Boolean clientRefEnabled;
public Boolean getClientRefEnabled(){
return this.clientRefEnabled;
}
public void setClientRefEnabled(Boolean clientRefEnabled){
this.clientRefEnabled = clientRefEnabled;
}

public static final String CLIENT_REF_DEFAULT = "clientRefDefault";
@Column(name="CLIENTREFDEFAULT", nullable=true, length=255)
private String clientRefDefault;
public String getClientRefDefault(){
return this.clientRefDefault;
}
public void setClientRefDefault(String clientRefDefault){
this.clientRefDefault = clientRefDefault;
}

public static final String REGISTRATION_AUTHORITY_REQ = "registrationAuthorityReq";
@Column(name="REGISTRATIONAUTHORITYREQ", nullable=true, length=20)
private Boolean registrationAuthorityReq;
public Boolean getRegistrationAuthorityReq(){
return this.registrationAuthorityReq;
}
public void setRegistrationAuthorityReq(Boolean registrationAuthorityReq){
this.registrationAuthorityReq = registrationAuthorityReq;
}

public static final String REGISTRATION_AUTHORITY_VISIBLE = "registrationAuthorityVisible";
@Column(name="REGISTRATIONAUTHORITYVISIBLE", nullable=true, length=20)
private Boolean registrationAuthorityVisible;
public Boolean getRegistrationAuthorityVisible(){
return this.registrationAuthorityVisible;
}
public void setRegistrationAuthorityVisible(Boolean registrationAuthorityVisible){
this.registrationAuthorityVisible = registrationAuthorityVisible;
}

public static final String REGISTRATION_AUTHORITY_ENABLED = "registrationAuthorityEnabled";
@Column(name="REGISTRATIONAUTHORITYENABLED", nullable=true, length=20)
private Boolean registrationAuthorityEnabled;
public Boolean getRegistrationAuthorityEnabled(){
return this.registrationAuthorityEnabled;
}
public void setRegistrationAuthorityEnabled(Boolean registrationAuthorityEnabled){
this.registrationAuthorityEnabled = registrationAuthorityEnabled;
}

public static final String REGISTRATION_AUTHORITY_DEFAULT = "registrationAuthorityDefault";
@Column(name="REGISTRATIONAUTHORITYDEFAULT", nullable=true, length=255)
private String registrationAuthorityDefault;
public String getRegistrationAuthorityDefault(){
return this.registrationAuthorityDefault;
}
public void setRegistrationAuthorityDefault(String registrationAuthorityDefault){
this.registrationAuthorityDefault = registrationAuthorityDefault;
}

public static final String REGISTRATION_LOCATION_REQ = "registrationLocationReq";
@Column(name="REGISTRATIONLOCATIONREQ", nullable=true, length=20)
private Boolean registrationLocationReq;
public Boolean getRegistrationLocationReq(){
return this.registrationLocationReq;
}
public void setRegistrationLocationReq(Boolean registrationLocationReq){
this.registrationLocationReq = registrationLocationReq;
}

public static final String REGISTRATION_LOCATION_VISIBLE = "registrationLocationVisible";
@Column(name="REGISTRATIONLOCATIONVISIBLE", nullable=true, length=20)
private Boolean registrationLocationVisible;
public Boolean getRegistrationLocationVisible(){
return this.registrationLocationVisible;
}
public void setRegistrationLocationVisible(Boolean registrationLocationVisible){
this.registrationLocationVisible = registrationLocationVisible;
}

public static final String REGISTRATION_LOCATION_ENABLED = "registrationLocationEnabled";
@Column(name="REGISTRATIONLOCATIONENABLED", nullable=true, length=20)
private Boolean registrationLocationEnabled;
public Boolean getRegistrationLocationEnabled(){
return this.registrationLocationEnabled;
}
public void setRegistrationLocationEnabled(Boolean registrationLocationEnabled){
this.registrationLocationEnabled = registrationLocationEnabled;
}

public static final String REGISTRATION_LOCATION_DEFAULT = "registrationLocationDefault";
@Column(name="REGISTRATIONLOCATIONDEFAULT", nullable=true, length=255)
private String registrationLocationDefault;
public String getRegistrationLocationDefault(){
return this.registrationLocationDefault;
}
public void setRegistrationLocationDefault(String registrationLocationDefault){
this.registrationLocationDefault = registrationLocationDefault;
}

public static final String CHAMBER_ID_REQ = "chamberIdReq";
@Column(name="CHAMBERIDREQ", nullable=true, length=20)
private Boolean chamberIdReq;
public Boolean getChamberIdReq(){
return this.chamberIdReq;
}
public void setChamberIdReq(Boolean chamberIdReq){
this.chamberIdReq = chamberIdReq;
}

public static final String CHAMBER_ID_VISIBLE = "chamberIdVisible";
@Column(name="CHAMBERIDVISIBLE", nullable=true, length=20)
private Boolean chamberIdVisible;
public Boolean getChamberIdVisible(){
return this.chamberIdVisible;
}
public void setChamberIdVisible(Boolean chamberIdVisible){
this.chamberIdVisible = chamberIdVisible;
}

public static final String CHAMBER_ID_ENABLED = "chamberIdEnabled";
@Column(name="CHAMBERIDENABLED", nullable=true, length=20)
private Boolean chamberIdEnabled;
public Boolean getChamberIdEnabled(){
return this.chamberIdEnabled;
}
public void setChamberIdEnabled(Boolean chamberIdEnabled){
this.chamberIdEnabled = chamberIdEnabled;
}

public static final String CHAMBER_ID_DEFAULT = "chamberIdDefault";
@Column(name="CHAMBERIDDEFAULT", nullable=true, length=255)
private String chamberIdDefault;
public String getChamberIdDefault(){
return this.chamberIdDefault;
}
public void setChamberIdDefault(String chamberIdDefault){
this.chamberIdDefault = chamberIdDefault;
}

public static final String LEGAL_ENTITY_REQ = "legalEntityReq";
@Column(name="LEGALENTITYREQ", nullable=true, length=20)
private Boolean legalEntityReq;
public Boolean getLegalEntityReq(){
return this.legalEntityReq;
}
public void setLegalEntityReq(Boolean legalEntityReq){
this.legalEntityReq = legalEntityReq;
}

public static final String LEGAL_ENTITY_VISIBLE = "legalEntityVisible";
@Column(name="LEGALENTITYVISIBLE", nullable=true, length=20)
private Boolean legalEntityVisible;
public Boolean getLegalEntityVisible(){
return this.legalEntityVisible;
}
public void setLegalEntityVisible(Boolean legalEntityVisible){
this.legalEntityVisible = legalEntityVisible;
}

public static final String LEGAL_ENTITY_ENABLED = "legalEntityEnabled";
@Column(name="LEGALENTITYENABLED", nullable=true, length=20)
private Boolean legalEntityEnabled;
public Boolean getLegalEntityEnabled(){
return this.legalEntityEnabled;
}
public void setLegalEntityEnabled(Boolean legalEntityEnabled){
this.legalEntityEnabled = legalEntityEnabled;
}

public static final String OTHER_LEGAL_ENTITY_REQ = "otherLegalEntityReq";
@Column(name="OTHERLEGALENTITYREQ", nullable=true, length=20)
private Boolean otherLegalEntityReq;
public Boolean getOtherLegalEntityReq(){
return this.otherLegalEntityReq;
}
public void setOtherLegalEntityReq(Boolean otherLegalEntityReq){
this.otherLegalEntityReq = otherLegalEntityReq;
}

public static final String OTHER_LEGAL_ENTITY_VISIBLE = "otherLegalEntityVisible";
@Column(name="OTHERLEGALENTITYVISIBLE", nullable=true, length=20)
private Boolean otherLegalEntityVisible;
public Boolean getOtherLegalEntityVisible(){
return this.otherLegalEntityVisible;
}
public void setOtherLegalEntityVisible(Boolean otherLegalEntityVisible){
this.otherLegalEntityVisible = otherLegalEntityVisible;
}

public static final String OTHER_LEGAL_ENTITY_ENABLED = "otherLegalEntityEnabled";
@Column(name="OTHERLEGALENTITYENABLED", nullable=true, length=20)
private Boolean otherLegalEntityEnabled;
public Boolean getOtherLegalEntityEnabled(){
return this.otherLegalEntityEnabled;
}
public void setOtherLegalEntityEnabled(Boolean otherLegalEntityEnabled){
this.otherLegalEntityEnabled = otherLegalEntityEnabled;
}

public static final String OTHER_LEGAL_ENTITY_DEFAULT = "otherLegalEntityDefault";
@Column(name="OTHERLEGALENTITYDEFAULT", nullable=true, length=255)
private String otherLegalEntityDefault;
public String getOtherLegalEntityDefault(){
return this.otherLegalEntityDefault;
}
public void setOtherLegalEntityDefault(String otherLegalEntityDefault){
this.otherLegalEntityDefault = otherLegalEntityDefault;
}

public static final String PHONE_ONE_REQ = "phoneOneReq";
@Column(name="PHONEONEREQ", nullable=true, length=20)
private Boolean phoneOneReq;
public Boolean getPhoneOneReq(){
return this.phoneOneReq;
}
public void setPhoneOneReq(Boolean phoneOneReq){
this.phoneOneReq = phoneOneReq;
}

public static final String PHONE_ONE_VISIBLE = "phoneOneVisible";
@Column(name="PHONEONEVISIBLE", nullable=true, length=20)
private Boolean phoneOneVisible;
public Boolean getPhoneOneVisible(){
return this.phoneOneVisible;
}
public void setPhoneOneVisible(Boolean phoneOneVisible){
this.phoneOneVisible = phoneOneVisible;
}

public static final String PHONE_ONE_ENABLED = "phoneOneEnabled";
@Column(name="PHONEONEENABLED", nullable=true, length=20)
private Boolean phoneOneEnabled;
public Boolean getPhoneOneEnabled(){
return this.phoneOneEnabled;
}
public void setPhoneOneEnabled(Boolean phoneOneEnabled){
this.phoneOneEnabled = phoneOneEnabled;
}

public static final String PHONE_ONE_DEFAULT = "phoneOneDefault";
@Column(name="PHONEONEDEFAULT", nullable=true, length=255)
private String phoneOneDefault;
public String getPhoneOneDefault(){
return this.phoneOneDefault;
}
public void setPhoneOneDefault(String phoneOneDefault){
this.phoneOneDefault = phoneOneDefault;
}

public static final String PHONE_TWO_REQ = "phoneTwoReq";
@Column(name="PHONETWOREQ", nullable=true, length=20)
private Boolean phoneTwoReq;
public Boolean getPhoneTwoReq(){
return this.phoneTwoReq;
}
public void setPhoneTwoReq(Boolean phoneTwoReq){
this.phoneTwoReq = phoneTwoReq;
}

public static final String PHONE_TWO_VISIBLE = "phoneTwoVisible";
@Column(name="PHONETWOVISIBLE", nullable=true, length=20)
private Boolean phoneTwoVisible;
public Boolean getPhoneTwoVisible(){
return this.phoneTwoVisible;
}
public void setPhoneTwoVisible(Boolean phoneTwoVisible){
this.phoneTwoVisible = phoneTwoVisible;
}

public static final String PHONE_TWO_ENABLED = "phoneTwoEnabled";
@Column(name="PHONETWOENABLED", nullable=true, length=20)
private Boolean phoneTwoEnabled;
public Boolean getPhoneTwoEnabled(){
return this.phoneTwoEnabled;
}
public void setPhoneTwoEnabled(Boolean phoneTwoEnabled){
this.phoneTwoEnabled = phoneTwoEnabled;
}

public static final String PHONE_TWO_DEFAULT = "phoneTwoDefault";
@Column(name="PHONETWODEFAULT", nullable=true, length=255)
private String phoneTwoDefault;
public String getPhoneTwoDefault(){
return this.phoneTwoDefault;
}
public void setPhoneTwoDefault(String phoneTwoDefault){
this.phoneTwoDefault = phoneTwoDefault;
}

public static final String POBOX_REQ = "poboxReq";
@Column(name="POBOXREQ", nullable=true, length=20)
private Boolean poboxReq;
public Boolean getPoboxReq(){
return this.poboxReq;
}
public void setPoboxReq(Boolean poboxReq){
this.poboxReq = poboxReq;
}

public static final String POBOX_VISIBLE = "poboxVisible";
@Column(name="POBOXVISIBLE", nullable=true, length=20)
private Boolean poboxVisible;
public Boolean getPoboxVisible(){
return this.poboxVisible;
}
public void setPoboxVisible(Boolean poboxVisible){
this.poboxVisible = poboxVisible;
}

public static final String POBOX_ENABLED = "poboxEnabled";
@Column(name="POBOXENABLED", nullable=true, length=20)
private Boolean poboxEnabled;
public Boolean getPoboxEnabled(){
return this.poboxEnabled;
}
public void setPoboxEnabled(Boolean poboxEnabled){
this.poboxEnabled = poboxEnabled;
}

public static final String POBOX_DEFAULT = "poboxDefault";
@Column(name="POBOXDEFAULT", nullable=true, length=255)
private String poboxDefault;
public String getPoboxDefault(){
return this.poboxDefault;
}
public void setPoboxDefault(String poboxDefault){
this.poboxDefault = poboxDefault;
}

public static final String ZIP_CODE_REQ = "zipCodeReq";
@Column(name="ZIPCODEREQ", nullable=true, length=20)
private Boolean zipCodeReq;
public Boolean getZipCodeReq(){
return this.zipCodeReq;
}
public void setZipCodeReq(Boolean zipCodeReq){
this.zipCodeReq = zipCodeReq;
}

public static final String ZIP_CODE_VISIBLE = "zipCodeVisible";
@Column(name="ZIPCODEVISIBLE", nullable=true, length=20)
private Boolean zipCodeVisible;
public Boolean getZipCodeVisible(){
return this.zipCodeVisible;
}
public void setZipCodeVisible(Boolean zipCodeVisible){
this.zipCodeVisible = zipCodeVisible;
}

public static final String ZIP_CODE_ENABLED = "zipCodeEnabled";
@Column(name="ZIPCODEENABLED", nullable=true, length=20)
private Boolean zipCodeEnabled;
public Boolean getZipCodeEnabled(){
return this.zipCodeEnabled;
}
public void setZipCodeEnabled(Boolean zipCodeEnabled){
this.zipCodeEnabled = zipCodeEnabled;
}

public static final String ZIP_CODE_DEFAULT = "zipCodeDefault";
@Column(name="ZIPCODEDEFAULT", nullable=true, length=255)
private String zipCodeDefault;
public String getZipCodeDefault(){
return this.zipCodeDefault;
}
public void setZipCodeDefault(String zipCodeDefault){
this.zipCodeDefault = zipCodeDefault;
}

public static final String BUILDING_NUM_REQ = "buildingNumReq";
@Column(name="BUILDINGNUMREQ", nullable=true, length=20)
private Boolean buildingNumReq;
public Boolean getBuildingNumReq(){
return this.buildingNumReq;
}
public void setBuildingNumReq(Boolean buildingNumReq){
this.buildingNumReq = buildingNumReq;
}

public static final String BUILDING_NUM_VISIBLE = "buildingNumVisible";
@Column(name="BUILDINGNUMVISIBLE", nullable=true, length=20)
private Boolean buildingNumVisible;
public Boolean getBuildingNumVisible(){
return this.buildingNumVisible;
}
public void setBuildingNumVisible(Boolean buildingNumVisible){
this.buildingNumVisible = buildingNumVisible;
}

public static final String BUILDING_NUM_ENABLED = "buildingNumEnabled";
@Column(name="BUILDINGNUMENABLED", nullable=true, length=20)
private Boolean buildingNumEnabled;
public Boolean getBuildingNumEnabled(){
return this.buildingNumEnabled;
}
public void setBuildingNumEnabled(Boolean buildingNumEnabled){
this.buildingNumEnabled = buildingNumEnabled;
}

public static final String BUILDING_NUM_DEFAULT = "buildingNumDefault";
@Column(name="BUILDINGNUMDEFAULT", nullable=true, length=255)
private String buildingNumDefault;
public String getBuildingNumDefault(){
return this.buildingNumDefault;
}
public void setBuildingNumDefault(String buildingNumDefault){
this.buildingNumDefault = buildingNumDefault;
}

public static final String STREET_NAME_REQ = "streetNameReq";
@Column(name="STREETNAMEREQ", nullable=true, length=20)
private Boolean streetNameReq;
public Boolean getStreetNameReq(){
return this.streetNameReq;
}
public void setStreetNameReq(Boolean streetNameReq){
this.streetNameReq = streetNameReq;
}

public static final String STREET_NAME_VISIBLE = "streetNameVisible";
@Column(name="STREETNAMEVISIBLE", nullable=true, length=20)
private Boolean streetNameVisible;
public Boolean getStreetNameVisible(){
return this.streetNameVisible;
}
public void setStreetNameVisible(Boolean streetNameVisible){
this.streetNameVisible = streetNameVisible;
}

public static final String STREET_NAME_ENABLED = "streetNameEnabled";
@Column(name="STREETNAMEENABLED", nullable=true, length=20)
private Boolean streetNameEnabled;
public Boolean getStreetNameEnabled(){
return this.streetNameEnabled;
}
public void setStreetNameEnabled(Boolean streetNameEnabled){
this.streetNameEnabled = streetNameEnabled;
}

public static final String STREET_NAME_DEFAULT = "streetNameDefault";
@Column(name="STREETNAMEDEFAULT", nullable=true, length=255)
private String streetNameDefault;
public String getStreetNameDefault(){
return this.streetNameDefault;
}
public void setStreetNameDefault(String streetNameDefault){
this.streetNameDefault = streetNameDefault;
}

public static final String LOCATION_REQ = "locationReq";
@Column(name="LOCATIONREQ", nullable=true, length=20)
private Boolean locationReq;
public Boolean getLocationReq(){
return this.locationReq;
}
public void setLocationReq(Boolean locationReq){
this.locationReq = locationReq;
}

public static final String LOCATION_VISIBLE = "locationVisible";
@Column(name="LOCATIONVISIBLE", nullable=true, length=20)
private Boolean locationVisible;
public Boolean getLocationVisible(){
return this.locationVisible;
}
public void setLocationVisible(Boolean locationVisible){
this.locationVisible = locationVisible;
}

public static final String LOCATION_ENABLED = "locationEnabled";
@Column(name="LOCATIONENABLED", nullable=true, length=20)
private Boolean locationEnabled;
public Boolean getLocationEnabled(){
return this.locationEnabled;
}
public void setLocationEnabled(Boolean locationEnabled){
this.locationEnabled = locationEnabled;
}

public static final String LOCATION_DEFAULT = "locationDefault";
@Column(name="LOCATIONDEFAULT", nullable=true, length=255)
private String locationDefault;
public String getLocationDefault(){
return this.locationDefault;
}
public void setLocationDefault(String locationDefault){
this.locationDefault = locationDefault;
}

public static final String LEGAL_ENTITY_DEFAULT = "legalEntityDefault";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="LEGALENTITYDEFAULTID", nullable=true)
private MPAY_CorpLegalEntity legalEntityDefault;
public MPAY_CorpLegalEntity getLegalEntityDefault(){
return this.legalEntityDefault;
}
public void setLegalEntityDefault(MPAY_CorpLegalEntity legalEntityDefault){
this.legalEntityDefault = legalEntityDefault;
}

public static final String PROFILES = "profiles";
@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.MERGE })
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinTable(name="MPAY_CorporateKycProfiles", joinColumns = @JoinColumn(name = "CORPORATEKYC_ID"), inverseJoinColumns = @JoinColumn(name = "PROFILES_ID"))
private List<MPAY_Profile> profiles = new ArrayList<MPAY_Profile>();
public List<MPAY_Profile> getProfiles(){
return this.profiles;
}
public void setProfiles(List<MPAY_Profile> profiles){
this.profiles = profiles;
}

public static final String REF_KYC_CORP_KYC_ATT_TYPES = "refKycCorpKycAttTypes";
@OneToMany(mappedBy = "refKyc")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CorpKycAttType> refKycCorpKycAttTypes;
public List<MPAY_CorpKycAttType> getRefKycCorpKycAttTypes(){
return this.refKycCorpKycAttTypes;
}
public void setRefKycCorpKycAttTypes(List<MPAY_CorpKycAttType> refKycCorpKycAttTypes){
this.refKycCorpKycAttTypes = refKycCorpKycAttTypes;
}

@Override
public String toString() {
return "MPAY_CorporateKyc [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", expectedActivitiesReq= " + getExpectedActivitiesReq() + ", expectedActivitiesVisible= " + getExpectedActivitiesVisible() + ", expectedActivitiesEnabled= " + getExpectedActivitiesEnabled() + ", expectedActivitiesDefault= " + getExpectedActivitiesDefault() + ", clientRefReq= " + getClientRefReq() + ", clientRefVisible= " + getClientRefVisible() + ", clientRefEnabled= " + getClientRefEnabled() + ", clientRefDefault= " + getClientRefDefault() + ", registrationAuthorityReq= " + getRegistrationAuthorityReq() + ", registrationAuthorityVisible= " + getRegistrationAuthorityVisible() + ", registrationAuthorityEnabled= " + getRegistrationAuthorityEnabled() + ", registrationAuthorityDefault= " + getRegistrationAuthorityDefault() + ", registrationLocationReq= " + getRegistrationLocationReq() + ", registrationLocationVisible= " + getRegistrationLocationVisible() + ", registrationLocationEnabled= " + getRegistrationLocationEnabled() + ", registrationLocationDefault= " + getRegistrationLocationDefault() + ", chamberIdReq= " + getChamberIdReq() + ", chamberIdVisible= " + getChamberIdVisible() + ", chamberIdEnabled= " + getChamberIdEnabled() + ", chamberIdDefault= " + getChamberIdDefault() + ", legalEntityReq= " + getLegalEntityReq() + ", legalEntityVisible= " + getLegalEntityVisible() + ", legalEntityEnabled= " + getLegalEntityEnabled() + ", otherLegalEntityReq= " + getOtherLegalEntityReq() + ", otherLegalEntityVisible= " + getOtherLegalEntityVisible() + ", otherLegalEntityEnabled= " + getOtherLegalEntityEnabled() + ", otherLegalEntityDefault= " + getOtherLegalEntityDefault() + ", phoneOneReq= " + getPhoneOneReq() + ", phoneOneVisible= " + getPhoneOneVisible() + ", phoneOneEnabled= " + getPhoneOneEnabled() + ", phoneOneDefault= " + getPhoneOneDefault() + ", phoneTwoReq= " + getPhoneTwoReq() + ", phoneTwoVisible= " + getPhoneTwoVisible() + ", phoneTwoEnabled= " + getPhoneTwoEnabled() + ", phoneTwoDefault= " + getPhoneTwoDefault() + ", poboxReq= " + getPoboxReq() + ", poboxVisible= " + getPoboxVisible() + ", poboxEnabled= " + getPoboxEnabled() + ", poboxDefault= " + getPoboxDefault() + ", zipCodeReq= " + getZipCodeReq() + ", zipCodeVisible= " + getZipCodeVisible() + ", zipCodeEnabled= " + getZipCodeEnabled() + ", zipCodeDefault= " + getZipCodeDefault() + ", buildingNumReq= " + getBuildingNumReq() + ", buildingNumVisible= " + getBuildingNumVisible() + ", buildingNumEnabled= " + getBuildingNumEnabled() + ", buildingNumDefault= " + getBuildingNumDefault() + ", streetNameReq= " + getStreetNameReq() + ", streetNameVisible= " + getStreetNameVisible() + ", streetNameEnabled= " + getStreetNameEnabled() + ", streetNameDefault= " + getStreetNameDefault() + ", locationReq= " + getLocationReq() + ", locationVisible= " + getLocationVisible() + ", locationEnabled= " + getLocationEnabled() + ", locationDefault= " + getLocationDefault() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getExpectedActivitiesDefault() == null) ? 0 : getExpectedActivitiesDefault().hashCode());
result = prime * result + ((getClientRefDefault() == null) ? 0 : getClientRefDefault().hashCode());
result = prime * result + ((getRegistrationAuthorityDefault() == null) ? 0 : getRegistrationAuthorityDefault().hashCode());
result = prime * result + ((getRegistrationLocationDefault() == null) ? 0 : getRegistrationLocationDefault().hashCode());
result = prime * result + ((getChamberIdDefault() == null) ? 0 : getChamberIdDefault().hashCode());
result = prime * result + ((getOtherLegalEntityDefault() == null) ? 0 : getOtherLegalEntityDefault().hashCode());
result = prime * result + ((getPhoneOneDefault() == null) ? 0 : getPhoneOneDefault().hashCode());
result = prime * result + ((getPhoneTwoDefault() == null) ? 0 : getPhoneTwoDefault().hashCode());
result = prime * result + ((getPoboxDefault() == null) ? 0 : getPoboxDefault().hashCode());
result = prime * result + ((getZipCodeDefault() == null) ? 0 : getZipCodeDefault().hashCode());
result = prime * result + ((getBuildingNumDefault() == null) ? 0 : getBuildingNumDefault().hashCode());
result = prime * result + ((getStreetNameDefault() == null) ? 0 : getStreetNameDefault().hashCode());
result = prime * result + ((getLocationDefault() == null) ? 0 : getLocationDefault().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CorporateKyc))
return false;
else {MPAY_CorporateKyc other = (MPAY_CorporateKyc) obj;
return this.hashCode() == other.hashCode();}
}


}