package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_ChargesSchemes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="ChargesSchemes")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ChargesScheme extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ChargesScheme(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

public static final String REF_CHARGES_SCHEME_CHARGES_SCHEME_DETAILS = "refChargesSchemeChargesSchemeDetails";
@OneToMany(mappedBy = "refChargesScheme")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ChargesSchemeDetail> refChargesSchemeChargesSchemeDetails;
public List<MPAY_ChargesSchemeDetail> getRefChargesSchemeChargesSchemeDetails(){
return this.refChargesSchemeChargesSchemeDetails;
}
public void setRefChargesSchemeChargesSchemeDetails(List<MPAY_ChargesSchemeDetail> refChargesSchemeChargesSchemeDetails){
this.refChargesSchemeChargesSchemeDetails = refChargesSchemeChargesSchemeDetails;
}

@Override
public String toString() {
return "MPAY_ChargesScheme [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", isActive= " + getIsActive() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ChargesScheme))
return false;
else {MPAY_ChargesScheme other = (MPAY_ChargesScheme) obj;
return this.hashCode() == other.hashCode();}
}


}