package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_ChargesSchemeDetails",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"REFCHARGESSCHEMEID","MSGTYPEID","PSPSCOPE","Z_TENANT_ID"})
})
@XmlRootElement(name="ChargesSchemeDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ChargesSchemeDetail extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ChargesSchemeDetail(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String SEND_CHARGE_BEARER_PERCENT = "sendChargeBearerPercent";
@Column(name="SENDCHARGEBEARERPERCENT", nullable=false, length=20)
private Long sendChargeBearerPercent;
public Long getSendChargeBearerPercent(){
return this.sendChargeBearerPercent;
}
public void setSendChargeBearerPercent(Long sendChargeBearerPercent){
this.sendChargeBearerPercent = sendChargeBearerPercent;
}

public static final String RECEIVE_CHARGE_BEARER_PERCENT = "receiveChargeBearerPercent";
@Column(name="RECEIVECHARGEBEARERPERCENT", nullable=false, length=20)
private Long receiveChargeBearerPercent;
public Long getReceiveChargeBearerPercent(){
return this.receiveChargeBearerPercent;
}
public void setReceiveChargeBearerPercent(Long receiveChargeBearerPercent){
this.receiveChargeBearerPercent = receiveChargeBearerPercent;
}

public static final String SNDR_CHRG_BN_BNFCRY_PRCNT = "sndrChrgBnBnfcryPrcnt";
@Column(name="SNDRCHRGBNBNFCRYPRCNT", nullable=false, length=20)
private Long sndrChrgBnBnfcryPrcnt;
public Long getSndrChrgBnBnfcryPrcnt(){
return this.sndrChrgBnBnfcryPrcnt;
}
public void setSndrChrgBnBnfcryPrcnt(Long sndrChrgBnBnfcryPrcnt){
this.sndrChrgBnBnfcryPrcnt = sndrChrgBnBnfcryPrcnt;
}

public static final String RCVR_CHRG_BNK_BNFCRY_PRCNT = "rcvrChrgBnkBnfcryPrcnt";
@Column(name="RCVRCHRGBNKBNFCRYPRCNT", nullable=false, length=20)
private Long rcvrChrgBnkBnfcryPrcnt;
public Long getRcvrChrgBnkBnfcryPrcnt(){
return this.rcvrChrgBnkBnfcryPrcnt;
}
public void setRcvrChrgBnkBnfcryPrcnt(Long rcvrChrgBnkBnfcryPrcnt){
this.rcvrChrgBnkBnfcryPrcnt = rcvrChrgBnkBnfcryPrcnt;
}

public static final String SNDR_CHRG_M_N_O_BNFCRY_PRCNT = "sndrChrgMNOBnfcryPrcnt";
@Column(name="SNDRCHRGMNOBNFCRYPRCNT", nullable=false, length=20)
private Long sndrChrgMNOBnfcryPrcnt;
public Long getSndrChrgMNOBnfcryPrcnt(){
return this.sndrChrgMNOBnfcryPrcnt;
}
public void setSndrChrgMNOBnfcryPrcnt(Long sndrChrgMNOBnfcryPrcnt){
this.sndrChrgMNOBnfcryPrcnt = sndrChrgMNOBnfcryPrcnt;
}

public static final String RCV_CHRG_M_N_O_BNFCRY_PRCNT = "rcvChrgMNOBnfcryPrcnt";
@Column(name="RCVCHRGMNOBNFCRYPRCNT", nullable=false, length=20)
private Long rcvChrgMNOBnfcryPrcnt;
public Long getRcvChrgMNOBnfcryPrcnt(){
return this.rcvChrgMNOBnfcryPrcnt;
}
public void setRcvChrgMNOBnfcryPrcnt(Long rcvChrgMNOBnfcryPrcnt){
this.rcvChrgMNOBnfcryPrcnt = rcvChrgMNOBnfcryPrcnt;
}

public static final String CHARGE_P_S_P_PERCENT = "chargePSPPercent";
@Column(name="CHARGEPSPPERCENT", nullable=false, length=20)
private Long chargePSPPercent;
public Long getChargePSPPercent(){
return this.chargePSPPercent;
}
public void setChargePSPPercent(Long chargePSPPercent){
this.chargePSPPercent = chargePSPPercent;
}

public static final String CHARGE_M_P_CLEARL_PERCENT = "chargeMPClearlPercent";
@Column(name="CHARGEMPCLEARLPERCENT", nullable=false, length=20)
private Long chargeMPClearlPercent;
public Long getChargeMPClearlPercent(){
return this.chargeMPClearlPercent;
}
public void setChargeMPClearlPercent(Long chargeMPClearlPercent){
this.chargeMPClearlPercent = chargeMPClearlPercent;
}

public static final String PSP_SCOPE = "pspScope";
@Column(name="PSPSCOPE", nullable=true, length=20)
private String pspScope;
public String getPspScope(){
return this.pspScope;
}
public void setPspScope(String pspScope){
this.pspScope = pspScope;
}

public static final String REF_CHARGES_SCHEME = "refChargesScheme";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCHARGESSCHEMEID", nullable=true)
private MPAY_ChargesScheme refChargesScheme;
public MPAY_ChargesScheme getRefChargesScheme(){
return this.refChargesScheme;
}
public void setRefChargesScheme(MPAY_ChargesScheme refChargesScheme){
this.refChargesScheme = refChargesScheme;
}

public static final String MSG_TYPE = "msgType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MSGTYPEID", nullable=false)
private MPAY_MessageType msgType;
public MPAY_MessageType getMsgType(){
return this.msgType;
}
public void setMsgType(MPAY_MessageType msgType){
this.msgType = msgType;
}

public static final String REF_CHARGES_DETAILS_CHARGES_SLICES = "refChargesDetailsChargesSlices";
@OneToMany(mappedBy = "refChargesDetails")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ChargesSlice> refChargesDetailsChargesSlices;
public List<MPAY_ChargesSlice> getRefChargesDetailsChargesSlices(){
return this.refChargesDetailsChargesSlices;
}
public void setRefChargesDetailsChargesSlices(List<MPAY_ChargesSlice> refChargesDetailsChargesSlices){
this.refChargesDetailsChargesSlices = refChargesDetailsChargesSlices;
}

@Override
public String toString() {
return "MPAY_ChargesSchemeDetail [id= " + getId() + ", description= " + getDescription() + ", isActive= " + getIsActive() + ", sendChargeBearerPercent= " + getSendChargeBearerPercent() + ", receiveChargeBearerPercent= " + getReceiveChargeBearerPercent() + ", sndrChrgBnBnfcryPrcnt= " + getSndrChrgBnBnfcryPrcnt() + ", rcvrChrgBnkBnfcryPrcnt= " + getRcvrChrgBnkBnfcryPrcnt() + ", sndrChrgMNOBnfcryPrcnt= " + getSndrChrgMNOBnfcryPrcnt() + ", rcvChrgMNOBnfcryPrcnt= " + getRcvChrgMNOBnfcryPrcnt() + ", chargePSPPercent= " + getChargePSPPercent() + ", chargeMPClearlPercent= " + getChargeMPClearlPercent() + ", pspScope= " + getPspScope() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
int SendChargeBearerPercent= new Long("null".equals(getSendChargeBearerPercent() + "") ? 0 : getSendChargeBearerPercent()).intValue();
result = prime * result + (int) (SendChargeBearerPercent ^ SendChargeBearerPercent >>> 32);
int ReceiveChargeBearerPercent= new Long("null".equals(getReceiveChargeBearerPercent() + "") ? 0 : getReceiveChargeBearerPercent()).intValue();
result = prime * result + (int) (ReceiveChargeBearerPercent ^ ReceiveChargeBearerPercent >>> 32);
int SndrChrgBnBnfcryPrcnt= new Long("null".equals(getSndrChrgBnBnfcryPrcnt() + "") ? 0 : getSndrChrgBnBnfcryPrcnt()).intValue();
result = prime * result + (int) (SndrChrgBnBnfcryPrcnt ^ SndrChrgBnBnfcryPrcnt >>> 32);
int RcvrChrgBnkBnfcryPrcnt= new Long("null".equals(getRcvrChrgBnkBnfcryPrcnt() + "") ? 0 : getRcvrChrgBnkBnfcryPrcnt()).intValue();
result = prime * result + (int) (RcvrChrgBnkBnfcryPrcnt ^ RcvrChrgBnkBnfcryPrcnt >>> 32);
int SndrChrgMNOBnfcryPrcnt= new Long("null".equals(getSndrChrgMNOBnfcryPrcnt() + "") ? 0 : getSndrChrgMNOBnfcryPrcnt()).intValue();
result = prime * result + (int) (SndrChrgMNOBnfcryPrcnt ^ SndrChrgMNOBnfcryPrcnt >>> 32);
int RcvChrgMNOBnfcryPrcnt= new Long("null".equals(getRcvChrgMNOBnfcryPrcnt() + "") ? 0 : getRcvChrgMNOBnfcryPrcnt()).intValue();
result = prime * result + (int) (RcvChrgMNOBnfcryPrcnt ^ RcvChrgMNOBnfcryPrcnt >>> 32);
int ChargePSPPercent= new Long("null".equals(getChargePSPPercent() + "") ? 0 : getChargePSPPercent()).intValue();
result = prime * result + (int) (ChargePSPPercent ^ ChargePSPPercent >>> 32);
int ChargeMPClearlPercent= new Long("null".equals(getChargeMPClearlPercent() + "") ? 0 : getChargeMPClearlPercent()).intValue();
result = prime * result + (int) (ChargeMPClearlPercent ^ ChargeMPClearlPercent >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ChargesSchemeDetail))
return false;
else {MPAY_ChargesSchemeDetail other = (MPAY_ChargesSchemeDetail) obj;
return this.hashCode() == other.hashCode();}
}


}