package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_BalancesRpt")
@XmlRootElement(name="BalancesRpt")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_BalancesRpt extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_BalancesRpt(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String TOTAL_AMOUNT = "totalAmount";
@Column(name="TOTALAMOUNT", nullable=true, precision=21, scale=8, length=50)
private java.math.BigDecimal totalAmount;
public java.math.BigDecimal getTotalAmount(){
return this.totalAmount;
}
public void setTotalAmount(java.math.BigDecimal totalAmount){
this.totalAmount = totalAmount;
}

public static final String CATEGORY = "category";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CATEGORYID", nullable=true)
private MPAY_AccountCategory category;
public MPAY_AccountCategory getCategory(){
return this.category;
}
public void setCategory(MPAY_AccountCategory category){
this.category = category;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=true)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_BalancesRpt [id= " + getId() + ", totalAmount= " + getTotalAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_BalancesRpt))
return false;
else {MPAY_BalancesRpt other = (MPAY_BalancesRpt) obj;
return this.hashCode() == other.hashCode();}
}


}