package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_TransCommissionsRpt")
@XmlRootElement(name="TransCommissionsRpt")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_TransCommissionsRpt extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_TransCommissionsRpt(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String BALANCE_BEFORE = "balanceBefore";
@Column(name="BALANCEBEFORE", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal balanceBefore;
public java.math.BigDecimal getBalanceBefore(){
return this.balanceBefore;
}
public void setBalanceBefore(java.math.BigDecimal balanceBefore){
this.balanceBefore = balanceBefore;
}

public static final String BALANCE_AFTER = "balanceAfter";
@Column(name="BALANCEAFTER", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal balanceAfter;
public java.math.BigDecimal getBalanceAfter(){
return this.balanceAfter;
}
public void setBalanceAfter(java.math.BigDecimal balanceAfter){
this.balanceAfter = balanceAfter;
}

public static final String DEBIT_CREDIT_FLAG = "debitCreditFlag";
@Column(name="DEBITCREDITFLAG", nullable=true, length=1)
private String debitCreditFlag;
public String getDebitCreditFlag(){
return this.debitCreditFlag;
}
public void setDebitCreditFlag(String debitCreditFlag){
this.debitCreditFlag = debitCreditFlag;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String CATEGORY = "category";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CATEGORYID", nullable=true)
private MPAY_AccountCategory category;
public MPAY_AccountCategory getCategory(){
return this.category;
}
public void setCategory(MPAY_AccountCategory category){
this.category = category;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=true)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_TransCommissionsRpt [id= " + getId() + ", amount= " + getAmount() + ", balanceBefore= " + getBalanceBefore() + ", balanceAfter= " + getBalanceAfter() + ", debitCreditFlag= " + getDebitCreditFlag() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_TransCommissionsRpt))
return false;
else {MPAY_TransCommissionsRpt other = (MPAY_TransCommissionsRpt) obj;
return this.hashCode() == other.hashCode();}
}


}