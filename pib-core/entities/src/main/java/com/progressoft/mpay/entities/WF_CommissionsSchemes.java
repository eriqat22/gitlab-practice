package com.progressoft.mpay.entities;

public class WF_CommissionsSchemes
 {
    // Steps
    public static final String STEP_Initialization = "211801";
    public static final String STEP_CreationApproval = "211802";
    public static final String STEP_RepairNew = "211803";
    public static final String STEP_EditNew = "211804";
    public static final String STEP_ViewApproved = "211805";
    public static final String STEP_EditApproved = "211806";
    public static final String STEP_ModificationApproval = "211807";
    public static final String STEP_RepairRejected = "211808";
    public static final String STEP_EditRejected = "211809";
    public static final String STEP_DeletionApproval = "211810";
    public static final String STEP_End = "211811";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_CreationApproval = "New Authorization";
    public static final String STEP_STATUS_RepairNew = "Repair New";
    public static final String STEP_STATUS_EditNew = "Edit New";
    public static final String STEP_STATUS_ViewApproved = "Approved";
    public static final String STEP_STATUS_EditApproved = "Modify";
    public static final String STEP_STATUS_ModificationApproval = "Authorization";
    public static final String STEP_STATUS_RepairRejected = "Repair Rejected";
    public static final String STEP_STATUS_EditRejected = "Edit Rejected";
    public static final String STEP_STATUS_DeletionApproval = "Deletion Authorization";
    public static final String STEP_STATUS_End = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Cancel = "Cancel";
    public static final String ACTION_NAME_Reset = "Reset";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveCreationApproval = "3";
    public static final String ACTION_CODE_RejectCreationApproval = "4";
    public static final String ACTION_CODE_EditRepairNew = "5";
    public static final String ACTION_CODE_DeleteRepairNew = "6";
    public static final String ACTION_CODE_SaveEditNew = "7";
    public static final String ACTION_CODE_CancelEditNew = "8";
    public static final String ACTION_CODE_EditViewApproved = "9";
    public static final String ACTION_CODE_DeleteViewApproved = "10";
    public static final String ACTION_CODE_SaveEditApproved = "11";
    public static final String ACTION_CODE_CancelEditApproved = "12";
    public static final String ACTION_CODE_ApproveModificationApproval = "13";
    public static final String ACTION_CODE_RejectModificationApproval = "14";
    public static final String ACTION_CODE_EditRepairRejected = "15";
    public static final String ACTION_CODE_ResetRepairRejected = "16";
    public static final String ACTION_CODE_SaveEditRejected = "17";
    public static final String ACTION_CODE_CancelEditRejected = "18";
    public static final String ACTION_CODE_ApproveDeletionApproval = "19";
    public static final String ACTION_CODE_RejectDeletionApproval = "20";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveCreationApproval = "1029902177";
    public static final String ACTION_KEY_RejectCreationApproval = "1782986335";
    public static final String ACTION_KEY_EditRepairNew = "1274475243";
    public static final String ACTION_KEY_DeleteRepairNew = "812026605";
    public static final String ACTION_KEY_SaveEditNew = "1682602479";
    public static final String ACTION_KEY_CancelEditNew = "1018444577";
    public static final String ACTION_KEY_EditViewApproved = "594783481";
    public static final String ACTION_KEY_DeleteViewApproved = "695251121";
    public static final String ACTION_KEY_SaveEditApproved = "1819056854";
    public static final String ACTION_KEY_CancelEditApproved = "318312162";
    public static final String ACTION_KEY_ApproveModificationApproval = "895307547";
    public static final String ACTION_KEY_RejectModificationApproval = "968665261";
    public static final String ACTION_KEY_EditRepairRejected = "1679153855";
    public static final String ACTION_KEY_ResetRepairRejected = "1828960459";
    public static final String ACTION_KEY_SaveEditRejected = "576644534";
    public static final String ACTION_KEY_CancelEditRejected = "173541223";
    public static final String ACTION_KEY_ApproveDeletionApproval = "126649091";
    public static final String ACTION_KEY_RejectDeletionApproval = "904288293";

}