package com.progressoft.mpay.entities;

import javax.persistence.*;
import java.util.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import com.progressoft.jfw.model.bussinessobject.changeHistory.ChangeItem;
import com.progressoft.jfw.model.bussinessobject.changeHistory.ChangeGroup;

@Entity
@Table(name="MPAY_Reg_Instrs_SttGRP")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Reg_Instrs_SttGrp extends ChangeGroup implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Reg_Instrs_SttGrp(){/*Default Constructor*/}

@OneToMany(mappedBy="changeGroup")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private Set<MPAY_Reg_Instrs_SttItm> changeItems;

public void setChangeItems(Set<MPAY_Reg_Instrs_SttItm> changeItems) {
this.changeItems = changeItems;
}

@Override
public Set<? extends ChangeItem> getChangeItems() {
return this.changeItems;
}


}