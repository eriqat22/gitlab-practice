package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_NetworkManIntegMsgs")
@XmlRootElement(name="NetworkManIntegMsgs")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_NetworkManIntegMsg extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_NetworkManIntegMsg(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REF_MESSAGE = "refMessage";
@Column(name="REFMESSAGE", nullable=false, length=40)
private String refMessage;
public String getRefMessage(){
return this.refMessage;
}
public void setRefMessage(String refMessage){
this.refMessage = refMessage;
}

public static final String NARRATION = "narration";
@Column(name="NARRATION", nullable=true, length=4000)
private String narration;
public String getNarration(){
return this.narration;
}
public void setNarration(String narration){
this.narration = narration;
}

public static final String REF_MSG_LOG = "refMsgLog";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMSGLOGID", nullable=false)
private MPAY_MpClearIntegMsgLog refMsgLog;
public MPAY_MpClearIntegMsgLog getRefMsgLog(){
return this.refMsgLog;
}
public void setRefMsgLog(MPAY_MpClearIntegMsgLog refMsgLog){
this.refMsgLog = refMsgLog;
}

public static final String INTEG_MSG_TYPE = "integMsgType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="INTEGMSGTYPEID", nullable=false)
private MPAY_MpClearIntegMsgType integMsgType;
public MPAY_MpClearIntegMsgType getIntegMsgType(){
return this.integMsgType;
}
public void setIntegMsgType(MPAY_MpClearIntegMsgType integMsgType){
this.integMsgType = integMsgType;
}

public static final String REF_ORIGINAL_MSG = "refOriginalMsg";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFORIGINALMSGID", nullable=true)
private MPAY_NetworkManIntegMsg refOriginalMsg;
public MPAY_NetworkManIntegMsg getRefOriginalMsg(){
return this.refOriginalMsg;
}
public void setRefOriginalMsg(MPAY_NetworkManIntegMsg refOriginalMsg){
this.refOriginalMsg = refOriginalMsg;
}

public static final String ACTION_TYPE = "actionType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="ACTIONTYPEID", nullable=true)
private MPAY_NetworkManActionType actionType;
public MPAY_NetworkManActionType getActionType(){
return this.actionType;
}
public void setActionType(MPAY_NetworkManActionType actionType){
this.actionType = actionType;
}

public static final String RESPONSE = "response";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="RESPONSEID", nullable=true)
private MPAY_MpClearResponseCode response;
public MPAY_MpClearResponseCode getResponse(){
return this.response;
}
public void setResponse(MPAY_MpClearResponseCode response){
this.response = response;
}

public static final String PROCESS_REJECTION = "processRejection";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PROCESSREJECTIONID", nullable=true)
private MPAY_MpClearIntegRjctReason processRejection;
public MPAY_MpClearIntegRjctReason getProcessRejection(){
return this.processRejection;
}
public void setProcessRejection(MPAY_MpClearIntegRjctReason processRejection){
this.processRejection = processRejection;
}

public static final String REF_ORIGINAL_MSG_NETWORK_MAN_INTEG_MSGS = "refOriginalMsgNetworkManIntegMsgs";
@OneToMany(mappedBy = "refOriginalMsg")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_NetworkManIntegMsg> refOriginalMsgNetworkManIntegMsgs;
public List<MPAY_NetworkManIntegMsg> getRefOriginalMsgNetworkManIntegMsgs(){
return this.refOriginalMsgNetworkManIntegMsgs;
}
public void setRefOriginalMsgNetworkManIntegMsgs(List<MPAY_NetworkManIntegMsg> refOriginalMsgNetworkManIntegMsgs){
this.refOriginalMsgNetworkManIntegMsgs = refOriginalMsgNetworkManIntegMsgs;
}

@Override
public String toString() {
return "MPAY_NetworkManIntegMsg [id= " + getId() + ", refMessage= " + getRefMessage() + ", narration= " + getNarration() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRefMessage() == null) ? 0 : getRefMessage().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_NetworkManIntegMsg))
return false;
else {MPAY_NetworkManIntegMsg other = (MPAY_NetworkManIntegMsg) obj;
return this.hashCode() == other.hashCode();}
}


}