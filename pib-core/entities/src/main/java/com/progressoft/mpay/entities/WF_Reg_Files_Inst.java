package com.progressoft.mpay.entities;

public class WF_Reg_Files_Inst
 {
    // Steps
    public static final String STEP_Initialization = "106801";
    public static final String STEP_NewStep = "106802";
    public static final String STEP_WaitingStep = "106803";
    public static final String STEP_Processed = "106804";
    // Statuses
    public static final String STEP_STATUS_Initialization = "Initialziation";
    public static final String STEP_STATUS_NewStep = "New";
    public static final String STEP_STATUS_WaitingStep = "Waiting Response";
    public static final String STEP_STATUS_Processed = "Processed";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_SVC_Advance = "SVC_Advance";
    public static final String ACTION_NAME_SVC_Failed = "SVC_Failed";
    public static final String ACTION_NAME_SVC_Processed = "SVC_Processed";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SVC_AdvanceNewStep = "3";
    public static final String ACTION_CODE_SVC_FailedNewStep = "6";
    public static final String ACTION_CODE_SVC_ProcessedWaitingStep = "4";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SVC_AdvanceNewStep = "1027676497";
    public static final String ACTION_KEY_SVC_FailedNewStep = "960257977";
    public static final String ACTION_KEY_SVC_ProcessedWaitingStep = "200369729";

}