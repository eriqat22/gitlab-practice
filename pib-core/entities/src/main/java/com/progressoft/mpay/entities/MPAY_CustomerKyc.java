package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.List;

@Entity

@Table(name="MPAY_CustomerKyc",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="CustomerKyc")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CustomerKyc extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CustomerKyc(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String ENGLISH_FULL_NAME_REQ = "englishFullNameReq";
@Column(name="ENGLISHFULLNAMEREQ", nullable=true, length=20)
private Boolean englishFullNameReq;
public Boolean getEnglishFullNameReq(){
return this.englishFullNameReq;
}
public void setEnglishFullNameReq(Boolean englishFullNameReq){
this.englishFullNameReq = englishFullNameReq;
}

public static final String ENGLISH_FULL_NAME_VISIBLE = "englishFullNameVisible";
@Column(name="ENGLISHFULLNAMEVISIBLE", nullable=true, length=20)
private Boolean englishFullNameVisible;
public Boolean getEnglishFullNameVisible(){
return this.englishFullNameVisible;
}
public void setEnglishFullNameVisible(Boolean englishFullNameVisible){
this.englishFullNameVisible = englishFullNameVisible;
}

public static final String ENGLISH_FULL_NAME_ENABLED = "englishFullNameEnabled";
@Column(name="ENGLISHFULLNAMEENABLED", nullable=true, length=20)
private Boolean englishFullNameEnabled;
public Boolean getEnglishFullNameEnabled(){
return this.englishFullNameEnabled;
}
public void setEnglishFullNameEnabled(Boolean englishFullNameEnabled){
this.englishFullNameEnabled = englishFullNameEnabled;
}

public static final String ENGLISH_FULL_NAME_DEFAULT = "englishFullNameDefault";
@Column(name="ENGLISHFULLNAMEDEFAULT", nullable=true, length=255)
private String englishFullNameDefault;
public String getEnglishFullNameDefault(){
return this.englishFullNameDefault;
}
public void setEnglishFullNameDefault(String englishFullNameDefault){
this.englishFullNameDefault = englishFullNameDefault;
}

public static final String ARABIC_FULL_NAME_REQUIRED = "arabicFullNameRequired";
@Column(name="ARABICFULLNAMEREQUIRED", nullable=true, length=20)
private Boolean arabicFullNameRequired;
public Boolean getArabicFullNameRequired(){
return this.arabicFullNameRequired;
}
public void setArabicFullNameRequired(Boolean arabicFullNameRequired){
this.arabicFullNameRequired = arabicFullNameRequired;
}

public static final String ARABIC_FULL_NAME_VISIBLE = "arabicFullNameVisible";
@Column(name="ARABICFULLNAMEVISIBLE", nullable=true, length=20)
private Boolean arabicFullNameVisible;
public Boolean getArabicFullNameVisible(){
return this.arabicFullNameVisible;
}
public void setArabicFullNameVisible(Boolean arabicFullNameVisible){
this.arabicFullNameVisible = arabicFullNameVisible;
}

public static final String ARABIC_FULL_NAME_ENABLED = "arabicFullNameEnabled";
@Column(name="ARABICFULLNAMEENABLED", nullable=true, length=20)
private Boolean arabicFullNameEnabled;
public Boolean getArabicFullNameEnabled(){
return this.arabicFullNameEnabled;
}
public void setArabicFullNameEnabled(Boolean arabicFullNameEnabled){
this.arabicFullNameEnabled = arabicFullNameEnabled;
}

public static final String ARABIC_FULL_NAME_DEFAULT = "arabicFullNameDefault";
@Column(name="ARABICFULLNAMEDEFAULT", nullable=true, length=255)
private String arabicFullNameDefault;
public String getArabicFullNameDefault(){
return this.arabicFullNameDefault;
}
public void setArabicFullNameDefault(String arabicFullNameDefault){
this.arabicFullNameDefault = arabicFullNameDefault;
}

public static final String IDENTIFICATION_REFERENCE_REQ = "identificationReferenceReq";
@Column(name="IDENTIFICATIONREFERENCEREQ", nullable=true, length=20)
private Boolean identificationReferenceReq;
public Boolean getIdentificationReferenceReq(){
return this.identificationReferenceReq;
}
public void setIdentificationReferenceReq(Boolean identificationReferenceReq){
this.identificationReferenceReq = identificationReferenceReq;
}

public static final String IDENTIFICATION_REFERENCE_VISIBLE = "identificationReferenceVisible";
@Column(name="IDENTIFICATIONREFERENCEVISIBLE", nullable=true, length=20)
private Boolean identificationReferenceVisible;
public Boolean getIdentificationReferenceVisible(){
return this.identificationReferenceVisible;
}
public void setIdentificationReferenceVisible(Boolean identificationReferenceVisible){
this.identificationReferenceVisible = identificationReferenceVisible;
}

public static final String IDENTIFICATION_REFERENCE_ENABLED = "identificationReferenceEnabled";
@Column(name="IDENTIFICATIONREFERENCEENABLED", nullable=true, length=20)
private Boolean identificationReferenceEnabled;
public Boolean getIdentificationReferenceEnabled(){
return this.identificationReferenceEnabled;
}
public void setIdentificationReferenceEnabled(Boolean identificationReferenceEnabled){
this.identificationReferenceEnabled = identificationReferenceEnabled;
}

public static final String IDENTIFICATION_REFERENCE_DEFAULT = "identificationReferenceDefault";
@Column(name="IDENTIFICATIONREFERENCEDEFAULT", nullable=true, length=255)
private String identificationReferenceDefault;
public String getIdentificationReferenceDefault(){
return this.identificationReferenceDefault;
}
public void setIdentificationReferenceDefault(String identificationReferenceDefault){
this.identificationReferenceDefault = identificationReferenceDefault;
}

public static final String IDENTIFICATION_CARD_REQ = "identificationCardReq";
@Column(name="IDENTIFICATIONCARDREQ", nullable=true, length=20)
private Boolean identificationCardReq;
public Boolean getIdentificationCardReq(){
return this.identificationCardReq;
}
public void setIdentificationCardReq(Boolean identificationCardReq){
this.identificationCardReq = identificationCardReq;
}

public static final String IDENTIFICATION_CARD_VISIBLE = "identificationCardVisible";
@Column(name="IDENTIFICATIONCARDVISIBLE", nullable=true, length=20)
private Boolean identificationCardVisible;
public Boolean getIdentificationCardVisible(){
return this.identificationCardVisible;
}
public void setIdentificationCardVisible(Boolean identificationCardVisible){
this.identificationCardVisible = identificationCardVisible;
}

public static final String IDENTIFICATION_CARD_ENABLED = "identificationCardEnabled";
@Column(name="IDENTIFICATIONCARDENABLED", nullable=true, length=20)
private Boolean identificationCardEnabled;
public Boolean getIdentificationCardEnabled(){
return this.identificationCardEnabled;
}
public void setIdentificationCardEnabled(Boolean identificationCardEnabled){
this.identificationCardEnabled = identificationCardEnabled;
}

public static final String IDENTIFICATION_CARD_DEFAULT = "identificationCardDefault";
@Column(name="IDENTIFICATIONCARDDEFAULT", nullable=true, length=255)
private String identificationCardDefault;
public String getIdentificationCardDefault(){
return this.identificationCardDefault;
}
public void setIdentificationCardDefault(String identificationCardDefault){
this.identificationCardDefault = identificationCardDefault;
}

public static final String ID_CARD_ISSUANCE_DATE_REQ = "idCardIssuanceDateReq";
@Column(name="IDCARDISSUANCEDATEREQ", nullable=true, length=20)
private Boolean idCardIssuanceDateReq;
public Boolean getIdCardIssuanceDateReq(){
return this.idCardIssuanceDateReq;
}
public void setIdCardIssuanceDateReq(Boolean idCardIssuanceDateReq){
this.idCardIssuanceDateReq = idCardIssuanceDateReq;
}

public static final String ID_CARD_ISSUANCE_DATE_VISIBLE = "idCardIssuanceDateVisible";
@Column(name="IDCARDISSUANCEDATEVISIBLE", nullable=true, length=20)
private Boolean idCardIssuanceDateVisible;
public Boolean getIdCardIssuanceDateVisible(){
return this.idCardIssuanceDateVisible;
}
public void setIdCardIssuanceDateVisible(Boolean idCardIssuanceDateVisible){
this.idCardIssuanceDateVisible = idCardIssuanceDateVisible;
}

public static final String ID_CARD_ISSUANCE_DATE_ENABLED = "idCardIssuanceDateEnabled";
@Column(name="IDCARDISSUANCEDATEENABLED", nullable=true, length=20)
private Boolean idCardIssuanceDateEnabled;
public Boolean getIdCardIssuanceDateEnabled(){
return this.idCardIssuanceDateEnabled;
}
public void setIdCardIssuanceDateEnabled(Boolean idCardIssuanceDateEnabled){
this.idCardIssuanceDateEnabled = idCardIssuanceDateEnabled;
}

public static final String ID_CARD_ISSUANCE_DATE_DEFAULT = "idCardIssuanceDateDefault";
@Column(name="IDCARDISSUANCEDATEDEFAULT", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date idCardIssuanceDateDefault;
public java.util.Date getIdCardIssuanceDateDefault(){
return this.idCardIssuanceDateDefault;
}
public void setIdCardIssuanceDateDefault(java.util.Date idCardIssuanceDateDefault){
this.idCardIssuanceDateDefault = idCardIssuanceDateDefault;
}

public static final String PASSPORT_ID_REQ = "passportIdReq";
@Column(name="PASSPORTIDREQ", nullable=true, length=20)
private Boolean passportIdReq;
public Boolean getPassportIdReq(){
return this.passportIdReq;
}
public void setPassportIdReq(Boolean passportIdReq){
this.passportIdReq = passportIdReq;
}

public static final String PASSPORT_ID_VISIBLE = "passportIdVisible";
@Column(name="PASSPORTIDVISIBLE", nullable=true, length=20)
private Boolean passportIdVisible;
public Boolean getPassportIdVisible(){
return this.passportIdVisible;
}
public void setPassportIdVisible(Boolean passportIdVisible){
this.passportIdVisible = passportIdVisible;
}

public static final String PASSPORT_ID_ENABLED = "passportIdEnabled";
@Column(name="PASSPORTIDENABLED", nullable=true, length=20)
private Boolean passportIdEnabled;
public Boolean getPassportIdEnabled(){
return this.passportIdEnabled;
}
public void setPassportIdEnabled(Boolean passportIdEnabled){
this.passportIdEnabled = passportIdEnabled;
}

public static final String PASSPORT_ID_DEFAULT = "passportIdDefault";
@Column(name="PASSPORTIDDEFAULT", nullable=true, length=255)
private String passportIdDefault;
public String getPassportIdDefault(){
return this.passportIdDefault;
}
public void setPassportIdDefault(String passportIdDefault){
this.passportIdDefault = passportIdDefault;
}

public static final String PASSPORT_ISSUANCE_DATE_REQ = "passportIssuanceDateReq";
@Column(name="PASSPORTISSUANCEDATEREQ", nullable=true, length=20)
private Boolean passportIssuanceDateReq;
public Boolean getPassportIssuanceDateReq(){
return this.passportIssuanceDateReq;
}
public void setPassportIssuanceDateReq(Boolean passportIssuanceDateReq){
this.passportIssuanceDateReq = passportIssuanceDateReq;
}

public static final String PASSPORT_ISSUANCE_DATE_VISIBLE = "passportIssuanceDateVisible";
@Column(name="PASSPORTISSUANCEDATEVISIBLE", nullable=true, length=20)
private Boolean passportIssuanceDateVisible;
public Boolean getPassportIssuanceDateVisible(){
return this.passportIssuanceDateVisible;
}
public void setPassportIssuanceDateVisible(Boolean passportIssuanceDateVisible){
this.passportIssuanceDateVisible = passportIssuanceDateVisible;
}

public static final String PASSPORT_ISSUANCE_DATE_ENABLED = "passportIssuanceDateEnabled";
@Column(name="PASSPORTISSUANCEDATEENABLED", nullable=true, length=20)
private Boolean passportIssuanceDateEnabled;
public Boolean getPassportIssuanceDateEnabled(){
return this.passportIssuanceDateEnabled;
}
public void setPassportIssuanceDateEnabled(Boolean passportIssuanceDateEnabled){
this.passportIssuanceDateEnabled = passportIssuanceDateEnabled;
}

public static final String PASSPORT_ISSUANCE_DATE_DEFAULT = "passportIssuanceDateDefault";
@Column(name="PASSPORTISSUANCEDATEDEFAULT", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date passportIssuanceDateDefault;
public java.util.Date getPassportIssuanceDateDefault(){
return this.passportIssuanceDateDefault;
}
public void setPassportIssuanceDateDefault(java.util.Date passportIssuanceDateDefault){
this.passportIssuanceDateDefault = passportIssuanceDateDefault;
}

public static final String PHONE_ONE_REQ = "phoneOneReq";
@Column(name="PHONEONEREQ", nullable=true, length=20)
private Boolean phoneOneReq;
public Boolean getPhoneOneReq(){
return this.phoneOneReq;
}
public void setPhoneOneReq(Boolean phoneOneReq){
this.phoneOneReq = phoneOneReq;
}

public static final String PHONE_ONE_VISIBLE = "phoneOneVisible";
@Column(name="PHONEONEVISIBLE", nullable=true, length=20)
private Boolean phoneOneVisible;
public Boolean getPhoneOneVisible(){
return this.phoneOneVisible;
}
public void setPhoneOneVisible(Boolean phoneOneVisible){
this.phoneOneVisible = phoneOneVisible;
}

public static final String PHONE_ONE_ENABLED = "phoneOneEnabled";
@Column(name="PHONEONEENABLED", nullable=true, length=20)
private Boolean phoneOneEnabled;
public Boolean getPhoneOneEnabled(){
return this.phoneOneEnabled;
}
public void setPhoneOneEnabled(Boolean phoneOneEnabled){
this.phoneOneEnabled = phoneOneEnabled;
}

public static final String PHONE_ONE_DEFAULT = "phoneOneDefault";
@Column(name="PHONEONEDEFAULT", nullable=true, length=255)
private String phoneOneDefault;
public String getPhoneOneDefault(){
return this.phoneOneDefault;
}
public void setPhoneOneDefault(String phoneOneDefault){
this.phoneOneDefault = phoneOneDefault;
}

public static final String PHONE_TWO_REQ = "phoneTwoReq";
@Column(name="PHONETWOREQ", nullable=true, length=20)
private Boolean phoneTwoReq;
public Boolean getPhoneTwoReq(){
return this.phoneTwoReq;
}
public void setPhoneTwoReq(Boolean phoneTwoReq){
this.phoneTwoReq = phoneTwoReq;
}

public static final String PHONE_TWO_VISIBLE = "phoneTwoVisible";
@Column(name="PHONETWOVISIBLE", nullable=true, length=20)
private Boolean phoneTwoVisible;
public Boolean getPhoneTwoVisible(){
return this.phoneTwoVisible;
}
public void setPhoneTwoVisible(Boolean phoneTwoVisible){
this.phoneTwoVisible = phoneTwoVisible;
}

public static final String PHONE_TWO_ENABLED = "phoneTwoEnabled";
@Column(name="PHONETWOENABLED", nullable=true, length=20)
private Boolean phoneTwoEnabled;
public Boolean getPhoneTwoEnabled(){
return this.phoneTwoEnabled;
}
public void setPhoneTwoEnabled(Boolean phoneTwoEnabled){
this.phoneTwoEnabled = phoneTwoEnabled;
}

public static final String PHONE_TWO_DEFAULT = "phoneTwoDefault";
@Column(name="PHONETWODEFAULT", nullable=true, length=255)
private String phoneTwoDefault;
public String getPhoneTwoDefault(){
return this.phoneTwoDefault;
}
public void setPhoneTwoDefault(String phoneTwoDefault){
this.phoneTwoDefault = phoneTwoDefault;
}

public static final String AREA_REQ = "areaReq";
@Column(name="AREAREQ", nullable=true, length=20)
private Boolean areaReq;
public Boolean getAreaReq(){
return this.areaReq;
}
public void setAreaReq(Boolean areaReq){
this.areaReq = areaReq;
}

public static final String AREA_VISIBLE = "areaVisible";
@Column(name="AREAVISIBLE", nullable=true, length=20)
private Boolean areaVisible;
public Boolean getAreaVisible(){
return this.areaVisible;
}
public void setAreaVisible(Boolean areaVisible){
this.areaVisible = areaVisible;
}

public static final String AREA_ENABLED = "areaEnabled";
@Column(name="AREAENABLED", nullable=true, length=20)
private Boolean areaEnabled;
public Boolean getAreaEnabled(){
return this.areaEnabled;
}
public void setAreaEnabled(Boolean areaEnabled){
this.areaEnabled = areaEnabled;
}

public static final String POBOX_REQ = "poboxReq";
@Column(name="POBOXREQ", nullable=true, length=20)
private Boolean poboxReq;
public Boolean getPoboxReq(){
return this.poboxReq;
}
public void setPoboxReq(Boolean poboxReq){
this.poboxReq = poboxReq;
}

public static final String POBOX_VISIBLE = "poboxVisible";
@Column(name="POBOXVISIBLE", nullable=true, length=20)
private Boolean poboxVisible;
public Boolean getPoboxVisible(){
return this.poboxVisible;
}
public void setPoboxVisible(Boolean poboxVisible){
this.poboxVisible = poboxVisible;
}

public static final String POBOX_ENABLED = "poboxEnabled";
@Column(name="POBOXENABLED", nullable=true, length=20)
private Boolean poboxEnabled;
public Boolean getPoboxEnabled(){
return this.poboxEnabled;
}
public void setPoboxEnabled(Boolean poboxEnabled){
this.poboxEnabled = poboxEnabled;
}

public static final String POBOX_DEFAULT = "poboxDefault";
@Column(name="POBOXDEFAULT", nullable=true, length=255)
private String poboxDefault;
public String getPoboxDefault(){
return this.poboxDefault;
}
public void setPoboxDefault(String poboxDefault){
this.poboxDefault = poboxDefault;
}

public static final String ZIP_CODE_REQ = "zipCodeReq";
@Column(name="ZIPCODEREQ", nullable=true, length=20)
private Boolean zipCodeReq;
public Boolean getZipCodeReq(){
return this.zipCodeReq;
}
public void setZipCodeReq(Boolean zipCodeReq){
this.zipCodeReq = zipCodeReq;
}

public static final String ZIP_CODE_VISIBLE = "zipCodeVisible";
@Column(name="ZIPCODEVISIBLE", nullable=true, length=20)
private Boolean zipCodeVisible;
public Boolean getZipCodeVisible(){
return this.zipCodeVisible;
}
public void setZipCodeVisible(Boolean zipCodeVisible){
this.zipCodeVisible = zipCodeVisible;
}

public static final String ZIP_CODE_ENABLED = "zipCodeEnabled";
@Column(name="ZIPCODEENABLED", nullable=true, length=20)
private Boolean zipCodeEnabled;
public Boolean getZipCodeEnabled(){
return this.zipCodeEnabled;
}
public void setZipCodeEnabled(Boolean zipCodeEnabled){
this.zipCodeEnabled = zipCodeEnabled;
}

public static final String ZIP_CODE_DEFAULT = "zipCodeDefault";
@Column(name="ZIPCODEDEFAULT", nullable=true, length=255)
private String zipCodeDefault;
public String getZipCodeDefault(){
return this.zipCodeDefault;
}
public void setZipCodeDefault(String zipCodeDefault){
this.zipCodeDefault = zipCodeDefault;
}

public static final String BUILDING_NUM_REQ = "buildingNumReq";
@Column(name="BUILDINGNUMREQ", nullable=true, length=20)
private Boolean buildingNumReq;
public Boolean getBuildingNumReq(){
return this.buildingNumReq;
}
public void setBuildingNumReq(Boolean buildingNumReq){
this.buildingNumReq = buildingNumReq;
}

public static final String BUILDING_NUM_VISIBLE = "buildingNumVisible";
@Column(name="BUILDINGNUMVISIBLE", nullable=true, length=20)
private Boolean buildingNumVisible;
public Boolean getBuildingNumVisible(){
return this.buildingNumVisible;
}
public void setBuildingNumVisible(Boolean buildingNumVisible){
this.buildingNumVisible = buildingNumVisible;
}

public static final String BUILDING_NUM_ENABLED = "buildingNumEnabled";
@Column(name="BUILDINGNUMENABLED", nullable=true, length=20)
private Boolean buildingNumEnabled;
public Boolean getBuildingNumEnabled(){
return this.buildingNumEnabled;
}
public void setBuildingNumEnabled(Boolean buildingNumEnabled){
this.buildingNumEnabled = buildingNumEnabled;
}

public static final String BUILDING_NUM_DEFAULT = "buildingNumDefault";
@Column(name="BUILDINGNUMDEFAULT", nullable=true, length=20)
private String buildingNumDefault;
public String getBuildingNumDefault(){
return this.buildingNumDefault;
}
public void setBuildingNumDefault(String buildingNumDefault){
this.buildingNumDefault = buildingNumDefault;
}

public static final String STREET_NAME_REQ = "streetNameReq";
@Column(name="STREETNAMEREQ", nullable=true, length=20)
private Boolean streetNameReq;
public Boolean getStreetNameReq(){
return this.streetNameReq;
}
public void setStreetNameReq(Boolean streetNameReq){
this.streetNameReq = streetNameReq;
}

public static final String STREET_NAME_VISIBLE = "streetNameVisible";
@Column(name="STREETNAMEVISIBLE", nullable=true, length=20)
private Boolean streetNameVisible;
public Boolean getStreetNameVisible(){
return this.streetNameVisible;
}
public void setStreetNameVisible(Boolean streetNameVisible){
this.streetNameVisible = streetNameVisible;
}

public static final String STREET_NAME_ENABLED = "streetNameEnabled";
@Column(name="STREETNAMEENABLED", nullable=true, length=20)
private Boolean streetNameEnabled;
public Boolean getStreetNameEnabled(){
return this.streetNameEnabled;
}
public void setStreetNameEnabled(Boolean streetNameEnabled){
this.streetNameEnabled = streetNameEnabled;
}

public static final String STREET_NAME_DEFAULT = "streetNameDefault";
@Column(name="STREETNAMEDEFAULT", nullable=true, length=255)
private String streetNameDefault;
public String getStreetNameDefault(){
return this.streetNameDefault;
}
public void setStreetNameDefault(String streetNameDefault){
this.streetNameDefault = streetNameDefault;
}

public static final String ADDRESS_REQ = "addressReq";
@Column(name="ADDRESSREQ", nullable=true, length=20)
private Boolean addressReq;
public Boolean getAddressReq(){
return this.addressReq;
}
public void setAddressReq(Boolean addressReq){
this.addressReq = addressReq;
}

public static final String ADDRESS_VISIBLE = "addressVisible";
@Column(name="ADDRESSVISIBLE", nullable=true, length=20)
private Boolean addressVisible;
public Boolean getAddressVisible(){
return this.addressVisible;
}
public void setAddressVisible(Boolean addressVisible){
this.addressVisible = addressVisible;
}

public static final String ADDRESS_ENABLED = "addressEnabled";
@Column(name="ADDRESSENABLED", nullable=true, length=20)
private Boolean addressEnabled;
public Boolean getAddressEnabled(){
return this.addressEnabled;
}
public void setAddressEnabled(Boolean addressEnabled){
this.addressEnabled = addressEnabled;
}

public static final String ADDRESS_DEFAULT = "addressDefault";
@Column(name="ADDRESSDEFAULT", nullable=true, length=1000)
private String addressDefault;
public String getAddressDefault(){
return this.addressDefault;
}
public void setAddressDefault(String addressDefault){
this.addressDefault = addressDefault;
}

public static final String NOTE_REQ = "noteReq";
@Column(name="NOTEREQ", nullable=true, length=20)
private Boolean noteReq;
public Boolean getNoteReq(){
return this.noteReq;
}
public void setNoteReq(Boolean noteReq){
this.noteReq = noteReq;
}

public static final String NOTE_VISIBLE = "noteVisible";
@Column(name="NOTEVISIBLE", nullable=true, length=20)
private Boolean noteVisible;
public Boolean getNoteVisible(){
return this.noteVisible;
}
public void setNoteVisible(Boolean noteVisible){
this.noteVisible = noteVisible;
}

public static final String NOTE_ENABLED = "noteEnabled";
@Column(name="NOTEENABLED", nullable=true, length=20)
private Boolean noteEnabled;
public Boolean getNoteEnabled(){
return this.noteEnabled;
}
public void setNoteEnabled(Boolean noteEnabled){
this.noteEnabled = noteEnabled;
}

public static final String NOTE_DEFAULT = "noteDefault";
@Column(name="NOTEDEFAULT", nullable=true, length=255)
private String noteDefault;
public String getNoteDefault(){
return this.noteDefault;
}
public void setNoteDefault(String noteDefault){
this.noteDefault = noteDefault;
}

public static final String OCCUPATION_REQ = "occupationReq";
@Column(name="OCCUPATIONREQ", nullable=true, length=20)
private Boolean occupationReq;
public Boolean getOccupationReq(){
return this.occupationReq;
}
public void setOccupationReq(Boolean occupationReq){
this.occupationReq = occupationReq;
}

public static final String OCCUPATION_VISIBLE = "occupationVisible";
@Column(name="OCCUPATIONVISIBLE", nullable=true, length=20)
private Boolean occupationVisible;
public Boolean getOccupationVisible(){
return this.occupationVisible;
}
public void setOccupationVisible(Boolean occupationVisible){
this.occupationVisible = occupationVisible;
}

public static final String OCCUPATION_ENABLED = "occupationEnabled";
@Column(name="OCCUPATIONENABLED", nullable=true, length=20)
private Boolean occupationEnabled;
public Boolean getOccupationEnabled(){
return this.occupationEnabled;
}
public void setOccupationEnabled(Boolean occupationEnabled){
this.occupationEnabled = occupationEnabled;
}

public static final String OCCUPATION_DEFAULT = "occupationDefault";
@Column(name="OCCUPATIONDEFAULT", nullable=true, length=50)
private String occupationDefault;
public String getOccupationDefault(){
return this.occupationDefault;
}
public void setOccupationDefault(String occupationDefault){
this.occupationDefault = occupationDefault;
}

public static final String ID_EXPIRY_DATE_REQ = "idExpiryDateReq";
@Column(name="IDEXPIRYDATEREQ", nullable=true, length=20)
private Boolean idExpiryDateReq;
public Boolean getIdExpiryDateReq(){
return this.idExpiryDateReq;
}
public void setIdExpiryDateReq(Boolean idExpiryDateReq){
this.idExpiryDateReq = idExpiryDateReq;
}

public static final String ID_EXPIRY_DATE_VISIBLE = "idExpiryDateVisible";
@Column(name="IDEXPIRYDATEVISIBLE", nullable=true, length=20)
private Boolean idExpiryDateVisible;
public Boolean getIdExpiryDateVisible(){
return this.idExpiryDateVisible;
}
public void setIdExpiryDateVisible(Boolean idExpiryDateVisible){
this.idExpiryDateVisible = idExpiryDateVisible;
}

public static final String ID_EXPIRY_DATE_ENABLED = "idExpiryDateEnabled";
@Column(name="IDEXPIRYDATEENABLED", nullable=true, length=20)
private Boolean idExpiryDateEnabled;
public Boolean getIdExpiryDateEnabled(){
return this.idExpiryDateEnabled;
}
public void setIdExpiryDateEnabled(Boolean idExpiryDateEnabled){
this.idExpiryDateEnabled = idExpiryDateEnabled;
}

public static final String ID_EXPIRY_DATE_DEFAULT = "idExpiryDateDefault";
@Column(name="IDEXPIRYDATEDEFAULT", nullable=true, length=100)
@Temporal(TemporalType.DATE)
private java.util.Date idExpiryDateDefault;
public java.util.Date getIdExpiryDateDefault(){
return this.idExpiryDateDefault;
}
public void setIdExpiryDateDefault(java.util.Date idExpiryDateDefault){
this.idExpiryDateDefault = idExpiryDateDefault;
}

public static final String AREA_DEFAULT = "areaDefault";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="AREADEFAULTID", nullable=true)
private MPAY_CityArea areaDefault;
public MPAY_CityArea getAreaDefault(){
return this.areaDefault;
}
public void setAreaDefault(MPAY_CityArea areaDefault){
this.areaDefault = areaDefault;
}

public static final String PROFILES = "profiles";
@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.MERGE })
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinTable(name="MPAY_CustomerKycProfiles", joinColumns = @JoinColumn(name = "CUSTOMERKYC_ID"), inverseJoinColumns = @JoinColumn(name = "PROFILES_ID"))
private List<MPAY_Profile> profiles = new ArrayList<MPAY_Profile>();
public List<MPAY_Profile> getProfiles(){
return this.profiles;
}
public void setProfiles(List<MPAY_Profile> profiles){
this.profiles = profiles;
}

public static final String REF_KYC_CUST_KYC_ATT_TYPES = "refKycCustKycAttTypes";
@OneToMany(mappedBy = "refKyc")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CustKycAttType> refKycCustKycAttTypes;
public List<MPAY_CustKycAttType> getRefKycCustKycAttTypes(){
return this.refKycCustKycAttTypes;
}
public void setRefKycCustKycAttTypes(List<MPAY_CustKycAttType> refKycCustKycAttTypes){
this.refKycCustKycAttTypes = refKycCustKycAttTypes;
}

@Override
public String toString() {
return "MPAY_CustomerKyc [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", englishFullNameReq= " + getEnglishFullNameReq() + ", englishFullNameVisible= " + getEnglishFullNameVisible() + ", englishFullNameEnabled= " + getEnglishFullNameEnabled() + ", englishFullNameDefault= " + getEnglishFullNameDefault() + ", arabicFullNameRequired= " + getArabicFullNameRequired() + ", arabicFullNameVisible= " + getArabicFullNameVisible() + ", arabicFullNameEnabled= " + getArabicFullNameEnabled() + ", arabicFullNameDefault= " + getArabicFullNameDefault() + ", identificationReferenceReq= " + getIdentificationReferenceReq() + ", identificationReferenceVisible= " + getIdentificationReferenceVisible() + ", identificationReferenceEnabled= " + getIdentificationReferenceEnabled() + ", identificationReferenceDefault= " + getIdentificationReferenceDefault() + ", identificationCardReq= " + getIdentificationCardReq() + ", identificationCardVisible= " + getIdentificationCardVisible() + ", identificationCardEnabled= " + getIdentificationCardEnabled() + ", identificationCardDefault= " + getIdentificationCardDefault() + ", idCardIssuanceDateReq= " + getIdCardIssuanceDateReq() + ", idCardIssuanceDateVisible= " + getIdCardIssuanceDateVisible() + ", idCardIssuanceDateEnabled= " + getIdCardIssuanceDateEnabled() + ", idCardIssuanceDateDefault= " + getIdCardIssuanceDateDefault() + ", passportIdReq= " + getPassportIdReq() + ", passportIdVisible= " + getPassportIdVisible() + ", passportIdEnabled= " + getPassportIdEnabled() + ", passportIdDefault= " + getPassportIdDefault() + ", passportIssuanceDateReq= " + getPassportIssuanceDateReq() + ", passportIssuanceDateVisible= " + getPassportIssuanceDateVisible() + ", passportIssuanceDateEnabled= " + getPassportIssuanceDateEnabled() + ", passportIssuanceDateDefault= " + getPassportIssuanceDateDefault() + ", phoneOneReq= " + getPhoneOneReq() + ", phoneOneVisible= " + getPhoneOneVisible() + ", phoneOneEnabled= " + getPhoneOneEnabled() + ", phoneOneDefault= " + getPhoneOneDefault() + ", phoneTwoReq= " + getPhoneTwoReq() + ", phoneTwoVisible= " + getPhoneTwoVisible() + ", phoneTwoEnabled= " + getPhoneTwoEnabled() + ", phoneTwoDefault= " + getPhoneTwoDefault() + ", areaReq= " + getAreaReq() + ", areaVisible= " + getAreaVisible() + ", areaEnabled= " + getAreaEnabled() + ", poboxReq= " + getPoboxReq() + ", poboxVisible= " + getPoboxVisible() + ", poboxEnabled= " + getPoboxEnabled() + ", poboxDefault= " + getPoboxDefault() + ", zipCodeReq= " + getZipCodeReq() + ", zipCodeVisible= " + getZipCodeVisible() + ", zipCodeEnabled= " + getZipCodeEnabled() + ", zipCodeDefault= " + getZipCodeDefault() + ", buildingNumReq= " + getBuildingNumReq() + ", buildingNumVisible= " + getBuildingNumVisible() + ", buildingNumEnabled= " + getBuildingNumEnabled() + ", buildingNumDefault= " + getBuildingNumDefault() + ", streetNameReq= " + getStreetNameReq() + ", streetNameVisible= " + getStreetNameVisible() + ", streetNameEnabled= " + getStreetNameEnabled() + ", streetNameDefault= " + getStreetNameDefault() + ", addressReq= " + getAddressReq() + ", addressVisible= " + getAddressVisible() + ", addressEnabled= " + getAddressEnabled() + ", addressDefault= " + getAddressDefault() + ", noteReq= " + getNoteReq() + ", noteVisible= " + getNoteVisible() + ", noteEnabled= " + getNoteEnabled() + ", noteDefault= " + getNoteDefault() + ", occupationReq= " + getOccupationReq() + ", occupationVisible= " + getOccupationVisible() + ", occupationEnabled= " + getOccupationEnabled() + ", occupationDefault= " + getOccupationDefault() + ", idExpiryDateReq= " + getIdExpiryDateReq() + ", idExpiryDateVisible= " + getIdExpiryDateVisible() + ", idExpiryDateEnabled= " + getIdExpiryDateEnabled() + ", idExpiryDateDefault= " + getIdExpiryDateDefault() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getEnglishFullNameDefault() == null) ? 0 : getEnglishFullNameDefault().hashCode());
result = prime * result + ((getArabicFullNameDefault() == null) ? 0 : getArabicFullNameDefault().hashCode());
result = prime * result + ((getIdentificationReferenceDefault() == null) ? 0 : getIdentificationReferenceDefault().hashCode());
result = prime * result + ((getIdentificationCardDefault() == null) ? 0 : getIdentificationCardDefault().hashCode());
result = prime * result + ((getIdCardIssuanceDateDefault() == null) ? 0 : getIdCardIssuanceDateDefault().hashCode());
result = prime * result + ((getPassportIdDefault() == null) ? 0 : getPassportIdDefault().hashCode());
result = prime * result + ((getPassportIssuanceDateDefault() == null) ? 0 : getPassportIssuanceDateDefault().hashCode());
result = prime * result + ((getPhoneOneDefault() == null) ? 0 : getPhoneOneDefault().hashCode());
result = prime * result + ((getPhoneTwoDefault() == null) ? 0 : getPhoneTwoDefault().hashCode());
result = prime * result + ((getPoboxDefault() == null) ? 0 : getPoboxDefault().hashCode());
result = prime * result + ((getZipCodeDefault() == null) ? 0 : getZipCodeDefault().hashCode());
result = prime * result + ((getBuildingNumDefault() == null) ? 0 : getBuildingNumDefault().hashCode());
result = prime * result + ((getStreetNameDefault() == null) ? 0 : getStreetNameDefault().hashCode());
result = prime * result + ((getOccupationDefault() == null) ? 0 : getOccupationDefault().hashCode());
result = prime * result + ((getIdExpiryDateDefault() == null) ? 0 : getIdExpiryDateDefault().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CustomerKyc))
return false;
else {MPAY_CustomerKyc other = (MPAY_CustomerKyc) obj;
return this.hashCode() == other.hashCode();}
}


}