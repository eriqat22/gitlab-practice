package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_ChargesSlices")
@XmlRootElement(name="ChargesSlices")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ChargesSlice extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ChargesSlice(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String TX_AMOUNT_LIMIT = "txAmountLimit";
@Column(name="TXAMOUNTLIMIT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal txAmountLimit;
public java.math.BigDecimal getTxAmountLimit(){
return this.txAmountLimit;
}
public void setTxAmountLimit(java.math.BigDecimal txAmountLimit){
this.txAmountLimit = txAmountLimit;
}

public static final String TX_COUNT_LIMIT = "txCountLimit";
@Column(name="TXCOUNTLIMIT", nullable=true, length=20)
private Long txCountLimit;
public Long getTxCountLimit(){
return this.txCountLimit;
}
public void setTxCountLimit(Long txCountLimit){
this.txCountLimit = txCountLimit;
}

public static final String CHARGE_TYPE = "chargeType";
@Column(name="CHARGETYPE", nullable=false, length=1)
private String chargeType;
public String getChargeType(){
return this.chargeType;
}
public void setChargeType(String chargeType){
this.chargeType = chargeType;
}

public static final String CHARGE_AMOUNT = "chargeAmount";
@Column(name="CHARGEAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal chargeAmount;
public java.math.BigDecimal getChargeAmount(){
return this.chargeAmount;
}
public void setChargeAmount(java.math.BigDecimal chargeAmount){
this.chargeAmount = chargeAmount;
}

public static final String CHARGE_PERCENT = "chargePercent";
@Column(name="CHARGEPERCENT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal chargePercent;
public java.math.BigDecimal getChargePercent(){
return this.chargePercent;
}
public void setChargePercent(java.math.BigDecimal chargePercent){
this.chargePercent = chargePercent;
}

public static final String MIN_AMOUNT = "minAmount";
@Column(name="MINAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal minAmount;
public java.math.BigDecimal getMinAmount(){
return this.minAmount;
}
public void setMinAmount(java.math.BigDecimal minAmount){
this.minAmount = minAmount;
}

public static final String MAX_AMOUNT = "maxAmount";
@Column(name="MAXAMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal maxAmount;
public java.math.BigDecimal getMaxAmount(){
return this.maxAmount;
}
public void setMaxAmount(java.math.BigDecimal maxAmount){
this.maxAmount = maxAmount;
}

public static final String REF_CHARGES_DETAILS = "refChargesDetails";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCHARGESDETAILSID", nullable=true)
private MPAY_ChargesSchemeDetail refChargesDetails;
public MPAY_ChargesSchemeDetail getRefChargesDetails(){
return this.refChargesDetails;
}
public void setRefChargesDetails(MPAY_ChargesSchemeDetail refChargesDetails){
this.refChargesDetails = refChargesDetails;
}

@Override
public String toString() {
return "MPAY_ChargesSlice [id= " + getId() + ", txAmountLimit= " + getTxAmountLimit() + ", txCountLimit= " + getTxCountLimit() + ", chargeType= " + getChargeType() + ", chargeAmount= " + getChargeAmount() + ", chargePercent= " + getChargePercent() + ", minAmount= " + getMinAmount() + ", maxAmount= " + getMaxAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
int TxCountLimit= new Long("null".equals(getTxCountLimit() + "") ? 0 : getTxCountLimit()).intValue();
result = prime * result + (int) (TxCountLimit ^ TxCountLimit >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ChargesSlice))
return false;
else {MPAY_ChargesSlice other = (MPAY_ChargesSlice) obj;
return this.hashCode() == other.hashCode();}
}


}