package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_AppsChecksums",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CHECKSUM","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="AppsChecksums")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_AppsChecksum extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_AppsChecksum(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CHECKSUM = "checksum";
@Column(name="CHECKSUM", nullable=false, length=1000)
private String checksum;
public String getChecksum(){
return this.checksum;
}
public void setChecksum(String checksum){
this.checksum = checksum;
}

public static final String CODE = "code";
@Column(name="CODE", nullable=false, length=1000)
private String code;
public String getCode(){
return this.code;
}
public void setCode(String code){
this.code = code;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=true, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

@Override
public String toString() {
return "MPAY_AppsChecksum [id= " + getId() + ", checksum= " + getChecksum() + ", code= " + getCode() + ", isActive= " + getIsActive() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getChecksum() == null) ? 0 : getChecksum().hashCode());
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_AppsChecksum))
return false;
else {MPAY_AppsChecksum other = (MPAY_AppsChecksum) obj;
return this.hashCode() == other.hashCode();}
}


}