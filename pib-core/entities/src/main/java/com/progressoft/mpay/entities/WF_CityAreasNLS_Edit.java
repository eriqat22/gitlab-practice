package com.progressoft.mpay.entities;

public class WF_CityAreasNLS_Edit
 {
    // Steps
    public static final String STEP_Initialization = "107601";
    public static final String STEP_ApprovalNewStep = "107602";
    public static final String STEP_RepairStep = "107603";
    public static final String STEP_ApprovalRepairStep = "107604";
    public static final String STEP_ActiveStep = "107605";
    public static final String STEP_EditStep = "107606";
    public static final String STEP_ApprovalEditStep = "107607";
    public static final String STEP_ApprovalDeleteStep = "107608";
    public static final String STEP_DeletedStep = "107609";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_ApprovalNewStep = "Approval New";
    public static final String STEP_STATUS_RepairStep = "Repair";
    public static final String STEP_STATUS_ApprovalRepairStep = "Approval Repair";
    public static final String STEP_STATUS_ActiveStep = "Active";
    public static final String STEP_STATUS_EditStep = "Edit";
    public static final String STEP_STATUS_ApprovalEditStep = "Approval Edit";
    public static final String STEP_STATUS_ApprovalDeleteStep = "Approval Delete";
    public static final String STEP_STATUS_DeletedStep = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveApprovalNewStep = "3";
    public static final String ACTION_CODE_RejectApprovalNewStep = "4";
    public static final String ACTION_CODE_SubmitRepairStep = "5";
    public static final String ACTION_CODE_DeleteRepairStep = "6";
    public static final String ACTION_CODE_ApproveApprovalRepairStep = "7";
    public static final String ACTION_CODE_RejectApprovalRepairStep = "8";
    public static final String ACTION_CODE_EditActiveStep = "9";
    public static final String ACTION_CODE_DeleteActiveStep = "12";
    public static final String ACTION_CODE_SubmitEditStep = "10";
    public static final String ACTION_CODE_CancelEditStep = "11";
    public static final String ACTION_CODE_ApproveApprovalEditStep = "13";
    public static final String ACTION_CODE_RejectApprovalEditStep = "14";
    public static final String ACTION_CODE_ApproveApprovalDeleteStep = "15";
    public static final String ACTION_CODE_RejectApprovalDeleteStep = "16";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveApprovalNewStep = "447577324";
    public static final String ACTION_KEY_RejectApprovalNewStep = "1492265508";
    public static final String ACTION_KEY_SubmitRepairStep = "1680343008";
    public static final String ACTION_KEY_DeleteRepairStep = "752680321";
    public static final String ACTION_KEY_ApproveApprovalRepairStep = "1757874845";
    public static final String ACTION_KEY_RejectApprovalRepairStep = "1771821854";
    public static final String ACTION_KEY_EditActiveStep = "677828851";
    public static final String ACTION_KEY_DeleteActiveStep = "823973083";
    public static final String ACTION_KEY_SubmitEditStep = "1755562704";
    public static final String ACTION_KEY_CancelEditStep = "353415107";
    public static final String ACTION_KEY_ApproveApprovalEditStep = "1640774237";
    public static final String ACTION_KEY_RejectApprovalEditStep = "1157966217";
    public static final String ACTION_KEY_ApproveApprovalDeleteStep = "525764031";
    public static final String ACTION_KEY_RejectApprovalDeleteStep = "364060834";

}