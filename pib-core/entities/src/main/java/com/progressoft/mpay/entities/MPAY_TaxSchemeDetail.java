package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_TaxSchemeDetails",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"REFSCHEMEID","MSGTYPEID","Z_TENANT_ID"})
})
@XmlRootElement(name="TaxSchemeDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_TaxSchemeDetail extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_TaxSchemeDetail(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String REF_SCHEME = "refScheme";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFSCHEMEID", nullable=true)
private MPAY_TaxScheme refScheme;
public MPAY_TaxScheme getRefScheme(){
return this.refScheme;
}
public void setRefScheme(MPAY_TaxScheme refScheme){
this.refScheme = refScheme;
}

public static final String MSG_TYPE = "msgType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MSGTYPEID", nullable=false)
private MPAY_MessageType msgType;
public MPAY_MessageType getMsgType(){
return this.msgType;
}
public void setMsgType(MPAY_MessageType msgType){
this.msgType = msgType;
}

public static final String REF_TAX_DETAILS_TAX_SLICES = "refTaxDetailsTaxSlices";
@OneToMany(mappedBy = "refTaxDetails")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_TaxSlice> refTaxDetailsTaxSlices;
public List<MPAY_TaxSlice> getRefTaxDetailsTaxSlices(){
return this.refTaxDetailsTaxSlices;
}
public void setRefTaxDetailsTaxSlices(List<MPAY_TaxSlice> refTaxDetailsTaxSlices){
this.refTaxDetailsTaxSlices = refTaxDetailsTaxSlices;
}

@Override
public String toString() {
return "MPAY_TaxSchemeDetail [id= " + getId() + ", description= " + getDescription() + ", isActive= " + getIsActive() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_TaxSchemeDetail))
return false;
else {MPAY_TaxSchemeDetail other = (MPAY_TaxSchemeDetail) obj;
return this.hashCode() == other.hashCode();}
}


}