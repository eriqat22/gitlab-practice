package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_Banks")
@XmlRootElement(name="Banks")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_Bank extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Bank(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=true, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String SETTLEMENT_PARTICIPANT = "settlementParticipant";
@Column(name="SETTLEMENTPARTICIPANT", nullable=false, length=128)
private String settlementParticipant;
public String getSettlementParticipant(){
return this.settlementParticipant;
}
public void setSettlementParticipant(String settlementParticipant){
this.settlementParticipant = settlementParticipant;
}

public static final String TELLER_CODE = "tellerCode";
@Column(name="TELLERCODE", nullable=true, length=20)
private String tellerCode;
public String getTellerCode(){
return this.tellerCode;
}
public void setTellerCode(String tellerCode){
this.tellerCode = tellerCode;
}

public static final String PROCESSOR = "processor";
@Column(name="PROCESSOR", nullable=true, length=1000)
private String processor;
public String getProcessor(){
return this.processor;
}
public void setProcessor(String processor){
this.processor = processor;
}

public static final String BIC_CODE = "bicCode";
@Column(name="BICCODE", nullable=true, length=20)
private String bicCode;
public String getBicCode(){
return this.bicCode;
}
public void setBicCode(String bicCode){
this.bicCode = bicCode;
}

public static final String REF_CHARGE_ACCOUNT = "refChargeAccount";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCHARGEACCOUNTID", nullable=true)
private MPAY_Account refChargeAccount;
public MPAY_Account getRefChargeAccount(){
return this.refChargeAccount;
}
public void setRefChargeAccount(MPAY_Account refChargeAccount){
this.refChargeAccount = refChargeAccount;
}

public static final String SENDER_BANK_TRANSACTIONS = "senderBankTransactions";
@OneToMany(mappedBy = "senderBank")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> senderBankTransactions;
public List<MPAY_Transaction> getSenderBankTransactions(){
return this.senderBankTransactions;
}
public void setSenderBankTransactions(List<MPAY_Transaction> senderBankTransactions){
this.senderBankTransactions = senderBankTransactions;
}

public static final String RECEIVER_BANK_TRANSACTIONS = "receiverBankTransactions";
@OneToMany(mappedBy = "receiverBank")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> receiverBankTransactions;
public List<MPAY_Transaction> getReceiverBankTransactions(){
return this.receiverBankTransactions;
}
public void setReceiverBankTransactions(List<MPAY_Transaction> receiverBankTransactions){
this.receiverBankTransactions = receiverBankTransactions;
}

public static final String BANK_BANKS_USERS = "bankBanksUsers";
@OneToMany(mappedBy = "bank")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_BanksUser> bankBanksUsers;
public List<MPAY_BanksUser> getBankBanksUsers(){
return this.bankBanksUsers;
}
public void setBankBanksUsers(List<MPAY_BanksUser> bankBanksUsers){
this.bankBanksUsers = bankBanksUsers;
}

public static final String REF_BANK_BANK_INTEG_SETTINGS = "refBankBankIntegSettings";
@OneToMany(mappedBy = "refBank")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_BankIntegSetting> refBankBankIntegSettings;
public List<MPAY_BankIntegSetting> getRefBankBankIntegSettings(){
return this.refBankBankIntegSettings;
}
public void setRefBankBankIntegSettings(List<MPAY_BankIntegSetting> refBankBankIntegSettings){
this.refBankBankIntegSettings = refBankBankIntegSettings;
}

@Override
public String toString() {
return "MPAY_Bank [id= " + getId() + ", description= " + getDescription() + ", name= " + getName() + ", code= " + getCode() + ", isActive= " + getIsActive() + ", settlementParticipant= " + getSettlementParticipant() + ", tellerCode= " + getTellerCode() + ", processor= " + getProcessor() + ", bicCode= " + getBicCode() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getTellerCode() == null) ? 0 : getTellerCode().hashCode());
result = prime * result + ((getProcessor() == null) ? 0 : getProcessor().hashCode());
result = prime * result + ((getBicCode() == null) ? 0 : getBicCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Bank))
return false;
else {MPAY_Bank other = (MPAY_Bank) obj;
return this.hashCode() == other.hashCode();}
}


}