package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_BankIntegReasons",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","PROCESSORCODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="BankIntegReasons")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_BankIntegReason extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_BankIntegReason(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String PROCESSOR_CODE = "processorCode";
@Column(name="PROCESSORCODE", nullable=true, length=12)
private String processorCode;
public String getProcessorCode(){
return this.processorCode;
}
public void setProcessorCode(String processorCode){
this.processorCode = processorCode;
}

public static final String BANK_INTEG_REASONS_N_L_S = "bankIntegReasonsNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "rsnId")
@MapKey(name="languageCode")
private Map<String, MPAY_BankIntegReasons_NLS> bankIntegReasonsNLS;
public Map<String, MPAY_BankIntegReasons_NLS> getBankIntegReasonsNLS(){
return this.bankIntegReasonsNLS;
}
public void setBankIntegReasonsNLS(Map<String, MPAY_BankIntegReasons_NLS> bankIntegReasonsNLS){
this.bankIntegReasonsNLS = bankIntegReasonsNLS;
}

@Override
public String toString() {
return "MPAY_BankIntegReason [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", processorCode= " + getProcessorCode() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getProcessorCode() == null) ? 0 : getProcessorCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_BankIntegReason))
return false;
else {MPAY_BankIntegReason other = (MPAY_BankIntegReason) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.bankIntegReasonsNLS;
}
}