package com.progressoft.mpay.entities;

public class WF_Notifications
 {
    // Steps
    public static final String STEP_Initialization = "100901";
    public static final String STEP_Send = "100902";
    public static final String STEP_Sent = "100903";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_Send = "Sending Notification";
    public static final String STEP_STATUS_Sent = "Sent";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_SVC_Send = "SVC_Send";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SVC_SendSend = "3";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SVC_SendSend = "2119571920";

}