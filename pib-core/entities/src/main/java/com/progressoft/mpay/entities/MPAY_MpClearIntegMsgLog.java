package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_MpClearIntegMsgLogs")
@XmlRootElement(name="MpClearIntegMsgLogs")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_MpClearIntegMsgLog extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_MpClearIntegMsgLog(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String INTEG_MSG_TYPE = "integMsgType";
@Column(name="INTEGMSGTYPE", nullable=true, length=255)
private String integMsgType;
public String getIntegMsgType(){
return this.integMsgType;
}
public void setIntegMsgType(String integMsgType){
this.integMsgType = integMsgType;
}

public static final String SOURCE = "source";
@Column(name="SOURCE", nullable=false, length=1)
private String source;
public String getSource(){
return this.source;
}
public void setSource(String source){
this.source = source;
}

public static final String REF_SENDER = "refSender";
@Column(name="REFSENDER", nullable=true, length=255)
private String refSender;
public String getRefSender(){
return this.refSender;
}
public void setRefSender(String refSender){
this.refSender = refSender;
}

public static final String SIGNING_STAMP = "signingStamp";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="SIGNINGSTAMP", nullable=true, length=255)
private java.sql.Timestamp signingStamp;
public java.sql.Timestamp getSigningStamp(){
return this.signingStamp;
}
public void setSigningStamp(java.sql.Timestamp signingStamp){
this.signingStamp = signingStamp;
}

public static final String CONTENT = "content";
@Column(name="CONTENT", nullable=true, length=4000)
private String content;
public String getContent(){
return this.content;
}
public void setContent(String content){
this.content = content;
}

public static final String TOKEN = "token";
@Column(name="TOKEN", nullable=true, length=4000)
private String token;
public String getToken(){
return this.token;
}
public void setToken(String token){
this.token = token;
}

public static final String HINT = "hint";
@Column(name="HINT", nullable=true, length=255)
private String hint;
public String getHint(){
return this.hint;
}
public void setHint(String hint){
this.hint = hint;
}

public static final String MESSAGE_ID = "messageId";
@Column(name="MESSAGEID", nullable=true, length=50)
private String messageId;
public String getMessageId(){
return this.messageId;
}
public void setMessageId(String messageId){
this.messageId = messageId;
}

public static final String RESPONSE_RECEIVED = "responseReceived";
@Column(name="RESPONSERECEIVED", nullable=false, length=1)
private Boolean responseReceived;
public Boolean getResponseReceived(){
return this.responseReceived;
}
public void setResponseReceived(Boolean responseReceived){
this.responseReceived = responseReceived;
}

public static final String IS_CANCELED = "isCanceled";
@Column(name="ISCANCELED", nullable=false, length=1)
private Boolean isCanceled;
public Boolean getIsCanceled(){
return this.isCanceled;
}
public void setIsCanceled(Boolean isCanceled){
this.isCanceled = isCanceled;
}

public static final String IS_PROCESSED = "isProcessed";
@Column(name="ISPROCESSED", nullable=false, length=1)
private Boolean isProcessed;
public Boolean getIsProcessed(){
return this.isProcessed;
}
public void setIsProcessed(Boolean isProcessed){
this.isProcessed = isProcessed;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=true)
private MPAY_MpClearIntegRjctReason reason;
public MPAY_MpClearIntegRjctReason getReason(){
return this.reason;
}
public void setReason(MPAY_MpClearIntegRjctReason reason){
this.reason = reason;
}

public static final String REF_MESSAGE = "refMessage";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMESSAGEID", nullable=true)
private MPAY_MPayMessage refMessage;
public MPAY_MPayMessage getRefMessage(){
return this.refMessage;
}
public void setRefMessage(MPAY_MPayMessage refMessage){
this.refMessage = refMessage;
}

public static final String INWARD_TRANSACTION = "inwardTransaction";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="INWARDTRANSACTIONID", nullable=true)
private MPAY_Transaction inwardTransaction;
public MPAY_Transaction getInwardTransaction(){
return this.inwardTransaction;
}
public void setInwardTransaction(MPAY_Transaction inwardTransaction){
this.inwardTransaction = inwardTransaction;
}

public static final String REF_LAST_MSG_LOG_SERVICE_CASH_OUT = "refLastMsgLogServiceCashOut";
@OneToMany(mappedBy = "refLastMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceCashOut> refLastMsgLogServiceCashOut;
public List<MPAY_ServiceCashOut> getRefLastMsgLogServiceCashOut(){
return this.refLastMsgLogServiceCashOut;
}
public void setRefLastMsgLogServiceCashOut(List<MPAY_ServiceCashOut> refLastMsgLogServiceCashOut){
this.refLastMsgLogServiceCashOut = refLastMsgLogServiceCashOut;
}

public static final String REF_LAST_MSG_LOG_SERVICE_CASH_IN = "refLastMsgLogServiceCashIn";
@OneToMany(mappedBy = "refLastMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceCashIn> refLastMsgLogServiceCashIn;
public List<MPAY_ServiceCashIn> getRefLastMsgLogServiceCashIn(){
return this.refLastMsgLogServiceCashIn;
}
public void setRefLastMsgLogServiceCashIn(List<MPAY_ServiceCashIn> refLastMsgLogServiceCashIn){
this.refLastMsgLogServiceCashIn = refLastMsgLogServiceCashIn;
}

public static final String REF_LAST_MSG_LOG_CORPORATES = "refLastMsgLogCorporates";
@OneToMany(mappedBy = "refLastMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Corporate> refLastMsgLogCorporates;
public List<MPAY_Corporate> getRefLastMsgLogCorporates(){
return this.refLastMsgLogCorporates;
}
public void setRefLastMsgLogCorporates(List<MPAY_Corporate> refLastMsgLogCorporates){
this.refLastMsgLogCorporates = refLastMsgLogCorporates;
}

public static final String REF_LAST_MSG_LOG_CASH_IN = "refLastMsgLogCashIn";
@OneToMany(mappedBy = "refLastMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CashIn> refLastMsgLogCashIn;
public List<MPAY_CashIn> getRefLastMsgLogCashIn(){
return this.refLastMsgLogCashIn;
}
public void setRefLastMsgLogCashIn(List<MPAY_CashIn> refLastMsgLogCashIn){
this.refLastMsgLogCashIn = refLastMsgLogCashIn;
}

public static final String REF_LAST_MSG_LOG_CUSTOMERS = "refLastMsgLogCustomers";
@OneToMany(mappedBy = "refLastMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Customer> refLastMsgLogCustomers;
public List<MPAY_Customer> getRefLastMsgLogCustomers(){
return this.refLastMsgLogCustomers;
}
public void setRefLastMsgLogCustomers(List<MPAY_Customer> refLastMsgLogCustomers){
this.refLastMsgLogCustomers = refLastMsgLogCustomers;
}

public static final String REF_LAST_MSG_LOG_NETWORK_MANAGEMENTS = "refLastMsgLogNetworkManagements";
@OneToMany(mappedBy = "refLastMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_NetworkManagement> refLastMsgLogNetworkManagements;
public List<MPAY_NetworkManagement> getRefLastMsgLogNetworkManagements(){
return this.refLastMsgLogNetworkManagements;
}
public void setRefLastMsgLogNetworkManagements(List<MPAY_NetworkManagement> refLastMsgLogNetworkManagements){
this.refLastMsgLogNetworkManagements = refLastMsgLogNetworkManagements;
}

public static final String REF_MSG_LOG_CUST_INTEG_MESSAGES = "refMsgLogCustIntegMessages";
@OneToMany(mappedBy = "refMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CustIntegMessage> refMsgLogCustIntegMessages;
public List<MPAY_CustIntegMessage> getRefMsgLogCustIntegMessages(){
return this.refMsgLogCustIntegMessages;
}
public void setRefMsgLogCustIntegMessages(List<MPAY_CustIntegMessage> refMsgLogCustIntegMessages){
this.refMsgLogCustIntegMessages = refMsgLogCustIntegMessages;
}

public static final String REF_MSG_LOG_CORP_INTEG_MESSAGES = "refMsgLogCorpIntegMessages";
@OneToMany(mappedBy = "refMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CorpIntegMessage> refMsgLogCorpIntegMessages;
public List<MPAY_CorpIntegMessage> getRefMsgLogCorpIntegMessages(){
return this.refMsgLogCorpIntegMessages;
}
public void setRefMsgLogCorpIntegMessages(List<MPAY_CorpIntegMessage> refMsgLogCorpIntegMessages){
this.refMsgLogCorpIntegMessages = refMsgLogCorpIntegMessages;
}

public static final String REF_MSG_LOG_NETWORK_MAN_INTEG_MSGS = "refMsgLogNetworkManIntegMsgs";
@OneToMany(mappedBy = "refMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_NetworkManIntegMsg> refMsgLogNetworkManIntegMsgs;
public List<MPAY_NetworkManIntegMsg> getRefMsgLogNetworkManIntegMsgs(){
return this.refMsgLogNetworkManIntegMsgs;
}
public void setRefMsgLogNetworkManIntegMsgs(List<MPAY_NetworkManIntegMsg> refMsgLogNetworkManIntegMsgs){
this.refMsgLogNetworkManIntegMsgs = refMsgLogNetworkManIntegMsgs;
}

public static final String REF_MSG_LOG_FINANCIAL_INTEG_MSGS = "refMsgLogFinancialIntegMsgs";
@OneToMany(mappedBy = "refMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_FinancialIntegMsg> refMsgLogFinancialIntegMsgs;
public List<MPAY_FinancialIntegMsg> getRefMsgLogFinancialIntegMsgs(){
return this.refMsgLogFinancialIntegMsgs;
}
public void setRefMsgLogFinancialIntegMsgs(List<MPAY_FinancialIntegMsg> refMsgLogFinancialIntegMsgs){
this.refMsgLogFinancialIntegMsgs = refMsgLogFinancialIntegMsgs;
}

public static final String REF_LAST_MSG_LOG_CASH_OUT = "refLastMsgLogCashOut";
@OneToMany(mappedBy = "refLastMsgLog")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CashOut> refLastMsgLogCashOut;
public List<MPAY_CashOut> getRefLastMsgLogCashOut(){
return this.refLastMsgLogCashOut;
}
public void setRefLastMsgLogCashOut(List<MPAY_CashOut> refLastMsgLogCashOut){
this.refLastMsgLogCashOut = refLastMsgLogCashOut;
}

@Override
public String toString() {
return "MPAY_MpClearIntegMsgLog [id= " + getId() + ", integMsgType= " + getIntegMsgType() + ", source= " + getSource() + ", refSender= " + getRefSender() + ", signingStamp= " + getSigningStamp() + ", content= " + getContent() + ", token= " + getToken() + ", hint= " + getHint() + ", messageId= " + getMessageId() + ", responseReceived= " + getResponseReceived() + ", isCanceled= " + getIsCanceled() + ", isProcessed= " + getIsProcessed() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getIntegMsgType() == null) ? 0 : getIntegMsgType().hashCode());
result = prime * result + ((getRefSender() == null) ? 0 : getRefSender().hashCode());
result = prime * result + ((getHint() == null) ? 0 : getHint().hashCode());
result = prime * result + ((getMessageId() == null) ? 0 : getMessageId().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_MpClearIntegMsgLog))
return false;
else {MPAY_MpClearIntegMsgLog other = (MPAY_MpClearIntegMsgLog) obj;
return this.hashCode() == other.hashCode();}
}


}