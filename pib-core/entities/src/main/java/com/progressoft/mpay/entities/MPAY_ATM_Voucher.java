package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_ATM_Vouchers")
@XmlRootElement(name="ATM_Vouchers")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ATM_Voucher extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ATM_Voucher(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String VALUE = "value";
@Column(name="VALUE", nullable=false, length=16)
private String value;
public String getValue(){
return this.value;
}
public void setValue(String value){
this.value = value;
}

public static final String REQUEST_DATE = "requestDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REQUESTDATE", nullable=false, length=100)
private java.sql.Timestamp requestDate;
public java.sql.Timestamp getRequestDate(){
return this.requestDate;
}
public void setRequestDate(java.sql.Timestamp requestDate){
this.requestDate = requestDate;
}

public static final String EXPIRY_DATE = "expiryDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="EXPIRYDATE", nullable=false, length=100)
private java.sql.Timestamp expiryDate;
public java.sql.Timestamp getExpiryDate(){
return this.expiryDate;
}
public void setExpiryDate(java.sql.Timestamp expiryDate){
this.expiryDate = expiryDate;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String IS_USED = "isUsed";
@Column(name="ISUSED", nullable=false, length=1)
private Boolean isUsed;
public Boolean getIsUsed(){
return this.isUsed;
}
public void setIsUsed(Boolean isUsed){
this.isUsed = isUsed;
}

public static final String IS_CANCELED = "isCanceled";
@Column(name="ISCANCELED", nullable=false, length=1)
private Boolean isCanceled;
public Boolean getIsCanceled(){
return this.isCanceled;
}
public void setIsCanceled(Boolean isCanceled){
this.isCanceled = isCanceled;
}

public static final String REF_MOBILE = "refMobile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMOBILEID", nullable=false)
private MPAY_CustomerMobile refMobile;
public MPAY_CustomerMobile getRefMobile(){
return this.refMobile;
}
public void setRefMobile(MPAY_CustomerMobile refMobile){
this.refMobile = refMobile;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=true)
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_ATM_Voucher [id= " + getId() + ", value= " + getValue() + ", requestDate= " + getRequestDate() + ", expiryDate= " + getExpiryDate() + ", amount= " + getAmount() + ", isUsed= " + getIsUsed() + ", isCanceled= " + getIsCanceled() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getValue() == null) ? 0 : getValue().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ATM_Voucher))
return false;
else {MPAY_ATM_Voucher other = (MPAY_ATM_Voucher) obj;
return this.hashCode() == other.hashCode();}
}


}