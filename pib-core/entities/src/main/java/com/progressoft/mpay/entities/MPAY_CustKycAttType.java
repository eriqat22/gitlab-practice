package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_CustKycAttTypes")
@XmlRootElement(name="CustKycAttTypes")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_CustKycAttType extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CustKycAttType(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REQUIRED = "required";
@Column(name="REQUIRED", nullable=false, length=150)
private Boolean required;
public Boolean getRequired(){
return this.required;
}
public void setRequired(Boolean required){
this.required = required;
}

public static final String REF_KYC = "refKyc";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFKYCID", nullable=false)
private MPAY_CustomerKyc refKyc;
public MPAY_CustomerKyc getRefKyc(){
return this.refKyc;
}
public void setRefKyc(MPAY_CustomerKyc refKyc){
this.refKyc = refKyc;
}

public static final String ATTACHMENT_TYPE = "attachmentType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="ATTACHMENTTYPEID", nullable=false)
private MPAY_AttachmentType attachmentType;
public MPAY_AttachmentType getAttachmentType(){
return this.attachmentType;
}
public void setAttachmentType(MPAY_AttachmentType attachmentType){
this.attachmentType = attachmentType;
}

@Override
public String toString() {
return "MPAY_CustKycAttType [id= " + getId() + ", required= " + getRequired() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CustKycAttType))
return false;
else {MPAY_CustKycAttType other = (MPAY_CustKycAttType) obj;
return this.hashCode() == other.hashCode();}
}


}