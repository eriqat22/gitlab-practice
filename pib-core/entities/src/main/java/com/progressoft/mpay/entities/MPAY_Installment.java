package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_Installment")
@XmlRootElement(name="Installment")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Installment extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Installment(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CUSTOMER_NAME = "customerName";
@Column(name="CUSTOMERNAME", nullable=true, length=100)
private String customerName;
public String getCustomerName(){
return this.customerName;
}
public void setCustomerName(String customerName){
this.customerName = customerName;
}

public static final String CUST_NATIONAL_ID = "custNationalId";
@Column(name="CUSTNATIONALID", nullable=true, length=20)
private String custNationalId;
public String getCustNationalId(){
return this.custNationalId;
}
public void setCustNationalId(String custNationalId){
this.custNationalId = custNationalId;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String TRAN_DATE = "tranDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="TRANDATE", nullable=true, length=12)
private java.sql.Timestamp tranDate;
public java.sql.Timestamp getTranDate(){
return this.tranDate;
}
public void setTranDate(java.sql.Timestamp tranDate){
this.tranDate = tranDate;
}

public static final String BORROWER_NATIONAL_ID = "borrowerNationalId";
@Column(name="BORROWERNATIONALID", nullable=true, length=20)
private String borrowerNationalId;
public String getBorrowerNationalId(){
return this.borrowerNationalId;
}
public void setBorrowerNationalId(String borrowerNationalId){
this.borrowerNationalId = borrowerNationalId;
}

public static final String BORROWER_NAME = "borrowerName";
@Column(name="BORROWERNAME", nullable=true, length=100)
private String borrowerName;
public String getBorrowerName(){
return this.borrowerName;
}
public void setBorrowerName(String borrowerName){
this.borrowerName = borrowerName;
}

public static final String RECEIVER_NAME = "receiverName";
@Column(name="RECEIVERNAME", nullable=true, length=100)
private String receiverName;
public String getReceiverName(){
return this.receiverName;
}
public void setReceiverName(String receiverName){
this.receiverName = receiverName;
}

public static final String TRAN_REFERENCE = "tranReference";
@Column(name="TRANREFERENCE", nullable=true, length=50)
private String tranReference;
public String getTranReference(){
return this.tranReference;
}
public void setTranReference(String tranReference){
this.tranReference = tranReference;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=true)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_Installment [id= " + getId() + ", customerName= " + getCustomerName() + ", custNationalId= " + getCustNationalId() + ", amount= " + getAmount() + ", tranDate= " + getTranDate() + ", borrowerNationalId= " + getBorrowerNationalId() + ", borrowerName= " + getBorrowerName() + ", receiverName= " + getReceiverName() + ", tranReference= " + getTranReference() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCustomerName() == null) ? 0 : getCustomerName().hashCode());
result = prime * result + ((getCustNationalId() == null) ? 0 : getCustNationalId().hashCode());
result = prime * result + ((getBorrowerNationalId() == null) ? 0 : getBorrowerNationalId().hashCode());
result = prime * result + ((getBorrowerName() == null) ? 0 : getBorrowerName().hashCode());
result = prime * result + ((getReceiverName() == null) ? 0 : getReceiverName().hashCode());
result = prime * result + ((getTranReference() == null) ? 0 : getTranReference().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Installment))
return false;
else {MPAY_Installment other = (MPAY_Installment) obj;
return this.hashCode() == other.hashCode();}
}


}