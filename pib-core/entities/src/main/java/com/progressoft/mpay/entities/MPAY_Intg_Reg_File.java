package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_Intg_Reg_Files")
@XmlRootElement(name="Intg_Reg_Files")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Intg_Reg_File extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Intg_Reg_File(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REG_FILE_NAME = "regFileName";
@Column(name="REGFILENAME", nullable=false, length=128)
private String regFileName;
public String getRegFileName(){
return this.regFileName;
}
public void setRegFileName(String regFileName){
this.regFileName = regFileName;
}

public static final String REG_FILE_RECEIVING_DT = "regFileReceivingDt";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REGFILERECEIVINGDT", nullable=false, length=100)
private java.sql.Timestamp regFileReceivingDt;
public java.sql.Timestamp getRegFileReceivingDt(){
return this.regFileReceivingDt;
}
public void setRegFileReceivingDt(java.sql.Timestamp regFileReceivingDt){
this.regFileReceivingDt = regFileReceivingDt;
}

public static final String REG_FILE_PROCESSING_DT = "regFileProcessingDt";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REGFILEPROCESSINGDT", nullable=false, length=100)
private java.sql.Timestamp regFileProcessingDt;
public java.sql.Timestamp getRegFileProcessingDt(){
return this.regFileProcessingDt;
}
public void setRegFileProcessingDt(java.sql.Timestamp regFileProcessingDt){
this.regFileProcessingDt = regFileProcessingDt;
}

public static final String REG_FILE_PARSING_STTS = "regFileParsingStts";
@Column(name="REGFILEPARSINGSTTS", nullable=false, length=255)
private Boolean regFileParsingStts;
public Boolean getRegFileParsingStts(){
return this.regFileParsingStts;
}
public void setRegFileParsingStts(Boolean regFileParsingStts){
this.regFileParsingStts = regFileParsingStts;
}

public static final String REG_FILE_RECORDS = "regFileRecords";
@Column(name="REGFILERECORDS", nullable=false, length=20)
private Long regFileRecords;
public Long getRegFileRecords(){
return this.regFileRecords;
}
public void setRegFileRecords(Long regFileRecords){
this.regFileRecords = regFileRecords;
}

public static final String REG_FILE_PROCESSED_RECORDS = "regFileProcessedRecords";
@Column(name="REGFILEPROCESSEDRECORDS", nullable=false, length=20)
private Long regFileProcessedRecords;
public Long getRegFileProcessedRecords(){
return this.regFileProcessedRecords;
}
public void setRegFileProcessedRecords(Long regFileProcessedRecords){
this.regFileProcessedRecords = regFileProcessedRecords;
}

public static final String REG_FILE_TYPE = "regFileType";
@Column(name="REGFILETYPE", nullable=false, length=20)
private String regFileType;
public String getRegFileType(){
return this.regFileType;
}
public void setRegFileType(String regFileType){
this.regFileType = regFileType;
}

public static final String REG_FILE_PROCESSING_STTS = "regFileProcessingStts";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REGFILEPROCESSINGSTTSID", nullable=false)
private MPAY_Reg_File_Stt regFileProcessingStts;
public MPAY_Reg_File_Stt getRegFileProcessingStts(){
return this.regFileProcessingStts;
}
public void setRegFileProcessingStts(MPAY_Reg_File_Stt regFileProcessingStts){
this.regFileProcessingStts = regFileProcessingStts;
}

public static final String REF_REG_FILE_INTG_CORP_REG_INSTRS = "refRegFileIntg_Corp_Reg_Instrs";
@OneToMany(mappedBy = "refRegFile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Intg_Corp_Reg_Instr> refRegFileIntg_Corp_Reg_Instrs;
public List<MPAY_Intg_Corp_Reg_Instr> getRefRegFileIntgCorpRegInstrs(){
return this.refRegFileIntg_Corp_Reg_Instrs;
}
public void setRefRegFileIntgCorpRegInstrs(List<MPAY_Intg_Corp_Reg_Instr> refRegFileIntg_Corp_Reg_Instrs){
this.refRegFileIntg_Corp_Reg_Instrs = refRegFileIntg_Corp_Reg_Instrs;
}

public static final String REF_REG_FILE_INTG_CUST_REG_INSTRS = "refRegFileIntg_Cust_Reg_Instrs";
@OneToMany(mappedBy = "refRegFile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Intg_Cust_Reg_Instr> refRegFileIntg_Cust_Reg_Instrs;
public List<MPAY_Intg_Cust_Reg_Instr> getRefRegFileIntgCustRegInstrs(){
return this.refRegFileIntg_Cust_Reg_Instrs;
}
public void setRefRegFileIntgCustRegInstrs(List<MPAY_Intg_Cust_Reg_Instr> refRegFileIntg_Cust_Reg_Instrs){
this.refRegFileIntg_Cust_Reg_Instrs = refRegFileIntg_Cust_Reg_Instrs;
}

@Override
public String toString() {
return "MPAY_Intg_Reg_File [id= " + getId() + ", regFileName= " + getRegFileName() + ", regFileReceivingDt= " + getRegFileReceivingDt() + ", regFileProcessingDt= " + getRegFileProcessingDt() + ", regFileParsingStts= " + getRegFileParsingStts() + ", regFileRecords= " + getRegFileRecords() + ", regFileProcessedRecords= " + getRegFileProcessedRecords() + ", regFileType= " + getRegFileType() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRegFileName() == null) ? 0 : getRegFileName().hashCode());
int RegFileRecords= new Long("null".equals(getRegFileRecords() + "") ? 0 : getRegFileRecords()).intValue();
result = prime * result + (int) (RegFileRecords ^ RegFileRecords >>> 32);
int RegFileProcessedRecords= new Long("null".equals(getRegFileProcessedRecords() + "") ? 0 : getRegFileProcessedRecords()).intValue();
result = prime * result + (int) (RegFileProcessedRecords ^ RegFileProcessedRecords >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Intg_Reg_File))
return false;
else {MPAY_Intg_Reg_File other = (MPAY_Intg_Reg_File) obj;
return this.hashCode() == other.hashCode();}
}


}