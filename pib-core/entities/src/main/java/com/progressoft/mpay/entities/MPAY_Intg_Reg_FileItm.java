package com.progressoft.mpay.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import com.progressoft.jfw.model.bussinessobject.changeHistory.ChangeItem;
import com.progressoft.jfw.model.bussinessobject.changeHistory.ChangeGroup;

@Entity
@Table(name="MPAY_Intg_Reg_FileITM")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Intg_Reg_FileItm extends ChangeItem implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Intg_Reg_FileItm(){/*Default Constructor*/}

@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="GROUPID", nullable=true)
@XmlTransient
protected MPAY_Intg_Reg_FileGrp changeGroup;

@Override
public void setChangeGroup(ChangeGroup changeGroup) {
if(changeGroup instanceof MPAY_Intg_Reg_FileGrp){
this.changeGroup = (MPAY_Intg_Reg_FileGrp) changeGroup;
}
}

@Override
public ChangeGroup getChangeGroup() {
return this.changeGroup;
}


}