package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_TrnsCheckoutRecords",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CHECKOUTID","Z_TENANT_ID"})
})
@XmlRootElement(name="TrnsCheckoutRecords")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_TrnsCheckoutRecord extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_TrnsCheckoutRecord(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CHECKOUT_ID = "checkoutId";
@Column(name="CHECKOUTID", nullable=false, length=100)
private String checkoutId;
public String getCheckoutId(){
return this.checkoutId;
}
public void setCheckoutId(String checkoutId){
this.checkoutId = checkoutId;
}

public static final String SENDER_MOBILE = "senderMobile";
@Column(name="SENDERMOBILE", nullable=false, length=50)
private String senderMobile;
public String getSenderMobile(){
return this.senderMobile;
}
public void setSenderMobile(String senderMobile){
this.senderMobile = senderMobile;
}

public static final String TOTAL_AMOUNT = "totalAmount";
@Column(name="TOTALAMOUNT", nullable=false, precision=21, scale=8, length=50)
private java.math.BigDecimal totalAmount;
public java.math.BigDecimal getTotalAmount(){
return this.totalAmount;
}
public void setTotalAmount(java.math.BigDecimal totalAmount){
this.totalAmount = totalAmount;
}

public static final String PROCESSING_STATUS = "processingStatus";
@Column(name="PROCESSINGSTATUS", nullable=false, length=50)
private String processingStatus;
public String getProcessingStatus(){
return this.processingStatus;
}
public void setProcessingStatus(String processingStatus){
this.processingStatus = processingStatus;
}

public static final String REF_MESSAGE = "refMessage";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMESSAGEID", nullable=false)
private MPAY_MPayMessage refMessage;
public MPAY_MPayMessage getRefMessage(){
return this.refMessage;
}
public void setRefMessage(MPAY_MPayMessage refMessage){
this.refMessage = refMessage;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_TrnsCheckoutRecord [id= " + getId() + ", checkoutId= " + getCheckoutId() + ", senderMobile= " + getSenderMobile() + ", totalAmount= " + getTotalAmount() + ", processingStatus= " + getProcessingStatus() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCheckoutId() == null) ? 0 : getCheckoutId().hashCode());
result = prime * result + ((getSenderMobile() == null) ? 0 : getSenderMobile().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_TrnsCheckoutRecord))
return false;
else {MPAY_TrnsCheckoutRecord other = (MPAY_TrnsCheckoutRecord) obj;
return this.hashCode() == other.hashCode();}
}


}