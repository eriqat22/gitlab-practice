package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_TransactionConfigs",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"MESSAGETYPEID","SENDERTYPEID","ISSENDERBANKED","RECEIVERTYPEID","ISRECEIVERBANKED","CURRENCYID","REFTYPEID","REFOPERATIONID","REFPROFILEID","Z_TENANT_ID"})
})
@XmlRootElement(name="TransactionConfigs")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_TransactionConfig extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_TransactionConfig(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=false, length=500)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String IS_SENDER_BANKED = "isSenderBanked";
@Column(name="ISSENDERBANKED", nullable=false, length=1)
private Boolean isSenderBanked;
public Boolean getIsSenderBanked(){
return this.isSenderBanked;
}
public void setIsSenderBanked(Boolean isSenderBanked){
this.isSenderBanked = isSenderBanked;
}

public static final String IS_RECEIVER_BANKED = "isReceiverBanked";
@Column(name="ISRECEIVERBANKED", nullable=false, length=1)
private Boolean isReceiverBanked;
public Boolean getIsReceiverBanked(){
return this.isReceiverBanked;
}
public void setIsReceiverBanked(Boolean isReceiverBanked){
this.isReceiverBanked = isReceiverBanked;
}

public static final String MIN_AMOUNT = "minAmount";
@Column(name="MINAMOUNT", nullable=false, length=500)
private Float minAmount;
public Float getMinAmount(){
return this.minAmount;
}
public void setMinAmount(Float minAmount){
this.minAmount = minAmount;
}

public static final String MAX_AMOUNT = "maxAmount";
@Column(name="MAXAMOUNT", nullable=false, length=500)
private Float maxAmount;
public Float getMaxAmount(){
return this.maxAmount;
}
public void setMaxAmount(Float maxAmount){
this.maxAmount = maxAmount;
}

public static final String REVERSABLE = "reversable";
@Column(name="REVERSABLE", nullable=false, length=7)
private Boolean reversable;
public Boolean getReversable(){
return this.reversable;
}
public void setReversable(Boolean reversable){
this.reversable = reversable;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=false)
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

public static final String SENDER_TYPE = "senderType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SENDERTYPEID", nullable=false)
private MPAY_ClientType senderType;
public MPAY_ClientType getSenderType(){
return this.senderType;
}
public void setSenderType(MPAY_ClientType senderType){
this.senderType = senderType;
}

public static final String RECEIVER_TYPE = "receiverType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="RECEIVERTYPEID", nullable=false)
private MPAY_ClientType receiverType;
public MPAY_ClientType getReceiverType(){
return this.receiverType;
}
public void setReceiverType(MPAY_ClientType receiverType){
this.receiverType = receiverType;
}

public static final String REF_TYPE = "refType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTYPEID", nullable=false)
private MPAY_TransactionType refType;
public MPAY_TransactionType getRefType(){
return this.refType;
}
public void setRefType(MPAY_TransactionType refType){
this.refType = refType;
}

public static final String REF_OPERATION = "refOperation";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFOPERATIONID", nullable=false)
private MPAY_EndPointOperation refOperation;
public MPAY_EndPointOperation getRefOperation(){
return this.refOperation;
}
public void setRefOperation(MPAY_EndPointOperation refOperation){
this.refOperation = refOperation;
}

public static final String REF_PROFILE = "refProfile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFPROFILEID", nullable=true)
private MPAY_Profile refProfile;
public MPAY_Profile getRefProfile(){
return this.refProfile;
}
public void setRefProfile(MPAY_Profile refProfile){
this.refProfile = refProfile;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

@Override
public String toString() {
return "MPAY_TransactionConfig [id= " + getId() + ", description= " + getDescription() + ", isSenderBanked= " + getIsSenderBanked() + ", isReceiverBanked= " + getIsReceiverBanked() + ", minAmount= " + getMinAmount() + ", maxAmount= " + getMaxAmount() + ", reversable= " + getReversable() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + Float.floatToIntBits(getMinAmount());
result = prime * result + Float.floatToIntBits(getMaxAmount());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_TransactionConfig))
return false;
else {MPAY_TransactionConfig other = (MPAY_TransactionConfig) obj;
return this.hashCode() == other.hashCode();}
}


}