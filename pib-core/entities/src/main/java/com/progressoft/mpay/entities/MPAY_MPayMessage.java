package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_MPayMessages",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"MESSAGEID","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"REFERENCE","Z_TENANT_ID"})
})
@XmlRootElement(name="MPayMessages")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_MPayMessage extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_MPayMessage(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String MESSAGE_I_D = "messageID";
@Column(name="MESSAGEID", nullable=false, length=255)
private String messageID;
public String getMessageID(){
return this.messageID;
}
public void setMessageID(String messageID){
this.messageID = messageID;
}

public static final String REFERENCE = "reference";
@Column(name="REFERENCE", nullable=true, length=1000)
private String reference;
public String getReference(){
return this.reference;
}
public void setReference(String reference){
this.reference = reference;
}

public static final String SENDER = "sender";
@Column(name="SENDER", nullable=true, length=100)
private String sender;
public String getSender(){
return this.sender;
}
public void setSender(String sender){
this.sender = sender;
}

public static final String REQUEST_CONTENT = "requestContent";
@Column(name="REQUESTCONTENT", nullable=true, length=8000)
@Lob
private String requestContent;
public String getRequestContent(){
return this.requestContent;
}
public void setRequestContent(String requestContent){
this.requestContent = requestContent;
}

public static final String REQUEST_TOKEN = "requestToken";
@Column(name="REQUESTTOKEN", nullable=true, length=4000)
private String requestToken;
public String getRequestToken(){
return this.requestToken;
}
public void setRequestToken(String requestToken){
this.requestToken = requestToken;
}

public static final String RESPONSE_CONTENT = "responseContent";
@Column(name="RESPONSECONTENT", nullable=true, length=8000)
@Lob
private String responseContent;
public String getResponseContent(){
return this.responseContent;
}
public void setResponseContent(String responseContent){
this.responseContent = responseContent;
}

public static final String RESPONSE_TOKEN = "responseToken";
@Column(name="RESPONSETOKEN", nullable=true, length=4000)
private String responseToken;
public String getResponseToken(){
return this.responseToken;
}
public void setResponseToken(String responseToken){
this.responseToken = responseToken;
}

public static final String PROCESSING_STAMP = "processingStamp";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="PROCESSINGSTAMP", nullable=true, length=32)
private java.sql.Timestamp processingStamp;
public java.sql.Timestamp getProcessingStamp(){
return this.processingStamp;
}
public void setProcessingStamp(java.sql.Timestamp processingStamp){
this.processingStamp = processingStamp;
}

public static final String REASON_DESC = "reasonDesc";
@Column(name="REASONDESC", nullable=true, length=4000)
private String reasonDesc;
public String getReasonDesc(){
return this.reasonDesc;
}
public void setReasonDesc(String reasonDesc){
this.reasonDesc = reasonDesc;
}

public static final String REQUESTED_ID = "requestedId";
@Column(name="REQUESTEDID", nullable=true, length=1000)
private String requestedId;
public String getRequestedId(){
return this.requestedId;
}
public void setRequestedId(String requestedId){
this.requestedId = requestedId;
}

public static final String SHOP_ID = "shopId";
@Column(name="SHOPID", nullable=true, length=1000)
private String shopId;
public String getShopId(){
return this.shopId;
}
public void setShopId(String shopId){
this.shopId = shopId;
}

public static final String CHANNEL_ID = "channelId";
@Column(name="CHANNELID", nullable=true, length=1000)
private String channelId;
public String getChannelId(){
return this.channelId;
}
public void setChannelId(String channelId){
this.channelId = channelId;
}

public static final String WALLET_ID = "walletId";
@Column(name="WALLETID", nullable=true, length=1000)
private String walletId;
public String getWalletId(){
return this.walletId;
}
public void setWalletId(String walletId){
this.walletId = walletId;
}

public static final String REF_OPERATION = "refOperation";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFOPERATIONID", nullable=true)
private MPAY_EndPointOperation refOperation;
public MPAY_EndPointOperation getRefOperation(){
return this.refOperation;
}
public void setRefOperation(MPAY_EndPointOperation refOperation){
this.refOperation = refOperation;
}

public static final String MESSAGE_TYPE = "messageType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MESSAGETYPEID", nullable=true)
private MPAY_MessageType messageType;
public MPAY_MessageType getMessageType(){
return this.messageType;
}
public void setMessageType(MPAY_MessageType messageType){
this.messageType = messageType;
}

public static final String PROCESSING_STATUS = "processingStatus";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PROCESSINGSTATUSID", nullable=true)
private MPAY_ProcessingStatus processingStatus;
public MPAY_ProcessingStatus getProcessingStatus(){
return this.processingStatus;
}
public void setProcessingStatus(MPAY_ProcessingStatus processingStatus){
this.processingStatus = processingStatus;
}

public static final String REF_TRSANSACTION = "refTrsansaction";
@OneToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTRSANSACTIONID", nullable=true)
private MPAY_Transaction refTrsansaction;
public MPAY_Transaction getRefTrsansaction(){
return this.refTrsansaction;
}
public void setRefTrsansaction(MPAY_Transaction refTrsansaction){
this.refTrsansaction = refTrsansaction;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=true)
private MPAY_Reason reason;
public MPAY_Reason getReason(){
return this.reason;
}
public void setReason(MPAY_Reason reason){
this.reason = reason;
}

public static final String BULK_PAYMENT = "bulkPayment";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BULKPAYMENTID", nullable=true)
private MPAY_BulkPayment bulkPayment;
public MPAY_BulkPayment getBulkPayment(){
return this.bulkPayment;
}
public void setBulkPayment(MPAY_BulkPayment bulkPayment){
this.bulkPayment = bulkPayment;
}

public static final String REF_MESSAGE_TRNS_CHECKOUT_RECORDS = "refMessageTrnsCheckoutRecords";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_TrnsCheckoutRecord> refMessageTrnsCheckoutRecords;
public List<MPAY_TrnsCheckoutRecord> getRefMessageTrnsCheckoutRecords(){
return this.refMessageTrnsCheckoutRecords;
}
public void setRefMessageTrnsCheckoutRecords(List<MPAY_TrnsCheckoutRecord> refMessageTrnsCheckoutRecords){
this.refMessageTrnsCheckoutRecords = refMessageTrnsCheckoutRecords;
}

public static final String REF_MESSAGE_SERVICE_CASH_OUT = "refMessageServiceCashOut";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceCashOut> refMessageServiceCashOut;
public List<MPAY_ServiceCashOut> getRefMessageServiceCashOut(){
return this.refMessageServiceCashOut;
}
public void setRefMessageServiceCashOut(List<MPAY_ServiceCashOut> refMessageServiceCashOut){
this.refMessageServiceCashOut = refMessageServiceCashOut;
}

public static final String REF_MESSAGE_SERVICE_CASH_IN = "refMessageServiceCashIn";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceCashIn> refMessageServiceCashIn;
public List<MPAY_ServiceCashIn> getRefMessageServiceCashIn(){
return this.refMessageServiceCashIn;
}
public void setRefMessageServiceCashIn(List<MPAY_ServiceCashIn> refMessageServiceCashIn){
this.refMessageServiceCashIn = refMessageServiceCashIn;
}

public static final String REF_MESSAGE_M_P_CLEAR_MESSAGES = "refMessageMPClearMessages";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_MPClearMessage> refMessageMPClearMessages;
public List<MPAY_MPClearMessage> getRefMessageMPClearMessages(){
return this.refMessageMPClearMessages;
}
public void setRefMessageMPClearMessages(List<MPAY_MPClearMessage> refMessageMPClearMessages){
this.refMessageMPClearMessages = refMessageMPClearMessages;
}

public static final String REF_MESSAGE_EXTERNAL_INTEG_MESSAGES = "refMessageExternalIntegMessages";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ExternalIntegMessage> refMessageExternalIntegMessages;
public List<MPAY_ExternalIntegMessage> getRefMessageExternalIntegMessages(){
return this.refMessageExternalIntegMessages;
}
public void setRefMessageExternalIntegMessages(List<MPAY_ExternalIntegMessage> refMessageExternalIntegMessages){
this.refMessageExternalIntegMessages = refMessageExternalIntegMessages;
}

public static final String REF_MESSAGE_SERVICE_INTEG_MESSAGES = "refMessageServiceIntegMessages";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceIntegMessage> refMessageServiceIntegMessages;
public List<MPAY_ServiceIntegMessage> getRefMessageServiceIntegMessages(){
return this.refMessageServiceIntegMessages;
}
public void setRefMessageServiceIntegMessages(List<MPAY_ServiceIntegMessage> refMessageServiceIntegMessages){
this.refMessageServiceIntegMessages = refMessageServiceIntegMessages;
}

public static final String REF_MESSAGE_CASH_IN = "refMessageCashIn";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CashIn> refMessageCashIn;
public List<MPAY_CashIn> getRefMessageCashIn(){
return this.refMessageCashIn;
}
public void setRefMessageCashIn(List<MPAY_CashIn> refMessageCashIn){
this.refMessageCashIn = refMessageCashIn;
}

public static final String REF_MESSAGE_NOTIFICATIONS = "refMessageNotifications";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Notification> refMessageNotifications;
public List<MPAY_Notification> getRefMessageNotifications(){
return this.refMessageNotifications;
}
public void setRefMessageNotifications(List<MPAY_Notification> refMessageNotifications){
this.refMessageNotifications = refMessageNotifications;
}

public static final String REF_MESSAGE_MP_CLEAR_INTEG_MSG_LOGS = "refMessageMpClearIntegMsgLogs";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_MpClearIntegMsgLog> refMessageMpClearIntegMsgLogs;
public List<MPAY_MpClearIntegMsgLog> getRefMessageMpClearIntegMsgLogs(){
return this.refMessageMpClearIntegMsgLogs;
}
public void setRefMessageMpClearIntegMsgLogs(List<MPAY_MpClearIntegMsgLog> refMessageMpClearIntegMsgLogs){
this.refMessageMpClearIntegMsgLogs = refMessageMpClearIntegMsgLogs;
}

public static final String REF_MESSAGE_CASH_OUT = "refMessageCashOut";
@OneToMany(mappedBy = "refMessage")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CashOut> refMessageCashOut;
public List<MPAY_CashOut> getRefMessageCashOut(){
return this.refMessageCashOut;
}
public void setRefMessageCashOut(List<MPAY_CashOut> refMessageCashOut){
this.refMessageCashOut = refMessageCashOut;
}

@Override
public String toString() {
return "MPAY_MPayMessage [id= " + getId() + ", messageID= " + getMessageID() + ", reference= " + getReference() + ", sender= " + getSender() + ", requestContent= " + getRequestContent() + ", requestToken= " + getRequestToken() + ", responseContent= " + getResponseContent() + ", responseToken= " + getResponseToken() + ", processingStamp= " + getProcessingStamp() + ", reasonDesc= " + getReasonDesc() + ", requestedId= " + getRequestedId() + ", shopId= " + getShopId() + ", channelId= " + getChannelId() + ", walletId= " + getWalletId() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getMessageID() == null) ? 0 : getMessageID().hashCode());
result = prime * result + ((getReference() == null) ? 0 : getReference().hashCode());
result = prime * result + ((getSender() == null) ? 0 : getSender().hashCode());
result = prime * result + ((getRequestToken() == null) ? 0 : getRequestToken().hashCode());
result = prime * result + ((getResponseToken() == null) ? 0 : getResponseToken().hashCode());
result = prime * result + ((getRequestedId() == null) ? 0 : getRequestedId().hashCode());
result = prime * result + ((getShopId() == null) ? 0 : getShopId().hashCode());
result = prime * result + ((getChannelId() == null) ? 0 : getChannelId().hashCode());
result = prime * result + ((getWalletId() == null) ? 0 : getWalletId().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_MPayMessage))
return false;
else {MPAY_MPayMessage other = (MPAY_MPayMessage) obj;
return this.hashCode() == other.hashCode();}
}


}