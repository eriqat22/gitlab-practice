package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_AmlHits")
@XmlRootElement(name="AmlHits")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_AmlHit extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_AmlHit(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String ENTITY_REFERENCE = "entityReference";
@Column(name="ENTITYREFERENCE", nullable=true, length=1000)
private String entityReference;
public String getEntityReference(){
return this.entityReference;
}
public void setEntityReference(String entityReference){
this.entityReference = entityReference;
}

public static final String SCORE = "score";
@Column(name="SCORE", nullable=true, length=3)
private String score;
public String getScore(){
return this.score;
}
public void setScore(String score){
this.score = score;
}

public static final String ENGLISH_NAME = "englishName";
@Column(name="ENGLISHNAME", nullable=true, length=1000)
private String englishName;
public String getEnglishName(){
return this.englishName;
}
public void setEnglishName(String englishName){
this.englishName = englishName;
}

public static final String ARABIC_NAME = "arabicName";
@Column(name="ARABICNAME", nullable=true, length=1000)
private String arabicName;
public String getArabicName(){
return this.arabicName;
}
public void setArabicName(String arabicName){
this.arabicName = arabicName;
}

public static final String ENGLISH_ADDRESS = "englishAddress";
@Column(name="ENGLISHADDRESS", nullable=true, length=4000)
private String englishAddress;
public String getEnglishAddress(){
return this.englishAddress;
}
public void setEnglishAddress(String englishAddress){
this.englishAddress = englishAddress;
}

public static final String DOCUMENT_SERIAL = "documentSerial";
@Column(name="DOCUMENTSERIAL", nullable=true, length=1000)
private String documentSerial;
public String getDocumentSerial(){
return this.documentSerial;
}
public void setDocumentSerial(String documentSerial){
this.documentSerial = documentSerial;
}

public static final String DATE_OF_BIRTH = "dateOfBirth";
@Column(name="DATEOFBIRTH", nullable=true, length=100)
private String dateOfBirth;
public String getDateOfBirth(){
return this.dateOfBirth;
}
public void setDateOfBirth(String dateOfBirth){
this.dateOfBirth = dateOfBirth;
}

public static final String TYPE_OF_ALIAS = "typeOfAlias";
@Column(name="TYPEOFALIAS", nullable=true, length=100)
private String typeOfAlias;
public String getTypeOfAlias(){
return this.typeOfAlias;
}
public void setTypeOfAlias(String typeOfAlias){
this.typeOfAlias = typeOfAlias;
}

public static final String IS_MATCH_FLAG = "isMatchFlag";
@Column(name="ISMATCHFLAG", nullable=true, length=5)
private String isMatchFlag;
public String getIsMatchFlag(){
return this.isMatchFlag;
}
public void setIsMatchFlag(String isMatchFlag){
this.isMatchFlag = isMatchFlag;
}

public static final String ITEM_SOURCE = "itemSource";
@Column(name="ITEMSOURCE", nullable=true, length=1000)
private String itemSource;
public String getItemSource(){
return this.itemSource;
}
public void setItemSource(String itemSource){
this.itemSource = itemSource;
}

public static final String ITEM_CATEGORY = "itemCategory";
@Column(name="ITEMCATEGORY", nullable=true, length=1000)
private String itemCategory;
public String getItemCategory(){
return this.itemCategory;
}
public void setItemCategory(String itemCategory){
this.itemCategory = itemCategory;
}

public static final String ITEM_SUB_CATEGORY = "itemSubCategory";
@Column(name="ITEMSUBCATEGORY", nullable=true, length=1000)
private String itemSubCategory;
public String getItemSubCategory(){
return this.itemSubCategory;
}
public void setItemSubCategory(String itemSubCategory){
this.itemSubCategory = itemSubCategory;
}

public static final String REF_CASE = "refCase";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCASEID", nullable=true)
private MPAY_AmlCase refCase;
public MPAY_AmlCase getRefCase(){
return this.refCase;
}
public void setRefCase(MPAY_AmlCase refCase){
this.refCase = refCase;
}

@Override
public String toString() {
return "MPAY_AmlHit [id= " + getId() + ", entityReference= " + getEntityReference() + ", score= " + getScore() + ", englishName= " + getEnglishName() + ", arabicName= " + getArabicName() + ", englishAddress= " + getEnglishAddress() + ", documentSerial= " + getDocumentSerial() + ", dateOfBirth= " + getDateOfBirth() + ", typeOfAlias= " + getTypeOfAlias() + ", isMatchFlag= " + getIsMatchFlag() + ", itemSource= " + getItemSource() + ", itemCategory= " + getItemCategory() + ", itemSubCategory= " + getItemSubCategory() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getEntityReference() == null) ? 0 : getEntityReference().hashCode());
result = prime * result + ((getScore() == null) ? 0 : getScore().hashCode());
result = prime * result + ((getEnglishName() == null) ? 0 : getEnglishName().hashCode());
result = prime * result + ((getArabicName() == null) ? 0 : getArabicName().hashCode());
result = prime * result + ((getEnglishAddress() == null) ? 0 : getEnglishAddress().hashCode());
result = prime * result + ((getDocumentSerial() == null) ? 0 : getDocumentSerial().hashCode());
result = prime * result + ((getDateOfBirth() == null) ? 0 : getDateOfBirth().hashCode());
result = prime * result + ((getTypeOfAlias() == null) ? 0 : getTypeOfAlias().hashCode());
result = prime * result + ((getIsMatchFlag() == null) ? 0 : getIsMatchFlag().hashCode());
result = prime * result + ((getItemSource() == null) ? 0 : getItemSource().hashCode());
result = prime * result + ((getItemCategory() == null) ? 0 : getItemCategory().hashCode());
result = prime * result + ((getItemSubCategory() == null) ? 0 : getItemSubCategory().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_AmlHit))
return false;
else {MPAY_AmlHit other = (MPAY_AmlHit) obj;
return this.hashCode() == other.hashCode();}
}


}