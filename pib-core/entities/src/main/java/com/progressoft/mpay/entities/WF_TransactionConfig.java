package com.progressoft.mpay.entities;

public class WF_TransactionConfig
 {
    // Steps
    public static final String STEP_Initialization = "105601";
    public static final String STEP_ApprovalNewStep = "105602";
    public static final String STEP_RepairStep = "105603";
    public static final String STEP_ApprovalRepairStep = "105604";
    public static final String STEP_ActiveStep = "105605";
    public static final String STEP_EditStep = "105606";
    public static final String STEP_ApprovalEditStep = "105607";
    public static final String STEP_ApprovalDeleteStep = "105608";
    public static final String STEP_DeletedStep = "105609";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_ApprovalNewStep = "Approval New";
    public static final String STEP_STATUS_RepairStep = "Repair";
    public static final String STEP_STATUS_ApprovalRepairStep = "Approval Repair";
    public static final String STEP_STATUS_ActiveStep = "Active";
    public static final String STEP_STATUS_EditStep = "Edit";
    public static final String STEP_STATUS_ApprovalEditStep = "Approval Edit";
    public static final String STEP_STATUS_ApprovalDeleteStep = "Approval Delete";
    public static final String STEP_STATUS_DeletedStep = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveApprovalNewStep = "3";
    public static final String ACTION_CODE_RejectApprovalNewStep = "4";
    public static final String ACTION_CODE_SubmitRepairStep = "5";
    public static final String ACTION_CODE_DeleteRepairStep = "6";
    public static final String ACTION_CODE_ApproveApprovalRepairStep = "7";
    public static final String ACTION_CODE_RejectApprovalRepairStep = "8";
    public static final String ACTION_CODE_EditActiveStep = "9";
    public static final String ACTION_CODE_DeleteActiveStep = "12";
    public static final String ACTION_CODE_SubmitEditStep = "10";
    public static final String ACTION_CODE_CancelEditStep = "11";
    public static final String ACTION_CODE_ApproveApprovalEditStep = "13";
    public static final String ACTION_CODE_RejectApprovalEditStep = "14";
    public static final String ACTION_CODE_ApproveApprovalDeleteStep = "15";
    public static final String ACTION_CODE_RejectApprovalDeleteStep = "16";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveApprovalNewStep = "1019313797";
    public static final String ACTION_KEY_RejectApprovalNewStep = "1297029890";
    public static final String ACTION_KEY_SubmitRepairStep = "1267224635";
    public static final String ACTION_KEY_DeleteRepairStep = "1391666984";
    public static final String ACTION_KEY_ApproveApprovalRepairStep = "755596626";
    public static final String ACTION_KEY_RejectApprovalRepairStep = "1471079631";
    public static final String ACTION_KEY_EditActiveStep = "174478780";
    public static final String ACTION_KEY_DeleteActiveStep = "753551186";
    public static final String ACTION_KEY_SubmitEditStep = "108419088";
    public static final String ACTION_KEY_CancelEditStep = "464961325";
    public static final String ACTION_KEY_ApproveApprovalEditStep = "1232439632";
    public static final String ACTION_KEY_RejectApprovalEditStep = "1879218844";
    public static final String ACTION_KEY_ApproveApprovalDeleteStep = "37996419";
    public static final String ACTION_KEY_RejectApprovalDeleteStep = "199410220";

}