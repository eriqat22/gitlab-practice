package com.progressoft.mpay.entities;

public class WF_CustomerDevice
 {
    // Steps
    public static final String STEP_Initialization = "1100101";
    public static final String STEP_CreationApproval = "1100102";
    public static final String STEP_RepairNew = "1100103";
    public static final String STEP_EditNew = "1100104";
    public static final String STEP_ViewApproved = "1100105";
    public static final String STEP_ModificationApproval = "1100107";
    public static final String STEP_RepairRejected = "1100108";
    public static final String STEP_EditRejected = "1100109";
    public static final String STEP_DeletionApproval = "1100110";
    public static final String STEP_End = "1100111";
    public static final String STEP_Stolen = "1100112";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_CreationApproval = "New Authorization";
    public static final String STEP_STATUS_RepairNew = "Repair New";
    public static final String STEP_STATUS_EditNew = "Edit New";
    public static final String STEP_STATUS_ViewApproved = "Approved";
    public static final String STEP_STATUS_ModificationApproval = "Authorization";
    public static final String STEP_STATUS_RepairRejected = "Repair Rejected";
    public static final String STEP_STATUS_EditRejected = "Edit Rejected";
    public static final String STEP_STATUS_DeletionApproval = "Deletion Authorization";
    public static final String STEP_STATUS_End = "Deleted";
    public static final String STEP_STATUS_Stolen = "Stolen";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_SVC_Approve = "SVC_Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Cancel = "Cancel";
    public static final String ACTION_NAME_SVC_Delete = "SVC_Delete";
    public static final String ACTION_NAME_Unblock = "Unblock";
    public static final String ACTION_NAME_Stolen = "Stolen";
    public static final String ACTION_NAME_Reset = "Reset";
    public static final String ACTION_NAME_Reactivate = "Reactivate";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveCreationApproval = "3";
    public static final String ACTION_CODE_SVC_ApproveCreationApproval = "4";
    public static final String ACTION_CODE_RejectCreationApproval = "5";
    public static final String ACTION_CODE_EditRepairNew = "6";
    public static final String ACTION_CODE_DeleteRepairNew = "7";
    public static final String ACTION_CODE_SaveEditNew = "8";
    public static final String ACTION_CODE_CancelEditNew = "9";
    public static final String ACTION_CODE_DeleteViewApproved = "11";
    public static final String ACTION_CODE_SVC_DeleteViewApproved = "12";
    public static final String ACTION_CODE_UnblockViewApproved = "23";
    public static final String ACTION_CODE_StolenViewApproved = "24";
    public static final String ACTION_CODE_ApproveModificationApproval = "15";
    public static final String ACTION_CODE_RejectModificationApproval = "16";
    public static final String ACTION_CODE_EditRepairRejected = "17";
    public static final String ACTION_CODE_ResetRepairRejected = "18";
    public static final String ACTION_CODE_SaveEditRejected = "19";
    public static final String ACTION_CODE_CancelEditRejected = "20";
    public static final String ACTION_CODE_ApproveDeletionApproval = "21";
    public static final String ACTION_CODE_RejectDeletionApproval = "22";
    public static final String ACTION_CODE_ReactivateStolen = "25";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveCreationApproval = "580996121";
    public static final String ACTION_KEY_SVC_ApproveCreationApproval = "399548250";
    public static final String ACTION_KEY_RejectCreationApproval = "1504948072";
    public static final String ACTION_KEY_EditRepairNew = "1457269025";
    public static final String ACTION_KEY_DeleteRepairNew = "994109167";
    public static final String ACTION_KEY_SaveEditNew = "1315589842";
    public static final String ACTION_KEY_CancelEditNew = "1303074521";
    public static final String ACTION_KEY_DeleteViewApproved = "936666016";
    public static final String ACTION_KEY_SVC_DeleteViewApproved = "1581197201";
    public static final String ACTION_KEY_UnblockViewApproved = "1815432955";
    public static final String ACTION_KEY_StolenViewApproved = "1756905082";
    public static final String ACTION_KEY_ApproveModificationApproval = "443870664";
    public static final String ACTION_KEY_RejectModificationApproval = "2065036708";
    public static final String ACTION_KEY_EditRepairRejected = "593992781";
    public static final String ACTION_KEY_ResetRepairRejected = "1710700500";
    public static final String ACTION_KEY_SaveEditRejected = "856191863";
    public static final String ACTION_KEY_CancelEditRejected = "983639082";
    public static final String ACTION_KEY_ApproveDeletionApproval = "738279361";
    public static final String ACTION_KEY_RejectDeletionApproval = "1932985220";
    public static final String ACTION_KEY_ReactivateStolen = "648554575";

}