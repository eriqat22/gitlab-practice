package com.progressoft.mpay.entities;

public class WF_ChargesSchemeDetails
 {
    // Steps
    public static final String STEP_Initialization = "101901";
    public static final String STEP_View = "101902";
    public static final String STEP_Deleted = "101903";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_View = "View";
    public static final String STEP_STATUS_Deleted = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_SVC_Delete = "SVC_Delete";
    public static final String ACTION_NAME_SVC_Approve = "SVC_Approve";
    public static final String ACTION_NAME_SVC_Cancel = "SVC_Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SaveView = "3";
    public static final String ACTION_CODE_SVC_DeleteView = "4";
    public static final String ACTION_CODE_SVC_ApproveView = "5";
    public static final String ACTION_CODE_SVC_CancelView = "6";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SaveView = "1955004895";
    public static final String ACTION_KEY_SVC_DeleteView = "800892546";
    public static final String ACTION_KEY_SVC_ApproveView = "759503388";
    public static final String ACTION_KEY_SVC_CancelView = "2089520891";

}