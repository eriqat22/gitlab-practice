package com.progressoft.mpay.entities;

public class WF_ServiceCashOut
 {
    // Steps
    public static final String STEP_Initialization = "101601";
    public static final String STEP_CalcuationStep = "101606";
    public static final String STEP_Processing = "101602";
    public static final String STEP_Accepted = "101603";
    public static final String STEP_Rejected = "101604";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_CalcuationStep = "Calculate Charges";
    public static final String STEP_STATUS_Processing = "Processing";
    public static final String STEP_STATUS_Accepted = "Accepted";
    public static final String STEP_STATUS_Rejected = "Rejected";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_SaveandSubmit = "Save and Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Recalculate = "Recalculate";
    public static final String ACTION_NAME_SVC_Accept = "SVC_Accept";
    public static final String ACTION_NAME_SVC_Reject = "SVC_Reject";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SaveandSubmitCalcuationStep = "5";
    public static final String ACTION_CODE_DeleteCalcuationStep = "6";
    public static final String ACTION_CODE_RecalculateCalcuationStep = "8";
    public static final String ACTION_CODE_SVC_AcceptProcessing = "3";
    public static final String ACTION_CODE_SVC_RejectProcessing = "4";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SaveandSubmitCalcuationStep = "111043211";
    public static final String ACTION_KEY_DeleteCalcuationStep = "1102500100";
    public static final String ACTION_KEY_RecalculateCalcuationStep = "398506446";
    public static final String ACTION_KEY_SVC_AcceptProcessing = "1332206970";
    public static final String ACTION_KEY_SVC_RejectProcessing = "1144612560";

}