package com.progressoft.mpay.entities;

public class WF_MpClear
 {
    // Steps
    public static final String STEP_Initialization = "108601";
    public static final String STEP_ApprovalNewStep = "108602";
    public static final String STEP_RepairStep = "108603";
    public static final String STEP_ApprovalRepairStep = "108604";
    public static final String STEP_ActiveStep = "108605";
    public static final String STEP_EditStep = "108606";
    public static final String STEP_ApprovalEditStep = "108607";
    public static final String STEP_ApprovalDeleteStep = "108608";
    public static final String STEP_DeletedStep = "108609";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_ApprovalNewStep = "Approval New";
    public static final String STEP_STATUS_RepairStep = "Repair";
    public static final String STEP_STATUS_ApprovalRepairStep = "Approval Repair";
    public static final String STEP_STATUS_ActiveStep = "Active";
    public static final String STEP_STATUS_EditStep = "Edit";
    public static final String STEP_STATUS_ApprovalEditStep = "Approval Edit";
    public static final String STEP_STATUS_ApprovalDeleteStep = "Approval Delete";
    public static final String STEP_STATUS_DeletedStep = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveApprovalNewStep = "3";
    public static final String ACTION_CODE_RejectApprovalNewStep = "4";
    public static final String ACTION_CODE_SubmitRepairStep = "5";
    public static final String ACTION_CODE_DeleteRepairStep = "6";
    public static final String ACTION_CODE_ApproveApprovalRepairStep = "7";
    public static final String ACTION_CODE_RejectApprovalRepairStep = "8";
    public static final String ACTION_CODE_EditActiveStep = "9";
    public static final String ACTION_CODE_SubmitEditStep = "10";
    public static final String ACTION_CODE_CancelEditStep = "11";
    public static final String ACTION_CODE_DeleteEditStep = "12";
    public static final String ACTION_CODE_ApproveApprovalEditStep = "13";
    public static final String ACTION_CODE_RejectApprovalEditStep = "14";
    public static final String ACTION_CODE_ApproveApprovalDeleteStep = "15";
    public static final String ACTION_CODE_RejectApprovalDeleteStep = "16";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveApprovalNewStep = "1498932111";
    public static final String ACTION_KEY_RejectApprovalNewStep = "1819638139";
    public static final String ACTION_KEY_SubmitRepairStep = "669186161";
    public static final String ACTION_KEY_DeleteRepairStep = "1422193847";
    public static final String ACTION_KEY_ApproveApprovalRepairStep = "1051212181";
    public static final String ACTION_KEY_RejectApprovalRepairStep = "1848347078";
    public static final String ACTION_KEY_EditActiveStep = "1222770123";
    public static final String ACTION_KEY_SubmitEditStep = "1090202342";
    public static final String ACTION_KEY_CancelEditStep = "255913482";
    public static final String ACTION_KEY_DeleteEditStep = "1961974310";
    public static final String ACTION_KEY_ApproveApprovalEditStep = "1605682457";
    public static final String ACTION_KEY_RejectApprovalEditStep = "1381949503";
    public static final String ACTION_KEY_ApproveApprovalDeleteStep = "1964224513";
    public static final String ACTION_KEY_RejectApprovalDeleteStep = "2016142366";

}