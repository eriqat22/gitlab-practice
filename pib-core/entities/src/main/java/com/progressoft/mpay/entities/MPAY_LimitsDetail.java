package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_LimitsDetails",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"REFLIMITID","MSGTYPEID","Z_TENANT_ID"})
})
@XmlRootElement(name="LimitsDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_LimitsDetail extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_LimitsDetail(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String TX_COUNT_LIMIT = "txCountLimit";
@Column(name="TXCOUNTLIMIT", nullable=false, length=20)
private Long txCountLimit;
public Long getTxCountLimit(){
return this.txCountLimit;
}
public void setTxCountLimit(Long txCountLimit){
this.txCountLimit = txCountLimit;
}

public static final String TX_AMOUNT_LIMIT = "txAmountLimit";
@Column(name="TXAMOUNTLIMIT", nullable=false, precision=21, scale=8, length=20)
private java.math.BigDecimal txAmountLimit;
public java.math.BigDecimal getTxAmountLimit(){
return this.txAmountLimit;
}
public void setTxAmountLimit(java.math.BigDecimal txAmountLimit){
this.txAmountLimit = txAmountLimit;
}

public static final String CHECK_TOTAL_AMOUNT = "checkTotalAmount";
@Column(name="CHECKTOTALAMOUNT", nullable=false, length=1)
private Boolean checkTotalAmount;
public Boolean getCheckTotalAmount(){
return this.checkTotalAmount;
}
public void setCheckTotalAmount(Boolean checkTotalAmount){
this.checkTotalAmount = checkTotalAmount;
}

public static final String REF_LIMIT = "refLimit";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFLIMITID", nullable=true)
private MPAY_Limit refLimit;
public MPAY_Limit getRefLimit(){
return this.refLimit;
}
public void setRefLimit(MPAY_Limit refLimit){
this.refLimit = refLimit;
}

public static final String MSG_TYPE = "msgType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="MSGTYPEID", nullable=false)
private MPAY_MessageType msgType;
public MPAY_MessageType getMsgType(){
return this.msgType;
}
public void setMsgType(MPAY_MessageType msgType){
this.msgType = msgType;
}

@Override
public String toString() {
return "MPAY_LimitsDetail [id= " + getId() + ", isActive= " + getIsActive() + ", txCountLimit= " + getTxCountLimit() + ", txAmountLimit= " + getTxAmountLimit() + ", checkTotalAmount= " + getCheckTotalAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
int TxCountLimit= new Long("null".equals(getTxCountLimit() + "") ? 0 : getTxCountLimit()).intValue();
result = prime * result + (int) (TxCountLimit ^ TxCountLimit >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_LimitsDetail))
return false;
else {MPAY_LimitsDetail other = (MPAY_LimitsDetail) obj;
return this.hashCode() == other.hashCode();}
}


}