package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_Limits")
@XmlRootElement(name="Limits")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Limit extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Limit(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String TOTAL_AMOUNT = "totalAmount";
@Column(name="TOTALAMOUNT", nullable=false, length=20)
private java.math.BigDecimal totalAmount;
public java.math.BigDecimal getTotalAmount(){
return this.totalAmount;
}
public void setTotalAmount(java.math.BigDecimal totalAmount){
this.totalAmount = totalAmount;
}

public static final String TOTAL_COUNT = "totalCount";
@Column(name="TOTALCOUNT", nullable=false, length=20)
private Long totalCount;
public Long getTotalCount(){
return this.totalCount;
}
public void setTotalCount(Long totalCount){
this.totalCount = totalCount;
}

public static final String CREDITOR_TOTAL_AMOUNT = "creditorTotalAmount";
@Column(name="CREDITORTOTALAMOUNT", nullable=false, length=20)
private java.math.BigDecimal creditorTotalAmount;
public java.math.BigDecimal getCreditorTotalAmount(){
return this.creditorTotalAmount;
}
public void setCreditorTotalAmount(java.math.BigDecimal creditorTotalAmount){
this.creditorTotalAmount = creditorTotalAmount;
}

public static final String REF_TYPE = "refType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFTYPEID", nullable=false)
private MPAY_LimitsType refType;
public MPAY_LimitsType getRefType(){
return this.refType;
}
public void setRefType(MPAY_LimitsType refType){
this.refType = refType;
}

public static final String REF_SCHEME = "refScheme";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFSCHEMEID", nullable=true)
private MPAY_LimitsScheme refScheme;
public MPAY_LimitsScheme getRefScheme(){
return this.refScheme;
}
public void setRefScheme(MPAY_LimitsScheme refScheme){
this.refScheme = refScheme;
}

public static final String REF_LIMIT_LIMITS_DETAILS = "refLimitLimitsDetails";
@OneToMany(mappedBy = "refLimit")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_LimitsDetail> refLimitLimitsDetails;
public List<MPAY_LimitsDetail> getRefLimitLimitsDetails(){
return this.refLimitLimitsDetails;
}
public void setRefLimitLimitsDetails(List<MPAY_LimitsDetail> refLimitLimitsDetails){
this.refLimitLimitsDetails = refLimitLimitsDetails;
}

@Override
public String toString() {
return "MPAY_Limit [id= " + getId() + ", description= " + getDescription() + ", isActive= " + getIsActive() + ", totalAmount= " + getTotalAmount() + ", totalCount= " + getTotalCount() + ", creditorTotalAmount= " + getCreditorTotalAmount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getTotalAmount() == null) ? 0 : getTotalAmount().hashCode());
result = prime * result + ((getTotalCount() == null) ? 0 : getTotalCount().hashCode());
result = prime * result + ((getCreditorTotalAmount() == null) ? 0 : getCreditorTotalAmount().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Limit))
return false;
else {MPAY_Limit other = (MPAY_Limit) obj;
return this.hashCode() == other.hashCode();}
}


}