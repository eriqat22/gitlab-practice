package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_MpClearResponseCodes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="MpClearResponseCodes")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_MpClearResponseCode extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_MpClearResponseCode(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String USAGE = "usage";
@Column(name="USAGE", nullable=true, length=255)
private String usage;
public String getUsage(){
return this.usage;
}
public void setUsage(String usage){
this.usage = usage;
}

public static final String MP_CLEAR_RESPONSE_CODES_N_L_S = "mpClearResponseCodesNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "intgId")
@MapKey(name="languageCode")
private Map<String, MPAY_MpClearResponseCodes_NLS> mpClearResponseCodesNLS;
public Map<String, MPAY_MpClearResponseCodes_NLS> getMpClearResponseCodesNLS(){
return this.mpClearResponseCodesNLS;
}
public void setMpClearResponseCodesNLS(Map<String, MPAY_MpClearResponseCodes_NLS> mpClearResponseCodesNLS){
this.mpClearResponseCodesNLS = mpClearResponseCodesNLS;
}

@Override
public String toString() {
return "MPAY_MpClearResponseCode [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", usage= " + getUsage() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getUsage() == null) ? 0 : getUsage().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_MpClearResponseCode))
return false;
else {MPAY_MpClearResponseCode other = (MPAY_MpClearResponseCode) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.mpClearResponseCodesNLS;
}
}