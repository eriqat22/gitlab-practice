package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_Reasons",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="Reasons")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_Reason extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Reason(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String MP_CLEAR_CODE = "mpClearCode";
@Column(name="MPCLEARCODE", nullable=true, length=12)
private String mpClearCode;
public String getMpClearCode(){
return this.mpClearCode;
}
public void setMpClearCode(String mpClearCode){
this.mpClearCode = mpClearCode;
}

public static final String SMS_ENABLED = "smsEnabled";
@Column(name="SMSENABLED", nullable=true, length=1)
private Boolean smsEnabled;
public Boolean getSmsEnabled(){
return this.smsEnabled;
}
public void setSmsEnabled(Boolean smsEnabled){
this.smsEnabled = smsEnabled;
}

public static final String REASONS_N_L_S = "reasonsNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "rsnId")
@MapKey(name="languageCode")
private Map<String, MPAY_Reasons_NLS> reasonsNLS;
public Map<String, MPAY_Reasons_NLS> getReasonsNLS(){
return this.reasonsNLS;
}
public void setReasonsNLS(Map<String, MPAY_Reasons_NLS> reasonsNLS){
this.reasonsNLS = reasonsNLS;
}

@Override
public String toString() {
return "MPAY_Reason [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", mpClearCode= " + getMpClearCode() + ", smsEnabled= " + getSmsEnabled() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getMpClearCode() == null) ? 0 : getMpClearCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Reason))
return false;
else {MPAY_Reason other = (MPAY_Reason) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.reasonsNLS;
}
}