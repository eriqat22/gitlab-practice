package com.progressoft.mpay.entities;

public class WF_Transactions
 {
    // Steps
    public static final String STEP_Initialization = "102401";
    public static final String STEP_View = "102405";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_View = "View";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Reverse = "Reverse";
    public static final String ACTION_NAME_Accept = "Accept";
    public static final String ACTION_NAME_Reject = "Reject";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ReverseView = "3";
    public static final String ACTION_CODE_AcceptView = "4";
    public static final String ACTION_CODE_RejectView = "5";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ReverseView = "952775034";
    public static final String ACTION_KEY_AcceptView = "676749125";
    public static final String ACTION_KEY_RejectView = "1928509733";

}