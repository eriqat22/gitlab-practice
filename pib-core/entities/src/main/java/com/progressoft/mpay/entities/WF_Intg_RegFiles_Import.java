package com.progressoft.mpay.entities;

public class WF_Intg_RegFiles_Import
 {
    // Steps
    public static final String STEP_Initialization = "106891";
    public static final String STEP_NewStep = "106892";
    public static final String STEP_MQ_NotificationStep = "106893";
    public static final String STEP_ProcessingStep = "106894";
    public static final String STEP_Processed = "106895";
    // Statuses
    public static final String STEP_STATUS_Initialization = "Initialziation";
    public static final String STEP_STATUS_NewStep = "New";
    public static final String STEP_STATUS_MQ_NotificationStep = "Notification";
    public static final String STEP_STATUS_ProcessingStep = "Processing";
    public static final String STEP_STATUS_Processed = "Finished";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_SVC_Notify = "SVC_Notify";
    public static final String ACTION_NAME_SVC_Processed = "SVC_Processed";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SubmitNewStep = "3";
    public static final String ACTION_CODE_SVC_NotifyMQ_NotificationStep = "4";
    public static final String ACTION_CODE_SVC_ProcessedProcessingStep = "5";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SubmitNewStep = "320916320";
    public static final String ACTION_KEY_SVC_NotifyMQ_NotificationStep = "208265655";
    public static final String ACTION_KEY_SVC_ProcessedProcessingStep = "744313403";

}