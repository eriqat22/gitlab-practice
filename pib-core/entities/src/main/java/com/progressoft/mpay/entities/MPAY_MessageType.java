package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_MessageTypes")
@XmlRootElement(name="MessageTypes")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_MessageType extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_MessageType(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String IS_SYSTEM = "isSystem";
@Column(name="ISSYSTEM", nullable=true, length=1)
private Boolean isSystem;
public Boolean getIsSystem(){
return this.isSystem;
}
public void setIsSystem(Boolean isSystem){
this.isSystem = isSystem;
}

public static final String IS_FINANCIAL = "isFinancial";
@Column(name="ISFINANCIAL", nullable=true, length=1)
private Boolean isFinancial;
public Boolean getIsFinancial(){
return this.isFinancial;
}
public void setIsFinancial(Boolean isFinancial){
this.isFinancial = isFinancial;
}

public static final String MP_CLEAR_CODE = "mpClearCode";
@Column(name="MPCLEARCODE", nullable=true, length=100)
private String mpClearCode;
public String getMpClearCode(){
return this.mpClearCode;
}
public void setMpClearCode(String mpClearCode){
this.mpClearCode = mpClearCode;
}

public static final String CATEGORY_CODE = "categoryCode";
@Column(name="CATEGORYCODE", nullable=true, length=10)
private String categoryCode;
public String getCategoryCode(){
return this.categoryCode;
}
public void setCategoryCode(String categoryCode){
this.categoryCode = categoryCode;
}

public static final String IS_O_T_P_ENABLED = "isOTPEnabled";
@Column(name="ISOTPENABLED", nullable=false, length=1)
private Boolean isOTPEnabled;
public Boolean getIsOTPEnabled(){
return this.isOTPEnabled;
}
public void setIsOTPEnabled(Boolean isOTPEnabled){
this.isOTPEnabled = isOTPEnabled;
}

public static final String IS_PINLESS_ENABLED = "isPinlessEnabled";
@Column(name="ISPINLESSENABLED", nullable=false, length=1)
private Boolean isPinlessEnabled;
public Boolean getIsPinlessEnabled(){
return this.isPinlessEnabled;
}
public void setIsPinlessEnabled(Boolean isPinlessEnabled){
this.isPinlessEnabled = isPinlessEnabled;
}

public static final String MESSAGE_TYPE_CLIENTS_COMMISSIONS = "messageTypeClientsCommissions";
@OneToMany(mappedBy = "messageType")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ClientsCommission> messageTypeClientsCommissions;
public List<MPAY_ClientsCommission> getMessageTypeClientsCommissions(){
return this.messageTypeClientsCommissions;
}
public void setMessageTypeClientsCommissions(List<MPAY_ClientsCommission> messageTypeClientsCommissions){
this.messageTypeClientsCommissions = messageTypeClientsCommissions;
}

public static final String MESSAGE_TYPE_CLIENTS_LIMITS = "messageTypeClientsLimits";
@OneToMany(mappedBy = "messageType")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ClientsLimit> messageTypeClientsLimits;
public List<MPAY_ClientsLimit> getMessageTypeClientsLimits(){
return this.messageTypeClientsLimits;
}
public void setMessageTypeClientsLimits(List<MPAY_ClientsLimit> messageTypeClientsLimits){
this.messageTypeClientsLimits = messageTypeClientsLimits;
}

public static final String MESSAGE_TYPE_CLIENTS_O_T_PS = "messageTypeClientsOTPs";
@OneToMany(mappedBy = "messageType")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ClientsOTP> messageTypeClientsOTPs;
public List<MPAY_ClientsOTP> getMessageTypeClientsOTPs(){
return this.messageTypeClientsOTPs;
}
public void setMessageTypeClientsOTPs(List<MPAY_ClientsOTP> messageTypeClientsOTPs){
this.messageTypeClientsOTPs = messageTypeClientsOTPs;
}

public static final String MESSAGE_TYPES_N_L_S = "messageTypesNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "msgId")
@MapKey(name="languageCode")
private Map<String, MPAY_MessageTypes_NLS> messageTypesNLS;
public Map<String, MPAY_MessageTypes_NLS> getMessageTypesNLS(){
return this.messageTypesNLS;
}
public void setMessageTypesNLS(Map<String, MPAY_MessageTypes_NLS> messageTypesNLS){
this.messageTypesNLS = messageTypesNLS;
}

@Override
public String toString() {
return "MPAY_MessageType [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", isSystem= " + getIsSystem() + ", isFinancial= " + getIsFinancial() + ", mpClearCode= " + getMpClearCode() + ", categoryCode= " + getCategoryCode() + ", isOTPEnabled= " + getIsOTPEnabled() + ", isPinlessEnabled= " + getIsPinlessEnabled() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getMpClearCode() == null) ? 0 : getMpClearCode().hashCode());
result = prime * result + ((getCategoryCode() == null) ? 0 : getCategoryCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_MessageType))
return false;
else {MPAY_MessageType other = (MPAY_MessageType) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.messageTypesNLS;
}
}