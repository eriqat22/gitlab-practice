package com.progressoft.mpay.entities;

import com.progressoft.jfw.model.bussinessobject.core.JFWLookableEntity;
import java.util.Map;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_Profiles",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"})
})
@XmlRootElement(name="Profiles")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_Profile extends JFWLookableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Profile(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CHARGES_SCHEME = "chargesScheme";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CHARGESSCHEMEID", nullable=false)
private MPAY_ChargesScheme chargesScheme;
public MPAY_ChargesScheme getChargesScheme(){
return this.chargesScheme;
}
public void setChargesScheme(MPAY_ChargesScheme chargesScheme){
this.chargesScheme = chargesScheme;
}

public static final String LIMITS_SCHEME = "limitsScheme";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="LIMITSSCHEMEID", nullable=false)
private MPAY_LimitsScheme limitsScheme;
public MPAY_LimitsScheme getLimitsScheme(){
return this.limitsScheme;
}
public void setLimitsScheme(MPAY_LimitsScheme limitsScheme){
this.limitsScheme = limitsScheme;
}

public static final String REQUEST_TYPES_SCHEME = "requestTypesScheme";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REQUESTTYPESSCHEMEID", nullable=false)
private MPAY_RequestTypesScheme requestTypesScheme;
public MPAY_RequestTypesScheme getRequestTypesScheme(){
return this.requestTypesScheme;
}
public void setRequestTypesScheme(MPAY_RequestTypesScheme requestTypesScheme){
this.requestTypesScheme = requestTypesScheme;
}

public static final String COMMISSIONS_SCHEME = "commissionsScheme";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="COMMISSIONSSCHEMEID", nullable=false)
private MPAY_CommissionScheme commissionsScheme;
public MPAY_CommissionScheme getCommissionsScheme(){
return this.commissionsScheme;
}
public void setCommissionsScheme(MPAY_CommissionScheme commissionsScheme){
this.commissionsScheme = commissionsScheme;
}

public static final String TAX_SCHEME = "taxScheme";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="TAXSCHEMEID", nullable=false)
private MPAY_TaxScheme taxScheme;
public MPAY_TaxScheme getTaxScheme(){
return this.taxScheme;
}
public void setTaxScheme(MPAY_TaxScheme taxScheme){
this.taxScheme = taxScheme;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=false)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

public static final String REF_PROFILE_TRANSACTION_CONFIGS = "refProfileTransactionConfigs";
@OneToMany(mappedBy = "refProfile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_TransactionConfig> refProfileTransactionConfigs;
public List<MPAY_TransactionConfig> getRefProfileTransactionConfigs(){
return this.refProfileTransactionConfigs;
}
public void setRefProfileTransactionConfigs(List<MPAY_TransactionConfig> refProfileTransactionConfigs){
this.refProfileTransactionConfigs = refProfileTransactionConfigs;
}

@Override
public String toString() {
return "MPAY_Profile [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Profile))
return false;
else {MPAY_Profile other = (MPAY_Profile) obj;
return this.hashCode() == other.hashCode();}
}


}