package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_ChannelType",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="ChannelType")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_ChannelType extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ChannelType(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String TRANSPORTER_BEAN = "transporterBean";
@Column(name="TRANSPORTERBEAN", nullable=true, length=255)
private String transporterBean;
public String getTransporterBean(){
return this.transporterBean;
}
public void setTransporterBean(String transporterBean){
this.transporterBean = transporterBean;
}

public static final String CHANNEL_TYPE_N_L_S = "channelTypeNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "channelTypeId")
@MapKey(name="languageCode")
private Map<String, MPAY_ChannelType_NLS> channelTypeNLS;
public Map<String, MPAY_ChannelType_NLS> getChannelTypeNLS(){
return this.channelTypeNLS;
}
public void setChannelTypeNLS(Map<String, MPAY_ChannelType_NLS> channelTypeNLS){
this.channelTypeNLS = channelTypeNLS;
}

@Override
public String toString() {
return "MPAY_ChannelType [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", transporterBean= " + getTransporterBean() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getTransporterBean() == null) ? 0 : getTransporterBean().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ChannelType))
return false;
else {MPAY_ChannelType other = (MPAY_ChannelType) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.channelTypeNLS;
}
}