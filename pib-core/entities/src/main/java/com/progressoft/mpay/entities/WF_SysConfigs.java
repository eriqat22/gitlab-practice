package com.progressoft.mpay.entities;

public class WF_SysConfigs
 {
    // Steps
    public static final String STEP_Initialization = "102401";
    public static final String STEP_View = "102405";
    public static final String STEP_Repair = "102406";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_View = "View";
    public static final String STEP_STATUS_Repair = "Repair";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_EditView = "3";
    public static final String ACTION_CODE_SaveRepair = "4";
    public static final String ACTION_CODE_CancelRepair = "5";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_EditView = "11653483";
    public static final String ACTION_KEY_SaveRepair = "341321592";
    public static final String ACTION_KEY_CancelRepair = "335271821";

}