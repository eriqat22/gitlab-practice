package com.progressoft.mpay.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import com.progressoft.jfw.model.bussinessobject.comments.CommentItem;

@Entity
@Table(name="MPAY_CustomerCMT")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CustomerCmt extends CommentItem implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CustomerCmt(){/*Default Constructor*/}


}