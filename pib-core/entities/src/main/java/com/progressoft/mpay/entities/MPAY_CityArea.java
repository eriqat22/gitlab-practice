package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_CityAreas")
@XmlRootElement(name="CityAreas")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_CityArea extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CityArea(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CITY_ID = "cityId";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CITYID", nullable=false)
private MPAY_City cityId;
@flexjson.JSON(include = false)
public MPAY_City getCityId(){
return this.cityId;
}
public void setCityId(MPAY_City cityId){
this.cityId = cityId;
}

public static final String CITY_AREAS_N_L_S = "cityAreasNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "areaId")
@MapKey(name="languageCode")
private Map<String, MPAY_CityAreas_NLS> cityAreasNLS;
public Map<String, MPAY_CityAreas_NLS> getCityAreasNLS(){
return this.cityAreasNLS;
}
public void setCityAreasNLS(Map<String, MPAY_CityAreas_NLS> cityAreasNLS){
this.cityAreasNLS = cityAreasNLS;
}

@Override
public String toString() {
return "MPAY_CityArea [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CityArea))
return false;
else {MPAY_CityArea other = (MPAY_CityArea) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.cityAreasNLS;
}
}