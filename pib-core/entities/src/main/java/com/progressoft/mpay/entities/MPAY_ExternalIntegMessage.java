package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_ExternalIntegMessages")
@XmlRootElement(name="ExternalIntegMessages")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_ExternalIntegMessage extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ExternalIntegMessage(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REQUEST_I_D = "requestID";
@Column(name="REQUESTID", nullable=false, length=255)
private String requestID;
public String getRequestID(){
return this.requestID;
}
public void setRequestID(String requestID){
this.requestID = requestID;
}

public static final String REQUEST_CONTENT = "requestContent";
@Column(name="REQUESTCONTENT", nullable=false, length=8000)
@Lob
private String requestContent;
public String getRequestContent(){
return this.requestContent;
}
public void setRequestContent(String requestContent){
this.requestContent = requestContent;
}

public static final String REQUEST_DATE = "requestDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REQUESTDATE", nullable=false, length=32)
private java.sql.Timestamp requestDate;
public java.sql.Timestamp getRequestDate(){
return this.requestDate;
}
public void setRequestDate(java.sql.Timestamp requestDate){
this.requestDate = requestDate;
}

public static final String REQUEST_TOKEN = "requestToken";
@Column(name="REQUESTTOKEN", nullable=true, length=4000)
private String requestToken;
public String getRequestToken(){
return this.requestToken;
}
public void setRequestToken(String requestToken){
this.requestToken = requestToken;
}

public static final String RESPONSE_I_D = "responseID";
@Column(name="RESPONSEID", nullable=true, length=255)
private String responseID;
public String getResponseID(){
return this.responseID;
}
public void setResponseID(String responseID){
this.responseID = responseID;
}

public static final String RESPONSE_CONTENT = "responseContent";
@Column(name="RESPONSECONTENT", nullable=true, length=8000)
@Lob
private String responseContent;
public String getResponseContent(){
return this.responseContent;
}
public void setResponseContent(String responseContent){
this.responseContent = responseContent;
}

public static final String RESPONSE_DATE = "responseDate";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="RESPONSEDATE", nullable=true, length=32)
private java.sql.Timestamp responseDate;
public java.sql.Timestamp getResponseDate(){
return this.responseDate;
}
public void setResponseDate(java.sql.Timestamp responseDate){
this.responseDate = responseDate;
}

public static final String RESPONSE_TOKEN = "responseToken";
@Column(name="RESPONSETOKEN", nullable=true, length=4000)
private String responseToken;
public String getResponseToken(){
return this.responseToken;
}
public void setResponseToken(String responseToken){
this.responseToken = responseToken;
}

public static final String REASON_CODE = "reasonCode";
@Column(name="REASONCODE", nullable=true, length=19)
private String reasonCode;
public String getReasonCode(){
return this.reasonCode;
}
public void setReasonCode(String reasonCode){
this.reasonCode = reasonCode;
}

public static final String REASON_DESCRIPTION = "reasonDescription";
@Column(name="REASONDESCRIPTION", nullable=true, length=1000)
private String reasonDescription;
public String getReasonDescription(){
return this.reasonDescription;
}
public void setReasonDescription(String reasonDescription){
this.reasonDescription = reasonDescription;
}

public static final String REQ_FIELD1 = "reqField1";
@Column(name="REQFIELD1", nullable=true, length=1000)
private String reqField1;
public String getReqField1(){
return this.reqField1;
}
public void setReqField1(String reqField1){
this.reqField1 = reqField1;
}

public static final String REQ_FIELD2 = "reqField2";
@Column(name="REQFIELD2", nullable=true, length=1000)
private String reqField2;
public String getReqField2(){
return this.reqField2;
}
public void setReqField2(String reqField2){
this.reqField2 = reqField2;
}

public static final String REQ_FIELD3 = "reqField3";
@Column(name="REQFIELD3", nullable=true, length=1000)
private String reqField3;
public String getReqField3(){
return this.reqField3;
}
public void setReqField3(String reqField3){
this.reqField3 = reqField3;
}

public static final String REQ_FIELD4 = "reqField4";
@Column(name="REQFIELD4", nullable=true, length=1000)
private String reqField4;
public String getReqField4(){
return this.reqField4;
}
public void setReqField4(String reqField4){
this.reqField4 = reqField4;
}

public static final String REQ_FIELD5 = "reqField5";
@Column(name="REQFIELD5", nullable=true, length=1000)
private String reqField5;
public String getReqField5(){
return this.reqField5;
}
public void setReqField5(String reqField5){
this.reqField5 = reqField5;
}

public static final String RSP_FIELD1 = "rspField1";
@Column(name="RSPFIELD1", nullable=true, length=1000)
private String rspField1;
public String getRspField1(){
return this.rspField1;
}
public void setRspField1(String rspField1){
this.rspField1 = rspField1;
}

public static final String RSP_FIELD2 = "rspField2";
@Column(name="RSPFIELD2", nullable=true, length=1000)
private String rspField2;
public String getRspField2(){
return this.rspField2;
}
public void setRspField2(String rspField2){
this.rspField2 = rspField2;
}

public static final String RSP_FIELD3 = "rspField3";
@Column(name="RSPFIELD3", nullable=true, length=1000)
private String rspField3;
public String getRspField3(){
return this.rspField3;
}
public void setRspField3(String rspField3){
this.rspField3 = rspField3;
}

public static final String RSP_FIELD4 = "rspField4";
@Column(name="RSPFIELD4", nullable=true, length=1000)
private String rspField4;
public String getRspField4(){
return this.rspField4;
}
public void setRspField4(String rspField4){
this.rspField4 = rspField4;
}

public static final String RSP_FIELD5 = "rspField5";
@Column(name="RSPFIELD5", nullable=true, length=1000)
private String rspField5;
public String getRspField5(){
return this.rspField5;
}
public void setRspField5(String rspField5){
this.rspField5 = rspField5;
}

public static final String REF_STATUS = "refStatus";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFSTATUSID", nullable=true)
private MPAY_ProcessingStatus refStatus;
public MPAY_ProcessingStatus getRefStatus(){
return this.refStatus;
}
public void setRefStatus(MPAY_ProcessingStatus refStatus){
this.refStatus = refStatus;
}

public static final String REF_SERVICE = "refService";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFSERVICEID", nullable=true)
private MPAY_CorpoarteService refService;
public MPAY_CorpoarteService getRefService(){
return this.refService;
}
public void setRefService(MPAY_CorpoarteService refService){
this.refService = refService;
}

public static final String REF_MESSAGE = "refMessage";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMESSAGEID", nullable=true)
private MPAY_MPayMessage refMessage;
public MPAY_MPayMessage getRefMessage(){
return this.refMessage;
}
public void setRefMessage(MPAY_MPayMessage refMessage){
this.refMessage = refMessage;
}

@Override
public String toString() {
return "MPAY_ExternalIntegMessage [id= " + getId() + ", requestID= " + getRequestID() + ", requestContent= " + getRequestContent() + ", requestDate= " + getRequestDate() + ", requestToken= " + getRequestToken() + ", responseID= " + getResponseID() + ", responseContent= " + getResponseContent() + ", responseDate= " + getResponseDate() + ", responseToken= " + getResponseToken() + ", reasonCode= " + getReasonCode() + ", reasonDescription= " + getReasonDescription() + ", reqField1= " + getReqField1() + ", reqField2= " + getReqField2() + ", reqField3= " + getReqField3() + ", reqField4= " + getReqField4() + ", reqField5= " + getReqField5() + ", rspField1= " + getRspField1() + ", rspField2= " + getRspField2() + ", rspField3= " + getRspField3() + ", rspField4= " + getRspField4() + ", rspField5= " + getRspField5() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRequestID() == null) ? 0 : getRequestID().hashCode());
result = prime * result + ((getRequestToken() == null) ? 0 : getRequestToken().hashCode());
result = prime * result + ((getResponseID() == null) ? 0 : getResponseID().hashCode());
result = prime * result + ((getResponseToken() == null) ? 0 : getResponseToken().hashCode());
result = prime * result + ((getReasonCode() == null) ? 0 : getReasonCode().hashCode());
result = prime * result + ((getReasonDescription() == null) ? 0 : getReasonDescription().hashCode());
result = prime * result + ((getReqField1() == null) ? 0 : getReqField1().hashCode());
result = prime * result + ((getReqField2() == null) ? 0 : getReqField2().hashCode());
result = prime * result + ((getReqField3() == null) ? 0 : getReqField3().hashCode());
result = prime * result + ((getReqField4() == null) ? 0 : getReqField4().hashCode());
result = prime * result + ((getReqField5() == null) ? 0 : getReqField5().hashCode());
result = prime * result + ((getRspField1() == null) ? 0 : getRspField1().hashCode());
result = prime * result + ((getRspField2() == null) ? 0 : getRspField2().hashCode());
result = prime * result + ((getRspField3() == null) ? 0 : getRspField3().hashCode());
result = prime * result + ((getRspField4() == null) ? 0 : getRspField4().hashCode());
result = prime * result + ((getRspField5() == null) ? 0 : getRspField5().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ExternalIntegMessage))
return false;
else {MPAY_ExternalIntegMessage other = (MPAY_ExternalIntegMessage) obj;
return this.hashCode() == other.hashCode();}
}


}