package com.progressoft.mpay.entities;

public class WF_PSPService
 {
    // Steps
    public static final String STEP_Initialization = "106601";
    public static final String STEP_View = "106602";
    public static final String STEP_Deleted = "106603";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_View = "Approved";
    public static final String STEP_STATUS_Deleted = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_SVC_Approve = "SVC_Approve";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_SaveView = "3";
    public static final String ACTION_CODE_DeleteView = "4";
    public static final String ACTION_CODE_SVC_ApproveView = "5";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_SaveView = "1579088103";
    public static final String ACTION_KEY_DeleteView = "1049206507";
    public static final String ACTION_KEY_SVC_ApproveView = "362008211";

}