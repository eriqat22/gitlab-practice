package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_FinancialIntegMsgs")
@XmlRootElement(name="FinancialIntegMsgs")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_FinancialIntegMsg extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_FinancialIntegMsg(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REF_MESSAGE = "refMessage";
@Column(name="REFMESSAGE", nullable=false, length=40)
private String refMessage;
public String getRefMessage(){
return this.refMessage;
}
public void setRefMessage(String refMessage){
this.refMessage = refMessage;
}

public static final String ACCT1 = "acct1";
@Column(name="ACCT1", nullable=true, length=148)
private String acct1;
public String getAcct1(){
return this.acct1;
}
public void setAcct1(String acct1){
this.acct1 = acct1;
}

public static final String ACCT2 = "acct2";
@Column(name="ACCT2", nullable=true, length=148)
private String acct2;
public String getAcct2(){
return this.acct2;
}
public void setAcct2(String acct2){
this.acct2 = acct2;
}

public static final String AMOUNT = "amount";
@Column(name="AMOUNT", nullable=true, precision=21, scale=8, length=20)
private java.math.BigDecimal amount;
public java.math.BigDecimal getAmount(){
return this.amount;
}
public void setAmount(java.math.BigDecimal amount){
this.amount = amount;
}

public static final String SWT_PROCESSING_TIME = "swtProcessingTime";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="SWTPROCESSINGTIME", nullable=true, length=32)
private java.sql.Timestamp swtProcessingTime;
public java.sql.Timestamp getSwtProcessingTime(){
return this.swtProcessingTime;
}
public void setSwtProcessingTime(java.sql.Timestamp swtProcessingTime){
this.swtProcessingTime = swtProcessingTime;
}

public static final String MAX_REPLY_TIME = "maxReplyTime";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="MAXREPLYTIME", nullable=true, length=32)
private java.sql.Timestamp maxReplyTime;
public java.sql.Timestamp getMaxReplyTime(){
return this.maxReplyTime;
}
public void setMaxReplyTime(java.sql.Timestamp maxReplyTime){
this.maxReplyTime = maxReplyTime;
}

public static final String SESSION_I_D = "sessionID";
@Column(name="SESSIONID", nullable=true, length=255)
private String sessionID;
public String getSessionID(){
return this.sessionID;
}
public void setSessionID(String sessionID){
this.sessionID = sessionID;
}

public static final String NARRATION = "narration";
@Column(name="NARRATION", nullable=true, length=4000)
private String narration;
public String getNarration(){
return this.narration;
}
public void setNarration(String narration){
this.narration = narration;
}

public static final String IS_PROCESSED = "isProcessed";
@Column(name="ISPROCESSED", nullable=true, length=1)
private Boolean isProcessed;
public Boolean getIsProcessed(){
return this.isProcessed;
}
public void setIsProcessed(Boolean isProcessed){
this.isProcessed = isProcessed;
}

public static final String IS_CANCELED = "isCanceled";
@Column(name="ISCANCELED", nullable=false, length=1)
private Boolean isCanceled;
public Boolean getIsCanceled(){
return this.isCanceled;
}
public void setIsCanceled(Boolean isCanceled){
this.isCanceled = isCanceled;
}

public static final String REF_MSG_LOG = "refMsgLog";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMSGLOGID", nullable=false)
private MPAY_MpClearIntegMsgLog refMsgLog;
public MPAY_MpClearIntegMsgLog getRefMsgLog(){
return this.refMsgLog;
}
public void setRefMsgLog(MPAY_MpClearIntegMsgLog refMsgLog){
this.refMsgLog = refMsgLog;
}

public static final String INTEG_MSG_TYPE = "integMsgType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="INTEGMSGTYPEID", nullable=false)
private MPAY_MpClearIntegMsgType integMsgType;
public MPAY_MpClearIntegMsgType getIntegMsgType(){
return this.integMsgType;
}
public void setIntegMsgType(MPAY_MpClearIntegMsgType integMsgType){
this.integMsgType = integMsgType;
}

public static final String PAYMENT_TYPE = "paymentType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PAYMENTTYPEID", nullable=true)
private MPAY_MessageType paymentType;
public MPAY_MessageType getPaymentType(){
return this.paymentType;
}
public void setPaymentType(MPAY_MessageType paymentType){
this.paymentType = paymentType;
}

public static final String REF_ORIGINAL_MSG = "refOriginalMsg";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFORIGINALMSGID", nullable=true)
private MPAY_FinancialIntegMsg refOriginalMsg;
public MPAY_FinancialIntegMsg getRefOriginalMsg(){
return this.refOriginalMsg;
}
public void setRefOriginalMsg(MPAY_FinancialIntegMsg refOriginalMsg){
this.refOriginalMsg = refOriginalMsg;
}

public static final String RESPONSE = "response";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="RESPONSEID", nullable=true)
private MPAY_MpClearResponseCode response;
public MPAY_MpClearResponseCode getResponse(){
return this.response;
}
public void setResponse(MPAY_MpClearResponseCode response){
this.response = response;
}

public static final String PROCESS_REJECTION = "processRejection";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PROCESSREJECTIONID", nullable=true)
private MPAY_MpClearIntegRjctReason processRejection;
public MPAY_MpClearIntegRjctReason getProcessRejection(){
return this.processRejection;
}
public void setProcessRejection(MPAY_MpClearIntegRjctReason processRejection){
this.processRejection = processRejection;
}

public static final String CURRENCY = "currency";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CURRENCYID", nullable=true)
private com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency;
public com.progressoft.jfw.model.bussinessobject.core.JFWCurrency getCurrency(){
return this.currency;
}
public void setCurrency(com.progressoft.jfw.model.bussinessobject.core.JFWCurrency currency){
this.currency = currency;
}

public static final String REF_ORIGINAL_MSG_FINANCIAL_INTEG_MSGS = "refOriginalMsgFinancialIntegMsgs";
@OneToMany(mappedBy = "refOriginalMsg")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_FinancialIntegMsg> refOriginalMsgFinancialIntegMsgs;
public List<MPAY_FinancialIntegMsg> getRefOriginalMsgFinancialIntegMsgs(){
return this.refOriginalMsgFinancialIntegMsgs;
}
public void setRefOriginalMsgFinancialIntegMsgs(List<MPAY_FinancialIntegMsg> refOriginalMsgFinancialIntegMsgs){
this.refOriginalMsgFinancialIntegMsgs = refOriginalMsgFinancialIntegMsgs;
}

@Override
public String toString() {
return "MPAY_FinancialIntegMsg [id= " + getId() + ", refMessage= " + getRefMessage() + ", acct1= " + getAcct1() + ", acct2= " + getAcct2() + ", amount= " + getAmount() + ", swtProcessingTime= " + getSwtProcessingTime() + ", maxReplyTime= " + getMaxReplyTime() + ", sessionID= " + getSessionID() + ", narration= " + getNarration() + ", isProcessed= " + getIsProcessed() + ", isCanceled= " + getIsCanceled() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRefMessage() == null) ? 0 : getRefMessage().hashCode());
result = prime * result + ((getAcct1() == null) ? 0 : getAcct1().hashCode());
result = prime * result + ((getAcct2() == null) ? 0 : getAcct2().hashCode());
result = prime * result + ((getSessionID() == null) ? 0 : getSessionID().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_FinancialIntegMsg))
return false;
else {MPAY_FinancialIntegMsg other = (MPAY_FinancialIntegMsg) obj;
return this.hashCode() == other.hashCode();}
}


}