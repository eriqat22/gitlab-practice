package com.progressoft.mpay.entities;

public class WF_MobileAccount
 {
    // Steps
    public static final String STEP_Initialization = "101001";
    public static final String STEP_Approval = "101002";
    public static final String STEP_Rejected = "101003";
    public static final String STEP_Modification = "101004";
    public static final String STEP_Approved = "101005";
    public static final String STEP_End = "101006";
    public static final String STEP_Integration = "101007";
    public static final String STEP_Deleting = "101008";
    public static final String STEP_DeletionApproval = "101009";
    public static final String STEP_Modified = "101010";
    public static final String STEP_Canceling = "101011";
    public static final String STEP_SwitchDefaultApproval = "101012";
    public static final String STEP_SuspensionApproval = "101013";
    public static final String STEP_Suspended = "101014";
    public static final String STEP_ReactivationApproval = "101015";
    public static final String STEP_DefaultApproval = "101016";
    public static final String STEP_DormantAccount = "101017";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_Approval = "Waiting Approval";
    public static final String STEP_STATUS_Rejected = "Rejected";
    public static final String STEP_STATUS_Modification = "Modified";
    public static final String STEP_STATUS_Approved = "Approved";
    public static final String STEP_STATUS_End = "Deleted";
    public static final String STEP_STATUS_Integration = "Waiting Reply";
    public static final String STEP_STATUS_Deleting = "Waiting Delete";
    public static final String STEP_STATUS_DeletionApproval = "Deletion Approval";
    public static final String STEP_STATUS_Modified = "Modified";
    public static final String STEP_STATUS_Canceling = "Canceling";
    public static final String STEP_STATUS_SwitchDefaultApproval = "Set Default Approval";
    public static final String STEP_STATUS_SuspensionApproval = "Suspension Approval";
    public static final String STEP_STATUS_Suspended = "Suspended";
    public static final String STEP_STATUS_ReactivationApproval = "Reactivation Approval";
    public static final String STEP_STATUS_DefaultApproval = "Waiting Approval";
    public static final String STEP_STATUS_DormantAccount = "Dormant";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_SVC_Approve = "SVC_Approve";
    public static final String ACTION_NAME_SVC_Reject = "SVC_Reject";
    public static final String ACTION_NAME_ShowChanges = "ShowChanges";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Cancel = "Cancel";
    public static final String ACTION_NAME_SVC_SendApproval = "SVC_SendApproval";
    public static final String ACTION_NAME_SVC_Delete = "SVC_Delete";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_SetDefault = "Set Default";
    public static final String ACTION_NAME_SetSwitchDefault = "Set Switch Default";
    public static final String ACTION_NAME_Suspend = "Suspend";
    public static final String ACTION_NAME_DisplayLimits = "Display Limits";
    public static final String ACTION_NAME_SVC_Suspend = "SVC_Suspend";
    public static final String ACTION_NAME_Dormant = "Dormant";
    public static final String ACTION_NAME_Resend = "Resend";
    public static final String ACTION_NAME_SVC_DefaultReject = "SVC_DefaultReject";
    public static final String ACTION_NAME_SVC_DeleteApproval = "SVC_DeleteApproval";
    public static final String ACTION_NAME_SendToApproval = "SendToApproval";
    public static final String ACTION_NAME_Reactivate = "Reactivate";
    public static final String ACTION_NAME_SVC_Reactivate = "SVC_Reactivate";
    public static final String ACTION_NAME_AwakeDormantAccount = "AwakeDormantAccount";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveApproval = "3";
    public static final String ACTION_CODE_RejectApproval = "4";
    public static final String ACTION_CODE_SVC_ApproveApproval = "5";
    public static final String ACTION_CODE_SVC_RejectApproval = "6";
    public static final String ACTION_CODE_ShowChangesApproval = "44";
    public static final String ACTION_CODE_EditRejected = "7";
    public static final String ACTION_CODE_DeleteRejected = "8";
    public static final String ACTION_CODE_CancelRejected = "9";
    public static final String ACTION_CODE_SVC_SendApprovalRejected = "28";
    public static final String ACTION_CODE_SVC_DeleteRejected = "29";
    public static final String ACTION_CODE_SaveModification = "10";
    public static final String ACTION_CODE_CancelModification = "11";
    public static final String ACTION_CODE_EditApproved = "12";
    public static final String ACTION_CODE_DeleteApproved = "13";
    public static final String ACTION_CODE_SVC_DeleteApproved = "14";
    public static final String ACTION_CODE_SetDefaultApproved = "30";
    public static final String ACTION_CODE_SetSwitchDefaultApproved = "31";
    public static final String ACTION_CODE_SuspendApproved = "32";
    public static final String ACTION_CODE_DisplayLimitsApproved = "45";
    public static final String ACTION_CODE_SVC_SuspendApproved = "33";
    public static final String ACTION_CODE_DormantApproved = "55";
    public static final String ACTION_CODE_SVC_ApproveIntegration = "15";
    public static final String ACTION_CODE_SVC_RejectIntegration = "16";
    public static final String ACTION_CODE_ResendIntegration = "17";
    public static final String ACTION_CODE_SVC_DeleteIntegration = "18";
    public static final String ACTION_CODE_SVC_DefaultRejectIntegration = "43";
    public static final String ACTION_CODE_SVC_DeleteDeleting = "19";
    public static final String ACTION_CODE_SVC_DeleteApprovalDeleting = "20";
    public static final String ACTION_CODE_ApproveDeletionApproval = "21";
    public static final String ACTION_CODE_RejectDeletionApproval = "22";
    public static final String ACTION_CODE_EditModified = "23";
    public static final String ACTION_CODE_SendToApprovalModified = "24";
    public static final String ACTION_CODE_SVC_SendApprovalModified = "25";
    public static final String ACTION_CODE_SVC_RejectCanceling = "26";
    public static final String ACTION_CODE_SVC_ApproveCanceling = "27";
    public static final String ACTION_CODE_ApproveSwitchDefaultApproval = "34";
    public static final String ACTION_CODE_RejectSwitchDefaultApproval = "35";
    public static final String ACTION_CODE_ApproveSuspensionApproval = "36";
    public static final String ACTION_CODE_RejectSuspensionApproval = "37";
    public static final String ACTION_CODE_ReactivateSuspended = "38";
    public static final String ACTION_CODE_SVC_ReactivateSuspended = "43";
    public static final String ACTION_CODE_SVC_DeleteSuspended = "46";
    public static final String ACTION_CODE_ApproveReactivationApproval = "39";
    public static final String ACTION_CODE_RejectReactivationApproval = "40";
    public static final String ACTION_CODE_ApproveDefaultApproval = "41";
    public static final String ACTION_CODE_RejectDefaultApproval = "42";
    public static final String ACTION_CODE_AwakeDormantAccountDormantAccount = "56";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveApproval = "1829173529";
    public static final String ACTION_KEY_RejectApproval = "2075064453";
    public static final String ACTION_KEY_SVC_ApproveApproval = "433476722";
    public static final String ACTION_KEY_SVC_RejectApproval = "2095138270";
    public static final String ACTION_KEY_ShowChangesApproval = "1020433056";
    public static final String ACTION_KEY_EditRejected = "1444773990";
    public static final String ACTION_KEY_DeleteRejected = "539904079";
    public static final String ACTION_KEY_CancelRejected = "472658138";
    public static final String ACTION_KEY_SVC_SendApprovalRejected = "1289815726";
    public static final String ACTION_KEY_SVC_DeleteRejected = "1279554627";
    public static final String ACTION_KEY_SaveModification = "1462878285";
    public static final String ACTION_KEY_CancelModification = "712347791";
    public static final String ACTION_KEY_EditApproved = "450972607";
    public static final String ACTION_KEY_DeleteApproved = "1868682699";
    public static final String ACTION_KEY_SVC_DeleteApproved = "1842323209";
    public static final String ACTION_KEY_SetDefaultApproved = "1956218944";
    public static final String ACTION_KEY_SetSwitchDefaultApproved = "1466206221";
    public static final String ACTION_KEY_SuspendApproved = "480607418";
    public static final String ACTION_KEY_DisplayLimitsApproved = "1149498924";
    public static final String ACTION_KEY_SVC_SuspendApproved = "1040467387";
    public static final String ACTION_KEY_DormantApproved = "1588989077";
    public static final String ACTION_KEY_SVC_ApproveIntegration = "298277827";
    public static final String ACTION_KEY_SVC_RejectIntegration = "1895592499";
    public static final String ACTION_KEY_ResendIntegration = "1797674594";
    public static final String ACTION_KEY_SVC_DeleteIntegration = "855509671";
    public static final String ACTION_KEY_SVC_DefaultRejectIntegration = "1137154373";
    public static final String ACTION_KEY_SVC_DeleteDeleting = "1109933403";
    public static final String ACTION_KEY_SVC_DeleteApprovalDeleting = "1270666085";
    public static final String ACTION_KEY_ApproveDeletionApproval = "839850656";
    public static final String ACTION_KEY_RejectDeletionApproval = "1108254195";
    public static final String ACTION_KEY_EditModified = "918290571";
    public static final String ACTION_KEY_SendToApprovalModified = "755476275";
    public static final String ACTION_KEY_SVC_SendApprovalModified = "1067897522";
    public static final String ACTION_KEY_SVC_RejectCanceling = "1007572870";
    public static final String ACTION_KEY_SVC_ApproveCanceling = "1690661095";
    public static final String ACTION_KEY_ApproveSwitchDefaultApproval = "1780774200";
    public static final String ACTION_KEY_RejectSwitchDefaultApproval = "460679080";
    public static final String ACTION_KEY_ApproveSuspensionApproval = "1726906252";
    public static final String ACTION_KEY_RejectSuspensionApproval = "2021210014";
    public static final String ACTION_KEY_ReactivateSuspended = "2010077800";
    public static final String ACTION_KEY_SVC_ReactivateSuspended = "1816504165";
    public static final String ACTION_KEY_SVC_DeleteSuspended = "1706945367";
    public static final String ACTION_KEY_ApproveReactivationApproval = "612271431";
    public static final String ACTION_KEY_RejectReactivationApproval = "1585993736";
    public static final String ACTION_KEY_ApproveDefaultApproval = "851921892";
    public static final String ACTION_KEY_RejectDefaultApproval = "1860254243";
    public static final String ACTION_KEY_AwakeDormantAccountDormantAccount = "55876982";

}