package com.progressoft.mpay.entities;

public class WF_PendingMPClearMessages
 {
    // Steps
    public static final String STEP_Initialization = "102401";
    public static final String STEP_Pending = "102405";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_Pending = "Pending";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Resend = "Resend";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ResendPending = "3";
    public static final String ACTION_CODE_CancelPending = "4";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ResendPending = "303867771";
    public static final String ACTION_KEY_CancelPending = "1655162741";

}