package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_CorpoarteServices")
@XmlRootElement(name="CorpoarteServices")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CorpoarteService extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CorpoarteService(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String NAME = "name";
@Column(name="NAME", nullable=false, length=128)
private String name;
public String getName(){
return this.name;
}
public void setName(String name){
this.name = name;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=128)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String ALIAS = "alias";
@Column(name="ALIAS", nullable=true, length=25)
private String alias;
public String getAlias(){
return this.alias;
}
public void setAlias(String alias){
this.alias = alias;
}

public static final String MPCLEAR_ALIAS = "mpclearAlias";
@Column(name="MPCLEARALIAS", nullable=true, length=16)
private String mpclearAlias;
public String getMpclearAlias(){
return this.mpclearAlias;
}
public void setMpclearAlias(String mpclearAlias){
this.mpclearAlias = mpclearAlias;
}

public static final String BANKED_UNBANKED = "bankedUnbanked";
@Column(name="BANKEDUNBANKED", nullable=false, length=20)
private String bankedUnbanked;
public String getBankedUnbanked(){
return this.bankedUnbanked;
}
public void setBankedUnbanked(String bankedUnbanked){
this.bankedUnbanked = bankedUnbanked;
}

public static final String BRANCH = "branch";
@Column(name="BRANCH", nullable=true, length=50)
private String branch;
public String getBranch(){
return this.branch;
}
public void setBranch(String branch){
this.branch = branch;
}

public static final String EXTERNAL_ACC = "externalAcc";
@Column(name="EXTERNALACC", nullable=true, length=100)
private String externalAcc;
public String getExternalAcc(){
return this.externalAcc;
}
public void setExternalAcc(String externalAcc){
this.externalAcc = externalAcc;
}

public static final String IBAN = "iban";
@Column(name="IBAN", nullable=true, length=100)
private String iban;
public String getIban(){
return this.iban;
}
public void setIban(String iban){
this.iban = iban;
}

public static final String MAS = "mas";
@Column(name="MAS", nullable=false, length=10)
private Long mas;
public Long getMas(){
return this.mas;
}
public void setMas(Long mas){
this.mas = mas;
}

public static final String IS_REGISTERED = "isRegistered";
@Column(name="ISREGISTERED", nullable=true, length=1)
private Boolean isRegistered;
public Boolean getIsRegistered(){
return this.isRegistered;
}
public void setIsRegistered(Boolean isRegistered){
this.isRegistered = isRegistered;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=5)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String NOTIFICATION_RECEIVER = "notificationReceiver";
@Column(name="NOTIFICATIONRECEIVER", nullable=true, length=1000)
private String notificationReceiver;
public String getNotificationReceiver(){
return this.notificationReceiver;
}
public void setNotificationReceiver(String notificationReceiver){
this.notificationReceiver = notificationReceiver;
}

public static final String PIN = "pin";
@Column(name="PIN", nullable=true, length=255)
private String pin;
public String getPin(){
return this.pin;
}
public void setPin(String pin){
this.pin = pin;
}

public static final String ACTIVATION_CODE = "activationCode";
@Column(name="ACTIVATIONCODE", nullable=true, length=255)
private String activationCode;
public String getActivationCode(){
return this.activationCode;
}
public void setActivationCode(String activationCode){
this.activationCode = activationCode;
}

public static final String RETRY_COUNT = "retryCount";
@Column(name="RETRYCOUNT", nullable=false, length=4)
private Long retryCount;
public Long getRetryCount(){
return this.retryCount;
}
public void setRetryCount(Long retryCount){
this.retryCount = retryCount;
}

public static final String IS_BLOCKED = "isBlocked";
@Column(name="ISBLOCKED", nullable=false, length=1)
private Boolean isBlocked;
public Boolean getIsBlocked(){
return this.isBlocked;
}
public void setIsBlocked(Boolean isBlocked){
this.isBlocked = isBlocked;
}

public static final String SESSION_ID = "sessionId";
@Column(name="SESSIONID", nullable=true, length=1000)
private String sessionId;
public String getSessionId(){
return this.sessionId;
}
public void setSessionId(String sessionId){
this.sessionId = sessionId;
}

public static final String EXTRA_DATA = "extraData";
@Column(name="EXTRADATA", nullable=true, length=1000)
private String extraData;
public String getExtraData(){
return this.extraData;
}
public void setExtraData(String extraData){
this.extraData = extraData;
}

public static final String NEW_NAME = "newName";
@Column(name="NEWNAME", nullable=true, length=128)
private String newName;
public String getNewName(){
return this.newName;
}
public void setNewName(String newName){
this.newName = newName;
}

public static final String NEW_DESCRIPTION = "newDescription";
@Column(name="NEWDESCRIPTION", nullable=true, length=128)
private String newDescription;
public String getNewDescription(){
return this.newDescription;
}
public void setNewDescription(String newDescription){
this.newDescription = newDescription;
}

public static final String NEW_ALIAS = "newAlias";
@Column(name="NEWALIAS", nullable=true, length=25)
private String newAlias;
public String getNewAlias(){
return this.newAlias;
}
public void setNewAlias(String newAlias){
this.newAlias = newAlias;
}

public static final String APPROVED_DATA = "approvedData";
@Column(name="APPROVEDDATA", nullable=true, length=4000)
private String approvedData;
public String getApprovedData(){
return this.approvedData;
}
public void setApprovedData(String approvedData){
this.approvedData = approvedData;
}

public static final String REJECTION_NOTE = "rejectionNote";
@Column(name="REJECTIONNOTE", nullable=true, length=255)
private String rejectionNote;
public String getRejectionNote(){
return this.rejectionNote;
}
public void setRejectionNote(String rejectionNote){
this.rejectionNote = rejectionNote;
}

public static final String APPROVAL_NOTE = "approvalNote";
@Column(name="APPROVALNOTE", nullable=true, length=255)
private String approvalNote;
public String getApprovalNote(){
return this.approvalNote;
}
public void setApprovalNote(String approvalNote){
this.approvalNote = approvalNote;
}

public static final String CHANGES = "changes";
@Column(name="CHANGES", nullable=true, length=1000)
@Transient
private String changes;
public String getChanges(){
return this.changes;
}
public void setChanges(String changes){
this.changes = changes;
}

public static final String PIN_LAST_CHANGED = "pinLastChanged";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="PINLASTCHANGED", nullable=true, length=12)
private java.sql.Timestamp pinLastChanged;
public java.sql.Timestamp getPinLastChanged(){
return this.pinLastChanged;
}
public void setPinLastChanged(java.sql.Timestamp pinLastChanged){
this.pinLastChanged = pinLastChanged;
}

public static final String MAX_NUMBER_OF_DEVICES = "maxNumberOfDevices";
@Column(name="MAXNUMBEROFDEVICES", nullable=false, length=10)
private Long maxNumberOfDevices;
public Long getMaxNumberOfDevices(){
return this.maxNumberOfDevices;
}
public void setMaxNumberOfDevices(Long maxNumberOfDevices){
this.maxNumberOfDevices = maxNumberOfDevices;
}

public static final String ACTIVATION_CODE_VALIDY = "activationCodeValidy";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="ACTIVATIONCODEVALIDY", nullable=true, length=12)
private java.sql.Timestamp activationCodeValidy;
public java.sql.Timestamp getActivationCodeValidy(){
return this.activationCodeValidy;
}
public void setActivationCodeValidy(java.sql.Timestamp activationCodeValidy){
this.activationCodeValidy = activationCodeValidy;
}

public static final String VIRTUAL_IBAN = "virtualIban";
@Column(name="VIRTUALIBAN", nullable=true, length=100)
private String virtualIban;
public String getVirtualIban(){
return this.virtualIban;
}
public void setVirtualIban(String virtualIban){
this.virtualIban = virtualIban;
}

public static final String EMAIL = "email";
@Column(name="EMAIL", nullable=true, length=128)
private String email;
public String getEmail(){
return this.email;
}
public void setEmail(String email){
this.email = email;
}

public static final String MOBILE_NUMBER = "mobileNumber";
@Column(name="MOBILENUMBER", nullable=true, length=50)
private String mobileNumber;
public String getMobileNumber(){
return this.mobileNumber;
}
public void setMobileNumber(String mobileNumber){
this.mobileNumber = mobileNumber;
}

public static final String ENABLE_S_M_S = "enableSMS";
@Column(name="ENABLESMS", nullable=true, length=10)
private Boolean enableSMS;
public Boolean getEnableSMS(){
return this.enableSMS;
}
public void setEnableSMS(Boolean enableSMS){
this.enableSMS = enableSMS;
}

public static final String ENABLE_EMAIL = "enableEmail";
@Column(name="ENABLEEMAIL", nullable=true, length=10)
private Boolean enableEmail;
public Boolean getEnableEmail(){
return this.enableEmail;
}
public void setEnableEmail(Boolean enableEmail){
this.enableEmail = enableEmail;
}

public static final String ENABLE_PUSH_NOTIFICATION = "enablePushNotification";
@Column(name="ENABLEPUSHNOTIFICATION", nullable=true, length=10)
private Boolean enablePushNotification;
public Boolean getEnablePushNotification(){
return this.enablePushNotification;
}
public void setEnablePushNotification(Boolean enablePushNotification){
this.enablePushNotification = enablePushNotification;
}

public static final String LATITUDE = "latitude";
@Column(name="LATITUDE", nullable=true, length=20)
private String latitude;
public String getLatitude(){
return this.latitude;
}
public void setLatitude(String latitude){
this.latitude = latitude;
}

public static final String LONGITUDE = "longitude";
@Column(name="LONGITUDE", nullable=true, length=20)
private String longitude;
public String getLongitude(){
return this.longitude;
}
public void setLongitude(String longitude){
this.longitude = longitude;
}

public static final String RATING = "rating";
@Column(name="RATING", nullable=true, length=10)
private String rating;
public String getRating(){
return this.rating;
}
public void setRating(String rating){
this.rating = rating;
}

public static final String RATE_COUNT = "rateCount";
@Column(name="RATECOUNT", nullable=true, length=10)
private Long rateCount;
public Long getRateCount(){
return this.rateCount;
}
public void setRateCount(Long rateCount){
this.rateCount = rateCount;
}

public static final String REF_CORPORATE = "refCorporate";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCORPORATEID", nullable=false)
private MPAY_Corporate refCorporate;
@flexjson.JSON(include = false)
public MPAY_Corporate getRefCorporate(){
return this.refCorporate;
}
public void setRefCorporate(MPAY_Corporate refCorporate){
this.refCorporate = refCorporate;
}

public static final String REF_PARENT_CORPORATE = "refParentCorporate";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFPARENTCORPORATEID", nullable=true)
private MPAY_Corporate refParentCorporate;
@flexjson.JSON(include = false)
public MPAY_Corporate getRefParentCorporate(){
return this.refParentCorporate;
}
public void setRefParentCorporate(MPAY_Corporate refParentCorporate){
this.refParentCorporate = refParentCorporate;
}

public static final String CATEGORY = "category";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CATEGORYID", nullable=false)
private MPAY_AccountCategory category;
public MPAY_AccountCategory getCategory(){
return this.category;
}
public void setCategory(MPAY_AccountCategory category){
this.category = category;
}

public static final String SERVICES = "services";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SERVICESID", nullable=true)
private MPAY_AgentService services;
public MPAY_AgentService getServices(){
return this.services;
}
public void setServices(MPAY_AgentService services){
this.services = services;
}

public static final String CITY = "city";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CITYID", nullable=true)
private MPAY_City city;
public MPAY_City getCity(){
return this.city;
}
public void setCity(MPAY_City city){
this.city = city;
}

public static final String AREA = "area";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="AREAID", nullable=true)
private MPAY_CityArea area;
public MPAY_CityArea getArea(){
return this.area;
}
public void setArea(MPAY_CityArea area){
this.area = area;
}

public static final String BANK = "bank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BANKID", nullable=false)
private MPAY_Bank bank;
public MPAY_Bank getBank(){
return this.bank;
}
public void setBank(MPAY_Bank bank){
this.bank = bank;
}

public static final String PAYMENT_TYPE = "paymentType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="PAYMENTTYPEID", nullable=false)
private MPAY_MessageType paymentType;
public MPAY_MessageType getPaymentType(){
return this.paymentType;
}
public void setPaymentType(MPAY_MessageType paymentType){
this.paymentType = paymentType;
}

public static final String NOTIFICATION_CHANNEL = "notificationChannel";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="NOTIFICATIONCHANNELID", nullable=false)
private MPAY_NotificationChannel notificationChannel;
public MPAY_NotificationChannel getNotificationChannel(){
return this.notificationChannel;
}
public void setNotificationChannel(MPAY_NotificationChannel notificationChannel){
this.notificationChannel = notificationChannel;
}

public static final String REF_PROFILE = "refProfile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFPROFILEID", nullable=false)
private MPAY_Profile refProfile;
public MPAY_Profile getRefProfile(){
return this.refProfile;
}
public void setRefProfile(MPAY_Profile refProfile){
this.refProfile = refProfile;
}

public static final String SERVICE_CATEGORY = "serviceCategory";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SERVICECATEGORYID", nullable=false)
private MPAY_ServicesCategory serviceCategory;
public MPAY_ServicesCategory getServiceCategory(){
return this.serviceCategory;
}
public void setServiceCategory(MPAY_ServicesCategory serviceCategory){
this.serviceCategory = serviceCategory;
}

public static final String SERVICE_TYPE = "serviceType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="SERVICETYPEID", nullable=false)
private MPAY_ServiceType serviceType;
public MPAY_ServiceType getServiceType(){
return this.serviceType;
}
public void setServiceType(MPAY_ServiceType serviceType){
this.serviceType = serviceType;
}

public static final String SENDER_SERVICE_TRANSACTIONS = "senderServiceTransactions";
@OneToMany(mappedBy = "senderService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> senderServiceTransactions;
public List<MPAY_Transaction> getSenderServiceTransactions(){
return this.senderServiceTransactions;
}
public void setSenderServiceTransactions(List<MPAY_Transaction> senderServiceTransactions){
this.senderServiceTransactions = senderServiceTransactions;
}

public static final String RECEIVER_SERVICE_TRANSACTIONS = "receiverServiceTransactions";
@OneToMany(mappedBy = "receiverService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> receiverServiceTransactions;
public List<MPAY_Transaction> getReceiverServiceTransactions(){
return this.receiverServiceTransactions;
}
public void setReceiverServiceTransactions(List<MPAY_Transaction> receiverServiceTransactions){
this.receiverServiceTransactions = receiverServiceTransactions;
}

public static final String SOURCE_SERVICE_SYSTEM_ACCOUNTS_CASH_OUT = "sourceServiceSystemAccountsCashOut";
@OneToMany(mappedBy = "sourceService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_SystemAccountsCashOut> sourceServiceSystemAccountsCashOut;
public List<MPAY_SystemAccountsCashOut> getSourceServiceSystemAccountsCashOut(){
return this.sourceServiceSystemAccountsCashOut;
}
public void setSourceServiceSystemAccountsCashOut(List<MPAY_SystemAccountsCashOut> sourceServiceSystemAccountsCashOut){
this.sourceServiceSystemAccountsCashOut = sourceServiceSystemAccountsCashOut;
}

public static final String DESTINATION_SERVICE_SYSTEM_ACCOUNTS_CASH_OUT = "destinationServiceSystemAccountsCashOut";
@OneToMany(mappedBy = "destinationService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_SystemAccountsCashOut> destinationServiceSystemAccountsCashOut;
public List<MPAY_SystemAccountsCashOut> getDestinationServiceSystemAccountsCashOut(){
return this.destinationServiceSystemAccountsCashOut;
}
public void setDestinationServiceSystemAccountsCashOut(List<MPAY_SystemAccountsCashOut> destinationServiceSystemAccountsCashOut){
this.destinationServiceSystemAccountsCashOut = destinationServiceSystemAccountsCashOut;
}

public static final String REF_CORPORATE_SERVICE_SERVICE_CASH_OUT = "refCorporateServiceServiceCashOut";
@OneToMany(mappedBy = "refCorporateService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceCashOut> refCorporateServiceServiceCashOut;
public List<MPAY_ServiceCashOut> getRefCorporateServiceServiceCashOut(){
return this.refCorporateServiceServiceCashOut;
}
public void setRefCorporateServiceServiceCashOut(List<MPAY_ServiceCashOut> refCorporateServiceServiceCashOut){
this.refCorporateServiceServiceCashOut = refCorporateServiceServiceCashOut;
}

public static final String REF_CORPORATE_SERVICE_SERVICE_CASH_IN = "refCorporateServiceServiceCashIn";
@OneToMany(mappedBy = "refCorporateService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceCashIn> refCorporateServiceServiceCashIn;
public List<MPAY_ServiceCashIn> getRefCorporateServiceServiceCashIn(){
return this.refCorporateServiceServiceCashIn;
}
public void setRefCorporateServiceServiceCashIn(List<MPAY_ServiceCashIn> refCorporateServiceServiceCashIn){
this.refCorporateServiceServiceCashIn = refCorporateServiceServiceCashIn;
}

public static final String SENDER_SERVICE_CLIENTS_O_T_PS = "senderServiceClientsOTPs";
@OneToMany(mappedBy = "senderService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ClientsOTP> senderServiceClientsOTPs;
public List<MPAY_ClientsOTP> getSenderServiceClientsOTPs(){
return this.senderServiceClientsOTPs;
}
public void setSenderServiceClientsOTPs(List<MPAY_ClientsOTP> senderServiceClientsOTPs){
this.senderServiceClientsOTPs = senderServiceClientsOTPs;
}

public static final String REF_SERVICE_SERVICE_INTEG_SETTINGS = "refServiceServiceIntegSettings";
@OneToMany(mappedBy = "refService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceIntegSetting> refServiceServiceIntegSettings;
public List<MPAY_ServiceIntegSetting> getRefServiceServiceIntegSettings(){
return this.refServiceServiceIntegSettings;
}
public void setRefServiceServiceIntegSettings(List<MPAY_ServiceIntegSetting> refServiceServiceIntegSettings){
this.refServiceServiceIntegSettings = refServiceServiceIntegSettings;
}

public static final String REF_SERVICE_EXTERNAL_INTEG_MESSAGES = "refServiceExternalIntegMessages";
@OneToMany(mappedBy = "refService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ExternalIntegMessage> refServiceExternalIntegMessages;
public List<MPAY_ExternalIntegMessage> getRefServiceExternalIntegMessages(){
return this.refServiceExternalIntegMessages;
}
public void setRefServiceExternalIntegMessages(List<MPAY_ExternalIntegMessage> refServiceExternalIntegMessages){
this.refServiceExternalIntegMessages = refServiceExternalIntegMessages;
}

public static final String REF_SERVICE_SERVICE_INTEG_MESSAGES = "refServiceServiceIntegMessages";
@OneToMany(mappedBy = "refService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceIntegMessage> refServiceServiceIntegMessages;
public List<MPAY_ServiceIntegMessage> getRefServiceServiceIntegMessages(){
return this.refServiceServiceIntegMessages;
}
public void setRefServiceServiceIntegMessages(List<MPAY_ServiceIntegMessage> refServiceServiceIntegMessages){
this.refServiceServiceIntegMessages = refServiceServiceIntegMessages;
}

public static final String SERVICE_SERVICE_ACCOUNTS = "serviceServiceAccounts";
@OneToMany(mappedBy = "service")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ServiceAccount> serviceServiceAccounts;
public List<MPAY_ServiceAccount> getServiceServiceAccounts(){
return this.serviceServiceAccounts;
}
public void setServiceServiceAccounts(List<MPAY_ServiceAccount> serviceServiceAccounts){
this.serviceServiceAccounts = serviceServiceAccounts;
}

public static final String REF_CORPORATE_SERVICE_CORPORATE_DEVICES = "refCorporateServiceCorporateDevices";
@OneToMany(mappedBy = "refCorporateService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CorporateDevice> refCorporateServiceCorporateDevices;
public List<MPAY_CorporateDevice> getRefCorporateServiceCorporateDevices(){
return this.refCorporateServiceCorporateDevices;
}
public void setRefCorporateServiceCorporateDevices(List<MPAY_CorporateDevice> refCorporateServiceCorporateDevices){
this.refCorporateServiceCorporateDevices = refCorporateServiceCorporateDevices;
}

public static final String REG_AGENT_CUSTOMERS = "regAgentCustomers";
@OneToMany(mappedBy = "regAgent")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Customer> regAgentCustomers;
public List<MPAY_Customer> getRegAgentCustomers(){
return this.regAgentCustomers;
}
public void setRegAgentCustomers(List<MPAY_Customer> regAgentCustomers){
this.regAgentCustomers = regAgentCustomers;
}

public static final String REF_SERVICE_CORP_INTEG_MESSAGES = "refServiceCorpIntegMessages";
@OneToMany(mappedBy = "refService")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CorpIntegMessage> refServiceCorpIntegMessages;
public List<MPAY_CorpIntegMessage> getRefServiceCorpIntegMessages(){
return this.refServiceCorpIntegMessages;
}
public void setRefServiceCorpIntegMessages(List<MPAY_CorpIntegMessage> refServiceCorpIntegMessages){
this.refServiceCorpIntegMessages = refServiceCorpIntegMessages;
}

@Override
public String toString() {
return "MPAY_CorpoarteService [id= " + getId() + ", name= " + getName() + ", description= " + getDescription() + ", alias= " + getAlias() + ", mpclearAlias= " + getMpclearAlias() + ", bankedUnbanked= " + getBankedUnbanked() + ", branch= " + getBranch() + ", externalAcc= " + getExternalAcc() + ", iban= " + getIban() + ", mas= " + getMas() + ", isRegistered= " + getIsRegistered() + ", isActive= " + getIsActive() + ", notificationReceiver= " + getNotificationReceiver() + ", pin= " + getPin() + ", activationCode= " + getActivationCode() + ", retryCount= " + getRetryCount() + ", isBlocked= " + getIsBlocked() + ", sessionId= " + getSessionId() + ", extraData= " + getExtraData() + ", newName= " + getNewName() + ", newDescription= " + getNewDescription() + ", newAlias= " + getNewAlias() + ", approvedData= " + getApprovedData() + ", rejectionNote= " + getRejectionNote() + ", approvalNote= " + getApprovalNote() + ", changes= " + getChanges() + ", pinLastChanged= " + getPinLastChanged() + ", maxNumberOfDevices= " + getMaxNumberOfDevices() + ", activationCodeValidy= " + getActivationCodeValidy() + ", virtualIban= " + getVirtualIban() + ", email= " + getEmail() + ", mobileNumber= " + getMobileNumber() + ", enableSMS= " + getEnableSMS() + ", enableEmail= " + getEnableEmail() + ", enablePushNotification= " + getEnablePushNotification() + ", latitude= " + getLatitude() + ", longitude= " + getLongitude() + ", rating= " + getRating() + ", rateCount= " + getRateCount() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getAlias() == null) ? 0 : getAlias().hashCode());
result = prime * result + ((getMpclearAlias() == null) ? 0 : getMpclearAlias().hashCode());
result = prime * result + ((getBranch() == null) ? 0 : getBranch().hashCode());
result = prime * result + ((getExternalAcc() == null) ? 0 : getExternalAcc().hashCode());
result = prime * result + ((getIban() == null) ? 0 : getIban().hashCode());
int Mas= new Long("null".equals(getMas() + "") ? 0 : getMas()).intValue();
result = prime * result + (int) (Mas ^ Mas >>> 32);
result = prime * result + ((getNotificationReceiver() == null) ? 0 : getNotificationReceiver().hashCode());
result = prime * result + ((getPin() == null) ? 0 : getPin().hashCode());
result = prime * result + ((getActivationCode() == null) ? 0 : getActivationCode().hashCode());
int RetryCount= new Long("null".equals(getRetryCount() + "") ? 0 : getRetryCount()).intValue();
result = prime * result + (int) (RetryCount ^ RetryCount >>> 32);
result = prime * result + ((getSessionId() == null) ? 0 : getSessionId().hashCode());
result = prime * result + ((getExtraData() == null) ? 0 : getExtraData().hashCode());
result = prime * result + ((getNewName() == null) ? 0 : getNewName().hashCode());
result = prime * result + ((getNewDescription() == null) ? 0 : getNewDescription().hashCode());
result = prime * result + ((getNewAlias() == null) ? 0 : getNewAlias().hashCode());
result = prime * result + ((getApprovedData() == null) ? 0 : getApprovedData().hashCode());
result = prime * result + ((getChanges() == null) ? 0 : getChanges().hashCode());
int MaxNumberOfDevices= new Long("null".equals(getMaxNumberOfDevices() + "") ? 0 : getMaxNumberOfDevices()).intValue();
result = prime * result + (int) (MaxNumberOfDevices ^ MaxNumberOfDevices >>> 32);
result = prime * result + ((getVirtualIban() == null) ? 0 : getVirtualIban().hashCode());
result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
result = prime * result + ((getMobileNumber() == null) ? 0 : getMobileNumber().hashCode());
result = prime * result + ((getLatitude() == null) ? 0 : getLatitude().hashCode());
result = prime * result + ((getLongitude() == null) ? 0 : getLongitude().hashCode());
result = prime * result + ((getRating() == null) ? 0 : getRating().hashCode());
int RateCount= new Long("null".equals(getRateCount() + "") ? 0 : getRateCount()).intValue();
result = prime * result + (int) (RateCount ^ RateCount >>> 32);
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CorpoarteService))
return false;
else {MPAY_CorpoarteService other = (MPAY_CorpoarteService) obj;
return this.hashCode() == other.hashCode();}
}


}