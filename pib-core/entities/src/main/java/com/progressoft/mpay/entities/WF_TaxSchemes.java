package com.progressoft.mpay.entities;

public class WF_TaxSchemes
 {
    // Steps
    public static final String STEP_Initialization = "311801";
    public static final String STEP_CreationApproval = "311802";
    public static final String STEP_RepairNew = "311803";
    public static final String STEP_EditNew = "311804";
    public static final String STEP_ViewApproved = "311805";
    public static final String STEP_EditApproved = "311806";
    public static final String STEP_ModificationApproval = "311807";
    public static final String STEP_RepairRejected = "311808";
    public static final String STEP_EditRejected = "311809";
    public static final String STEP_DeletionApproval = "311810";
    public static final String STEP_End = "311811";
    // Statuses
    public static final String STEP_STATUS_Initialization = "New";
    public static final String STEP_STATUS_CreationApproval = "New Authorization";
    public static final String STEP_STATUS_RepairNew = "Repair New";
    public static final String STEP_STATUS_EditNew = "Edit New";
    public static final String STEP_STATUS_ViewApproved = "Approved";
    public static final String STEP_STATUS_EditApproved = "Modify";
    public static final String STEP_STATUS_ModificationApproval = "Authorization";
    public static final String STEP_STATUS_RepairRejected = "Repair Rejected";
    public static final String STEP_STATUS_EditRejected = "Edit Rejected";
    public static final String STEP_STATUS_DeletionApproval = "Deletion Authorization";
    public static final String STEP_STATUS_End = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Save = "Save";
    public static final String ACTION_NAME_Cancel = "Cancel";
    public static final String ACTION_NAME_Reset = "Reset";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveCreationApproval = "3";
    public static final String ACTION_CODE_RejectCreationApproval = "4";
    public static final String ACTION_CODE_EditRepairNew = "5";
    public static final String ACTION_CODE_DeleteRepairNew = "6";
    public static final String ACTION_CODE_SaveEditNew = "7";
    public static final String ACTION_CODE_CancelEditNew = "8";
    public static final String ACTION_CODE_EditViewApproved = "9";
    public static final String ACTION_CODE_DeleteViewApproved = "10";
    public static final String ACTION_CODE_SaveEditApproved = "11";
    public static final String ACTION_CODE_CancelEditApproved = "12";
    public static final String ACTION_CODE_ApproveModificationApproval = "13";
    public static final String ACTION_CODE_RejectModificationApproval = "14";
    public static final String ACTION_CODE_EditRepairRejected = "15";
    public static final String ACTION_CODE_ResetRepairRejected = "16";
    public static final String ACTION_CODE_SaveEditRejected = "17";
    public static final String ACTION_CODE_CancelEditRejected = "18";
    public static final String ACTION_CODE_ApproveDeletionApproval = "19";
    public static final String ACTION_CODE_RejectDeletionApproval = "20";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveCreationApproval = "1117164674";
    public static final String ACTION_KEY_RejectCreationApproval = "1789523879";
    public static final String ACTION_KEY_EditRepairNew = "1364109138";
    public static final String ACTION_KEY_DeleteRepairNew = "490337541";
    public static final String ACTION_KEY_SaveEditNew = "521808078";
    public static final String ACTION_KEY_CancelEditNew = "1336354760";
    public static final String ACTION_KEY_EditViewApproved = "2036371469";
    public static final String ACTION_KEY_DeleteViewApproved = "425022119";
    public static final String ACTION_KEY_SaveEditApproved = "487515874";
    public static final String ACTION_KEY_CancelEditApproved = "1196921300";
    public static final String ACTION_KEY_ApproveModificationApproval = "712544262";
    public static final String ACTION_KEY_RejectModificationApproval = "2141814494";
    public static final String ACTION_KEY_EditRepairRejected = "1928656346";
    public static final String ACTION_KEY_ResetRepairRejected = "1500173448";
    public static final String ACTION_KEY_SaveEditRejected = "1732400083";
    public static final String ACTION_KEY_CancelEditRejected = "1550769442";
    public static final String ACTION_KEY_ApproveDeletionApproval = "559973460";
    public static final String ACTION_KEY_RejectDeletionApproval = "320029570";

}