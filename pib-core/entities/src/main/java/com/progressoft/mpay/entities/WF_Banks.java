package com.progressoft.mpay.entities;

public class WF_Banks
 {
    // Steps
    public static final String STEP_Initialization = "108801";
    public static final String STEP_ApprovalNewStep = "108802";
    public static final String STEP_RepairStep = "108803";
    public static final String STEP_ApprovalRepairStep = "108804";
    public static final String STEP_ActiveStep = "108805";
    public static final String STEP_EditStep = "108806";
    public static final String STEP_ApprovalEditStep = "108807";
    public static final String STEP_ApprovalDeleteStep = "108808";
    public static final String STEP_DeletedStep = "108809";
    // Statuses
    public static final String STEP_STATUS_Initialization = "";
    public static final String STEP_STATUS_ApprovalNewStep = "Approval New";
    public static final String STEP_STATUS_RepairStep = "Repair";
    public static final String STEP_STATUS_ApprovalRepairStep = "Approval Repair";
    public static final String STEP_STATUS_ActiveStep = "Active";
    public static final String STEP_STATUS_EditStep = "Edit";
    public static final String STEP_STATUS_ApprovalEditStep = "Approval Edit";
    public static final String STEP_STATUS_ApprovalDeleteStep = "Approval Delete";
    public static final String STEP_STATUS_DeletedStep = "Deleted";
    // Action Names
    public static final String ACTION_NAME_Initialize = "Initialize";
    public static final String ACTION_NAME_Create = "Create";
    public static final String ACTION_NAME_Approve = "Approve";
    public static final String ACTION_NAME_Reject = "Reject";
    public static final String ACTION_NAME_Submit = "Submit";
    public static final String ACTION_NAME_Delete = "Delete";
    public static final String ACTION_NAME_Edit = "Edit";
    public static final String ACTION_NAME_Cancel = "Cancel";
    // Action Codes
    public static final String ACTION_CODE_InitializeInitialization = "1";
    public static final String ACTION_CODE_CreateInitialization = "2";
    public static final String ACTION_CODE_ApproveApprovalNewStep = "3";
    public static final String ACTION_CODE_RejectApprovalNewStep = "4";
    public static final String ACTION_CODE_SubmitRepairStep = "5";
    public static final String ACTION_CODE_DeleteRepairStep = "6";
    public static final String ACTION_CODE_ApproveApprovalRepairStep = "7";
    public static final String ACTION_CODE_RejectApprovalRepairStep = "8";
    public static final String ACTION_CODE_EditActiveStep = "9";
    public static final String ACTION_CODE_SubmitEditStep = "10";
    public static final String ACTION_CODE_CancelEditStep = "11";
    public static final String ACTION_CODE_DeleteEditStep = "12";
    public static final String ACTION_CODE_ApproveApprovalEditStep = "13";
    public static final String ACTION_CODE_RejectApprovalEditStep = "14";
    public static final String ACTION_CODE_ApproveApprovalDeleteStep = "15";
    public static final String ACTION_CODE_RejectApprovalDeleteStep = "16";
    // Action Keys
    public static final String ACTION_KEY_InitializeInitialization = "10";
    public static final String ACTION_KEY_CreateInitialization = "1";
    public static final String ACTION_KEY_ApproveApprovalNewStep = "640738070";
    public static final String ACTION_KEY_RejectApprovalNewStep = "1357460049";
    public static final String ACTION_KEY_SubmitRepairStep = "911490651";
    public static final String ACTION_KEY_DeleteRepairStep = "1913914304";
    public static final String ACTION_KEY_ApproveApprovalRepairStep = "575707730";
    public static final String ACTION_KEY_RejectApprovalRepairStep = "1176067470";
    public static final String ACTION_KEY_EditActiveStep = "1801842173";
    public static final String ACTION_KEY_SubmitEditStep = "1887449935";
    public static final String ACTION_KEY_CancelEditStep = "1692614994";
    public static final String ACTION_KEY_DeleteEditStep = "821910494";
    public static final String ACTION_KEY_ApproveApprovalEditStep = "1923215840";
    public static final String ACTION_KEY_RejectApprovalEditStep = "499591517";
    public static final String ACTION_KEY_ApproveApprovalDeleteStep = "244067457";
    public static final String ACTION_KEY_RejectApprovalDeleteStep = "1039726195";

}