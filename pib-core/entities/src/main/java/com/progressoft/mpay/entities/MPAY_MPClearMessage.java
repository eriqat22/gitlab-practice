package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_MPClearMessages")
@XmlRootElement(name="MPClearMessages")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_MPClearMessage extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_MPClearMessage(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String INTEG_MSG_TYPE = "integMsgType";
@Column(name="INTEGMSGTYPE", nullable=true, length=255)
private String integMsgType;
public String getIntegMsgType(){
return this.integMsgType;
}
public void setIntegMsgType(String integMsgType){
this.integMsgType = integMsgType;
}

public static final String SOURCE = "source";
@Column(name="SOURCE", nullable=false, length=1)
private String source;
public String getSource(){
return this.source;
}
public void setSource(String source){
this.source = source;
}

public static final String REF_SENDER = "refSender";
@Column(name="REFSENDER", nullable=true, length=255)
private String refSender;
public String getRefSender(){
return this.refSender;
}
public void setRefSender(String refSender){
this.refSender = refSender;
}

public static final String SIGNING_STAMP = "signingStamp";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="SIGNINGSTAMP", nullable=true, length=255)
private java.sql.Timestamp signingStamp;
public java.sql.Timestamp getSigningStamp(){
return this.signingStamp;
}
public void setSigningStamp(java.sql.Timestamp signingStamp){
this.signingStamp = signingStamp;
}

public static final String CONTENT = "content";
@Column(name="CONTENT", nullable=true, length=4000)
private String content;
public String getContent(){
return this.content;
}
public void setContent(String content){
this.content = content;
}

public static final String TOKEN = "token";
@Column(name="TOKEN", nullable=true, length=4000)
private String token;
public String getToken(){
return this.token;
}
public void setToken(String token){
this.token = token;
}

public static final String HINT = "hint";
@Column(name="HINT", nullable=true, length=255)
private String hint;
public String getHint(){
return this.hint;
}
public void setHint(String hint){
this.hint = hint;
}

public static final String MESSAGE_ID = "messageId";
@Column(name="MESSAGEID", nullable=true, length=50)
private String messageId;
public String getMessageId(){
return this.messageId;
}
public void setMessageId(String messageId){
this.messageId = messageId;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=true)
private MPAY_MpClearIntegRjctReason reason;
public MPAY_MpClearIntegRjctReason getReason(){
return this.reason;
}
public void setReason(MPAY_MpClearIntegRjctReason reason){
this.reason = reason;
}

public static final String REF_MESSAGE = "refMessage";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMESSAGEID", nullable=true)
private MPAY_MPayMessage refMessage;
public MPAY_MPayMessage getRefMessage(){
return this.refMessage;
}
public void setRefMessage(MPAY_MPayMessage refMessage){
this.refMessage = refMessage;
}

@Override
public String toString() {
return "MPAY_MPClearMessage [id= " + getId() + ", integMsgType= " + getIntegMsgType() + ", source= " + getSource() + ", refSender= " + getRefSender() + ", signingStamp= " + getSigningStamp() + ", content= " + getContent() + ", token= " + getToken() + ", hint= " + getHint() + ", messageId= " + getMessageId() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getIntegMsgType() == null) ? 0 : getIntegMsgType().hashCode());
result = prime * result + ((getRefSender() == null) ? 0 : getRefSender().hashCode());
result = prime * result + ((getHint() == null) ? 0 : getHint().hashCode());
result = prime * result + ((getMessageId() == null) ? 0 : getMessageId().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_MPClearMessage))
return false;
else {MPAY_MPClearMessage other = (MPAY_MPClearMessage) obj;
return this.hashCode() == other.hashCode();}
}


}