package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_CommissionParameters",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"REFSLICEID","NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="CommissionParameters")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CommissionParameter extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CommissionParameter(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String NAME = "name";
@Column(name="NAME", nullable=false, length=255)
private String name;
public String getName(){
return this.name;
}
public void setName(String name){
this.name = name;
}

public static final String VALUE = "value";
@Column(name="VALUE", nullable=false, length=1000)
private String value;
public String getValue(){
return this.value;
}
public void setValue(String value){
this.value = value;
}

public static final String REF_SLICE = "refSlice";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFSLICEID", nullable=true)
private MPAY_CommissionSlice refSlice;
public MPAY_CommissionSlice getRefSlice(){
return this.refSlice;
}
public void setRefSlice(MPAY_CommissionSlice refSlice){
this.refSlice = refSlice;
}

@Override
public String toString() {
return "MPAY_CommissionParameter [id= " + getId() + ", name= " + getName() + ", value= " + getValue() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getValue() == null) ? 0 : getValue().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CommissionParameter))
return false;
else {MPAY_CommissionParameter other = (MPAY_CommissionParameter) obj;
return this.hashCode() == other.hashCode();}
}


}