package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_MobileActivations",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"ACTIVATIONCODE","Z_TENANT_ID"})
})
@XmlRootElement(name="MobileActivations")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_MobileActivation extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_MobileActivation(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REQUESTING_MOBILE = "requestingMobile";
@Column(name="REQUESTINGMOBILE", nullable=false, length=255)
private String requestingMobile;
public String getRequestingMobile(){
return this.requestingMobile;
}
public void setRequestingMobile(String requestingMobile){
this.requestingMobile = requestingMobile;
}

public static final String ACTIVATION_CODE = "activationCode";
@Column(name="ACTIVATIONCODE", nullable=false, length=255)
private String activationCode;
public String getActivationCode(){
return this.activationCode;
}
public void setActivationCode(String activationCode){
this.activationCode = activationCode;
}

public static final String FROM_AGENT = "fromAgent";
@Column(name="FROMAGENT", nullable=false, length=255)
private Boolean fromAgent;
public Boolean getFromAgent(){
return this.fromAgent;
}
public void setFromAgent(Boolean fromAgent){
this.fromAgent = fromAgent;
}

@Override
public String toString() {
return "MPAY_MobileActivation [id= " + getId() + ", requestingMobile= " + getRequestingMobile() + ", activationCode= " + getActivationCode() + ", fromAgent= " + getFromAgent() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRequestingMobile() == null) ? 0 : getRequestingMobile().hashCode());
result = prime * result + ((getActivationCode() == null) ? 0 : getActivationCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_MobileActivation))
return false;
else {MPAY_MobileActivation other = (MPAY_MobileActivation) obj;
return this.hashCode() == other.hashCode();}
}


}