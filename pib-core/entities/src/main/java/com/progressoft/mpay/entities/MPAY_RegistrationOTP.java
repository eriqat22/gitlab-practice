package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity

@Table(name="MPAY_RegistrationOTPs")
@XmlRootElement(name="RegistrationOTPs")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_RegistrationOTP extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_RegistrationOTP(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CODE = "code";
@Column(name="CODE", nullable=false, length=255)
private String code;
public String getCode(){
return this.code;
}
public void setCode(String code){
this.code = code;
}

public static final String REQUEST_TIME = "requestTime";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REQUESTTIME", nullable=false, length=12)
private java.sql.Timestamp requestTime;
public java.sql.Timestamp getRequestTime(){
return this.requestTime;
}
public void setRequestTime(java.sql.Timestamp requestTime){
this.requestTime = requestTime;
}

public static final String _MOBILE = "Mobile";
@Column(name="MOBILE", nullable=false, length=255)
private String Mobile;
public String getMobile(){
return this.Mobile;
}
public void setMobile(String Mobile){
this.Mobile = Mobile;
}

@Override
public String toString() {
return "MPAY_RegistrationOTP [id= " + getId() + ", code= " + getCode() + ", requestTime= " + getRequestTime() + ", Mobile= " + getMobile() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_RegistrationOTP))
return false;
else {MPAY_RegistrationOTP other = (MPAY_RegistrationOTP) obj;
return this.hashCode() == other.hashCode();}
}


}