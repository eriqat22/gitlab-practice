package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_Service_Detail_Rep")
@XmlRootElement(name="Service_Detail_Rep")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Service_Detail_Rep extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Service_Detail_Rep(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String EXTRACT_STAMP = "extractStamp";
@Column(name="EXTRACTSTAMP", nullable=true, length=250)
private String extractStamp;
public String getExtractStamp(){
return this.extractStamp;
}
public void setExtractStamp(String extractStamp){
this.extractStamp = extractStamp;
}

public static final String CORPORATE_NAME = "corporateName";
@Column(name="CORPORATENAME", nullable=true, length=250)
private String corporateName;
public String getCorporateName(){
return this.corporateName;
}
public void setCorporateName(String corporateName){
this.corporateName = corporateName;
}

public static final String SERVICE_NAME = "serviceName";
@Column(name="SERVICENAME", nullable=true, length=250)
private String serviceName;
public String getServiceName(){
return this.serviceName;
}
public void setServiceName(String serviceName){
this.serviceName = serviceName;
}

public static final String STATUS = "status";
@Column(name="STATUS", nullable=true, length=250)
private String status;
public String getStatus(){
return this.status;
}
public void setStatus(String status){
this.status = status;
}

public static final String SERVICE_DESCRIPTION = "serviceDescription";
@Column(name="SERVICEDESCRIPTION", nullable=true, length=250)
private String serviceDescription;
public String getServiceDescription(){
return this.serviceDescription;
}
public void setServiceDescription(String serviceDescription){
this.serviceDescription = serviceDescription;
}

public static final String SERVICE_ALIAS = "serviceAlias";
@Column(name="SERVICEALIAS", nullable=true, length=250)
private String serviceAlias;
public String getServiceAlias(){
return this.serviceAlias;
}
public void setServiceAlias(String serviceAlias){
this.serviceAlias = serviceAlias;
}

public static final String BLOCKED = "blocked";
@Column(name="BLOCKED", nullable=true, length=250)
private String blocked;
public String getBlocked(){
return this.blocked;
}
public void setBlocked(String blocked){
this.blocked = blocked;
}

public static final String SERVICE_BANK_NAME = "serviceBankName";
@Column(name="SERVICEBANKNAME", nullable=true, length=250)
private String serviceBankName;
public String getServiceBankName(){
return this.serviceBankName;
}
public void setServiceBankName(String serviceBankName){
this.serviceBankName = serviceBankName;
}

public static final String SERVIER_BANK_NUMBER = "servierBankNumber";
@Column(name="SERVIERBANKNUMBER", nullable=true, length=250)
private String servierBankNumber;
public String getServierBankNumber(){
return this.servierBankNumber;
}
public void setServierBankNumber(String servierBankNumber){
this.servierBankNumber = servierBankNumber;
}

public static final String SERVICE_I_B_A_N = "serviceIBAN";
@Column(name="SERVICEIBAN", nullable=true, length=250)
private String serviceIBAN;
public String getServiceIBAN(){
return this.serviceIBAN;
}
public void setServiceIBAN(String serviceIBAN){
this.serviceIBAN = serviceIBAN;
}

public static final String SERVICE_M_P_A_Y_ACCOUNT = "serviceMPAYAccount";
@Column(name="SERVICEMPAYACCOUNT", nullable=true, length=250)
private String serviceMPAYAccount;
public String getServiceMPAYAccount(){
return this.serviceMPAYAccount;
}
public void setServiceMPAYAccount(String serviceMPAYAccount){
this.serviceMPAYAccount = serviceMPAYAccount;
}

public static final String SERVICE_BALANCE = "serviceBalance";
@Column(name="SERVICEBALANCE", nullable=true, length=250)
private String serviceBalance;
public String getServiceBalance(){
return this.serviceBalance;
}
public void setServiceBalance(String serviceBalance){
this.serviceBalance = serviceBalance;
}

public static final String PAYMENT_TYPE = "paymentType";
@Column(name="PAYMENTTYPE", nullable=true, length=250)
private String paymentType;
public String getPaymentType(){
return this.paymentType;
}
public void setPaymentType(String paymentType){
this.paymentType = paymentType;
}

public static final String REGISTERED = "registered";
@Column(name="REGISTERED", nullable=true, length=250)
private String registered;
public String getRegistered(){
return this.registered;
}
public void setRegistered(String registered){
this.registered = registered;
}

public static final String ACTIVE = "active";
@Column(name="ACTIVE", nullable=true, length=250)
private String active;
public String getActive(){
return this.active;
}
public void setActive(String active){
this.active = active;
}

public static final String NOTIFICATION_CHANNEL = "notificationChannel";
@Column(name="NOTIFICATIONCHANNEL", nullable=true, length=250)
private String notificationChannel;
public String getNotificationChannel(){
return this.notificationChannel;
}
public void setNotificationChannel(String notificationChannel){
this.notificationChannel = notificationChannel;
}

public static final String NOTIFICATION_RECEIVER = "notificationReceiver";
@Column(name="NOTIFICATIONRECEIVER", nullable=true, length=250)
private String notificationReceiver;
public String getNotificationReceiver(){
return this.notificationReceiver;
}
public void setNotificationReceiver(String notificationReceiver){
this.notificationReceiver = notificationReceiver;
}

public static final String PROFILE = "profile";
@Column(name="PROFILE", nullable=true, length=250)
private String profile;
public String getProfile(){
return this.profile;
}
public void setProfile(String profile){
this.profile = profile;
}

public static final String SERVICE_CATEGORY = "serviceCategory";
@Column(name="SERVICECATEGORY", nullable=true, length=250)
private String serviceCategory;
public String getServiceCategory(){
return this.serviceCategory;
}
public void setServiceCategory(String serviceCategory){
this.serviceCategory = serviceCategory;
}

public static final String SERVICE_TYPE = "serviceType";
@Column(name="SERVICETYPE", nullable=true, length=250)
private String serviceType;
public String getServiceType(){
return this.serviceType;
}
public void setServiceType(String serviceType){
this.serviceType = serviceType;
}

public static final String PIN_LAST_CHANGES = "pinLastChanges";
@Column(name="PINLASTCHANGES", nullable=true, length=250)
private String pinLastChanges;
public String getPinLastChanges(){
return this.pinLastChanges;
}
public void setPinLastChanges(String pinLastChanges){
this.pinLastChanges = pinLastChanges;
}

public static final String RETRY_COUNT = "retryCount";
@Column(name="RETRYCOUNT", nullable=true, length=250)
private String retryCount;
public String getRetryCount(){
return this.retryCount;
}
public void setRetryCount(String retryCount){
this.retryCount = retryCount;
}

public static final String LAST_TRANSACTIONDATE = "lastTransactiondate";
@Column(name="LASTTRANSACTIONDATE", nullable=true, length=250)
private String lastTransactiondate;
public String getLastTransactiondate(){
return this.lastTransactiondate;
}
public void setLastTransactiondate(String lastTransactiondate){
this.lastTransactiondate = lastTransactiondate;
}

public static final String LAST_TRANSACTION_TYPE = "lastTransactionType";
@Column(name="LASTTRANSACTIONTYPE", nullable=true, length=250)
private String lastTransactionType;
public String getLastTransactionType(){
return this.lastTransactionType;
}
public void setLastTransactionType(String lastTransactionType){
this.lastTransactionType = lastTransactionType;
}

public static final String LAST_TRANSACTION_AMOUNT = "lastTransactionAmount";
@Column(name="LASTTRANSACTIONAMOUNT", nullable=true, length=250)
private String lastTransactionAmount;
public String getLastTransactionAmount(){
return this.lastTransactionAmount;
}
public void setLastTransactionAmount(String lastTransactionAmount){
this.lastTransactionAmount = lastTransactionAmount;
}

public static final String CREATED_DATE = "createdDate";
@Column(name="CREATEDDATE", nullable=true, length=250)
private String createdDate;
public String getCreatedDate(){
return this.createdDate;
}
public void setCreatedDate(String createdDate){
this.createdDate = createdDate;
}

public static final String CREATED_USER = "createdUser";
@Column(name="CREATEDUSER", nullable=true, length=250)
private String createdUser;
public String getCreatedUser(){
return this.createdUser;
}
public void setCreatedUser(String createdUser){
this.createdUser = createdUser;
}

public static final String MODIFIED_DATE = "modifiedDate";
@Column(name="MODIFIEDDATE", nullable=true, length=250)
private String modifiedDate;
public String getModifiedDate(){
return this.modifiedDate;
}
public void setModifiedDate(String modifiedDate){
this.modifiedDate = modifiedDate;
}

public static final String MODIFIED_USER = "modifiedUser";
@Column(name="MODIFIEDUSER", nullable=true, length=250)
private String modifiedUser;
public String getModifiedUser(){
return this.modifiedUser;
}
public void setModifiedUser(String modifiedUser){
this.modifiedUser = modifiedUser;
}

public static final String DELETED_DATE = "deletedDate";
@Column(name="DELETEDDATE", nullable=true, length=250)
private String deletedDate;
public String getDeletedDate(){
return this.deletedDate;
}
public void setDeletedDate(String deletedDate){
this.deletedDate = deletedDate;
}

public static final String DELETED_USER = "deletedUser";
@Column(name="DELETEDUSER", nullable=true, length=250)
private String deletedUser;
public String getDeletedUser(){
return this.deletedUser;
}
public void setDeletedUser(String deletedUser){
this.deletedUser = deletedUser;
}

@Override
public String toString() {
return "MPAY_Service_Detail_Rep [id= " + getId() + ", extractStamp= " + getExtractStamp() + ", corporateName= " + getCorporateName() + ", serviceName= " + getServiceName() + ", status= " + getStatus() + ", serviceDescription= " + getServiceDescription() + ", serviceAlias= " + getServiceAlias() + ", blocked= " + getBlocked() + ", serviceBankName= " + getServiceBankName() + ", servierBankNumber= " + getServierBankNumber() + ", serviceIBAN= " + getServiceIBAN() + ", serviceMPAYAccount= " + getServiceMPAYAccount() + ", serviceBalance= " + getServiceBalance() + ", paymentType= " + getPaymentType() + ", registered= " + getRegistered() + ", active= " + getActive() + ", notificationChannel= " + getNotificationChannel() + ", notificationReceiver= " + getNotificationReceiver() + ", profile= " + getProfile() + ", serviceCategory= " + getServiceCategory() + ", serviceType= " + getServiceType() + ", pinLastChanges= " + getPinLastChanges() + ", retryCount= " + getRetryCount() + ", lastTransactiondate= " + getLastTransactiondate() + ", lastTransactionType= " + getLastTransactionType() + ", lastTransactionAmount= " + getLastTransactionAmount() + ", createdDate= " + getCreatedDate() + ", createdUser= " + getCreatedUser() + ", modifiedDate= " + getModifiedDate() + ", modifiedUser= " + getModifiedUser() + ", deletedDate= " + getDeletedDate() + ", deletedUser= " + getDeletedUser() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getExtractStamp() == null) ? 0 : getExtractStamp().hashCode());
result = prime * result + ((getCorporateName() == null) ? 0 : getCorporateName().hashCode());
result = prime * result + ((getServiceName() == null) ? 0 : getServiceName().hashCode());
result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
result = prime * result + ((getServiceDescription() == null) ? 0 : getServiceDescription().hashCode());
result = prime * result + ((getServiceAlias() == null) ? 0 : getServiceAlias().hashCode());
result = prime * result + ((getBlocked() == null) ? 0 : getBlocked().hashCode());
result = prime * result + ((getServiceBankName() == null) ? 0 : getServiceBankName().hashCode());
result = prime * result + ((getServierBankNumber() == null) ? 0 : getServierBankNumber().hashCode());
result = prime * result + ((getServiceIBAN() == null) ? 0 : getServiceIBAN().hashCode());
result = prime * result + ((getServiceMPAYAccount() == null) ? 0 : getServiceMPAYAccount().hashCode());
result = prime * result + ((getServiceBalance() == null) ? 0 : getServiceBalance().hashCode());
result = prime * result + ((getPaymentType() == null) ? 0 : getPaymentType().hashCode());
result = prime * result + ((getRegistered() == null) ? 0 : getRegistered().hashCode());
result = prime * result + ((getActive() == null) ? 0 : getActive().hashCode());
result = prime * result + ((getNotificationChannel() == null) ? 0 : getNotificationChannel().hashCode());
result = prime * result + ((getNotificationReceiver() == null) ? 0 : getNotificationReceiver().hashCode());
result = prime * result + ((getProfile() == null) ? 0 : getProfile().hashCode());
result = prime * result + ((getServiceCategory() == null) ? 0 : getServiceCategory().hashCode());
result = prime * result + ((getServiceType() == null) ? 0 : getServiceType().hashCode());
result = prime * result + ((getPinLastChanges() == null) ? 0 : getPinLastChanges().hashCode());
result = prime * result + ((getRetryCount() == null) ? 0 : getRetryCount().hashCode());
result = prime * result + ((getLastTransactiondate() == null) ? 0 : getLastTransactiondate().hashCode());
result = prime * result + ((getLastTransactionType() == null) ? 0 : getLastTransactionType().hashCode());
result = prime * result + ((getLastTransactionAmount() == null) ? 0 : getLastTransactionAmount().hashCode());
result = prime * result + ((getCreatedDate() == null) ? 0 : getCreatedDate().hashCode());
result = prime * result + ((getCreatedUser() == null) ? 0 : getCreatedUser().hashCode());
result = prime * result + ((getModifiedDate() == null) ? 0 : getModifiedDate().hashCode());
result = prime * result + ((getModifiedUser() == null) ? 0 : getModifiedUser().hashCode());
result = prime * result + ((getDeletedDate() == null) ? 0 : getDeletedDate().hashCode());
result = prime * result + ((getDeletedUser() == null) ? 0 : getDeletedUser().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Service_Detail_Rep))
return false;
else {MPAY_Service_Detail_Rep other = (MPAY_Service_Detail_Rep) obj;
return this.hashCode() == other.hashCode();}
}


}