package com.progressoft.mpay.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import com.progressoft.jfw.model.bussinessobject.attachments.AttachmentItem;

@Entity
@Table(name="MPAY_Intg_Cust_Reg_InstrATT")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Intg_Cust_Reg_InstrAtt extends AttachmentItem implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Intg_Cust_Reg_InstrAtt(){/*Default Constructor*/}


}