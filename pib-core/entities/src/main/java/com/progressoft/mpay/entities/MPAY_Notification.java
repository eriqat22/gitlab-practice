package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_Notifications")
@XmlRootElement(name="Notifications")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_Notification extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Notification(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String RECEIVER = "receiver";
@Column(name="RECEIVER", nullable=true, length=255)
private String receiver;
public String getReceiver(){
return this.receiver;
}
public void setReceiver(String receiver){
this.receiver = receiver;
}

public static final String CONTENT = "content";
@Column(name="CONTENT", nullable=false, length=4000)
private String content;
public String getContent(){
return this.content;
}
public void setContent(String content){
this.content = content;
}

public static final String SUCCESS = "success";
@Column(name="SUCCESS", nullable=true, length=1)
private Boolean success;
public Boolean getSuccess(){
return this.success;
}
public void setSuccess(Boolean success){
this.success = success;
}

public static final String ERROR_DESC = "errorDesc";
@Column(name="ERRORDESC", nullable=true, length=200)
private String errorDesc;
public String getErrorDesc(){
return this.errorDesc;
}
public void setErrorDesc(String errorDesc){
this.errorDesc = errorDesc;
}

public static final String EXTRA_DATA1 = "extraData1";
@Column(name="EXTRADATA1", nullable=true, length=1000)
private String extraData1;
public String getExtraData1(){
return this.extraData1;
}
public void setExtraData1(String extraData1){
this.extraData1 = extraData1;
}

public static final String EXTRA_DATA2 = "extraData2";
@Column(name="EXTRADATA2", nullable=true, length=1000)
private String extraData2;
public String getExtraData2(){
return this.extraData2;
}
public void setExtraData2(String extraData2){
this.extraData2 = extraData2;
}

public static final String EXTRA_DATA3 = "extraData3";
@Column(name="EXTRADATA3", nullable=true, length=1000)
private String extraData3;
public String getExtraData3(){
return this.extraData3;
}
public void setExtraData3(String extraData3){
this.extraData3 = extraData3;
}

public static final String REF_MESSAGE = "refMessage";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFMESSAGEID", nullable=true)
private MPAY_MPayMessage refMessage;
public MPAY_MPayMessage getRefMessage(){
return this.refMessage;
}
public void setRefMessage(MPAY_MPayMessage refMessage){
this.refMessage = refMessage;
}

public static final String REF_CHANNEL = "refChannel";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCHANNELID", nullable=false)
private MPAY_NotificationChannel refChannel;
public MPAY_NotificationChannel getRefChannel(){
return this.refChannel;
}
public void setRefChannel(MPAY_NotificationChannel refChannel){
this.refChannel = refChannel;
}

@Override
public String toString() {
return "MPAY_Notification [id= " + getId() + ", receiver= " + getReceiver() + ", content= " + getContent() + ", success= " + getSuccess() + ", errorDesc= " + getErrorDesc() + ", extraData1= " + getExtraData1() + ", extraData2= " + getExtraData2() + ", extraData3= " + getExtraData3() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getReceiver() == null) ? 0 : getReceiver().hashCode());
result = prime * result + ((getErrorDesc() == null) ? 0 : getErrorDesc().hashCode());
result = prime * result + ((getExtraData1() == null) ? 0 : getExtraData1().hashCode());
result = prime * result + ((getExtraData2() == null) ? 0 : getExtraData2().hashCode());
result = prime * result + ((getExtraData3() == null) ? 0 : getExtraData3().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Notification))
return false;
else {MPAY_Notification other = (MPAY_Notification) obj;
return this.hashCode() == other.hashCode();}
}


}