package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_Reg_Instrs_Stts")
@XmlRootElement(name="Reg_Instrs_Stts")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_Reg_Instrs_Stt extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Reg_Instrs_Stt(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CODE = "code";
@Column(name="CODE", nullable=false, length=12)
private String code;
public String getCode(){
return this.code;
}
public void setCode(String code){
this.code = code;
}

public static final String NAME = "name";
@Column(name="NAME", nullable=false, length=128)
private String name;
public String getName(){
return this.name;
}
public void setName(String name){
this.name = name;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String REG_INSTR_PROCESSING_STTS_INTG_CORP_REG_INSTRS = "regInstrProcessingSttsIntg_Corp_Reg_Instrs";
@OneToMany(mappedBy = "regInstrProcessingStts")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Intg_Corp_Reg_Instr> regInstrProcessingSttsIntg_Corp_Reg_Instrs;
public List<MPAY_Intg_Corp_Reg_Instr> getRegInstrProcessingSttsIntgCorpRegInstrs(){
return this.regInstrProcessingSttsIntg_Corp_Reg_Instrs;
}
public void setRegInstrProcessingSttsIntgCorpRegInstrs(List<MPAY_Intg_Corp_Reg_Instr> regInstrProcessingSttsIntg_Corp_Reg_Instrs){
this.regInstrProcessingSttsIntg_Corp_Reg_Instrs = regInstrProcessingSttsIntg_Corp_Reg_Instrs;
}

public static final String REG_INSTR_PROCESSING_STTS_INTG_CUST_REG_INSTRS = "regInstrProcessingSttsIntg_Cust_Reg_Instrs";
@OneToMany(mappedBy = "regInstrProcessingStts")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Intg_Cust_Reg_Instr> regInstrProcessingSttsIntg_Cust_Reg_Instrs;
public List<MPAY_Intg_Cust_Reg_Instr> getRegInstrProcessingSttsIntgCustRegInstrs(){
return this.regInstrProcessingSttsIntg_Cust_Reg_Instrs;
}
public void setRegInstrProcessingSttsIntgCustRegInstrs(List<MPAY_Intg_Cust_Reg_Instr> regInstrProcessingSttsIntg_Cust_Reg_Instrs){
this.regInstrProcessingSttsIntg_Cust_Reg_Instrs = regInstrProcessingSttsIntg_Cust_Reg_Instrs;
}

@Override
public String toString() {
return "MPAY_Reg_Instrs_Stt [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Reg_Instrs_Stt))
return false;
else {MPAY_Reg_Instrs_Stt other = (MPAY_Reg_Instrs_Stt) obj;
return this.hashCode() == other.hashCode();}
}


}