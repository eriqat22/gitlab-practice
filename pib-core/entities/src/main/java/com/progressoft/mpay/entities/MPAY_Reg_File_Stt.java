package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_Reg_File_Stts",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="Reg_File_Stts")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_Reg_File_Stt extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Reg_File_Stt(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String CODE = "code";
@Column(name="CODE", nullable=false, length=12)
private String code;
public String getCode(){
return this.code;
}
public void setCode(String code){
this.code = code;
}

public static final String NAME = "name";
@Column(name="NAME", nullable=false, length=128)
private String name;
public String getName(){
return this.name;
}
public void setName(String name){
this.name = name;
}

public static final String DESCRIPTION = "description";
@Column(name="DESCRIPTION", nullable=true, length=255)
private String description;
public String getDescription(){
return this.description;
}
public void setDescription(String description){
this.description = description;
}

public static final String REG_FILE_PROCESSING_STTS_INTG_REG_FILES = "regFileProcessingSttsIntg_Reg_Files";
@OneToMany(mappedBy = "regFileProcessingStts")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Intg_Reg_File> regFileProcessingSttsIntg_Reg_Files;
public List<MPAY_Intg_Reg_File> getRegFileProcessingSttsIntgRegFiles(){
return this.regFileProcessingSttsIntg_Reg_Files;
}
public void setRegFileProcessingSttsIntgRegFiles(List<MPAY_Intg_Reg_File> regFileProcessingSttsIntg_Reg_Files){
this.regFileProcessingSttsIntg_Reg_Files = regFileProcessingSttsIntg_Reg_Files;
}

@Override
public String toString() {
return "MPAY_Reg_File_Stt [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Reg_File_Stt))
return false;
else {MPAY_Reg_File_Stt other = (MPAY_Reg_File_Stt) obj;
return this.hashCode() == other.hashCode();}
}


}