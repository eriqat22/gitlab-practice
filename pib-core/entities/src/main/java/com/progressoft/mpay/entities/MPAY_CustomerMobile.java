package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_CustomerMobiles")
@XmlRootElement(name="CustomerMobiles")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_CustomerMobile extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_CustomerMobile(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String MOBILE_NUMBER = "mobileNumber";
@Column(name="MOBILENUMBER", nullable=false, length=20)
private String mobileNumber;
public String getMobileNumber(){
return this.mobileNumber;
}
public void setMobileNumber(String mobileNumber){
this.mobileNumber = mobileNumber;
}

public static final String NEW_MOBILE_NUMBER = "newMobileNumber";
@Column(name="NEWMOBILENUMBER", nullable=true, length=20)
private String newMobileNumber;
public String getNewMobileNumber(){
return this.newMobileNumber;
}
public void setNewMobileNumber(String newMobileNumber){
this.newMobileNumber = newMobileNumber;
}

public static final String ALIAS = "alias";
@Column(name="ALIAS", nullable=true, length=25)
private String alias;
public String getAlias(){
return this.alias;
}
public void setAlias(String alias){
this.alias = alias;
}

public static final String NEW_ALIAS = "newAlias";
@Column(name="NEWALIAS", nullable=true, length=25)
private String newAlias;
public String getNewAlias(){
return this.newAlias;
}
public void setNewAlias(String newAlias){
this.newAlias = newAlias;
}

public static final String EMAIL = "email";
@Column(name="EMAIL", nullable=true, length=250)
private String email;
public String getEmail(){
return this.email;
}
public void setEmail(String email){
this.email = email;
}

public static final String ENABLE_S_M_S = "enableSMS";
@Column(name="ENABLESMS", nullable=true, length=10)
private Boolean enableSMS;
public Boolean getEnableSMS(){
return this.enableSMS;
}
public void setEnableSMS(Boolean enableSMS){
this.enableSMS = enableSMS;
}

public static final String ENABLE_EMAIL = "enableEmail";
@Column(name="ENABLEEMAIL", nullable=true, length=10)
private Boolean enableEmail;
public Boolean getEnableEmail(){
return this.enableEmail;
}
public void setEnableEmail(Boolean enableEmail){
this.enableEmail = enableEmail;
}

public static final String ENABLE_PUSH_NOTIFICATION = "enablePushNotification";
@Column(name="ENABLEPUSHNOTIFICATION", nullable=true, length=10)
private Boolean enablePushNotification;
public Boolean getEnablePushNotification(){
return this.enablePushNotification;
}
public void setEnablePushNotification(Boolean enablePushNotification){
this.enablePushNotification = enablePushNotification;
}

public static final String NOTIFICATION_SHOW_TYPE = "notificationShowType";
@Column(name="NOTIFICATIONSHOWTYPE", nullable=false, length=50)
private String notificationShowType;
public String getNotificationShowType(){
return this.notificationShowType;
}
public void setNotificationShowType(String notificationShowType){
this.notificationShowType = notificationShowType;
}

public static final String NFC_SERIAL = "nfcSerial";
@Column(name="NFCSERIAL", nullable=true, length=255)
private String nfcSerial;
public String getNfcSerial(){
return this.nfcSerial;
}
public void setNfcSerial(String nfcSerial){
this.nfcSerial = nfcSerial;
}

public static final String MAX_NUMBER_OF_DEVICES = "maxNumberOfDevices";
@Column(name="MAXNUMBEROFDEVICES", nullable=false, length=10)
private Long maxNumberOfDevices;
public Long getMaxNumberOfDevices(){
return this.maxNumberOfDevices;
}
public void setMaxNumberOfDevices(Long maxNumberOfDevices){
this.maxNumberOfDevices = maxNumberOfDevices;
}

public static final String ACTIVATION_CODE_VALIDY = "activationCodeValidy";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="ACTIVATIONCODEVALIDY", nullable=true, length=12)
private java.sql.Timestamp activationCodeValidy;
public java.sql.Timestamp getActivationCodeValidy(){
return this.activationCodeValidy;
}
public void setActivationCodeValidy(java.sql.Timestamp activationCodeValidy){
this.activationCodeValidy = activationCodeValidy;
}

public static final String PROMO_CODE = "promoCode";
@Column(name="PROMOCODE", nullable=true, length=100)
private String promoCode;
public String getPromoCode(){
return this.promoCode;
}
public void setPromoCode(String promoCode){
this.promoCode = promoCode;
}

public static final String CHANGES = "changes";
@Column(name="CHANGES", nullable=true, length=1000)
@Transient
private String changes;
public String getChanges(){
return this.changes;
}
public void setChanges(String changes){
this.changes = changes;
}

public static final String RETRY_COUNT = "retryCount";
@Column(name="RETRYCOUNT", nullable=false, length=4)
private Long retryCount;
public Long getRetryCount(){
return this.retryCount;
}
public void setRetryCount(Long retryCount){
this.retryCount = retryCount;
}

public static final String PIN_LAST_CHANGED = "pinLastChanged";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="PINLASTCHANGED", nullable=true, length=12)
private java.sql.Timestamp pinLastChanged;
public java.sql.Timestamp getPinLastChanged(){
return this.pinLastChanged;
}
public void setPinLastChanged(java.sql.Timestamp pinLastChanged){
this.pinLastChanged = pinLastChanged;
}

public static final String IS_BLOCKED = "isBlocked";
@Column(name="ISBLOCKED", nullable=false, length=1)
private Boolean isBlocked;
public Boolean getIsBlocked(){
return this.isBlocked;
}
public void setIsBlocked(Boolean isBlocked){
this.isBlocked = isBlocked;
}

public static final String IS_ACTIVE = "isActive";
@Column(name="ISACTIVE", nullable=false, length=1)
private Boolean isActive;
public Boolean getIsActive(){
return this.isActive;
}
public void setIsActive(Boolean isActive){
this.isActive = isActive;
}

public static final String IS_REGISTERED = "isRegistered";
@Column(name="ISREGISTERED", nullable=true, length=1)
private Boolean isRegistered;
public Boolean getIsRegistered(){
return this.isRegistered;
}
public void setIsRegistered(Boolean isRegistered){
this.isRegistered = isRegistered;
}

public static final String REJECTION_NOTE = "rejectionNote";
@Column(name="REJECTIONNOTE", nullable=true, length=255)
private String rejectionNote;
public String getRejectionNote(){
return this.rejectionNote;
}
public void setRejectionNote(String rejectionNote){
this.rejectionNote = rejectionNote;
}

public static final String APPROVAL_NOTE = "approvalNote";
@Column(name="APPROVALNOTE", nullable=true, length=255)
private String approvalNote;
public String getApprovalNote(){
return this.approvalNote;
}
public void setApprovalNote(String approvalNote){
this.approvalNote = approvalNote;
}

public static final String BANKED_UNBANKED = "bankedUnbanked";
@Column(name="BANKEDUNBANKED", nullable=false, length=20)
private String bankedUnbanked;
public String getBankedUnbanked(){
return this.bankedUnbanked;
}
public void setBankedUnbanked(String bankedUnbanked){
this.bankedUnbanked = bankedUnbanked;
}

public static final String BANK_BRANCH = "bankBranch";
@Column(name="BANKBRANCH", nullable=true, length=50)
private String bankBranch;
public String getBankBranch(){
return this.bankBranch;
}
public void setBankBranch(String bankBranch){
this.bankBranch = bankBranch;
}

public static final String EXTERNAL_ACC = "externalAcc";
@Column(name="EXTERNALACC", nullable=true, length=100)
private String externalAcc;
public String getExternalAcc(){
return this.externalAcc;
}
public void setExternalAcc(String externalAcc){
this.externalAcc = externalAcc;
}

public static final String IBAN = "iban";
@Column(name="IBAN", nullable=true, length=100)
private String iban;
public String getIban(){
return this.iban;
}
public void setIban(String iban){
this.iban = iban;
}

public static final String MAS = "mas";
@Column(name="MAS", nullable=false, length=10)
private Long mas;
public Long getMas(){
return this.mas;
}
public void setMas(Long mas){
this.mas = mas;
}

public static final String PIN = "pin";
@Column(name="PIN", nullable=true, length=255)
private String pin;
public String getPin(){
return this.pin;
}
public void setPin(String pin){
this.pin = pin;
}

public static final String BALANCE_THRESHOLD = "balanceThreshold";
@Column(name="BALANCETHRESHOLD", nullable=true, length=50)
private Float balanceThreshold;
public Float getBalanceThreshold(){
return this.balanceThreshold;
}
public void setBalanceThreshold(Float balanceThreshold){
this.balanceThreshold = balanceThreshold;
}

public static final String DAILY_BALANCE_TIME = "dailyBalanceTime";
@Column(name="DAILYBALANCETIME", nullable=true, length=2)
private String dailyBalanceTime;
public String getDailyBalanceTime(){
return this.dailyBalanceTime;
}
public void setDailyBalanceTime(String dailyBalanceTime){
this.dailyBalanceTime = dailyBalanceTime;
}

public static final String HINT = "hint";
@Column(name="HINT", nullable=true, length=255)
private String hint;
public String getHint(){
return this.hint;
}
public void setHint(String hint){
this.hint = hint;
}

public static final String ACTIVATION_CODE = "activationCode";
@Column(name="ACTIVATIONCODE", nullable=true, length=255)
private String activationCode;
public String getActivationCode(){
return this.activationCode;
}
public void setActivationCode(String activationCode){
this.activationCode = activationCode;
}

public static final String SESSION_ID = "sessionId";
@Column(name="SESSIONID", nullable=true, length=1000)
private String sessionId;
public String getSessionId(){
return this.sessionId;
}
public void setSessionId(String sessionId){
this.sessionId = sessionId;
}

public static final String EXTRA_DATA = "extraData";
@Column(name="EXTRADATA", nullable=true, length=1000)
private String extraData;
public String getExtraData(){
return this.extraData;
}
public void setExtraData(String extraData){
this.extraData = extraData;
}

public static final String APPROVED_DATA = "approvedData";
@Column(name="APPROVEDDATA", nullable=true, length=4000)
private String approvedData;
public String getApprovedData(){
return this.approvedData;
}
public void setApprovedData(String approvedData){
this.approvedData = approvedData;
}

public static final String REF_CUSTOMER = "refCustomer";
@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE, CascadeType.MERGE })
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFCUSTOMERID", nullable=false)
private MPAY_Customer refCustomer;
@flexjson.JSON(include = false)
public MPAY_Customer getRefCustomer(){
return this.refCustomer;
}
public void setRefCustomer(MPAY_Customer refCustomer){
this.refCustomer = refCustomer;
}

public static final String BANK = "bank";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="BANKID", nullable=false)
private MPAY_Bank bank;
public MPAY_Bank getBank(){
return this.bank;
}
public void setBank(MPAY_Bank bank){
this.bank = bank;
}

public static final String REF_PROFILE = "refProfile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFPROFILEID", nullable=false)
private MPAY_Profile refProfile;
public MPAY_Profile getRefProfile(){
return this.refProfile;
}
public void setRefProfile(MPAY_Profile refProfile){
this.refProfile = refProfile;
}

public static final String CHANNEL_TYPE = "channelType";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="CHANNELTYPEID", nullable=true)
private MPAY_ChannelType channelType;
public MPAY_ChannelType getChannelType(){
return this.channelType;
}
public void setChannelType(MPAY_ChannelType channelType){
this.channelType = channelType;
}

public static final String REFERRAL_ID = "referralId";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFERRALID", nullable=true)
private MPAY_CustomerMobile referralId;
public MPAY_CustomerMobile getReferralId(){
return this.referralId;
}
public void setReferralId(MPAY_CustomerMobile referralId){
this.referralId = referralId;
}

public static final String SENDER_MOBILE_TRANSACTIONS = "senderMobileTransactions";
@OneToMany(mappedBy = "senderMobile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> senderMobileTransactions;
public List<MPAY_Transaction> getSenderMobileTransactions(){
return this.senderMobileTransactions;
}
public void setSenderMobileTransactions(List<MPAY_Transaction> senderMobileTransactions){
this.senderMobileTransactions = senderMobileTransactions;
}

public static final String RECEIVER_MOBILE_TRANSACTIONS = "receiverMobileTransactions";
@OneToMany(mappedBy = "receiverMobile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Transaction> receiverMobileTransactions;
public List<MPAY_Transaction> getReceiverMobileTransactions(){
return this.receiverMobileTransactions;
}
public void setReceiverMobileTransactions(List<MPAY_Transaction> receiverMobileTransactions){
this.receiverMobileTransactions = receiverMobileTransactions;
}

public static final String REF_MOBILE_A_T_M_VOUCHERS = "refMobileATM_Vouchers";
@OneToMany(mappedBy = "refMobile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ATM_Voucher> refMobileATM_Vouchers;
public List<MPAY_ATM_Voucher> getRefMobileATMVouchers(){
return this.refMobileATM_Vouchers;
}
public void setRefMobileATMVouchers(List<MPAY_ATM_Voucher> refMobileATM_Vouchers){
this.refMobileATM_Vouchers = refMobileATM_Vouchers;
}

public static final String SENDER_MOBILE_CLIENTS_O_T_PS = "senderMobileClientsOTPs";
@OneToMany(mappedBy = "senderMobile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_ClientsOTP> senderMobileClientsOTPs;
public List<MPAY_ClientsOTP> getSenderMobileClientsOTPs(){
return this.senderMobileClientsOTPs;
}
public void setSenderMobileClientsOTPs(List<MPAY_ClientsOTP> senderMobileClientsOTPs){
this.senderMobileClientsOTPs = senderMobileClientsOTPs;
}

public static final String REF_CUST_MOB_CASH_IN = "refCustMobCashIn";
@OneToMany(mappedBy = "refCustMob")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CashIn> refCustMobCashIn;
public List<MPAY_CashIn> getRefCustMobCashIn(){
return this.refCustMobCashIn;
}
public void setRefCustMobCashIn(List<MPAY_CashIn> refCustMobCashIn){
this.refCustMobCashIn = refCustMobCashIn;
}

public static final String REFERRAL_ID_CUSTOMER_MOBILES = "referralIdCustomerMobiles";
@OneToMany(mappedBy = "referralId")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CustomerMobile> referralIdCustomerMobiles;
public List<MPAY_CustomerMobile> getReferralIdCustomerMobiles(){
return this.referralIdCustomerMobiles;
}
public void setReferralIdCustomerMobiles(List<MPAY_CustomerMobile> referralIdCustomerMobiles){
this.referralIdCustomerMobiles = referralIdCustomerMobiles;
}

public static final String MOBILE_MOBILE_ACCOUNTS = "mobileMobileAccounts";
@OneToMany(mappedBy = "mobile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_MobileAccount> mobileMobileAccounts;
public List<MPAY_MobileAccount> getMobileMobileAccounts(){
return this.mobileMobileAccounts;
}
public void setMobileMobileAccounts(List<MPAY_MobileAccount> mobileMobileAccounts){
this.mobileMobileAccounts = mobileMobileAccounts;
}

public static final String REF_MOBILE_FAVORITE_MOBILES = "refMobileFavoriteMobiles";
@OneToMany(mappedBy = "refMobile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_FavoriteMobile> refMobileFavoriteMobiles;
public List<MPAY_FavoriteMobile> getRefMobileFavoriteMobiles(){
return this.refMobileFavoriteMobiles;
}
public void setRefMobileFavoriteMobiles(List<MPAY_FavoriteMobile> refMobileFavoriteMobiles){
this.refMobileFavoriteMobiles = refMobileFavoriteMobiles;
}

public static final String REF_CUSTOMER_MOBILE_CUSTOMER_DEVICES = "refCustomerMobileCustomerDevices";
@OneToMany(mappedBy = "refCustomerMobile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CustomerDevice> refCustomerMobileCustomerDevices;
public List<MPAY_CustomerDevice> getRefCustomerMobileCustomerDevices(){
return this.refCustomerMobileCustomerDevices;
}
public void setRefCustomerMobileCustomerDevices(List<MPAY_CustomerDevice> refCustomerMobileCustomerDevices){
this.refCustomerMobileCustomerDevices = refCustomerMobileCustomerDevices;
}

public static final String REF_MOBILE_CUST_INTEG_MESSAGES = "refMobileCustIntegMessages";
@OneToMany(mappedBy = "refMobile")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CustIntegMessage> refMobileCustIntegMessages;
public List<MPAY_CustIntegMessage> getRefMobileCustIntegMessages(){
return this.refMobileCustIntegMessages;
}
public void setRefMobileCustIntegMessages(List<MPAY_CustIntegMessage> refMobileCustIntegMessages){
this.refMobileCustIntegMessages = refMobileCustIntegMessages;
}

public static final String REF_CUST_MOB_CASH_OUT = "refCustMobCashOut";
@OneToMany(mappedBy = "refCustMob")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_CashOut> refCustMobCashOut;
public List<MPAY_CashOut> getRefCustMobCashOut(){
return this.refCustMobCashOut;
}
public void setRefCustMobCashOut(List<MPAY_CashOut> refCustMobCashOut){
this.refCustMobCashOut = refCustMobCashOut;
}

@Override
public String toString() {
return "MPAY_CustomerMobile [id= " + getId() + ", mobileNumber= " + getMobileNumber() + ", newMobileNumber= " + getNewMobileNumber() + ", alias= " + getAlias() + ", newAlias= " + getNewAlias() + ", email= " + getEmail() + ", enableSMS= " + getEnableSMS() + ", enableEmail= " + getEnableEmail() + ", enablePushNotification= " + getEnablePushNotification() + ", notificationShowType= " + getNotificationShowType() + ", nfcSerial= " + getNfcSerial() + ", maxNumberOfDevices= " + getMaxNumberOfDevices() + ", activationCodeValidy= " + getActivationCodeValidy() + ", promoCode= " + getPromoCode() + ", changes= " + getChanges() + ", retryCount= " + getRetryCount() + ", pinLastChanged= " + getPinLastChanged() + ", isBlocked= " + getIsBlocked() + ", isActive= " + getIsActive() + ", isRegistered= " + getIsRegistered() + ", rejectionNote= " + getRejectionNote() + ", approvalNote= " + getApprovalNote() + ", bankedUnbanked= " + getBankedUnbanked() + ", bankBranch= " + getBankBranch() + ", externalAcc= " + getExternalAcc() + ", iban= " + getIban() + ", mas= " + getMas() + ", pin= " + getPin() + ", balanceThreshold= " + getBalanceThreshold() + ", dailyBalanceTime= " + getDailyBalanceTime() + ", hint= " + getHint() + ", activationCode= " + getActivationCode() + ", sessionId= " + getSessionId() + ", extraData= " + getExtraData() + ", approvedData= " + getApprovedData() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getMobileNumber() == null) ? 0 : getMobileNumber().hashCode());
result = prime * result + ((getNewMobileNumber() == null) ? 0 : getNewMobileNumber().hashCode());
result = prime * result + ((getAlias() == null) ? 0 : getAlias().hashCode());
result = prime * result + ((getNewAlias() == null) ? 0 : getNewAlias().hashCode());
result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
result = prime * result + ((getNfcSerial() == null) ? 0 : getNfcSerial().hashCode());
int MaxNumberOfDevices= new Long("null".equals(getMaxNumberOfDevices() + "") ? 0 : getMaxNumberOfDevices()).intValue();
result = prime * result + (int) (MaxNumberOfDevices ^ MaxNumberOfDevices >>> 32);
result = prime * result + ((getPromoCode() == null) ? 0 : getPromoCode().hashCode());
result = prime * result + ((getChanges() == null) ? 0 : getChanges().hashCode());
int RetryCount= new Long("null".equals(getRetryCount() + "") ? 0 : getRetryCount()).intValue();
result = prime * result + (int) (RetryCount ^ RetryCount >>> 32);
result = prime * result + ((getBankBranch() == null) ? 0 : getBankBranch().hashCode());
result = prime * result + ((getExternalAcc() == null) ? 0 : getExternalAcc().hashCode());
result = prime * result + ((getIban() == null) ? 0 : getIban().hashCode());
int Mas= new Long("null".equals(getMas() + "") ? 0 : getMas()).intValue();
result = prime * result + (int) (Mas ^ Mas >>> 32);
result = prime * result + ((getPin() == null) ? 0 : getPin().hashCode());
result = prime * result + ((getBalanceThreshold() == null) ? 0 : getBalanceThreshold().hashCode());
result = prime * result + ((getDailyBalanceTime() == null) ? 0 : getDailyBalanceTime().hashCode());
result = prime * result + ((getHint() == null) ? 0 : getHint().hashCode());
result = prime * result + ((getActivationCode() == null) ? 0 : getActivationCode().hashCode());
result = prime * result + ((getSessionId() == null) ? 0 : getSessionId().hashCode());
result = prime * result + ((getExtraData() == null) ? 0 : getExtraData().hashCode());
result = prime * result + ((getApprovedData() == null) ? 0 : getApprovedData().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_CustomerMobile))
return false;
else {MPAY_CustomerMobile other = (MPAY_CustomerMobile) obj;
return this.hashCode() == other.hashCode();}
}


}