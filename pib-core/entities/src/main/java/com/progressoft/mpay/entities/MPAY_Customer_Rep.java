package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;

@Entity

@Table(name="MPAY_Customer_Rep")
@XmlRootElement(name="Customer_Rep")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Customer_Rep extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Customer_Rep(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String EXTRACT_STAMP = "extractStamp";
@Column(name="EXTRACTSTAMP", nullable=true, length=250)
private String extractStamp;
public String getExtractStamp(){
return this.extractStamp;
}
public void setExtractStamp(String extractStamp){
this.extractStamp = extractStamp;
}

public static final String CUSTOMER_NAME = "customerName";
@Column(name="CUSTOMERNAME", nullable=true, length=250)
private String customerName;
public String getCustomerName(){
return this.customerName;
}
public void setCustomerName(String customerName){
this.customerName = customerName;
}

public static final String CUSTOMER_TYPE = "customerType";
@Column(name="CUSTOMERTYPE", nullable=true, length=250)
private String customerType;
public String getCustomerType(){
return this.customerType;
}
public void setCustomerType(String customerType){
this.customerType = customerType;
}

public static final String STATUS = "status";
@Column(name="STATUS", nullable=true, length=250)
private String status;
public String getStatus(){
return this.status;
}
public void setStatus(String status){
this.status = status;
}

public static final String ACTIVE = "active";
@Column(name="ACTIVE", nullable=true, length=250)
private String active;
public String getActive(){
return this.active;
}
public void setActive(String active){
this.active = active;
}

public static final String NIN = "nin";
@Column(name="NIN", nullable=true, length=250)
private String nin;
public String getNin(){
return this.nin;
}
public void setNin(String nin){
this.nin = nin;
}

public static final String NATIONALITY = "nationality";
@Column(name="NATIONALITY", nullable=true, length=250)
private String nationality;
public String getNationality(){
return this.nationality;
}
public void setNationality(String nationality){
this.nationality = nationality;
}

public static final String IDEXPIRYDATE = "idexpirydate";
@Column(name="IDEXPIRYDATE", nullable=true, length=250)
private String idexpirydate;
public String getIdexpirydate(){
return this.idexpirydate;
}
public void setIdexpirydate(String idexpirydate){
this.idexpirydate = idexpirydate;
}

public static final String I_D_TYPE = "iDType";
@Column(name="IDTYPE", nullable=true, length=250)
private String iDType;
public String getIDType(){
return this.iDType;
}
public void setIDType(String iDType){
this.iDType = iDType;
}

public static final String DOB = "dob";
@Column(name="DOB", nullable=true, length=250)
private String dob;
public String getDob(){
return this.dob;
}
public void setDob(String dob){
this.dob = dob;
}

public static final String CUSTOMER_MOBILE = "customerMobile";
@Column(name="CUSTOMERMOBILE", nullable=true, length=250)
private String customerMobile;
public String getCustomerMobile(){
return this.customerMobile;
}
public void setCustomerMobile(String customerMobile){
this.customerMobile = customerMobile;
}

public static final String IBAN = "iban";
@Column(name="IBAN", nullable=true, length=250)
private String iban;
public String getIban(){
return this.iban;
}
public void setIban(String iban){
this.iban = iban;
}

public static final String ACCOUNT_NUMBER = "accountNumber";
@Column(name="ACCOUNTNUMBER", nullable=true, length=250)
private String accountNumber;
public String getAccountNumber(){
return this.accountNumber;
}
public void setAccountNumber(String accountNumber){
this.accountNumber = accountNumber;
}

public static final String BANK_ACC_NUMBER = "bankAccNumber";
@Column(name="BANKACCNUMBER", nullable=true, length=250)
private String bankAccNumber;
public String getBankAccNumber(){
return this.bankAccNumber;
}
public void setBankAccNumber(String bankAccNumber){
this.bankAccNumber = bankAccNumber;
}

public static final String BALANCES = "balances";
@Column(name="BALANCES", nullable=true, length=250)
private String balances;
public String getBalances(){
return this.balances;
}
public void setBalances(String balances){
this.balances = balances;
}

public static final String _LANGUAGEINDICATOR = "Languageindicator";
@Column(name="LANGUAGEINDICATOR", nullable=true, length=250)
private String Languageindicator;
public String getLanguageindicator(){
return this.Languageindicator;
}
public void setLanguageindicator(String Languageindicator){
this.Languageindicator = Languageindicator;
}

public static final String NOTIFICATION_CHANNEL = "notificationChannel";
@Column(name="NOTIFICATIONCHANNEL", nullable=true, length=250)
private String notificationChannel;
public String getNotificationChannel(){
return this.notificationChannel;
}
public void setNotificationChannel(String notificationChannel){
this.notificationChannel = notificationChannel;
}

public static final String DEVICE_ID = "deviceId";
@Column(name="DEVICEID", nullable=true, length=250)
private String deviceId;
public String getDeviceId(){
return this.deviceId;
}
public void setDeviceId(String deviceId){
this.deviceId = deviceId;
}

public static final String DEVICE_NAME = "deviceName";
@Column(name="DEVICENAME", nullable=true, length=250)
private String deviceName;
public String getDeviceName(){
return this.deviceName;
}
public void setDeviceName(String deviceName){
this.deviceName = deviceName;
}

public static final String DEVICE_STATUS = "deviceStatus";
@Column(name="DEVICESTATUS", nullable=true, length=250)
private String deviceStatus;
public String getDeviceStatus(){
return this.deviceStatus;
}
public void setDeviceStatus(String deviceStatus){
this.deviceStatus = deviceStatus;
}

public static final String DEVICE_ACTIVE = "deviceActive";
@Column(name="DEVICEACTIVE", nullable=true, length=250)
private String deviceActive;
public String getDeviceActive(){
return this.deviceActive;
}
public void setDeviceActive(String deviceActive){
this.deviceActive = deviceActive;
}

public static final String DEVICE_STOLEN = "deviceStolen";
@Column(name="DEVICESTOLEN", nullable=true, length=250)
private String deviceStolen;
public String getDeviceStolen(){
return this.deviceStolen;
}
public void setDeviceStolen(String deviceStolen){
this.deviceStolen = deviceStolen;
}

public static final String DEVICE_RETRY = "deviceRetry";
@Column(name="DEVICERETRY", nullable=true, length=250)
private String deviceRetry;
public String getDeviceRetry(){
return this.deviceRetry;
}
public void setDeviceRetry(String deviceRetry){
this.deviceRetry = deviceRetry;
}

public static final String MAXIMUM_NUMBER_OF_DEVICES = "maximumNumberOfDevices";
@Column(name="MAXIMUMNUMBEROFDEVICES", nullable=true, length=250)
private String maximumNumberOfDevices;
public String getMaximumNumberOfDevices(){
return this.maximumNumberOfDevices;
}
public void setMaximumNumberOfDevices(String maximumNumberOfDevices){
this.maximumNumberOfDevices = maximumNumberOfDevices;
}

public static final String _LAST_LOGON_DATE = "LastLogonDate";
@Column(name="LASTLOGONDATE", nullable=true, length=250)
private String LastLogonDate;
public String getLastLogonDate(){
return this.LastLogonDate;
}
public void setLastLogonDate(String LastLogonDate){
this.LastLogonDate = LastLogonDate;
}

public static final String CUSTOMER_ALLADDRESS_DETAILS = "customerAlladdressDetails";
@Column(name="CUSTOMERALLADDRESSDETAILS", nullable=true, length=250)
private String customerAlladdressDetails;
public String getCustomerAlladdressDetails(){
return this.customerAlladdressDetails;
}
public void setCustomerAlladdressDetails(String customerAlladdressDetails){
this.customerAlladdressDetails = customerAlladdressDetails;
}

public static final String GENDER = "gender";
@Column(name="GENDER", nullable=true, length=250)
private String gender;
public String getGender(){
return this.gender;
}
public void setGender(String gender){
this.gender = gender;
}

public static final String EMAIL_ADDRESS = "emailAddress";
@Column(name="EMAILADDRESS", nullable=true, length=250)
private String emailAddress;
public String getEmailAddress(){
return this.emailAddress;
}
public void setEmailAddress(String emailAddress){
this.emailAddress = emailAddress;
}

public static final String DORMANCY_FLAG = "dormancyFlag";
@Column(name="DORMANCYFLAG", nullable=true, length=250)
private String dormancyFlag;
public String getDormancyFlag(){
return this.dormancyFlag;
}
public void setDormancyFlag(String dormancyFlag){
this.dormancyFlag = dormancyFlag;
}

public static final String _LAST_TRANSACTION_DATE = "LastTransactionDate";
@Column(name="LASTTRANSACTIONDATE", nullable=true, length=250)
private String LastTransactionDate;
public String getLastTransactionDate(){
return this.LastTransactionDate;
}
public void setLastTransactionDate(String LastTransactionDate){
this.LastTransactionDate = LastTransactionDate;
}

public static final String _LAST_TRANSACTION_TYPE = "LastTransactionType";
@Column(name="LASTTRANSACTIONTYPE", nullable=true, length=250)
private String LastTransactionType;
public String getLastTransactionType(){
return this.LastTransactionType;
}
public void setLastTransactionType(String LastTransactionType){
this.LastTransactionType = LastTransactionType;
}

public static final String _LAST_TRANSACTION_AMOUNT = "LastTransactionAmount";
@Column(name="LASTTRANSACTIONAMOUNT", nullable=true, length=250)
private String LastTransactionAmount;
public String getLastTransactionAmount(){
return this.LastTransactionAmount;
}
public void setLastTransactionAmount(String LastTransactionAmount){
this.LastTransactionAmount = LastTransactionAmount;
}

public static final String CREATED_DATE = "createdDate";
@Column(name="CREATEDDATE", nullable=true, length=250)
private String createdDate;
public String getCreatedDate(){
return this.createdDate;
}
public void setCreatedDate(String createdDate){
this.createdDate = createdDate;
}

public static final String CREATED_USER = "createdUser";
@Column(name="CREATEDUSER", nullable=true, length=250)
private String createdUser;
public String getCreatedUser(){
return this.createdUser;
}
public void setCreatedUser(String createdUser){
this.createdUser = createdUser;
}

public static final String MODIFIED_DATE = "modifiedDate";
@Column(name="MODIFIEDDATE", nullable=true, length=250)
private String modifiedDate;
public String getModifiedDate(){
return this.modifiedDate;
}
public void setModifiedDate(String modifiedDate){
this.modifiedDate = modifiedDate;
}

public static final String MODIFIED_USER = "modifiedUser";
@Column(name="MODIFIEDUSER", nullable=true, length=250)
private String modifiedUser;
public String getModifiedUser(){
return this.modifiedUser;
}
public void setModifiedUser(String modifiedUser){
this.modifiedUser = modifiedUser;
}

public static final String DELETED_DATE = "deletedDate";
@Column(name="DELETEDDATE", nullable=true, length=250)
private String deletedDate;
public String getDeletedDate(){
return this.deletedDate;
}
public void setDeletedDate(String deletedDate){
this.deletedDate = deletedDate;
}

public static final String DELETED_USER = "deletedUser";
@Column(name="DELETEDUSER", nullable=true, length=250)
private String deletedUser;
public String getDeletedUser(){
return this.deletedUser;
}
public void setDeletedUser(String deletedUser){
this.deletedUser = deletedUser;
}

@Override
public String toString() {
return "MPAY_Customer_Rep [id= " + getId() + ", extractStamp= " + getExtractStamp() + ", customerName= " + getCustomerName() + ", customerType= " + getCustomerType() + ", status= " + getStatus() + ", active= " + getActive() + ", nin= " + getNin() + ", nationality= " + getNationality() + ", idexpirydate= " + getIdexpirydate() + ", iDType= " + getIDType() + ", dob= " + getDob() + ", customerMobile= " + getCustomerMobile() + ", iban= " + getIban() + ", accountNumber= " + getAccountNumber() + ", bankAccNumber= " + getBankAccNumber() + ", balances= " + getBalances() + ", Languageindicator= " + getLanguageindicator() + ", notificationChannel= " + getNotificationChannel() + ", deviceId= " + getDeviceId() + ", deviceName= " + getDeviceName() + ", deviceStatus= " + getDeviceStatus() + ", deviceActive= " + getDeviceActive() + ", deviceStolen= " + getDeviceStolen() + ", deviceRetry= " + getDeviceRetry() + ", maximumNumberOfDevices= " + getMaximumNumberOfDevices() + ", LastLogonDate= " + getLastLogonDate() + ", customerAlladdressDetails= " + getCustomerAlladdressDetails() + ", gender= " + getGender() + ", emailAddress= " + getEmailAddress() + ", dormancyFlag= " + getDormancyFlag() + ", LastTransactionDate= " + getLastTransactionDate() + ", LastTransactionType= " + getLastTransactionType() + ", LastTransactionAmount= " + getLastTransactionAmount() + ", createdDate= " + getCreatedDate() + ", createdUser= " + getCreatedUser() + ", modifiedDate= " + getModifiedDate() + ", modifiedUser= " + getModifiedUser() + ", deletedDate= " + getDeletedDate() + ", deletedUser= " + getDeletedUser() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getExtractStamp() == null) ? 0 : getExtractStamp().hashCode());
result = prime * result + ((getCustomerName() == null) ? 0 : getCustomerName().hashCode());
result = prime * result + ((getCustomerType() == null) ? 0 : getCustomerType().hashCode());
result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
result = prime * result + ((getActive() == null) ? 0 : getActive().hashCode());
result = prime * result + ((getNin() == null) ? 0 : getNin().hashCode());
result = prime * result + ((getNationality() == null) ? 0 : getNationality().hashCode());
result = prime * result + ((getIdexpirydate() == null) ? 0 : getIdexpirydate().hashCode());
result = prime * result + ((getIDType() == null) ? 0 : getIDType().hashCode());
result = prime * result + ((getDob() == null) ? 0 : getDob().hashCode());
result = prime * result + ((getCustomerMobile() == null) ? 0 : getCustomerMobile().hashCode());
result = prime * result + ((getIban() == null) ? 0 : getIban().hashCode());
result = prime * result + ((getAccountNumber() == null) ? 0 : getAccountNumber().hashCode());
result = prime * result + ((getBankAccNumber() == null) ? 0 : getBankAccNumber().hashCode());
result = prime * result + ((getBalances() == null) ? 0 : getBalances().hashCode());
result = prime * result + ((getLanguageindicator() == null) ? 0 : getLanguageindicator().hashCode());
result = prime * result + ((getNotificationChannel() == null) ? 0 : getNotificationChannel().hashCode());
result = prime * result + ((getDeviceId() == null) ? 0 : getDeviceId().hashCode());
result = prime * result + ((getDeviceName() == null) ? 0 : getDeviceName().hashCode());
result = prime * result + ((getDeviceStatus() == null) ? 0 : getDeviceStatus().hashCode());
result = prime * result + ((getDeviceActive() == null) ? 0 : getDeviceActive().hashCode());
result = prime * result + ((getDeviceStolen() == null) ? 0 : getDeviceStolen().hashCode());
result = prime * result + ((getDeviceRetry() == null) ? 0 : getDeviceRetry().hashCode());
result = prime * result + ((getMaximumNumberOfDevices() == null) ? 0 : getMaximumNumberOfDevices().hashCode());
result = prime * result + ((getLastLogonDate() == null) ? 0 : getLastLogonDate().hashCode());
result = prime * result + ((getCustomerAlladdressDetails() == null) ? 0 : getCustomerAlladdressDetails().hashCode());
result = prime * result + ((getGender() == null) ? 0 : getGender().hashCode());
result = prime * result + ((getEmailAddress() == null) ? 0 : getEmailAddress().hashCode());
result = prime * result + ((getDormancyFlag() == null) ? 0 : getDormancyFlag().hashCode());
result = prime * result + ((getLastTransactionDate() == null) ? 0 : getLastTransactionDate().hashCode());
result = prime * result + ((getLastTransactionType() == null) ? 0 : getLastTransactionType().hashCode());
result = prime * result + ((getLastTransactionAmount() == null) ? 0 : getLastTransactionAmount().hashCode());
result = prime * result + ((getCreatedDate() == null) ? 0 : getCreatedDate().hashCode());
result = prime * result + ((getCreatedUser() == null) ? 0 : getCreatedUser().hashCode());
result = prime * result + ((getModifiedDate() == null) ? 0 : getModifiedDate().hashCode());
result = prime * result + ((getModifiedUser() == null) ? 0 : getModifiedUser().hashCode());
result = prime * result + ((getDeletedDate() == null) ? 0 : getDeletedDate().hashCode());
result = prime * result + ((getDeletedUser() == null) ? 0 : getDeletedUser().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Customer_Rep))
return false;
else {MPAY_Customer_Rep other = (MPAY_Customer_Rep) obj;
return this.hashCode() == other.hashCode();}
}


}