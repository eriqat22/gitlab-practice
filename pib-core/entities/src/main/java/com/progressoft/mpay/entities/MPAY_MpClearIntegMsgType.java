package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_MpClearIntegMsgTypes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="MpClearIntegMsgTypes")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_MpClearIntegMsgType extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_MpClearIntegMsgType(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String PROCESSOR = "processor";
@Column(name="PROCESSOR", nullable=true, length=1000)
private String processor;
public String getProcessor(){
return this.processor;
}
public void setProcessor(String processor){
this.processor = processor;
}

public static final String IS_REQUEST = "isRequest";
@Column(name="ISREQUEST", nullable=false, length=1)
private Boolean isRequest;
public Boolean getIsRequest(){
return this.isRequest;
}
public void setIsRequest(Boolean isRequest){
this.isRequest = isRequest;
}

public static final String IS_FINANCIAL = "isFinancial";
@Column(name="ISFINANCIAL", nullable=false, length=1)
private Boolean isFinancial;
public Boolean getIsFinancial(){
return this.isFinancial;
}
public void setIsFinancial(Boolean isFinancial){
this.isFinancial = isFinancial;
}

public static final String RESPONSE_CODE = "responseCode";
@Column(name="RESPONSECODE", nullable=true, length=12)
private String responseCode;
public String getResponseCode(){
return this.responseCode;
}
public void setResponseCode(String responseCode){
this.responseCode = responseCode;
}

public static final String MP_CLEAR_INTEG_MSG_TYPES_N_L_S = "mpClearIntegMsgTypesNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "intgId")
@MapKey(name="languageCode")
private Map<String, MPAY_MpClearIntegMsgTypes_NLS> mpClearIntegMsgTypesNLS;
public Map<String, MPAY_MpClearIntegMsgTypes_NLS> getMpClearIntegMsgTypesNLS(){
return this.mpClearIntegMsgTypesNLS;
}
public void setMpClearIntegMsgTypesNLS(Map<String, MPAY_MpClearIntegMsgTypes_NLS> mpClearIntegMsgTypesNLS){
this.mpClearIntegMsgTypesNLS = mpClearIntegMsgTypesNLS;
}

@Override
public String toString() {
return "MPAY_MpClearIntegMsgType [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", processor= " + getProcessor() + ", isRequest= " + getIsRequest() + ", isFinancial= " + getIsFinancial() + ", responseCode= " + getResponseCode() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getProcessor() == null) ? 0 : getProcessor().hashCode());
result = prime * result + ((getResponseCode() == null) ? 0 : getResponseCode().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_MpClearIntegMsgType))
return false;
else {MPAY_MpClearIntegMsgType other = (MPAY_MpClearIntegMsgType) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.mpClearIntegMsgTypesNLS;
}
}