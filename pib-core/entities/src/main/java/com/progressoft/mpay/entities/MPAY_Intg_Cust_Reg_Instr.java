package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import com.progressoft.jfw.model.bussinessobject.core.TimestampAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

@Entity

@Table(name="MPAY_Intg_Cust_Reg_Instrs")
@XmlRootElement(name="Intg_Cust_Reg_Instrs")
@XmlAccessorType(XmlAccessType.FIELD)
public class MPAY_Intg_Cust_Reg_Instr extends JFWEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_Intg_Cust_Reg_Instr(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String REG_INSTR_CREATION_DT = "regInstrCreationDt";
@XmlJavaTypeAdapter(TimestampAdapter.class)
@Column(name="REGINSTRCREATIONDT", nullable=false, length=100)
private java.sql.Timestamp regInstrCreationDt;
public java.sql.Timestamp getRegInstrCreationDt(){
return this.regInstrCreationDt;
}
public void setRegInstrCreationDt(java.sql.Timestamp regInstrCreationDt){
this.regInstrCreationDt = regInstrCreationDt;
}

public static final String REG_INSTR_RECORD_ID = "regInstrRecordId";
@Column(name="REGINSTRRECORDID", nullable=false, length=5000)
@Lob
private String regInstrRecordId;
public String getRegInstrRecordId(){
return this.regInstrRecordId;
}
public void setRegInstrRecordId(String regInstrRecordId){
this.regInstrRecordId = regInstrRecordId;
}

public static final String FIRST_NAME = "firstName";
@Column(name="FIRSTNAME", nullable=true, length=5000)
@Lob
private String firstName;
public String getFirstName(){
return this.firstName;
}
public void setFirstName(String firstName){
this.firstName = firstName;
}

public static final String MIDDLE_NAME = "middleName";
@Column(name="MIDDLENAME", nullable=true, length=5000)
@Lob
private String middleName;
public String getMiddleName(){
return this.middleName;
}
public void setMiddleName(String middleName){
this.middleName = middleName;
}

public static final String LAST_NAME = "lastName";
@Column(name="LASTNAME", nullable=true, length=5000)
@Lob
private String lastName;
public String getLastName(){
return this.lastName;
}
public void setLastName(String lastName){
this.lastName = lastName;
}

public static final String CITY = "city";
@Column(name="CITY", nullable=true, length=5000)
@Lob
private String city;
public String getCity(){
return this.city;
}
public void setCity(String city){
this.city = city;
}

public static final String NATIONALITY = "nationality";
@Column(name="NATIONALITY", nullable=true, length=5000)
@Lob
private String nationality;
public String getNationality(){
return this.nationality;
}
public void setNationality(String nationality){
this.nationality = nationality;
}

public static final String ID_TYPE = "idType";
@Column(name="IDTYPE", nullable=true, length=5000)
@Lob
private String idType;
public String getIdType(){
return this.idType;
}
public void setIdType(String idType){
this.idType = idType;
}

public static final String ID_NUM = "idNum";
@Column(name="IDNUM", nullable=true, length=5000)
@Lob
private String idNum;
public String getIdNum(){
return this.idNum;
}
public void setIdNum(String idNum){
this.idNum = idNum;
}

public static final String DOB = "dob";
@Column(name="DOB", nullable=true, length=5000)
@Lob
private String dob;
public String getDob(){
return this.dob;
}
public void setDob(String dob){
this.dob = dob;
}

public static final String CLIENT_REF = "clientRef";
@Column(name="CLIENTREF", nullable=true, length=5000)
@Lob
private String clientRef;
public String getClientRef(){
return this.clientRef;
}
public void setClientRef(String clientRef){
this.clientRef = clientRef;
}

public static final String PHONE_ONE = "phoneOne";
@Column(name="PHONEONE", nullable=true, length=5000)
@Lob
private String phoneOne;
public String getPhoneOne(){
return this.phoneOne;
}
public void setPhoneOne(String phoneOne){
this.phoneOne = phoneOne;
}

public static final String PHONE_TWO = "phoneTwo";
@Column(name="PHONETWO", nullable=true, length=5000)
@Lob
private String phoneTwo;
public String getPhoneTwo(){
return this.phoneTwo;
}
public void setPhoneTwo(String phoneTwo){
this.phoneTwo = phoneTwo;
}

public static final String PREF_LANG = "prefLang";
@Column(name="PREFLANG", nullable=true, length=5000)
@Lob
private String prefLang;
public String getPrefLang(){
return this.prefLang;
}
public void setPrefLang(String prefLang){
this.prefLang = prefLang;
}

public static final String EMAIL = "email";
@Column(name="EMAIL", nullable=true, length=5000)
@Lob
private String email;
public String getEmail(){
return this.email;
}
public void setEmail(String email){
this.email = email;
}

public static final String POBOX = "pobox";
@Column(name="POBOX", nullable=true, length=5000)
@Lob
private String pobox;
public String getPobox(){
return this.pobox;
}
public void setPobox(String pobox){
this.pobox = pobox;
}

public static final String ZIP_CODE = "zipCode";
@Column(name="ZIPCODE", nullable=true, length=5000)
@Lob
private String zipCode;
public String getZipCode(){
return this.zipCode;
}
public void setZipCode(String zipCode){
this.zipCode = zipCode;
}

public static final String BUILDING_NUM = "buildingNum";
@Column(name="BUILDINGNUM", nullable=true, length=5000)
@Lob
private String buildingNum;
public String getBuildingNum(){
return this.buildingNum;
}
public void setBuildingNum(String buildingNum){
this.buildingNum = buildingNum;
}

public static final String STREET_NAME = "streetName";
@Column(name="STREETNAME", nullable=true, length=5000)
@Lob
private String streetName;
public String getStreetName(){
return this.streetName;
}
public void setStreetName(String streetName){
this.streetName = streetName;
}

public static final String NOTE = "note";
@Column(name="NOTE", nullable=true, length=5000)
@Lob
private String note;
public String getNote(){
return this.note;
}
public void setNote(String note){
this.note = note;
}

public static final String MOBILE_NUMBER = "mobileNumber";
@Column(name="MOBILENUMBER", nullable=true, length=5000)
@Lob
private String mobileNumber;
public String getMobileNumber(){
return this.mobileNumber;
}
public void setMobileNumber(String mobileNumber){
this.mobileNumber = mobileNumber;
}

public static final String OPERATOR = "operator";
@Column(name="OPERATOR", nullable=true, length=5000)
@Lob
private String operator;
public String getOperator(){
return this.operator;
}
public void setOperator(String operator){
this.operator = operator;
}

public static final String NFC_SERIAL = "nfcSerial";
@Column(name="NFCSERIAL", nullable=true, length=5000)
@Lob
private String nfcSerial;
public String getNfcSerial(){
return this.nfcSerial;
}
public void setNfcSerial(String nfcSerial){
this.nfcSerial = nfcSerial;
}

public static final String ALIAS = "alias";
@Column(name="ALIAS", nullable=true, length=5000)
@Lob
private String alias;
public String getAlias(){
return this.alias;
}
public void setAlias(String alias){
this.alias = alias;
}

public static final String BANK_SHORT_NAME = "bankShortName";
@Column(name="BANKSHORTNAME", nullable=true, length=5000)
@Lob
private String bankShortName;
public String getBankShortName(){
return this.bankShortName;
}
public void setBankShortName(String bankShortName){
this.bankShortName = bankShortName;
}

public static final String MOBILE_ACC_SELECTOR = "mobileAccSelector";
@Column(name="MOBILEACCSELECTOR", nullable=true, length=5000)
@Lob
private String mobileAccSelector;
public String getMobileAccSelector(){
return this.mobileAccSelector;
}
public void setMobileAccSelector(String mobileAccSelector){
this.mobileAccSelector = mobileAccSelector;
}

public static final String NOTIFICATION_SHOW_TYPE = "notificationShowType";
@Column(name="NOTIFICATIONSHOWTYPE", nullable=true, length=5000)
@Lob
private String notificationShowType;
public String getNotificationShowType(){
return this.notificationShowType;
}
public void setNotificationShowType(String notificationShowType){
this.notificationShowType = notificationShowType;
}

public static final String REASON_DESCRIPTION = "reasonDescription";
@Column(name="REASONDESCRIPTION", nullable=true, length=5000)
@Lob
private String reasonDescription;
public String getReasonDescription(){
return this.reasonDescription;
}
public void setReasonDescription(String reasonDescription){
this.reasonDescription = reasonDescription;
}

public static final String EXTERNAL_ACC = "externalAcc";
@Column(name="EXTERNALACC", nullable=true, length=5000)
@Lob
private String externalAcc;
public String getExternalAcc(){
return this.externalAcc;
}
public void setExternalAcc(String externalAcc){
this.externalAcc = externalAcc;
}

public static final String REF_REG_FILE = "refRegFile";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REFREGFILEID", nullable=false)
private MPAY_Intg_Reg_File refRegFile;
public MPAY_Intg_Reg_File getRefRegFile(){
return this.refRegFile;
}
public void setRefRegFile(MPAY_Intg_Reg_File refRegFile){
this.refRegFile = refRegFile;
}

public static final String REG_INSTR_PROCESSING_STTS = "regInstrProcessingStts";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REGINSTRPROCESSINGSTTSID", nullable=false)
private MPAY_Reg_Instrs_Stt regInstrProcessingStts;
public MPAY_Reg_Instrs_Stt getRegInstrProcessingStts(){
return this.regInstrProcessingStts;
}
public void setRegInstrProcessingStts(MPAY_Reg_Instrs_Stt regInstrProcessingStts){
this.regInstrProcessingStts = regInstrProcessingStts;
}

public static final String REASON = "reason";
@ManyToOne
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@JoinColumn(name="REASONID", nullable=true)
@Lob
private MPAY_Reason reason;
public MPAY_Reason getReason(){
return this.reason;
}
public void setReason(MPAY_Reason reason){
this.reason = reason;
}

public static final String INST_ID_CUSTOMERS = "instIdCustomers";
@OneToMany(mappedBy = "instId")
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
private List<MPAY_Customer> instIdCustomers;
public List<MPAY_Customer> getInstIdCustomers(){
return this.instIdCustomers;
}
public void setInstIdCustomers(List<MPAY_Customer> instIdCustomers){
this.instIdCustomers = instIdCustomers;
}

@Override
public String toString() {
return "MPAY_Intg_Cust_Reg_Instr [id= " + getId() + ", regInstrCreationDt= " + getRegInstrCreationDt() + ", regInstrRecordId= " + getRegInstrRecordId() + ", firstName= " + getFirstName() + ", middleName= " + getMiddleName() + ", lastName= " + getLastName() + ", city= " + getCity() + ", nationality= " + getNationality() + ", idType= " + getIdType() + ", idNum= " + getIdNum() + ", dob= " + getDob() + ", clientRef= " + getClientRef() + ", phoneOne= " + getPhoneOne() + ", phoneTwo= " + getPhoneTwo() + ", prefLang= " + getPrefLang() + ", email= " + getEmail() + ", pobox= " + getPobox() + ", zipCode= " + getZipCode() + ", buildingNum= " + getBuildingNum() + ", streetName= " + getStreetName() + ", note= " + getNote() + ", mobileNumber= " + getMobileNumber() + ", operator= " + getOperator() + ", nfcSerial= " + getNfcSerial() + ", alias= " + getAlias() + ", bankShortName= " + getBankShortName() + ", mobileAccSelector= " + getMobileAccSelector() + ", notificationShowType= " + getNotificationShowType() + ", reasonDescription= " + getReasonDescription() + ", externalAcc= " + getExternalAcc() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getRegInstrRecordId() == null) ? 0 : getRegInstrRecordId().hashCode());
result = prime * result + ((getFirstName() == null) ? 0 : getFirstName().hashCode());
result = prime * result + ((getMiddleName() == null) ? 0 : getMiddleName().hashCode());
result = prime * result + ((getLastName() == null) ? 0 : getLastName().hashCode());
result = prime * result + ((getCity() == null) ? 0 : getCity().hashCode());
result = prime * result + ((getNationality() == null) ? 0 : getNationality().hashCode());
result = prime * result + ((getIdType() == null) ? 0 : getIdType().hashCode());
result = prime * result + ((getIdNum() == null) ? 0 : getIdNum().hashCode());
result = prime * result + ((getDob() == null) ? 0 : getDob().hashCode());
result = prime * result + ((getClientRef() == null) ? 0 : getClientRef().hashCode());
result = prime * result + ((getPhoneOne() == null) ? 0 : getPhoneOne().hashCode());
result = prime * result + ((getPhoneTwo() == null) ? 0 : getPhoneTwo().hashCode());
result = prime * result + ((getPrefLang() == null) ? 0 : getPrefLang().hashCode());
result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
result = prime * result + ((getPobox() == null) ? 0 : getPobox().hashCode());
result = prime * result + ((getZipCode() == null) ? 0 : getZipCode().hashCode());
result = prime * result + ((getBuildingNum() == null) ? 0 : getBuildingNum().hashCode());
result = prime * result + ((getStreetName() == null) ? 0 : getStreetName().hashCode());
result = prime * result + ((getMobileNumber() == null) ? 0 : getMobileNumber().hashCode());
result = prime * result + ((getOperator() == null) ? 0 : getOperator().hashCode());
result = prime * result + ((getNfcSerial() == null) ? 0 : getNfcSerial().hashCode());
result = prime * result + ((getAlias() == null) ? 0 : getAlias().hashCode());
result = prime * result + ((getBankShortName() == null) ? 0 : getBankShortName().hashCode());
result = prime * result + ((getMobileAccSelector() == null) ? 0 : getMobileAccSelector().hashCode());
result = prime * result + ((getNotificationShowType() == null) ? 0 : getNotificationShowType().hashCode());
result = prime * result + ((getExternalAcc() == null) ? 0 : getExternalAcc().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_Intg_Cust_Reg_Instr))
return false;
else {MPAY_Intg_Cust_Reg_Instr other = (MPAY_Intg_Cust_Reg_Instr) obj;
return this.hashCode() == other.hashCode();}
}


}