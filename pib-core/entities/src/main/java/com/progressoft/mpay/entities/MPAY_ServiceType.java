package com.progressoft.mpay.entities;

import java.util.Map;
import com.progressoft.jfw.model.bussinessobject.core.JFWTranslation;
import com.progressoft.jfw.model.bussinessobject.core.JFWAbstractTranslatableEntity;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.xml.bind.annotation.*;
import java.util.List;

@Entity

@Table(name="MPAY_ServiceTypes",
uniqueConstraints=
{
	@UniqueConstraint(columnNames={"CODE","Z_TENANT_ID"}),
	@UniqueConstraint(columnNames={"NAME","Z_TENANT_ID"})
})
@XmlRootElement(name="ServiceTypes")
@XmlAccessorType(XmlAccessType.FIELD)
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy .READ_WRITE )
public class MPAY_ServiceType extends JFWAbstractTranslatableEntity implements Serializable {
private static final long serialVersionUID = 1L;

public MPAY_ServiceType(){/*Default Constructor*/}

public static final String ID = "id";
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="ID", nullable=false, insertable=false)
private Long id;
public Long getId(){
return this.id;
}
public void setId(Long id){
this.id = id;
}

public static final String VPC_ACCESS_CODE = "vpcAccessCode";
@Column(name="VPCACCESSCODE", nullable=false, length=255)
private String vpcAccessCode;
public String getVpcAccessCode(){
return this.vpcAccessCode;
}
public void setVpcAccessCode(String vpcAccessCode){
this.vpcAccessCode = vpcAccessCode;
}

public static final String VPC_MERCHANT = "vpcMerchant";
@Column(name="VPCMERCHANT", nullable=false, length=255)
private String vpcMerchant;
public String getVpcMerchant(){
return this.vpcMerchant;
}
public void setVpcMerchant(String vpcMerchant){
this.vpcMerchant = vpcMerchant;
}

public static final String SECURE_SECRET = "secureSecret";
@Column(name="SECURESECRET", nullable=false, length=255)
private String secureSecret;
public String getSecureSecret(){
return this.secureSecret;
}
public void setSecureSecret(String secureSecret){
this.secureSecret = secureSecret;
}

public static final String SERVICE_TYPES_N_L_S = "serviceTypesNLS";
@org.hibernate.annotations.Cascade(org.hibernate.annotations.CascadeType.REPLICATE)
@OneToMany(mappedBy = "serviceTypeId")
@MapKey(name="languageCode")
private Map<String, MPAY_ServiceTypes_NLS> serviceTypesNLS;
public Map<String, MPAY_ServiceTypes_NLS> getServiceTypesNLS(){
return this.serviceTypesNLS;
}
public void setServiceTypesNLS(Map<String, MPAY_ServiceTypes_NLS> serviceTypesNLS){
this.serviceTypesNLS = serviceTypesNLS;
}

@Override
public String toString() {
return "MPAY_ServiceType [id= " + getId() + ", code= " + getCode() + ", name= " + getName() + ", description= " + getDescription() + ", vpcAccessCode= " + getVpcAccessCode() + ", vpcMerchant= " + getVpcMerchant() + ", secureSecret= " + getSecureSecret() + "]";
}

@Override
public int hashCode() {
final int prime = 31;
int result = 1;
int Id= new Long("null".equals(getId() + "") ? 0 : getId()).intValue();
result = prime * result + (int) (Id ^ Id >>> 32);
result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
result = prime * result + ((getVpcAccessCode() == null) ? 0 : getVpcAccessCode().hashCode());
result = prime * result + ((getVpcMerchant() == null) ? 0 : getVpcMerchant().hashCode());
result = prime * result + ((getSecureSecret() == null) ? 0 : getSecureSecret().hashCode());
return result;
}

@Override
public boolean equals(Object obj) {
if (this == obj)
return true;
else if (obj == null)
return false;
else if (!(obj instanceof MPAY_ServiceType))
return false;
else {MPAY_ServiceType other = (MPAY_ServiceType) obj;
return this.hashCode() == other.hashCode();}
}

@Override
public Map<String, ? extends JFWTranslation> getTranslationMap() {
return this.serviceTypesNLS;
}
}