package com.progressoft.mpay.plugins.mobile.registration;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.MobileNumberValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.registration.customers.CustomerWorkflowStatuses;
import org.apache.commons.lang.NullArgumentException;

public class RegistrationValidator {
    private RegistrationValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");

        RegistrationMessage message = (RegistrationMessage) context.getRequest();

        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
                context.getOperation().getMessageType());
        if (!result.isValid())
            return result;

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        result = MobileNumberValidator.validate(context, message.getSender());
        if (!result.isValid())
            return result;

        result = validateBank(context);
        if (!result.isValid())
            return result;

        result = validateBasicInfo(context, message);
        if (!result.isValid())
            return result;

        result = validateRegisteredMobile(context, message);
        if (!result.isValid())
            return result;

        result = validateRegisteredCustomer(context, message);
        if (!result.isValid())
            return result;

        result = validateAlias(context, message);
        if (!result.isValid())
            return result;

        result = validateMaxNumberOFAccounts(context);
        if (!result.isValid())
            return result;

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateBasicInfo(MessageProcessingContext context, RegistrationMessage message) {
        ValidationResult result = validateIdType(context);
        if (!result.isValid())
            return result;

        result = validateCountry(context);
        if (!result.isValid())
            return result;

        return validateProfile(context);
    }

    private static ValidationResult validateIdType(MessageProcessingContext context) {
        MPAY_IDType idType = (MPAY_IDType) context.getExtraData().get(Constants.IdTypeKey);
        if (idType == null || !idType.getIsCustomer())
            return new ValidationResult(ReasonCodes.INVALID_ID_TYPE, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCountry(MessageProcessingContext context) {
        JfwCountry country = (JfwCountry) context.getExtraData().get(Constants.CountryKey);
        if (country == null)
            return new ValidationResult(ReasonCodes.INVALID_NATIONALITY, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateRegisteredMobile(ProcessingContext context, RegistrationMessage message) {
        MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(message.getSender());
        if (mobile == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (mobile.getDeletedFlag() == null || !mobile.getDeletedFlag())
            return new ValidationResult(ReasonCodes.MOBILE_ALREADY_REGISTERED, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateRegisteredCustomer(ProcessingContext context, RegistrationMessage message) {
        MPAY_Customer customer = context.getSender().getCustomer();
        if (customer == null)
            return new ValidationResult(ReasonCodes.CUSTOMER_NOT_FOUND, null, false);

        if (!(customer.getStatusId().getCode().equals(CustomerWorkflowStatuses.APPROVED)))
            return new ValidationResult(ReasonCodes.CUSTOMER_NOT_FOUND, null, false);

        if (customer.getDeletedFlag() == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);

        if (customer.getDeletedFlag())
            return new ValidationResult(ReasonCodes.CUSTOMER_NOT_FOUND, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateAlias(ProcessingContext context, RegistrationMessage message) {
        if (message.getAlias() == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(message.getAlias(),
                ReceiverInfoType.ALIAS);
        if (mobile != null)
            return new ValidationResult(ReasonCodes.ALIAS_IS_DEFINED, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateBank(MessageProcessingContext context) {
        if (context.getSender().getBank() == null)
            return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
        if (context.getSender().getBank().getDeletedFlag() != null && context.getSender().getBank().getDeletedFlag())
            return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
        if (!context.getSender().getBank().getIsActive())
            return new ValidationResult(ReasonCodes.BANK_INACTIVE, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateProfile(MessageProcessingContext context) {
        MPAY_Profile profile = (MPAY_Profile) context.getExtraData().get(Constants.ProfileKey);
        if (profile == null)
            return new ValidationResult(ReasonCodes.PROFILE_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateMaxNumberOFAccounts(MessageProcessingContext context) {
        MPAY_ClientType clientType = context.getLookupsLoader().getCustomerClientType();
        if (clientType.getMaxNumberOfOwners() == 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (clientType.getMaxNumberOfOwners() <= context.getDataProvider()
                .getNumberOfCustomerMobiles(context.getSender().getCustomer().getId()))
            return new ValidationResult(ReasonCodes.MAX_NUMBER_OF_OWNERS_REACHED, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
