package com.progressoft.mpay.plugins.mobile.registration;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;

import java.math.BigDecimal;

public class RegistrationMessage extends MPayRequest {
    private static final String ID_TYPE_CODE_KEY = "idTypeCode";
    private static final String ID_NUMBER_KEY = "idNumber";
    private static final String ALIAS_KEY = "alias";
    private static final String NFC_SERIAL_KEY = "nfcSerial";
    private static final String COUNTRY_CODE_KEY = "countryCode";
    private static final String NOTE_KEY = "note";
    private static final String PROFILE = "profile";
    private static final String BRANCH = "branch";
    private static final String EXTERNAL_ACCOUNT = "externalAccount";
    private static final String IBAN = "iban";
    private static final String WALLET_TYPE = "walletType";
    private static final String TRANSACTIONS_SIZE = "transactionsSize";
    private static final String MAX_NUMBER_OF_DEVICES = "maxNumberOfDevices";
    private static final String CHARGE_AMOUNT_NUMBER = "chargeAmountNumber";
    private static final String CHARGE_AMOUNT_CHARACTER = "chargeAmountCharacter";
    private static final String CHARGE_DURATION = "chargeDuration";
    private static final String NOTIFICATION_SHOW_TYPE = "notificationShowType";

    private String idTypeCode;
    private String idNumber;
    private String alias;
    private String nfcSerial;
    private String countryCode;
    private String note;
    private String profile;

    private String branch;
    private String externalAccount;
    private String iban;
    private String walletType;
    private String transactionsSize;
    private Long maxNumberOfDevices;
    private BigDecimal chargeAmountNumber;
    private String chargeAmountCharacter;
    private String chargeDuration;
    private String notificationShowType;

    public RegistrationMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static RegistrationMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        RegistrationMessage message = new RegistrationMessage(request);
        fillGenericFields(message);
        fillCustomerMobileInfo(message);
        message.setAlias(message.getValue(ALIAS_KEY));

        return message;
    }

    private static void fillCustomerMobileInfo(RegistrationMessage message) throws MessageParsingException {
        message.setIdTypeCode(message.getValue(ID_TYPE_CODE_KEY));
        if (StringUtils.isEmpty(message.getIdTypeCode()))
            throw new MessageParsingException(ID_TYPE_CODE_KEY);
        message.setIdNumber(message.getValue(ID_NUMBER_KEY));
        if (StringUtils.isEmpty(message.getIdNumber()))
            throw new MessageParsingException(ID_NUMBER_KEY);
        message.setCountryCode(message.getValue(COUNTRY_CODE_KEY));
        if (StringUtils.isEmpty(message.getCountryCode()))
            throw new MessageParsingException(COUNTRY_CODE_KEY);
        message.setNfcSerial(message.getValue(NFC_SERIAL_KEY));
        message.setNote(message.getValue(NOTE_KEY));
        message.setProfile(message.getValue(PROFILE));
        message.setBranch(message.getValue(BRANCH));
        message.setExternalAccount(message.getValue(EXTERNAL_ACCOUNT));
        message.setIban(message.getValue(IBAN));
        message.setChargeAmountCharacter(message.getValue(CHARGE_AMOUNT_CHARACTER));
        message.setChargeDuration(message.getValue(CHARGE_DURATION));


        message.setNotificationShowType(message.getValue(NOTIFICATION_SHOW_TYPE));
        if (message.getNotificationShowType() != null && !message.getNotificationShowType().isEmpty())
            if (!message.getNotificationShowType().equalsIgnoreCase("1") && !message.getNotificationShowType().equalsIgnoreCase("2"))
                throw new MessageParsingException(NOTIFICATION_SHOW_TYPE);

        String chargeAmountNumber = message.getValue(CHARGE_AMOUNT_NUMBER);
        if (chargeAmountNumber != null) {
            try {
                message.setChargeAmountNumber(BigDecimal.valueOf(Double.valueOf(chargeAmountNumber)));
            } catch (NumberFormatException ex) {
                throw new MessageParsingException(CHARGE_AMOUNT_NUMBER);
            }
        }


        message.setTransactionsSize(message.getValue(TRANSACTIONS_SIZE));
        if (message.getTransactionsSize() != null && !message.getTransactionsSize().isEmpty())
            if (!message.getTransactionsSize().equalsIgnoreCase("1") && !message.getTransactionsSize().equalsIgnoreCase("2")
                    && !message.getTransactionsSize().equalsIgnoreCase("3"))
                throw new MessageParsingException(TRANSACTIONS_SIZE);

        String maxNumberOfDevice = message.getValue(MAX_NUMBER_OF_DEVICES);
        if (maxNumberOfDevice != null && !maxNumberOfDevice.isEmpty()) {
            try {
                message.setMaxNumberOfDevices(Long.valueOf(maxNumberOfDevice));
            } catch (NumberFormatException ex) {
                throw new MessageParsingException(MAX_NUMBER_OF_DEVICES);
            }
        }

        message.setWalletType(message.getValue(WALLET_TYPE));
        if (message.getWalletType() != null && !message.getWalletType().isEmpty())
            if (!message.getWalletType().equalsIgnoreCase("1") && !message.getWalletType().equalsIgnoreCase("2"))
                throw new MessageParsingException(WALLET_TYPE);


    }

    private static void fillGenericFields(RegistrationMessage message) throws MessageParsingException {
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getIdTypeCode() {
        return idTypeCode;
    }

    public void setIdTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNfcSerial() {
        return nfcSerial;
    }

    public void setNfcSerial(String nfcSerial) {
        this.nfcSerial = nfcSerial;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getTransactionsSize() {
        return transactionsSize;
    }

    public void setTransactionsSize(String transactionsSize) {
        this.transactionsSize = transactionsSize;
    }

    public Long getMaxNumberOfDevices() {
        return maxNumberOfDevices;
    }

    public void setMaxNumberOfDevices(Long maxNumberOfDevices) {
        this.maxNumberOfDevices = maxNumberOfDevices;
    }

    public BigDecimal getChargeAmountNumber() {
        return chargeAmountNumber;
    }

    public void setChargeAmountNumber(BigDecimal chargeAmountNumber) {
        this.chargeAmountNumber = chargeAmountNumber;
    }

    public String getChargeAmountCharacter() {
        return chargeAmountCharacter;
    }

    public void setChargeAmountCharacter(String chargeAmountCharacter) {
        this.chargeAmountCharacter = chargeAmountCharacter;
    }

    public String getChargeDuration() {
        return chargeDuration;
    }

    public void setChargeDuration(String chargeDuration) {
        this.chargeDuration = chargeDuration;
    }

    public String getNotificationShowType() {
        return notificationShowType;
    }

    public void setNotificationShowType(String notificationShowType) {
        this.notificationShowType = notificationShowType;
    }
}
