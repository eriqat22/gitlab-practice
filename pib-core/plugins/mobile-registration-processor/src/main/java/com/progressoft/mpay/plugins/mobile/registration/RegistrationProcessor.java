package com.progressoft.mpay.plugins.mobile.registration;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.BankType;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.registration.customers.CustomerMobileWorkflowStatuses;
import com.progressoft.mpay.registration.customers.PostApproveCustomerMobile;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class RegistrationProcessor implements MessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationProcessor.class);

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = RegistrationValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
                    e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatus,
                                                  String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatus);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws InvalidActionException {
        logger.debug("Inside AcceptMessage ...");
        MPAY_CustomerMobile customerMobile = getCustomerMobileFromRequest(context);
        registerCustomerMobile(customerMobile, context);
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null,
                    ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private void registerCustomerMobile(MPAY_CustomerMobile customerMobile, ProcessingContext context)
            throws InvalidActionException {
        JfwHelper.createEntity(MPAYView.CUSTOMER_MOBILES, customerMobile);
        Map<String, Object> v = new HashMap<>();
        v.put(WorkflowService.WF_ARG_BEAN, customerMobile);
        context.getDataProvider().updateWorkflowStep(CustomerMobileWorkflowStatuses.INTEGRATION,
                customerMobile.getWorkflowId());
        customerMobile
                .setStatusId(context.getDataProvider().getWorkflowStatus(CustomerMobileWorkflowStatuses.INTEGRATION));
        context.getDataProvider().updateCustomerMobile(customerMobile);
        PostApproveCustomerMobile approveCustomerMobilePostFunction = AppContext.getApplicationContext()
                .getBean("PostApproveCustomerMobile", PostApproveCustomerMobile.class);
        try {
            approveCustomerMobilePostFunction.execute(v, null, null);
            MPAY_MpClearIntegMsgLog refLastMsgLog = customerMobile.getRefCustomer().getRefLastMsgLog();
            refLastMsgLog.setRefMessage(context.getMessage());
        } catch (WorkflowException e) {
            throw new BusinessException(e);
        }
    }

    private MPAY_CustomerMobile getCustomerMobileFromRequest(MessageProcessingContext context) {
        RegistrationMessage message = (RegistrationMessage) context.getRequest();
        MPAY_CustomerMobile customerMobile = new MPAY_CustomerMobile();
        customerMobile.setIsActive(true);
        customerMobile.setIsRegistered(false);
        customerMobile.setMobileNumber(message.getSender());
        customerMobile.setAlias(message.getAlias());
        customerMobile.setNfcSerial(message.getNfcSerial());
        customerMobile.setBankedUnbanked(context.getSender().getBank().getSettlementParticipant().equals(BankType.PARTICIPANT) ? BankedUnbankedFlag.BANKED : BankedUnbankedFlag.UNBANKED);
        customerMobile.setBank(context.getLookupsLoader().getBankByCode(context.getSystemParameters().getRegistrationBankCode()));
        customerMobile.setMas(1L);
        customerMobile.setApprovalNote(message.getNote());
        customerMobile.setIsBlocked(false);
        customerMobile.setRetryCount(0L);
        customerMobile.setRefCustomer(context.getDataProvider().getCustomer(message.getIdTypeCode(), message.getIdNumber()));
        customerMobile.setHint(String.valueOf(context.getMessage().getId()));
        customerMobile.setBalanceThreshold(Float.parseFloat("0"));
        customerMobile.setChannelType(LookupsLoader.getInstance().listChannelTypes().get(0));
        customerMobile.setRefProfile((MPAY_Profile) context.getExtraData().get(Constants.ProfileKey));
        customerMobile.setEnableSMS(true);
        if (validateData(message.getBranch()))
            customerMobile.setBankBranch(message.getBranch());
        if (validateData(message.getExternalAccount()))
            customerMobile.setExternalAcc(message.getExternalAccount());
        if (validateData(message.getIban()))
            customerMobile.setIban(message.getIban());

        if (message.getMaxNumberOfDevices() > 0)
            customerMobile.setMaxNumberOfDevices(message.getMaxNumberOfDevices());

        if (validateData(message.getNotificationShowType()))
            customerMobile.setNotificationShowType(message.getNotificationShowType());
        return customerMobile;
    }


    private boolean validateData(String value) {
        return value != null && !value.isEmpty();
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        RegistrationMessage message = RegistrationMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        sender.setCustomer(context.getDataProvider().getCustomer(message.getIdTypeCode(), message.getIdNumber()));
        OTPHanlder.loadOTP(context, message.getPin());
        MPAY_Bank bank = loadBank(context);
        if (bank == null)
            return;
        sender.setBank(bank);
        loadLookups(context, message);
    }

    private void loadLookups(MessageProcessingContext context, RegistrationMessage message) {
        MPAY_IDType idType = context.getLookupsLoader().getIDType(message.getIdTypeCode());
        if (idType == null)
            return;
        context.getExtraData().put(Constants.IdTypeKey, idType);
        JfwCountry country = context.getLookupsLoader().getCountry(message.getCountryCode());
        if (country == null)
            return;
        context.getExtraData().put(Constants.CountryKey, country);

        MPAY_Profile profile;
        if (message.getProfile() == null || message.getProfile().isEmpty())
            profile = context.getLookupsLoader().getProfile(context.getSystemParameters().getRegistrationProfileCode());
        else
            profile = context.getLookupsLoader().getProfile(message.getProfile());
        if (profile == null)
            return;
        context.getExtraData().put(Constants.ProfileKey, profile);
    }

    private MPAY_Bank loadBank(ProcessingContext context) {
        return context.getLookupsLoader().getBankByCode(context.getSystemParameters().getRegistrationBankCode());
    }

}
