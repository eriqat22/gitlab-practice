package com.progressoft.mpay.plugins.transactionreversal;

public class Constants {
	public static final String NOTIFICATION_TOKEN_KEY = "NotificationToken";
	public static final String ORIGINAL_MESSAGE_KEY = "OriginalMessage";
	public static final String ORIGINAL_TRANSACTION_KEY = "OriginalTransaction";
	public static final String ORIGINAL_TRANSACTION_CONFIG_KEY = "OriginalTransactionConfig";

	private Constants() {

	}
}
