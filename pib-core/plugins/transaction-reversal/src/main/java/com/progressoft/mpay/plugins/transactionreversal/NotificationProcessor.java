package com.progressoft.mpay.plugins.transactionreversal;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.JvDetailsTypes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.plugins.ProcessingContext;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		logger.debug("Inside createAcceptanceNotificationMessages ...");
		try {
			MPAY_Transaction originalTransction = (MPAY_Transaction)context.getExtraData().get(Constants.ORIGINAL_TRANSACTION_KEY);
			if(originalTransction.getSenderMobile() != null)
				return handleWhenSenderMobile(context, originalTransction);
			else
				return handleWhenSenderService(context, originalTransction);

		} catch (Exception e) {
			logger.error("Error when CreateReversalNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static List<MPAY_Notification> handleWhenSenderMobile(ProcessingContext context, MPAY_Transaction originalTransction) {
		String reference = context.getMessage().getReference();
		List<MPAY_Notification> notifications = new ArrayList<>();
		MPAY_CustomerMobile senderMobile = originalTransction.getSenderMobile();
		MPAY_CorpoarteService receiverService = originalTransction.getReceiverService();
		MPAY_EndPointOperation operation = context.getOperation();
		String currency = originalTransction.getCurrency().getStringISOCode();
		if (senderMobile != null) {
			long senderLanguageId;
			ReversalNotificationContext senderNotificationContext = new ReversalNotificationContext();
			senderNotificationContext.setCurrency(currency);
			senderNotificationContext.setReference(reference);
			MPAY_Account senderAccount = originalTransction.getSenderMobileAccount().getRefAccount();
			context.getDataProvider().refreshEntity(senderAccount);
			senderNotificationContext.setOrignalTransactionReference(originalTransction.getReference());
			senderNotificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
			senderNotificationContext.setSenderBanked(senderAccount.getIsBanked());
			senderNotificationContext.setExtraData1(getBalanceAfterForReversal(context, originalTransction));
			senderLanguageId = senderMobile.getRefCustomer().getPrefLang().getId();
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), senderNotificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(),
					senderLanguageId, senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		if (receiverService != null) {
			long receiverLanguageId;
			ReversalNotificationContext receiverNotificationContext = new ReversalNotificationContext();
			receiverNotificationContext.setCurrency(currency);
			receiverNotificationContext.setReference(reference);
			MPAY_Account receiverAccount = originalTransction.getReceiverServiceAccount().getRefAccount();
			context.getDataProvider().refreshEntity(receiverAccount);
			receiverNotificationContext.setOrignalTransactionReference(originalTransction.getReference());
			receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
			receiverNotificationContext.setReceiverBanked(receiverAccount.getIsBanked());
			receiverLanguageId = receiverService.getRefCorporate().getPrefLang().getId();
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), receiverNotificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(),
					receiverLanguageId, receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
		}
		return notifications;
	}

	private static List<MPAY_Notification> handleWhenSenderService(ProcessingContext context, MPAY_Transaction originalTransction) {
		String reference = context.getMessage().getReference();
		List<MPAY_Notification> notifications = new ArrayList<>();
		MPAY_CorpoarteService senderService = originalTransction.getReceiverService();
		MPAY_CustomerMobile receiverMobile = originalTransction.getSenderMobile();
		MPAY_EndPointOperation operation = context.getOperation();
		String currency = originalTransction.getCurrency().getStringISOCode();
		if (senderService != null) {
			long senderLanguageId;
			ReversalNotificationContext senderNotificationContext = new ReversalNotificationContext();
			senderNotificationContext.setCurrency(currency);
			senderNotificationContext.setReference(reference);
			MPAY_Account senderAccount = originalTransction.getSenderServiceAccount().getRefAccount();
			context.getDataProvider().refreshEntity(senderAccount);
			senderNotificationContext.setOrignalTransactionReference(originalTransction.getReference());
			senderNotificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
			senderNotificationContext.setSenderBanked(context.getSender().getAccount().getIsBanked());
			senderNotificationContext.setExtraData1(getBalanceAfterForReversal(context, originalTransction));
			senderLanguageId = senderService.getRefCorporate().getPrefLang().getId();
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), senderNotificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(),
					senderLanguageId, senderService.getNotificationReceiver(), senderService.getNotificationChannel().getCode()));
		}
		if (receiverMobile != null) {
			long receiverLanguageId;
			ReversalNotificationContext receiverNotificationContext = new ReversalNotificationContext();
			receiverNotificationContext.setCurrency(currency);
			receiverNotificationContext.setReference(reference);
			MPAY_Account receiverAccount = originalTransction.getReceiverMobileAccount().getRefAccount();
			context.getDataProvider().refreshEntity(receiverAccount);
			receiverNotificationContext.setOrignalTransactionReference(originalTransction.getReference());
			receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
			receiverNotificationContext.setReceiverBanked(receiverAccount.getIsBanked());
			receiverLanguageId = receiverMobile.getRefCustomer().getPrefLang().getId();
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), receiverNotificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(),
					receiverLanguageId, receiverMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		return notifications;
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			TransactionReversalMessage request = TransactionReversalMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String senderService = request.getSender();
			String notificationChannel = NotificationChannelsCode.EMAIL;
			MPAY_Language language = context.getSystemParameters().getSystemLanguage();
			MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
			if (service != null) {
				senderService = service.getNotificationReceiver();
				notificationChannel = service.getNotificationChannel().getCode();
				language = service.getRefCorporate().getPrefLang();
			}
			ReversalNotificationContext notificationContext = new ReversalNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());

			notificationContext.setOrignalTransactionReference(request.getReference());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_SENDER,
					context.getMessage().getRefOperation().getId(), language.getId(), senderService, notificationChannel));
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static String getBalanceAfterForReversal(ProcessingContext context, MPAY_Transaction originalTransction) {
		List<MPAY_JvDetail> reversalDetails = originalTransction.getRefTrsansactionJvDetails().stream().filter(detail -> detail.getJvType().getCode().equals(JvDetailsTypes.REVERSAL.toString()))
				.collect(Collectors.toList());
		 MPAY_JvDetail maxSerial = reversalDetails.stream().max(Comparator.comparing(MPAY_JvDetail::getSerial)).orElseGet(MPAY_JvDetail::new);
		 return maxSerial.getBalanceAfter().toString();
	}
}
