package com.progressoft.mpay.plugins.transactionreversal;

import java.sql.SQLException;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entities.MPAY_TransactionConfig;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.transactionreversal.integrations.TransactionReversalOfflineResponseProcessor;

public class TransactionReversalProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(TransactionReversalProcessor.class);

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage...");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessMessage(context);
			ValidationResult validationResult = TransactionReversalValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage", ex);
			return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException, WorkflowException {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = new MessageProcessingResult();
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
		MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
		MPAY_Transaction transaction = (MPAY_Transaction) context.getExtraData().get(Constants.ORIGINAL_TRANSACTION_KEY);
		JVPostingResult postingResult = TransactionHelper.reverseTransaction(transaction, context);
		if (!postingResult.isSuccess())
			return rejectMessage(context, ProcessingStatusCodes.REJECTED, postingResult.getReason(), null);
		context.getMessage().setReason(reason);
		result.setMessage(context.getMessage());
		context.getMessage().setProcessingStatus(status);
		context.getMessage().setReason(reason);
		result.setReasonCode(context.getMessage().getReason().getCode());
		result.setStatus(status);
		result.setStatusDescription(context.getMessage().getReasonDesc());
		handleNotifications(context, result, status);
		handleOriginalTransaction(context);
		context.getDataProvider().mergeTransaction(transaction);
		return result;
	}

	private void handleOriginalTransaction(MessageProcessingContext context) {
		MPAY_Transaction originalTransaction = (MPAY_Transaction) context.getExtraData().get(Constants.ORIGINAL_TRANSACTION_KEY);
		context.getDataProvider().reverseTransaction(originalTransaction.getId(), context.getMessage().getMessageID());
	}

	private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result, MPAY_ProcessingStatus status) {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)) {
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		} else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
			result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException, SQLException {
		logger.debug("Inside PreProcessMessage ...");
		TransactionReversalMessage message = TransactionReversalMessage.parseMessage(context.getRequest());

		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		sender.setInfo(message.getSender());
		sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
		if (sender.getService() == null)
			return;
		sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
		if (sender.getServiceAccount() == null)
			return;
		sender.setAccount(sender.getServiceAccount().getRefAccount());
		sender.setCorporate(sender.getService().getRefCorporate());
		sender.setBank(sender.getServiceAccount().getBank());
		sender.setProfile(sender.getServiceAccount().getRefProfile());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
		MPAY_MPayMessage originalMessage = context.getDataProvider().getMPayMessage(message.getReference());
		if (originalMessage == null)
			return;
		context.getExtraData().put(Constants.ORIGINAL_MESSAGE_KEY, originalMessage);
		MPAY_Transaction originalTransaction = context.getDataProvider().getTransactionByMessageId(originalMessage.getId());
		if (originalTransaction == null)
			return;
		context.getExtraData().put(Constants.ORIGINAL_TRANSACTION_KEY, context.getDataProvider().getTransactionByMessageId(originalMessage.getId()));
		MPAY_TransactionConfig originalTransactionConfig = loadOriginalTransactionConfig(context, originalTransaction, originalMessage);
		if (originalTransactionConfig != null)
			context.getExtraData().put(Constants.ORIGINAL_TRANSACTION_CONFIG_KEY, originalTransactionConfig);
		context.getMessage().setMessageType(context.getOperation().getMessageType());
	}

	private MPAY_TransactionConfig loadOriginalTransactionConfig(MessageProcessingContext context, MPAY_Transaction originalTransaction, MPAY_MPayMessage originalMessage) {
		long personClientTypeId = context.getLookupsLoader().getCustomerClientType().getId();
		long senderType = originalTransaction.getSenderService() == null ? personClientTypeId : originalTransaction.getSenderService().getRefCorporate().getClientType().getId();
		long receiverType = originalTransaction.getReceiverService() == null ? personClientTypeId : originalTransaction.getReceiverService().getRefCorporate().getClientType().getId();
		boolean isSenderBanked = originalTransaction.getSenderServiceAccount() == null ? originalTransaction.getSenderMobileAccount().getRefAccount().getIsBanked()
				: originalTransaction.getSenderServiceAccount().getRefAccount().getIsBanked();
		boolean isReceiverBanked = originalTransaction.getReceiverServiceAccount() == null ? originalTransaction.getReceiverMobileAccount().getRefAccount().getIsBanked()
				: originalTransaction.getReceiverServiceAccount().getRefAccount().getIsBanked();
		return context.getLookupsLoader().getTransactionConfig(senderType, isSenderBanked, receiverType, isReceiverBanked, originalMessage.getRefOperation().getMessageType().getId(),
				originalTransaction.getRefType().getCode());
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		return MessageProcessingResult.create(context, status, reason, reasonDescription);
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		try {
			return new TransactionReversalOfflineResponseProcessor().processIntegration(context);
		} catch (Exception e) {
			logger.error("Error when ProcessIntegration", e);
			return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null, context.isRequest());
		}
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		logger.debug("Inside Reverse ...");
		return null;
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		if (context.getMessage() != null) {
			context.getMessage().setReason(reason);
			context.getMessage().setReasonDesc(reasonDescription);
			context.getMessage().setProcessingStatus(status);
		}
		if (context.getTransaction() != null) {
			context.getTransaction().setReason(reason);
			context.getTransaction().setReasonDesc(reasonDescription);
			context.getTransaction().setProcessingStatus(status);
		}
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setTransaction(context.getTransaction());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		if (reasonCode.equals(ReasonCodes.VALID)) {
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		}
		return result;
	}
}
