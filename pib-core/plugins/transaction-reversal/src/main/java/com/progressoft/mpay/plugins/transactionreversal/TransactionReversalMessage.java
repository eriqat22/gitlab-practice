package com.progressoft.mpay.plugins.transactionreversal;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang3.StringUtils;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class TransactionReversalMessage extends MPayRequest {
	public static final String REFERENCE_KEY = "reference";

	private String reference;

	public TransactionReversalMessage() {
		//
	}

	public TransactionReversalMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public static TransactionReversalMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		TransactionReversalMessage message = new TransactionReversalMessage(request);
		if (StringUtils.isEmpty(message.getSender()))
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setReference(message.getValue(REFERENCE_KEY));
		if(message.getReference() == null)
			throw new MessageParsingException(TransactionReversalMessage.REFERENCE_KEY);
		return message;
	}
}
