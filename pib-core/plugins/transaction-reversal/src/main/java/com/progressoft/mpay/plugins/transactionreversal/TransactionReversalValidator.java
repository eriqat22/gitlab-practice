package com.progressoft.mpay.plugins.transactionreversal;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entities.MPAY_TransactionConfig;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;

public class TransactionReversalValidator {
	private static final Logger logger = LoggerFactory.getLogger(TransactionReversalValidator.class);

	private TransactionReversalValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = validateSender(context);
		if (!result.isValid())
			return result;

		return validateOriginalRequest(context);
	}

	private static ValidationResult validateOriginalRequest(MessageProcessingContext context) {
		MPAY_MPayMessage originalMessage = (MPAY_MPayMessage) context.getExtraData().get(Constants.ORIGINAL_MESSAGE_KEY);
		MPAY_Transaction originalTransaction = (MPAY_Transaction) context.getExtraData().get(Constants.ORIGINAL_TRANSACTION_KEY);
		MPAY_TransactionConfig originalTransactionConfig = (MPAY_TransactionConfig) context.getExtraData().get(Constants.ORIGINAL_TRANSACTION_CONFIG_KEY);
		if (originalMessage == null || originalTransaction == null)
			return new ValidationResult(ReasonCodes.TRANSACTION_NOT_FOUND, null, false);
		if (!originalMessage.getProcessingStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return new ValidationResult(ReasonCodes.INVALID_TRANSACTION_STATUS, null, false);
		if (originalTransaction.getIsReversed())
			return new ValidationResult(ReasonCodes.TRANSACTION_ALREADY_REVERSED, null, false);
		if (originalTransactionConfig == null)
			return new ValidationResult(ReasonCodes.INVALID_OPERATION, null, false);
		if(originalTransaction.getReceiverService() == null || context.getSender().getCorporate().getId() != originalTransaction.getReceiverService().getRefCorporate().getId())
			return new ValidationResult(ReasonCodes.UNAUTHORIZED_OPERATION, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateSender(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		return PinCodeValidator.validate(context, context.getSender().getService(), context.getRequest().getPin());
	}
}
