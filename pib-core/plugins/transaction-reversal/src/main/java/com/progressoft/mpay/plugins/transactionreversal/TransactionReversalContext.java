package com.progressoft.mpay.plugins.transactionreversal;

import com.progressoft.mpay.plugins.NotificationContext;

public class TransactionReversalContext extends NotificationContext {
	private String receiver;
	private String currency;
	private String amount;
	private String senderBalance;
	private String receiverBalance;
	private boolean isSenderBanked;
	private boolean isReceiverBanked;
	private String senderCharges;
	private String receiverCharges;

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getSenderBalance() {
		return senderBalance;
	}

	public void setSenderBalance(String balance) {
		this.senderBalance = balance;
	}

	public boolean isSenderBanked() {
		return isSenderBanked;
	}

	public void setSenderBanked(boolean isBanked) {
		this.isSenderBanked = isBanked;
	}

	public boolean isReceiverBanked() {
		return isReceiverBanked;
	}

	public void setReceiverBanked(boolean isReceiverBanked) {
		this.isReceiverBanked = isReceiverBanked;
	}

	public String getSenderCharges() {
		return this.senderCharges;
	}

	public void setSenderCharges(String senderCharges) {
		this.senderCharges = senderCharges;
	}

	public String getReceiverBalance() {
		return receiverBalance;
	}

	public void setReceiverBalance(String receiverBalance) {
		this.receiverBalance = receiverBalance;
	}

	public String getReceiverCharges() {
		return receiverCharges;
	}

	public void setReceiverCharges(String receiverCharges) {
		this.receiverCharges = receiverCharges;
	}

	public boolean hasSenderCharges() {
		if (this.senderCharges == null || this.senderCharges.trim().length() == 0)
			return false;
		return Double.parseDouble(this.senderCharges) > 0;
	}

	public boolean hasReceiverCharges() {
		if (this.receiverCharges == null || this.receiverCharges.trim().length() == 0)
			return false;
		return Double.parseDouble(this.receiverCharges) > 0;
	}
}
