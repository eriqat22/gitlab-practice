package com.progressoft.mpay.plugins.efincustomerinquiry;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;

public class EfinCustomerInquiryValidator {
	private static final Logger logger = LoggerFactory.getLogger(EfinCustomerInquiryValidator.class);

	private EfinCustomerInquiryValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside validate =============");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = MobileValidator.validate(context.getSender().getMobile(), true);
		if(!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if(!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if(!result.isValid())
			return result;

		MPAY_MPayMessage message = (MPAY_MPayMessage) context.getExtraData().get(Constants.MESSAGE_KEY);
		if (message == null)
			return new ValidationResult(ReasonCodes.MESSAGE_NOT_FOUND, null, false);

		if (!message.getSender().equals(context.getRequest().getSender()))
			return new ValidationResult(ReasonCodes.MESSAGE_NOT_FOUND, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
