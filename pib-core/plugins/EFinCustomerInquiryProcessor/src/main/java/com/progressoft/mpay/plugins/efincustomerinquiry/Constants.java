package com.progressoft.mpay.plugins.efincustomerinquiry;

public class Constants {
	public static final String STATUS_KEY = "status";
	public static final String REASON_CODE_KEY = "reason";
	public static final String MESSAGE_KEY = "Request";

	private Constants() {

	}
}
