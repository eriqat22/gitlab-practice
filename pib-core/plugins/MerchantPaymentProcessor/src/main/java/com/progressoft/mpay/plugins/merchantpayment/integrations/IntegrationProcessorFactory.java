package com.progressoft.mpay.plugins.merchantpayment.integrations;

import javax.transaction.NotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;

public class IntegrationProcessorFactory {
	private static final Logger logger = LoggerFactory.getLogger(IntegrationProcessorFactory.class);

	private IntegrationProcessorFactory() {

	}

	public static IntegrationProcessor createProcessor(int messageType) throws NotSupportedException {
		logger.debug("Inside CreateProcessor ...");
		logger.debug("messageType: " + messageType);
		if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)
			return new MerchantPayInwardProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE)
			return new MerchantPayResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_RESPONSE)
			return new MerchantPayOfflineResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST)
			return new MerchantPayReversalAdviseProcessor();
		else
			throw new NotSupportedException("messageType == " + messageType);
	}
}
