package com.progressoft.mpay.plugins.merchantpayment;

import ch.lambdaj.Lambda;
import com.progressoft.mpay.AccountLimits;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Iterator;

public class LibyaLimitsValidator {

    private static final Logger logger = LoggerFactory.getLogger(com.progressoft.mpay.plugins.validators.LimitsValidator.class);

    private LibyaLimitsValidator() {
    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate...");
        if (context == null) {
            throw new NullArgumentException("context");
        } else {
            ProcessingContextSide side;
            if (context.getTransactionNature() != null && !context.getTransactionNature().equals("1")) {
                side = context.getReceiver();
                return validate(side, context.getAmount(), context.getSender().getService().getPaymentType());
            } else {
                side = context.getSender();
                return validate(side, context.getAmount(), context.getReceiver().getService().getPaymentType());
            }

        }
    }

    public static ValidationResult validate(ProcessingContextSide side, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside Validate ...");
        if (side == null) {
            throw new NullArgumentException("side");
        } else {
            MPAY_Profile profile = side.getProfile();
            if (profile == null) {
                return new ValidationResult("00", null, true);
            } else {
                MPAY_LimitsScheme scheme = profile.getLimitsScheme();
                return scheme != null && scheme.getIsActive() ? validateLimits(side.getLimits(), amount.add(side.getCharge()), scheme, messageType) : new ValidationResult("00", null, true);
            }
        }
    }

    private static ValidationResult validateLimits(AccountLimits limits, BigDecimal amount, MPAY_LimitsScheme scheme, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateLimits ...");
        Iterator var4 = scheme.getRefSchemeLimits().iterator();

        while (var4.hasNext()) {
            MPAY_Limit limit = (MPAY_Limit) var4.next();
            if (limit.getIsActive()) {
                MPAY_LimitsDetail detail = getActiveLimitDetail(limit, messageType);
                if (detail != null) {
                    long limitTypeId = limit.getRefType().getId();
                    ValidationResult result = validateLimitDetails(limits, amount, messageType, detail, limitTypeId);
                    if (!result.isValid()) {
                        return result;
                    }
                }
            }
        }

        return new ValidationResult("00", null, true);
    }

    private static ValidationResult validateLimitDetails(AccountLimits limits, BigDecimal amount, MPAY_MessageType messageType, MPAY_LimitsDetail detail, long limitTypeId) {
        ValidationResult result;
        if (limitTypeId == 1L) {
            result = validateDailyLimit(limits, detail, amount, messageType);
            if (!result.isValid()) {
                return result;
            }
        } else if (limitTypeId == 2L) {
            result = validateWeeklyLimit(limits, detail, amount, messageType);
            if (!result.isValid()) {
                return result;
            }
        } else if (limitTypeId == 3L) {
            result = validateMonthlyLimit(limits, detail, amount, messageType);
            if (!result.isValid()) {
                return result;
            }
        } else {
            if (limitTypeId != 4L) {
                throw new InvalidArgumentException("Limit type not supported: " + limitTypeId);
            }

            result = validateYearlyLimit(limits, detail, amount, messageType);
            if (!result.isValid()) {
                return result;
            }
        }

        return new ValidationResult("00", null, true);
    }

    private static ValidationResult validateYearlyLimit(AccountLimits limits, MPAY_LimitsDetail detail, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateYearlyLimit ...");
        if (limits.getYear() != LocalDate.now().getYear() && amount.compareTo(detail.getTxAmountLimit()) < 0) {
            return new ValidationResult("00", null, true);
        } else if (detail.getTxCountLimit() > 0L && (long) limits.getYearlyCount() >= detail.getTxCountLimit()) {
            return new ValidationResult("64", null, false);
        } else if (messageType.getIsFinancial() && amount != null) {
            return limits.getYearlyAmount().add(amount).compareTo(detail.getTxAmountLimit()) > 0 ? new ValidationResult("65", null, false) : new ValidationResult("00", null, true);
        } else {
            return new ValidationResult("00", null, true);
        }
    }

    private static ValidationResult validateMonthlyLimit(AccountLimits limits, MPAY_LimitsDetail detail, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateMonthlyLimit ...");
        if (limits.getMonth() != LocalDate.now().getMonthValue() && amount.compareTo(detail.getTxAmountLimit()) < 0) {
            return new ValidationResult("00", null, true);
        } else if (detail.getTxCountLimit() > 0L && (long) limits.getMonthlyCount() >= detail.getTxCountLimit()) {
            return new ValidationResult("62", null, false);
        } else if (messageType.getIsFinancial() && amount != null) {
            return limits.getMonthlyAmount().add(amount).compareTo(detail.getTxAmountLimit()) > 0 ? new ValidationResult("63", null, false) : new ValidationResult("00", null, true);
        } else {
            return new ValidationResult("00", null, true);
        }
    }

    private static ValidationResult validateWeeklyLimit(AccountLimits limits, MPAY_LimitsDetail detail, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateWeeklyLimit ...");
        if (limits.getWeekNumber() != Calendar.getInstance().get(3) && amount.compareTo(detail.getTxAmountLimit()) < 0) {
            return new ValidationResult("00", null, true);
        } else if (detail.getTxCountLimit() > 0L && (long) limits.getWeeklyCount() >= detail.getTxCountLimit()) {
            return new ValidationResult("60", null, false);
        } else if (messageType.getIsFinancial() && amount != null) {
            return limits.getWeeklyAmount().add(amount).compareTo(detail.getTxAmountLimit()) > 0 ? new ValidationResult("61", null, false) : new ValidationResult("00", null, true);
        } else {
            return new ValidationResult("00", null, true);
        }
    }

    private static ValidationResult validateDailyLimit(AccountLimits limits, MPAY_LimitsDetail detail, BigDecimal amount, MPAY_MessageType messageType) {
        logger.debug("Inside ValidateDailyLimit ...");
        if (!limits.getDailyDate().equals(SystemHelper.getCurrentDateWithoutTime()) && amount.compareTo(detail.getTxAmountLimit()) < 0) {
            return new ValidationResult("00", null, true);
        } else if (detail.getTxCountLimit() > 0L && (long) limits.getDailyCount() >= detail.getTxCountLimit()) {
            return new ValidationResult("19", null, false);
        } else if (messageType.getIsFinancial() && amount != null) {
            return limits.getDailyAmount().add(amount).compareTo(detail.getTxAmountLimit()) > 0 ? new ValidationResult("20", null, false) : new ValidationResult("00", null, true);
        } else {
            return new ValidationResult("00", null, true);
        }
    }

    private static MPAY_LimitsDetail getActiveLimitDetail(MPAY_Limit limit, MPAY_MessageType messageType) {
        logger.debug("Inside GetActiveLimitDetail ...");
        return (MPAY_LimitsDetail) Lambda.selectFirst(limit.getRefLimitLimitsDetails(), Lambda.having(Lambda.on(MPAY_LimitsDetail.class).getMsgType().getId(), Matchers.equalTo(messageType.getId())).and(Lambda.having(Lambda.on(MPAY_LimitsDetail.class).getIsActive(), Matchers.equalTo(true))));
    }
}


