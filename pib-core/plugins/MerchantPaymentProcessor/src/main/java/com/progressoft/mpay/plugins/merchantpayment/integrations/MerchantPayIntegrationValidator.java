package com.progressoft.mpay.plugins.merchantpayment.integrations;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;

public class MerchantPayIntegrationValidator {
	private static final Logger logger = LoggerFactory.getLogger(MerchantPayIntegrationValidator.class);

	private MerchantPayIntegrationValidator() {

	}

	public static ValidationResult validate(IntegrationProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result;

		if (context.getTransactionNature() == null)
			return new ValidationResult(ReasonCodes.UNSUPPORTED_REQUEST, null, false);

		if (context.getTransactionNature().equals(ReceiverInfoType.MOBILE))
			result = validateMobile(context);
		else
			result = validateService(context);

		if (!result.isValid())
			return result;

		return TransactionConfigValidator.validate(context.getTransactionConfig(), context.getAmount());
	}

	private static ValidationResult validateService(IntegrationProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
		if (!result.isValid())
			return result;

		return CorporateValidator.validate(context.getReceiver().getCorporate(), false);
	}

	private static ValidationResult validateMobile(IntegrationProcessingContext context) {
		ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
		if (!result.isValid())
			return result;

		return CustomerValidator.validate(context.getReceiver().getCustomer(), false);
	}
}
