package com.progressoft.mpay.plugins.merchantpayment.integrations;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public abstract class IntegrationProcessor {
	public abstract IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context);
}
