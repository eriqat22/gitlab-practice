package com.progressoft.mpay.smssmpp;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppClient;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.impl.DefaultSmppClient;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.type.Address;

public class SmppHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(SendSms.class);

	public static boolean sendLongMessage(String msisdn, String message, SmppProperties properties) throws Exception {
		try {
			byte sourceTon = (byte) 0x03;
			if (properties.getSourceAddress() != null && properties.getSourceAddress().length() > 0) {
				sourceTon = (byte) 0x05;
			}
			LOGGER.info("DataCoding: " + properties.getDataCoding() + ", DeliveryThreadSleep: " + properties.getDeliveryThreadSleep() + ", DestinationAddressNpi: "
					+ properties.getDestinationAddressNpi() + ", DestinationAddressTon" + properties.getDestinationAddressTon() + ", Encoding" + properties.getEncoding()
					+ ", Host: " + properties.getHost() + ", Password: " + properties.getPassword() + ", Port: " + properties.getPort() + ", SegmentLength: " + properties.getSegmentLength()
					+ ", ServiceType: " + properties.getServiceType() + ", SourceAddress: " + properties.getSourceAddress() + ", SourceAddressNpi: " + properties.getSourceAddressNpi()
					+ ", SourceAddressTon: " + properties.getSourceAddressTon() + ", SystemId: " + properties.getSystemId() + ", SystemType: " + properties.getSystemType()
					+ ", Phone Number: " + msisdn);

			byte[] textBytes = message.getBytes(properties.getEncoding());
			int maximumMultipartMessageSegmentSize = Integer.parseInt(properties.getSegmentLength());
			byte[] byteSingleMessage = textBytes;
			byte[][] byteMessagesArray = splitUnicodeMessage(byteSingleMessage, maximumMultipartMessageSegmentSize);
			SubmitSmResp response = null;
			SmppClient client = new DefaultSmppClient();
			SmppSession session = client.bind(createClientConfigurationNoSSL(properties), null);
			// submit all messages
			for (int i = 0; i < byteMessagesArray.length; i++) {
				SubmitSm submit0 = new SubmitSm();
				submit0.setEsmClass(SmppConstants.ESM_CLASS_UDHI_MASK);
				submit0.setRegisteredDelivery(SmppConstants.REGISTERED_DELIVERY_SMSC_RECEIPT_REQUESTED);
				submit0.setSourceAddress(new Address(sourceTon, (byte) 0x00, properties.getSourceAddress()));
				submit0.setDestAddress(new Address((byte) 0x03, (byte) 0x00, msisdn));
				submit0.setShortMessage(byteMessagesArray[i]);
				response = session.submit(submit0, Long.parseLong(properties.getTimeout()));
				LOGGER.info("Smpp Response: " + response.getCommandStatus());
			}
			session.close();
			if (response.getCommandStatus() == 0)
				return true;
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
			throw ex;
		}
		return false;
	}

	private static SmppSessionConfiguration createClientConfigurationNoSSL(SmppProperties properties) {
		SmppSessionConfiguration configuration = new SmppSessionConfiguration();
		configuration.setWindowSize(1);
		configuration.setName("session");
		configuration.setType(SmppBindType.TRANSCEIVER);
		configuration.setHost(properties.getHost());
		configuration.setPort(Integer.parseInt(properties.getPort()));
		configuration.setConnectTimeout(Long.parseLong(properties.getTimeout()));
		configuration.setBindTimeout(Long.parseLong(properties.getTimeout()));
		configuration.setSystemId(properties.getSystemId());
		configuration.setSystemType(properties.getSystemType());
		configuration.setPassword(properties.getPassword());
		configuration.getLoggingOptions().setLogBytes(true);
		return configuration;
	}

	private static byte[][] splitUnicodeMessage(byte[] aMessage, Integer maximumMultipartMessageSegmentSize) {
		final byte UDHIE_HEADER_LENGTH = 0x05;
		final byte UDHIE_IDENTIFIER_SAR = 0x00;
		final byte UDHIE_SAR_LENGTH = 0x03;
		// determine how many messages have to be sent
		int numberOfSegments = aMessage.length / maximumMultipartMessageSegmentSize;
		int messageLength = aMessage.length;
		if (numberOfSegments > 255) {
			numberOfSegments = 255;
			messageLength = numberOfSegments * maximumMultipartMessageSegmentSize;
		}
		if ((messageLength % maximumMultipartMessageSegmentSize) > 0) {
			numberOfSegments++;
		}
		// prepare array for all of the msg segments
		byte[][] segments = new byte[numberOfSegments][];
		int lengthOfData;
		// generate new reference number
		byte[] referenceNumber = new byte[1];
		new Random().nextBytes(referenceNumber);
		// split the message adding required headers
		for (int i = 0; i < numberOfSegments; i++) {
			if (numberOfSegments - i == 1) {
				lengthOfData = messageLength - i * maximumMultipartMessageSegmentSize;
			} else {
				lengthOfData = maximumMultipartMessageSegmentSize;
			}
			// new array to store the header
			segments[i] = new byte[6 + lengthOfData];
			// UDH header
			// doesn't include itself, its header length
			segments[i][0] = UDHIE_HEADER_LENGTH;
			// SAR identifier
			segments[i][1] = UDHIE_IDENTIFIER_SAR;
			// SAR length
			segments[i][2] = UDHIE_SAR_LENGTH;
			// reference number (same for all messages)
			segments[i][3] = referenceNumber[0];
			// total number of segments
			segments[i][4] = (byte) numberOfSegments;
			// segment number
			segments[i][5] = (byte) (i + 1);
			// copy the data into the array
			System.arraycopy(aMessage, (i * maximumMultipartMessageSegmentSize), segments[i], 6, lengthOfData);
		}
		return segments;
	}

}
