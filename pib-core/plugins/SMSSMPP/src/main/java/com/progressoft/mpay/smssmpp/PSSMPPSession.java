package com.progressoft.mpay.smssmpp;

import org.jsmpp.bean.BindType;
import org.jsmpp.bean.NumberingPlanIndicator;
import org.jsmpp.bean.TypeOfNumber;
import org.jsmpp.extra.SessionState;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.SMPPSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

public class PSSMPPSession {

	private static SMPPSession instance;
	private static final Logger LOGGER = LoggerFactory.getLogger(SendSms.class);

	private PSSMPPSession() {
	}

	public static SMPPSession getInstance(SmppProperties smppParameters) throws IOException {
		if (instance == null || instance.getSessionState() == SessionState.CLOSED) {
			instance = new SMPPSession();
			try {
				if (!instance.getSessionState().isBound()) {
					LOGGER.info("Creating New SMPP Session");
					instance.connectAndBind(smppParameters.getHost(), Integer.valueOf(smppParameters.getPort()), new BindParameter(BindType.BIND_TRX, smppParameters.getSystemId(),
							smppParameters.getPassword(), smppParameters.getSystemType(), TypeOfNumber.UNKNOWN, NumberingPlanIndicator.UNKNOWN, null));
				}
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
				throw e;
			}
		}
		return instance;
	}
}
