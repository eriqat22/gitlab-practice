package com.progressoft.mpay.smssmpp;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.MPayCryptographer.DefaultCryptographer;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.sms.NotificationRequest;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
public class SendSms implements Processor {
    public static final String SUCCESS_MESSAGE = "SUCCESS";
    public static final String FAILED_MESSAGE = "FAIL";
    private static final Logger LOGGER = LoggerFactory.getLogger(SendSms.class);
    private static JAXBContext jaxbContext;

    @Autowired
    private SmppProperties smppProperties;

    private static JAXBContext getJaxbContext() throws JAXBException {
        if (jaxbContext == null)
            jaxbContext = JAXBContext.newInstance(NotificationRequest.class);
        return jaxbContext;
    }
    @Override
    public void process(Exchange exchange)  {
        try {
            Unmarshaller unmarshaller = getJaxbContext().createUnmarshaller();
            NotificationRequest request = getNotificationFromBody(exchange, unmarshaller);
            String extra1 = request.getExtra1();
            String messageBody = request.getContent();
            List<String> decryptedList = Arrays.asList(smppProperties.getEncryptedNotificationTypes().split(","));
            if ((isNeedToDeycrypt(decryptedList, extra1)) || isNeedToDeycrypt(decryptedList, request.getExtra2())) {
                messageBody = setMessageBodyWithDecryptedPin(request);
            }
            String url = SystemParameters.getInstance().getSmppUrl();
            String serviceId =SystemParameters.getInstance().getSmsServiceId() ;
            if (url == null || url.trim().isEmpty())
                return;
            String body = getBody(request, messageBody, serviceId);
            StringEntity stringEntity = new StringEntity(body, "UTF-8");
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(stringEntity);
            httpPost.addHeader("Content-Type", "text/xml; charset=UTF-8");
            HttpClient httpClient = HttpClients.createDefault();
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity entity = httpResponse.getEntity();
            String response = EntityUtils.toString(entity);
            Document document = loadXMLFromString(response);
            String result = document.getElementsByTagName("SendGeneralSMSMsgResult").item(0).getChildNodes().item(0).getNodeValue();
            if (!result.startsWith("0")) {
                exchange.getIn().setBody(FAILED_MESSAGE);
                throw new Exception("failed to send SMS");
            }else
                exchange.getIn().setBody(SUCCESS_MESSAGE);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            exchange.setProperty("errorDescription", e.getMessage());
            exchange.getIn().setBody(FAILED_MESSAGE);
        }
    }
    private String getBody(NotificationRequest request, String messageBody, String serviceId) {
        return "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soap:Body>\n" +
                "    <SendGeneralSMSMsg xmlns=\"http://tempuri.org/\">\n" +
                "      <SrvID>" + serviceId + "</SrvID>\n" +
                "      <MsgText>" +messageBody + "</MsgText>\n" +
                "      <MsgMobNo>" + request.getReciever().substring(2) + "</MsgMobNo>\n" +
                "    </SendGeneralSMSMsg>\n" +
                "  </soap:Body>\n" +
                "</soap:Envelope>";
    }
    private boolean isNeedToDeycrypt(List<String> decryptedList, String extra) {
        return Objects.nonNull(extra) && decryptedList.contains(extra);
    }
    private String setMessageBodyWithDecryptedPin(NotificationRequest request) {
        String messageBody;
        String encryptedOtp = request.getContent().substring(request.getContent().indexOf("\"") + 1,
                request.getContent().lastIndexOf("\""));
        String decreyptedOtp = DefaultCryptographer.INSTANCE.get().decrypt(encryptedOtp);
        messageBody = request.getContent().replace("\"" + encryptedOtp + "\"", decreyptedOtp);
        return messageBody;
    }
    private NotificationRequest getNotificationFromBody(Exchange exchange, Unmarshaller unmarshaller)
            throws JAXBException {
        return (NotificationRequest) unmarshaller
                .unmarshal(new StringReader(String.valueOf(exchange.getIn().getBody())));
    }
    private Document loadXMLFromString(String xml) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            throw new MessageParsingException("Failed to parse Message", e);
        }
    }
}
