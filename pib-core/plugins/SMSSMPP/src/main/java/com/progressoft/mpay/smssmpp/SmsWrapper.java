package com.progressoft.mpay.smssmpp;

import java.util.Random;

import org.jsmpp.bean.Alphabet;
import org.jsmpp.bean.ESMClass;
import org.jsmpp.bean.GeneralDataCoding;
import org.jsmpp.bean.MessageClass;
import org.jsmpp.bean.NumberingPlanIndicator;
import org.jsmpp.bean.RegisteredDelivery;
import org.jsmpp.bean.SMSCDeliveryReceipt;
import org.jsmpp.bean.TypeOfNumber;
import org.jsmpp.session.SMPPSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmsWrapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendSms.class);

	public String sendMessage(String receiverMobileNumber, String messageBody, SmppProperties smppParameters) {
		String messageID = null;
		try {
			SMPPSession session = PSSMPPSession.getInstance(smppParameters);
			if (session == null)
				throw new NullPointerException("smpp session   ");

			session.setTransactionTimer(Long.parseLong(smppParameters.getTimeout()));

			byte[] textBytes = messageBody.getBytes(smppParameters.getEncoding());
			int maximumMultipartMessageSegmentSize = Integer.parseInt(smppParameters.getSegmentLength());
			byte[] byteSingleMessage = textBytes;
			byte[][] byteMessagesArray = splitUnicodeMessage(byteSingleMessage, maximumMultipartMessageSegmentSize);

			for (int i = 0; i < byteMessagesArray.length; i++) {
				messageID = session.submitShortMessage(smppParameters.getServiceType(), TypeOfNumber.valueOf(smppParameters.getSourceAddressTon()),
						NumberingPlanIndicator.valueOf(smppParameters.getSourceAddressNpi()), smppParameters.getSourceAddress(),
						TypeOfNumber.valueOf(smppParameters.getDestinationAddressTon()), NumberingPlanIndicator.valueOf(smppParameters.getDestinationAddressNpi()),
						receiverMobileNumber, new ESMClass((byte)0x40), (byte) 0, (byte) 1, null, null, new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT), (byte) 0,
						new GeneralDataCoding(false, true, MessageClass.CLASS1, Alphabet.valueOf(smppParameters.getDataCoding())), (byte) 0,
						byteMessagesArray[i]);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return messageID;
	}

	private static byte[][] splitUnicodeMessage(byte[] aMessage, Integer maximumMultipartMessageSegmentSize) {
		final byte UDHIE_HEADER_LENGTH = 0x05;
		final byte UDHIE_IDENTIFIER_SAR = 0x00;
		final byte UDHIE_SAR_LENGTH = 0x03;
		// determine how many messages have to be sent
		int numberOfSegments = aMessage.length / maximumMultipartMessageSegmentSize;
		int messageLength = aMessage.length;
		if (numberOfSegments > 255) {
			numberOfSegments = 255;
			messageLength = numberOfSegments * maximumMultipartMessageSegmentSize;
		}
		if ((messageLength % maximumMultipartMessageSegmentSize) > 0) {
			numberOfSegments++;
		}
		// prepare array for all of the msg segments
		byte[][] segments = new byte[numberOfSegments][];
		int lengthOfData;
		// generate new reference number
		byte[] referenceNumber = new byte[1];
		new Random().nextBytes(referenceNumber);
		// split the message adding required headers
		for (int i = 0; i < numberOfSegments; i++) {
			if (numberOfSegments - i == 1) {
				lengthOfData = messageLength - i * maximumMultipartMessageSegmentSize;
			} else {
				lengthOfData = maximumMultipartMessageSegmentSize;
			}
			// new array to store the header
			segments[i] = new byte[6 + lengthOfData];
			// UDH header
			// doesn't include itself, its header length
			segments[i][0] = UDHIE_HEADER_LENGTH;
			// SAR identifier
			segments[i][1] = UDHIE_IDENTIFIER_SAR;
			// SAR length
			segments[i][2] = UDHIE_SAR_LENGTH;
			// reference number (same for all messages)
			segments[i][3] = referenceNumber[0];
			// total number of segments
			segments[i][4] = (byte) numberOfSegments;
			// segment number
			segments[i][5] = (byte) (i + 1);
			// copy the data into the array
			System.arraycopy(aMessage, (i * maximumMultipartMessageSegmentSize), segments[i], 6, lengthOfData);
		}
		return segments;
	}

}
