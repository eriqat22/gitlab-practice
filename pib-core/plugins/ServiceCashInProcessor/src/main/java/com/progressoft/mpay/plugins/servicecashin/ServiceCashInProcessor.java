package com.progressoft.mpay.plugins.servicecashin;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.plugins.servicecashin.integration.ServiceCashInResponseProcessor;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class ServiceCashInProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(ServiceCashInProcessor.class);

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        try {
            logger.debug("Inside ProcessIntegration ...");
            IntegrationProcessingResult result = ServiceCashInResponseProcessor.processIntegration(context);
            handleCommissions(context, result.getProcessingStatus());
            return result;
        } catch (Exception e) {
            logger.error("Error when ProcessIntegration in CashInProcessor", e);
            return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null, false);
        }
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult validationResult = ServiceCashInValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in ServiceCashInProcessor", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        if (context.getTransaction() != null) {
            TransactionHelper.reverseTransaction(context);
        }
        return MessageProcessingResult.create(context, status, reason, reasonDescription);
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException, WorkflowException {
        logger.debug("Inside AcceptMessage ...");
        ServiceCashInMessage message = (ServiceCashInMessage) context.getRequest();
        MessageProcessingResult result = new MessageProcessingResult();
        MPAY_ProcessingStatus status;
        MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
        String reasonDescription = null;

        context.getMessage().setReason(reason);
        MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO, message.getNotes());
        transaction.setTotalAmount(context.getAmount());
        transaction.setOriginalAmount(transaction.getTotalAmount().subtract(transaction.getReceiverCharge().add(transaction.getReceiverTax())));
        result.setMessage(context.getMessage());
        context.setTransaction(transaction);
        result.setTransaction(transaction);
        context.getDataProvider().persistProcessingResult(result);
        JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), transaction);

        if (ArrayUtils.contains(context.getSystemParameters().getAgentTypeCode().split(";"), context.getReceiver().getService().getRefCorporate().getClientType().getCode())) {
            if (postingResult.isSuccess()) {
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
            } else {
                if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                    reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
                    context.getMessage().setReasonDesc(postingResult.getReason());
                } else
                    reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
            }
        } else {
            if (postingResult.isSuccess())
                status = handleMPClear(context, result);
            else {
                if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                    reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
                    context.getMessage().setReasonDesc(postingResult.getReason());
                } else
                    reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
            }
        }
        context.getMessage().setProcessingStatus(status);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
        transaction.setReason(context.getMessage().getReason());
        transaction.setReasonDesc(context.getMessage().getReasonDesc());
        result.setStatus(status);
        result.setStatusDescription(context.getMessage().getReasonDesc());
        OTPHanlder.removeOTP(context);
        handleLimits(context, status, transaction);
        handleCommissions(context, status);
        handleServiceCashInEntity(context, result, status, transaction);
        return result;
    }

    private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status, MPAY_Transaction transaction) throws SQLException {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED) || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
            context.getDataProvider().updateAccountLimit(context.getReceiver().getAccount().getId(), context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
                    SystemHelper.getCurrentDateWithoutTime());
        }
    }

    private void handleServiceCashInEntity(MessageProcessingContext context, MessageProcessingResult result, MPAY_ProcessingStatus status, MPAY_Transaction transaction)
            throws WorkflowException, SQLException {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)) {

            if (context.getMessage().getRefMessageServiceCashIn() != null && !context.getMessage().getRefMessageServiceCashIn().isEmpty()) {
                MPAY_ServiceCashIn cashIn = context.getMessage().getRefMessageServiceCashIn().get(0);
                JfwHelper.executeActionWithServiceUser(MPAYView.SERVICE_CASH_IN, cashIn.getId(), "SVC_Accept");
            }
            result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
            context.getDataProvider().updateAccountLimit(context.getReceiver().getAccount().getId(), context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
                    SystemHelper.getCurrentDateWithoutTime());
        }
    }

    private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
        if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            return;
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.OUTWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
                    CommissionDirections.SENDER);
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.INWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
                    CommissionDirections.RECEIVER);
    }

    private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, MessageProcessingResult result) {
        Integer mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType);
        MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegType));
        messageLog.setRefMessage(context.getMessage());
        result.setMpClearMessage(messageLog);
        return status;
    }

    private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type) {
        logger.debug("Inside CreateMPClearRequest ...");
        try {
            ServiceCashInMessage request = (ServiceCashInMessage) context.getRequest();
            String messageID = context.getDataProvider().getNextMPClearMessageId();
            BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
            IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(type);
            message.setBinary(false);
            if (context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_CREDIT))
                message.setField(MPClearCommonFields.PROCESSING_CODE, new IsoValue<String>(IsoType.NUMERIC, String.valueOf(ProcessingCodeRanges.CREDIT_START_VALUE), 6));
            else
                message.setField(MPClearCommonFields.PROCESSING_CODE, new IsoValue<String>(IsoType.NUMERIC, String.valueOf(ProcessingCodeRanges.DEBIT_START_VALUE), 6));
            String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
            String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

            message.setField(4, new IsoValue<>(IsoType.NUMERIC, formatedAmount, 12));
            MPClearHelper.setMessageEncoded(message);
            message.setField(7, new IsoValue<>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
            message.setField(46, new IsoValue<>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
            message.setField(49, new IsoValue<>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

            String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getService(), context.getSender().getServiceAccount(), false);
            MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

            String account2;
            if (context.getReceiver().getService() == null)
                account2 = MPClearHelper.getAccount(context.getSystemParameters(), request.getReceiver(), request.getReceiverType());
            else
                account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getService(), context.getReceiver().getServiceAccount(), false);

            MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

            String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
            message.setField(104, new IsoValue<>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
            message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));

            return message;
        } catch (Exception e) {
            logger.error("Error when CreateMPClearRequest", e);
            return null;
        }
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, SQLException {
        logger.debug("Inside PreProcessMessage ...");
        ServiceCashInMessage message = ServiceCashInMessage.parseMessage(context.getRequest());

        context.setRequest(message);
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setAmount(message.getAmount());
        sender.setInfo(message.getSender());
        context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return;
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), message.getSenderAccount()));
        if (sender.getServiceAccount() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        sender.setBank(sender.getServiceAccount().getBank());
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        sender.setBanked(sender.getAccount().getIsBanked());
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

        receiver.setInfo(message.getReceiver());
        receiver.setService(context.getDataProvider().getCorporateService(message.getReceiver(), message.getReceiverType()));
        if (receiver.getService() == null)
            return;
        receiver.setCorporate(receiver.getService().getRefCorporate());
        receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), receiver.getService(), message.getReceiverAccount()));
        if (receiver.getServiceAccount() == null)
            return;
        receiver.setAccount(receiver.getServiceAccount().getRefAccount());
        receiver.setBank(receiver.getServiceAccount().getBank());
        receiver.setProfile(receiver.getServiceAccount().getRefProfile());
        receiver.setBanked(receiver.getAccount().getIsBanked());
        receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
        receiver.setLimits(context.getDataProvider().getAccountLimits(receiver.getAccount().getId(), context.getOperation().getMessageType().getId()));
        context.setDirection(TransactionDirection.ONUS);
        context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(), sender.isBanked(), receiver.getCorporate().getClientType().getId(),
                receiver.isBanked(), context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
        if (context.getTransactionConfig() != null)
            context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
        OTPHanlder.loadOTP(context, message.getPin());
        if (message.getChargesAmount() == null)
            ChargesCalculator.calculate(context);
        else
            receiver.setCharge(message.getChargesAmount());
        if (message.getTaxesAmount() == null)
            TaxCalculator.claculate(context);
        else
            receiver.setTax(message.getTaxesAmount());
        handlePushNotification(context);
    }

    private void handlePushNotification(MessageProcessingContext context) {
        String receiverDeviceToken = context.getReceiver().getService().getRefCorporateServiceCorporateDevices()
                .stream()
                .filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CorporateDevice::getExtraData).orElse(null);
        context.setReceiverNotificationToken(receiverDeviceToken);
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside Reverse ...");
        try {
            ServiceCashInMessage message = ServiceCashInMessage.parseMessage(context.getRequest());
            ProcessingContextSide receiver = new ProcessingContextSide();
            receiver.setInfo(message.getReceiver());
            receiver.setService(context.getDataProvider().getCorporateService(message.getReceiver(), message.getReceiverType()));
            receiver.setCorporate(receiver.getService().getRefCorporate());
            receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), receiver.getService(), message.getReceiverAccount()));
            receiver.setAccount(receiver.getServiceAccount().getRefAccount());
            context.setReceiver(receiver);
            JVPostingResult result = TransactionHelper.reverseTransaction(context);
            context.getDataProvider().updateAccountLimit(context.getReceiver().getAccount().getId(),
                    context.getMessage().getRefOperation().getMessageType().getId(), context.getTransaction().getTotalAmount().negate(), -1,
                    SystemHelper.getCurrentDateWithoutTime());

            context.getDataProvider().mergeTransaction(context.getTransaction());
            if (result.isSuccess())
                return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

            return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
        } catch (Exception ex) {
            logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
            return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), ProcessingStatusCodes.FAILED);
        }
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        if (context.getMessage() != null) {
            context.getMessage().setReason(reason);
            context.getMessage().setReasonDesc(reasonDescription);
            context.getMessage().setProcessingStatus(status);
        }
        if (context.getTransaction() != null) {
            context.getTransaction().setReason(reason);
            context.getTransaction().setReasonDesc(reasonDescription);
            context.getTransaction().setProcessingStatus(status);
        }
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setTransaction(context.getTransaction());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        if (reasonCode.equals(ReasonCodes.VALID)) {
            result.setNotifications(NotificationProcessor.createReversalNotificationMessages(context));
        }
        return result;
    }
}
