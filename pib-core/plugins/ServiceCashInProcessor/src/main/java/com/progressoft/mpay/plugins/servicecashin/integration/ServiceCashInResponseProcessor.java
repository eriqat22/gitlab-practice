package com.progressoft.mpay.plugins.servicecashin.integration;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entities.MPAY_ServiceCashIn;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.servicecashin.NotificationProcessor;
import com.progressoft.mpay.plugins.servicecashin.ServiceCashInMessage;

/**
 * Created by user on 20/12/17.
 */
public class ServiceCashInResponseProcessor {
    private static final Logger logger = LoggerFactory.getLogger(ServiceCashInResponseProcessor.class);

    private ServiceCashInResponseProcessor() {

    }

    public static IntegrationProcessingResult processIntegration(IntegrationProcessingContext context)
            throws WorkflowException {
        try {
            logger.debug("Inside ProcessIntegration ...");
            preProcessIntegration(context);
            if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return null;
            if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return accept(context);
            else
                return reject(context);
        } catch (Exception ex) {
            logger.error("Error when ProcessIntegration in CashInResponseProcessor", ex);
            if (context.getTransaction() != null) {
                TransactionHelper.reverseTransaction(context);
                try {
                    context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
                            context.getTransactionConfig().getMessageType().getId(),
                            BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1,
                            SystemHelper.getCurrentDateWithoutTime());
                } catch (SQLException e) {
                    logger.error("Failed to reverse limits", e);
                }
            }
            IntegrationProcessingResult result = IntegrationProcessingResult.create(context,
                    ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), null, false);
            if (!context.getMessage().getRefMessageServiceCashIn().isEmpty()) {
                MPAY_ServiceCashIn serviceCashIn = context.getMessage().getRefMessageServiceCashIn().get(0);
                serviceCashIn.setProcessingStatus(result.getProcessingStatus());
                JfwHelper.executeActionWithServiceUser(MPAYView.SERVICE_CASH_IN, serviceCashIn.getId(), "SVC_Reject");
                serviceCashIn.setProcessingStatus(result.getProcessingStatus());
                serviceCashIn.setReason(result.getReason());
                serviceCashIn.setReasonDesc(result.getReasonDescription());
            }

            return result;
        }
    }

    private static void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
        logger.debug("Inside PreProcessIntegration ...");
        if (context.getMessage() != null)
            context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        ServiceCashInMessage request = null;
        try {
            request = ServiceCashInMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
        } catch (MessageParsingException ex) {
            logger.error("Error while parsing message", ex);
            throw new WorkflowException(ex);
        }
        context.setAmount(request.getAmount());
        context.setSender(sender);
        context.setReceiver(receiver);

        sender.setService(context.getTransaction().getSenderService());
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(context.getTransaction().getSenderServiceAccount());
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        sender.setBanked(sender.getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
        sender.setCharge(context.getTransaction().getSenderCharge());
        sender.setTax(context.getTransaction().getSenderTax());
        sender.setInfo(sender.getService().getName());
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

        receiver.setService(context.getTransaction().getReceiverService());
        receiver.setCorporate(receiver.getService().getRefCorporate());
        receiver.setServiceAccount(context.getTransaction().getReceiverServiceAccount());
        receiver.setAccount(receiver.getServiceAccount().getRefAccount());
        receiver.setBanked(receiver.getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
        receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
                receiver.getAccount().getAccNumber()));
        receiver.setProfile(receiver.getServiceAccount().getRefProfile());
        receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
        receiver.setCharge(context.getTransaction().getReceiverCharge());
        receiver.setTax(context.getTransaction().getReceiverTax());

        context.setTransactionConfig(
                context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(),
                        sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(),
                        context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT,
                        context.getOperation().getId()));
        handlePushNotification(context);
    }

    private static void handlePushNotification(IntegrationProcessingContext context) {
        String receiverDeviceToken = context.getReceiver().getService().getRefCorporateServiceCorporateDevices()
                .stream()
                .filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CorporateDevice::getExtraData).orElse(null);
        context.setReceiverNotificationToken(receiverDeviceToken);
    }

    private static IntegrationProcessingResult accept(IntegrationProcessingContext context) throws WorkflowException {
        logger.debug("Inside Accept ...");
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED,
                ReasonCodes.VALID, null, null, false);
        result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
        if (!context.getMessage().getRefMessageServiceCashIn().isEmpty()) {
            MPAY_ServiceCashIn serviceCashIn = context.getMessage().getRefMessageServiceCashIn().get(0);
            serviceCashIn.setProcessingStatus(result.getProcessingStatus());
            JfwHelper.executeActionWithServiceUser(MPAYView.SERVICE_CASH_IN, serviceCashIn.getId(), "SVC_Accept");
        }

        return result;
    }

    private static IntegrationProcessingResult reject(IntegrationProcessingContext context)
            throws SQLException, WorkflowException {
        logger.debug("Inside Reject ...");
        BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
        if (integrationResult != null)
            TransactionHelper.reverseTransaction(context);
        String reasonDescription = null;
        if (context.getMpClearIsoMessage().getField(47) != null)
            reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED,
                ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
                context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
        context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
                context.getTransactionConfig().getMessageType().getId(),
                BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1,
                SystemHelper.getCurrentDateWithoutTime());
        if (!context.getMessage().getRefMessageServiceCashIn().isEmpty()) {
            MPAY_ServiceCashIn serviceCashIn = context.getMessage().getRefMessageServiceCashIn().get(0);
            serviceCashIn.setProcessingStatus(result.getProcessingStatus());
            serviceCashIn.setReason(result.getReason());
            serviceCashIn.setReasonDesc(result.getReasonDescription());
            JfwHelper.executeActionWithServiceUser(MPAYView.SERVICE_CASH_IN, serviceCashIn.getId(), "SVC_Reject");
        }
        result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));

        return result;
    }
}
