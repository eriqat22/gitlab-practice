package com.progressoft.mpay.plugins.servicecashin;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    private NotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateAcceptanceNotificationMessages ...");
            String reference;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            MPAY_CorpoarteService receiverService = context.getReceiver().getService();
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (receiverService != null) {
                final MPAY_Language prefLang = context.getReceiver().getCorporate().getPrefLang();
                long receiverLanguageId = prefLang.getId();
                ServiceCashInNotificationContext notificationContext = new ServiceCashInNotificationContext();
                notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
                notificationContext.setReference(reference);
                context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getAccount().getBalance()));
                notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
                notificationContext.setReceiverTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getTax()));
                notificationContext.setExtraData2(String.valueOf(receiverLanguageId));
                notificationContext.setExtraData1(receiverService.getName());

                addNotification(context.getMessage(), notifications, receiverService.getEmail(), prefLang,
                        receiverService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, receiverService.getMobileNumber(), prefLang,
                        receiverService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                        receiverService.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_RECEIVER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error in CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            ServiceCashInMessage request = ServiceCashInMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            String receiverService = request.getReceiver();
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();
            MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(receiverService, request.getReceiverType());
            if (service != null) {
                receiverService = service.getNotificationReceiver();
                if (receiverService == null)
                    return new ArrayList<>();
                language = service.getRefCorporate().getPrefLang();
            }
            ServiceCashInNotificationContext notificationContext = new ServiceCashInNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
            notificationContext.setExtraData2(String.valueOf(language));
            notificationContext.setExtraData1(service.getName());

            if (service != null) {
                addNotification(context.getMessage(), notifications, service.getEmail(), language,
                        service.getEnableEmail(), notificationContext, NotificationType.REJECTED_RECEIVER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, service.getMobileNumber(), language,
                        service.getEnableSMS(), notificationContext, NotificationType.REJECTED_RECEIVER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), language,
                        service.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_RECEIVER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error in CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(MessageProcessingContext context) {
        logger.debug("Inside CreateReversalNotificationMessages ...");
        try {
            String reference;
            List<MPAY_Notification> notifications = new ArrayList<>();

            reference = context.getTransaction().getReference();
            MPAY_CorpoarteService senderService = context.getTransaction().getSenderService();
            String currency = context.getTransaction().getCurrency().getStringISOCode();
            if (senderService != null) {
                long senderLanguageId;
                final MPAY_Language prefLang = senderService.getRefCorporate().getPrefLang();
                ServiceCashInNotificationContext senderNotificationContext = new ServiceCashInNotificationContext();
                senderNotificationContext.setCurrency(currency);
                senderNotificationContext.setReference(reference);
                MPAY_Account senderAccount = context.getTransaction().getSenderServiceAccount().getRefAccount();
                context.getDataProvider().refreshEntity(senderAccount);
                senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
                senderLanguageId = prefLang.getId();
                senderNotificationContext.setExtraData2(String.valueOf(senderLanguageId));
                senderNotificationContext.setExtraData1(senderService.getName());

                addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
                        senderService.getEnableEmail(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
                        senderService.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
                        senderService.getEnablePushNotification(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateReversalNotificationMessages", e);
            return new ArrayList<>();
        }
    }
}
