package com.progressoft.mpay.plugins.merchantpaymentcancellation;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class MerchantPayCancelMessage extends MPayRequest {

	private static final String TRANSACTION_REF_KEY = "transactionRef";

	private String transactionRef;

	public MerchantPayCancelMessage() {
		//
	}

	public MerchantPayCancelMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static MerchantPayCancelMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		MerchantPayCancelMessage message = new MerchantPayCancelMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || message.getSenderType().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setTransactionRef(message.getValue(TRANSACTION_REF_KEY));
		if (message.getTransactionRef() == null || message.getTransactionRef().trim().length() == 0)
			throw new MessageParsingException(TRANSACTION_REF_KEY);

		return message;
	}

	public String getTransactionRef() {
		return transactionRef;
	}

	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}
}