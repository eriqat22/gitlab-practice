package com.progressoft.mpay.plugins.merchantpaymentcancellation;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateAcceptanceNotificationMessages ...");
		try {

			String reference;
			long senderLanguageId;
			long receiverLanguageId;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();

			MPAY_CustomerMobile senderMobile;
			MPAY_CorpoarteService receiverService;
			MPAY_Account senderAccount;
			MPAY_Account receiverAccount;
			if (context.getTransaction().getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT)) {
				senderMobile = context.getSender().getMobile();
				senderAccount = context.getSender().getMobileAccount().getRefAccount();
				receiverService = context.getReceiver().getService();
				receiverAccount = context.getReceiver().getServiceAccount().getRefAccount();
			} else {
				senderMobile = context.getReceiver().getMobile();
				senderAccount = context.getReceiver().getMobileAccount().getRefAccount();
				receiverService = context.getSender().getService();
				receiverAccount = context.getSender().getServiceAccount().getRefAccount();
			}
			context.getDataProvider().refreshEntity(senderAccount);
			context.getDataProvider().refreshEntity(receiverAccount);
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

			MerchantPayCancelNotificationContext notificationContext = new MerchantPayCancelNotificationContext();
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);
			String receiver;
			String sender;
			if (senderMobile != null) {
				sender = senderMobile.getMobileNumber();

				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
				notificationContext.setSenderBanked(senderAccount.getIsBanked());
				notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
				senderLanguageId = senderMobile.getRefCustomer().getPrefLang().getId();
				if (senderMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS) && senderMobile.getAlias() != null && !senderMobile.getAlias().isEmpty())
					sender = senderMobile.getAlias();
			} else {
				sender = context.getSender().getInfo();
				senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			if (receiverService != null) {
				receiver = receiverService.getName();

				if (context.getExtraData().get(MerchantPayCancelProcessor.NOTIFICATION_TOKEN) != null)
					notificationContext.setExtraData1((String) context.getExtraData().get(MerchantPayCancelProcessor.NOTIFICATION_TOKEN));

				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
				notificationContext.setReceiverBanked(receiverAccount.getIsBanked());
				notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
				receiverLanguageId = receiverService.getRefCorporate().getPrefLang().getId();
			} else {
				receiver = context.getReceiver().getInfo();
				receiverLanguageId = senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			notificationContext.setReceiver(receiver);
			notificationContext.setSender(sender);
			return generateNotifications(context, senderLanguageId, receiverLanguageId, senderMobile, receiverService, operation, notificationContext);

		} catch (Exception e) {
			logger.error("Error when CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static List<MPAY_Notification> generateNotifications(ProcessingContext context, long senderLanguageId, long receiverLanguageId, MPAY_CustomerMobile senderMobile, MPAY_CorpoarteService receiverService, MPAY_EndPointOperation operation,
			MerchantPayCancelNotificationContext notificationContext) {
		List<MPAY_Notification> notifications = new ArrayList<>();
		if (senderMobile != null) {
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId, senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		if (receiverService != null) {
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId, receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
		}
		return notifications;
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			String notificationChannel = NotificationChannelsCode.SMS;
			MerchantPayCancelMessage request = MerchantPayCancelMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String sender = request.getSender();
			MPAY_Language language = context.getSystemParameters().getSystemLanguage();
			if (request.getSenderType().equals(ReceiverInfoType.MOBILE)) {
				MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
				if (mobile != null) {
					language = mobile.getRefCustomer().getPrefLang();
				}
			} else {
				notificationChannel = NotificationChannelsCode.EMAIL;
				MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
				if (service != null) {
					language = service.getRefCorporate().getPrefLang()
					;
				}
			}
			MerchantPayCancelNotificationContext notificationContext = new MerchantPayCancelNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_SENDER, context.getMessage().getRefOperation().getId(), language.getId(), sender, notificationChannel));
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}

}
