package com.progressoft.mpay.plugins.merchantpaymentcancellation;

import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;

public class MerchantPayCancelProcessor implements MessageProcessor {
	public static final String NOTIFICATION_TOKEN = "notificationToken";

	private static final Logger logger = LoggerFactory.getLogger(MerchantPayCancelProcessor.class);
	protected EntityManager em;

	@PersistenceContext(unitName = "JFWUnit")
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessMessage(context);
			ValidationResult validationResult = MerchantPayCancelValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());

		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage", ex);
			return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = reverseTransaction(context);
		prepareResponseContent(context, result);
		OTPHanlder.removeOTP(context);
		return result;
	}

	private MessageProcessingResult reverseTransaction(MessageProcessingContext context) {
		logger.debug("Inside Reverse ...");
		try {
			if (context.getTransaction().getIsReversed())
				return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
			JVPostingResult result = TransactionHelper.reverseTransaction(context);
			context.getDataProvider().mergeTransaction(context.getTransaction());
			if (result.isSuccess())
				return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

			return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
		} catch (Exception ex) {
			logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
			return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), ProcessingStatusCodes.FAILED);
		}
	}

	private void prepareResponseContent(MessageProcessingContext context, MessageProcessingResult result) {
		String response = generateResponse(context);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		return createMessageProcessingResult(context, status, reason, reasonDescription);
	}

	private MessageProcessingResult createMessageProcessingResult(MessageProcessingContext context, MPAY_ProcessingStatus status, MPAY_Reason reason, String reasonDescription) {
		MessageProcessingResult result = new MessageProcessingResult();
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		result.setReasonCode(reason.getCode());
		if (context.getMessage() != null) {
			context.getMessage().setProcessingStatus(status);
			context.getMessage().setReason(reason);
			context.getMessage().setReasonDesc(reasonDescription);
			result.setMessage(context.getMessage());
		}

		return result;
	}

	private String generateResponse(MessageProcessingContext context) {
		logger.debug("Inside GenerateResponse ...");
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		setSystemLanguage(context);
		String description = getReasonDescription(context);
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());

		return response.toString();
	}

	private void setSystemLanguage(MessageProcessingContext context) {
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
	}

	private String getReasonDescription(MessageProcessingContext context) {
		String description;
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		return description;
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		prepareResponseReason(context, reasonCode, reasonDescription);
		MPAY_ProcessingStatus status = prepareProcessingStatus(context, processingStatus);
		return prepareProcessingResult(context, reasonCode, reasonDescription, status);
	}

	private MPAY_ProcessingStatus prepareProcessingStatus(MessageProcessingContext context, String processingStatus) {
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setProcessingStatus(status);
		return status;
	}

	private void prepareResponseReason(MessageProcessingContext context, String reasonCode, String reasonDescription) {
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
	}

	private MessageProcessingResult prepareProcessingResult(MessageProcessingContext context, String reasonCode, String reasonDescription, MPAY_ProcessingStatus status) {
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException, SQLException {
		logger.debug("Inside PreProcessMessage ...");
		MerchantPayCancelMessage message = MerchantPayCancelMessage.parseMessage(context.getRequest());
		context.setRequest(message);
		context.setSender(new ProcessingContextSide());
		context.setReceiver(new ProcessingContextSide());
		context.setTransaction(context.getDataProvider().getTransactionByTransactionRef(message.getTransactionRef()));
		if (context.getTransaction() == null)
			return;
		prepareSenderAndReceiver(context, message);
		OTPHanlder.loadOTP(context, message.getPin());
	}

	private void prepareSenderAndReceiver(MessageProcessingContext context, MerchantPayCancelMessage message) throws MessageParsingException, SQLException, WorkflowException {
		if (context.getTransaction().getRefType().getCode().equals(TransactionTypeCodes.DIRECT_DEBIT)) {
			prepareContextCustomerReceiverSide(context);
			prepareContextMerchantSenderSide(context, message);
		} else if (context.getTransaction().getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT)) {
			prepareContextCustomerSenderSide(context);
			prepareContextMerchantReceiverSide(context, message);
		}
	}

	private void prepareContextMerchantSenderSide(MessageProcessingContext context, MerchantPayCancelMessage message) throws SQLException {
		context.getSender().setInfo(message.getSender());
		context.getSender().setService(context.getTransaction().getSenderService());
		if (context.getSender().getService() == null)
			return;
		context.getSender().setServiceAccount(context.getTransaction().getReceiverServiceAccount());
		if (context.getSender().getServiceAccount() == null)
			return;
		context.getSender().setCorporate(context.getSender().getService().getRefCorporate());
		context.getSender().setAccount(context.getSender().getServiceAccount().getRefAccount());
		context.getSender().setBank(context.getSender().getServiceAccount().getBank());
		context.getSender().setProfile(context.getSender().getServiceAccount().getRefProfile());
		context.getSender().setBanked(context.getSender().getAccount().getIsBanked());
		context.getSender().setLimits(context.getDataProvider().getAccountLimits(context.getSender().getService().getId(), context.getOperation().getMessageType().getId()));
	}

	private void prepareContextMerchantReceiverSide(MessageProcessingContext context, MerchantPayCancelMessage message) throws SQLException {
		context.getReceiver().setInfo(message.getSender());
		context.getReceiver().setService(context.getTransaction().getReceiverService());
		if (context.getReceiver().getService() == null)
			return;
		context.getReceiver().setServiceAccount(context.getTransaction().getReceiverServiceAccount());
		if (context.getReceiver().getServiceAccount() == null)
			return;
		context.getReceiver().setCorporate(context.getReceiver().getService().getRefCorporate());
		context.getReceiver().setAccount(context.getReceiver().getServiceAccount().getRefAccount());
		context.getReceiver().setBank(context.getReceiver().getServiceAccount().getBank());
		context.getReceiver().setProfile(context.getReceiver().getService().getRefProfile());
		context.getReceiver().setBanked(context.getReceiver().getAccount().getIsBanked());
		context.getReceiver().setLimits(context.getDataProvider().getAccountLimits(context.getReceiver().getService().getId(), context.getOperation().getMessageType().getId()));
	}

	private void prepareContextCustomerReceiverSide(MessageProcessingContext context) throws SQLException {
		context.getReceiver().setInfo(context.getTransaction().getReceiverInfo());
		context.getReceiver().setMobile(context.getTransaction().getReceiverMobile());
		if (context.getReceiver().getMobile() == null)
			return;
		context.getReceiver().setMobileAccount(context.getTransaction().getReceiverMobileAccount());
		if (context.getReceiver().getMobileAccount() == null)
			return;
		context.getReceiver().setCustomer(context.getReceiver().getMobile().getRefCustomer());
		context.getReceiver().setAccount(context.getReceiver().getMobileAccount().getRefAccount());
		context.getReceiver().setBank(context.getReceiver().getMobileAccount().getBank());
		context.getReceiver().setProfile(context.getReceiver().getMobile().getRefProfile());
		context.getReceiver().setBanked(context.getReceiver().getAccount().getIsBanked());
		context.getReceiver().setLimits(context.getDataProvider().getAccountLimits(context.getReceiver().getMobile().getId(), context.getOperation().getMessageType().getId()));
	}

	private void prepareContextCustomerSenderSide(MessageProcessingContext context) throws SQLException {
		context.getSender().setInfo(context.getTransaction().getSenderInfo());
		context.getSender().setMobile(context.getTransaction().getSenderMobile());
		if (context.getSender().getMobile() == null)
			return;
		context.getSender().setMobileAccount(context.getTransaction().getSenderMobileAccount());
		if (context.getSender().getMobileAccount() == null)
			return;
		context.getSender().setCustomer(context.getSender().getMobile().getRefCustomer());
		context.getSender().setAccount(context.getSender().getMobileAccount().getRefAccount());
		context.getSender().setBank(context.getSender().getMobileAccount().getBank());
		context.getSender().setProfile(context.getSender().getMobileAccount().getRefProfile());
		context.getSender().setBanked(context.getSender().getAccount().getIsBanked());
		context.getSender().setLimits(context.getDataProvider().getAccountLimits(context.getSender().getMobile().getId(), context.getOperation().getMessageType().getId()));
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		return null;
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext paramIntegrationProcessingContext) {
		return null;
	}
}
