package com.progressoft.mpay.plugins.merchantpaymentcancellation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.InvalidArgumentException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PSPValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;

public class MerchantPayCancelValidator {
	private static final Logger logger = LoggerFactory.getLogger(MerchantPayCancelValidator.class);

	private MerchantPayCancelValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE)) {
			if (context.getTransaction().getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT))
				result = validateForCredit(context);
			else if (context.getTransaction().getRefType().getCode().equals(TransactionTypeCodes.DIRECT_DEBIT))
				result = validateForDebit(context);
			else
				throw new InvalidArgumentException(context.getTransaction().getRefType().getCode() + " transactionType");
			if (result.isValid())
				result = PSPValidator.validate(context, false);
			return result;
		} else
			return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
	}

	private static ValidationResult validateForDebit(MessageProcessingContext context) {
		ValidationResult result = validateCorporate(context, context.getSender());
		if (result.isValid())
			result = validateCustomer(context.getReceiver(),context.getMessage().getMessageType().getIsFinancial());
		return result;
	}

	private static ValidationResult validateTransaction(MessageProcessingContext context, ProcessingContextSide contextSide) {
		ValidationResult result = new ValidationResult(ReasonCodes.VALID, null, true);
		if (!context.getTransaction().getProcessingStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return result;
		if ((context.getTransaction().getSenderService() != null && !contextSide.getService().getName().equals(context.getTransaction().getSenderService().getName()))
				|| (context.getTransaction().getReceiverService() != null && !contextSide.getService().getName().equals(context.getTransaction().getReceiverService().getName())))
			result = new ValidationResult(ReasonCodes.TRANSACTION_NOT_FOUND, null, false);
		return result;
	}

	private static ValidationResult validateForCredit(MessageProcessingContext context) {
		ValidationResult result = validateCorporate(context, context.getReceiver());
		if (result.isValid())
			result = validateCustomer(context.getSender(),context.getMessage().getMessageType().getIsFinancial());
		return result;
	}

	private static ValidationResult validateCustomer(ProcessingContextSide contextSide,boolean isFinancial) {

		ValidationResult result = MobileValidator.validate(contextSide.getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(contextSide.getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(contextSide.getMobileAccount(), true,isFinancial);
		return result;
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context, ProcessingContextSide contextSide) {
		ValidationResult result = ServiceValidator.validate(contextSide.getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(contextSide.getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(contextSide.getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = validateTransaction(context, contextSide);
		if (!result.isValid())
			return result;

		MerchantPayCancelMessage request = (MerchantPayCancelMessage) context.getRequest();

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), request.getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, contextSide.getService(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		result = PinCodeValidator.validate(context, contextSide.getService(), request.getPin());
		return result;
	}
}
