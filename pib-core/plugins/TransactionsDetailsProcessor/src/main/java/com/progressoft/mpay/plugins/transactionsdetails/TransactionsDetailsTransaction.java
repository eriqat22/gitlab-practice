package com.progressoft.mpay.plugins.transactionsdetails;

import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import org.apache.commons.lang.NullArgumentException;

import java.sql.Timestamp;

public class TransactionsDetailsTransaction {
    private static final String CREDIT_TYPE_CODE = "1";
    private long id;
    private double amnt;
    private double commissionAmount;
    private double taxAmount;
    private double totalAmount;
    private double externalFees;
    private String curr;
    private String sender;
    private String receiver;
    private Timestamp date;
    private String desc;
    private long status;
    private long type; // 1:Money Transfer 2:Bill Payment
    // 3:Gov Payment 4:ePay
    // Advice 5:cashIn 6:cashOut
    private long reference;
    private boolean isReversed;
    private String mpClearIntegMsgLogId;
    private String statusDesc;
    private String reasonDescription;


    public TransactionsDetailsTransaction(MPAY_Transaction transaction, String sender, MessageProcessingContext context) {
        if (transaction == null)
            throw new NullArgumentException("transaction");
        this.id = transaction.getId();
        this.desc = transaction.getComments();
        prepareAmounts(transaction, sender);
        this.setCurr(transaction.getCurrency().getStringISOCode());
        if (transaction.getInwardMessage() != null)
            this.mpClearIntegMsgLogId = transaction.getInwardMessage().getMessageId();
        if (transaction.getSenderMobile() != null)
            //this.sender = isByAlias(transaction.getSenderMobile(), sender) ? transaction.getSenderMobile().getAlias() : transaction.getSenderMobile().getMobileNumber();
            this.sender = transaction.getSenderMobile().getMobileNumber();
        else if (transaction.getSenderService() != null)
            this.sender = transaction.getSenderService().getDescription() == null ? transaction.getSenderService().getName() : transaction.getSenderService().getDescription();
        else
            this.sender = transaction.getSenderInfo();

        if (transaction.getReceiverMobile() != null)
            //this.receiver = isByAlias(transaction.getReceiverMobile(), sender) ? transaction.getReceiverMobile().getAlias() : transaction.getReceiverMobile().getMobileNumber();
            this.receiver = transaction.getReceiverMobile().getMobileNumber();
        else if (transaction.getReceiverService() != null)
            this.receiver = transaction.getReceiverService().getDescription() == null ? transaction.getReceiverService().getName() : transaction.getReceiverService().getDescription();
        else
            this.receiver = transaction.getReceiverInfo();

        this.date = transaction.getTransDate();
        this.status = transaction.getProcessingStatus().getId();
        String description = transaction.getProcessingStatus().getDescription();
        this.statusDesc = description != null ? description : "";
        this.type = transaction.getRefOperation().getMessageType().getId();
        this.reference = Long.parseLong(transaction.getReference());
        this.isReversed = transaction.getIsReversed();
        getReasonDescriptionBasedOnNls(transaction, context);
    }

    private void getReasonDescriptionBasedOnNls(MPAY_Transaction transaction, MessageProcessingContext context) {
        MPAY_Reason reason = transaction.getReason();
        if (reason != null) {
            MPAY_Reasons_NLS reasonsNls = reason.getReasonsNLS().get(context.getRequest().getLang() == 1 ? "en" : "ar");
            if (reasonsNls != null)
                reasonDescription = reasonsNls.getDescription();
        }
    }

    private int prepareAmountSign(MPAY_Transaction transaction, String sender) {
        if (transaction.getRefType().getCode().equals(CREDIT_TYPE_CODE))
            return getCreditSign(transaction, sender);
        else
            return getDebitSign(transaction, sender);
    }

    private int getDebitSign(MPAY_Transaction transaction, String sender) {
        if (sender.equals(getTransactionSender(transaction)))
            return 1;
        else
            return -1;
    }

    private int getCreditSign(MPAY_Transaction transaction, String sender) {
        if (sender.equals(getTransactionSender(transaction)))
            return -1;
        else
            return 1;
    }

    private String getTransactionSender(MPAY_Transaction transaction) {
        if (transaction.getSenderMobile() == null && transaction.getSenderService() == null)
            return transaction.getSenderInfo();
        else if (transaction.getSenderMobile() == null)
            return transaction.getSenderService().getName();
        else
            return transaction.getSenderMobile().getMobileNumber();
    }

    private String getTransactionReceiver(MPAY_Transaction transaction) {
        if (transaction.getReceiverMobile() == null && transaction.getReceiverService() == null)
            return transaction.getReceiverInfo();
        else if (transaction.getReceiverMobile() == null)
            return transaction.getReceiverService().getName();
        else
            return transaction.getReceiverMobile().getMobileNumber();
    }

    private void prepareAmounts(MPAY_Transaction transaction, String sender) {
        int amountSign = prepareAmountSign(transaction, sender);
        String transactionSender = getTransactionSender(transaction);
        String transactionReceiver = getTransactionReceiver(transaction);
        this.amnt = transaction.getOriginalAmount().doubleValue();
        if (transaction.getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT)) {
            if (sender.equals(transactionSender)) {
                this.commissionAmount = transaction.getSenderCharge().doubleValue();
                this.taxAmount = transaction.getSenderTax().doubleValue();
                this.totalAmount = transaction.getTotalAmount().doubleValue();
            } else if (sender.equals(transactionReceiver)) {
                this.commissionAmount = transaction.getReceiverCharge().doubleValue();
                this.taxAmount = transaction.getReceiverTax().doubleValue();
                this.totalAmount = transaction.getOriginalAmount().subtract(transaction.getReceiverCharge().add(transaction.getReceiverTax()))
                        .doubleValue();
            }
        } else {
            if (sender.equals(transactionSender)) {
                this.commissionAmount = transaction.getSenderCharge().doubleValue();
                this.taxAmount = transaction.getSenderTax().doubleValue();
                this.totalAmount = transaction.getOriginalAmount().subtract(transaction.getSenderCharge().add(transaction.getSenderTax()))
                        .doubleValue();
            } else if (sender.equals(transactionReceiver)) {
                this.commissionAmount = transaction.getReceiverCharge().doubleValue();
                this.taxAmount = transaction.getReceiverTax().doubleValue();
                this.totalAmount = transaction.getTotalAmount().doubleValue();
            }
        }

        this.amnt = amountSign * this.amnt;
        this.totalAmount = amountSign * transaction.getTotalAmount().doubleValue();

        if (commissionAmount > 0)
            this.commissionAmount = -1 * this.commissionAmount;
        if (this.externalFees > 0)
            this.externalFees = -1 * transaction.getExternalFees().doubleValue();
    }

   public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAmnt() {
        return amnt;
    }

    public void setAmnt(double amnt) {
        this.amnt = amnt;
    }

    public double getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(double commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getExternalFees() {
        return externalFees;
    }

    public void setExternalFees(double externalFees) {
        this.externalFees = externalFees;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public  String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    double getReference() {
        return reference;
    }

    void setReference(long refe) {
        this.reference = refe;
    }

    private boolean isByAlias(MPAY_CustomerMobile mobile, String sender) {
        if (mobile.getMobileNumber().equals(sender))
            return false;
        return mobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) && mobile.getAlias() != null;
    }

    public boolean isReversed() {
        return isReversed;
    }

    public void setReversed(boolean isReversed) {
        this.isReversed = isReversed;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getMpClearIntegMsgLogId() {
        return mpClearIntegMsgLogId;
    }

    public void setMpClearIntegMsgLogId(String mpClearIntegMsgLogId) {
        this.mpClearIntegMsgLogId = mpClearIntegMsgLogId;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

}

