package com.progressoft.mpay.plugins.transactionsdetails;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.entity.TransactionDetailsFilter;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class TransactionsDetailsProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(TransactionsDetailsProcessor.class);

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
        logger.debug("Inside ProcessIntegration ...");
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult validationResult = TransactionsDetailsValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false, new ArrayList<MPAY_Transaction>());
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        logger.debug("Inside AcceptMessage ...");
        TransactionsDetailsMessage message = (TransactionsDetailsMessage) context.getRequest();
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        TransactionDetailsFilter filter = fillFilter(message);
        List<MPAY_Transaction> transactions = context.getDataProvider().searchTransactions(filter);
        String response = generateResponse(context, true, transactions);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private TransactionDetailsFilter fillFilter(TransactionsDetailsMessage message) {
        TransactionDetailsFilter filter = new TransactionDetailsFilter();
        filter.setFromTime(message.getFromTime());
        filter.setToTime(message.getToTime());
        filter.setTxDirection(message.getTxDirection());
        filter.setTxOperation(message.getTxOperation());
        filter.setTxReceiverMobile(message.getTxReceiverMobile());
        filter.setTxReceiverService(message.getTxReceiverService());
        filter.setTxReference(message.getTxReference());
        filter.setTxSenderMobile(message.getTxSenderMobile());
        filter.setTxSenderService(message.getTxSenderService());
        filter.setTxType(message.getTxType());
        return filter;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted, List<MPAY_Transaction> transactions) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        if (isAccepted) {
            List<TransactionsDetailsTransaction> miniTransactions = new ArrayList<>();
            if (!transactions.isEmpty()) {
                String sender = getSender(context);
                for (MPAY_Transaction transaction : transactions)
                    miniTransactions.add(new TransactionsDetailsTransaction(transaction, sender,context));
            }
            response.getExtraData().add(new ExtraData(Constants.TransactionsKey, getListJson(miniTransactions)));
            response.getExtraData().add(new ExtraData(Constants.BalanceKey, SystemHelper.formatAmount(context.getSystemParameters(), context.getDataProvider().getBalance(context.getSender().getAccount().getId()))));
        } else
            response.getExtraData().add(new ExtraData(Constants.TransactionsKey, getListJson(new ArrayList<>())));

        return response.toString();
    }

    private String getSender(MessageProcessingContext context) {
        String sender = null;
        if (context.getSender().getMobile() == null && context.getSender().getService() == null)
            sender = context.getSender().getInfo();
        else if (context.getSender().getMobile() != null)
            sender = context.getSender().getMobile().getMobileNumber();
        else if (context.getSender().getService() != null)
            sender = context.getSender().getService().getName();
        return sender;
    }

    private String getListJson(List<TransactionsDetailsTransaction> transactions) {
        logger.debug("Inside GetListJson ...");
        Gson gson = new GsonBuilder().create();
        return gson.toJson(transactions);
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
        logger.debug("Inside PreProcessMessage ...");
        TransactionsDetailsMessage message = TransactionsDetailsMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setRequest(message);
        if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
            sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
            if (sender.getMobile() == null)
                return;
            sender.setCustomer(sender.getMobile().getRefCustomer());
            sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
            if (sender.getMobileAccount() == null)
                return;
            sender.setProfile(sender.getMobileAccount().getRefProfile());
            sender.setAccount(sender.getMobileAccount().getRefAccount());
        } else {
            sender.setService(context.getDataProvider().getCorporateService(context.getRequest().getSender(), context.getRequest().getSenderType()));
            if (sender.getService() == null)
                return;
            sender.setCorporate(sender.getService().getRefCorporate());
            sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
            if (sender.getServiceAccount() == null)
                return;
            sender.setProfile(sender.getServiceAccount().getRefProfile());
            sender.setAccount(sender.getServiceAccount().getRefAccount());
        }
        OTPHanlder.loadOTP(context, message.getPin());
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext arg0) {
        logger.debug("Inside Reverse ...");
        return null;
    }
}
