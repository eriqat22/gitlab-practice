package com.progressoft.mpay.plugins.transactionsdetails;

import java.sql.Timestamp;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class TransactionsDetailsMessage extends MPayRequest {
	private static final Logger logger = LoggerFactory.getLogger(TransactionsDetailsMessage.class);

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String TX_SENDER_MOBILE_KEY = "txSenderMobile";
	private static final String TX_RECEIVER_MOBILE_KEY = "txReceiverMobile";
	private static final String TX_SENDER_SERVICE_KEY = "txSenderService";
	private static final String TX_RECEIVER_SERVICE_KEY = "txReceiverService";
	private static final String TX_TYPE_KEY = "txType";
	private static final String TX_DIRECTION_KEY = "txDirection";
	private static final String TX_OPERATION_KEY = "txOperation";
	private static final String TX_REFERENCE_KEY = "txReference";
	private static final String FROM_TIME_KEY = "fromTime";
	private static final String TO_TIME_KEY = "toTime";

	private String txSenderMobile;
	private String txReceiverMobile;
	private String txSenderService;
	private String txReceiverService;
	private String txType;
	private String txDirection;
	private String txOperation;
	private String txReference;
	private Timestamp fromTime;
	private Timestamp toTime;

	public TransactionsDetailsMessage() {
		//
	}

	public TransactionsDetailsMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getTxSenderMobile() {
		return txSenderMobile;
	}

	public void setTxSenderMobile(String txSenderMobile) {
		this.txSenderMobile = txSenderMobile;
	}

	public String getTxReceiverMobile() {
		return txReceiverMobile;
	}

	public void setTxReceiverMobile(String txReceiverMobile) {
		this.txReceiverMobile = txReceiverMobile;
	}

	public String getTxSenderService() {
		return txSenderService;
	}

	public void setTxSenderService(String txSenderService) {
		this.txSenderService = txSenderService;
	}

	public String getTxReceiverService() {
		return txReceiverService;
	}

	public void setTxReceiverService(String txReceiverService) {
		this.txReceiverService = txReceiverService;
	}

	public String getTxType() {
		return txType;
	}

	public void setTxType(String txType) {
		this.txType = txType;
	}

	public String getTxDirection() {
		return txDirection;
	}

	public void setTxDirection(String txDirection) {
		this.txDirection = txDirection;
	}

	public String getTxOperation() {
		return txOperation;
	}

	public void setTxOperation(String txOperation) {
		this.txOperation = txOperation;
	}

	public String getTxReference() {
		return txReference;
	}

	public void setTxReference(String txReference) {
		this.txReference = txReference;
	}

	public Timestamp getFromTime() {
		return fromTime;
	}

	public void setFromTime(Timestamp fromTime) {
		this.fromTime = fromTime;
	}

	public Timestamp getToTime() {
		return toTime;
	}

	public void setToTime(Timestamp toTime) {
		this.toTime = toTime;
	}

	public static TransactionsDetailsMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		TransactionsDetailsMessage message = new TransactionsDetailsMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(SENDER_KEY);

		if (message.getSenderType() == null
				|| !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(SENDER_TYPE_KEY);

		message.setTxSenderMobile(message.getValue(TX_SENDER_MOBILE_KEY));
		message.setTxReceiverMobile(message.getValue(TX_RECEIVER_MOBILE_KEY));
		message.setTxSenderService(message.getValue(TX_SENDER_SERVICE_KEY));
		message.setTxReceiverService(message.getValue(TX_RECEIVER_SERVICE_KEY));
		message.setTxType(message.getValue(TX_TYPE_KEY));
		message.setTxDirection(message.getValue(TX_DIRECTION_KEY));
		message.setTxOperation(message.getValue(TX_OPERATION_KEY));
		message.setTxReference(message.getValue(TX_REFERENCE_KEY));
		try {
			message.setFromTime(new Timestamp(SystemHelper.parseDate(message.getValue(FROM_TIME_KEY), DATE_FORMAT).getTime()));
		} catch (Exception ex) {
			logger.debug("Invalid fromDate Format", ex);
			throw new MessageParsingException(FROM_TIME_KEY);
		}
		try {
			message.setToTime(new Timestamp(SystemHelper.parseDate(message.getValue(TO_TIME_KEY), DATE_FORMAT).getTime()));
		} catch (Exception ex) {
			logger.debug("Invalid toDate Format", ex);
			throw new MessageParsingException(TO_TIME_KEY);
		}
		return message;
	}
}