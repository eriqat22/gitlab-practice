package com.progressoft.mpay.plugins.transactionsdetails;

public class Constants {
	public static final String BalanceKey = "bal";
	public static final String TransactionsKey = "txs";
}
