package com.progressoft.mpay.plugins.agentcashout.integrations;

import com.progressoft.jfw.shared.exception.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.agentcashout.AgentCashOutMessage;
import com.progressoft.mpay.plugins.agentcashout.NotificationProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Objects;

public class AgentCashOutResponseProcessor {
	private static final Logger logger = LoggerFactory.getLogger(AgentCashOutResponseProcessor.class);

	private AgentCashOutResponseProcessor() {

	}

	public static IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		try {
			logger.debug("Inside ProcessIntegration ...");
			preProcessIntegration(context);
			if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return null;
			if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return accept(context);
			else
				return reject(context);
		} catch (Exception ex) {
			logger.error("Error when ProcessIntegration in AgentCashOutResponseProcessor", ex);
			if (context.getTransaction() != null) {
				TransactionHelper.reverseTransaction(context);
				try {
					context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
							context.getTransactionConfig().getMessageType().getId(),
							BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1,
							SystemHelper.getCurrentDateWithoutTime());
				} catch (SQLException e) {
					logger.error("Failed to reverse limits", e);
				}
			}
			return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED,
					ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), null, false);
		}
	}

	private static void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside PreProcessIntegration ...");
		context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		AgentCashOutMessage request;
		try {
			request = AgentCashOutMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
		} catch (MessageParsingException ex) {
			logger.error("Error while parsing message", ex);
			throw new WorkflowException(ex);
		}
		context.setAmount(Objects.requireNonNull(request).getAmount());
		context.setSender(sender);
		context.setReceiver(receiver);
		sender.setMobile(context.getTransaction().getSenderMobile());
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setMobileAccount(context.getTransaction().getSenderMobileAccount());
		sender.setAccount(context.getSender().getMobileAccount().getRefAccount());
		sender.setBanked(sender.getMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
		sender.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
				sender.getAccount().getAccNumber()));
		sender.setInfo(sender.getMobile().getMobileNumber());
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

		receiver.setService(context.getTransaction().getReceiverService());
		receiver.setServiceAccount(context.getTransaction().getReceiverServiceAccount());
		receiver.setAccount(context.getReceiver().getServiceAccount().getRefAccount());
		receiver.setCorporate(receiver.getService().getRefCorporate());
		receiver.setBanked(
				receiver.getServiceAccount().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
		receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
				receiver.getAccount().getAccNumber()));
		receiver.setProfile(receiver.getServiceAccount().getRefProfile());
		receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

		context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(
				context.getLookupsLoader().getCustomerClientType().getId(), sender.isBanked(),
				receiver.getCorporate().getClientType().getId(), receiver.isBanked(),
				context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT,
				context.getOperation().getId()));

		handlePushNotificationToken(context,request);
	}

	private static void handlePushNotificationToken(ProcessingContext context, AgentCashOutMessage message) {
		final MPAY_CustomerDevice customerDevice = context.getDataProvider().getCustomerDevice(message.getDeviceId());
		final String receiverDeviceToken = context.getReceiver().getService().getRefCorporateServiceCorporateDevices()
				.stream()
				.filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
				.findFirst()
				.map(MPAY_CorporateDevice::getExtraData).orElse(null);
		context.setSenderNotificationToken(customerDevice != null ? customerDevice.getExtraData() : null);
		context.setReceiverNotificationToken(receiverDeviceToken);
	}

	private static IntegrationProcessingResult accept(IntegrationProcessingContext context) {
		logger.debug("Inside Accept ...");
		IntegrationProcessingResult result;
		result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null,
				null, false);
		result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		return result;
	}

	private static IntegrationProcessingResult reject(IntegrationProcessingContext context) throws SQLException {
		logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);

		context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
				context.getTransactionConfig().getMessageType().getId(),
				BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1,
				SystemHelper.getCurrentDateWithoutTime());
		String reasonDescription = null;
		if (context.getMpClearIsoMessage().getField(47) != null)
			reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED,
				ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
				context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
		result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
		return result;
	}
}