package com.progressoft.mpay.plugins.agentcashout;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.entities.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.agentcashout.integrations.AgentCashOutResponseProcessor;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class AgentCashOutProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(AgentCashOutProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = AgentCashOutValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
                    e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode,
                                                  String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        if (context.getTransaction() != null) {
            TransactionHelper.reverseTransaction(context);
        }
        return MessageProcessingResult.create(context, status, reason, reasonDescription);
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context)
            throws SQLException {
        MessageProcessingResult result = new MessageProcessingResult();
        MPAY_ProcessingStatus status;
        MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
        String reasonDescription = null;

        context.getMessage().setReason(reason);
        MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT,
                BigDecimal.ZERO, context.getRequest().getNotes());
        result.setMessage(context.getMessage());
        context.setTransaction(transaction);
        result.setTransaction(transaction);
        context.getDataProvider().persistProcessingResult(result);

        JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(),
                context.getLookupsLoader(), transaction);
        if (postingResult.isSuccess())
            status = handleMPClear(context, result);
        else {
            if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
                context.getMessage().setReasonDesc(postingResult.getReason());
            } else
                reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
        }

        context.getMessage().setProcessingStatus(status);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
        transaction.setReason(context.getMessage().getReason());
        transaction.setReasonDesc(context.getMessage().getReasonDesc());
        result.setStatus(status);
        result.setStatusDescription(context.getMessage().getReasonDesc());
        if (status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
            context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
                    context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
                    SystemHelper.getCurrentDateWithoutTime());
        } else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
            result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
        OTPHanlder.removeOTP(context);
        handleCommissions(context, status);
        return result;
    }

    private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
        if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            return;
        CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
                CommissionDirections.SENDER);
        if (context.getDirection() == TransactionDirection.ONUS)
            CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
                    CommissionDirections.RECEIVER);
    }

    private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, MessageProcessingResult result) {
        MPAY_ProcessingStatus status;
        Integer mpClearIntegType;
        mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
        status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType);
        MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage,
                Integer.toHexString(mpClearIntegType));
        messageLog.setRefMessage(context.getMessage());
        result.setMpClearMessage(messageLog);
        return status;
    }

    private void preProcessMessage(MessageProcessingContext context)
            throws MessageParsingException, SQLException, WorkflowException {
        logger.debug("inside PreProcessMessage -----------------------------");
        AgentCashOutMessage message = AgentCashOutMessage.parseMessage(context.getRequest());
        context.setRequest(message);
        context.setAmount(message.getAmount());
        context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        loadSender(context);
        if (context.getSender().getMobile() == null || context.getSender().getMobileAccount() == null)
            return;
        loadReceiver(context);
        if (context.getReceiver().getService() == null || context.getReceiver().getServiceAccount() == null)
            return;
        OTPHanlder.loadOTP(context, message.getPin());
        loadTransactionConfig(context);
        ChargesCalculator.calculate(context);
        TaxCalculator.claculate(context);
        handlePushNotificationToken(context, message);
    }

    private void handlePushNotificationToken( ProcessingContext context, AgentCashOutMessage message) {
        final MPAY_CustomerDevice customerDevice = context.getDataProvider().getCustomerDevice(message.getDeviceId());
        final String receiverDeviceToken = context.getReceiver().getService().getRefCorporateServiceCorporateDevices()
                .stream()
                .filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CorporateDevice::getExtraData).orElse(null);
        context.setSenderNotificationToken(customerDevice != null ? customerDevice.getExtraData() : null);
        context.setReceiverNotificationToken(receiverDeviceToken);
    }

    private void loadTransactionConfig(MessageProcessingContext context) {
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        context.setDirection(TransactionDirection.ONUS);
        context.setTransactionConfig(
                context.getLookupsLoader().getTransactionConfig(personClientType, context.getSender().isBanked(),
                        context.getReceiver().getCorporate().getClientType().getId(), context.getReceiver().isBanked(),
                        context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
        if (context.getTransactionConfig() != null)
            context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
    }

    private void loadReceiver(MessageProcessingContext context) {
        AgentCashOutMessage message = (AgentCashOutMessage) context.getRequest();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setReceiver(receiver);
        receiver.setInfo(message.getReceiverInfo());
        receiver.setService(
                context.getDataProvider().getCorporateService(message.getReceiverInfo(), message.getReceiverType()));
        if (receiver.getService() == null)
            return;
        receiver.setNotes(message.getNotes());
        receiver.setCorporate(receiver.getService().getRefCorporate());
        receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), receiver.getService(),
                message.getReceiverAccount()));
        if (receiver.getServiceAccount() == null)
            return;
        receiver.setAccount(receiver.getServiceAccount().getRefAccount());
        receiver.setBank(receiver.getServiceAccount().getBank());
        receiver.setProfile(receiver.getService().getRefProfile());
        receiver.setBanked(receiver.getAccount().getIsBanked());
        receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
    }

    private void loadSender(MessageProcessingContext context) throws SQLException {
        AgentCashOutMessage message = (AgentCashOutMessage) context.getRequest();
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        sender.setInfo(message.getSender());
        sender.setMobile(context.getDataProvider().getCustomerMobile(message.getSender()));
        if (sender.getMobile() == null)
            return;
        sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(),
                message.getSenderAccount()));
        if (sender.getMobileAccount() == null)
            return;
        sender.setAccount(sender.getMobileAccount().getRefAccount());
        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setBank(sender.getMobileAccount().getBank());
        sender.setProfile(sender.getMobileAccount().getRefProfile());
        sender.setBanked(sender.getAccount().getIsBanked());
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
                context.getOperation().getMessageType().getId()));
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        try {
            logger.debug("Inside ProcessIntegration ...");
            IntegrationProcessingResult result = AgentCashOutResponseProcessor.processIntegration(context);
            handleCommissions(context, result.getProcessingStatus());
            return result;
        } catch (Exception e) {
            logger.error("Error when ProcessIntegration in AgentCashOutProcessor", e);
            return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED,
                    ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null, false);
        }
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type) {
        logger.debug("Inside CreateMPClearRequest ...");
        try {
            String messageID = context.getDataProvider().getNextMPClearMessageId();
            BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
            IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
                    .newMessage(type);
            message.setBinary(false);

            String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(),
                    context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
            String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge,
                    AmountType.CHARGE);

            message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
            MPClearHelper.setMessageEncoded(message);
            message.setField(7, new IsoValue<String>(IsoType.DATE10,
                    MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
            message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
            message.setField(49,
                    new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

            boolean isByAlias = false;
            if (context.getSender().getMobile().getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS)
                    && context.getSender().getMobile().getAlias() != null)
                isByAlias = true;
            String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getMobile(),
                    context.getSender().getMobileAccount(), isByAlias);
            MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

            String account2 = MPClearHelper.getAccount(context.getSystemParameters(),
                    context.getReceiver().getService(), context.getReceiver().getServiceAccount(), false);
            MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

            String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
            message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
            message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

            return message;
        } catch (Exception e) {
            logger.error("Error when CreateMPClearRequest in AgentCashOutProcessor", e);
            return null;
        }
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }
}
