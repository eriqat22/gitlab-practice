package com.progressoft.mpay.plugins.agentcashout;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.AmountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PSPValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;
import com.progressoft.mpay.plugins.validators.WalletCapValidator;

public class AgentCashOutValidator {
	private static final Logger logger = LoggerFactory.getLogger(AgentCashOutValidator.class);

	private AgentCashOutValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = validateCustomer(context);
		if (!result.isValid())
			return result;

		AgentCashOutMessage request = (AgentCashOutMessage) context.getRequest();

		result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(), request.getDeviceId(), context.getSender().getMobile().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		result = validateReceiver(context);
		if (!result.isValid())
			return result;

		result = validateFinancialData(context, request);
		if (!result.isValid())
			return result;

		return PSPValidator.validate(context, false);
	}

	private static ValidationResult validateCustomer(MessageProcessingContext context) {
		ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = PinCodeValidator.validate(context, context.getSender().getMobile(), context.getRequest().getPin());
		if (!result.isValid())
			return result;
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateFinancialData(MessageProcessingContext context, AgentCashOutMessage request) {
		ValidationResult result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context);
		if (!result.isValid())
			return result;

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateReceiver(MessageProcessingContext context) {
		logger.debug("Inside ValidateReceiver ...");

		ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getServiceAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return WalletCapValidator.validate(context);
	}
}
