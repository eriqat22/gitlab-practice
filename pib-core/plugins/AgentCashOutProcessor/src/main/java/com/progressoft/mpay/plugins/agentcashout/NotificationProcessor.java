package com.progressoft.mpay.plugins.agentcashout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    private NotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateAcceptanceNotificationMessages ...");
        try {
            String receiver;
            String sender;
            String reference;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            MPAY_CustomerMobile senderMobile = context.getSender().getMobile();
            MPAY_CorpoarteService receiverService = context.getReceiver().getService();

            AgentCashOutNotificationContext notificationContext = new AgentCashOutNotificationContext();
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setReference(reference);

            if (senderMobile != null) {
                sender = senderMobile.getMobileNumber();
                if (senderMobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) && senderMobile.getAlias() != null)
                    sender = senderMobile.getAlias();
                context.getDataProvider().refreshEntity(context.getSender().getAccount());
                notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getAccount().getBalance()));
                notificationContext.setSenderBanked(context.getSender().getMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
            } else {
                sender = context.getSender().getInfo();
            }
            if (receiverService != null) {
                receiver = receiverService.getName();
                context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getAccount().getBalance()));
                notificationContext.setReceiverBanked(context.getReceiver().getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
            } else {
                receiver = context.getReceiver().getInfo();
            }
            if (context.getReceiver().getNotes() != null) {
                notificationContext.setNotes(context.getReceiver().getNotes());
                notificationContext.setHasNotes(true);
            }
            notificationContext.setReceiver(receiver);
            notificationContext.setSender(sender);
            return generateNotification(context, senderMobile, receiverService, notificationContext);
        } catch (Exception e) {
            logger.error("Error when CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    private static List<MPAY_Notification> generateNotification(ProcessingContext context, MPAY_CustomerMobile senderMobile,
                                                                MPAY_CorpoarteService receiverService, AgentCashOutNotificationContext notificationContext) {
        List<MPAY_Notification> notifications = new ArrayList<>();
        if (senderMobile != null) {
            final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
            notificationContext.setExtraData1(senderMobile.getMobileNumber());
            addNotification(context.getMessage(), notifications, senderMobile.getEmail(), prefLang,
                    senderMobile.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
            addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
                    senderMobile.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
            addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
                    senderMobile.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER,
                    NotificationChannelsCode.PUSH_NOTIFICATION);

        }
        if (receiverService != null) {
            final MPAY_Language prefLang = receiverService.getRefCorporate().getPrefLang();
            notificationContext.setExtraData1(receiverService.getName());
            addNotification(context.getMessage(), notifications, receiverService.getEmail(), prefLang,
                    receiverService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.EMAIL);
            addNotification(context.getMessage(), notifications, receiverService.getMobileNumber(), prefLang,
                    receiverService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.SMS);
            addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                    receiverService.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_RECEIVER,
                    NotificationChannelsCode.PUSH_NOTIFICATION);

        }
        return notifications;
    }


//	private static void handlePushNotifications(AgentCashOutNotificationContext notificationContext, ProcessingContext context, MPAY_EndPointOperation operation, MPAY_CorpoarteService receiverService, long receiverLanguageId, List<MPAY_Notification> notifications) {
//		List<String> tokens = (List<String>) context.getExtraData().get(Constants.NOTIFICATION_TOKEN_KEY);
//		if (tokens == null)
//			return;
//		for (String token : tokens) {
//			notificationContext.setExtraData1(token);
//			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId, receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
//		}
//	}

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            AgentCashOutMessage request = AgentCashOutMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();
            MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request != null ? request.getSender() : null);
            if (mobile != null) {
                language = mobile.getRefCustomer().getPrefLang();
            }
            AgentCashOutNotificationContext notificationContext = new AgentCashOutNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            if (context.getTransaction() == null)
                notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
            else
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request != null ? request.getAmount() : null));
            notificationContext.setReceiver(request.getReceiverInfo());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(Objects.requireNonNull(MPayHelper.getReasonNLS(context.getMessage().getReason(), language)).getDescription());
            notificationContext.setExtraData1(mobile != null ? mobile.getMobileNumber() : null);

            if (mobile != null) {
                addNotification(context.getMessage(), notifications, mobile.getEmail(), language,
                        mobile.getEnableEmail(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, mobile.getMobileNumber(), language,
                        mobile.getEnableSMS(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), language,
                        mobile.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_SENDER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }

            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateReversalNotificationMessages ...");
        try {
            String reference;
            List<MPAY_Notification> notifications = new ArrayList<>();

            reference = context.getTransaction().getReference();
            MPAY_CustomerMobile receiverMobile = context.getTransaction().getReceiverMobile();
            MPAY_CustomerMobile senderMobile = context.getTransaction().getSenderMobile();
            String currency = context.getTransaction().getCurrency().getStringISOCode();
            if (receiverMobile != null) {
                AgentCashOutNotificationContext receiverNotificationContext = new AgentCashOutNotificationContext();
                receiverNotificationContext.setCurrency(currency);
                receiverNotificationContext.setReference(reference);
                MPAY_Account receiverAccount = context.getTransaction().getReceiverMobileAccount().getRefAccount();
                context.getDataProvider().refreshEntity(receiverAccount);
                receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
                receiverNotificationContext.setReceiverBanked(context.getTransaction().getReceiverMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                receiverNotificationContext.setExtraData1(receiverMobile.getMobileNumber());

                final MPAY_Language prefLang = receiverMobile.getRefCustomer().getPrefLang();
                addNotification(context.getMessage(), notifications, receiverMobile.getEmail(), prefLang,
                        receiverMobile.getEnableEmail(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, receiverMobile.getMobileNumber(), prefLang,
                        receiverMobile.getEnableSMS(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                        receiverMobile.getEnablePushNotification(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL,
                        NotificationChannelsCode.PUSH_NOTIFICATION);

            }
            if (senderMobile != null) {
                AgentCashOutNotificationContext senderNotificationContext = new AgentCashOutNotificationContext();
                senderNotificationContext.setCurrency(currency);
                senderNotificationContext.setReference(reference);
                MPAY_Account senderAccount = context.getTransaction().getSenderMobileAccount().getRefAccount();
                context.getDataProvider().refreshEntity(senderAccount);
                senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
                senderNotificationContext.setReceiverBanked(context.getTransaction().getSenderMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                senderNotificationContext.setExtraData1(senderMobile.getMobileNumber());

                final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
                addNotification(context.getMessage(), notifications, senderMobile.getEmail(), prefLang,
                        senderMobile.getEnableEmail(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
                        senderMobile.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
                        senderMobile.getEnablePushNotification(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateReversalNotificationMessages", e);
            return new ArrayList<>();
        }
    }
}