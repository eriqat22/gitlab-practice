package com.progressoft.mpay.plugins.agentcashout;

public class Constants {
	public static final String NOTIFICATION_TOKEN_KEY = "NotificationToken";

	private Constants() {

	}
}
