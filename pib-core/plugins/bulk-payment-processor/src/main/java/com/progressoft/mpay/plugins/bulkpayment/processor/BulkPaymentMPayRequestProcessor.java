package com.progressoft.mpay.plugins.bulkpayment.processor;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.mpay.*;
import com.progressoft.mpay.common.RequestProcessor;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.notifications.INotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BulkPaymentMPayRequestProcessor extends RequestProcessor {

    private static final String MERGE_PROCESSING_RESULT = "MergeProcessingResult";
    private static final Logger LOGGER = LoggerFactory.getLogger(BulkPaymentMPayRequestProcessor.class);

    public BulkPaymentMPayRequestProcessor(IDataProvider dataProvider, ISystemParameters systemParameters,
                                           ILookupsLoader lookupsLoader, ICryptographer cryptographer, INotificationHelper notificationHelper,
                                           Communicator communicator) {
        super(dataProvider, systemParameters, lookupsLoader, cryptographer, notificationHelper, communicator);
    }

    public MPayResponseEnvelope processMessage(String body, String token, IServiceUserManager serviceUserManager,
                                               MPAY_BulkPayment mpayBulkPayment) {
        boolean enabledByMe = false;
        MPayRequest request = null;
        MessageProcessingContext context = null;
        MPayResponseEnvelope responseEnvelope;
        try {
            LOGGER.info("Request Received : ", body);

            request = MPayRequest.fromJson(body);
            if (request == null)
                return generateResponseEnvelop(body, token, ReasonCodes.FAILED_TO_PARSE_MESSAGE, null);

            enabledByMe = serviceUserManager.enableServiceUser(request.getTenant());

            MPAY_EndPointOperation operation = lookupsLoader.getEndPointOperation(request.getOperation());

            if (!operation.getOperation().equals("b2p")) {
                if (operation.getIsSystem())
                    return generateResponseEnvelop(body, token, ReasonCodes.UNSUPPORTED_REQUEST, request);
            }

            if (!operation.getIsActive())
                return generateResponseEnvelop(body, token, ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE, request);

            MessageProcessor processor = MessageProcessorHelper.getProcessor(operation);
            if (processor == null)
                return generateResponseEnvelop(body, token, ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE, request);

            context = prepareMessageProcessingContext(body, token, request, operation);
            MPAY_MPayMessage message = createMessage(serviceUserManager, context, mpayBulkPayment);

            message.setBulkPayment(null);
            responseEnvelope = tryPersistMessage(body, token, message, request);
            if (responseEnvelope != null)
                return responseEnvelope;
            context.setMessage(message);
            message.setBulkPayment(mpayBulkPayment);
            return process(body, token, request, context, processor);
        } catch (Exception ex) {
            LOGGER.error(MERGE_PROCESSING_RESULT, ex);
            if (context != null)
                return rejectMessage(context, request, body, token, ex.getMessage());
            else
                return generateResponseEnvelop(body, token, ReasonCodes.INTERNAL_SYSTEM_ERROR, request);
        } finally {
            if (enabledByMe) {
                serviceUserManager.disableServiceUser();
            }
        }
    }

    private MPAY_MPayMessage createMessage(IServiceUserManager serviceUserManager, MessageProcessingContext context,
                                           MPAY_BulkPayment mpayBulkPayment) throws WorkflowException {
        return MessageProcessorHelper.createMessage(serviceUserManager, context, ProcessingStatusCodes.PENDING, null,
                null, mpayBulkPayment);
    }

    private MessageProcessingContext prepareMessageProcessingContext(String body, String token, MPayRequest request,
                                                                     MPAY_EndPointOperation operation) {
        MessageProcessingContext context;
        context = new MessageProcessingContext(dataProvider, lookupsLoader, systemParameters, cryptographer,
                notificationHelper);
        context.setOperation(operation);
        context.setRequest(request);
        context.setOriginalRequest(body);
        context.setToken(token);
        context.setLanguage(getLanguage(request));
        return context;
    }

    private MPayResponseEnvelope generateResponseEnvelop(String body, String token, String reasonCode,
                                                         MPayRequest request) {
        MPayResponseEnvelope responseEnvelope;
        responseEnvelope = new MPayResponseEnvelope();
        responseEnvelope.setResponse(
                rejectMessage(null, body, token, lookupsLoader.getReason(reasonCode), getLanguage(request)));
        responseEnvelope
                .setToken(cryptographer.createJsonToken(systemParameters, responseEnvelope.getResponse().toValues()));
        return responseEnvelope;
    }

    private MPayResponseEnvelope process(String body, String token, MPayRequest request,
                                         MessageProcessingContext context, MessageProcessor processor) {
        MPayResponseEnvelope responseEnvelope;
        MessageProcessingResult result;

        try {
            result = processor.processMessage(context);
            if (result.getResponse() == null) {
                responseEnvelope = new MPayResponseEnvelope();
                responseEnvelope.setResponse(generateResponse(result.getMessage().getReference(),
                        result.getMessage().getProcessingStatus().getCode(), result.getMessage().getReason(),
                        context.getLanguage()));
                responseEnvelope.setToken(
                        cryptographer.createJsonToken(systemParameters, responseEnvelope.getResponse().toValues()));
                result.setResponse(responseEnvelope.toString());
            } else {
                responseEnvelope = new MPayResponseEnvelope();
                responseEnvelope.setResponse(MPayResponse.fromJson(result.getResponse()));
                responseEnvelope.setToken(
                        cryptographer.createJsonToken(systemParameters, responseEnvelope.getResponse().toValues()));
                result.setResponse(responseEnvelope.toString());
            }
            if (result.getMessage().getResponseContent() == null)
                result.getMessage().setResponseContent(result.getResponse());
            dataProvider.persistProcessingResult(result);
            if (result.getMpClearMessage() != null && !result.isHandleMPClearOffline())
                MPClearHelper.sendToActiveMQ(result.getMpClearMessage(), communicator);
            notificationHelper.sendNotifications(result);
            return MPayResponseEnvelope.fromJson(result.getResponse());
        } catch (Exception ex) {
            LOGGER.error(MERGE_PROCESSING_RESULT, ex);
            return rejectMessage(context, request, body, token, ex.toString());
        }
    }

    private MPayResponseEnvelope tryPersistMessage(String body, String token, MPAY_MPayMessage message,
                                                   MPayRequest request) {
        try {
            dataProvider.persistMPayMessage(message);
            return null;
        } catch (Exception e) {
            LOGGER.debug(MERGE_PROCESSING_RESULT, e);
            return generateResponseEnvelop(body, token, ReasonCodes.MESSAGE_IS_DUPLICATED, request);
        }
    }

    private MPayResponseEnvelope rejectMessage(MessageProcessingContext context, MPayRequest request, String body,
                                               String token, String reasonDescription) {
        MPAY_Reason reason = lookupsLoader.getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
        String reference = null;
        if (context.getMessage() != null) {
            reference = context.getMessage().getReference();
            context.getMessage().setReason(reason);
            context.getMessage().setProcessingStatus(lookupsLoader.getProcessingStatus(ProcessingStatusCodes.FAILED));
            context.getMessage().setReasonDesc(reasonDescription);
            if (context.getTransaction() != null) {
                context.getTransaction().setReason(reason);
                context.getTransaction().setProcessingStatus(context.getMessage().getProcessingStatus());
                context.getTransaction().setReasonDesc(context.getMessage().getReasonDesc());
            }
            dataProvider.mergeProcessingContext(context);
        }
        MPayResponse response = rejectMessage(reference, body, token, reason, getLanguage(request));
        MPayResponseEnvelope responseEnvelope = new MPayResponseEnvelope();
        responseEnvelope.setResponse(response);
        responseEnvelope
                .setToken(cryptographer.createJsonToken(systemParameters, responseEnvelope.getResponse().toValues()));
        return responseEnvelope;
    }

    private MPayResponse rejectMessage(String reference, String requset, String token, MPAY_Reason reason,
                                       MPAY_Language language) {
        LOGGER.debug("Invalid Message Received.");
        LOGGER.debug("Receiving Date: " + SystemHelper.getSystemTimestamp().toString());
        LOGGER.debug("Request: " + requset);
        LOGGER.debug("token: " + token);
        MPAY_Language actualLanguage = language;
        MPayResponse response = new MPayResponse();
        long ref = 0;
        if (reference != null)
            ref = Long.parseLong(reference);
        response.setRef(ref);
        response.setErrorCd(reason.getCode());
        response.setStatusCode(ProcessingStatusCodes.REJECTED);
        if (actualLanguage == null)
            actualLanguage = systemParameters.getSystemLanguage();
        String description;
        MPAY_Reasons_NLS nls = lookupsLoader.getReasonNLS(reason.getId(), actualLanguage.getCode());
        if (nls == null)
            description = lookupsLoader.getReasonNLS(reason.getId(), systemParameters.getSystemLanguage().getCode())
                    .getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        return response;
    }

    private MPayResponse generateResponse(String reference, String processingCode, MPAY_Reason reason,
                                          MPAY_Language language) {
        MPayResponse response = new MPayResponse();
        response.setErrorCd(reason.getCode());
        MPAY_Language actualLanguage = language;
        if (actualLanguage == null)
            actualLanguage = systemParameters.getSystemLanguage();
        MPAY_Reasons_NLS nls = lookupsLoader.getReasonNLS(reason.getId(), actualLanguage.getCode());
        String description;
        if (nls == null)
            description = lookupsLoader.getReasonNLS(reason.getId(), systemParameters.getSystemLanguage().getCode())
                    .getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setRef(Long.parseLong(reference));
        response.setStatusCode(processingCode);
        return response;
    }

    private MPAY_Language getLanguage(MPayRequest request) {
        LOGGER.debug("Inside getLanguage ....");
        if (request == null)
            return systemParameters.getSystemLanguage();
        return lookupsLoader.getLanguageByCode(LanguageMapper.getInstance().getLanguageLocaleCode(request.getLang() + ""));
    }
}