package com.progressoft.mpay.plugins.bulkpayment.payment;

import com.progressoft.mpay.entities.MPAY_BulkPayment;
import com.progressoft.mpay.plugins.bulkpayment.model.BulkPaymentFile;
import com.progressoft.mpay.plugins.bulkpayment.model.view.BulkPaymentFileView;

public class BulkPayment {

	public BulkPaymentFileView processFile(MPAY_BulkPayment mpayBulkPayment, String fileContent) {
		BulkPaymentFile.Builder builder;
		BulkPaymentFile bulkPaymentFile;
		validateFileName(mpayBulkPayment.getFileName());
		validateFileContent(fileContent);
		builder = new BulkPaymentFile.Builder().fileName(mpayBulkPayment.getFileName());
		bulkPaymentFile = builder.build();
		bulkPaymentFile.fetchAndProcessFileContent(fileContent, mpayBulkPayment);
		return bulkPaymentFile;
	}

	private void validateFileContent(String fileContent) {
		if (fileContent == null)
			throw new IvalidFileException("File Content Should be not null");
		if (fileContent.trim().isEmpty())
			throw new IvalidFileException("File Content Should be not empty");
	}

	private void validateFileName(String fileName) {
		if (fileName == null)
			throw new IvalidFileException("File Name Should be not null");
		if (fileName.trim().isEmpty())
			throw new IvalidFileException("File Name Should be not empty");
	}

	public class IvalidFileException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public IvalidFileException(String message) {
			super(message);
		}
	}

}