package com.progressoft.mpay.plugins.bulkpayment.parser;

import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayRequest;

import static com.progressoft.mpay.plugins.bulkpayment.parser.MpayRequestParser.MpayRequestParserColumns.*;

public class MpayRequestParser {

	 enum MpayRequestParserColumns {

		OPERATION_COLUMN(0), TENANT_COLUMN(1), SENDER_COLUMN(2), SENDER_TYPE_COLUMN(3), DEVICEID_COLUMN(4), LANG_COLUMN(5), MSGID_COLUMN(6), PIN_COLUMN(7), REQUESTEDID_COLUMN(8), SHOPID_COLUMN(9), CHANNELID_COLUMN(10), WALLETID_COLUMN(11), EXTRA_DATA_COLUMN(12);

		private int columnNumber;

		private MpayRequestParserColumns(int columnNumber) {
			this.columnNumber = columnNumber;
		}

		public int getColumnNumber() {
			return columnNumber;
		}
	}

	private enum MpayRequestParserConstants {
		SEMMICOLON(";"), COMMA(","), COLON(":");
		private String value;

		private MpayRequestParserConstants(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public MPayRequest genrateMpayRequest(String record) {
		validateEmptyRecord(record);
		String[] recordsData = record.split(MpayRequestParserConstants.COMMA.getValue());
		validateData(recordsData);
		MPayRequest request = setMpayRequestValue(recordsData);
		readExtraData(request, recordsData);
		return request;
	}

	private MPayRequest setMpayRequestValue(String[] recordsData) {
		MPayRequest request = new MPayRequest();
		request.setOperation(recordsData[OPERATION_COLUMN.getColumnNumber()]);
		request.setTenant(recordsData[TENANT_COLUMN.getColumnNumber()]);
		request.setSender(recordsData[SENDER_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[SENDER_COLUMN.getColumnNumber()]);
		request.setSenderType(recordsData[SENDER_TYPE_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[SENDER_TYPE_COLUMN.getColumnNumber()]);
		request.setDeviceId(recordsData[DEVICEID_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[DEVICEID_COLUMN.getColumnNumber()]);
		request.setLang(getLang(recordsData[LANG_COLUMN.getColumnNumber()]));
		request.setMsgId(recordsData[MSGID_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[MSGID_COLUMN.getColumnNumber()]);
		request.setPin(recordsData[PIN_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[PIN_COLUMN.getColumnNumber()]);
		request.setRequestedId(recordsData[REQUESTEDID_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[REQUESTEDID_COLUMN.getColumnNumber()]);
		request.setShopId(recordsData[SHOPID_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[SHOPID_COLUMN.getColumnNumber()]);
		request.setChannelId(recordsData[CHANNELID_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[CHANNELID_COLUMN.getColumnNumber()]);
		request.setWalletId(recordsData[WALLETID_COLUMN.getColumnNumber()].trim().isEmpty() ? null : recordsData[WALLETID_COLUMN.getColumnNumber()]);
		return request;
	}

	private long getLang(String recordsDatum) {
		return recordsDatum.trim().isEmpty() ? 1 : Long.parseLong(recordsDatum);
	}

	private void readExtraData(MPayRequest request, String[] recordsData) {
		for (int i = EXTRA_DATA_COLUMN.getColumnNumber(); i < recordsData.length; i++) {
			try {
				String[] recordSplitSemmiColon = recordsData[i].split(MpayRequestParserConstants.SEMMICOLON.getValue());
				String key = recordSplitSemmiColon[0].split(MpayRequestParserConstants.COLON.getValue())[1];
				String value = recordSplitSemmiColon[1].split(MpayRequestParserConstants.COLON.getValue())[1];
				request.getExtraData().add(new ExtraData(key, value));
			} catch (Exception e) {
				throw new MessageRequestParsingException(e);
			}
		}
	}

	private void validateData(String[] recordData) {
		validateValue(recordData[OPERATION_COLUMN.getColumnNumber()], "OPERATION COLUMN");
		validateValidOperationName(recordData);
		validateValue(recordData[TENANT_COLUMN.getColumnNumber()], "TENANT COLUMN");
		validateValue(recordData[SENDER_TYPE_COLUMN.getColumnNumber()], "SENDER TYPE");
		validateValue(recordData[SENDER_COLUMN.getColumnNumber()], "SENDER");
	}

	private void validateValidOperationName(String[] recordData) {
		if (!SystemParameters.getInstance().getBulkPaymentOperations().contains(recordData[OPERATION_COLUMN.getColumnNumber()]))
			throw new BulkPaymentUnSupportedOperationException();
	}

	private void validateValue(String value, String fieldName) {
		if (value == null || value.trim().isEmpty()) {
			throw new InvalidRecordException("Invalid value :- " + fieldName);
		}
	}

	private void validateEmptyRecord(String record) {
		if (record == null || record.trim().isEmpty())
			throw new EmptyRecordException();
	}

	public class EmptyRecordException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public EmptyRecordException() {
			super();
		}
	}

	public class InvalidRecordException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public InvalidRecordException(String message) {
			super(message);
		}
	}

	public class MessageRequestParsingException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public MessageRequestParsingException(Throwable e) {
			super(e);
		}
	}

	public class BulkPaymentUnSupportedOperationException extends RuntimeException {
		private static final long serialVersionUID = 1L;

	}

}