package com.progressoft.mpay.plugins.bulkpayment.model;

import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_BulkPayment;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MPayResponseEnvelope;
import com.progressoft.mpay.plugins.bulkpayment.model.view.BulkPaymentFileView;
import com.progressoft.mpay.plugins.bulkpayment.parser.MpayRequestParser;
import com.progressoft.mpay.plugins.bulkpayment.processor.BulkPaymentMPayRequestProcessor;

import java.util.HashMap;
import java.util.Map;

public class BulkPaymentFile implements BulkPaymentFileView {
	private static final String COMMA_WITH_SPACE = " , ";
	private static final String NEW_LINE = "\n";
	private static final String COMMA = ",";
	private Map<Integer, String> inValidRequestRecord = new HashMap<>();
	private Map<Integer, String> failedResponseRecord = new HashMap<>();
	private BulkPaymentMPayRequestProcessor mPayRequestProcessor;
	private int recordNumber = 0;
	private String fileName;

	public BulkPaymentFile(Builder builder) {
		this.fileName = builder.fileName;
		mPayRequestProcessor = new BulkPaymentMPayRequestProcessor(MPayContext.getInstance().getDataProvider(),
				MPayContext.getInstance().getSystemParameters(), MPayContext.getInstance().getLookupsLoader(),
				MPayContext.getInstance().getCryptographer(), MPayContext.getInstance().getNotificationHelper(),
				MPayContext.getInstance().getCommunicator());
	}

	public void fetchAndProcessFileContent(String fileContent, MPAY_BulkPayment mapyBulkPayment) {
		MpayRequestParser requestParser = new MpayRequestParser();
		String[] recordsData = fileContent.split(NEW_LINE);
		for (String record : recordsData) {
			try {
				MPayRequest mPayRequest = requestParser.genrateMpayRequest(record);
				MPayResponseEnvelope responseEnvelope = processMessage(mPayRequest, mapyBulkPayment);
				if (isFailed(responseEnvelope))
					failedResponseRecord.put(recordNumber + 1,
							mPayRequest.getRequestedId() + COMMA + responseEnvelope.getResponse().getErrorCd()
									+ COMMA_WITH_SPACE + responseEnvelope.getResponse().getDesc());
			} catch (Exception e) {
				inValidRequestRecord.put(recordNumber + 1, e.getMessage());
			}
			recordNumber++;
		}
	}

	private MPayResponseEnvelope processMessage(MPayRequest mPayRequest, MPAY_BulkPayment mapyBulkPayment) {
		String body = mPayRequest.toString();
		return mPayRequestProcessor.processMessage(body, "token", MPayContext.getInstance().getServiceUser(),
				mapyBulkPayment);
	}

	private boolean isFailed(MPayResponseEnvelope responseEnvelope) {
		return !ReasonCodes.VALID.equals(responseEnvelope.getResponse().getErrorCd());
	}

	@Override
	public Map<Integer, String> getInValidRequestRecord() {
		return inValidRequestRecord;
	}

	@Override
	public Map<Integer, String> getFailedResponseRecords() {
		return failedResponseRecord;
	}

	@Override
	public Integer getNumberOfRecords() {
		return recordNumber;
	}

	@Override
	public String getFileName() {
		return fileName;
	}

	public static class Builder {
		private String fileName;

		public Builder fileName(String fileName) {
			this.fileName = fileName;
			return this;
		}

		public BulkPaymentFile build() {
			return new BulkPaymentFile(this);
		}
	}
}