package com.progressoft.mpay.plugins.bulkpayment.model.view;

import java.util.Map;

public interface BulkPaymentFileView {

	String getFileName();

	Map<Integer, String> getInValidRequestRecord();

	Map<Integer, String> getFailedResponseRecords();

	Integer getNumberOfRecords();
}