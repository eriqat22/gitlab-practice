package com.progressoft.mpay.plugins.bulkpayment.payment.view;

import java.util.Map;

public interface BulkPaymentView {
	String getFileContent();

	String getFileName();

	Map<Integer, String> getInValidRequestRecord();

	Map<Integer, String> getFailedResponseRecord();

	Integer getRecordNumber();
}