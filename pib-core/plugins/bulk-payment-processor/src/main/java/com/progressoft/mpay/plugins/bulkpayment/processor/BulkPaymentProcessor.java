package com.progressoft.mpay.plugins.bulkpayment.processor;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.bulkpayment.process.IBulkPaymentProcessor;
import com.progressoft.mpay.entities.MPAY_BulkPayment;
import com.progressoft.mpay.entities.MPAY_BulkPaymentAtt;
import com.progressoft.mpay.entities.MPAY_BulkRegistration;
import com.progressoft.mpay.plugins.bulkpayment.model.view.BulkPaymentFileView;
import com.progressoft.mpay.plugins.bulkpayment.payment.BulkPayment;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;

public class BulkPaymentProcessor implements IBulkPaymentProcessor {

    private static final String FILE_PAYMENT_CONTAINER_CSV = "fileRegistrationContainer.csv";
    private static final String PARTIALY_SUCCESS_STATUS = "PARTIALY_SUCCESS";
    private static final String FAILED_PAYMENTS_CSV = "failedPayments.csv";
    private static final String INVALID_RECORDS_CSV = "invalidRecords.csv";
    private static final String SUCCESS_STATUS = "SUCCESS";
    private static final String CONTENT_TYPE = "text/csv";
    private static final String FAILED_STATUS = "FAILED";
    private static final String NEWLINE = "\n";
    private static final String COMMA = ",";

    @Override
    @SuppressWarnings("deprecation")
    public void process(MPAY_BulkPayment mpayBulkPayment) {
        try {
            validateBulkPayment(mpayBulkPayment);
            MPAY_BulkPaymentAtt attahcment = getBulkPaymentAtt(mpayBulkPayment);
            validateMpayAttachment(attahcment);
            File tempFile = createTempFile(attahcment);
            String fileContent = IOUtils.toString(tempFile.toURI());
            BulkPayment bulkPayment = new BulkPayment();
            BulkPaymentFileView bulkPaymentFileView = bulkPayment.processFile(mpayBulkPayment,
                    fileContent);
            showInValidRecordsToUI(mpayBulkPayment, bulkPaymentFileView.getInValidRequestRecord());
            showFailedPaymentToUI(mpayBulkPayment, bulkPaymentFileView.getFailedResponseRecords());
            reflectStatusAccordingToBulkPayment(bulkPaymentFileView, mpayBulkPayment);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private MPAY_BulkPaymentAtt getBulkPaymentAtt(MPAY_BulkPayment mpayBulkPayment) {
        return MPayContext.getInstance().getDataProvider()
                .getBulkPaymentFileAttachment(String.valueOf(mpayBulkPayment.getId()));
    }

    private void showFailedPaymentToUI(MPAY_BulkPayment mpayBulkPayment, Map<Integer, String> failedResponseRecord) {
        showAttachmentToUI(mpayBulkPayment, failedResponseRecord, FAILED_PAYMENTS_CSV);
    }

    private void showInValidRecordsToUI(MPAY_BulkPayment mpayBulkPayment, Map<Integer, String> inValidRequestRecord) {
        showAttachmentToUI(mpayBulkPayment, inValidRequestRecord, INVALID_RECORDS_CSV);
    }

    @SuppressWarnings("deprecation")
    private void showAttachmentToUI(MPAY_BulkPayment bulkPayment, Map<Integer, String> invalidRecord, String fileName) {
        if (invalidRecord.size() > 0) {
            File inValidFile = new File(fileName);
            try {
                FileUtils.writeStringToFile(inValidFile, getInvalidContent(invalidRecord));
                createInvalidBulkPaymentAtt(bulkPayment, FileUtils.readFileToByteArray(inValidFile), fileName);
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private void createInvalidBulkPaymentAtt(MPAY_BulkPayment bulkPayment, byte[] readFileToByteArray,
                                             String fileName) {
        MPAY_BulkPaymentAtt paymentAttachment = new MPAY_BulkPaymentAtt();
        paymentAttachment.setAttFile(readFileToByteArray);
        paymentAttachment.setRecordId(String.valueOf(bulkPayment.getId()));
        paymentAttachment.setEntityId(MPAY_BulkRegistration.class.getName());
        paymentAttachment.setName(fileName);
        paymentAttachment.setContentType(CONTENT_TYPE);
        paymentAttachment.setSize(new BigDecimal(readFileToByteArray.length));
        paymentAttachment.setAttachmentToken("No");
        MPayContext.getInstance().getDataProvider().persistPaymentAttachment(paymentAttachment);
    }

    private String getInvalidContent(Map<Integer, String> invalidRecords) {
        StringBuilder builder = new StringBuilder();
        builder.append("line number,request id,error code,error description").append(NEWLINE);
        invalidRecords.forEach((k, v) -> builder.append(k + COMMA + v + NEWLINE));
        return builder.toString();
    }

    private void validateMpayAttachment(MPAY_BulkPaymentAtt attahcment) {
        if (attahcment == null || attahcment.getAttFile() == null || attahcment.getAttFile().length == 0)
            throw new InvalidBulkPaymentAttachementFile("Invalid Attachment");
    }

    private void validateBulkPayment(MPAY_BulkPayment bulkPayment) {
        if (Objects.isNull(bulkPayment))
            throw new InvalidMpayBulkPaymentFile("Invalid bulk payment file");
    }

    private File createTempFile(MPAY_BulkPaymentAtt attahcment) throws IOException {
        byte[] attFile = attahcment.getAttFile();
        File tempFile = new File(FILE_PAYMENT_CONTAINER_CSV);
        FileUtils.writeByteArrayToFile(tempFile, attFile);
        return tempFile;
    }

    private void reflectStatusAccordingToBulkPayment(BulkPaymentFileView bulkPaymentFileView,
                                                     MPAY_BulkPayment mpayBulkPayment) {
        int invalidRecordSize = bulkPaymentFileView.getInValidRequestRecord().size() + bulkPaymentFileView.getFailedResponseRecords().size();
        if (invalidRecordSize == bulkPaymentFileView.getNumberOfRecords())
            mpayBulkPayment.setStatusId(setWorkflowStatus(FAILED_STATUS));
        else if (invalidRecordSize == 0)
            mpayBulkPayment.setStatusId(setWorkflowStatus(SUCCESS_STATUS));
        else
            mpayBulkPayment.setStatusId(setWorkflowStatus(PARTIALY_SUCCESS_STATUS));
    }

    private WorkflowStatus setWorkflowStatus(String workflowStatus) {
        return MPayContext.getInstance().getDataProvider().getWorkflowStatus(workflowStatus);
    }

    public class InvalidMpayBulkPaymentFile extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public InvalidMpayBulkPaymentFile(String message) {
            super(message);
        }
    }

    public class InvalidBulkPaymentAttachementFile extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public InvalidBulkPaymentAttachementFile(String message) {
            super(message);
        }
    }
}