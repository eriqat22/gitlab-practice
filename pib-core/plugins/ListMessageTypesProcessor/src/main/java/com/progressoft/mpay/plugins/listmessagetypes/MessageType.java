package com.progressoft.mpay.plugins.listmessagetypes;

public class MessageType {
	private String code;
	private String description;

	public MessageType()
	{

	}

	public MessageType(String code, String description)
	{
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
