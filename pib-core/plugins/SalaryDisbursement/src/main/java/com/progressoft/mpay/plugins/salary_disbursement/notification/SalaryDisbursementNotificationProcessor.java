package com.progressoft.mpay.plugins.salary_disbursement.notification;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;

import com.progressoft.mpay.plugins.salary_disbursement.message.SalaryDisbursementMessage;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class SalaryDisbursementNotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(SalaryDisbursementNotificationProcessor.class);

    private SalaryDisbursementNotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateAcceptanceNotificationMessages ...");
        try {
            String receiver;
            String sender;
            String reference;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();

            MPAY_CorpoarteService senderService = context.getSender().getService();
            MPAY_CustomerMobile receiverMobile = context.getReceiver().getMobile();

            SalaryDisbursementNotificationContext notificationContext = new SalaryDisbursementNotificationContext();
            notificationContext
                    .setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());

            notificationContext.setReference(reference);

            if (senderService != null) {
                sender = senderService.getName();
                context.getDataProvider().refreshEntity(context.getSender().getAccount());
                notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
                        context.getSender().getAccount().getBalance()));
                notificationContext.setSenderBanked(
                        context.getSender().getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setSenderCharges(
                        SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
            } else {
                sender = context.getSender().getInfo();
            }
            if (receiverMobile != null) {
                receiver = receiverMobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS)
                        && receiverMobile.getAlias() != null ? receiverMobile.getAlias()
                        : receiverMobile.getMobileNumber();
                context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(),
                        context.getReceiver().getAccount().getBalance()));
                notificationContext.setReceiverBanked(
                        context.getReceiver().getMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setReceiverCharges(
                        SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
            } else {
                receiver = context.getReceiver().getInfo();
            }
            if (context.getReceiver().getNotes() != null) {
                notificationContext.setNotes(context.getReceiver().getNotes());
                notificationContext.setHasNotes(true);
            }
            notificationContext.setReceiver(receiver);
            notificationContext.setSender(sender);
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (receiverMobile != null) {
                notificationContext.setExtraData1(receiverMobile.getMobileNumber());
                final MPAY_Language prefLang = receiverMobile.getRefCustomer().getPrefLang();
                addNotification(context.getMessage(), notifications, receiverMobile.getEmail(), prefLang,
                        receiverMobile.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, receiverMobile.getMobileNumber(), prefLang,
                        receiverMobile.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                        receiverMobile.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_RECEIVER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            if (senderService != null) {
                notificationContext.setExtraData1(senderService.getName());
                final MPAY_Language prefLang = senderService.getRefCorporate().getPrefLang();
                addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
                        senderService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
                        senderService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
                        senderService.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            SalaryDisbursementMessage request = SalaryDisbursementMessage
                    .parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();
            MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(Objects.requireNonNull(request).getSender(),
                    request.getSenderType());
            if (service != null) {
                language = service.getRefCorporate().getPrefLang();
            }
            SalaryDisbursementNotificationContext notificationContext = new SalaryDisbursementNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            if (context.getTransaction() == null)
                notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
            else
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext
                    .setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
            notificationContext.setReceiver(request.getReceiverInfo());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(
                    Objects.requireNonNull(MPayHelper.getReasonNLS(context.getMessage().getReason(), language)).getDescription());

            if (service != null) {
                notificationContext.setExtraData1(service.getName());
                addNotification(context.getMessage(), notifications, service.getEmail(), language,
                        service.getEnableEmail(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, service.getMobileNumber(), language,
                        service.getEnableSMS(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), language,
                        service.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_SENDER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }

            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateReversalNotificationMessages ...");
        try {
            String reference;
            List<MPAY_Notification> notifications = new ArrayList<>();

            reference = context.getTransaction().getReference();
            MPAY_CustomerMobile receiverMobile = context.getTransaction().getReceiverMobile();
            MPAY_CustomerMobile senderMobile = context.getTransaction().getSenderMobile();
            String currency = context.getTransaction().getCurrency().getStringISOCode();
            if (receiverMobile != null) {
                SalaryDisbursementNotificationContext receiverNotificationContext = new SalaryDisbursementNotificationContext();
                receiverNotificationContext.setCurrency(currency);
                receiverNotificationContext.setReference(reference);
                MPAY_Account receiverAccount = context.getTransaction().getReceiverMobileAccount().getRefAccount();
                context.getDataProvider().refreshEntity(receiverAccount);
                receiverNotificationContext.setReceiverBalance(
                        SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
                receiverNotificationContext.setReceiverBanked(context.getTransaction().getReceiverMobileAccount()
                        .getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                receiverNotificationContext.setExtraData1(receiverMobile.getMobileNumber());

                final MPAY_Language prefLang = receiverMobile.getRefCustomer().getPrefLang();
                addNotification(context.getMessage(), notifications, receiverMobile.getEmail(),prefLang ,
                        receiverMobile.getEnableEmail(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, receiverMobile.getMobileNumber(), prefLang,
                        receiverMobile.getEnableSMS(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                        receiverMobile.getEnablePushNotification(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            if (senderMobile != null) {
                SalaryDisbursementNotificationContext senderNotificationContext = new SalaryDisbursementNotificationContext();
                senderNotificationContext.setCurrency(currency);
                senderNotificationContext.setReference(reference);
                MPAY_Account senderAccount = context.getTransaction().getSenderMobileAccount().getRefAccount();
                context.getDataProvider().refreshEntity(senderAccount);
                senderNotificationContext.setReceiverBalance(
                        SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
                senderNotificationContext.setReceiverBanked(context.getTransaction().getSenderMobileAccount()
                        .getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                senderNotificationContext.setExtraData1(senderMobile.getMobileNumber());

                final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
                addNotification(context.getMessage(), notifications, senderMobile.getEmail(),prefLang ,
                        senderMobile.getEnableEmail(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
                        senderMobile.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
                        senderMobile.getEnablePushNotification(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateReversalNotificationMessages", e);
            return new ArrayList<>();
        }
    }
}