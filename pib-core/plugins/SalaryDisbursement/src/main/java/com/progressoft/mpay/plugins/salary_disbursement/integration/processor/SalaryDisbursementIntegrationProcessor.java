package com.progressoft.mpay.plugins.salary_disbursement.integration.processor;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

@FunctionalInterface
public interface SalaryDisbursementIntegrationProcessor {
	public abstract IntegrationProcessingResult processIntegration(IntegrationProcessingContext context);
}
