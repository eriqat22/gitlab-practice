package com.progressoft.mpay.plugins.salary_disbursement.notification;

import com.progressoft.mpay.plugins.salary_disbursement.notification.view.SalaryDisbursementNotificationContextView;
import com.progressoft.mpay.plugins.NotificationContext;

public class SalaryDisbursementNotificationContext extends NotificationContext
		implements SalaryDisbursementNotificationContextView {
	private String receiver;
	private String currency;
	private String amount;
	private String senderBalance;
	private String receiverBalance;
	private boolean isSenderBanked;
	private boolean isReceiverBanked;
	private String senderCharges;
	private String receiverCharges;

	@Override
	public String getReceiver() {
		return receiver;
	}

	@Override
	public String getCurrency() {
		return currency;
	}

	@Override
	public String getAmount() {
		return amount;
	}

	@Override
	public String getSenderBalance() {
		return senderBalance;
	}

	@Override
	public String getReceiverBalance() {
		return receiverBalance;
	}

	@Override
	public boolean isSenderBanked() {
		return isSenderBanked;
	}

	@Override
	public boolean isReceiverBanked() {
		return isReceiverBanked;
	}

	@Override
	public String getSenderCharges() {
		return senderCharges;
	}

	@Override
	public String getReceiverCharges() {
		return receiverCharges;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setSenderBalance(String senderBalance) {
		this.senderBalance = senderBalance;
	}

	public void setReceiverBalance(String receiverBalance) {
		this.receiverBalance = receiverBalance;
	}

	public void setSenderBanked(boolean isSenderBanked) {
		this.isSenderBanked = isSenderBanked;
	}

	public void setReceiverBanked(boolean isReceiverBanked) {
		this.isReceiverBanked = isReceiverBanked;
	}

	public void setSenderCharges(String senderCharges) {
		this.senderCharges = senderCharges;
	}

	public void setReceiverCharges(String receiverCharges) {
		this.receiverCharges = receiverCharges;
	}

	public boolean hasSenderCharges() {
		if (this.senderCharges == null || this.senderCharges.trim().length() == 0)
			return false;
		return Double.parseDouble(this.senderCharges) > 0;
	}

	public boolean hasReceiverCharges() {
		if (this.receiverCharges == null || this.receiverCharges.trim().length() == 0)
			return false;
		return Double.parseDouble(this.receiverCharges) > 0;
	}

}
