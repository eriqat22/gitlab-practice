package com.progressoft.mpay.plugins.salary_disbursement.integration.processor;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.salary_disbursement.message.SalaryDisbursementMessage;
import com.progressoft.mpay.plugins.salary_disbursement.notification.SalaryDisbursementNotificationProcessor;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;

public class SalaryDisbursementResponseProcessor implements SalaryDisbursementIntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(SalaryDisbursementResponseProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		try {
			logger.debug("Inside ProcessIntegration ...");
			preProcessIntegration(context);
			if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return null;
			if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return accept(context);
			else
				return reject(context);
		} catch (Exception ex) {
			logger.error("Error in ProcessIntegration", ex);
			return reject(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside PreProcessIntegration...");
		if (context.getMessage() != null)
			context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		SalaryDisbursementMessage request = null;
		try {
			request = SalaryDisbursementMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
		} catch (MessageParsingException ex) {
			logger.error("Error while parsing message", ex);
			throw new WorkflowException(ex);
		}
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setAmount(request.getAmount());
		sender.setService(context.getTransaction().getSenderService());
		sender.setCorporate(sender.getService().getRefCorporate());
		sender.setServiceAccount(context.getTransaction().getSenderServiceAccount());
		sender.setAccount(sender.getServiceAccount().getRefAccount());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(), sender.getAccount().getAccNumber()));
		sender.setInfo(sender.getService().getRefCorporate().getServiceName());
		sender.setProfile(sender.getServiceAccount().getRefProfile());
		sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

		context.setOperation(context.getMessage().getRefOperation());
		receiver.setMobile(context.getTransaction().getReceiverMobile());
		context.setDirection(context.getTransaction().getDirection().getId());
		long personClientType = sender.getCorporate().getClientType().getId();
		context.getReceiver().setInfo(request.getReceiverInfo());
		if (receiver.getMobile() == null) {
			context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(),
					context.getLookupsLoader().getMPClear().getClientType(), false, context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT));
			return;
		}
		receiver.setMobileAccount(context.getTransaction().getReceiverMobileAccount());
		receiver.setAccount(receiver.getMobileAccount().getRefAccount());
		receiver.setCustomer(receiver.getMobile().getRefCustomer());
		receiver.setBank(receiver.getMobileAccount().getBank());
		receiver.setBanked(receiver.getAccount().getIsBanked());
		receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(), receiver.getAccount().getAccNumber()));
		receiver.setProfile(receiver.getMobileAccount().getRefProfile());
		receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
		context.setDirection(context.getTransaction().getDirection().getId());
		context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(), context.getLookupsLoader().getCustomerClientType().getId(), receiver.isBanked(),
				context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT));
		handlePushNotification(context,request);
	}

	private void handlePushNotification(IntegrationProcessingContext context, SalaryDisbursementMessage message) {
		MPAY_CorporateDevice senderCorporateDevice = context.getDataProvider().getCorporateDevice(message.getDeviceId());
		String receiverDeviceToken = context.getReceiver().getMobile().getRefCustomerMobileCustomerDevices()
				.stream()
				.filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
				.findFirst()
				.map(MPAY_CustomerDevice::getExtraData).orElse(null);
		context.setSenderNotificationToken(senderCorporateDevice != null ? senderCorporateDevice.getExtraData() : null);
		context.setReceiverNotificationToken(receiverDeviceToken);
	}

	private IntegrationProcessingResult accept(IntegrationProcessingContext context)  {
		logger.debug("Inside Accept ...");
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
		result.setNotifications(SalaryDisbursementNotificationProcessor.createAcceptanceNotificationMessages(context));
		return result;
	}

	private IntegrationProcessingResult reject(IntegrationProcessingContext context) throws SQLException {
		logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		String reasonDescription = null;
		if (context.getMpClearIsoMessage().getField(47) != null)
			reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
				context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
		context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(),
				BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
		result.setNotifications(SalaryDisbursementNotificationProcessor.createRejectionNotificationMessages(context));
		return result;
	}

	private IntegrationProcessingResult reject(IntegrationProcessingContext context, String processingCode, String reasonCode, String reasonDescription) {
		logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, processingCode, reasonCode, reasonDescription, null, false);
		try {
			context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(),
					BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
		} catch (SQLException e) {
			logger.error("Failed when trying to reverse limits", e);
		}
		result.setNotifications(SalaryDisbursementNotificationProcessor.createRejectionNotificationMessages(context));
		return result;
	}

}
