package com.progressoft.mpay.plugins.salary_disbursement.processor;


import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.plugins.salary_disbursement.integration.factory.SalaryDisbursementIntegrationProcessorFactory;
import com.progressoft.mpay.plugins.salary_disbursement.integration.processor.SalaryDisbursementIntegrationProcessor;
import com.progressoft.mpay.plugins.salary_disbursement.message.SalaryDisbursementMessage;
import com.progressoft.mpay.plugins.salary_disbursement.notification.SalaryDisbursementNotificationProcessor;
import com.progressoft.mpay.plugins.salary_disbursement.validator.SalaryDisbursementValidator;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class SalaryDisbursementProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(SalaryDisbursementProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = SalaryDisbursementValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside ProcessIntegration ...");
        IsoMessage mpClearIsoMessage = context.getMpClearIsoMessage();
        try {
            SalaryDisbursementIntegrationProcessor processor = SalaryDisbursementIntegrationProcessorFactory.createProcessor(mpClearIsoMessage.getType());
            IntegrationProcessingResult result = processor.processIntegration(context);
            handleCommissions(context, result.getProcessingStatus());
            return result;
        } catch (Exception e) {
            logger.error("Error when ProcessIntegration", e);
            IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null, context.isRequest());
            if (context.isRequest())
                result.setMpClearOutMessage(MPClearHelper.createMPClearResponse(context, mpClearIsoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(), MPClearHelper.getResponseType(mpClearIsoMessage.getType()), result.getReason().getCode(), result.getReasonDescription(),
                        String.valueOf(ProcessingCodeRanges.CREDIT_START_VALUE)));
            return result;
        }
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside Reverse ...");
        try {
            JVPostingResult result = TransactionHelper.reverseTransaction(context);
            MPAY_MobileAccount mobileAccount = SystemHelper.getMobileAccount(context.getSystemParameters(), context.getSender().getMobile(), null);
            if (mobileAccount != null)
                context.getDataProvider().updateAccountLimit(mobileAccount.getRefAccount().getId(),
                        context.getMessage().getRefOperation().getMessageType().getId(),
                        context.getTransaction().getTotalAmount().negate(), -1, SystemHelper.getCurrentDateWithoutTime());
            context.getDataProvider().mergeTransaction(context.getTransaction());
            if (result.isSuccess())
                return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

            return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
        } catch (Exception ex) {
            logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
            return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), ProcessingStatusCodes.FAILED);
        }
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException {
        logger.debug("Inside AcceptMessage ...");
        SalaryDisbursementMessage message = (SalaryDisbursementMessage) context.getRequest();
        MessageProcessingResult result = new MessageProcessingResult();
        MPAY_ProcessingStatus status;
        MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
        String reasonDescription = null;

        context.getMessage().setReason(reason);
        MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO, message.getNotes());
        result.setMessage(context.getMessage());
        context.setTransaction(transaction);
        result.setTransaction(transaction);
        context.getDataProvider().persistProcessingResult(result);
        JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), transaction);
        if (postingResult.isSuccess()) {
            BankIntegrationResult integrationResult = BankIntegrationHandler.handleIntegrations(context);
            switch (integrationResult.getStatus()) {
                case BankIntegrationStatus.ACCEPTED:
                    status = handleMPClear(context, message, result);
                    break;
                case BankIntegrationStatus.REJECTED:
                    status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
                    reason = context.getLookupsLoader().getReason(integrationResult.getReasonCode());
                    reasonDescription = integrationResult.getReasonDescription();
                    TransactionHelper.reverseTransaction(context);
                    break;
                default:
                    status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
                    reason = context.getLookupsLoader().getReason(integrationResult.getReasonCode());
                    reasonDescription = integrationResult.getReasonDescription();
                    TransactionHelper.reverseTransaction(context);
                    break;
            }
        } else {
            if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
                context.getMessage().setReasonDesc(postingResult.getReason());
            } else
                reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
        }
        context.getMessage().setProcessingStatus(status);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
        transaction.setReason(context.getMessage().getReason());
        transaction.setReasonDesc(context.getMessage().getReasonDesc());
        result.setStatus(status);
        result.setStatusDescription(context.getMessage().getReasonDesc());
        handleNotifications(context, result, status);
        OTPHanlder.removeOTP(context);
        handleCommissions(context, status);
        handleLimits(context, status, transaction);
        return result;
    }

    private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
        if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            return;
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.OUTWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(), CommissionDirections.SENDER);
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.INWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(), CommissionDirections.RECEIVER);
    }

    private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, SalaryDisbursementMessage message, MessageProcessingResult result) {
        MPAY_ProcessingStatus status;
        Integer mpClearIntegrationType;
        if (context.getSystemParameters().getOfflineModeEnabled() && context.getDirection() == TransactionDirection.ONUS) {
            mpClearIntegrationType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
            result.setHandleMPClearOffline(true);
        } else {
            mpClearIntegrationType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        }
        IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegrationType, message.getReceiverType().equals(ReceiverInfoType.ALIAS));
        MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegrationType));
        messageLog.setRefMessage(context.getMessage());
        result.setMpClearMessage(messageLog);
        return status;
    }

    private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result, MPAY_ProcessingStatus status) {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)) {
            result.setNotifications(SalaryDisbursementNotificationProcessor.createAcceptanceNotificationMessages(context));
        } else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
            result.setNotifications(SalaryDisbursementNotificationProcessor.createRejectionNotificationMessages(context));
    }

    private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status, MPAY_Transaction transaction) throws SQLException {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED) || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
            context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
                    context.getMessage().getRefOperation().getMessageType().getId(),
                    transaction.getTotalAmount(), 1, SystemHelper.getCurrentDateWithoutTime());
        }
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, SQLException {
        logger.debug("Inside PreProcessMessage ...");
        SalaryDisbursementMessage message = SalaryDisbursementMessage.parseMessage(context.getRequest());

        context.setRequest(message);
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setAmount(message.getAmount());
        sender.setInfo(message.getSender());
        context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return;
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setBank(sender.getServiceAccount().getBank());
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        sender.setBanked(sender.getAccount().getIsBanked());
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
        receiver.setInfo(message.getReceiverInfo());
        receiver.setMobile(context.getDataProvider().getCustomerMobile(message.getReceiverInfo(), message.getReceiverType()));
        if (receiver.getMobile() != null && receiver.getMobile().getStatusId().getCode().equalsIgnoreCase("100103"))
            receiver.setMobile(null);
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        OTPHanlder.loadOTP(context, message.getPin());
        receiver.setNotes(message.getNotes());
        if (receiver.getMobile() != null) {
            receiver.setCustomer(receiver.getMobile().getRefCustomer());
            receiver.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), receiver.getMobile(), null));
            if (receiver.getMobileAccount() == null)
                return;
            receiver.setAccount(receiver.getMobileAccount().getRefAccount());
            receiver.setBank(receiver.getMobileAccount().getBank());
            receiver.setProfile(receiver.getMobile().getRefProfile());
            receiver.setBanked(receiver.getAccount().getIsBanked());
            receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
            context.setDirection(TransactionDirection.ONUS);
            context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(), sender.isBanked(), personClientType, receiver.isBanked(), context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
        } else {
            context.getReceiver().setService(context.getLookupsLoader().getPSP().getPspMPClearService());
            context.getReceiver().setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), context.getReceiver().getService(), null));
            if (context.getReceiver().getServiceAccount() != null)
                context.getReceiver().setBank(context.getReceiver().getServiceAccount().getBank());
            context.setDirection(TransactionDirection.OUTWARD);
            context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(), sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false, context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
        }
        if (context.getTransactionConfig() != null)
            context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
        ChargesCalculator.calculate(context);
        TaxCalculator.claculate(context);
        handlePushNotification(context, message);
    }

    private void handlePushNotification(MessageProcessingContext context, SalaryDisbursementMessage message) {
        String receiverDeviceToken = context.getReceiver().getMobile().getRefCustomerMobileCustomerDevices()
                .stream()
                .filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CustomerDevice::getExtraData).orElse(null);
        MPAY_CorporateDevice senderCorporateDevice = context.getDataProvider().getCorporateDevice(message.getDeviceId());
        context.setSenderNotificationToken(senderCorporateDevice != null ? senderCorporateDevice.getExtraData() : null);
        context.setReceiverNotificationToken(receiverDeviceToken);
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        if (context.getTransaction() != null) {
            BankIntegrationResult result = BankIntegrationHandler.reverse(context);
            if (result != null)
                TransactionHelper.reverseTransaction(context);
        }
        return MessageProcessingResult.create(context, status, reason, reasonDescription);
    }

    private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isReceiverAlias) {
        logger.debug("Inside CreateMPClearRequest ...");
        SalaryDisbursementMessage request = (SalaryDisbursementMessage) context.getRequest();
        String messageID = context.getDataProvider().getNextMPClearMessageId();
        BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
        IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(type);
        message.setBinary(false);

        String formattedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
        String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

        message.setField(4, new IsoValue<>(IsoType.NUMERIC, formattedAmount, 12));
        MPClearHelper.setMessageEncoded(message);
        message.setField(7, new IsoValue<>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
        message.setField(46, new IsoValue<>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
        message.setField(49, new IsoValue<>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

        String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getService(), context.getSender().getServiceAccount(), false);
        MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

        String account2;
        if (context.getReceiver().getMobile() == null)
            account2 = MPClearHelper.getAccount(context.getSystemParameters(), request.getReceiverInfo(), request.getReceiverType());
        else
            account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getMobile(), context.getReceiver().getMobileAccount(), isReceiverAlias);

        MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

        String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
        message.setField(104, new IsoValue<>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
        message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));

        return message;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        if (context.getMessage() != null) {
            context.getMessage().setReason(reason);
            context.getMessage().setReasonDesc(reasonDescription);
            context.getMessage().setProcessingStatus(status);
        }
        if (context.getTransaction() != null) {
            context.getTransaction().setReason(reason);
            context.getTransaction().setReasonDesc(reasonDescription);
            context.getTransaction().setProcessingStatus(status);
        }
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setTransaction(context.getTransaction());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        if (reasonCode.equals(ReasonCodes.VALID)) {
            result.setNotifications(SalaryDisbursementNotificationProcessor.createReversalNotificationMessages(context));
        }
        return result;
    }
}
