package com.progressoft.mpay.plugins.salary_disbursement.notification.view;

public interface SalaryDisbursementNotificationContextView {

	String getReceiver();

	String getCurrency();

	String getAmount();

	String getSenderBalance();

	String getReceiverBalance();

	boolean isSenderBanked();

	boolean isReceiverBanked();

	String getSenderCharges();

	String getReceiverCharges();

}