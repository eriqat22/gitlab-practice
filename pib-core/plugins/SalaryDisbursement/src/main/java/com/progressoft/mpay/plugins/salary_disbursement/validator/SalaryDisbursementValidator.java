package com.progressoft.mpay.plugins.salary_disbursement.validator;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.salary_disbursement.message.SalaryDisbursementMessage;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SalaryDisbursementValidator {
    private static final Logger logger = LoggerFactory.getLogger(SalaryDisbursementValidator.class);

    private SalaryDisbursementValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ....");
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
                context.getOperation().getMessageType());
        if (!result.isValid())
            return result;
        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        SalaryDisbursementMessage request = (SalaryDisbursementMessage) context.getRequest();

        result = validateSenderInfo(context);
        if (!result.isValid())
            return result;

        if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
            result = MobileNumberValidator.validate(context, request.getReceiverInfo());
            if (!result.isValid())
                return result;
        }

        if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getMobile() != null) {
            result = validateReceiver(context);
            if (!result.isValid())
                return result;
        }

        return validateSystemSettings(context, request);
    }

    private static ValidationResult validateSystemSettings(MessageProcessingContext context,
                                                           SalaryDisbursementMessage request) {
        ValidationResult result = AmountValidator.validate(context);
        if (!result.isValid())
            return result;

        result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
        if (!result.isValid())
            return result;

        result = LimitsValidator.validate(context);
        if (!result.isValid())
            return result;

        return PSPValidator.validate(context, false);
    }

    private static ValidationResult validateSenderInfo(MessageProcessingContext context) {
        logger.debug("Inside validateSenderInfo ...");
        ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        return result;
    }

    private static ValidationResult validateReceiver(MessageProcessingContext context) {
        logger.debug("Inside ValidateReceiver ...");

        ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
        if (!result.isValid())
            return result;

        result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getReceiver().getMobileAccount(), false, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
