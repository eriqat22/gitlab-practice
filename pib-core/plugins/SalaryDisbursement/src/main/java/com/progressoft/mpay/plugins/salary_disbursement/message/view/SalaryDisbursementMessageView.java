package com.progressoft.mpay.plugins.salary_disbursement.message.view;

import java.math.BigDecimal;

public interface SalaryDisbursementMessageView {
	String getReceiverInfo();

	String getReceiverType();

	BigDecimal getAmount();

	String getNotes();

}