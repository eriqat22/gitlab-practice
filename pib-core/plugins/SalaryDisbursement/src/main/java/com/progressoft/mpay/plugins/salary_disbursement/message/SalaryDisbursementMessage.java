package com.progressoft.mpay.plugins.salary_disbursement.message;

import java.math.BigDecimal;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.plugins.salary_disbursement.message.view.SalaryDisbursementMessageView;

public class SalaryDisbursementMessage extends MPayRequest implements SalaryDisbursementMessageView {

	private static final String REGEX = "\\r\\n";
	private String receiverInfo;
	private String receiverType;
	private BigDecimal amount;
	private String notes;

	public enum SalaryDisbursementExtraData {
		RECEIVER_ID("rcvId"), RECEIVER_TYPE("rcvType"), AMOUNT("amnt"), NOTES("data");

		private String value;

		private SalaryDisbursementExtraData(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	private SalaryDisbursementMessage(Builder builder) {
		this.receiverInfo = builder.receiverInfo;
		this.receiverType = builder.receiverType;
		this.amount = builder.amount;
		this.notes = builder.notes;
	}


	@Override
	public String getReceiverInfo() {
		return receiverInfo;
	}

	@Override
	public String getReceiverType() {
		return receiverType;
	}

	@Override
	public BigDecimal getAmount() {
		return amount;
	}

	@Override
	public String getNotes() {
		return notes;
	}

	public static class Builder {
		private String receiverInfo;
		private MPayRequest request;
		private String receiverType;
		private BigDecimal amount;
		private String notes;

		public Builder(MPayRequest request) {
			this.request = request;
		}

		public Builder receiverInfo(String receiverInfo) {
			this.receiverInfo = receiverInfo;
			return this;
		}

		public Builder receiverType(String receiverType) {
			this.receiverType = receiverType;
			return this;
		}

		public Builder amount(BigDecimal amount) {
			this.amount = amount;
			return this;
		}

		public Builder notes(String notes) {
			this.notes = notes;
			return this;
		}

		public SalaryDisbursementMessage build() {
			SalaryDisbursementMessage salaryDisbursementMessage = new SalaryDisbursementMessage(this);
			setRequestValue(salaryDisbursementMessage);
			return salaryDisbursementMessage;
		}

		private void setRequestValue(SalaryDisbursementMessage salaryDisbursementMessage) {
			salaryDisbursementMessage.setOperation(request.getOperation());
			salaryDisbursementMessage.setSender(request.getSender());
			salaryDisbursementMessage.setSenderType(request.getSenderType());
			salaryDisbursementMessage.setDeviceId(request.getDeviceId());
			salaryDisbursementMessage.setLang(request.getLang());
			salaryDisbursementMessage.setMsgId(request.getMsgId());
			salaryDisbursementMessage.setPin(request.getPin());
			salaryDisbursementMessage.setExtraData(request.getExtraData());
		}
	}

	public static SalaryDisbursementMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		Builder builder = new Builder(request);

		if (isSenderNullOrEmpty(request.getSender()))
			throw new MessageParsingException(SENDER_KEY);

		if (isSenderTypeNullOrNotCorporate(request.getSenderType()))
			throw new MessageParsingException(SENDER_TYPE_KEY);
		validateAmountAndSetAmountValue(builder, request);

		validateRecevierInfoAndSetRecevierInfoValue(builder, request);

		validateReceiverTypeAndSetRecevierTypeValue(builder, request);

		builder.notes(request.getValue(SalaryDisbursementExtraData.NOTES.value)==null?"":request.getValue(SalaryDisbursementExtraData.NOTES.value).replaceAll(REGEX, ""));
		return builder.build();
	}

	private static boolean isAmountNull(String amount) {
		return amount == null;
	}

	private static boolean isSenderTypeNullOrNotCorporate(String senderType) {
		return senderType == null || !senderType.trim().equals(ReceiverInfoType.CORPORATE);
	}

	private static boolean isSenderNullOrEmpty(String sender) {
		return sender == null || sender.trim().length() == 0;
	}

	private static void validateReceiverTypeAndSetRecevierTypeValue(Builder builder, MPayRequest request)
			throws MessageParsingException {
		String receiverType = request.getValue(SalaryDisbursementExtraData.RECEIVER_TYPE.value);
		if (receiverType == null)
			throw new MessageParsingException(SalaryDisbursementExtraData.RECEIVER_TYPE.value);
		builder.receiverType(receiverType.replaceAll(REGEX, ""));
	}

	private static void validateRecevierInfoAndSetRecevierInfoValue(Builder builder, MPayRequest request)
			throws MessageParsingException {
		String receiverInfo = request.getValue(SalaryDisbursementExtraData.RECEIVER_ID.value);
		if (receiverInfo == null)
			throw new MessageParsingException(SalaryDisbursementExtraData.RECEIVER_ID.value);
		builder.receiverInfo(receiverInfo.replaceAll(REGEX, ""));
	}

	private static void validateAmountAndSetAmountValue(Builder builder, MPayRequest request)
			throws MessageParsingException {
		String amount = request.getValue(SalaryDisbursementExtraData.AMOUNT.value).trim();
		if (isAmountNull(amount))
			throw new MessageParsingException(SalaryDisbursementExtraData.AMOUNT.value);
		try {
			builder.amount(BigDecimal.valueOf(Double.valueOf(amount.replaceAll(REGEX, ""))));
		} catch (NumberFormatException e) {
			throw new MessageParsingException(SalaryDisbursementExtraData.AMOUNT.value);
		}
	}

}
