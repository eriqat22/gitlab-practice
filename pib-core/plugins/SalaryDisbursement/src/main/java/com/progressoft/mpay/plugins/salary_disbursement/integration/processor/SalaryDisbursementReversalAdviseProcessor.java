package com.progressoft.mpay.plugins.salary_disbursement.integration.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class SalaryDisbursementReversalAdviseProcessor implements SalaryDisbursementIntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(SalaryDisbursementReversalAdviseProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

}
