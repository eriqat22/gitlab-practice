package com.progressoft.mpay.plugins.salary_disbursement.constant;

public class Constants {
	public static final String NOTIFICATION_TOKEN_KEY = "NotificationToken";

	private Constants() {

	}
}
