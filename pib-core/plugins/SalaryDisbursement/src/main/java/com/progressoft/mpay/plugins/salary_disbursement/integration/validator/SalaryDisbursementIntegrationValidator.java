package com.progressoft.mpay.plugins.salary_disbursement.integration.validator;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;

public class SalaryDisbursementIntegrationValidator {

	private static final Logger logger = LoggerFactory.getLogger(SalaryDisbursementIntegrationValidator.class);

	private SalaryDisbursementIntegrationValidator() {

	}

	public static ValidationResult validate(IntegrationProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
		if (!result.isValid())
			return result;

		result = TransactionConfigValidator.validate(context.getTransactionConfig(), context.getAmount());
		if (!result.isValid())
			return result;

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
