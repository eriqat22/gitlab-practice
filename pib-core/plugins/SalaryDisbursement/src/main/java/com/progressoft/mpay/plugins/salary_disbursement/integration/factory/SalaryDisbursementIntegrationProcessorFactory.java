package com.progressoft.mpay.plugins.salary_disbursement.integration.factory;

import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.salary_disbursement.integration.processor.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.NotSupportedException;

public class SalaryDisbursementIntegrationProcessorFactory {
    private static final Logger logger = LoggerFactory.getLogger(SalaryDisbursementIntegrationProcessorFactory.class);

    private SalaryDisbursementIntegrationProcessorFactory() {

    }

    public static SalaryDisbursementIntegrationProcessor createProcessor(int messageType) throws NotSupportedException {
        logger.debug("Inside SalaryDisbursement CreateProcessor ...");
        logger.debug("messageType: " + messageType);

        if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)
            return new SalaryDisbursementInwardProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE)
            return new SalaryDisbursementResponseProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_RESPONSE)
            return new SalaryDisbursementOfflineResponseProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST)
            return new SalaryDisbursementReversalAdviseProcessor();
        else
            throw new NotSupportedException("messageType == " + messageType);
    }
}
