package com.progressoft.mpay.plugins.aliaschange;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.SystemNetworkStatus;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;

public class AliasChangeValidator {
	private static final Logger logger = LoggerFactory.getLogger(AliasChangeValidator.class);

	private AliasChangeValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = validateSender(context);
		if (!result.isValid())
			return result;

		AliasChangeMessage message = (AliasChangeMessage) context.getRequest();
		if (context.getDataProvider().isAliasUsed(message.getAlias()))
			return new ValidationResult(ReasonCodes.ALIAS_IS_DEFINED, null, false);

		if (!SystemNetworkStatus.isSystemOnline(context.getDataProvider()))
			return new ValidationResult(ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE, null, false);

		result = LimitsValidator.validate(context);

		return result;
	}

	private static ValidationResult validateSender(MessageProcessingContext context) {
		if(context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)){

			ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
			if (!result.isValid())
				return result;

			result = CustomerValidator.validate(context.getSender().getCustomer(), true);
			if (!result.isValid())
				return result;

			result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
			if (!result.isValid())
				return result;

			result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
			if (!result.isValid())
				return result;

			result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
			if (!result.isValid())
				return result;

			return PinCodeValidator.validate(context, context.getSender().getMobile(), context.getRequest().getPin());
		}
		else{

			ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
			if (!result.isValid())
				return result;

			result = CorporateValidator.validate(context.getSender().getCorporate(), true);
			if (!result.isValid())
				return result;

			result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
			if (!result.isValid())
				return result;

			result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getService().getId()));
			if (!result.isValid())
				return result;

			result = SignInValidator.validate(context, context.getSender().getService(), context.getRequest().getDeviceId());
			if (!result.isValid())
				return result;

			return PinCodeValidator.validate(context, context.getSender().getService(), context.getRequest().getPin());
		}
	}
}
