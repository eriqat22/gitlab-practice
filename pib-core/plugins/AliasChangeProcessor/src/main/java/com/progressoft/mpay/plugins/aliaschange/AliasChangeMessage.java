package com.progressoft.mpay.plugins.aliaschange;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class AliasChangeMessage extends MPayRequest {
	private static final String ALIAS_KEY = "alias";

	private String alias;

	public AliasChangeMessage() {
		//
	}

	public AliasChangeMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public static AliasChangeMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		AliasChangeMessage message = new AliasChangeMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || ((!message.getSenderType().trim().equals(ReceiverInfoType.MOBILE)&&!message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)
				&& (!message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setAlias(message.getValue(ALIAS_KEY));
		if (message.getAlias() == null || message.getAlias().trim().length() == 0)
			throw new MessageParsingException(ALIAS_KEY);
		return message;
	}
}
