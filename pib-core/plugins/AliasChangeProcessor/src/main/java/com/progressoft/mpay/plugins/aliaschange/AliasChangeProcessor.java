package com.progressoft.mpay.plugins.aliaschange;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.EnumMap;
import java.util.Map;

import com.progressoft.mpay.entities.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.integration.JfwFacade.Option;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.NotificationContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class AliasChangeProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(AliasChangeProcessor.class);

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside ProcessIntegration ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return null;
            if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return acceptIntegration(context);
            else
                return rejectIntegration(context);
        } catch (Exception ex) {
            logger.error("Error when ProcessIntegration in AliasChangeProcessor", ex);
            return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED,
                    ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), null, false);
        }
    }

    private IntegrationProcessingResult rejectIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside RejectIntegration ...");

        AliasChangeMessage message;
        try {
            message = AliasChangeMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            IntegrationProcessingResult result;
            if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
                result = rejectCustomerAliasIntegration(context,message.getDeviceId());
            } else
                result = rejectServiceAliasIntegration(context,message.getDeviceId());
            return result;
        } catch (MessageParsingException e) {
            logger.error("Error when ProcessIntegration in AliasChangeProcessor", e);
            return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED,
                    ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null, false);
        }

    }

    private IntegrationProcessingResult rejectCustomerAliasIntegration(IntegrationProcessingContext context, String deviceId) {
        String reasonDescription = context.getMpClearIsoMessage().getField(44).getValue().toString() + " - "
                + context.getMpClearIsoMessage().getField(47).getValue().toString();
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED,
                ReasonCodes.REJECTED_BY_NATIONAL_SWITCH, reasonDescription, null, context.isRequest());
        MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(context.getMessage().getSender());

        NotificationContext notificationContext = new NotificationContext();
        notificationContext.setReference(context.getMessage().getReference());

        String deviceToken = mobile.getRefCustomerMobileCustomerDevices()
                .stream()
                .filter(d -> d.getDeviceID().equals(deviceId) && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CustomerDevice::getExtraData).orElse(null);
        notificationContext.setExtraData1(mobile.getMobileNumber());
        addNotification(context.getMessage(), result.getNotifications(), mobile.getEmail(), mobile.getRefCustomer().getPrefLang(),
                mobile.getEnableEmail(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
        addNotification(context.getMessage(), result.getNotifications(), mobile.getMobileNumber(), mobile.getRefCustomer().getPrefLang(),
                mobile.getEnableSMS(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
        addNotification(context.getMessage(), result.getNotifications(), deviceToken, mobile.getRefCustomer().getPrefLang(),
                mobile.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION);

        return result;
    }

    private IntegrationProcessingResult rejectServiceAliasIntegration(IntegrationProcessingContext context, String deviceId) {
        String reasonDescription = context.getMpClearIsoMessage().getField(44).getValue().toString() + " - "
                + context.getMpClearIsoMessage().getField(47).getValue().toString();
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED,
                ReasonCodes.REJECTED_BY_NATIONAL_SWITCH, reasonDescription, null, context.isRequest());
        MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(context.getMessage().getSender(),
                ReceiverInfoType.CORPORATE);

        NotificationContext notificationContext = new NotificationContext();
        notificationContext.setReference(context.getMessage().getReference());

        String deviceToken = service.getRefCorporateServiceCorporateDevices()
                .stream()
                .filter(d -> d.getDeviceID().equals(deviceId) && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CorporateDevice::getExtraData).orElse(null);
        notificationContext.setExtraData1(service.getName());
        addNotification(context.getMessage(), result.getNotifications(), service.getEmail(), service.getRefCorporate().getPrefLang(),
                service.getEnableEmail(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
        addNotification(context.getMessage(), result.getNotifications(), service.getMobileNumber(), service.getRefCorporate().getPrefLang(),
                service.getEnableSMS(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
        addNotification(context.getMessage(), result.getNotifications(), deviceToken, service.getRefCorporate().getPrefLang(),
                service.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION);

        return result;
    }

    private IntegrationProcessingResult acceptIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside AcceptIntegration ...");
        try {

            IntegrationProcessingResult result = IntegrationProcessingResult.create(context,
                    ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, context.isRequest());

            String senderType = context.getOriginalMPClearMessage().getHint()
                    .substring(context.getOriginalMPClearMessage().getHint().indexOf(';') + 1);

            if (senderType.equals(ReceiverInfoType.MOBILE))
                result = customerProcessIntegration(context, result);
            else
                result = serviceProcessIntegration(context, result);
            return result;
        } catch (Exception e) {
            throw new MPayGenericException("Error while acceptIntegration", e);
        }
    }

    private IntegrationProcessingResult customerProcessIntegration(IntegrationProcessingContext context,
                                                                   IntegrationProcessingResult result) throws MessageParsingException, WorkflowException {
        MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(context.getMessage().getSender());
        AliasChangeMessage message = AliasChangeMessage
                .parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
        mobile.setAlias(message.getAlias());
        context.getDataProvider().mergeCustomerMobile(mobile);
        if (mobile.getStatusId().getCode().equals(Constants.APPROVAL_WF_STEP_CODE)) {
            Map<Option, Object> options = new EnumMap<>(Option.class);
            options.put(Option.ACTION_NAME, "SVC_Approve");
            JfwHelper.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS, mobile, options);
        }
        NotificationContext notificationContext = new NotificationContext();
        notificationContext.setReference(context.getMessage().getReference());
        notificationContext.setSender(message.getSender());
        notificationContext.setExtraData1(message.getAlias());

        String deviceToken = mobile.getRefCustomerMobileCustomerDevices()
                .stream()
                .filter(d -> message.getDeviceId().equals(d.getDeviceID()) && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CustomerDevice::getExtraData).orElse(null);
        notificationContext.setExtraData1(mobile.getMobileNumber());
        addNotification(context.getMessage(), result.getNotifications(), mobile.getEmail(), mobile.getRefCustomer().getPrefLang(),
                mobile.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
        addNotification(context.getMessage(), result.getNotifications(), mobile.getMobileNumber(), mobile.getRefCustomer().getPrefLang(),
                mobile.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
        addNotification(context.getMessage(), result.getNotifications(), deviceToken, mobile.getRefCustomer().getPrefLang(),
                mobile.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION);

        return result;
    }

    private IntegrationProcessingResult serviceProcessIntegration(IntegrationProcessingContext context,
                                                                  IntegrationProcessingResult result) throws MessageParsingException, WorkflowException {
        MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(context.getMessage().getSender(),
                ReceiverInfoType.CORPORATE);
        AliasChangeMessage message = AliasChangeMessage
                .parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
        service.setAlias(message.getAlias());
        context.getDataProvider().mergeCorporateService(service);
        if (service.getStatusId().getCode().equals(Constants.APPROVAL_WF_STEP_CODE)) {
            Map<Option, Object> options = new EnumMap<>(Option.class);
            options.put(Option.ACTION_NAME, "SVC_Approve");
            JfwHelper.executeAction(MPAYView.CORPORATE_SERVICE_VIEWS, service, options);
        }
        NotificationContext notificationContext = new NotificationContext();
        notificationContext.setReference(context.getMessage().getReference());

        String deviceToken = service.getRefCorporateServiceCorporateDevices()
                .stream()
                .filter(d -> message.getDeviceId().equals(d.getDeviceID()) && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CorporateDevice::getExtraData).orElse(null);
        notificationContext.setExtraData1(service.getName());
        addNotification(context.getMessage(), result.getNotifications(), service.getEmail(), service.getRefCorporate().getPrefLang(),
                service.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
        addNotification(context.getMessage(), result.getNotifications(), service.getMobileNumber(), service.getRefCorporate().getPrefLang(),
                service.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
        addNotification(context.getMessage(), result.getNotifications(), deviceToken, service.getRefCorporate().getPrefLang(),
                service.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION);

        return result;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult validationResult = AliasChangeValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in AliasChangeProcessor", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
                                                  String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        return createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context){
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null,
                ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        AliasChangeMessage message = (AliasChangeMessage) context.getRequest();

        if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
            MPClearClientHelper.getClient(context.getSystemParameters().getMPClearProcessor())
                    .changeCorporateServiceAlias(
                            new CoreComponents(context.getDataProvider(), context.getSystemParameters(),
                                    context.getLookupsLoader()),
                            context.getSender().getCorporate(), context.getSender().getService(), context.getMessage(),
                            message.getAlias());
        else
            MPClearClientHelper.getClient(context.getSystemParameters().getMPClearProcessor()).changeMobileAlias(
                    new CoreComponents(context.getDataProvider(), context.getSystemParameters(),
                            context.getLookupsLoader()),
                    context.getSender().getCustomer(), context.getSender().getMobile(), context.getMessage(),
                    message.getAlias());
        OTPHanlder.removeOTP(context);
        return result;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
        logger.debug("Inside PreProcessMessage ...");
        AliasChangeMessage message = AliasChangeMessage.parseMessage(context.getRequest());
        context.setRequest(message);
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        if (context.getRequest().getSenderType().trim().equals(ReceiverInfoType.MOBILE)) {
            sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
            if (sender.getMobile() == null)
                return;
            sender.setCustomer(sender.getMobile().getRefCustomer());
            sender.setMobileAccount(
                    SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
            if (sender.getMobileAccount() == null)
                return;
            sender.setProfile(sender.getMobileAccount().getRefProfile());
        } else {
            sender.setService(context.getDataProvider().getCorporateService(context.getRequest().getSender(),
                    ReceiverInfoType.CORPORATE));
            if (sender.getService() == null)
                return;
            sender.setCorporate(sender.getService().getRefCorporate());
            sender.setServiceAccount(
                    SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
            if (sender.getServiceAccount() == null)
                return;
            sender.setProfile(sender.getServiceAccount().getRefProfile());
        }
        OTPHanlder.loadOTP(context, message.getPin());
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext arg0) {
        logger.debug("Inside Reverse ...");
        return null;
    }
}
