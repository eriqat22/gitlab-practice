package com.progressoft.mpay.plugins.coporatecridetmoneytransfer.integrations;

import javax.transaction.NotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;

public class IntegrationProcessorFactory {
	private static final Logger logger = LoggerFactory.getLogger(IntegrationProcessorFactory.class);

	private IntegrationProcessorFactory() {

	}

	public static IntegrationProcessor createProcessor(int messageType) throws NotSupportedException {
		logger.debug("Inside CreateProcessor ...");
		logger.debug("messageType: " + messageType);
		if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)
			return new CorporateToCorporateInwardProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE)
			return new CorporateToCorporateResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_RESPONSE)
			return new CorporateToCorporateOfflineResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST)
			return new CorporateToCorporateReversalAdviseProcessor();
		else
			throw new NotSupportedException("messageType == " + messageType);
	}
}
