package com.progressoft.mpay.plugins.coporatecridetmoneytransfer.integrations;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public interface IntegrationProcessor {
	public abstract IntegrationProcessingResult processIntegration(IntegrationProcessingContext context);
}
