package com.progressoft.mpay.plugins.coporatecridetmoneytransfer.notification;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.coporatecridetmoneytransfer.message.CorporateToCorporateMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    private NotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateAcceptanceNotificationMessages ...");
        try {
            String receiver;
            String sender;
            String reference;
            long senderLanguageId;
            long receiverLanguageId;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            MPAY_CorpoarteService senderService = context.getSender().getService();
            MPAY_CorpoarteService receiverService = context.getReceiver().getService();
            MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

            CorporateToCorporateNotificationContext notificationContext = new CorporateToCorporateNotificationContext();
            notificationContext
                    .setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setOperation(operation.getOperation());
            notificationContext.setReference(reference);
            notificationContext.setFromService(context.getSender().getInfo());
            notificationContext.setToService(context.getReceiver().getInfo());
            if (senderService != null) {
                sender = senderService.getNotificationReceiver();
                context.getDataProvider().refreshEntity(context.getSender().getAccount());
                notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
                        context.getSender().getAccount().getBalance()));
                notificationContext.setSenderBanked(
                        context.getSender().getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setSenderCharges(
                        SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
                senderLanguageId = context.getSender().getCorporate().getPrefLang().getId();
            } else {
                sender = context.getSender().getInfo();
                senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
            }
            if (receiverService != null) {
                receiver = receiverService.getNotificationReceiver();
                context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(),
                        context.getReceiver().getAccount().getBalance()));
                notificationContext.setReceiverBanked(
                        context.getReceiver().getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setReceiverCharges(
                        SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
                receiverLanguageId = context.getReceiver().getCorporate().getPrefLang().getId();
            } else {
                receiver = context.getReceiver().getInfo();
                receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
            }
            if (context.getReceiver().getNotes() != null) {
                notificationContext.setNotes(context.getReceiver().getNotes());
                notificationContext.setHasNotes(true);
            }
            notificationContext.setReceiver(receiver);
            notificationContext.setSender(sender);
            return generateNotifications(context, senderLanguageId, receiverLanguageId, senderService, receiverService,
                    operation, notificationContext);
        } catch (Exception e) {
            logger.error("Error when CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    private static List<MPAY_Notification> generateNotifications(ProcessingContext context, long senderLanguageId,
                                                                 long receiverLanguageId, MPAY_CorpoarteService senderService, MPAY_CorpoarteService receiverService,
                                                                 MPAY_EndPointOperation operation, CorporateToCorporateNotificationContext notificationContext) {
        List<MPAY_Notification> notifications = new ArrayList<>();
        if (senderService != null) {
            notifications.add(context.getNotificationHelper().createNotification(context.getMessage(),
                    notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
                    senderService.getNotificationReceiver(), senderService.getNotificationChannel().getCode()));
        }
        if (receiverService != null) {
            notifications.add(context.getNotificationHelper().createNotification(context.getMessage(),
                    notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId,
                    receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
        }
        return notifications;
    }

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            CorporateToCorporateMessage request = CorporateToCorporateMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));

            MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(),
                    request.getSenderType());
            if (service == null)
                return new ArrayList<>();

            String senderInfo = service.getNotificationReceiver();
            MPAY_Language language = service.getRefCorporate().getPrefLang();
            CorporateToCorporateNotificationContext notificationContext = new CorporateToCorporateNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            if (context.getTransaction() == null)
                notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
            else
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
            notificationContext.setReceiver(request.getReceiverInfo());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(
                    MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
            notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext,
                    NotificationType.REJECTED_SENDER, context.getMessage().getRefOperation().getId(),
                    language.getId(), senderInfo, service.getNotificationChannel().getCode()));
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateReversalNotificationMessages ...");
        try {
            String receiver = null;
            String reference;
            List<MPAY_Notification> notifications = new ArrayList<>();

            reference = context.getTransaction().getReference();
            MPAY_CorpoarteService senderService = context.getTransaction().getSenderService();
            MPAY_CorpoarteService receiverService = context.getTransaction().getReceiverService();
            MPAY_EndPointOperation operation = context.getTransaction().getRefOperation();
            String currency = context.getTransaction().getCurrency().getStringISOCode();
            if (senderService != null) {
                CorporateToCorporateNotificationContext senderNotificationContext = new CorporateToCorporateNotificationContext();
                senderNotificationContext.setCurrency(currency);
                senderNotificationContext.setReference(reference);
                MPAY_Account senderAccount = context.getTransaction().getSenderServiceAccount().getRefAccount();
                context.getDataProvider().refreshEntity(senderAccount);
                senderNotificationContext.setReceiverBalance(
                        SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
                senderNotificationContext.setReceiverBanked(context.getTransaction().getSenderServiceAccount()
                        .getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                long senderLanguageId = senderService.getRefCorporate().getPrefLang().getId();
                senderNotificationContext.setReceiver(receiver);
                notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(),
                        senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
                        senderLanguageId, senderService.getNotificationReceiver(),
                        senderService.getNotificationChannel().getCode()));
            }
            if (receiverService != null) {
                CorporateToCorporateNotificationContext receiverNotificationContext = new CorporateToCorporateNotificationContext();
                receiverNotificationContext.setCurrency(currency);
                receiverNotificationContext.setReference(reference);
                MPAY_Account receiverAccount = context.getTransaction().getReceiverServiceAccount().getRefAccount();
                context.getDataProvider().refreshEntity(receiverAccount);
                receiverNotificationContext.setReceiverBalance(
                        SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
                receiverNotificationContext.setReceiverBanked(context.getTransaction().getReceiverServiceAccount()
                        .getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                long receiverLanguageId = receiverService.getRefCorporate().getPrefLang().getId();
                receiverNotificationContext.setReceiver(receiver);
                notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(),
                        receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
                        receiverLanguageId, receiverService.getNotificationReceiver(),
                        receiverService.getNotificationChannel().getCode()));
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateReversalNotificationMessages", e);
            return new ArrayList<>();
        }
    }
}