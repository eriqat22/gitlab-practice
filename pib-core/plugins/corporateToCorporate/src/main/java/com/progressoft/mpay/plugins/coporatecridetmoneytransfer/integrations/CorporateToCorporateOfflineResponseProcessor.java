package com.progressoft.mpay.plugins.coporatecridetmoneytransfer.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class CorporateToCorporateOfflineResponseProcessor implements IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(CorporateToCorporateOfflineResponseProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		if (context.getMpClearIsoMessage().getField(44).getValue().equals("00"))
			return accept(context);
		else
			return reject(context);
	}

	private IntegrationProcessingResult accept(IntegrationProcessingContext context) {
		logger.debug("Inside Accept ...");
		return IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null,
				null, false);
	}

	private IntegrationProcessingResult reject(IntegrationProcessingContext context) {
		logger.debug("Inside Reject ...");
		return IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED,
				ReasonCodes.REJECTED_BY_NATIONAL_SWITCH, context.getMpClearIsoMessage().getField(44).getValue() + " "
						+ context.getMpClearIsoMessage().getField(47).getValue(),
				null, false);
	}
}
