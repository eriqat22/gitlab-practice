package com.progressoft.mpay.plugins.coporatecridetmoneytransfer.validator;


import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.plugins.coporatecridetmoneytransfer.message.CorporateToCorporateMessage;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class CorporateToCorporateMessageValidator {
	private static final Logger logger = LoggerFactory.getLogger(CorporateToCorporateMessageValidator.class);

	private CorporateToCorporateMessageValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ....");
		if (context == null)
			throw new NullArgumentException("context");

		CorporateToCorporateMessage request = (CorporateToCorporateMessage) context.getRequest();
		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			result = validateCorporate(context, request);
		else
			return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
		if (result.isValid())
			return validateSettings(context, request);


		return result;
	}

	private static ValidationResult validateSettings(MessageProcessingContext context, CorporateToCorporateMessage request) {

		ValidationResult result = TransactionConfigValidator.validate(context.getTransactionConfig(),
				request.getAmount());
		if (!result.isValid())
			return result;

		result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context);
		if (!result.isValid())
			return result;

		return PSPValidator.validate(context, false);
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context, CorporateToCorporateMessage request) {
		ValidationResult result = validateCorporateInfo(context);
		if (!result.isValid())
			return result;

		result = PinCodeValidator.validate(context, context.getSender().getService(), request.getPin());
		if (!result.isValid())
			return result;

		if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getService() != null)
			result = validateReceiverService(context);
		if (!result.isValid())
			return result;

		return result;
	}

	private static ValidationResult validateCorporateInfo(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		return result;
	}

	private static ValidationResult validateReceiverService(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getServiceAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return WalletCapValidator.validate(context);
	}

}
