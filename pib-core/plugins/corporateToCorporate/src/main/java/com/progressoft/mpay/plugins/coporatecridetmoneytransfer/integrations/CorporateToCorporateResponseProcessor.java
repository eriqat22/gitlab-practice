package com.progressoft.mpay.plugins.coporatecridetmoneytransfer.integrations;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.progressoft.mpay.plugins.coporatecridetmoneytransfer.message.CorporateToCorporateMessage;
import com.progressoft.mpay.plugins.coporatecridetmoneytransfer.notification.NotificationProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;

public class CorporateToCorporateResponseProcessor implements IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(CorporateToCorporateResponseProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		try {
			logger.debug("Inside ProcessIntegration ...");
			preProcessIntegration(context);
			if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return null;
			if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return accept(context);
			else
				return reject(context);
		} catch (Exception ex) {
			logger.error("Error in ProcessIntegration", ex);
			return reject(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside PreProcessIntegration ...");
		if (context.getMessage() != null)
			context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		CorporateToCorporateMessage request = null;
		try {
			request = CorporateToCorporateMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
		} catch (MessageParsingException ex) {
			logger.error("Error while parsing message", ex);
			throw new WorkflowException(ex);
		}
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setAmount(request.getAmount());
		sender.setService(context.getTransaction().getSenderService());
		sender.setCorporate(sender.getService().getRefCorporate());
		sender.setServiceAccount(context.getTransaction().getSenderServiceAccount());
		sender.setAccount(sender.getServiceAccount().getRefAccount());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
				sender.getAccount().getAccNumber()));
		sender.setInfo(sender.getService().getName());
		sender.setProfile(sender.getServiceAccount().getRefProfile());
		sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

		context.setOperation(context.getMessage().getRefOperation());
		receiver.setService(context.getTransaction().getReceiverService());
		context.setDirection(context.getTransaction().getDirection().getId());
		context.getReceiver().setInfo(request.getReceiverInfo());
		if (receiver.getService() == null) {
			context.setTransactionConfig(
					context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(),
							sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
							context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT,
							context.getOperation().getId()));
			return;
		}
		receiver.setServiceAccount(context.getTransaction().getReceiverServiceAccount());
		receiver.setAccount(receiver.getServiceAccount().getRefAccount());
		receiver.setCorporate(receiver.getService().getRefCorporate());
		receiver.setBank(receiver.getServiceAccount().getBank());
		receiver.setBanked(receiver.getAccount().getIsBanked());
		receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
				receiver.getAccount().getAccNumber()));
		receiver.setProfile(receiver.getServiceAccount().getRefProfile());
		receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

		context.setDirection(context.getTransaction().getDirection().getId());
		long receiverCorporateTypeId = context.getReceiver().getCorporate().getClientType().getId();
		if (context.getReceiver().getCorporate().getId() == context.getLookupsLoader().getPSP().getId())
			receiverCorporateTypeId = context.getLookupsLoader().getMPClear().getClientType();
		context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(
				sender.getCorporate().getClientType().getId(), sender.isBanked(), receiverCorporateTypeId,
				receiver.isBanked(), context.getOperation().getMessageType().getId(),
				TransactionTypeCodes.DIRECT_CREDIT, context.getOperation().getId()));
	}

	private IntegrationProcessingResult accept(IntegrationProcessingContext context) throws SQLException {
		logger.debug("Inside Accept ...");
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED,
				ReasonCodes.VALID, null, null, false);
		result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		return result;
	}

	private IntegrationProcessingResult reject(IntegrationProcessingContext context) throws SQLException {
		logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		String reasonDescription = null;
		if (context.getMpClearIsoMessage().getField(47) != null)
			reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED,
				ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
				context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
		context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
				context.getTransactionConfig().getMessageType().getId(),
				BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1,
				SystemHelper.getCurrentDateWithoutTime());
		result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
		return result;
	}

	private IntegrationProcessingResult reject(IntegrationProcessingContext context, String processingCode,
			String reasonCode, String reasonDescription) {
		logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, processingCode, reasonCode,
				reasonDescription, null, false);
		try {
			context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
					context.getTransactionConfig().getMessageType().getId(),
					BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1,
					SystemHelper.getCurrentDateWithoutTime());
		} catch (SQLException e) {
			logger.error("Failed when trying to reverse limits", e);
		}
		result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
		return result;
	}
}
