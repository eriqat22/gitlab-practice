package com.progressoft.mpay.plugins.customerlanguage;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class CustomerLanguageMessage extends MPayRequest {
	private static final Logger logger = LoggerFactory.getLogger(CustomerLanguageMessage.class);
	private static final String NEW_LANGUAGE_KEY = "newLang";

	private long newLanguage;

	public CustomerLanguageMessage() {
		//
	}

	public CustomerLanguageMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public long getNewLanguage() {
		return newLanguage;
	}

	public void setNewLanguage(long newLanguage) {
		this.newLanguage = newLanguage;
	}

	public static CustomerLanguageMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		CustomerLanguageMessage message = new CustomerLanguageMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		try {
			message.setNewLanguage(Long.parseLong(message.getValue(NEW_LANGUAGE_KEY)));
		} catch (Exception ex) {
			logger.debug("Invalid fromDate Format", ex);
			throw new MessageParsingException(NEW_LANGUAGE_KEY);
		}
		return message;
	}
}
