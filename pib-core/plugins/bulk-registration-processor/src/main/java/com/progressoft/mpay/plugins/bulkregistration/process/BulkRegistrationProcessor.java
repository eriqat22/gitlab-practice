package com.progressoft.mpay.plugins.bulkregistration.process;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.shared.exception.WorkflowException;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.bulkregistration.process.IBulkRegestrationProcessor;
import com.progressoft.mpay.common.CoreComponents;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.mpclear.IMPClearClient;
import com.progressoft.mpay.mpclear.MPClearClientHelper;
import com.progressoft.mpay.plugins.bulkregistration.customerregstration.CustomerBulkRegistration;
import com.progressoft.mpay.plugins.bulkregistration.model.view.BulkRegistrationFileModelView;
import com.progressoft.mpay.plugins.bulkregistration.process.exception.InvalidBulkRegistrationFileException;
import com.progressoft.mpay.plugins.bulkregistration.usecase.*;
import com.progressoft.workflow.osworkflow.jpa.beans.JPAWorkflowEntry;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class BulkRegistrationProcessor implements IBulkRegestrationProcessor {

	private static Logger logger = LoggerFactory.getLogger(BulkRegistrationProcessor.class);

	private static final String MOBILE_NUMBER_QUERY = "FROM MPAY_CustomerMobile WHERE mobileNumber = :mobileNumber  AND statusId.code != '100106'";
	private static final String ALIAS_QUERY = "FROM MPAY_CustomerMobile WHERE alias = :alias AND statusId.code != '100106'";

	private static final String NUMBER_OF_ACCOUNT = "FROM MPAY_CustomerMobile where refCustomer.id = :customerId";

	private static final String ID_NUMBER_QUERY = "FROM MPAY_Customer WHERE idNum = :idNum AND  idType.code = :typeCode And nationality.countryStringIsoCode = :countryCode And statusId.code != '100006'";
	private static final String FILE_REGISTRATION_CONTAINER_CSV = "fileRegistrationContainer.csv";
	private static final String PARTIALY_SUCCESS_STATUS = "PARTIALY_SUCCESS";
	private static final String INVALID_RECORDS_CSV = "invalidRecords.csv";
	private static final String MOBILE_NUMBER_PARAM = "mobileNumber";
	private static final String WF_CUSTOMER = "WF_Customer";
	private static final String SUCCESS_STATUS = "SUCCESS";
	private static final String CONTENT_TYPE = "text/csv";
	private static final String FAILED_STATUS = "FAILED";
	private static final String ID_NUM_PARAM = "idNum";
	private static final String ALIAS_PARAM = "alias";
	private static final String NEWLINE = "\n";

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private JPAWorkFlowEntryUseCase jPAWorkFlowEntryUsecase;

	@Autowired
	private InitMpayCustomerValues initMpayCustomerValues;

	@Autowired
	private JPACurrentStepUseCase jPACurrentStepUseCase;

	@Autowired
	private InitMpayCustomerMobileValues initMpayCustomerMobileValues;

	@Autowired
	private InitMpayMobileAccountValues initMpayMobileAccountValues;

	@Override
	public void process(MPAY_BulkRegistration mpayBulkRegistration) {
		logger.debug("Process............");
		try {
			if (mpayBulkRegistration == null)
				throw new InvalidBulkRegistrationFileException("Bulk registration cannot be null");

			CustomerBulkRegistration customerBulkRegistration = new CustomerBulkRegistration.Builder(
					MPayContext.getInstance().getLookupsLoader()).build();
			BulkRegistrationFileModelView bulkRegistrationModel = customerBulkRegistration.processFile(IOUtils.toString(
					createTempFile(validateBulkRegistrationAttAndReturned(mpayBulkRegistration)).toURI(), "UTF-8"));
			foreachValidCustomer(mpayBulkRegistration, bulkRegistrationModel);
			showInvalidRecordFile(mpayBulkRegistration, bulkRegistrationModel.getInValidCustomerRecord());
			reflectStatusToBulkFile(bulkRegistrationModel, mpayBulkRegistration);
		} catch (Exception e) {
			throw new InvalidBulkRegistrationFileException("Error While Process bulk", e);
		}
	}

	private void foreachValidCustomer(MPAY_BulkRegistration mpayRegistration,
			BulkRegistrationFileModelView registrationFileModel) {
		logger.debug("Inside foreachValidCustomer.................");

		IMPClearClient client = MPClearClientHelper
				.getClient(MPayContext.getInstance().getSystemParameters().getMPClearProcessor());
		Iterator<Entry<Integer, MPAY_Customer>> iterator = registrationFileModel.getValidCustomerModel().entrySet()
				.iterator();
		CoreComponents coreComponents = new CoreComponents(MPayContext.getInstance().getDataProvider(),
				MPayContext.getInstance().getSystemParameters(), MPayContext.getInstance().getLookupsLoader());
		iterateForEachRecord(mpayRegistration, registrationFileModel.getInValidCustomerRecord(), client, iterator,
				coreComponents);

	}

	private void iterateForEachRecord(MPAY_BulkRegistration mpayRegistration, Map<Integer, String> invalidRecord,
			IMPClearClient client, Iterator<Entry<Integer, MPAY_Customer>> iterator, CoreComponents coreComponents) {
		int counter = 0;
		while (iterator.hasNext()) {
			Entry<Integer, MPAY_Customer> entry = iterator.next();
			try {
				MPAY_Customer customer = customerCreation(mpayRegistration, entry.getValue());

				Query numberOfAccount = createNumberOfAccountQuery(NUMBER_OF_ACCOUNT);
				numberOfAccount.setParameter("customerId", customer.getId());
				List<?> numberAccounts = numberOfAccount.getResultList();
				if (numberAccounts != null && customer.getClientType().getMaxNumberOfAccount() < numberAccounts.size())
					throw new IllegalArgumentException("Maximum Accounts exceded ");

				MPAY_CustomerMobile customerMobile = customerMobileCreation(customer);
				MPAY_MobileAccount mobileAccount = mobileAccountCreation(customer, customerMobile);
				setMobileMobileAccountsToCustomerMobile(customerMobile, mobileAccount);
				if ("old".equals(customer.getWsToken()))
					client.addMobileAccount(coreComponents, customer, customerMobile, mobileAccount);
				else
					client.addCustomer(coreComponents, mobileAccount.getMobile().getRefCustomer(), customerMobile);
				logger.info("Finish Record : " + counter++ + "-----------------------");

			} catch (Exception e) {
				logger.debug("Error While foreachValidCustomer", e);
				invalidRecord.put(entry.getKey(), e.getMessage());
				iterator.remove();
			}
		}

	}

	private MPAY_MobileAccount mobileAccountCreation(MPAY_Customer customer, MPAY_CustomerMobile customerMobile) {
		JPAWorkflowEntry accountWorkflowEntry = jPAWorkFlowEntryUsecase.save("WF_MobileAccount");
		MPAY_MobileAccount mobileAccount = initMpayMobileAccountValues.initMpayMobileAccountValue(customer,
				customerMobile, accountWorkflowEntry,
				MPayContext.getInstance().getDataProvider().getWorkflowStatus("101007"));
		jPACurrentStepUseCase.save(accountWorkflowEntry, 101007);
		entityManager.persist(mobileAccount);
		return mobileAccount;
	}

	private MPAY_CustomerMobile customerMobileCreation(MPAY_Customer customer) {
		JPAWorkflowEntry mobileWorkflowEntry = jPAWorkFlowEntryUsecase.save("WF_CustomerMobile");
		MPAY_CustomerMobile customerMobile = initMpayCustomerMobileValues.initMpayCustomerMobileValue(customer,
				mobileWorkflowEntry, MPayContext.getInstance().getDataProvider().getWorkflowStatus("100107"));
		jPACurrentStepUseCase.save(mobileWorkflowEntry, 100107);
		entityManager.persist(customerMobile);
		return customerMobile;
	}

	private MPAY_Customer customerCreation(MPAY_BulkRegistration mpayRegistration, MPAY_Customer mpayCustomer) {
		String result = validateValueFromDB(mpayCustomer);
		if (result.equals("00")) {
			JPAWorkflowEntry customerWorkflowEntry = jPAWorkFlowEntryUsecase.save(WF_CUSTOMER);
			MPAY_Customer customer = initMpayCustomerValues.initMpayCustomerValue(mpayCustomer, mpayRegistration,
					AppContext.getCurrentPrefOrg().getId(), customerWorkflowEntry.getId(),
					MPayContext.getInstance().getDataProvider().getWorkflowStatus("100007"));
			jPACurrentStepUseCase.save(customerWorkflowEntry, 100007);
			entityManager.persist(customer);
			return customer;
		}
		String mobileNumber = mpayCustomer.getMobileNumber();
		MPAY_Customer customer = MPayContext.getInstance().getDataProvider()
				.getCustomer(mpayCustomer.getIdType().getCode(), mpayCustomer.getIdNum());
		customer.setMobileNumber(mobileNumber);
		customer.setAlias(mpayCustomer.getAlias());
		customer.setWsToken("old");
		return customer;
	}

	private void setMobileMobileAccountsToCustomerMobile(MPAY_CustomerMobile customerMobile,
			MPAY_MobileAccount mobileAccount) {
		List<MPAY_MobileAccount> accounts = new ArrayList<>();
		accounts.add(mobileAccount);
		customerMobile.setMobileMobileAccounts(accounts);
		entityManager.merge(customerMobile);
	}

	private String validateValueFromDB(MPAY_Customer customer) {
		Query aliasQuery = createAliasQuery(ALIAS_QUERY);
		aliasQuery.setParameter(ALIAS_PARAM, customer.getAlias());
		List<?> aliasResultList = aliasQuery.getResultList();
		if (aliasResultList != null && !aliasResultList.isEmpty())
			throw new IllegalArgumentException("Alias is Already used ");

		Query mobileNumber = createMobileNumberQuery(MOBILE_NUMBER_QUERY);
		mobileNumber.setParameter(MOBILE_NUMBER_PARAM, customer.getMobileNumber());
		List<?> mobileNumberResultList = mobileNumber.getResultList();
		if (mobileNumberResultList != null && !mobileNumberResultList.isEmpty())
			throw new IllegalArgumentException("MobileNumber is Already used ");

		Query idNumber = createIdNumberQuery(ID_NUMBER_QUERY);
		idNumber.setParameter(ID_NUM_PARAM, customer.getIdNum());
		idNumber.setParameter("typeCode", customer.getIdType().getCode());
		idNumber.setParameter("countryCode", customer.getNationality().getCountryStringIsoCode());
		List<?> idNumberResultList = idNumber.getResultList();
		if (idNumberResultList != null && !idNumberResultList.isEmpty())
			return "03";
		return "00";
	}

	private Query createAliasQuery(String sql) {
		return entityManager.createQuery(sql);
	}

	private Query createMobileNumberQuery(String sql) {
		return entityManager.createQuery(sql);
	}

	private Query createIdNumberQuery(String sql) {
		return entityManager.createQuery(sql);
	}

	private Query createNumberOfAccountQuery(String sql) {
		return entityManager.createQuery(sql);
	}

	private MPAY_BulkRegistrationAtt validateBulkRegistrationAttAndReturned(MPAY_BulkRegistration bulkRegistration) {
		MPAY_BulkRegistrationAtt attahcment = MPayContext.getInstance().getDataProvider()
				.getBulkRegistrationAttachment(String.valueOf(bulkRegistration.getId()));
		if (attahcment == null || attahcment.getAttFile() == null || attahcment.getAttFile().length == 0)
			throw new InvalidBulkRegistrationFileException("Invalid Attachment");
		return attahcment;

	}

	private File createTempFile(MPAY_BulkRegistrationAtt attahcment) throws IOException {
		File tempFile = new File(FILE_REGISTRATION_CONTAINER_CSV);
		FileUtils.writeByteArrayToFile(tempFile, attahcment.getAttFile());
		return tempFile;
	}

	@SuppressWarnings("deprecation")
	private void showInvalidRecordFile(MPAY_BulkRegistration bulkRegistration, Map<Integer, String> invalidRecord)
			throws IOException {
		StringBuilder builder = new StringBuilder();
		if (invalidRecord.size() > 0) {
			File inValidFile = new File(INVALID_RECORDS_CSV);
			invalidRecord.forEach((k, v) -> builder.append(k + " - " + v + NEWLINE));
			FileUtils.writeStringToFile(inValidFile, builder.toString());
			createInvalidBulkRegistrationAtt(bulkRegistration, FileUtils.readFileToByteArray(inValidFile));
		}
	}

	private void createInvalidBulkRegistrationAtt(MPAY_BulkRegistration bulkRegistration, byte[] readFileToByteArray) {
		MPAY_BulkRegistrationAtt registrationAtt = new MPAY_BulkRegistrationAtt();
		registrationAtt.setAttFile(readFileToByteArray);
		registrationAtt.setRecordId(String.valueOf(bulkRegistration.getId()));
		registrationAtt.setEntityId(MPAY_BulkRegistration.class.getName());
		registrationAtt.setName(INVALID_RECORDS_CSV);
		registrationAtt.setContentType(CONTENT_TYPE);
		registrationAtt.setSize(new BigDecimal(readFileToByteArray.length));
		registrationAtt.setAttachmentToken("No");
		MPayContext.getInstance().getDataProvider().persistCustomerAtt(registrationAtt);
	}

	private void reflectStatusToBulkFile(BulkRegistrationFileModelView customerBulkRegistration,
			MPAY_BulkRegistration bulkRegistration) throws WorkflowException {
		logger.debug("Inside reflectStatusToBulkFile........");
		int invalidRecordSize = customerBulkRegistration.getInValidCustomerRecord().size();
		int validCustomerSize = customerBulkRegistration.getValidCustomerModel().size();
		if (invalidRecordSize > 0 && validCustomerSize == 0)
			bulkRegistration.setStatusId(setWorkflowStatus(FAILED_STATUS));
		else if (invalidRecordSize == 0 && validCustomerSize > 0)
			bulkRegistration.setStatusId(setWorkflowStatus(SUCCESS_STATUS));
		else
			bulkRegistration.setStatusId(setWorkflowStatus(PARTIALY_SUCCESS_STATUS));
	}

	private WorkflowStatus setWorkflowStatus(String workflowStatus) {
		return MPayContext.getInstance().getDataProvider().getWorkflowStatus(workflowStatus);
	}

}