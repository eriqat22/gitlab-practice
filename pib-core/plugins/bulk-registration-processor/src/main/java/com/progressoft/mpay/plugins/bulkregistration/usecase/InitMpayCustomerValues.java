package com.progressoft.mpay.plugins.bulkregistration.usecase;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.mpay.entities.MPAY_BulkRegistration;
import com.progressoft.mpay.entities.MPAY_Customer;

@FunctionalInterface
public interface InitMpayCustomerValues {
	 MPAY_Customer initMpayCustomerValue(MPAY_Customer mpayCustomer, MPAY_BulkRegistration bulkRegistration, Long orgId, Long wfId, WorkflowStatus status);
}
