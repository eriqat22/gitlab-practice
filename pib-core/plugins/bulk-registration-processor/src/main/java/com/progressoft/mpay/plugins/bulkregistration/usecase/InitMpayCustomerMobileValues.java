package com.progressoft.mpay.plugins.bulkregistration.usecase;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.workflow.osworkflow.jpa.beans.JPAWorkflowEntry;

@FunctionalInterface
public interface InitMpayCustomerMobileValues {

    MPAY_CustomerMobile initMpayCustomerMobileValue(MPAY_Customer customer, JPAWorkflowEntry entry, WorkflowStatus status);
}
