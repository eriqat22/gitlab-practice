package com.progressoft.mpay.plugins.bulkregistration.usecase.impl;

import com.progressoft.mpay.plugins.bulkregistration.usecase.JPAWorkFlowEntryUseCase;
import com.progressoft.workflow.osworkflow.jpa.beans.JPAWorkflowEntry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class JPAWorkFlowEntryUsecaseImpl implements JPAWorkFlowEntryUseCase {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public JPAWorkflowEntry save(String workFlowName) {
		JPAWorkflowEntry entry=new JPAWorkflowEntry();
		entry.setWorkflowName(workFlowName);
		entry.setWorkflowState(1);
		entry.setWorkflowVersion(1);
		entityManager.persist(entry);
		return entry;
	}

}
