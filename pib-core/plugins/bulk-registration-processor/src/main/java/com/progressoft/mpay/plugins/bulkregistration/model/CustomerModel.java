package com.progressoft.mpay.plugins.bulkregistration.model;


import com.progressoft.mpay.plugins.bulkregistration.model.view.CustomerModelView;

import java.util.Date;

public class CustomerModel implements CustomerModelView {

	private String firstName;
	private String middleName;
	private String lastName;
	private String idNum;
	private Date dob;
	private String clientRef;
	private String phoneOne;
	private String phoneTwo;
	private String email;
	private String pobox;
	private String zipCode;
	private String buildingNum;
	private String streetName;
	private String note;
	private String mobileNumber;
	private String alias;
	private String externalAcc;
	private String notificationShowType;
	private String nfcSerial;
	private String idType;
	private String city;
	private String prefLang;
	private String bank;
	private String refProfile;
	private String nationality;
	private String isBeneficiary;
	private String beneficiaryName;
	private String beneficiaryId;
	private String beneficiaryIdType;
	private String beneficiaryEmail;
	private String guardian;
	private String guardianEmail;

	public CustomerModel(BuilderModel builder) {
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.alias = builder.alias;
		this.bank = builder.bank;
		this.buildingNum = builder.buildingNum;
		this.city = builder.city;
		this.clientRef = builder.clientRef;
		this.dob = builder.dob;
		this.email = builder.email;
		this.externalAcc = builder.externalAcc;
		this.idNum = builder.idNum;
		this.middleName = builder.middleName;
		this.zipCode = builder.zipCode;
		this.streetName = builder.streetName;
		this.nationality = builder.nationality;
		this.refProfile = builder.refProfile;
		this.prefLang = builder.prefLang;
		this.idType = builder.idType;
		this.nfcSerial = builder.nfcSerial;
		this.notificationShowType = builder.notificationShowType;
		this.mobileNumber = builder.mobileNumber;
		this.note = builder.note;
		this.phoneOne = builder.phoneOne;
		this.phoneTwo = builder.phoneTwo;
		this.pobox = builder.pobox;
		this.isBeneficiary = builder.isBeneficiary;
		this.beneficiaryEmail = builder.beneficiaryEmail;
		this.beneficiaryId = builder.beneficiaryId;
		this.beneficiaryIdType = builder.beneficiaryIdType;
		this.beneficiaryName = builder.beneficiaryName;
		this.guardian = builder.guardian;
		this.guardianEmail = builder.guardianEmail;
	}

	@Override
	public String getFullName() {
		return firstName + " " + middleName + " " + lastName;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getMiddleName() {
		return middleName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public String getIdNum() {
		return idNum;
	}

	@Override
	public Date getDob() {
		return dob;
	}

	@Override
	public String getClientRef() {
		return clientRef;
	}

	@Override
	public String getPhoneOne() {
		return phoneOne;
	}

	@Override
	public String getPhoneTwo() {
		return phoneTwo;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public String getPobox() {
		return pobox;
	}

	@Override
	public String getZipCode() {
		return zipCode;
	}

	@Override
	public String getBuildingNum() {
		return buildingNum;
	}

	@Override
	public String getStreetName() {
		return streetName;
	}

	@Override
	public String getNote() {
		return note;
	}

	@Override
	public String getMobileNumber() {
		return mobileNumber;
	}

	@Override
	public String getAlias() {
		return alias;
	}

	@Override
	public String getExternalAcc() {
		return externalAcc;
	}

	@Override
	public String getNotificationShowType() {
		return notificationShowType;
	}

	@Override
	public String getNfcSerial() {
		return nfcSerial;
	}

	@Override
	public String getIdType() {
		return idType;
	}

	@Override
	public String getCity() {
		return city;
	}

	@Override
	public String getPrefLang() {
		return prefLang;
	}

	@Override
	public String getBank() {
		return bank;
	}

	@Override
	public String getRefProfile() {
		return refProfile;
	}

	@Override
	public String getNationality() {
		return nationality;
	}

	@Override
	public String getIsBeneficiary() {
		return isBeneficiary;
	}

	@Override
	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	@Override
	public String getBeneficiaryId() {
		return beneficiaryId;
	}

	@Override
	public String getBeneficiaryIdType() {
		return beneficiaryIdType;
	}

	@Override
	public String getBeneficiaryEmail() {
		return beneficiaryEmail;
	}

	@Override
	public String getGuardian() {
		return guardian;
	}

	@Override
	public String getGuardianEmail() {
		return guardianEmail;
	}

	public static class BuilderModel {
		private String firstName;
		private String middleName;
		private String lastName;
		private String idNum;
		private Date dob;
		private String clientRef;
		private String phoneOne;
		private String phoneTwo;
		private String email;
		private String pobox;
		private String zipCode;
		private String buildingNum;
		private String streetName;
		private String note;
		private String mobileNumber;
		private String alias;
		private String externalAcc;
		private String notificationShowType;
		private String nfcSerial;
		private String idType;
		private String city;
		private String prefLang;
		private String bank;
		private String refProfile;
		private String nationality;
		private String isBeneficiary;
		private String beneficiaryName;
		private String beneficiaryId;
		private String beneficiaryIdType;
		private String beneficiaryEmail;
		private String guardian;
		private String guardianEmail;

		public BuilderModel isBeneficiary(String isBeneficiary) {
			this.isBeneficiary = isBeneficiary;
			return this;
		}

		public BuilderModel beneficiaryName(String beneficiaryName) {
			this.beneficiaryName = beneficiaryName;
			return this;
		}

		public BuilderModel beneficiaryId(String beneficiaryId) {
			this.beneficiaryId = beneficiaryId;
			return this;
		}

		public BuilderModel beneficiaryIdType(String beneficiaryIdType) {
			this.beneficiaryIdType = beneficiaryIdType;
			return this;
		}

		public BuilderModel beneficiaryEmail(String beneficiaryEmail) {
			this.beneficiaryEmail = beneficiaryEmail;
			return this;
		}

		public BuilderModel guardian(String guardian) {
			this.guardian = guardian;
			return this;
		}

		public BuilderModel guardianEmail(String guardianEmail) {
			this.guardianEmail = guardianEmail;
			return this;
		}

		public BuilderModel firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public BuilderModel middleName(String middleName) {
			this.middleName = middleName;
			return this;
		}

		public BuilderModel lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public BuilderModel idNumber(String idNumber) {
			this.idNum = idNumber;
			return this;
		}

		public BuilderModel dob(Date dob) {
			this.dob = dob;
			return this;
		}

		public BuilderModel clientRef(String clientRef) {
			this.clientRef = clientRef;
			return this;
		}

		public BuilderModel phoneOne(String phoneOne) {
			this.phoneOne = phoneOne;
			return this;
		}

		public BuilderModel phoneTwo(String phoneTwo) {
			this.phoneTwo = phoneTwo;
			return this;
		}

		public BuilderModel email(String email) {
			this.email = email;
			return this;
		}

		public BuilderModel pobox(String pobox) {
			this.pobox = pobox;
			return this;
		}

		public BuilderModel zipCode(String zipCode) {
			this.zipCode = zipCode;
			return this;
		}

		public BuilderModel buildingNum(String buildingNum) {
			this.buildingNum = buildingNum;
			return this;
		}

		public BuilderModel streetName(String streetName) {
			this.streetName = streetName;
			return this;
		}

		public BuilderModel note(String note) {
			this.note = note;
			return this;
		}

		public BuilderModel mobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
			return this;
		}

		public BuilderModel alias(String alias) {
			this.alias = alias;
			return this;
		}

		public BuilderModel externalAcc(String externalAcc) {
			this.externalAcc = externalAcc;
			return this;
		}

		public BuilderModel notificationShowType(String notificationShowType) {
			this.notificationShowType = notificationShowType;
			return this;
		}

		public BuilderModel nfcSerial(String nfcSerial) {
			this.nfcSerial = nfcSerial;
			return this;
		}

		public BuilderModel idType(String idType) {
			this.idType = idType;
			return this;
		}

		public BuilderModel city(String city) {
			this.city = city;
			return this;
		}

		public BuilderModel prefLang(String prefLang) {
			this.prefLang = prefLang;
			return this;
		}

		public BuilderModel bank(String bank) {
			this.bank = bank;
			return this;
		}

		public BuilderModel refProfile(String refProfile) {
			this.refProfile = refProfile;
			return this;
		}

		public BuilderModel nationality(String nationality) {
			this.nationality = nationality;
			return this;
		}

		public CustomerModel build() {
			return new CustomerModel(this);
		}
	}
}