package com.progressoft.mpay.plugins.bulkregistration.converter;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.plugins.bulkregistration.model.CustomerModel;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MPAYCustomerConverter {
    private static MPAYCustomerConverter customerConverter;
    private ILookupsLoader lookupsLoader;
    private String personClientType;

    public MPAYCustomerConverter(ILookupsLoader lookupsLoader) {
        this.lookupsLoader = lookupsLoader;
        this.personClientType = lookupsLoader.getSystemConfigurations(SysConfigKeys.PERSON_CLIENT_TYPE)
                .getConfigValue();
    }

    public static MPAYCustomerConverter getMpayCustomerConvertor(ILookupsLoader lookupsLoader) {
        if (customerConverter == null)
            customerConverter = new MPAYCustomerConverter(lookupsLoader);
        return customerConverter;
    }

    public MPAY_Customer toMpayCustomer(CustomerModel customerModel) {
        if (Objects.isNull(customerModel))
            throw new InvalidCustomerModelException("Customer model should be not null");
        validateLookupsData(customerModel);
        return generateMpayCustomer(customerModel);
    }

    private MPAY_Customer generateMpayCustomer(CustomerModel customerModel) {
        MPAY_Customer mpayCustomer = new MPAY_Customer();
        mpayCustomer.setFullName(customerModel.getFullName());
        mpayCustomer.setFirstName(customerModel.getFirstName());
        mpayCustomer.setMiddleName(customerModel.getMiddleName());
        mpayCustomer.setLastName(customerModel.getLastName());
        mpayCustomer.setIdNum(customerModel.getIdNum());
        mpayCustomer.setDob(customerModel.getDob());
        mpayCustomer.setClientRef(customerModel.getClientRef());
        mpayCustomer.setPhoneOne(validatePhoneOne(customerModel.getPhoneOne()));
        mpayCustomer.setPhoneTwo(validatePhoneTwo(customerModel.getPhoneTwo()));
        mpayCustomer.setEmail(validateEmail(customerModel.getEmail()));
        mpayCustomer.setPobox(validatePobox(customerModel.getPobox()));
        mpayCustomer.setZipCode(validateZipCode(customerModel.getZipCode()));
        mpayCustomer.setBuildingNum(customerModel.getBuildingNum());
        mpayCustomer.setStreetName(customerModel.getStreetName());
        mpayCustomer.setNote(validateNote(customerModel.getNote()));
        mpayCustomer.setMobileNumber(validateMobileNumber(customerModel.getMobileNumber()));
        mpayCustomer.setAlias(customerModel.getAlias());
        mpayCustomer.setExternalAcc(customerModel.getExternalAcc());
        mpayCustomer.setNfcSerial(customerModel.getNfcSerial());
        mpayCustomer.setNotificationShowType(customerModel.getNotificationShowType());
        mpayCustomer.setIdType(getIdTypeByCode(customerModel.getIdType()));
        mpayCustomer.setClientType(getPersonClientByCode());
        mpayCustomer.setCity(getCityByCode(customerModel.getCity()));
        mpayCustomer.setPrefLang(getLanguageByCode(customerModel.getPrefLang()));
        mpayCustomer.setBank(getBankByCode(customerModel.getBank()));
        mpayCustomer.setRefProfile(getProfileByCode(customerModel.getRefProfile()));
        mpayCustomer.setNationality(getCountryByCode(customerModel.getNationality()));
        mpayCustomer.setBankedUnbanked(MpayCustomerConverterConstants.UNBANKED_CUSTOMER.getValue());
        mpayCustomer.setMas(0L);
        mpayCustomer.setIsActive(false);
        mpayCustomer.setMaxNumberOfDevices(0L);
        mpayCustomer.setIsRegistered(false);
        return mpayCustomer;

    }

    private String validateMobileNumber(String mobileNumber) {
        isMatchRegex(MpayCustomerConverterRegex.MOBILE_NUMBER_REGEX.getRegex(), mobileNumber, "Invalid mobile number");
        return mobileNumber;
    }

    private String validatePhoneOne(String phoneOne) {
        isMatchRegex(MpayCustomerConverterRegex.PHONE_REGEX.getRegex(), phoneOne, "Invalid Phone One type");
        return phoneOne;
    }

    private String validatePhoneTwo(String phoneTwo) {
        isMatchRegex(MpayCustomerConverterRegex.PHONE_REGEX.getRegex(), phoneTwo, "Invalid Phone Two type");

        return phoneTwo;
    }

    private String validateEmail(String email) {
        isMatchRegex(MpayCustomerConverterRegex.EMAIL_REGEX.getRegex(), email, "Invalid Email type");
        return email;
    }

    private String validateNote(String note) {
        isMatchRegex(MpayCustomerConverterRegex.NOTE_REGEX.getRegex(), note, "Invalid Note type");
        return note;
    }

    private String validatePobox(String pobox) {
        isMatchRegex(MpayCustomerConverterRegex.POBOX_REGEX.getRegex(), pobox, "Invalid pobox type");
        return pobox;
    }

    private String validateZipCode(String zipCode) {
        isMatchRegex(MpayCustomerConverterRegex.ZIPCODE_REGEX.getRegex(), zipCode, "Invalid zipCode type");
        return zipCode;
    }

    private void isMatchRegex(String regex, String value, String errMessage) {
        if (!value.trim().isEmpty()) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(value);
            if (!matcher.matches())
                throw new InvalidCustomerModelException(errMessage);
        }
    }

    private MPAY_ClientType getPersonClientByCode() {
        return getClientTypeByCode(this.personClientType);
    }


    private void validateLookupsData(CustomerModel customerModel) {
        if (invalidNotificationShowType(customerModel.getNotificationShowType()))
            throw new InvalidCustomerModelException("Invalid Notification show type ");
        if (invalidIdType(customerModel.getIdType()))
            throw new InvalidCustomerModelException("Invalid Id Type");
        if (invalidCity(customerModel.getCity()))
            throw new InvalidCustomerModelException("Invalid City  Code");
        if (invalidPrefLang(customerModel.getPrefLang()))
            throw new InvalidCustomerModelException("Invalid Language Code");
        if (invalidBankCode(customerModel.getBank()))
            throw new InvalidCustomerModelException("Invalid Bank Code");
        if (invalidProfileCode(customerModel.getRefProfile()))
            throw new InvalidCustomerModelException("Invalid Profile Code");
        if (invalidCountry(customerModel.getNationality()))
            throw new InvalidCustomerModelException("Invalid Nationality Code");
        if (customerModel.getBeneficiaryIdType() != null && invalidIdType(customerModel.getBeneficiaryIdType()))
            throw new InvalidCustomerModelException("Invalid Beneficiary Id Type");
    }

    private boolean invalidCountry(String countryCode) {
        return getCountryByCode(countryCode) == null;
    }

    private JfwCountry getCountryByCode(String countryCode) {
        return lookupsLoader.getCountry(countryCode);
    }

    private boolean invalidProfileCode(String profileCode) {
        return getProfileByCode(profileCode) == null;
    }

    private MPAY_Profile getProfileByCode(String profileCode) {
        return lookupsLoader.getProfile(profileCode);
    }

    private boolean invalidBankCode(String bankCode) {
        return getBankByCode(bankCode) == null;
    }

    private MPAY_Bank getBankByCode(String bankCode) {
        return lookupsLoader.getBankByCode(bankCode);
    }

    private boolean invalidPrefLang(String prefLangCode) {
        return getLanguageByCode(prefLangCode) == null;
    }

    private MPAY_Language getLanguageByCode(String prefLangCode) {
        return lookupsLoader.getLanguageByCode(prefLangCode);
    }

    private boolean invalidCity(String cityCode) {
        return getCityByCode(cityCode) == null;
    }

    private MPAY_City getCityByCode(String cityCode) {
        return lookupsLoader.getCity(cityCode);
    }

    private MPAY_ClientType getClientTypeByCode(String clientType) {
        return lookupsLoader.getClientType(clientType);
    }

    private boolean invalidIdType(String idTypeCode) {
        return !(getIdTypeByCode(idTypeCode) != null
                || MpayCustomerConverterConstants.PASSPORT_ID_TYPE.getValue().equals(idTypeCode)
                || MpayCustomerConverterConstants.NATIONAL_ID_TYPE_CODE.getValue().equals(idTypeCode));
    }

    private MPAY_IDType getIdTypeByCode(String idTypeCode) {
        return lookupsLoader.getIDType(idTypeCode);
    }

    private boolean invalidNotificationShowType(String notificationShowType) {
        return !(NotificationShowTypeCodes.ALIAS.equals(notificationShowType)
                || NotificationShowTypeCodes.MOBILE_NUMBER.equals(notificationShowType));
    }

    private enum MpayCustomerConverterRegex {
        EMAIL_REGEX(
                "^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,4}))$"), MOBILE_NUMBER_REGEX(
                "^(?:009627)[7-9-8][0-9]{7}$"), PHONE_REGEX("^(0|[0-9][0-9]*)$"), NOTE_REGEX(
                "^[^<>%$&#\"\'~^=+><]*$"), ZIPCODE_REGEX("^[0-9]*$"), POBOX_REGEX("^[0-9]*$");
        private String regex;

        MpayCustomerConverterRegex(String regex) {
            this.regex = regex;
        }

        public String getRegex() {
            return regex;
        }
    }

    private enum MpayCustomerConverterConstants {
        PASSPORT_ID_TYPE("888"), NATIONAL_ID_TYPE_CODE("777"), IS_NOT_BENEFICIARY("2"), UNBANKED_CUSTOMER("1");
        private String value;

        MpayCustomerConverterConstants(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public class InvalidCustomerModelException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public InvalidCustomerModelException(String cause) {
            super(cause);
        }
    }
}