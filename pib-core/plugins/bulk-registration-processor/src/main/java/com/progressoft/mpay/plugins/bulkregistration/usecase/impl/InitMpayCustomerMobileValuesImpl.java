package com.progressoft.mpay.plugins.bulkregistration.usecase.impl;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.mpay.entities.MPAY_ChannelType;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.plugins.bulkregistration.usecase.InitMpayCustomerMobileValues;
import com.progressoft.workflow.osworkflow.jpa.beans.JPAWorkflowEntry;

public class InitMpayCustomerMobileValuesImpl implements InitMpayCustomerMobileValues {

	@Override
	public MPAY_CustomerMobile initMpayCustomerMobileValue(MPAY_Customer customer, JPAWorkflowEntry entry,
			WorkflowStatus status) {
		MPAY_CustomerMobile customerMobile = new MPAY_CustomerMobile();
		customerMobile.setRefCustomer(customer);
		customerMobile.setAlias(customer.getAlias());
		customerMobile.setApprovedData(customer.getApprovedData());
		customerMobile.setIsActive(true);
		customerMobile.setBank(customer.getBank());
		customerMobile.setBankedUnbanked(customer.getBankedUnbanked());
		customerMobile.setEmail(customer.getEmail());
		customerMobile.setExternalAcc(customer.getExternalAcc());
		customerMobile.setMobileNumber(customer.getMobileNumber());
		customerMobile.setNotificationShowType(customer.getNotificationShowType());
		customerMobile.setStatusId(status);
		customerMobile.setTenantId(customer.getTenantId());
		customerMobile.setWorkflowId(entry.getId());
		customerMobile.setRefProfile(customer.getRefProfile());
		customerMobile.setMas(1L);
		MPAY_ChannelType channelType = new MPAY_ChannelType();
		channelType.setId(1L);
		customerMobile.setIsBlocked(false);
		customerMobile.setChannelType(channelType);
		customerMobile.setMaxNumberOfDevices(0L);
		customerMobile.setRetryCount(0L);
		customerMobile.setIsRegistered(false);
		return customerMobile;
	}

}
