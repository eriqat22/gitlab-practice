package com.progressoft.mpay.plugins.bulkregistration.model;

import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.plugins.bulkregistration.converter.MPAYCustomerConverter;
import com.progressoft.mpay.plugins.bulkregistration.model.view.BulkRegistrationFileModelView;
import com.progressoft.mpay.plugins.bulkregistration.parser.CustomerModelParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class BulkRegistrationFileModel implements BulkRegistrationFileModelView {
	private static Logger logger = LoggerFactory.getLogger(BulkRegistrationFileModel.class);

	private TreeMap<Integer, String> inValidCustomerRecord = new TreeMap<>();
	private Map<Integer, MPAY_Customer> validCustomerModel = new HashMap<>();
	private ILookupsLoader lookupsLoader;

	private BulkRegistrationFileModel(Builder builder) {
		this.lookupsLoader = builder.lookupsLoader;
	}

	public void fetchFileContent(String fileContent) {
		if (fileContent == null)
			throw new IllegalArgumentException("The file content should be not null");
		logger.info("Inside fetch File content .......");
		CustomerModelParser parser = new CustomerModelParser();
		MPAYCustomerConverter mpayCustomerConvertor = MPAYCustomerConverter.getMpayCustomerConvertor(lookupsLoader);
		long start = System.nanoTime();

		readFileContent(fileContent, parser, mpayCustomerConvertor);
		long time = System.nanoTime() - start;
		logger.info("Parsing to record Total Time :( " , time / fileContent.length() / 1000.0 + ")");

	}

	private void readFileContent(String fileContent, CustomerModelParser parser, MPAYCustomerConverter mpayCustomerConvertor) {

		InputStream inputStream = new ByteArrayInputStream(fileContent.getBytes());
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		readRecord(parser, mpayCustomerConvertor, bufferedReader);
	}

	private void readRecord(CustomerModelParser parser, MPAYCustomerConverter mpayCustomerConvertor, BufferedReader bufferedReader) {
		int recordNumber = 0;
		int counter = 0;
		String line;
		try {
			while ((line = bufferedReader.readLine()) != null) {
				logger.info("Parsing to record :" , counter++);
				validateRecordAndPutInsideMap(parser, mpayCustomerConvertor, recordNumber, line);
				recordNumber++;
			}
			bufferedReader.close();
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private void validateRecordAndPutInsideMap(CustomerModelParser parser, MPAYCustomerConverter mpayCustomerConvertor, int recordNumber, String line) {
		try {
			validCustomerModel.put(recordNumber, mpayCustomerConvertor.toMpayCustomer(parser.genrateCustomerModel(line)));
		} catch (Exception e) {
			logger.debug("Error While processRecord", e);
			inValidCustomerRecord.put(recordNumber, e.getMessage());
		}
	}

	@Override
	public Map<Integer, MPAY_Customer> getValidCustomerModel() {
		return validCustomerModel;
	}

	@Override
	public Map<Integer, String> getInValidCustomerRecord() {
		return inValidCustomerRecord;
	}

	public static class Builder {
		private ILookupsLoader lookupsLoader;

		public Builder(ILookupsLoader lookupsLoader) {
			this.lookupsLoader = lookupsLoader;
		}

		public BulkRegistrationFileModel build() {

			return new BulkRegistrationFileModel(this);
		}
	}

}