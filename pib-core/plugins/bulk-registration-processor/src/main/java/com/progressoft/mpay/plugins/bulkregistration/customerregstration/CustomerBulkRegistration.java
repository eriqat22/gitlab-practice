package com.progressoft.mpay.plugins.bulkregistration.customerregstration;

import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.plugins.bulkregistration.model.BulkRegistrationFileModel;
import com.progressoft.mpay.plugins.bulkregistration.model.view.BulkRegistrationFileModelView;

public class CustomerBulkRegistration {

	private ILookupsLoader lookupsLoader;

	private CustomerBulkRegistration(Builder builder) {
		validateLookupsLoader(builder);
		this.lookupsLoader = builder.lookupsLoader;
	}

	public BulkRegistrationFileModelView processFile(String fileContent) {
		BulkRegistrationFileModel.Builder builder;
		BulkRegistrationFileModel bulkRegistrationFileModel;
		validateFileContent(fileContent);

		builder = new BulkRegistrationFileModel.Builder(lookupsLoader);
		bulkRegistrationFileModel = builder.build();
		bulkRegistrationFileModel.fetchFileContent(fileContent);
		return bulkRegistrationFileModel;

	}

	private void validateLookupsLoader(Builder builder) {
		if (builder.lookupsLoader == null)
			throw new IvalidLookupsLoaderException("The lookups loader should be not null");
	}

	private void validateFileContent(String fileContent) {
		if (fileContent == null || fileContent.trim().isEmpty())
			throw new IvalidFileException("Invalid file content");
	}

	public static class Builder {

		private ILookupsLoader lookupsLoader;

		public Builder(ILookupsLoader lookupsLoader) {
			this.lookupsLoader = lookupsLoader;
		}

		public CustomerBulkRegistration build() {
			return new CustomerBulkRegistration(this);
		}
	}

	public class IvalidLookupsLoaderException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public IvalidLookupsLoaderException(String message) {
			super(message);
		}
	}

	public class IvalidFileException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public IvalidFileException(String message) {
			super(message);
		}
	}
}
