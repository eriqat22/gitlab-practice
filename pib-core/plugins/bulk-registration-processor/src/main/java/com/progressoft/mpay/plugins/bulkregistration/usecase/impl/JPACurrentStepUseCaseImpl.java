package com.progressoft.mpay.plugins.bulkregistration.usecase.impl;

import com.progressoft.mpay.plugins.bulkregistration.usecase.JPACurrentStepUseCase;
import com.progressoft.workflow.osworkflow.jpa.beans.JPACurrentStep;
import com.progressoft.workflow.osworkflow.jpa.beans.JPAWorkflowEntry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class JPACurrentStepUseCaseImpl implements JPACurrentStepUseCase {
	private static final String STATUS = "1";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void save(JPAWorkflowEntry entry, int stepId) {
		JPACurrentStep currentStep = new JPACurrentStep();
		currentStep.setStatus(STATUS);
		currentStep.setWorkflowEntry(entry);
		currentStep.setStepId(stepId);
		entityManager.persist(currentStep);

	}

}
