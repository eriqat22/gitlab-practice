package com.progressoft.mpay.plugins.bulkregistration.parser;

import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.plugins.bulkregistration.model.CustomerModel;
import org.apache.commons.lang.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomerModelParser {

	private enum CustomerModelParserConstants {
		DATE_FORMAT("dd-MM-yyyy"), IS_NOT_BENEFICIARY("2"), IS_BENEFICIARY("1"), COMMA(",");
		private String value;

		private CustomerModelParserConstants(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	private enum CustomerModelParserColumns {
		FIRST_NAME_COLUMN(0), MIDDLE_NAME_COLUMN(1), LAST_NAME_COLUMN(2), ID_NUMBER_COLUMN(3), DATE_OF_BIRTH_COLUMN(4), CLIENT_REFERENCE_COLUMN(5), PHONE_ONE_COLUMN(6), PHONE_TWO_COLUMN(7), EMAIL_COLUMN(8), POBOX_COULMN(9), ZIPCODE_COLUMN(10), BUILDING_NUM_COLUMN(11), STREET_NAME_COLUMN(
				12), NOTE_COLUMN(13), MOBILE_NUMBER_COLUMN(14), ALIAS_COLUMN(15), EXTERNAL_ACC_COLUMN(16), NOTIFICATION_SHOW_TYPE_COLUMN(17), NFC_SERIAL_COLUMN(18), ID_TYPE_COLUMN(19), CITY_COLUMN(20), PREF_LANG_COLUMN(21), BANK_COLUMN(22), PROFILE_COLUMN(23), NATIONALITY_COLUMN(
						24), IS_BENEFICIARY_COLUMN(
								25), FIELDS_NUMBER_COLUMN_WITHOUT_BENEFICIARY(25), BENEFICIARY_NAME_COLUMN(26), BENEFICIARY_ID_COLUMN(27), BENEFICIARY_ID_TYPE_COLUMN(28), BENEFICIARY_EMAIL_COLUMN(29), GUARDIAN_NAME_COLUMN(30), GUARDIAN_EMAIL_COLUMN(31), FILEDS_NUMBER_WITH_BENEFICIARY(31);
		private Integer columnNumber;

		private CustomerModelParserColumns(Integer columnNumber) {
			this.columnNumber = columnNumber;
		}

		public int getColumnNumber() {
			return columnNumber;
		}
	}

	public CustomerModel genrateCustomerModel(String record) {
		validateEmptyRecord(record);
		String[] recordData = splitRecordAndValidateRecordLength(record);
		validateData(recordData);
		return buildCustomerModel(recordData);
	}

	private CustomerModel buildCustomerModel(String[] recordData) {
		CustomerModel.BuilderModel customerBuilder = new CustomerModel.BuilderModel().firstName(recordData[CustomerModelParserColumns.FIRST_NAME_COLUMN.getColumnNumber()]).middleName(recordData[CustomerModelParserColumns.MIDDLE_NAME_COLUMN.getColumnNumber()])
				.lastName(recordData[CustomerModelParserColumns.LAST_NAME_COLUMN.getColumnNumber()]).idNumber(recordData[CustomerModelParserColumns.ID_NUMBER_COLUMN.getColumnNumber()]).dob(parseDate(recordData[CustomerModelParserColumns.DATE_OF_BIRTH_COLUMN.getColumnNumber()]))
				.clientRef(recordData[CustomerModelParserColumns.CLIENT_REFERENCE_COLUMN.getColumnNumber()]).phoneOne(recordData[CustomerModelParserColumns.PHONE_ONE_COLUMN.getColumnNumber()]).phoneTwo(recordData[CustomerModelParserColumns.PHONE_TWO_COLUMN.getColumnNumber()])
				.email(recordData[CustomerModelParserColumns.EMAIL_COLUMN.getColumnNumber()]).pobox(recordData[CustomerModelParserColumns.POBOX_COULMN.getColumnNumber()]).zipCode(recordData[CustomerModelParserColumns.ZIPCODE_COLUMN.getColumnNumber()])
				.buildingNum(recordData[CustomerModelParserColumns.BUILDING_NUM_COLUMN.getColumnNumber()]).streetName(recordData[CustomerModelParserColumns.STREET_NAME_COLUMN.getColumnNumber()]).note(recordData[CustomerModelParserColumns.NOTE_COLUMN.getColumnNumber()])
				.mobileNumber(recordData[CustomerModelParserColumns.MOBILE_NUMBER_COLUMN.getColumnNumber()]).alias(recordData[CustomerModelParserColumns.ALIAS_COLUMN.getColumnNumber()]).externalAcc(recordData[CustomerModelParserColumns.EXTERNAL_ACC_COLUMN.getColumnNumber()])
				.notificationShowType(recordData[CustomerModelParserColumns.NOTIFICATION_SHOW_TYPE_COLUMN.getColumnNumber()]).nfcSerial(recordData[CustomerModelParserColumns.NFC_SERIAL_COLUMN.getColumnNumber()]).city(recordData[CustomerModelParserColumns.CITY_COLUMN.getColumnNumber()])
				.prefLang(LanguageMapper.getInstance().getLanguageLocaleCode(recordData[CustomerModelParserColumns.PREF_LANG_COLUMN.getColumnNumber()])).bank(recordData[CustomerModelParserColumns.BANK_COLUMN.getColumnNumber()]).refProfile(recordData[CustomerModelParserColumns.PROFILE_COLUMN.getColumnNumber()])
				.idType(recordData[CustomerModelParserColumns.ID_TYPE_COLUMN.getColumnNumber()]).nationality(recordData[CustomerModelParserColumns.NATIONALITY_COLUMN.getColumnNumber()]).isBeneficiary(recordData[CustomerModelParserColumns.IS_BENEFICIARY_COLUMN.getColumnNumber()]);

		setDataIsCustomerNotBeneficiary(recordData, customerBuilder);
		return customerBuilder.build();
	}

	private void setDataIsCustomerNotBeneficiary(String[] recordData, CustomerModel.BuilderModel customerModelBuilder) {
		if (recordData.length - 1 >= CustomerModelParserColumns.FILEDS_NUMBER_WITH_BENEFICIARY.getColumnNumber()) {
			validateBeneficiaryAndGuardianValue(recordData);
			customerModelBuilder.beneficiaryName(recordData[CustomerModelParserColumns.BENEFICIARY_NAME_COLUMN.getColumnNumber()]).beneficiaryId(recordData[CustomerModelParserColumns.BENEFICIARY_ID_COLUMN.getColumnNumber()])
					.beneficiaryIdType(recordData[CustomerModelParserColumns.BENEFICIARY_ID_TYPE_COLUMN.getColumnNumber()]).beneficiaryEmail(recordData[CustomerModelParserColumns.BENEFICIARY_EMAIL_COLUMN.getColumnNumber()])
					.guardian(recordData[CustomerModelParserColumns.GUARDIAN_NAME_COLUMN.getColumnNumber()]).guardianEmail(recordData[CustomerModelParserColumns.GUARDIAN_EMAIL_COLUMN.getColumnNumber()]);
		}
	}

	private void validateData(String[] recordData) {
		validateValue(recordData[CustomerModelParserColumns.FIRST_NAME_COLUMN.getColumnNumber()].trim(), "FIRST NAME");
		validateValue(recordData[CustomerModelParserColumns.MIDDLE_NAME_COLUMN.getColumnNumber()].trim(), "MIDDLE NAME");
		validateValue(recordData[CustomerModelParserColumns.LAST_NAME_COLUMN.getColumnNumber()].trim(), "LAST NAME");
		validateValue(recordData[CustomerModelParserColumns.ID_NUMBER_COLUMN.getColumnNumber()].trim(), "ID NUMBER");
		parseDate(recordData[CustomerModelParserColumns.DATE_OF_BIRTH_COLUMN.getColumnNumber()].trim());
		validateValue(recordData[CustomerModelParserColumns.CLIENT_REFERENCE_COLUMN.getColumnNumber()].trim(), "CLIENT REFERENCE");
		validateValue(recordData[CustomerModelParserColumns.MOBILE_NUMBER_COLUMN.getColumnNumber()].trim(), "MOBILE NUMBER");
		validateValue(recordData[CustomerModelParserColumns.NOTIFICATION_SHOW_TYPE_COLUMN.getColumnNumber()].trim(), "NOTIFICATION SHOW TYPE");
		validateValue(recordData[CustomerModelParserColumns.ID_TYPE_COLUMN.getColumnNumber()].trim(), "ID TYPE");
		validateValue(recordData[CustomerModelParserColumns.CITY_COLUMN.getColumnNumber()].trim(), "CITY");
		validateValue(recordData[CustomerModelParserColumns.PREF_LANG_COLUMN.getColumnNumber()].trim(), "PREF LANG");
		validateValue(recordData[CustomerModelParserColumns.BANK_COLUMN.getColumnNumber()].trim(), "BANK");
		validateValue(recordData[CustomerModelParserColumns.PROFILE_COLUMN.getColumnNumber()].trim(), "PROFILE");
		validateValue(recordData[CustomerModelParserColumns.IS_BENEFICIARY_COLUMN.getColumnNumber()].trim(), "IS BENEFICARY");
	}

	private void validateBeneficiaryAndGuardianValue(String[] recordData) {
		validateValue(recordData[CustomerModelParserColumns.BENEFICIARY_NAME_COLUMN.getColumnNumber()].trim(), "BENEFICIARY NAME");
		validateValue(recordData[CustomerModelParserColumns.BENEFICIARY_ID_COLUMN.getColumnNumber()].trim(), "BENEFICIARY ID");
		validateValue(recordData[CustomerModelParserColumns.BENEFICIARY_ID_TYPE_COLUMN.getColumnNumber()].trim(), "BENEFICIARY ID TYPE");
		validateValue(recordData[CustomerModelParserColumns.BENEFICIARY_EMAIL_COLUMN.getColumnNumber()].trim(), "BENEFICIARY EMAIL");
		validateValue(recordData[CustomerModelParserColumns.GUARDIAN_NAME_COLUMN.getColumnNumber()].trim(), "GUARDIAN NAME");
		validateValue(recordData[CustomerModelParserColumns.GUARDIAN_EMAIL_COLUMN.getColumnNumber()].trim(), "GUARDIAN EMAIL");
	}

	private Date parseDate(String sourceDate) {
		validateCustomerDate(sourceDate);
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(CustomerModelParserConstants.DATE_FORMAT.getValue());
			dateFormat.setLenient(false);
			Date formatedDate = dateFormat.parse(sourceDate);
			compareCustomerDOBAndCurrentDate(formatedDate);
			checkIfCustomerDateCorrectDepandOnTheSysConfiguration(formatedDate);
			return formatedDate;
		} catch (ParseException e) {
			throw new CustomerParseDateException(e.getMessage());
		}
	}

	private void checkIfCustomerDateCorrectDepandOnTheSysConfiguration(Date formatedDate) {
		int minAge = Integer.parseInt(MPayContext.getInstance().getLookupsLoader().getSystemConfigurations(SysConfigKeys.MIN_AGE).getConfigValue());
		if (DateUtils.addYears(formatedDate, (int) minAge).after(SystemHelper.getCurrentDateTime()))
			throw new CustomerParseDateException(formatedDate + " || Invalid customer min Age");
	}

	private void compareCustomerDOBAndCurrentDate(Date formatedDate) {
		if (formatedDate.after(SystemHelper.getCurrentDateTime()))
			throw new CustomerParseDateException(formatedDate + " || Customer invalid DOBFuture");
	}

	private void validateCustomerDate(String sourceDate) {
		if (sourceDate == null || sourceDate.trim().isEmpty())
			throw new InvalidRecordException("CustomerDate should not be empty");
	}

	private String[] splitRecordAndValidateRecordLength(String record) {
		String[] recordData = record.trim().split(CustomerModelParserConstants.COMMA.getValue());
		if (recordData.length - 1 == CustomerModelParserColumns.FIELDS_NUMBER_COLUMN_WITHOUT_BENEFICIARY.getColumnNumber() || recordData.length - 1 == CustomerModelParserColumns.FILEDS_NUMBER_WITH_BENEFICIARY.getColumnNumber()) {
			if (recordData[CustomerModelParserColumns.IS_BENEFICIARY_COLUMN.getColumnNumber()].trim().equals(CustomerModelParserConstants.IS_BENEFICIARY.getValue())
					|| recordData[CustomerModelParserColumns.IS_BENEFICIARY_COLUMN.getColumnNumber()].equals(CustomerModelParserConstants.IS_NOT_BENEFICIARY.getValue())) {
				return recordData;
			} else {
				throw new InvalidRecordException("Invalid value Is Beneficary Value ");
			}
		} else {
			throw new InvalidRecordException("Some values are missing");
		}
	}

	private void validateValue(String value, String fieldName) {
		if (value.trim().isEmpty()) {
			throw new InvalidRecordException("Invalid value :- " + fieldName);
		}
	}

	private void validateEmptyRecord(String record) {
		if (record == null || record.trim().isEmpty())
			throw new EmptyRecordException();
	}

	public class EmptyRecordException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public EmptyRecordException() {
			super();
		}
	}

	public class InvalidRecordException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public InvalidRecordException(String cause) {
			super(cause);
		}
	}

	public class CustomerParseDateException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		CustomerParseDateException(String cause) {
			super(cause);
		}
	}
}