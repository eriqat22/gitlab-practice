package com.progressoft.mpay.plugins.bulkregistration.model.view;

import com.progressoft.mpay.entities.MPAY_Customer;

import java.util.Map;

public interface BulkRegistrationFileModelView {

	Map<Integer, MPAY_Customer> getValidCustomerModel();

	Map<Integer, String> getInValidCustomerRecord();

}