package com.progressoft.mpay.plugins.bulkregistration.model.view;

import java.util.Date;

public interface CustomerModelView {
	String getFullName();

	String getFirstName();

	String getMiddleName();

	String getLastName();

	String getIdNum();

	Date getDob();

	String getClientRef();

	String getPhoneOne();

	String getPhoneTwo();

	String getEmail();

	String getPobox();

	String getZipCode();

	String getBuildingNum();

	String getStreetName();

	String getNote();

	String getMobileNumber();

	String getAlias();

	String getExternalAcc();

	String getNotificationShowType();

	String getNfcSerial();

	String getIdType();

	String getCity();

	String getPrefLang();

	String getBank();

	String getRefProfile();

	String getNationality();

	String getIsBeneficiary();

	String getBeneficiaryName();

	String getBeneficiaryId();

	String getBeneficiaryIdType();

	String getBeneficiaryEmail();

	String getGuardian();

	String getGuardianEmail();
}
