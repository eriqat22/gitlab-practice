package com.progressoft.mpay.plugins.bulkregistration.usecase.impl;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.mpay.entities.MPAY_BulkRegistration;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.plugins.bulkregistration.usecase.InitMpayCustomerValues;

public class InitMpayCustomerValuesImpl implements InitMpayCustomerValues {

	@Override
	public MPAY_Customer initMpayCustomerValue(MPAY_Customer customer, MPAY_BulkRegistration bulkRegistration,
			Long orgId, Long wfId, WorkflowStatus status) {
		customer.setBulkRegistration(bulkRegistration);
		customer.setOrgId(orgId);
		customer.setWorkflowId(wfId);
		customer.setStatusId(status);
		customer.setMas(1L);
		customer.setIsActive(true);
		return customer;
	}

}
