package com.progressoft.mpay.plugins.bulkregistration.usecase;

import com.progressoft.workflow.osworkflow.jpa.beans.JPAWorkflowEntry;

@FunctionalInterface
public interface JPACurrentStepUseCase {
	 void save(JPAWorkflowEntry entry, int statusId);
}
