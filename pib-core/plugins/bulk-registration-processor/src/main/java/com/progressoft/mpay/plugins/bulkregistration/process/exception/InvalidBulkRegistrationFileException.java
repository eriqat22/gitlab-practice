package com.progressoft.mpay.plugins.bulkregistration.process.exception;

public class InvalidBulkRegistrationFileException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InvalidBulkRegistrationFileException(String message) {
		super(message);
	}

	public InvalidBulkRegistrationFileException(String message, Throwable cause) {
		super(message, cause);
	}
}
