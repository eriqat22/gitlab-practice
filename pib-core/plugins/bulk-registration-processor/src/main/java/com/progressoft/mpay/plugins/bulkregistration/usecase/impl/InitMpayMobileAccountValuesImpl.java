package com.progressoft.mpay.plugins.bulkregistration.usecase.impl;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.plugins.bulkregistration.usecase.InitMpayMobileAccountValues;
import com.progressoft.workflow.osworkflow.jpa.beans.JPAWorkflowEntry;

import java.sql.Timestamp;

public class InitMpayMobileAccountValuesImpl implements InitMpayMobileAccountValues {

	@Override
	public MPAY_MobileAccount initMpayMobileAccountValue(MPAY_Customer customer, MPAY_CustomerMobile custMobile,
			JPAWorkflowEntry entry, WorkflowStatus status) {
		MPAY_MobileAccount mobileAccount = new MPAY_MobileAccount();
		mobileAccount.setBank(customer.getBank());
		mobileAccount.setBankedUnbanked(customer.getBankedUnbanked());
		mobileAccount.setExternalAcc(customer.getExternalAcc());
		mobileAccount.setMobile(custMobile);
		mobileAccount.setRefProfile(customer.getRefProfile());
		mobileAccount.setIsActive(true);
		mobileAccount.setTenantId(customer.getTenantId());
		mobileAccount.setStatusId(status);
		mobileAccount.setWorkflowId(entry.getId());
		mobileAccount.setMas(1L);
		mobileAccount.setIsDefault(true);
		mobileAccount.setIsRegistered(false);
		mobileAccount.setIsSwitchDefault(true);
		mobileAccount.setLastTransactionDate(new Timestamp(System.currentTimeMillis()));
		return mobileAccount;
	}

}
