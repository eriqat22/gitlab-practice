package com.progressoft.mpay.plugins.signin;

public class Constants {
	public static final String DEVICE_KEY = "DeviceKey";
	public static final String ALIAS_KEY = "alias";
	public static final String NAME = "name";
	public static final String IS_ADMIN_KEY = "isAdmin";
	public static final String IBAN = "iban";
	public static final String DESCRIPTION = "description";
	public static final String MOBILE_NUMBER = "mobileNumber";
	public static final String CITY_CODE = "cityCode";
	public static final String COUNTRY_CODE = "countryCode";
	public static final String POST_CODE = "postCode";
	public static final String ADDRESS = "address";
	public static final String BALANCE = "balance";


	private Constants() {

	}
}
