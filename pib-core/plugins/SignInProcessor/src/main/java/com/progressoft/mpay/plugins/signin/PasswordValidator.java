package com.progressoft.mpay.plugins.signin;

import java.sql.Timestamp;
import java.util.Calendar;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class PasswordValidator {
	private static final Logger logger = LoggerFactory.getLogger(PasswordValidator.class);

	private PasswordValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context, boolean isCustomer) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		checkIfDeviceContainExtraData(context);
		ValidationResult result;
		if (isCustomer)
			result = validateCustomerDevice(context);
		else
			result = validateCorporateDevice(context);
		return result;
	}

	private static ValidationResult validateCustomerDevice(MessageProcessingContext context) {
		MPAY_CustomerDevice device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DEVICE_KEY);
		SignInMessage message = (SignInMessage) context.getRequest();
		boolean isValid = device.getPassword().equals(message.getPassword());
		if (device.getIsBlocked())
			return new ValidationResult(ReasonCodes.DEVICE_BLOCKED, null, false);
		handleCustomerDeviceSignIn(context, device, isValid);
		if (isValid) {
			if (SystemHelper.getCurrentDateTime().after(calculatePasswordExpiredDate(context, device)))
				return new ValidationResult(ReasonCodes.PASS_WORD_EXPIRED, null, false);
			isValid = true;
		}

		if (!isValid)
			return new ValidationResult(ReasonCodes.INVALID_PASSKEY, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateCorporateDevice(MessageProcessingContext context) {
		MPAY_CorporateDevice device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DEVICE_KEY);
		if (device.getIsBlocked())
			return new ValidationResult(ReasonCodes.DEVICE_BLOCKED, null, false);
		SignInMessage message = (SignInMessage) context.getRequest();
		boolean isValid = device.getPassword().equals(message.getPassword());
		handleCorporateDeviceSignIn(context, device, isValid);
		if (!isValid)
			return new ValidationResult(ReasonCodes.INVALID_PASSKEY, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static void checkIfDeviceContainExtraData(MessageProcessingContext context) {
		if (!context.getExtraData().containsKey(Constants.DEVICE_KEY))
			new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static Timestamp calculatePasswordExpiredDate(ProcessingContext context, MPAY_CustomerDevice device) {
		Timestamp passwordLastChanged = device.getPasswordLastChanged();
		String passwordExpiryTimeInHours = context.getSystemParameters().getPasswordExpiryTimeInHours();
		Calendar instance = Calendar.getInstance();
		instance.setTimeInMillis(passwordLastChanged.getTime());
		instance.set(Calendar.HOUR, Integer.parseInt(passwordExpiryTimeInHours) + instance.get(Calendar.HOUR));

		return new Timestamp(instance.getTimeInMillis());
	}

	private static void handleCustomerDeviceSignIn(ProcessingContext context, MPAY_CustomerDevice device,
			boolean isValid) {
		if (isValid) {
			device.setRetryCount(0l);
		} else {
			device.setRetryCount(device.getRetryCount() + 1);
			if (device.getRetryCount() >= context.getSystemParameters().getPasswordMaxRetryCount()) {
				device.setIsBlocked(true);
			}
		}
		context.getDataProvider().mergeCustomerDevice(device);
	}

	private static void handleCorporateDeviceSignIn(ProcessingContext context, MPAY_CorporateDevice device,
			boolean isValid) {
		if (isValid) {
			device.setRetryCount(0l);
		} else {
			device.setRetryCount(device.getRetryCount() + 1);
			if (device.getRetryCount() >= context.getSystemParameters().getPasswordMaxRetryCount()) {
				device.setIsBlocked(true);
			}
		}
		context.getDataProvider().mergeCorporateDevice(device);
	}
}
