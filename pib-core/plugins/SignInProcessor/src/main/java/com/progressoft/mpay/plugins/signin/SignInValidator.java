package com.progressoft.mpay.plugins.signin;

import com.progressoft.mpay.entities.WF_Customer;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;

public class SignInValidator {
	private static final Logger logger = LoggerFactory.getLogger(SignInValidator.class);

	private SignInValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			return validateCustomer(context);
		else if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			return validateCorporate(context);
		else
			return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
	}

	private static ValidationResult validateCustomer(MessageProcessingContext context) {

		if (isCustomerOnCreatedStep(context))
			return new ValidationResult(ReasonCodes.VISIT_NEAREST_BRANCH, null, false);

		ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,
				context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = CustomerDeviceValidator.validate(context);
		if (!result.isValid())
			return result;

		result = PasswordValidator.validate(context, true);
		if (!result.isValid())
			return result;

		return LimitsValidator.validate(context);
	}

	private static boolean isCustomerOnCreatedStep(MessageProcessingContext context) {
		return context.getSender().getCustomer().getStatusId().getCode().equals(WF_Customer.STEP_Created);
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,
				context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = CorporateDeviceValidator.validate(context);
		if (!result.isValid())
			return result;

		result = PasswordValidator.validate(context, false);
		if (!result.isValid())
			return result;
		return LimitsValidator.validate(context);
	}
}
