package com.progressoft.mpay.plugins.signin;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;

public class SignInProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(SignInProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = SignInValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error in ProcessMessage", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
			String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		return createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		MessageProcessingResult result;
		logger.debug("Inside AcceptMessage ...");
		SignInMessage request = (SignInMessage)context.getRequest();
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)) {
			MPAY_CustomerDevice device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DEVICE_KEY);
			if(device != null) {
				device.setIsOnline(true);
				device.setLastLoginTime(SystemHelper.getSystemTimestamp());
				if (request.getDeviceToken() != null)
					device.setExtraData(request.getDeviceToken());

				context.getDataProvider().mergeCustomerDevice(device);
			}
			result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
			String response = generateResponse(context, true);
			result.setResponse(response);
		} else {
			MPAY_CorporateDevice device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DEVICE_KEY);
			device.setIsOnline(true);
			device.setLastLoginTime(SystemHelper.getSystemTimestamp());
			if(request.getDeviceToken() != null)
				device.setExtraData(request.getDeviceToken());
			context.getDataProvider().mergeCorporateDevice(device);
			result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
			String response = generateResponse(context, true);
			result.setResponse(response);
		}
		return result;
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
				context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
					context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		if (isAccepted && context.getSender().getMobile() != null) {
			response.getExtraData().add(new ExtraData(Constants.ALIAS_KEY, context.getSender().getMobile().getAlias()));
			response.getExtraData().add(new ExtraData(Constants.NAME, context.getSender().getCustomer().getFullName()));
			response.getExtraData()
					.add(new ExtraData(Constants.IBAN, context.getSender().getCustomer().getIban()));
			response.getExtraData()
					.add(new ExtraData(Constants.COUNTRY_CODE, context.getSender().getCustomer().getNationality().getCountryStringIsoCode()));
			response.getExtraData()
					.add(new ExtraData(Constants.CITY_CODE, context.getSender().getCustomer().getCity().getCode()));
			response.getExtraData()
					.add(new ExtraData(Constants.POST_CODE, context.getSender().getCustomer().getPobox()));
			response.getExtraData()
					.add(new ExtraData(Constants.ADDRESS, context.getSender().getCustomer().getAddress()));
			response.getExtraData()
					.add(new ExtraData(Constants.BALANCE, String.valueOf(context.getSender().getMobileAccount().getRefAccount().getBalance())));

		} else if (isAccepted && context.getSender().getService() != null) {
			response.getExtraData()
					.add(new ExtraData(Constants.ALIAS_KEY, context.getSender().getService().getAlias()));
			response.getExtraData().add(new ExtraData(Constants.NAME, context.getSender().getCorporate().getName()));
			response.getExtraData()
					.add(new ExtraData(Constants.DESCRIPTION, context.getSender().getService().getDescription()));
			response.getExtraData()
					.add(new ExtraData(Constants.IBAN, context.getSender().getCorporate().getIban()));
			response.getExtraData()
					.add(new ExtraData(Constants.MOBILE_NUMBER, context.getSender().getCorporate().getMobileNumber()));
			response.getExtraData()
					.add(new ExtraData(Constants.CITY_CODE, context.getSender().getCorporate().getCity().getCode()));
			response.getExtraData()
					.add(new ExtraData(Constants.COUNTRY_CODE, context.getSender().getCorporate().getRefCountry().getCountryStringIsoCode()));
			response.getExtraData()
					.add(new ExtraData(Constants.POST_CODE, context.getSender().getCorporate().getPobox()));
			response.getExtraData()
					.add(new ExtraData(Constants.BALANCE, String.valueOf(context.getSender().getServiceAccount().getRefAccount().getBalance())));
			String isAdmin = "0";
			if ((boolean) context.getExtraData().get(Constants.IS_ADMIN_KEY))
				isAdmin = "1";
			response.getExtraData().add(new ExtraData(Constants.IS_ADMIN_KEY, isAdmin));
		}

		return response.toString();
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
		logger.debug("Inside PreProcessMessage ...");
		SignInMessage message = SignInMessage.parseMessage(context.getRequest());
		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
			sender.setMobile(context.getDataProvider().getCustomerMobile(message.getSender()));
			if (sender.getMobile() == null)
				return;
			sender.setCustomer(sender.getMobile().getRefCustomer());
			sender.setMobileAccount(
					SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
			if (sender.getMobileAccount() == null)
				return;
			sender.setProfile(sender.getMobileAccount().getRefProfile());
			MPAY_CustomerDevice device = context.getDataProvider().getCustomerDevice(message.getDeviceId());
			if (device != null)
				context.getExtraData().put(Constants.DEVICE_KEY, device);
		} else {
			sender.setService(
					context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
			if (sender.getService() == null)
				return;
			sender.setCorporate(sender.getService().getRefCorporate());
			sender.setServiceAccount(
					SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
			if (sender.getServiceAccount() == null)
				return;
			sender.setProfile(sender.getServiceAccount().getRefProfile());
			setExtraData(context, message, sender);
		}
	}

	private void setExtraData(MessageProcessingContext context, SignInMessage message, ProcessingContextSide sender) {
		boolean isAdmin = false;
		if (context.getSystemParameters().getServiceAdminMessageTypeCode()
				.equals(sender.getService().getPaymentType().getCode()))
			isAdmin = true;
		context.getExtraData().put(Constants.IS_ADMIN_KEY, isAdmin);
		MPAY_CorporateDevice device = context.getDataProvider().getCorporateDevice(message.getDeviceId());
		if (device != null)
			context.getExtraData().put(Constants.DEVICE_KEY, device);
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
			String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		logger.debug("Inside Reverse ...");
		return null;
	}

	@Override
	public boolean isNeedToSessionId(){
		return false;
	}
}
