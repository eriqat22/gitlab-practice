package com.progressoft.mpay.plugins.signin;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class SignInMessage extends MPayRequest {
	private static final String PASS_WORD_KEY = "pass";
	private static final String DEVICE_TOKEN_KEY = "deviceToken";

	private String password;
	private String deviceToken;

	public SignInMessage() {
		//
	}

	public SignInMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public static SignInMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		SignInMessage message = new SignInMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setPassword(message.getValue(PASS_WORD_KEY));
		if (message.getPassword() == null)
			throw new MessageParsingException(PASS_WORD_KEY);

		message.setDeviceToken(message.getValue(DEVICE_TOKEN_KEY));

		return message;
	}
}
