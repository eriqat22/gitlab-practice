package com.progressoft.mpay.plugins.crm.transactiondetails.processor;

import com.google.gson.GsonBuilder;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.entity.TransactionDetailsFilter;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.crm.transactiondetails.builder.Result;
import com.progressoft.mpay.plugins.crm.transactiondetails.dao.CRMTransactionsDetailsTransaction;
import com.progressoft.mpay.plugins.crm.transactiondetails.message.CRMTransactionsDetailsMessage;
import com.progressoft.mpay.plugins.crm.transactiondetails.validator.CRMTransactionsDetailsValidator;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CRMTransactionsDetailsProcessor implements MessageProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(CRMTransactionsDetailsProcessor.class);

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
        LOGGER.debug("Inside ProcessIntegration ...");
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        LOGGER.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        LOGGER.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext arg0) {
        LOGGER.debug("Inside Reverse ...");
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        LOGGER.debug("Inside processMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            return isValidResult(context, CRMTransactionsDetailsValidator.validate(context));
        } catch (MessageParsingException e) {
            LOGGER.debug("Invalid Message Received", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            LOGGER.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        LOGGER.debug("Inside PreProcessMessage ...");
        CRMTransactionsDetailsMessage message = CRMTransactionsDetailsMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setRequest(message);
        if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
            if (loadSenderMobile(context, sender)) return;
        } else {
            if (loadSenderService(context, sender)) return;
        }
        OTPHanlder.loadOTP(context, message.getPin());
    }

    private boolean loadSenderService(MessageProcessingContext context, ProcessingContextSide sender) {
        sender.setService(context.getDataProvider().getCorporateService(context.getRequest().getSender(), context.getRequest().getSenderType()));
        if (sender.getService() == null)
            return true;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return true;
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        return false;
    }

    private boolean loadSenderMobile(MessageProcessingContext context, ProcessingContextSide sender) {
        sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
        if (sender.getMobile() == null)
            return true;
        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
        if (sender.getMobileAccount() == null)
            return true;
        sender.setProfile(sender.getMobileAccount().getRefProfile());
        return false;
    }

    private MessageProcessingResult isValidResult(MessageProcessingContext context, ValidationResult validationResult) {
        if (validationResult.isValid())
            return acceptMessage(context);
        else
            return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        LOGGER.debug("Inside RejectMessage ...");
        MessageProcessingResult processingResult = createResult(buildResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED));
        String response = generateResponse(context, false, new ArrayList<>());
        processingResult.getMessage().setResponseContent(response);
        processingResult.setResponse(response);
        return processingResult;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        LOGGER.debug("Inside AcceptMessage ...");
        CRMTransactionsDetailsMessage message = (CRMTransactionsDetailsMessage) context.getRequest();
        MessageProcessingResult result = createResult(buildResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED));
        String response = generateResponse(context, true, context.getDataProvider().searchTransactions(buildFilter(message)));
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private TransactionDetailsFilter buildFilter(CRMTransactionsDetailsMessage message) {
        TransactionDetailsFilter filter = new TransactionDetailsFilter();
        filter.setFromTime(message.getFromTime());
        filter.setToTime(message.getToTime());
        filter.setTxDirection(message.getTxDirection());
        filter.setTxOperation(message.getTxOperation());
        filter.setTxReceiverMobile(message.getTxReceiverMobile());
        filter.setTxReceiverService(message.getTxReceiverService());
        filter.setTxReference(message.getTxReference());
        filter.setTxSenderMobile(message.getTxSenderMobile());
        filter.setTxSenderService(message.getTxSenderService());
        filter.setTxType(message.getTxType());
        return filter;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted, List<MPAY_Transaction> transactions) {
        LOGGER.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        response.setDesc(getDescriptionBasedOnNls(context));
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        if (isAccepted)
            buildTransactionIsAccepted(context, transactions, response);
        else
            response.getExtraData().add(new ExtraData("txs", getListJson(new ArrayList<>())));
        return response.toString();
    }

    private String getDescriptionBasedOnNls(MessageProcessingContext context) {
        String description;
        MPAY_Reasons_NLS reasonNLS = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (reasonNLS == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = reasonNLS.getDescription();
        return description;
    }

    private void buildTransactionIsAccepted(MessageProcessingContext context, List<MPAY_Transaction> transactions, MPayResponse response) {
        List<CRMTransactionsDetailsTransaction> transactionsDetailsTransactions = new ArrayList<>();
        if (!transactions.isEmpty())
            for (MPAY_Transaction transaction : transactions)
                transactionsDetailsTransactions.add(new CRMTransactionsDetailsTransaction(transaction, getSender(context)));
        response.getExtraData().add(new ExtraData("txs", getListJson(transactionsDetailsTransactions)));
    }

    private String getSender(MessageProcessingContext context) {
        String sender = null;
        if (context.getSender().getMobile() == null && context.getSender().getService() == null)
            sender = context.getSender().getInfo();
        else if (context.getSender().getMobile() != null)
            sender = context.getSender().getMobile().getMobileNumber();
        else if (context.getSender().getService() != null)
            sender = context.getSender().getService().getName();
        return sender;
    }

    private String getListJson(List<CRMTransactionsDetailsTransaction> transactions) {
        LOGGER.debug("Inside GetListJson ...");
        return new GsonBuilder().create().toJson(transactions);
    }

    private MessageProcessingResult createResult(Result result) {
        LOGGER.debug("Inside CreateResult ...");
        MessageProcessingContext processingContext = result.getProcessingContext();
        MPAY_Reason reason = processingContext.getLookupsLoader().getReason(result.getReasonCode());
        MPAY_ProcessingStatus status = processingContext.getLookupsLoader().getProcessingStatus(result.getProcessingStatus());
        processingContext.getMessage().setReason(reason);
        processingContext.getMessage().setReasonDesc(result.getReasonDescription());
        processingContext.getMessage().setProcessingStatus(status);
        MessageProcessingResult processingResult = new MessageProcessingResult();
        processingResult.setMessage(processingContext.getMessage());
        processingResult.setReasonCode(result.getReasonCode());
        processingResult.setStatus(status);
        processingResult.setStatusDescription(result.getReasonDescription());
        return processingResult;
    }

    private Result buildResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatusCodes) {
        return new Result.ResultBuilder().processingContext(context).reasonCode(reasonCode).reasonDescription(reasonDescription).processingStatus(processingStatusCodes).build();
    }
}
