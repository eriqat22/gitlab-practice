package com.progressoft.mpay.plugins.crm.transactiondetails.dao;

import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_Transaction;
import org.apache.commons.lang.NullArgumentException;
import org.joda.time.format.DateTimeFormat;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

public class CRMTransactionsDetailsTransaction {
    private static final String CREDIT_TYPE_CODE = "1";
    private static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private String processingStatusDescription;
    private String receiverServiceAccount;
    private String receiverMobileAccount;
    private String senderServiceAccount;
    private String senderMobileAccount;
    private BigDecimal receiverCharge;
    private BigDecimal senderCharge;
    private String typeDescription;
    private BigDecimal receiverTax;
    private double chargeAmount;
    private double externalFees;
    private String receiverBank;
    private double totalAmount;
    private String reasonCode;
    private String senderBank;
    private String currency;
    private String receiver;
    private String comments;
    private String date;
    private long reference;
    private double amount;
    private String sender;
    private String desc;
    private long status;
    private long type;
    private long id;
    private String transactionType;
    private String receiverInfo;
    private String messageId;
    private BigDecimal senderTax;
    private String isReversed;
    private String serviceName;
    private String operationName;
    private String updatingDate;
    private String updateBy;

    public CRMTransactionsDetailsTransaction() {
    }

    public CRMTransactionsDetailsTransaction(MPAY_Transaction transaction, String sender) {
        if (transaction == null)
            throw new NullArgumentException("transaction");

        this.reasonCode = transaction.getReason().getCode();
        this.processingStatusDescription = transaction.getProcessingStatus().getDescription();
        this.receiverTax = transaction.getReceiverTax();
        this.id = transaction.getId();
        this.desc = transaction.getComments();
        this.senderCharge = transaction.getSenderCharge();
        this.receiverCharge = transaction.getReceiverCharge();
        this.currency = (transaction.getCurrency().getStringISOCode());
        MPAY_Bank senderBank = transaction.getSenderBank();
        if (senderBank != null)
            this.senderBank = senderBank.getCode();
        this.receiverBank = transaction.getReceiverBank().getCode();
        //this.date = FORMAT.format(transaction.getTransDate());
        this.date = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS").print(transaction.getTransDate().getTime());
        this.status = transaction.getProcessingStatus().getId();
        this.type = transaction.getRefOperation().getMessageType().getId();
        this.typeDescription = transaction.getRefOperation().getMessageType().getDescription();
        this.comments = transaction.getComments();
        this.reference = Long.parseLong(transaction.getReference());
        this.senderTax = transaction.getSenderTax();
        this.messageId = transaction.getRefMessage() == null ? null : transaction.getRefMessage().getMessageID();
        this.isReversed = transaction.getIsReversed() ? "1" : "0";
        this.operationName = transaction.getRefOperation().getOperation();
        this.transactionType = transaction.getRefType().getDescription();
        this.receiverInfo = transaction.getReceiverInfo();
        this.updatingDate = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS").print(transaction.getUpdatingDate().getTime());
        this.updateBy = transaction.getUpdatedBy();
        prepareSender(transaction);
        prepareReceiver(transaction);
        prepareAmounts(transaction, sender);
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationNane) {
        this.operationName = operationNane;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getIsReversed() {
        return isReversed;
    }

    public void setIsReversed(String isReversed) {
        this.isReversed = isReversed;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getExternalFees() {
        return externalFees;
    }

    public void setExternalFees(double externalFees) {
        this.externalFees = externalFees;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public long getReference() {
        return reference;
    }

    public void setReference(long reference) {
        this.reference = reference;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public BigDecimal getSenderCharge() {
        return senderCharge;
    }

    public void setSenderCharge(BigDecimal senderCharge) {
        this.senderCharge = senderCharge;
    }

    public BigDecimal getReceiverCharge() {
        return receiverCharge;
    }

    public void setReceiverCharge(BigDecimal receiverCharge) {
        this.receiverCharge = receiverCharge;
    }

    public BigDecimal getReceiverTax() {
        return receiverTax;
    }

    public void setReceiverTax(BigDecimal receiverTax) {
        this.receiverTax = receiverTax;
    }

    public String getProcessingStatusDescription() {
        return processingStatusDescription;
    }

    public void setProcessingStatusDescription(String processingStatusDescription) {
        this.processingStatusDescription = processingStatusDescription;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getSenderMobileAccount() {
        return senderMobileAccount;
    }

    public void setSenderMobileAccount(String senderMobileAccount) {
        this.senderMobileAccount = senderMobileAccount;
    }

    public String getReceiverMobileAccount() {
        return receiverMobileAccount;
    }

    public void setReceiverMobileAccount(String receiverMobileAccount) {
        this.receiverMobileAccount = receiverMobileAccount;
    }

    public String getSenderServiceAccount() {
        return senderServiceAccount;
    }

    public void setSenderServiceAccount(String senderServiceAccount) {
        this.senderServiceAccount = senderServiceAccount;
    }

    public String getReceiverServiceAccount() {
        return receiverServiceAccount;
    }

    public void setReceiverServiceAccount(String receiverServiceAccount) {
        this.receiverServiceAccount = receiverServiceAccount;
    }

    public String getReceiverBank() {
        return receiverBank;
    }

    public void setReceiverBank(String receiverBank) {
        this.receiverBank = receiverBank;
    }

    public String getSenderBank() {
        return senderBank;
    }

    public void setSenderBank(String senderBank) {
        this.senderBank = senderBank;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public BigDecimal getSenderTax() {
        return senderTax;
    }

    public void setSenderTax(BigDecimal senderTax) {
        this.senderTax = senderTax;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getReceiverInfo() {
        return receiverInfo;
    }

    public void setReceiverInfo(String receiverInfo) {
        this.receiverInfo = receiverInfo;
    }

    public String getUpdatingDate() {
        return updatingDate;
    }

    public void setUpdatingDate(String updatingDate) {
        this.updatingDate = updatingDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    private int prepareAmountSign(MPAY_Transaction transaction, String sender) {
        if (transaction.getRefType().getCode().equals(CREDIT_TYPE_CODE))
            return getCreditSign(transaction, sender);
        else
            return getDebitSign(transaction, sender);
    }

    private int getDebitSign(MPAY_Transaction transaction, String sender) {
        if (sender.equals(getTransactionSender(transaction)))
            return 1;
        else
            return -1;
    }

    private int getCreditSign(MPAY_Transaction transaction, String sender) {
        if (sender.equals(getTransactionSender(transaction)))
            return -1;
        else
            return 1;
    }

    private String getTransactionSender(MPAY_Transaction transaction) {
        if (transaction.getSenderMobile() == null && transaction.getSenderService() == null)
            return transaction.getSenderInfo();
        else if (transaction.getSenderMobile() == null)
            return transaction.getSenderService().getName();
        else
            return transaction.getSenderMobile().getMobileNumber();
    }

    private String getTransactionReceiver(MPAY_Transaction transaction) {
        if (transaction.getReceiverMobile() == null && transaction.getReceiverService() == null)
            return transaction.getReceiverInfo();
        else if (transaction.getReceiverMobile() == null)
            return transaction.getReceiverService().getName();
        else
            return transaction.getReceiverMobile().getMobileNumber();
    }

    private void prepareReceiver(MPAY_Transaction transaction) {
        if (transaction.getReceiverMobile() != null) {
            this.receiver = transaction.getReceiverMobile().getMobileNumber();
            this.receiverMobileAccount = transaction.getReceiverMobileAccount().getRefAccount().getAccNumber();
        } else if (transaction.getReceiverService() != null) {
            this.receiver = transaction.getReceiverService().getName();//transaction.getReceiverService().getDescription() == null ? transaction.getReceiverService().getName() : transaction.getReceiverService().getDescription();
            this.receiverServiceAccount = transaction.getReceiverServiceAccount().getRefAccount().getAccNumber();
            this.serviceName = transaction.getReceiverService().getName();
        } else
            this.receiver = transaction.getReceiverInfo();
    }

    private void prepareSender(MPAY_Transaction transaction) {
        if (transaction.getSenderMobile() != null) {
            this.sender = transaction.getSenderMobile().getMobileNumber();
            this.senderMobileAccount = transaction.getSenderMobileAccount().getRefAccount().getAccNumber();
        } else if (transaction.getSenderService() != null) {
            this.sender = transaction.getSenderService().getName();//transaction.getSenderService().getDescription() == null ? transaction.getSenderService().getName() : transaction.getSenderService().getDescription();
            this.serviceName = transaction.getSenderService().getName();
            this.senderServiceAccount = transaction.getSenderServiceAccount().getRefAccount().getAccNumber();
        } else
            this.sender = transaction.getSenderInfo();
    }

    private void prepareAmounts(MPAY_Transaction transaction, String sender) {
        int amountSign = prepareAmountSign(transaction, sender);
        if (sender.equals(getTransactionSender(transaction))) {
            this.amount = transaction.getOriginalAmount().doubleValue();
            this.chargeAmount = transaction.getSenderCharge().doubleValue();
            this.totalAmount = transaction.getTotalAmount().doubleValue();
        } else if (sender.equals(getTransactionReceiver(transaction))) {
            this.amount = transaction.getOriginalAmount().doubleValue();
            this.totalAmount = transaction.getTotalAmount().doubleValue();
            this.chargeAmount = transaction.getReceiverCharge().doubleValue();
        } else {
            this.amount = transaction.getOriginalAmount().doubleValue();
            this.chargeAmount = transaction.getSenderCharge().add(transaction.getReceiverCharge()).doubleValue();
            this.totalAmount = transaction.getTotalAmount().doubleValue();
        }
        this.amount = amountSign * this.amount;
        if (chargeAmount > 0)
            this.chargeAmount = -1 * this.chargeAmount;
        this.totalAmount = amountSign * this.totalAmount;
        if (this.externalFees > 0)
            this.externalFees = -1 * transaction.getExternalFees().doubleValue();
    }
}
