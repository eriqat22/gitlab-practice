package com.progressoft.mpay.plugins.crm.corporateregistration.validator;

import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CorporateExistValidator {
	private static final Logger logger = LoggerFactory.getLogger(CorporateExistValidator.class);

	private CorporateExistValidator() {

	}

	public static ValidationResult validate(MPAY_Corporate corporate) {
		logger.debug("Inside Validate ...");
		if (corporate == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		return new ValidationResult(ReasonCodes.CORPORATE_IS_ALREADY_REGISTERD, null, false);
	}
}
