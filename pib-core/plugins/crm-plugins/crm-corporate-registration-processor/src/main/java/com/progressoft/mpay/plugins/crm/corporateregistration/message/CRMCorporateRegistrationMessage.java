package com.progressoft.mpay.plugins.crm.corporateregistration.message;

import com.progressoft.mpay.messages.MPayRequest;
import org.apache.commons.lang.NullArgumentException;

import java.math.BigDecimal;
import java.util.Date;

public class CRMCorporateRegistrationMessage extends MPayRequest {
	private String shortName;
	private String fullName;
	private String corporateType;
	private Date registrationDate;
	private String idType;
	private String registrationId;
	private String notificationsLanguage;
	private String city;
	private String country;
	private String typeOfSector;
	private String serviceName;
	private String serviceDescription;
	private String bankedUnBankedType;
	private String bank;
	private String walletType;
	private String transactionSize;
	private String branch;
	private String externalAccount;
	private String alias;
	private BigDecimal chargeAmountNumber;
	private String chargeAmountCharacter;
	private String chargeDuration;
	private String paymentType;
	private String srviceNotificationChannel;
	private String srviceNotificationRecevier;
	private String profile;
	private String serviceType;
	private String serviceCategory;
	private String expectedActivities;
	private String reference;
	private String registrationAuthority;
	private String registrationLocation;
	private String idInChamberOfECommerce;
	private String legalEntity;
	private String other;
	private String phone1;
	private String phone2;
	private String email;
	private String mobileNumber;
	private String poBox;
	private String zipCode;
	private String buildingNumber;
	private String streetName;
	private String location;
	private String consigneeArabicName;
	private String consigneeEnglishName;
	private String consigneeGender;
	private String consigneeNationality;
	private String consigneeOtherNationality;
	private String consigneePlaceOfBirth;
	private Date consigneeDateOfBirth;
	private String consigneeEmail;
	private String consigneeNationalId;
	private String consigneeIdReference;
	private String passportId;
	private String consigneePassportId;
	private Date passportIssuanceDate;
	private String idCard;
	private Date idCardIssuanceDate;
	private String address;
	private Date consigneePassportIssuanceDate;
	private String consigneeIdentificationCard;
	private Date consigneeIdentificationCardIssuanceDate;
	private String consigneeAddress;
	private String walletAccount;
	private Long maxNumberOfDevices ;
	private String iban;



	public CRMCorporateRegistrationMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
		setRequestedId(request.getRequestedId());
	}


	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getIban() {
		return iban;
	}

	public Long getMaxNumberOfDevices() {
		return maxNumberOfDevices;
	}

	public void setMaxNumberOfDevices(Long maxNumberOfDevices) {
		this.maxNumberOfDevices = maxNumberOfDevices;
	}
	public String getWalletAccount() {
		return walletAccount;
	}

	public void setWalletAccount(String walletAccount) {
		this.walletAccount = walletAccount;
	}

	public String getConsigneeAddress() {
		return consigneeAddress;
	}

	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}

	public Date getConsigneeIdentificationCardIssuanceDate() {
		return consigneeIdentificationCardIssuanceDate;
	}

	public void setConsigneeIdentificationCardIssuanceDate(Date consigneeIdentificationCardIssuanceDate) {
		this.consigneeIdentificationCardIssuanceDate = consigneeIdentificationCardIssuanceDate;
	}

	public String getConsigneeIdentificationCard() {
		return consigneeIdentificationCard;
	}

	public void setConsigneeIdentificationCard(String consigneeIdentificationCard) {
		this.consigneeIdentificationCard = consigneeIdentificationCard;
	}

	public Date getConsigneePassportIssuanceDate() {
		return consigneePassportIssuanceDate;
	}

	public void setConsigneePassportIssuanceDate(Date consigneePassportIssuanceDate) {
		this.consigneePassportIssuanceDate = consigneePassportIssuanceDate;
	}

	public String getConsigneePassportId() {
		return consigneePassportId;
	}

	public void setConsigneePassportId(String consigneePassportId) {
		this.consigneePassportId = consigneePassportId;
	}

	public Date getConsigneeDateOfBirth() {
		return consigneeDateOfBirth;
	}

	public void setConsigneeDateOfBirth(Date consigneeDateOfBirth) {
		this.consigneeDateOfBirth = consigneeDateOfBirth;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getTransactionSize() {
		return transactionSize;
	}

	public void setTransactionSize(String transactionSize) {
		this.transactionSize = transactionSize;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getExternalAccount() {
		return externalAccount;
	}

	public void setExternalAccount(String externalAccount) {
		this.externalAccount = externalAccount;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public BigDecimal getChargeAmountNumber() {
		return chargeAmountNumber;
	}

	public void setChargeAmountNumber(BigDecimal chargeAmountNumber) {
		this.chargeAmountNumber = chargeAmountNumber;
	}

	public String getChargeAmountCharacter() {
		return chargeAmountCharacter;
	}

	public void setChargeAmountCharacter(String chargeAmountCharacter) {
		this.chargeAmountCharacter = chargeAmountCharacter;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getIdCardIssuanceDate() {
		return idCardIssuanceDate;
	}

	public void setIdCardIssuanceDate(Date idCardIssuanceDate) {
		this.idCardIssuanceDate = idCardIssuanceDate;
	}

	public Date getPassportIssuanceDate() {
		return passportIssuanceDate;
	}

	public void setPassportIssuanceDate(Date passportIssuanceDate) {
		this.passportIssuanceDate = passportIssuanceDate;
	}

	public String getPassportId() {
		return passportId;
	}

	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}

	public String getConsigneeIdReference() {
		return consigneeIdReference;
	}

	public void setConsigneeIdReference(String consigneeIdReference) {
		this.consigneeIdReference = consigneeIdReference;
	}

	public String getConsigneeNationalId() {
		return consigneeNationalId;
	}

	public void setConsigneeNationalId(String consigneeNationalId) {
		this.consigneeNationalId = consigneeNationalId;
	}

	public String getConsigneeEmail() {
		return consigneeEmail;
	}

	public void setConsigneeEmail(String consigneeEmail) {
		this.consigneeEmail = consigneeEmail;
	}

	public String getConsigneePlaceOfBirth() {
		return consigneePlaceOfBirth;
	}

	public void setConsigneePlaceOfBirth(String consigneePlaceOfBirth) {
		this.consigneePlaceOfBirth = consigneePlaceOfBirth;
	}

	public String getConsigneeOtherNationality() {
		return consigneeOtherNationality;
	}

	public void setConsigneeOtherNationality(String consigneeotherNationality) {
		this.consigneeOtherNationality = consigneeotherNationality;
	}

	public String getConsigneeNationality() {
		return consigneeNationality;
	}

	public void setConsigneeNationality(String consigneeNationality) {
		this.consigneeNationality = consigneeNationality;
	}

	public String getConsigneeGender() {
		return consigneeGender;
	}

	public void setConsigneeGender(String consigneeGender) {
		this.consigneeGender = consigneeGender;
	}

	public String getConsigneeEnglishName() {
		return consigneeEnglishName;
	}

	public void setConsigneeEnglishName(String consigneeEnglishName) {
		this.consigneeEnglishName = consigneeEnglishName;
	}

	public String getConsigneeArabicName() {
		return consigneeArabicName;
	}

	public void setConsigneeArabicName(String consigneeArabicName) {
		this.consigneeArabicName = consigneeArabicName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPoBox() {
		return poBox;
	}

	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getLegalEntity() {
		return legalEntity;
	}

	public void setLegalEntity(String legalEntity) {
		this.legalEntity = legalEntity;
	}

	public String getIdInChamberOfECommerce() {
		return idInChamberOfECommerce;
	}

	public void setIdInChamberOfECommerce(String idInChamberOfECommerce) {
		this.idInChamberOfECommerce = idInChamberOfECommerce;
	}

	public String getRegistrationLocation() {
		return registrationLocation;
	}

	public void setRegistrationLocation(String registrationLocation) {
		this.registrationLocation = registrationLocation;
	}

	public String getRegistrationAuthority() {
		return registrationAuthority;
	}

	public void setRegistrationAuthority(String registrationAuthority) {
		this.registrationAuthority = registrationAuthority;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getExpectedActivities() {
		return expectedActivities;
	}

	public void setExpectedActivities(String expectedActivities) {
		this.expectedActivities = expectedActivities;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCorporateType() {
		return corporateType;
	}

	public void setCorporateType(String corporateType) {
		this.corporateType = corporateType;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getNotificationsLanguage() {
		return notificationsLanguage;
	}

	public void setNotificationsLanguage(String notificationsLanguage) {
		this.notificationsLanguage = notificationsLanguage;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTypeOfSector() {
		return typeOfSector;
	}

	public void setTypeOfSector(String typeOfSelector) {
		this.typeOfSector = typeOfSelector;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public String getBankedUnBankedType() {
		return bankedUnBankedType;
	}

	public void setBankedUnBankedType(String bankedUnBankedType) {
		this.bankedUnBankedType = bankedUnBankedType;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getWalletType() {
		return walletType;
	}

	public void setWalletType(String walletType) {
		this.walletType = walletType;
	}

	public String getChargeDuration() {
		return chargeDuration;
	}

	public void setChargeDuration(String chargeDuration) {
		this.chargeDuration = chargeDuration;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getSrviceNotificationChannel() {
		return srviceNotificationChannel;
	}

	public void setServiceNotificationChannel(String srviceNotificationChannel) {
		this.srviceNotificationChannel = srviceNotificationChannel;
	}

	public String getSrviceNotificationRecevier() {
		return srviceNotificationRecevier;
	}

	public void setSrviceNotificationRecevier(String srviceNotificationRecevier) {
		this.srviceNotificationRecevier = srviceNotificationRecevier;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

}
