package com.progressoft.mpay.plugins.crm.corporateregistration.message.parse;

import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.plugins.crm.corporateregistration.message.CRMCorporateRegistrationMessage;
import com.progressoft.mpay.plugins.crm.corporateregistration.message.metadata.CRMCorporateRegistrationMessageMetaData;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

public class CRMCorporateRegistrationParseMessage {

    private static final Logger logger = LoggerFactory.getLogger(CRMCorporateRegistrationParseMessage.class);

    public static CRMCorporateRegistrationMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CRMCorporateRegistrationMessage message = new CRMCorporateRegistrationMessage(request);
        fillGenericFields(message);
        fillCorporateInfo(message);
        return message;
    }

    private static void fillCorporateInfo(CRMCorporateRegistrationMessage message) throws MessageParsingException {

        message.setShortName(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SHORT_NAME.getValue()));
        if (message.getShortName() == null || message.getShortName().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SHORT_NAME.getValue());

        message.setFullName(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.FULL_NAME.getValue()));
        if (message.getFullName() == null || message.getFullName().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.FULL_NAME.getValue());

        message.setExpectedActivities(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.EXPECTED_ACTIVITIES.getValue()));
        message.setTypeOfSector(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.TYPE_OF_SECTOR.getValue()));
        if (StringUtils.isEmpty(message.getTypeOfSector())
                || (!message.getTypeOfSector().equals(CRMCorporateRegistrationMessageMetaData.CorporateSectorType.PRIVATE.getValue()) && !message.getTypeOfSector().equals(CRMCorporateRegistrationMessageMetaData.CorporateSectorType.PUBLIC.getValue())))
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.TYPE_OF_SECTOR.getValue());
        message.setCorporateType(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CORPORATE_TYPE.getValue()));
        if (StringUtils.isEmpty(message.getCorporateType()))
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CORPORATE_TYPE.getValue());
        message.setReference(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.REFERENCE.getValue()));
        message.setRegistrationAuthority(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.REGISTRATION_AUTHORITY.getValue()));
        message.setRegistrationLocation(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.REGISTRATION_LOCATION.getValue()));
        SimpleDateFormat format = new SimpleDateFormat(CRMCorporateRegistrationMessageMetaData.DATE_FORMAT);
        checkRegistrationDate(message, format);
        message.setIdType(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.ID_TYPE.getValue()));
        if (StringUtils.isEmpty(message.getIdType()))
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.ID_TYPE.getValue());
        message.setRegistrationId(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.REGISTRATION_ID.getValue()));
        if (StringUtils.isEmpty(message.getRegistrationId()))
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.REGISTRATION_ID.getValue());
        message.setIdInChamberOfECommerce(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.ID_IN_Chamber_OF_ECOMMERCE.getValue()));
        message.setNotificationsLanguage(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.NOTIFICATIONS_LANGUAGE.getValue()));
        if (StringUtils.isEmpty(message.getNotificationsLanguage())
                || (!message.getNotificationsLanguage().equals(CRMCorporateRegistrationMessageMetaData.NotificationLangauage.ARABIC.getValue()) && !message.getNotificationsLanguage().equals(CRMCorporateRegistrationMessageMetaData.NotificationLangauage.ENGLISH.getValue())))
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.NOTIFICATIONS_LANGUAGE.getValue());
        message.setLegalEntity(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.LEGAL_ENTITY.getValue()));
        message.setOther(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.OTHER.getValue()));
        message.setPhone1(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.PHONE1.getValue()));
        message.setPhone2(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.PHONE2.getValue()));
        message.setEmail(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.EMAIL.getValue()));
        message.setMobileNumber(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.MOBILENUMBER.getValue()));
        message.setPoBox(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.POBOX.getValue()));
        message.setZipCode(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.ZIPCODE.getValue()));
        message.setBuildingNumber(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BUILDING_NUMBER.getValue()));
        message.setStreetName(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.STREET_NAME.getValue()));
        message.setLocation(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.LOCATION.getValue()));
        message.setCountry(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.COUNTRY.getValue()));
        if (message.getCountry() == null || message.getCountry().trim().length() == 0)
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.COUNTRY.getValue());
        message.setCity(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CITY.getValue()));
        if (message.getCity() == null || message.getCity().trim().length() == 0)
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CITY.getValue());
        message.setConsigneeArabicName(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_ARABIC_NAME.getValue()));
        message.setConsigneeEnglishName(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_ENGLISH_NAME.getValue()));

        message.setConsigneeGender(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_GENDER.getValue()));

        String consigneeGender = message.getConsigneeGender();
        if (consigneeGender != null && !consigneeGender.isEmpty())
            if (!consigneeGender.equals(CRMCorporateRegistrationMessageMetaData.CorporateGender.MALE.getValue())
                    && !consigneeGender.equals(CRMCorporateRegistrationMessageMetaData.CorporateGender.FEMALE.getValue()))
                throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_GENDER.getValue());

        message.setConsigneeNationality(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_NATIONALITY.getValue()));
        message.setConsigneeOtherNationality(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_OTHER_NATIONALITY.getValue()));
        message.setConsigneePlaceOfBirth(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_PLACE_OF_BIRTH.getValue()));
        checkConsigneeDateOfBirth(message, format);
        message.setConsigneeEmail(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_EMAIL.getValue()));
        message.setConsigneeNationalId(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_NATIONAL_ID.getValue()));
        message.setConsigneeIdReference(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_ID_REFERENCE.getValue()));
        message.setConsigneePassportId(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_PASSPORT_ID.getValue()));
        checkConsigneePassportIssuanceDate(message, format);
        message.setConsigneeIdentificationCard(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_IDENTIFICATION_CARD.getValue()));
        checkConsigneeIdentificationCardIssuanceDate(message, format);
        message.setConsigneeAddress(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_ADDRESS.getValue()));
        message.setServiceName(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_NAME.getValue()));
        if (message.getServiceName() == null || message.getServiceName().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_NAME.getValue());
        message.setServiceDescription(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_DESCRIPTION.getValue()));
        if (message.getServiceDescription() == null || message.getServiceDescription().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_DESCRIPTION.getValue());

        message.setAlias(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.ALIAS.getValue()));

        if (!message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BANKED_UN_BANKED_TYPE.getValue()).equals("1")
                && !message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BANKED_UN_BANKED_TYPE.getValue()).equals("2"))
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BANKED_UN_BANKED_TYPE.getValue());
        message.setBankedUnBankedType(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BANKED_UN_BANKED_TYPE.getValue()));

        if (message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BANK.getValue()) == null || message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BANK.getValue()).isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BANK.getValue());
        message.setBank(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BANK.getValue()));

        message.setBranch(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.BRANCH.getValue()));
        message.setExternalAccount(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.EXTERNAL_ACCOUNT.getValue()));

        message.setWalletType(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.WALLET_TYPE.getValue()));
        if (!message.getWalletType().equals(CRMCorporateRegistrationMessageMetaData.CorporateWalletType.INDIVIDUAL.getValue())
                && !message.getWalletType().equals(CRMCorporateRegistrationMessageMetaData.CorporateWalletType.CORPORATE.getValue())) {
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.WALLET_TYPE.getValue());
        }

        String transactionSize = message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.TRANSACTION_SIZE.getValue());
        if (transactionSize != null && !transactionSize.isEmpty()) {
            if (!transactionSize.equals(CRMCorporateRegistrationMessageMetaData.TransactionsSize.SMALL.getValue()) && !transactionSize.equals(CRMCorporateRegistrationMessageMetaData.TransactionsSize.MEDIUM.getValue()) && !transactionSize.equals(CRMCorporateRegistrationMessageMetaData.TransactionsSize.LARG.getValue()))
                throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.TRANSACTION_SIZE.getValue());
            message.setTransactionSize(transactionSize);
        }
        checkChargeAmountNumber(message);
        message.setChargeAmountCharacter(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CHARGE_AMOUNT_CHARACTERS.getValue()));
        message.setWalletAccount(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.WALLET_ACCOUNT.getValue()));

        message.setChargeDuration(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CHARGE_DURATION.getValue()));
        if (!CRMCorporateRegistrationMessageMetaData.CorporateChargeDuration.MONTHLY.getValue().equals(message.getChargeDuration())
                && !CRMCorporateRegistrationMessageMetaData.CorporateChargeDuration.ONCE.getValue().equals(message.getChargeDuration()))
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CHARGE_DURATION.getValue());

        message.setPaymentType(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.PAYMENT_TYPE.getValue()));
        if (message.getPaymentType() == null || message.getPaymentType().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.PAYMENT_TYPE.getValue());

        message.setServiceNotificationChannel(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_NOTIFICATION_CHANNEL.getValue()));

        String srviceNotificationChannel = message.getSrviceNotificationChannel();
        if (!srviceNotificationChannel.equals(CRMCorporateRegistrationMessageMetaData.CorporateServiceNotificationChannel.EMAIL.getValue()) && !srviceNotificationChannel.equals(CRMCorporateRegistrationMessageMetaData.CorporateServiceNotificationChannel.SMS.getValue())
                && !srviceNotificationChannel.equals(CRMCorporateRegistrationMessageMetaData.CorporateServiceNotificationChannel.PUSH_NOTIFICATION.getValue())) {
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_NOTIFICATION_CHANNEL.getValue());

        } else {
            message.setSrviceNotificationRecevier(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SRVICE_NOTIFICATION_RECEVIER.getValue()));
            validateServiceNotificationReciver(message);
            if (srviceNotificationChannel.equals(CRMCorporateRegistrationMessageMetaData.CorporateServiceNotificationChannel.EMAIL.getValue()))
                checkServiceNotificationReciverRegex(message, SystemParameters.getInstance().getEmailRegex(), " invalid email regex");

            else if (srviceNotificationChannel.equals(CRMCorporateRegistrationMessageMetaData.CorporateServiceNotificationChannel.SMS.getValue()))
                checkServiceNotificationReciverRegex(message, SystemParameters.getInstance().getMobileNumberRegex(), " invalid mobile regex");

        }

        message.setProfile(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.PROFILE.getValue()));
        if (message.getProfile() == null || message.getProfile().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.PROFILE.getValue());

        message.setServiceCategory(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_CATEGORY.getValue()));
        if (message.getServiceCategory() == null || message.getServiceCategory().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_CATEGORY.getValue());

        message.setServiceType(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_TYPE.getValue()));
        if (message.getServiceType() == null || message.getServiceType().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SERVICE_TYPE.getValue());

        message.setMaxNumberOfDevices(Long.valueOf(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.MAX_NUMBER_OF_DEVICES.getValue())));
        message.setIban(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.IBAN.getValue()));
    }

    private static void checkServiceNotificationReciverRegex(CRMCorporateRegistrationMessage message, String regex, String msg) throws MessageParsingException {
        try {
            if (!message.getSrviceNotificationRecevier().matches(regex))
                throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SRVICE_NOTIFICATION_RECEVIER.getValue() + msg);
        } catch (Exception e) {
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SRVICE_NOTIFICATION_RECEVIER.getValue() + msg);
        }
    }

    private static void validateServiceNotificationReciver(CRMCorporateRegistrationMessage message) throws MessageParsingException {
        if (message.getSrviceNotificationRecevier() == null || message.getSrviceNotificationRecevier().isEmpty())
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.SRVICE_NOTIFICATION_RECEVIER.getValue());
    }

    private static void checkChargeAmountNumber(CRMCorporateRegistrationMessage message) throws MessageParsingException {
        String chargeAmount = message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CHARGE_AMOUNT_NUMBER.getValue());
        if (chargeAmount != null && !chargeAmount.isEmpty()) {
            try {
                message.setChargeAmountNumber(BigDecimal.valueOf(Double.parseDouble(chargeAmount)));
            } catch (Exception e) {
                throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CHARGE_AMOUNT_NUMBER.getValue());
            }
        }
    }

    private static void checkConsigneeIdentificationCardIssuanceDate(CRMCorporateRegistrationMessage message, SimpleDateFormat format) throws MessageParsingException {
        if (message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_IDENTIFICATION_CARD_ISSUANCE_DATE.getValue()) == null)
            return;
        try {
            message.setConsigneeIdentificationCardIssuanceDate(format.parse(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_IDENTIFICATION_CARD_ISSUANCE_DATE.getValue())));
        } catch (Exception e) {
            logger.debug("Invalid  Consignee Identification Card Issuance Date format", e);
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_IDENTIFICATION_CARD_ISSUANCE_DATE.getValue());
        }
    }

    private static void checkRegistrationDate(CRMCorporateRegistrationMessage message, SimpleDateFormat format) throws MessageParsingException {
        try {
            message.setRegistrationDate(format.parse(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.REGISTRATION_DATE.getValue())));
        } catch (Exception e) {
            logger.debug("Invalid Date of Birth format", e);
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.REGISTRATION_DATE.getValue());
        }
    }

    private static void checkConsigneePassportIssuanceDate(CRMCorporateRegistrationMessage message, SimpleDateFormat format) throws MessageParsingException {
        if (message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_PASSPORT_ISSUANCE_DATE.getValue()) == null)
            return;
        try {
            message.setConsigneePassportIssuanceDate(format.parse(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_PASSPORT_ISSUANCE_DATE.getValue())));
        } catch (Exception e) {
            logger.debug("Invalid  Consignee Passport Issuance Date format", e);
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_PASSPORT_ISSUANCE_DATE.getValue());
        }
    }

    private static void checkConsigneeDateOfBirth(CRMCorporateRegistrationMessage message, SimpleDateFormat format) throws MessageParsingException {
        if (message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_DATE_OF_BIRTH.getValue()) == null)
            return;
        try {
            message.setConsigneeDateOfBirth(format.parse(message.getValue(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.CONSIGNEE_DATE_OF_BIRTH.getValue())));
        } catch (Exception e) {
            logger.debug("Invalid Date of Birth format", e);
            throw new MessageParsingException(CRMCorporateRegistrationMessageMetaData.CorporateRegistrationMessageExtraData.REGISTRATION_DATE.getValue());
        }
    }

    private static void fillGenericFields(CRMCorporateRegistrationMessage message) throws MessageParsingException {
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
    }
}
