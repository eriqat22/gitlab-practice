package com.progressoft.mpay.plugins.crm.corporateregistration.validator;

import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaymentTypeValidator {
	private static final Logger logger = LoggerFactory.getLogger(PaymentTypeValidator.class);

	private PaymentTypeValidator() {

	}

	public static ValidationResult validate(MPAY_MessageType messageType) {
		logger.debug("Inside Validate ...");
		if (messageType == null)
			return new ValidationResult(ReasonCodes.PAYMENT_TYPE_NOT_SUPPORTED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

}
