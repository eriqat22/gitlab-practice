package com.progressoft.mpay.plugins.crm.corporateregistration.constant;

public class Constants {
	private Constants() {

	}

	public static final String USER = "user";
	public static final String ID_TYPE="idType";
	public static final String COUNTRY="country";
	public static final String CITY="city";
	public static final String LANGUAGE="lang";
	public static final String PROFILE_KEY="profileKey";
	public static final String TRANSACTION_SIZE="transactionSize";
	public static final String CORPORATE_TYPES = "corporatesType";
	public static final String BANK = "bank";
	public static final String MESSAGE_TYPE = "messageType";
	public static final String NOTIFICATION_CHANNEL = "notificationChannel";
	public static final String SERVICE_TYPE = "serviceType";
	public static final String SERVICES_CATEGRORY = "servicesCategorie";
	public static final String CORP_LEGAL_ENTITY = "corpLegalEntity";
	public static final String CONSIGNEE_NATIONALITY = "consigneeNationality";
	public static final String IS_CORPORATE_ALREADY_EXIST = "isCorporateDublicate";
	public static final String IS_CORPORATE_SERVICE_ALREADY_EXIST = "isCorporateService";
}
