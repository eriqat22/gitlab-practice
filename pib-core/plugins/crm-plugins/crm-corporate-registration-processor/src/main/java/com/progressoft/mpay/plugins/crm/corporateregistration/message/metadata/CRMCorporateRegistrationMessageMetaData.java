package com.progressoft.mpay.plugins.crm.corporateregistration.message.metadata;

public class CRMCorporateRegistrationMessageMetaData {

    public static final String DATE_FORMAT = "dd/MM/yyyy";

    public enum CorporateRegistrationMessageExtraData {
        SHORT_NAME("shortName"), FULL_NAME("fullName"), CORPORATE_TYPE("corporateType"), REGISTRATION_DATE("registrationDate"), ID_TYPE("idType"), REGISTRATION_ID("registrationId"),

        NOTIFICATIONS_LANGUAGE("notificationsLang"), CITY("city"), COUNTRY("country"), TYPE_OF_SECTOR("typeOfSector"), SERVICE_NAME("serviceName"), SERVICE_DESCRIPTION("serviceDescription"),

        BANKED_UN_BANKED_TYPE("bankedUnBankedType"), BANK("bank"), WALLET_TYPE("walletType"), CHARGE_DURATION("chargeDuration"), PAYMENT_TYPE("paymentType"),

        SERVICE_NOTIFICATION_CHANNEL("srviceNotificationChannel"), SRVICE_NOTIFICATION_RECEVIER("srviceNotificationRecevier"), PROFILE("profile"), SERVICE_TYPE("serviceType"),

        USERNAME("userName"), PASSWORD("password"), EXPECTED_ACTIVITIES("expectedActivities"), REFERENCE("reference"), REGISTRATION_AUTHORITY("registrationAuthority"),

        REGISTRATION_LOCATION("registrationLocation"), ID_IN_Chamber_OF_ECOMMERCE("idInChamberOfECommerce"), LEGAL_ENTITY("legalEntity"), OTHER("other"), PHONE1("phone1"),

        PHONE2("phone2"), EMAIL("email"), MOBILENUMBER("mobileNumber"), POBOX("poBox"), ZIPCODE("zipCode"), BUILDING_NUMBER("buildingNumber"), STREET_NAME("streetName"),

        LOCATION("location"), CONSIGNEE_ARABIC_NAME("consigneeArabicName"), CONSIGNEE_ENGLISH_NAME("consigneeEnglishName"), CONSIGNEE_GENDER("consigneeGender"),

        CONSIGNEE_NATIONALITY("consigneeNationality"), CONSIGNEE_OTHER_NATIONALITY("consigneeOtherNationality"), CONSIGNEE_PLACE_OF_BIRTH("consigneePlaceOfBirth"),

        CONSIGNEE_DATE_OF_BIRTH("consigneeDateOfBirth"), CONSIGNEE_EMAIL("consigneeEmail"), CONSIGNEE_NATIONAL_ID("consigneeNationalId"),

        CONSIGNEE_ID_REFERENCE("consigneeIdReference"), PASSPORT_ID("passportId"), PASSPORT_ISSUANCE_DATE("passportIssuanceDate"),

        ID_CARD("idCard"), ID_CARD_ISSUANCE_DATE("idCardIssuanceDate"), ADDRESS("address"), CONSIGNEE_PASSPORT_ID("consigneePassportId"),

        CONSIGNEE_PASSPORT_ISSUANCE_DATE("consigneePassportIssuanceDate"), CONSIGNEE_IDENTIFICATION_CARD("consigneeIdentificationCard"),

        CONSIGNEE_IDENTIFICATION_CARD_ISSUANCE_DATE("consigneeIdentificationCardIssuanceDate"), CONSIGNEE_ADDRESS("consigneeAddress"), ALIAS("alias"), BRANCH("branch"),

        EXTERNAL_ACCOUNT("externalAccount"), TRANSACTION_SIZE("transactionSize"), CHARGE_AMOUNT_NUMBER("chargeAmountNumber"),

        CHARGE_AMOUNT_CHARACTERS("chargeAmountCharacters"), WALLET_ACCOUNT("walletAccount"), SERVICE_CATEGORY("serviceCategory"),

        MAX_NUMBER_OF_DEVICES("maxNumberOfDevices"), IBAN("iban");

        private String value;

        CorporateRegistrationMessageExtraData(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public enum CorporateChargeDuration {

        ONCE("1"), MONTHLY("2");
        private String value;

        CorporateChargeDuration(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum CorporateSectorType {
        PRIVATE("1"), PUBLIC("2");

        private String value;

        CorporateSectorType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    public enum TransactionsSize {
        SMALL("1"), MEDIUM("2"), LARG("3");

        private String value;

        TransactionsSize(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public enum NotificationLangauage {
        ENGLISH("1"), ARABIC("3");

        private String value;

        NotificationLangauage(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    public enum CorporateWalletType {

        INDIVIDUAL("1"), CORPORATE("2");

        private String value;

        CorporateWalletType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum CorporateGender {

        MALE("1"), FEMALE("2");

        private String value;

        CorporateGender(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum CorporateServiceNotificationChannel {

        SMS("1"), EMAIL("2"), PUSH_NOTIFICATION("3");

        private String value;

        CorporateServiceNotificationChannel(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
