package com.progressoft.mpay.plugins.crm.corporateregistration.processor;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.crm.corporateregistration.constant.Constants;
import com.progressoft.mpay.plugins.crm.corporateregistration.message.CRMCorporateRegistrationMessage;
import com.progressoft.mpay.plugins.crm.corporateregistration.message.parse.CRMCorporateRegistrationParseMessage;
import com.progressoft.mpay.plugins.crm.corporateregistration.validator.CorporateRegistrationValidator;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.registration.corporates.CorporateWorkflowStatuses;
import com.progressoft.mpay.registration.corporates.PostApproveCorporate;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CRMCorporateRegistrationProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(CRMCorporateRegistrationProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ....");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = CorporateRegistrationValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
                    e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside processIntegration ....");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside reverse ....");
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        logger.debug("Inside accept ....");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        logger.debug("Inside reject ....");
        return null;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        CRMCorporateRegistrationMessage message = CRMCorporateRegistrationParseMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);

        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), ReceiverInfoType.CORPORATE));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());


        context.getExtraData().put(Constants.IS_CORPORATE_ALREADY_EXIST, context.getDataProvider().getCorporateByIdTypeAndRegistrationId(message.getIdType(), message.getRegistrationId()));
        context.getExtraData().put(Constants.IS_CORPORATE_SERVICE_ALREADY_EXIST, context.getDataProvider().getCorporateService(message.getServiceName(), ReceiverInfoType.CORPORATE));

        MPAY_Bank bank = loadBank(context);
        if (bank == null)
            return;
        sender.setBank(bank);
        loadLookups(context, message);
    }

    private MPAY_Bank loadBank(ProcessingContext context) {
        return context.getLookupsLoader().getBankByCode(context.getSystemParameters().getRegistrationBankCode());
    }

    private void loadLookups(MessageProcessingContext context, CRMCorporateRegistrationMessage message) {

        MPAY_IDType idType = context.getLookupsLoader().getIDType(message.getIdType());
        if (idType == null)
            return;
        context.getExtraData().put(Constants.ID_TYPE, idType);

        JfwCountry country = context.getLookupsLoader().getCountry(message.getCountry());
        if (country == null)
            return;
        context.getExtraData().put(Constants.COUNTRY, country);

        JfwCountry nationality = context.getLookupsLoader().getCountry(message.getConsigneeNationality());
        if (nationality != null)
            context.getExtraData().put(Constants.CONSIGNEE_NATIONALITY, nationality);

        MPAY_City city = context.getLookupsLoader().getCity(message.getCity());
        if (city == null)
            return;
        context.getExtraData().put(Constants.CITY, city);

        MPAY_Language language = context.getLookupsLoader()
                .getLanguageByCode(getNotificationsLanguage(message));
        if (language == null)
            return;
        context.getExtraData().put(Constants.LANGUAGE, language);

        if (message.getProfile() == null) {

            MPAY_Profile mpayProfile = context.getLookupsLoader()
                    .getProfile(context.getSystemParameters().getRegistrationProfileCode());
            if (mpayProfile == null)
                return;
            context.getExtraData().put(Constants.PROFILE_KEY, mpayProfile);
        } else {
            MPAY_Profile mpayProfile = context.getLookupsLoader().getProfile(message.getProfile());
            if (mpayProfile == null)
                return;
            context.getExtraData().put(Constants.PROFILE_KEY, mpayProfile);
        }

        MPAY_TransactionSize transactionSize = context.getLookupsLoader()
                .getTransactionSizeByCode(message.getTransactionSize());
        if (transactionSize != null)
            context.getExtraData().put(Constants.TRANSACTION_SIZE, transactionSize);

        List<String> corporatesCode = Arrays.asList(context.getLookupsLoader()
                .getSystemConfigurations(SysConfigKeys.EXPOSED_CORPORATES_CODES).getConfigValue().split(";"));
        if (!corporatesCode.contains(message.getCorporateType()))
            return;

        context.getExtraData().put(Constants.CORPORATE_TYPES,
                context.getLookupsLoader().getClientType(message.getCorporateType()));

        MPAY_Bank bankByCode = context.getLookupsLoader().getBankByCode(message.getBank());
        if (bankByCode == null)
            return;
        context.getExtraData().put(Constants.BANK, bankByCode);

        MPAY_MessageType messageTypeByCode = context.getLookupsLoader().getMessageTypes(message.getPaymentType());
        if (messageTypeByCode == null)
            return;
        context.getExtraData().put(Constants.MESSAGE_TYPE, messageTypeByCode);

        MPAY_NotificationChannel channel = context.getLookupsLoader()
                .getNotificationChannel(message.getSrviceNotificationChannel());
        if (channel == null)
            return;
        context.getExtraData().put(Constants.NOTIFICATION_CHANNEL, channel);

        MPAY_ServiceType serviceType = context.getLookupsLoader().getServiceType(message.getServiceType());
        if (serviceType == null)
            return;
        context.getExtraData().put(Constants.SERVICE_TYPE, serviceType);

        MPAY_ServicesCategory servicesCategory = context.getLookupsLoader()
                .getServiceCategory(message.getServiceCategory());
        if (servicesCategory == null)
            return;
        context.getExtraData().put(Constants.SERVICES_CATEGRORY, servicesCategory);

        MPAY_CorpLegalEntity corpLegalEntity = context.getLookupsLoader()
                .getLegalEntityByCode(message.getLegalEntity());
        if (corpLegalEntity == null)
            return;
        context.getExtraData().put(Constants.CORP_LEGAL_ENTITY, corpLegalEntity);

    }

    private String getNotificationsLanguage(CRMCorporateRegistrationMessage message) {
        return message.getNotificationsLanguage().equals("1") ? "en" : "ar";
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatus,
                                                  String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatus);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws InvalidActionException {
        logger.debug("Inside AcceptMessage ...");
        MPAY_Corporate corporate = getCorporateFromRequest(context);
        registerCorporate(corporate, context);
        MPAY_CorpoarteService service = corporate.getRefCorporateCorpoarteServices().get(0);
        if (service != null)
            service.setCity(corporate.getCity());
        context.getDataProvider().updateCorporateService(service);
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private MPAY_Corporate getCorporateFromRequest(MessageProcessingContext context) {
        CRMCorporateRegistrationMessage message = (CRMCorporateRegistrationMessage) context.getRequest();
        MPAY_Corporate corporate = new MPAY_Corporate();
        corporate.setMas("1");
        corporate.setHint(String.valueOf(context.getMessage().getId()));
        corporate.setName(message.getShortName());
        corporate.setDescription(message.getFullName());
        corporate.setExpectedActivities(message.getExpectedActivities());
        corporate.setSectorType(message.getTypeOfSector());
        corporate.setClientType((MPAY_ClientType) context.getExtraData().get(Constants.CORPORATE_TYPES));
        corporate.setClientRef(message.getReference());
        corporate.setRegistrationAuthority(message.getRegistrationAuthority());
        corporate.setRegistrationLocation(message.getRegistrationLocation());
        corporate.setRegistrationDate(message.getRegistrationDate());
        corporate.setIdType((MPAY_IDType) context.getExtraData().get(Constants.ID_TYPE));
        corporate.setRegistrationId(message.getRegistrationId());
        corporate.setChamberId(message.getIdInChamberOfECommerce());
        corporate.setIsRegistered(false);
        corporate.setIsActive(true);
        corporate.setPrefLang((MPAY_Language) context.getExtraData().get(Constants.LANGUAGE));
        corporate.setLegalEntity((MPAY_CorpLegalEntity) context.getExtraData().get(Constants.CORP_LEGAL_ENTITY));
        corporate.setOtherLegalEntity(message.getOther());
        corporate.setPhoneOne(message.getPhone1());
        corporate.setPhoneTwo(message.getPhone2());
        corporate.setEmail(message.getEmail());
        corporate.setMobileNumber(message.getMobileNumber());
        corporate.setPobox(message.getPoBox());
        corporate.setZipCode(message.getZipCode());
        corporate.setBuildingNum(message.getBuildingNumber());
        corporate.setStreetName(message.getStreetName());
        corporate.setLocation(message.getLocation());
        corporate.setCity((MPAY_City) context.getExtraData().get(Constants.CITY));
        corporate.setRefCountry((JfwCountry) context.getExtraData().get(Constants.COUNTRY));
        corporate.setConsigneeArabicName(message.getConsigneeArabicName());
        corporate.setConsigneeEnglishName(message.getConsigneeEnglishName());
        corporate.setGender(message.getConsigneeGender());
        corporate.setNationality((JfwCountry) context.getExtraData().get(Constants.CONSIGNEE_NATIONALITY));
        corporate.setOtherNationality(message.getConsigneeOtherNationality());
        corporate.setPlaceOfBirth(message.getConsigneePlaceOfBirth());
        corporate.setDateOfBirth(message.getConsigneeDateOfBirth());
        corporate.setEmail(message.getConsigneeEmail());
        corporate.setNationalID(message.getConsigneeNationalId());
        corporate.setIdentificationReference(message.getConsigneeIdReference());
        corporate.setPassportID(message.getConsigneePassportId());
        corporate.setPassportIssuanceDate(message.getConsigneePassportIssuanceDate());
        corporate.setIdentificationCard(message.getConsigneeIdentificationCard());
        corporate.setIdentificationCardIssuanceDate(message.getConsigneeIdentificationCardIssuanceDate());
        corporate.setAddress(message.getConsigneeAddress());
        corporate.setServiceName(message.getServiceName());
        corporate.setServiceDescription(message.getServiceDescription());
        corporate.setAlias(message.getAlias());
        corporate.setBankedUnbanked(message.getBankedUnBankedType());
        corporate.setBank((MPAY_Bank) context.getExtraData().get(Constants.BANK));
        corporate.setBranch(message.getBranch());
        corporate.setExternalAcc(message.getExternalAccount());
        corporate.setPaymentType((MPAY_MessageType) context.getExtraData().get(Constants.MESSAGE_TYPE));
        corporate.setRefProfile((MPAY_Profile) context.getExtraData().get(Constants.PROFILE_KEY));
        corporate.setServiceCategory((MPAY_ServicesCategory) context.getExtraData().get(Constants.SERVICES_CATEGRORY));
        corporate.setServiceType((MPAY_ServiceType) context.getExtraData().get(Constants.SERVICE_TYPE));
        corporate.setMaxNumberOfDevices(0L);
        corporate.setCosigneeEMail(message.getConsigneeEmail());
        corporate.setMaxNumberOfDevices(message.getMaxNumberOfDevices());
        corporate.setCreatedBy(message.getRequestedId());
        corporate.setIban(message.getIban());
        return corporate;
    }

    private void registerCorporate(MPAY_Corporate corporate, ProcessingContext context) throws InvalidActionException {
        JfwHelper.createEntity(MPAYView.CORPORATE, corporate);
        Map<String, Object> v = new HashMap<>();
        v.put(WorkflowService.WF_ARG_BEAN, corporate);
        context.getDataProvider().updateWorkflowStep(CorporateWorkflowStatuses.INTEGRATION, corporate.getWorkflowId());
        corporate.setStatusId(context.getDataProvider().getWorkflowStatus(CorporateWorkflowStatuses.INTEGRATION));
        context.getDataProvider().updateCorporate(corporate);
        PostApproveCorporate approveCorporate = AppContext.getApplicationContext().getBean("PostApproveCorporate",
                PostApproveCorporate.class);
        try {
            approveCorporate.execute(v, null, null);
        } catch (WorkflowException e) {
            throw new BusinessException(e);
        }
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }
}
