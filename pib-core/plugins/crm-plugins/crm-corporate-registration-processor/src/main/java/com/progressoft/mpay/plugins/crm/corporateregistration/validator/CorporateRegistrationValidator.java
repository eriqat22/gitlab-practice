package com.progressoft.mpay.plugins.crm.corporateregistration.validator;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.crm.corporateregistration.constant.Constants;
import com.progressoft.mpay.plugins.crm.corporateregistration.message.CRMCorporateRegistrationMessage;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;

public class CorporateRegistrationValidator {

    private CorporateRegistrationValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");

        CRMCorporateRegistrationMessage message = (CRMCorporateRegistrationMessage) context.getRequest();

        ValidationResult result = CorporateExistValidator.validate((MPAY_Corporate) context.getExtraData().get(Constants.IS_CORPORATE_ALREADY_EXIST));
        if (!result.isValid())
            return result;//done

        //validate sender
        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;//done

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;//done

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;//done


        result = CorporateServiceExistValidator.validate((MPAY_CorpoarteService) context.getExtraData().get(Constants.IS_CORPORATE_SERVICE_ALREADY_EXIST));
        if (!result.isValid())
            return result;//done

        result = validateBasicInfo(context);
        if (!result.isValid())
            return result;//done

        result = ExposedCorporteValidator.validate((MPAY_ClientType) context.getExtraData().get(Constants.CORPORATE_TYPES));
        if (!result.isValid())
            return result;//done

        result = validateBank(context);
        if (!result.isValid())
            return result;//done

        result = PaymentTypeValidator.validate((MPAY_MessageType) context.getExtraData().get(Constants.MESSAGE_TYPE));
        if (!result.isValid())
            return result;//done

        result = ServiceNotificationChannelValidator.validate((MPAY_NotificationChannel) context.getExtraData().get(Constants.NOTIFICATION_CHANNEL));
        if (!result.isValid())
            return result;//done

        result = ServiceTypeValidator.validate((MPAY_ServiceType) context.getExtraData().get(Constants.SERVICE_TYPE));
        if (!result.isValid())
            return result;//done

        result = ServiceCategoryValidator.validate((MPAY_ServicesCategory) context.getExtraData().get(Constants.SERVICES_CATEGRORY));
        if (!result.isValid())
            return result;//done

        result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;//done

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;//done

        result = validateAlias(context, message);
        if (!result.isValid())
            return result;//done

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateBasicInfo(MessageProcessingContext context) {
        ValidationResult result = validateIdType(context);
        if (!result.isValid())
            return result;

        result = validateCountry(context);
        if (!result.isValid())
            return result;

        result = validateCity(context);
        if (!result.isValid())
            return result;

        result = validateLanguage(context);
        if (!result.isValid())
            return result;

        return validateProfile(context);
    }

    private static ValidationResult validateIdType(MessageProcessingContext context) {
        MPAY_IDType idType = (MPAY_IDType) context.getExtraData().get(Constants.ID_TYPE);
        if (idType == null || idType.getIsCustomer())
            return new ValidationResult(ReasonCodes.INVALID_ID_TYPE, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCountry(MessageProcessingContext context) {
        JfwCountry country = (JfwCountry) context.getExtraData().get(Constants.COUNTRY);
        if (country == null)
            return new ValidationResult(ReasonCodes.INVALID_NATIONALITY, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCity(MessageProcessingContext context) {
        MPAY_City city = (MPAY_City) context.getExtraData().get(Constants.CITY);
        if (city == null)
            return new ValidationResult(ReasonCodes.CITY_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateLanguage(MessageProcessingContext context) {
        MPAY_Language language = (MPAY_Language) context.getExtraData().get(Constants.LANGUAGE);
        if (language == null)
            return new ValidationResult(ReasonCodes.LANGUAGE_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateAlias(ProcessingContext context, CRMCorporateRegistrationMessage message) {
        if (message.getAlias() == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        MPAY_CorpoarteService corporateService = context.getDataProvider().getCorporateService(message.getAlias(),
                ReceiverInfoType.ALIAS);
        if (corporateService != null)
            return new ValidationResult(ReasonCodes.ALIAS_IS_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateBank(MessageProcessingContext context) {
        MPAY_Bank bank = (MPAY_Bank) context.getExtraData().get(Constants.BANK);
        if (bank == null)
            return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
        if (bank.getDeletedFlag() != null && bank.getDeletedFlag())
            return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
        if (!bank.getIsActive())
            return new ValidationResult(ReasonCodes.BANK_INACTIVE, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateProfile(MessageProcessingContext context) {
        MPAY_Profile profile = (MPAY_Profile) context.getExtraData().get(Constants.PROFILE_KEY);
        if (profile == null)
            return new ValidationResult(ReasonCodes.PROFILE_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }


}
