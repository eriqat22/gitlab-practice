package com.progressoft.mpay.plugins.crm.corporateregistration.validator;

import com.progressoft.mpay.entities.MPAY_ClientType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExposedCorporteValidator {
	private static final Logger logger = LoggerFactory.getLogger(ExposedCorporteValidator.class);

	private ExposedCorporteValidator() {

	}

	public static ValidationResult validate(MPAY_ClientType clientType) {
		logger.debug("Inside Validate ...");
		if (clientType == null)
			return new ValidationResult(ReasonCodes.INVALID_CLIENT_ID_NUMBER, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
