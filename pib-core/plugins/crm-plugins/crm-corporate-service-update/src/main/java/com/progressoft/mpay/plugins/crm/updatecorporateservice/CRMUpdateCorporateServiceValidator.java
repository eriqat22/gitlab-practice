package com.progressoft.mpay.plugins.crm.updatecorporateservice;

import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;

public class CRMUpdateCorporateServiceValidator {
    private CRMUpdateCorporateServiceValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");

        CRMUpdateCorporateServiceMessage message = (CRMUpdateCorporateServiceMessage) context.getRequest();


        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;


        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        MPAY_CorpoarteService service = (MPAY_CorpoarteService) context.getExtraData().get("service");
        result = ServiceValidator.validate(service, true);
        if (!result.isValid())
            return result;


        if (message.getNewServiceName() != null && !message.getNewServiceName().isEmpty()) {
            result = validateRegisteredService(context, message);
            if (!result.isValid())
                return result;
        }
        if (message.getProfile() != null && !message.getProfile().isEmpty()) {
            result = validateProfile(context, message);
            if (!result.isValid())
                return result;
        }
        if (message.getBank() != null && !message.getBank().isEmpty()) {
            result = validateBank(context, message);
            if (!result.isValid())
                return result;
        }

        if (message.getPaymentType() != null && !message.getPaymentType().isEmpty()) {
            result = validatePaymentType(context, message);
            if (!result.isValid())
                return result;
        }

        if (message.getServiceCategory() != null && !message.getServiceCategory().isEmpty()) {
            result = validateServiceCategory(context, message);
            if (!result.isValid())
                return result;
        }

        if (message.getServiceType() != null && !message.getServiceType().isEmpty()) {
            result = validateServiceType(context, message);
            if (!result.isValid())
                return result;
        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateRegisteredService(MessageProcessingContext context, CRMUpdateCorporateServiceMessage message) {
        MPAY_CorpoarteService newCorporateService = context.getDataProvider().getCorporateService(message.getNewServiceName(), ReceiverInfoType.CORPORATE);
        if (newCorporateService == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        else
            return new ValidationResult(ReasonCodes.CORPORATE_SERVICE_IS_ALREADY_REGISTERD, null, false);
    }

    private static ValidationResult validateProfile(MessageProcessingContext context, CRMUpdateCorporateServiceMessage message) {
        MPAY_Profile profile = context.getLookupsLoader().getProfile(message.getProfile());
        if (profile == null)
            return new ValidationResult(ReasonCodes.PROFILE_NOT_DEFINED, null, false);
        else {
            context.getExtraData().put("profile", profile);
            return new ValidationResult(ReasonCodes.VALID, null, true);
        }
    }

    private static ValidationResult validateBank(MessageProcessingContext context, CRMUpdateCorporateServiceMessage message) {
        MPAY_Bank bank = context.getLookupsLoader().getBankByCode(message.getBank());
        if (bank == null)
            return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
        else {
            context.getExtraData().put("bank", bank);
            return new ValidationResult(ReasonCodes.VALID, null, true);
        }
    }

    private static ValidationResult validatePaymentType(MessageProcessingContext context, CRMUpdateCorporateServiceMessage message) {
        MPAY_MessageType messageTypes = context.getLookupsLoader().getMessageTypes(message.getPaymentType());
        if (messageTypes == null)
            return new ValidationResult(ReasonCodes.PAYMENT_TYPE_NOT_SUPPORTED, null, false);
        else {
            context.getExtraData().put("messageTypes", messageTypes);
            return new ValidationResult(ReasonCodes.VALID, null, true);
        }
    }

    private static ValidationResult validateServiceCategory(MessageProcessingContext context, CRMUpdateCorporateServiceMessage message) {
        MPAY_ServicesCategory serviceCategory = context.getLookupsLoader().getServiceCategory(message.getServiceCategory());
        if (serviceCategory == null)
            return new ValidationResult(ReasonCodes.SERVICE_CATEGORY_NOT_SUPPORTED, null, false);
        else {
            context.getExtraData().put("serviceCategory", serviceCategory);
            return new ValidationResult(ReasonCodes.VALID, null, true);
        }
    }

    private static ValidationResult validateServiceType(MessageProcessingContext context, CRMUpdateCorporateServiceMessage message) {
        MPAY_ServiceType serviceType = context.getLookupsLoader().getServiceType(message.getServiceType());
        if (serviceType == null)
            return new ValidationResult(ReasonCodes.PAYMENT_TYPE_NOT_SUPPORTED, null, false);
        else {
            context.getExtraData().put("serviceType", serviceType);
            return new ValidationResult(ReasonCodes.VALID, null, true);
        }
    }
}
