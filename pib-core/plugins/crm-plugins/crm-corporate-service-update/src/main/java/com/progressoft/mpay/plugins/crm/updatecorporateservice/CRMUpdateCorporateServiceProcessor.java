package com.progressoft.mpay.plugins.crm.updatecorporateservice;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class CRMUpdateCorporateServiceProcessor implements MessageProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CRMUpdateCorporateServiceProcessor.class);

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        LOGGER.debug("Inside processMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = CRMUpdateCorporateServiceValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            LOGGER.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            LOGGER.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }


    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatus, String reasonCode, String reasonDescription) {
        LOGGER.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatus);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        LOGGER.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        LOGGER.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        LOGGER.debug("Inside acceptMessage ...");
        context.getDataProvider().updateCorporateService(updateCorporateServiceFromRequest(context));
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }


    private MPAY_CorpoarteService updateCorporateServiceFromRequest(MessageProcessingContext context) {
        CRMUpdateCorporateServiceMessage message = (CRMUpdateCorporateServiceMessage) context.getRequest();
        MPAY_CorpoarteService service = (MPAY_CorpoarteService) context.getExtraData().get("service");

        if (validateData(message.getBranch()))
            service.setBranch(message.getBranch());


        if (message.getMaxNumberOfDevices() > 0)
            service.setMaxNumberOfDevices(message.getMaxNumberOfDevices());

        if (validateData(message.getNewServiceName()))
            service.setName(message.getNewServiceName());

        if (validateData(message.getDescription()))
            service.setDescription(message.getDescription());

        if (validateData(message.getType()))
            service.setBankedUnbanked(message.getType());


        if (validateData(message.getBank()))
            service.setBank((MPAY_Bank) context.getExtraData().get("bank"));

        if (validateData(message.getExternalAccount()))
            service.setExternalAcc(message.getExternalAccount());

        if (validateData(message.getIban()))
            service.setIban(message.getIban());


        if (validateData(message.getPaymentType()))
            service.setPaymentType((MPAY_MessageType) context.getExtraData().get("messageTypes"));

        if (validateData(message.getProfile()))
            service.setRefProfile((MPAY_Profile) context.getExtraData().get("profile"));

        if (validateData(message.getServiceCategory()))
            service.setServiceCategory((MPAY_ServicesCategory) context.getExtraData().get("serviceCategory"));

        if (validateData(message.getServiceType()))
            service.setServiceType((MPAY_ServiceType) context.getExtraData().get("serviceType"));
        return service;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        CRMUpdateCorporateServiceMessage message = CRMUpdateCorporateServiceMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType());
        sender.setService(service);
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        context.getExtraData().put("service", context.getDataProvider().getCorporateService(message.getServiceName(), ReceiverInfoType.CORPORATE));
    }

    private boolean validateData(String value) {
        return value != null && !value.isEmpty();
    }
}
