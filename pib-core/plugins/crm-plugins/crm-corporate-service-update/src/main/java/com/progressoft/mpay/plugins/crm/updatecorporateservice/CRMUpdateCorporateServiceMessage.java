package com.progressoft.mpay.plugins.crm.updatecorporateservice;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRMUpdateCorporateServiceMessage extends MPayRequest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CRMUpdateCorporateServiceMessage.class);
    private static final String BRANCH = "branch";
    private static final String WALLET_TYPE = "walletType";
    private static final String TRANSACTION_SIZE = "transactionsSize";
    private static final String CHARGE_AMOUNT_NUMBER = "chargeAmountNumber";
    private static final String CHARGE_AMOUNT_CHARACTER = "chargeAmountCharacter";
    private static final String WALLET_ACCOUNT = "walletAccount";
    private static final String CHARGE_DURATION = "chargeDuration";
    private static final String MAX_NUMBER_OF_DEVICES = "maxNumberOfDevices";
    private static final String SERVICE_NAME = "serviceName";

    //--new
    private static final String NEW_SERVICE_NAME = "newServiceName";
    private static final String DESCRIPTION = "description";
    private static final String TYPE = "type";//bank or un bank
    private static final String BANK = "bank";
    private static final String EXTERNAL_ACCOUNT = "externalAccount";
    private static final String IBAN = "iban";
    private static final String PAYMENT_TYPE = "paymentType";
    private static final String PROFILE = "profile";
    private static final String SERVICE_CATEGORY = "serviceCategory";
    private static final String SERVICE_TYPE = "serviceType";


    private String newServiceName;
    private String description;
    private String type;
    private String bank;
    private String externalAccount;
    private String iban;
    private String paymentType;
    private String profile;
    private String serviceCategory;
    private String serviceType;
    private String transactionsSize;
    private long maxNumberOfDevices;
    private String walletType;
    private String branch;
    private long chargeAmountNumber;
    private String chargeAmountCharacter;
    private String walletAccount;
    private String chargeDuration;
    private String serviceName;

    public CRMUpdateCorporateServiceMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setRequestedId(request.getRequestedId());
        setExtraData(request.getExtraData());
    }

    public static CRMUpdateCorporateServiceMessage parseMessage(MPayRequest request) throws MessageParsingException {
        LOGGER.debug("inside parseMessage...");
        if (request == null)
            return null;
        CRMUpdateCorporateServiceMessage message = new CRMUpdateCorporateServiceMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
        message.setServiceName(request.getValue(SERVICE_NAME));
        if (message.getServiceName() == null || message.getServiceName().isEmpty())
            throw new MessageParsingException(SERVICE_NAME);
        setAndValidateTransactionSize(message);
        setAndValidateMaxNumberOfDevice(message);
        setAndValidateWalletType(message);
        message.setBranch(message.getValue(BRANCH));
        setAndValidateChargeAmountNumber(message);
        message.setChargeAmountCharacter(message.getValue(CHARGE_AMOUNT_CHARACTER));
        message.setWalletAccount(message.getValue(WALLET_ACCOUNT));
        setAndValidateChargeDuration(message);
        message.setNewServiceName(request.getValue(NEW_SERVICE_NAME));
        message.setDescription(request.getValue(DESCRIPTION));
        setAndValidateType(message);
        message.setBank(request.getValue(BANK));
        message.setExternalAccount(request.getValue(EXTERNAL_ACCOUNT));
        message.setIban(request.getValue(IBAN));
        message.setPaymentType(request.getValue(PAYMENT_TYPE));
        message.setProfile(request.getValue(PROFILE));
        message.setServiceCategory(request.getValue(SERVICE_CATEGORY));
        message.setServiceType(request.getValue(SERVICE_TYPE));
        return message;
    }

    private static void setAndValidateType(CRMUpdateCorporateServiceMessage message) throws MessageParsingException {
        message.setType(message.getValue(TYPE));
        if (message.getType() != null && !message.getType().isEmpty())
            if (!message.getWalletType().equalsIgnoreCase("1") && !message.getWalletType().equalsIgnoreCase("2"))
                throw new MessageParsingException(TYPE);
    }

    private static void setAndValidateWalletType(CRMUpdateCorporateServiceMessage message) throws MessageParsingException {
        message.setWalletType(message.getValue(WALLET_TYPE));
        if (message.getWalletType() != null && !message.getWalletType().isEmpty())
            if (!message.getWalletType().equalsIgnoreCase("1") && !message.getWalletType().equalsIgnoreCase("2"))
                throw new MessageParsingException(WALLET_TYPE);
    }

    private static void setAndValidateChargeDuration(CRMUpdateCorporateServiceMessage message) throws MessageParsingException {
        message.setChargeDuration(message.getValue(CHARGE_DURATION));
        if (message.getChargeDuration() != null && !message.getChargeDuration().isEmpty())
            if (!message.getWalletType().equalsIgnoreCase("0") && !message.getWalletType().equalsIgnoreCase("1"))
                throw new MessageParsingException(CHARGE_DURATION);
    }

    private static void setAndValidateMaxNumberOfDevice(CRMUpdateCorporateServiceMessage message) throws MessageParsingException {
        String maxNumberOfDevice = message.getValue(MAX_NUMBER_OF_DEVICES);
        if (maxNumberOfDevice != null && !maxNumberOfDevice.isEmpty()) {
            try {
                message.setMaxNumberOfDevices(Long.valueOf(maxNumberOfDevice));
            } catch (NumberFormatException ex) {
                throw new MessageParsingException(MAX_NUMBER_OF_DEVICES);
            }
        }
    }

    private static void setAndValidateChargeAmountNumber(CRMUpdateCorporateServiceMessage message) throws MessageParsingException {
        String chargeAmountNumber = message.getValue(CHARGE_AMOUNT_NUMBER);
        if (chargeAmountNumber != null && !chargeAmountNumber.isEmpty()) {
            try {
                message.setChargeAmountNumber(Long.valueOf(chargeAmountNumber));
            } catch (NumberFormatException ex) {
                throw new MessageParsingException(CHARGE_AMOUNT_NUMBER);
            }
        }
    }

    private static void setAndValidateTransactionSize(CRMUpdateCorporateServiceMessage message) throws MessageParsingException {
        message.setTransactionsSize(message.getValue(TRANSACTION_SIZE));
        if (message.getTransactionsSize() != null && !message.getTransactionsSize().isEmpty())
            if (!message.getTransactionsSize().equalsIgnoreCase("1") && !message.getTransactionsSize().equalsIgnoreCase("2")
                    && !message.getTransactionsSize().equalsIgnoreCase("3"))
                throw new MessageParsingException(TRANSACTION_SIZE);
    }

    public String getTransactionsSize() {
        return transactionsSize;
    }

    public void setTransactionsSize(String transactionsSize) {
        this.transactionsSize = transactionsSize;
    }

    public long getMaxNumberOfDevices() {
        return maxNumberOfDevices;
    }

    public void setMaxNumberOfDevices(long maxNumberOfDevices) {
        this.maxNumberOfDevices = maxNumberOfDevices;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public long getChargeAmountNumber() {
        return chargeAmountNumber;
    }

    public void setChargeAmountNumber(long chargeAmountNumber) {
        this.chargeAmountNumber = chargeAmountNumber;
    }

    public String getChargeAmountCharacter() {
        return chargeAmountCharacter;
    }

    public void setChargeAmountCharacter(String chargeAmountCharacter) {
        this.chargeAmountCharacter = chargeAmountCharacter;
    }

    public String getWalletAccount() {
        return walletAccount;
    }

    public void setWalletAccount(String walletAccount) {
        this.walletAccount = walletAccount;
    }

    public String getChargeDuration() {
        return chargeDuration;
    }

    public void setChargeDuration(String chargeDuration) {
        this.chargeDuration = chargeDuration;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getNewServiceName() {
        return newServiceName;
    }

    public void setNewServiceName(String newServiceName) {
        this.newServiceName = newServiceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
