package com.progressoft.mpay.plugins.crm.customertermination;

import com.progressoft.jfw.integration.JfwFacade;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CRMCustomerTerminationProcessor implements MessageProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(CRMCustomerTerminationProcessor.class);

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        LOGGER.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult result = CRMCustomerTerminationValidator.validate(context);
            if (result.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, result.getReasonCode(), result.getReasonDescription());
        } catch (MessageParsingException e) {
            LOGGER.debug("Error while parsing message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            LOGGER.error("Error when ProcessMessage in CustomerInquiryProcessor", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext arg0) {
        return null;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        LOGGER.debug("Inside PreProcessMessage ...");
        CRMCustomerTerminationMessage message = CRMCustomerTerminationMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setRequest(message);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), ReceiverInfoType.CORPORATE));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        context.getExtraData().put("customer", loadCustomer(context));
    }

    private MPAY_Customer loadCustomer(MessageProcessingContext context) {
        CRMCustomerTerminationMessage message = (CRMCustomerTerminationMessage) context.getRequest();
        MPAY_CustomerMobile customerMobile = context.getDataProvider().getCustomerMobile(message.getMobileNumber());
        if (customerMobile == null)
            return null;
        return customerMobile.getRefCustomer();
    }


    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        LOGGER.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        LOGGER.debug("Inside AcceptMessage ...");
        try {
            terminateCustomerMobile(context);
        } catch (TerminationException e) {
            return rejectMessage(context, ReasonCodes.THIS_CUSTOMER_ACCOUNT_HAS_A_BALANCE, null);
        }
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);

        return result;
    }

    private void terminateCustomerMobile(MessageProcessingContext context) {
        CRMCustomerTerminationMessage message = (CRMCustomerTerminationMessage) context.getRequest();
        MPAY_Customer customer = (MPAY_Customer) context.getExtraData().get("customer");

        List<MPAY_CustomerMobile> filterMobile = customer.getRefCustomerCustomerMobiles().stream().filter(m -> m.getDeletedFlag() == null || !m.getDeletedFlag()).collect(Collectors.toList());
        JfwFacade facade = (JfwFacade) AppContext.getApplicationContext().getBean("defaultJfwFacade");
        if (!filterMobile.isEmpty()) {
            if (filterMobile.size() == 1) {
                MPAY_CustomerMobile mobile = filterMobile.get(0);

                MPAY_Account account = mobile.getMobileMobileAccounts().get(0).getRefAccount();
                if (!account.getBalance().equals(BigDecimal.ZERO))
                    throw new TerminationException();

                facade.executeAction(MPAYView.CUSTOMER.viewName, 402, customer, new HashMap<>(), new HashMap<>());
                facade.executeAction(MPAYView.CUSTOMER.viewName, 801, customer, new HashMap<>(), new HashMap<>());
            } else {
                for (MPAY_CustomerMobile mobile : filterMobile) {
                    if (mobile.getMobileNumber().equalsIgnoreCase(message.getMobileNumber())) {
                        MPAY_Account account = mobile.getMobileMobileAccounts().get(0).getRefAccount();
                        if (!account.getBalance().equals(BigDecimal.ZERO))
                            throw new TerminationException();
                        facade.executeAction(MPAYView.CUSTOMER_MOBILE_VIEWS.viewName, 403, mobile, new HashMap<>(), new HashMap<>());

                    }
                }
            }
        }
    }


    private String generateResponse(MessageProcessingContext context) {
        LOGGER.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }


    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String
            reasonDescription, String processingStatus) {
        LOGGER.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }


    public class TerminationException extends RuntimeException {

    }
}
