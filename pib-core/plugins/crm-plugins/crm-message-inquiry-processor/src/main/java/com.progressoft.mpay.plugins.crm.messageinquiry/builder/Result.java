package com.progressoft.mpay.plugins.crm.messageinquiry.builder;

import com.progressoft.mpay.plugins.MessageProcessingContext;

public class Result {
    private MessageProcessingContext processingContext;
    private String reasonCode;
    private String reasonDescription;
    private String processingStatus;

    private Result(ResultBuilder resultBuilder) {
        this.processingContext = resultBuilder.processingContext;
        this.reasonCode = resultBuilder.reasonCode;
        this.reasonDescription = resultBuilder.reasonDescription;
        this.processingStatus = resultBuilder.processingStatus;
    }


    public MessageProcessingContext getProcessingContext() {
        return processingContext;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public String getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(String processingStatus) {
        this.processingStatus = processingStatus;
    }

    public static class ResultBuilder {

        private MessageProcessingContext processingContext;
        private String reasonCode;
        private String reasonDescription;
        private String processingStatus;


        public ResultBuilder processingContext(MessageProcessingContext processingContext) {
            this.processingContext = processingContext;
            return this;
        }

        public ResultBuilder reasonCode(String reasonCode) {
            this.reasonCode = reasonCode;
            return this;
        }

        public ResultBuilder reasonDescription(String reasonDescription) {
            this.reasonDescription = reasonDescription;
            return this;
        }

        public ResultBuilder processingStatus(String processingStatus) {
            this.processingStatus = processingStatus;
            return this;
        }

        public Result build() {
            return new Result(this);
        }
    }

}
