package com.progressoft.mpay.plugins.crm.messageinquiry.message;


import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

public class CRMMessageInquiryMessage extends MPayRequest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CRMMessageInquiryMessage.class);


    private static final String SENDER_SERVICE = "senderService";
    private static final String SENDER_MOBILE = "senderMobile";
    private static final String MESSAGE_TYPE = "messageType";
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String REFERENCE = "reference";
    private static final String FROM_TIME_KEY = "fromTime";
    private static final String TO_TIME_KEY = "toTime";
    private static final String PROCESSING_STATUS = "processingStatus";
    private static final String REASON_ID = "reasonId";
    private static final String REQUESTED_ID = "requestedId";
    private static final String SHOP_ID = "shopId";
    private static final String CHANNEL_ID = "channelId";
    private static final String MESSAGE_ID = "messageId";
    private static final String OPERATION = "operation";


    private String senderService;
    private String senderMobile;
    private String msgReference;
    private Timestamp fromTime;
    private Timestamp toTime;
    private String processingStatus;
    private String messageType;
    private String msgReasonId;
    private String msgRequestedId;
    private String msgShopId;
    private String msgChannelId;
    private String messageId;

    public CRMMessageInquiryMessage() {
    }

    public CRMMessageInquiryMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static CRMMessageInquiryMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CRMMessageInquiryMessage message = new CRMMessageInquiryMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(SENDER_KEY);

        /*if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(SENDER_TYPE_KEY);*/

        message.setSenderMobile(message.getValue(SENDER_MOBILE));
        message.setSenderService(message.getValue(SENDER_SERVICE));
        message.setMsgReference(message.getValue(REFERENCE));
        message.setProcessingStatus(message.getValue(PROCESSING_STATUS));
        message.setMsgReasonId(message.getValue(REASON_ID));
        message.setMsgChannelId(message.getValue(CHANNEL_ID));
        message.setMsgShopId(message.getValue(SHOP_ID));
        message.setMsgRequestedId(message.getValue(REQUESTED_ID));
        message.setMessageType(message.getValue(MESSAGE_TYPE));
        message.setMessageId(message.getValue(MESSAGE_ID));
        message.setOperation(message.getValue(OPERATION));

        getFromTime(message);
        getToTime(message);
        return message;
    }

    private static void getToTime(CRMMessageInquiryMessage message) throws MessageParsingException {
        try {
            message.setToTime(new Timestamp(SystemHelper.parseDate(message.getValue(TO_TIME_KEY), DATE_FORMAT).getTime()));
        } catch (Exception ex) {
            LOGGER.debug("Invalid toDate Format", ex);
            throw new MessageParsingException(TO_TIME_KEY);
        }
    }

    private static void getFromTime(CRMMessageInquiryMessage message) throws MessageParsingException {
        try {
            message.setFromTime(new Timestamp(SystemHelper.parseDate(message.getValue(FROM_TIME_KEY), DATE_FORMAT).getTime()));
        } catch (Exception ex) {
            LOGGER.debug("Invalid fromDate Format", ex);
            throw new MessageParsingException(FROM_TIME_KEY);
        }
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getSenderService() {
        return senderService;
    }

    public void setSenderService(String senderService) {
        this.senderService = senderService;
    }

    public String getSenderMobile() {
        return senderMobile;
    }

    public void setSenderMobile(String senderMobile) {
        this.senderMobile = senderMobile;
    }

    public String getMsgReference() {
        return msgReference;
    }

    public void setMsgReference(String msgReference) {
        this.msgReference = msgReference;
    }

    public Timestamp getFromTime() {
        return fromTime;
    }

    public void setFromTime(Timestamp fromTime) {
        this.fromTime = fromTime;
    }

    public Timestamp getToTime() {
        return toTime;
    }

    public void setToTime(Timestamp toTime) {
        this.toTime = toTime;
    }

    public String getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(String processingStatus) {
        this.processingStatus = processingStatus;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMsgReasonId() {
        return msgReasonId;
    }

    public void setMsgReasonId(String msgReasonId) {
        this.msgReasonId = msgReasonId;
    }

    public String getMsgRequestedId() {
        return msgRequestedId;
    }

    public void setMsgRequestedId(String msgRequestedId) {
        this.msgRequestedId = msgRequestedId;
    }

    public String getMsgShopId() {
        return msgShopId;
    }

    public void setMsgShopId(String msgShopId) {
        this.msgShopId = msgShopId;
    }

    public String getMsgChannelId() {
        return msgChannelId;
    }

    public void setMsgChannelId(String msgChannelId) {
        this.msgChannelId = msgChannelId;
    }
}
