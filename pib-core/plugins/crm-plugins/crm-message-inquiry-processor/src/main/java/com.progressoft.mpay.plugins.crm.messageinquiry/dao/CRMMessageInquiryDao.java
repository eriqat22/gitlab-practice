package com.progressoft.mpay.plugins.crm.messageinquiry.dao;

import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import org.joda.time.format.DateTimeFormat;

public class CRMMessageInquiryDao {

    private String messageId;
    private String reference;
    private String sender;
    private String messageType;
    private String processingDate;
    private String processingStatusCode;
    private String processingStatusDescription;
    private String requestId;
    private String shopId;
    private String channelId;
    private String reason;


    public CRMMessageInquiryDao(MPAY_MPayMessage mPayMessages, MessageProcessingContext context) {
        if (mPayMessages == null)
            throw new NullArgumentException("mPayMessages");

        this.messageId = mPayMessages.getMessageID();
        this.reference = mPayMessages.getReference();
        this.sender = mPayMessages.getSender();
        this.messageType = mPayMessages.getMessageType().getCode();
        this.processingDate = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS").print(mPayMessages.getProcessingStamp().getTime());
        this.processingStatusDescription = mPayMessages.getProcessingStatus().getDescription();
        this.processingStatusCode = mPayMessages.getProcessingStatus().getCode();
        this.requestId = mPayMessages.getRequestedId();
        this.shopId = mPayMessages.getShopId();
        this.channelId = mPayMessages.getChannelId();

        MPAY_Reason mpayReason = mPayMessages.getReason();
        if (mpayReason != null) {
            MPAY_Reasons_NLS reasonsNls = mpayReason.getReasonsNLS().get(context.getRequest().getLang() == 1 ? "en" : "ar");
            if (reasonsNls != null) {
                this.reason = mpayReason.getCode() + "-" + reasonsNls.getDescription();
            }
        }
    }


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getProcessingDate() {
        return processingDate;
    }

    public void setProcessingDate(String processingDate) {
        this.processingDate = processingDate;
    }

    public String getProcessingStatusCode() {
        return processingStatusCode;
    }

    public void setProcessingStatusCode(String processingStatusCode) {
        this.processingStatusCode = processingStatusCode;
    }

    public String getProcessingStatusDescription() {
        return processingStatusDescription;
    }

    public void setProcessingStatusDescription(String processingStatusDescription) {
        this.processingStatusDescription = processingStatusDescription;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

}
