package com.progressoft.mpay.plugins.crm.servicecashout;

import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

import java.math.BigDecimal;

public class CRMServiceCashOutMessage extends MPayRequest {

	private static final String RECEIVER_KEY = "receiver";
	private static final String RECEIVER_TYPE_KEY = "receiverType";
	private static final String AMOUNT_KEY = "amount";
	private static final String CHARGES_AMOUNT_KEY = "charges";
	public static final String NOTES_KEY = "notes";
	private static final String SENDER_ACCOUNT_KEY = "senderAccount";
	private static final String RECEIVER_ACCOUNT_KEY = "receiverAccount";

	private String receiver;
	private String receiverType;
	private BigDecimal amount;
	private BigDecimal chargesAmount;
	private String notes;
	private String senderAccount;
	private String receiverAccount;

	public CRMServiceCashOutMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiverMobile) {
		this.receiver = receiverMobile;
	}

	public String getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(String receiverType) {
		this.receiverType = receiverType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getChargesAmount() {
		return chargesAmount;
	}

	public void setChargesAmount(BigDecimal chargesAmount) {
		this.chargesAmount = chargesAmount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public String getReceiverAccount() {
		return receiverAccount;
	}

	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	public static CRMServiceCashOutMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		CRMServiceCashOutMessage message = new CRMServiceCashOutMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(SENDER_KEY);
		if (message.getSenderType() == null || message.getSenderType().trim().length() == 0)
			throw new MessageParsingException(SENDER_TYPE_KEY);
		String amount = message.getValue(AMOUNT_KEY);
		if (amount == null)
			throw new MessageParsingException(AMOUNT_KEY);
		try {
			message.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
		} catch (NumberFormatException ex) {
			throw new MessageParsingException(AMOUNT_KEY);
		}
		String charges = message.getValue(CHARGES_AMOUNT_KEY);
		if (charges == null)
			throw new MessageParsingException(CHARGES_AMOUNT_KEY);
		try {
			message.setChargesAmount(BigDecimal.valueOf(Double.valueOf(charges)));
		} catch (NumberFormatException ex) {
			throw new MessageParsingException(CHARGES_AMOUNT_KEY);
		}
		message.setReceiver(message.getValue(RECEIVER_KEY));
		if (message.getReceiver() == null)
			throw new MessageParsingException(RECEIVER_KEY);
		message.setReceiverType(message.getValue(RECEIVER_TYPE_KEY));
		if (message.getReceiverType() == null)
			throw new MessageParsingException(RECEIVER_TYPE_KEY);
		message.setSenderAccount(message.getValue(SENDER_ACCOUNT_KEY));
		message.setReceiverAccount(message.getValue(RECEIVER_ACCOUNT_KEY));
		return message;
	}
}
