package com.progressoft.mpay.plugins.crm.updatecustomermobile;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRMUpdateCustomerMobileMessage extends MPayRequest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CRMUpdateCustomerMobileMessage.class);
    private static final String MOBILE_NUMBER = "mobileNumber";
    private static final String NEW_MOBILE_NUMBER = "newMobileNumber";
    private static final String NFC_SERIAL = "nfcSerial";
    private static final String BRANCH = "branch";
    private static final String EXTERNAL_ACCOUNT = "externalAccount";
    private static final String IBAN = "iban";
    private static final String TRANSACTIONS_SIZE = "transactionsSize";
    private static final String MAX_NUMBER_OF_DEVICES = "maxNumberOfDevices";
    private static final String PROFILE = "profile";
    private static final String WALLET_TYPE = "walletType";


    private String transactionsSize;
    private long maxNumberOfDevices;
    private String externalAccount;
    private String newMobileNumber;
    private String mobileNumber;
    private String nfcSerial;
    private String profile;
    private String branch;
    private String iban;
    private String walletType;


    public CRMUpdateCustomerMobileMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setRequestedId(request.getRequestedId());
        setExtraData(request.getExtraData());
    }

    public static CRMUpdateCustomerMobileMessage parseMessage(MPayRequest request) throws MessageParsingException {
        LOGGER.debug("inside parseMessage...");
        if (request == null)
            return null;
        CRMUpdateCustomerMobileMessage message = new CRMUpdateCustomerMobileMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        //validate is number empty
        //check transaction size is not empty
        //and if profile not empty check is exist in db
        //validate is new number not empty check in data base is already exist

        message.setMobileNumber(message.getValue(MOBILE_NUMBER));
        if (message.getMobileNumber() == null || message.getMobileNumber().isEmpty())
            throw new MessageParsingException(MOBILE_NUMBER);
        message.setTransactionsSize(message.getValue(TRANSACTIONS_SIZE));
        if (message.getTransactionsSize() != null && !message.getTransactionsSize().isEmpty())
            if (!message.getTransactionsSize().equalsIgnoreCase("1") && !message.getTransactionsSize().equalsIgnoreCase("2")
                    && !message.getTransactionsSize().equalsIgnoreCase("3"))
                throw new MessageParsingException(TRANSACTIONS_SIZE);

        String maxNumberOfDevice = message.getValue(MAX_NUMBER_OF_DEVICES);
        if (maxNumberOfDevice != null && !maxNumberOfDevice.isEmpty()) {
            try {
                message.setMaxNumberOfDevices(Long.valueOf(maxNumberOfDevice));
            } catch (NumberFormatException ex) {
                throw new MessageParsingException(MAX_NUMBER_OF_DEVICES);
            }
        }

        message.setWalletType(message.getValue(WALLET_TYPE));
        if (message.getWalletType() != null && !message.getWalletType().isEmpty())
            if (!message.getWalletType().equalsIgnoreCase("1") && !message.getWalletType().equalsIgnoreCase("2"))
                throw new MessageParsingException(WALLET_TYPE);

        message.setBranch(message.getValue(BRANCH));
        message.setExternalAccount(message.getValue(EXTERNAL_ACCOUNT));
        message.setIban(message.getValue(IBAN));
        message.setNfcSerial(message.getValue(NFC_SERIAL));
        message.setProfile(message.getValue(PROFILE));
        message.setNewMobileNumber(message.getValue(NEW_MOBILE_NUMBER));
        return message;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getNewMobileNumber() {
        return newMobileNumber;
    }

    public void setNewMobileNumber(String newMobileNumber) {
        this.newMobileNumber = newMobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }


    public String getNfcSerial() {
        return nfcSerial;
    }

    public void setNfcSerial(String nfcSerial) {
        this.nfcSerial = nfcSerial;
    }


    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getTransactionsSize() {
        return transactionsSize;
    }

    public void setTransactionsSize(String transactionsSize) {
        this.transactionsSize = transactionsSize;
    }

    public long getMaxNumberOfDevices() {
        return maxNumberOfDevices;
    }

    public void setMaxNumberOfDevices(long maxNumberOfDevices) {
        this.maxNumberOfDevices = maxNumberOfDevices;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

}
