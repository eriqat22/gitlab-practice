package com.progressoft.mpay.plugins.crm.updatecustomermobile;

import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;

public class CRMUpdateCustomerMobileValidator {
    private CRMUpdateCustomerMobileValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");

        CRMUpdateCustomerMobileMessage message = (CRMUpdateCustomerMobileMessage) context.getRequest();


        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;


        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;
        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        MPAY_CustomerMobile mpayCustomerMobile = (MPAY_CustomerMobile) context.getExtraData().get("mobileNumber");


        if (mpayCustomerMobile == null)
            return new ValidationResult(ReasonCodes.INVALID_MOBILE_NUMBER, null, false);
        else {
            result = MobileValidator.validate(mpayCustomerMobile, true);
            if (!result.isValid())
                return result;
        }


        if (message.getNewMobileNumber() != null && !message.getNewMobileNumber().isEmpty()) {
            result = MobileNumberValidator.validate(context, message.getNewMobileNumber());
            if (!result.isValid())
                return result;

            result = validateRegisteredMobile(context, message);
            if (!result.isValid())
                return result;
        }


        if (message.getProfile() != null && !message.getProfile().isEmpty()) {
            result = validateProfile(context, message);
            if (!result.isValid())
                return result;
        }


        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateProfile(MessageProcessingContext context, CRMUpdateCustomerMobileMessage message) {
        MPAY_Profile profile = context.getLookupsLoader().getProfile(message.getProfile());
        if (profile == null)
            return new ValidationResult(ReasonCodes.PROFILE_NOT_DEFINED, null, false);
        else {
            context.getExtraData().put("profile", profile);
            return new ValidationResult(ReasonCodes.VALID, null, true);
        }
    }

    private static ValidationResult validateRegisteredMobile(ProcessingContext context, CRMUpdateCustomerMobileMessage message) {
        MPAY_CustomerMobile customerMobile = context.getDataProvider().getCustomerMobile(message.getNewMobileNumber());
        if (customerMobile == null)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        else
            return new ValidationResult(ReasonCodes.MOBILE_ALREADY_REGISTERED, null, false);
    }
}
