package com.progressoft.mpay.plugins.crm.updatecustomermobile;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRMUpdateCustomerMobileProcessor implements MessageProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CRMUpdateCustomerMobileProcessor.class);

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        LOGGER.debug("Inside processMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = CRMUpdateCustomerMobileValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            LOGGER.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            LOGGER.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }


    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatus, String reasonCode, String reasonDescription) {
        LOGGER.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatus);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        LOGGER.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        LOGGER.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        LOGGER.debug("Inside AcceptMessage ...");
        context.getDataProvider().updateCustomerMobile(updateCustomerFromRequest(context));
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }


    private MPAY_CustomerMobile updateCustomerFromRequest(MessageProcessingContext context) {
        CRMUpdateCustomerMobileMessage message = (CRMUpdateCustomerMobileMessage) context.getRequest();
        MPAY_CustomerMobile customerMobile = (MPAY_CustomerMobile) context.getExtraData().get("mobileNumber");

        if (validateData(message.getNewMobileNumber()))
            customerMobile.setMobileNumber(message.getNewMobileNumber());

        if (validateData(message.getNfcSerial()))
            customerMobile.setNfcSerial(message.getNfcSerial());

        if (validateData(message.getBranch()))
            customerMobile.setBankBranch(message.getBranch());

        if (validateData(message.getExternalAccount()))
            customerMobile.setExternalAcc(message.getExternalAccount());

        if (validateData(message.getIban()))
            customerMobile.setIban(message.getIban());

        if (validateData(message.getProfile()))
            customerMobile.setRefProfile((MPAY_Profile) context.getExtraData().get("profile"));

        if (message.getMaxNumberOfDevices() > 0)
            customerMobile.setMaxNumberOfDevices(message.getMaxNumberOfDevices());

        return customerMobile;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        CRMUpdateCustomerMobileMessage message = CRMUpdateCustomerMobileMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());

        MPAY_CustomerMobile customerMobile = context.getDataProvider().getCustomerMobile(message.getMobileNumber());
        context.getExtraData().put("mobileNumber", customerMobile);
    }

    private boolean validateData(String value) {
        return value != null && !value.isEmpty();
    }
}
