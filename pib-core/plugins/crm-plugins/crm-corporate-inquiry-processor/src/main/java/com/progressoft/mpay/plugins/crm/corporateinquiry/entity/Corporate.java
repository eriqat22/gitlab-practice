package com.progressoft.mpay.plugins.crm.corporateinquiry.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.util.ArrayList;
import java.util.List;

public class Corporate {


    private String name;
    private String description;
    private String sectorType;
    private String clientRef;
    private java.util.Date registrationDate;
    private String registrationId;
    private Boolean isRegistered;
    private Boolean isActive;
    private String phoneOne;
    private String phoneTwo;
    private String email;
    private String mobileNumber;
    private String pobox;
    private String zipCode;
    private String buildingNum;
    private String streetName;
    private String location;
    private String note;
    private String rejectionNote;
    private String approvalNote;
    private String consigneeArabicName;
    private String consigneeEnglishName;
    private String gender;
    private String otherNationality;
    private String placeOfBirth;
    private java.util.Date dateOfBirth;
    private String cosigneeEMail;
    private String nationalID;
    private String identificationReference;
    private String passportID;
    private java.util.Date passportIssuanceDate;
    private String identificationCard;
    private java.util.Date identificationCardIssuanceDate;
    private String address;
    private String serviceName;
    private String serviceDescription;
    private String alias;
    private String bankedUnbanked;
    private String branch;
    private String externalAcc;
    private String iban;
    private String walletType;
    private String mas;
    private java.math.BigDecimal chargeAmountNumber;
    private String chargeAmountCharacters;
    private String walletAccount;
    private String chargeDuration;
    private String serviceNotificationReceiver;
    private Long maxNumberOfDevices;
    private String approvedData;
    private String changes;
    private String hint;
    private String clientType;
    private String idType;
    private String city;
    private String bank;
    private String paymentType;
    private String serviceNotificationChannel;
    private String refProfile;
    private String serviceType;
    private String nationality;
    private List<Service> services;

    public Corporate() {
        services = new ArrayList<>();
    }

    public Corporate(MPAY_Corporate mpayCorporate) {
        if (mpayCorporate == null)
            throw new NullArgumentException("Corporate");
        this.services = new ArrayList<>();
        this.name = mpayCorporate.getName();
        this.description = mpayCorporate.getDescription();
        this.sectorType = mpayCorporate.getSectorType();
        this.clientRef = mpayCorporate.getClientRef();
        this.registrationDate = mpayCorporate.getRegistrationDate();
        this.registrationId = mpayCorporate.getRegistrationId();
        this.isRegistered = mpayCorporate.getIsRegistered();
        this.isActive = mpayCorporate.getIsActive();
        this.phoneOne = mpayCorporate.getPhoneOne();
        this.phoneTwo = mpayCorporate.getPhoneTwo();
        this.email = mpayCorporate.getEmail();
        this.mobileNumber = mpayCorporate.getMobileNumber();
        this.pobox = mpayCorporate.getPobox();
        this.zipCode = mpayCorporate.getZipCode();
        this.buildingNum = mpayCorporate.getBuildingNum();
        this.streetName = mpayCorporate.getStreetName();
        this.location = mpayCorporate.getLocation();
        this.note = mpayCorporate.getNote();
        this.rejectionNote = mpayCorporate.getRejectionNote();
        this.approvalNote = mpayCorporate.getApprovalNote();
        this.consigneeArabicName = mpayCorporate.getConsigneeArabicName();
        this.consigneeEnglishName = mpayCorporate.getConsigneeEnglishName();
        this.gender = mpayCorporate.getGender();
        this.otherNationality = mpayCorporate.getOtherNationality();
        this.placeOfBirth = mpayCorporate.getPlaceOfBirth();
        this.dateOfBirth = mpayCorporate.getDateOfBirth();
        this.cosigneeEMail = mpayCorporate.getCosigneeEMail();
        this.nationalID = mpayCorporate.getNationalID();
        this.identificationReference = mpayCorporate.getIdentificationReference();
        this.passportID = mpayCorporate.getPassportID();
        this.passportIssuanceDate = mpayCorporate.getPassportIssuanceDate();
        this.identificationCard = mpayCorporate.getIdentificationCard();
        this.identificationCardIssuanceDate = mpayCorporate.getIdentificationCardIssuanceDate();
        this.address = mpayCorporate.getAddress();
        this.serviceName = mpayCorporate.getServiceName();
        this.serviceDescription = mpayCorporate.getServiceDescription();
        this.alias = mpayCorporate.getAlias();
        this.bankedUnbanked = mpayCorporate.getBankedUnbanked();
        this.branch = mpayCorporate.getBranch();
        this.externalAcc = mpayCorporate.getExternalAcc();
        this.iban = mpayCorporate.getIban();
        this.mas = mpayCorporate.getMas();
        this.maxNumberOfDevices = mpayCorporate.getMaxNumberOfDevices();
        this.approvedData = mpayCorporate.getApprovedData();
        this.changes = mpayCorporate.getChanges();
        this.hint = mpayCorporate.getHint();
        this.clientType = mpayCorporate.getClientType().getCode();
        this.idType = mpayCorporate.getIdType().getCode();
        this.city = mpayCorporate.getCity().getCode();
        this.bank = mpayCorporate.getBank().getCode();
        MPAY_MessageType type = mpayCorporate.getPaymentType();
        this.paymentType = type != null ? type.getCode() : "";
        MPAY_Profile profile = mpayCorporate.getRefProfile();
        this.refProfile = profile != null ? profile.getCode() : "";
        this.serviceType = mpayCorporate.getSectorType();
        JfwCountry country = mpayCorporate.getNationality();
        this.nationality = country != null ? country.getCode() : "";
        this.services = new ArrayList<>();
        for (MPAY_CorpoarteService service : mpayCorporate.getRefCorporateCorpoarteServices())
            this.services.add(new Service(service));
    }


    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getSectorType() {
        return this.sectorType;
    }

    public void setSectorType(String sectorType) {
        this.sectorType = sectorType;
    }

    public String getClientRef() {
        return this.clientRef;
    }

    public void setClientRef(String clientRef) {
        this.clientRef = clientRef;
    }


    public java.util.Date getRegistrationDate() {
        return this.registrationDate;
    }

    public void setRegistrationDate(java.util.Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getRegistrationId() {
        return this.registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }


    public Boolean getIsRegistered() {
        return this.isRegistered;
    }

    public void setIsRegistered(Boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }


    public String getPhoneOne() {
        return this.phoneOne;
    }

    public void setPhoneOne(String phoneOne) {
        this.phoneOne = phoneOne;
    }

    public String getPhoneTwo() {
        return this.phoneTwo;
    }

    public void setPhoneTwo(String phoneTwo) {
        this.phoneTwo = phoneTwo;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPobox() {
        return this.pobox;
    }

    public void setPobox(String pobox) {
        this.pobox = pobox;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getBuildingNum() {
        return this.buildingNum;
    }

    public void setBuildingNum(String buildingNum) {
        this.buildingNum = buildingNum;
    }

    public String getStreetName() {
        return this.streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRejectionNote() {
        return this.rejectionNote;
    }

    public void setRejectionNote(String rejectionNote) {
        this.rejectionNote = rejectionNote;
    }

    public String getApprovalNote() {
        return this.approvalNote;
    }

    public void setApprovalNote(String approvalNote) {
        this.approvalNote = approvalNote;
    }

    public String getConsigneeArabicName() {
        return this.consigneeArabicName;
    }

    public void setConsigneeArabicName(String consigneeArabicName) {
        this.consigneeArabicName = consigneeArabicName;
    }

    public String getConsigneeEnglishName() {
        return this.consigneeEnglishName;
    }

    public void setConsigneeEnglishName(String consigneeEnglishName) {
        this.consigneeEnglishName = consigneeEnglishName;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOtherNationality() {
        return this.otherNationality;
    }

    public void setOtherNationality(String otherNationality) {
        this.otherNationality = otherNationality;
    }

    public String getPlaceOfBirth() {
        return this.placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public java.util.Date getDateOfBirth() {
        return this.dateOfBirth;
    }

    public void setDateOfBirth(java.util.Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCosigneeEMail() {
        return this.cosigneeEMail;
    }

    public void setCosigneeEMail(String cosigneeEMail) {
        this.cosigneeEMail = cosigneeEMail;
    }

    public String getNationalID() {
        return this.nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }

    public String getIdentificationReference() {
        return this.identificationReference;
    }

    public void setIdentificationReference(String identificationReference) {
        this.identificationReference = identificationReference;
    }

    public String getPassportID() {
        return this.passportID;
    }

    public void setPassportID(String passportID) {
        this.passportID = passportID;
    }

    public java.util.Date getPassportIssuanceDate() {
        return this.passportIssuanceDate;
    }

    public void setPassportIssuanceDate(java.util.Date passportIssuanceDate) {
        this.passportIssuanceDate = passportIssuanceDate;
    }

    public String getIdentificationCard() {
        return this.identificationCard;
    }

    public void setIdentificationCard(String identificationCard) {
        this.identificationCard = identificationCard;
    }

    public java.util.Date getIdentificationCardIssuanceDate() {
        return this.identificationCardIssuanceDate;
    }

    public void setIdentificationCardIssuanceDate(java.util.Date identificationCardIssuanceDate) {
        this.identificationCardIssuanceDate = identificationCardIssuanceDate;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getServiceName() {
        return this.serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return this.serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBankedUnbanked() {
        return this.bankedUnbanked;
    }

    public void setBankedUnbanked(String bankedUnbanked) {
        this.bankedUnbanked = bankedUnbanked;
    }

    public String getBranch() {
        return this.branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getExternalAcc() {
        return this.externalAcc;
    }

    public void setExternalAcc(String externalAcc) {
        this.externalAcc = externalAcc;
    }

    public String getIban() {
        return this.iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getWalletType() {
        return this.walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getMas() {
        return this.mas;
    }

    public void setMas(String mas) {
        this.mas = mas;
    }

    public java.math.BigDecimal getChargeAmountNumber() {
        return this.chargeAmountNumber;
    }

    public void setChargeAmountNumber(java.math.BigDecimal chargeAmountNumber) {
        this.chargeAmountNumber = chargeAmountNumber;
    }

    public String getChargeAmountCharacters() {
        return this.chargeAmountCharacters;
    }

    public void setChargeAmountCharacters(String chargeAmountCharacters) {
        this.chargeAmountCharacters = chargeAmountCharacters;
    }

    public String getWalletAccount() {
        return this.walletAccount;
    }

    public void setWalletAccount(String walletAccount) {
        this.walletAccount = walletAccount;
    }

    public String getChargeDuration() {
        return this.chargeDuration;
    }

    public void setChargeDuration(String chargeDuration) {
        this.chargeDuration = chargeDuration;
    }

    public String getServiceNotificationReceiver() {
        return this.serviceNotificationReceiver;
    }

    public void setServiceNotificationReceiver(String serviceNotificationReceiver) {
        this.serviceNotificationReceiver = serviceNotificationReceiver;
    }

    public Long getMaxNumberOfDevices() {
        return this.maxNumberOfDevices;
    }

    public void setMaxNumberOfDevices(Long maxNumberOfDevices) {
        this.maxNumberOfDevices = maxNumberOfDevices;
    }

    public String getApprovedData() {
        return this.approvedData;
    }

    public void setApprovedData(String approvedData) {
        this.approvedData = approvedData;
    }

    public String getChanges() {
        return this.changes;
    }

    public void setChanges(String changes) {
        this.changes = changes;
    }

    public String getHint() {
        return this.hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getClientType() {
        return this.clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getIdType() {
        return this.idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBank() {
        return this.bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getPaymentType() {
        return this.paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getServiceNotificationChannel() {
        return this.serviceNotificationChannel;
    }

    public void setServiceNotificationChannel(String serviceNotificationChannel) {
        this.serviceNotificationChannel = serviceNotificationChannel;
    }

    public String getRefProfile() {
        return this.refProfile;
    }

    public void setRefProfile(String refProfile) {
        this.refProfile = refProfile;
    }

    public String getServiceType() {
        return this.serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getNationality() {
        return this.nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}
