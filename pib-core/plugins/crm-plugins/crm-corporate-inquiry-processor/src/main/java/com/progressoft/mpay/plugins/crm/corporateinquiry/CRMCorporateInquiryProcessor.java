package com.progressoft.mpay.plugins.crm.corporateinquiry;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.crm.corporateinquiry.entity.Corporate;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRMCorporateInquiryProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(CRMCorporateInquiryProcessor.class);

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult result = CRMCorporateInquiryValidator.validate(context);
            if (result.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, result.getReasonCode(), result.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in CustomerInquiryProcessor", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        logger.debug("Inside PreProcessMessage ...");
        CRMCorporateInquiryMessage message = CRMCorporateInquiryMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());
    }

    private Corporate loadCorporate(MessageProcessingContext context) {
        CRMCorporateInquiryMessage message = (CRMCorporateInquiryMessage) context.getRequest();
        MPAY_Corporate mpayCorporate = null;
        if (!StringUtils.isEmpty(message.getRegistrationId()) && !StringUtils.isEmpty(message.getIdTypeCode()))
            mpayCorporate = context.getDataProvider().getCorporateByIdTypeAndRegistrationId(message.getIdTypeCode(), message.getRegistrationId());
        else if (message.getAlias() != null && !message.getAlias().isEmpty())
            mpayCorporate = context.getDataProvider().getCorporateByAlias(message.getAlias());
        else if (message.getServiceName() != null && !message.getServiceName().isEmpty())
            mpayCorporate = context.getDataProvider().getCorporateByServiceName(message.getServiceName());

        if (mpayCorporate == null)
            return null;
        return new Corporate(mpayCorporate);
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context, true);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        String corporateData = null;

        if (isAccepted) {
            Corporate corporate = loadCorporate(context);
            if (corporate != null)
                corporateData = corporate.toString();
        }
        response.getExtraData().add(new ExtraData(Constants.CORPORATE, corporateData));
        return response.toString();
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext arg0) {
        return null;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String
            reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }
}
