package com.progressoft.mpay.plugins.crm.corporateinquiry;

public class Constants {
    public static final String CORPORATE = "corporate";
    public static final String BALANCE = "balance";

    private Constants() {

    }
}
