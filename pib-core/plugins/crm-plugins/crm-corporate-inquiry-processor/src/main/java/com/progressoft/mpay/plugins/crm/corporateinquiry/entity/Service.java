package com.progressoft.mpay.plugins.crm.corporateinquiry.entity;

import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.exceptions.NullArgumentException;

public class Service {


    private String name;

    public Service() {
    }

    public Service(MPAY_CorpoarteService service) {
        if (service == null)
            throw new NullArgumentException("service");
        this.name = service.getName();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
