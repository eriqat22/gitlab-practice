package com.progressoft.mpay.plugins.crm.corporatetermination;

import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRMCorporateTerminationValidator {
    private static final Logger logger = LoggerFactory.getLogger(CRMCorporateTerminationValidator.class);

    private CRMCorporateTerminationValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ...");
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;


        MPAY_Corporate corporate = (MPAY_Corporate) context.getExtraData().get("corporate");
        if (corporate == null)
            return new ValidationResult(ReasonCodes.INVALID_SERVICE_NAME, null, false);
        else {
            result = CorporateValidator.validate(corporate, false);
            if (!result.isValid())
                return result;

        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
