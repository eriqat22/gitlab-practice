package com.progressoft.mpay.plugins.crm.corporatetermination;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

public class CRMCorporateTerminationMessage extends MPayRequest {
    private static final String SERVICE_NAME = "serviceName";

    private String serviceName;

    public CRMCorporateTerminationMessage() {
        //
    }

    public CRMCorporateTerminationMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static CRMCorporateTerminationMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CRMCorporateTerminationMessage message = new CRMCorporateTerminationMessage(request);


        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        message.setServiceName(message.getValue(SERVICE_NAME));
        if (message.getServiceName() == null || message.getServiceName().isEmpty())
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        return message;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
