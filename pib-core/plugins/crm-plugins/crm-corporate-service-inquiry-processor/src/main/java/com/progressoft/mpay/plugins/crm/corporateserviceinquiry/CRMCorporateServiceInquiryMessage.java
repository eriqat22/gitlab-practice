package com.progressoft.mpay.plugins.crm.corporateserviceinquiry;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;

public class CRMCorporateServiceInquiryMessage extends MPayRequest {
    private static final String ID_TYPE_CODE_KEY = "idTypeCode";
    private static final String REGISTRATION_ID = "registrationId";
    private static final String ALIAS_KEY = "serviceAlias";
    private static final String SERVICE_NAME = "serviceName";

    private String idTypeCode;
    private String registrationId;
    private String alias;
    private String serviceName;

    public CRMCorporateServiceInquiryMessage() {
        //
    }

    public CRMCorporateServiceInquiryMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static CRMCorporateServiceInquiryMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CRMCorporateServiceInquiryMessage message = new CRMCorporateServiceInquiryMessage(request);

        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        message.setIdTypeCode(message.getValue(ID_TYPE_CODE_KEY));
        message.setRegistrationId(message.getValue(REGISTRATION_ID));
        message.setAlias(message.getValue(ALIAS_KEY));
        message.setServiceName(message.getValue(SERVICE_NAME));

        validateMessage(message);
        return message;
    }

    private static void validateMessage(CRMCorporateServiceInquiryMessage message) throws MessageParsingException {
        if (StringUtils.isEmpty(message.getRegistrationId()) && !StringUtils.isEmpty(message.getIdTypeCode()))
            throw new MessageParsingException(REGISTRATION_ID);
        if (!StringUtils.isEmpty(message.getRegistrationId()) && StringUtils.isEmpty(message.getIdTypeCode()))
            throw new MessageParsingException(ID_TYPE_CODE_KEY);

        if (StringUtils.isEmpty(message.getAlias()) &&
                StringUtils.isEmpty(message.getIdTypeCode()) &&
                StringUtils.isEmpty(message.getRegistrationId()) && StringUtils.isEmpty(message.getServiceName()))
            throw new MessageParsingException("All Fields cannot be empty");
    }

    public String getIdTypeCode() {
        return idTypeCode;
    }

    public void setIdTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }


    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

}
