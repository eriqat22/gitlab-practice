package com.progressoft.mpay.plugins.crm.corporateserviceinquiry.entity;

import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.util.ArrayList;
import java.util.List;

public class Service {


    private String name;
    private String description;
    private String alias;
    private String mpclearAlias;
    private String bankedUnbanked;
    private String branch;
    private String externalAcc;
    private String iban;
    private String walletType;
    private Long mas;
    private Boolean isRegistered;
    private Boolean isActive;
    private String notificationReceiver;
    private String walletAccount;
    private Boolean isBlocked;
    private String extraData;
    private String newName;
    private String newDescription;
    private String newAlias;
    private String approvedData;
    private String rejectionNote;
    private String approvalNote;
    private String changes;
    private Long maxNumberOfDevices;
    private String city;
    private String bank;
    private String paymentType;
    private String notificationChannel;
    private String serviceType;
    private List<Account> accounts;

    public Service() {
        accounts = new ArrayList<>();
    }

    public Service(MPAY_CorpoarteService service) {
        if (service == null)
            throw new NullArgumentException("service");
        this.accounts = new ArrayList<>();
        this.name = service.getName();
        this.description = service.getDescription();
        this.alias = service.getAlias();
        this.mpclearAlias = service.getMpclearAlias();
        this.bankedUnbanked = service.getBankedUnbanked();
        this.branch = service.getBranch();
        this.externalAcc = service.getExternalAcc();
        this.iban = service.getIban();

        this.mas = service.getMas();
        this.isRegistered = service.getIsRegistered();
        this.isActive = service.getIsActive();
        this.notificationReceiver = service.getNotificationReceiver();
        this.isBlocked = service.getIsBlocked();
        this.extraData = service.getExtraData();
        this.newName = service.getNewName();
        this.newDescription = service.getNewDescription();
        this.newAlias = service.getNewAlias();
        this.approvedData = service.getApprovedData();
        this.rejectionNote = service.getRejectionNote();
        this.approvalNote = service.getApprovalNote();
        this.changes = service.getChanges();
        this.maxNumberOfDevices = service.getMaxNumberOfDevices();
        MPAY_City city = service.getCity();
        this.city = city != null ? city.getCode() : "";
        MPAY_Bank bank = service.getBank();
        this.bank = bank != null ? bank.getCode() : "";
        MPAY_MessageType paymentType = service.getPaymentType();
        this.paymentType = paymentType != null ? paymentType.getCode() : "";
        MPAY_NotificationChannel notificationChannel = service.getNotificationChannel();
        this.notificationChannel = notificationChannel != null ? notificationChannel.getCode() : "";
        MPAY_ServiceType serviceType = service.getServiceType();
        this.serviceType = serviceType != null ? serviceType.getCode() : "";
        for (MPAY_ServiceAccount account : service.getServiceServiceAccounts()) {
            accounts.add(new Account(account));
        }

    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getMpclearAlias() {
        return this.mpclearAlias;
    }

    public void setMpclearAlias(String mpclearAlias) {
        this.mpclearAlias = mpclearAlias;
    }

    public String getBankedUnbanked() {
        return this.bankedUnbanked;
    }

    public void setBankedUnbanked(String bankedUnbanked) {
        this.bankedUnbanked = bankedUnbanked;
    }

    public String getBranch() {
        return this.branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getExternalAcc() {
        return this.externalAcc;
    }

    public void setExternalAcc(String externalAcc) {
        this.externalAcc = externalAcc;
    }

    public String getIban() {
        return this.iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getWalletType() {
        return this.walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public Long getMas() {
        return this.mas;
    }

    public void setMas(Long mas) {
        this.mas = mas;
    }

    public Boolean getIsRegistered() {
        return this.isRegistered;
    }

    public void setIsRegistered(Boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getNotificationReceiver() {
        return this.notificationReceiver;
    }

    public void setNotificationReceiver(String notificationReceiver) {
        this.notificationReceiver = notificationReceiver;
    }


    public String getWalletAccount() {
        return this.walletAccount;
    }

    public void setWalletAccount(String walletAccount) {
        this.walletAccount = walletAccount;
    }


    public Boolean getIsBlocked() {
        return this.isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }


    public String getExtraData() {
        return this.extraData;
    }

    public void setExtraData(String extraData) {
        this.extraData = extraData;
    }

    public String getNewName() {
        return this.newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public String getNewDescription() {
        return this.newDescription;
    }

    public void setNewDescription(String newDescription) {
        this.newDescription = newDescription;
    }

    public String getNewAlias() {
        return this.newAlias;
    }

    public void setNewAlias(String newAlias) {
        this.newAlias = newAlias;
    }

    public String getApprovedData() {
        return this.approvedData;
    }

    public void setApprovedData(String approvedData) {
        this.approvedData = approvedData;
    }

    public String getRejectionNote() {
        return this.rejectionNote;
    }

    public void setRejectionNote(String rejectionNote) {
        this.rejectionNote = rejectionNote;
    }

    public String getApprovalNote() {
        return this.approvalNote;
    }

    public void setApprovalNote(String approvalNote) {
        this.approvalNote = approvalNote;
    }

    public String getChanges() {
        return this.changes;
    }

    public void setChanges(String changes) {
        this.changes = changes;
    }


    public Long getMaxNumberOfDevices() {
        return this.maxNumberOfDevices;
    }

    public void setMaxNumberOfDevices(Long maxNumberOfDevices) {
        this.maxNumberOfDevices = maxNumberOfDevices;
    }


    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBank() {
        return this.bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }


    public String getPaymentType() {
        return this.paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getNotificationChannel() {
        return this.notificationChannel;
    }

    public void setNotificationChannel(String notificationChannel) {
        this.notificationChannel = notificationChannel;
    }

    public String getServiceType() {
        return this.serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
