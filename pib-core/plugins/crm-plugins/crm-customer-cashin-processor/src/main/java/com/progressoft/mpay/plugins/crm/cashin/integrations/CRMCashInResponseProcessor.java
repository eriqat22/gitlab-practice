package com.progressoft.mpay.plugins.crm.cashin.integrations;

import com.progressoft.jfw.shared.exception.WorkflowException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entities.MPAY_CashIn;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.crm.cashin.CRMCashInMessage;
import com.progressoft.mpay.plugins.crm.cashin.CRMNotificationProcessor;
import com.progressoft.mpay.transactions.AccountingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class CRMCashInResponseProcessor {
    private static final String SVC_ACCEPT = "SVC_Accept";
    private static final String SVC_REJECT = "SVC_Reject";
    private static final Logger logger = LoggerFactory.getLogger(CRMCashInResponseProcessor.class);

    private CRMCashInResponseProcessor() {

    }

    public static IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) throws WorkflowException {
        try {
            logger.debug("Inside ProcessIntegration ...");
            preProcessIntegration(context);
            if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return null;
            if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return accept(context);
            else
                return reject(context);
        } catch (Exception ex) {
            logger.error("Error when ProcessIntegration in CRMCashInResponseProcessor", ex);
            if (context.getTransaction() != null) {
                TransactionHelper.reverseTransaction(context);
                try {
                    context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(), BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
                } catch (SQLException e) {
                    logger.error("Failed to reverse limits", e);
                }
            }
            IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), null, false);
            if (!context.getMessage().getRefMessageCashIn().isEmpty()) {
                MPAY_CashIn cashIn = context.getMessage().getRefMessageCashIn().get(0);
                executeWorkFlowAction(cashIn, SVC_REJECT);
                cashIn.setProcessingStatus(result.getProcessingStatus());
                cashIn.setReason(result.getReason());
                cashIn.setReasonDesc(result.getReasonDescription());
                context.getDataProvider().mergeCashIn(cashIn);
            }

            return result;
        }
    }

    private static void executeWorkFlowAction(MPAY_CashIn cashIn, String viewName) throws WorkflowException {
        try {
            JfwHelper.executeActionWithServiceUser(MPAYView.CASH_IN, cashIn.getId(), viewName);
        } catch (com.opensymphony.workflow.WorkflowException e) {
            throw new WorkflowException(e);
        }
    }

    private static void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
        logger.debug("Inside PreProcessIntegration ...");
        if (context.getMessage() != null)
            context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        CRMCashInMessage request = null;
        try {
            request = CRMCashInMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
        } catch (MessageParsingException ex) {
            logger.error("Error while parsing message", ex);
            throw new WorkflowException(ex);
        }
        if (request != null)
            context.setAmount(request.getAmount());
        context.setSender(sender);
        context.setReceiver(receiver);
        sender.setService(context.getTransaction().getSenderService());
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(context.getTransaction().getSenderServiceAccount());
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        sender.setBanked(sender.getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
        sender.setCharge(context.getTransaction().getSenderCharge());
        sender.setTax(context.getTransaction().getSenderTax());
        sender.setInfo(sender.getService().getName());
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

        receiver.setMobile(context.getTransaction().getReceiverMobile());
        receiver.setMobileAccount(context.getTransaction().getReceiverMobileAccount());
        receiver.setAccount(receiver.getMobileAccount().getRefAccount());
        receiver.setCustomer(receiver.getMobile().getRefCustomer());
        receiver.setBanked(receiver.getMobileAccount().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
        receiver.setCharge(context.getTransaction().getReceiverCharge());
        receiver.setTax(context.getTransaction().getReceiverTax());
        receiver.setProfile(receiver.getMobileAccount().getRefProfile());
        receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

        context.setTransactionConfig(
                context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(), sender.isBanked(), context.getLookupsLoader().getCustomerClientType().getId(), receiver.isBanked(), context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT, context.getOperation().getId()));
        context.setDirection(Long.parseLong(context.getTransaction().getDirection().getCode()));
    }

    private static IntegrationProcessingResult accept(IntegrationProcessingContext context) throws WorkflowException, SQLException {
        logger.debug("Inside Accept ...");
        IntegrationProcessingResult result;
        JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), context.getTransaction());
        MPAY_CashIn cashIn = null;
        String cashInExecutionAction;
        if (!context.getMessage().getRefMessageCashIn().isEmpty())
            cashIn = context.getMessage().getRefMessageCashIn().get(0);
        if (postingResult.isSuccess()) {
            result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
            result.setNotifications(CRMNotificationProcessor.createAcceptanceNotificationMessages(context));
            cashInExecutionAction = SVC_ACCEPT;
        } else {
            result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, postingResult.getReason(), null, null, false);
            result.setNotifications(CRMNotificationProcessor.createRejectionNotificationMessages(context));
            cashInExecutionAction = SVC_REJECT;
            context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(), BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
        }
        if (cashIn != null) {
            cashIn.setReason(result.getMpayMessage().getReason());
            cashIn.setReasonDesc(result.getMpayMessage().getReasonDesc());
            cashIn.setProcessingStatus(result.getMpayMessage().getProcessingStatus());
            executeWorkFlowAction(cashIn, cashInExecutionAction);
            //  context.getDataProvider().mergeCashIn(cashIn);
        }
        return result;
    }


    private static IntegrationProcessingResult reject(IntegrationProcessingContext context) throws WorkflowException, SQLException {
        logger.debug("Inside Reject ...");
        BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
        if (integrationResult != null)
            TransactionHelper.reverseTransaction(context);
        context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(), BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
        String reasonDescription = null;
        if (context.getMpClearIsoMessage().getField(47) != null)
            reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, ReasonCodes.REJECTED_BY_NATIONAL_SWITCH, context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
        result.setNotifications(CRMNotificationProcessor.createRejectionNotificationMessages(context));
        MPAY_CashIn cashIn = null;
        if (!context.getMessage().getRefMessageCashIn().isEmpty())
            cashIn = context.getMessage().getRefMessageCashIn().get(0);
        if (cashIn != null) {
            executeWorkFlowAction(cashIn, SVC_REJECT);
            cashIn.setReason(result.getMpayMessage().getReason());
            cashIn.setReasonDesc(result.getMpayMessage().getReasonDesc());
            cashIn.setProcessingStatus(result.getMpayMessage().getProcessingStatus());
            context.getDataProvider().mergeCashIn(cashIn);
        }
        return result;
    }
}