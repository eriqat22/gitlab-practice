package com.progressoft.mpay.plugins.crm.cashin;

import com.progressoft.mpay.plugins.NotificationContext;

public class CashInNotificationContext extends NotificationContext {
	private String currency;
	private String amount;
	private String receiverBalance;
	private String receiverCharges;
	private String receiverTax;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getReceiverBalance() {
		return receiverBalance;
	}

	public void setReceiverBalance(String receiverBalance) {
		this.receiverBalance = receiverBalance;
	}

	public String getReceiverCharges() {
		return receiverCharges;
	}

	public void setReceiverCharges(String receiverCharges) {
		this.receiverCharges = receiverCharges;
	}

	public String getReceiverTax() {
		return receiverTax;
	}

	public void setReceiverTax(String receiverTax) {
		this.receiverTax = receiverTax;
	}

	public boolean hasReceiverCharges() {
		if (this.receiverCharges == null || this.receiverCharges.trim().length() == 0)
			return false;
		return Double.parseDouble(this.receiverCharges) > 0;
	}

	public boolean hasReceiverTax() {
		if (this.receiverTax == null || this.receiverTax.trim().length() == 0)
			return false;
		return Double.parseDouble(this.receiverTax) > 0;
	}
}
