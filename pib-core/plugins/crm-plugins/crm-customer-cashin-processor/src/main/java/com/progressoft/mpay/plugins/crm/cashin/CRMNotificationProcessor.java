package com.progressoft.mpay.plugins.crm.cashin;

import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CRMNotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(CRMNotificationProcessor.class);

    private CRMNotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateAcceptanceNotificationMessages ...");
            String reference;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            List<MPAY_Notification> notifications = new ArrayList<>();
            MPAY_CustomerMobile receiverMobile = context.getReceiver().getMobile();
            MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();
            if (receiverMobile != null) {
                CashInNotificationContext notificationContext = new CashInNotificationContext();
                notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
                notificationContext.setReference(reference);
                context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getAccount().getBalance()));
                notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
                notificationContext.setReceiverTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getTax()));
                notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(),
                        context.getReceiver().getCustomer().getPrefLang().getId(), receiverMobile.getMobileNumber(), NotificationChannelsCode.SMS));
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error in CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
//            if (!context.getMessage().getReason().getSmsEnabled())
//                return notifications;
            CRMCashInMessage request = CRMCashInMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            String receiverMobile = request.getReceiver();
            long languageId = request.getLang();
            String languageSymbol = LanguageMapper.getInstance().getLanguageLocaleCode(String.valueOf(languageId));
            MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(receiverMobile);
            if (mobile != null) {
                receiverMobile = mobile.getMobileNumber();
                languageId = mobile.getRefCustomer().getPrefLang().getId();
            }
            CashInNotificationContext notificationContext = new CashInNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(context.getMessage().getReason().getReasonsNLS().get(languageSymbol).getDescription());
            notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_RECEIVER,
                    context.getMessage().getRefOperation().getId(), languageId, receiverMobile, NotificationChannelsCode.SMS));
            return notifications;
        } catch (Exception e) {
            logger.error("Error in CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(MessageProcessingContext context) {
        logger.debug("Inside CreateReversalNotificationMessages ...");
        try {
            String reference;
            List<MPAY_Notification> notifications = new ArrayList<>();

            reference = context.getTransaction().getReference();
            MPAY_CustomerMobile receiverMobile = context.getTransaction().getReceiverMobile();
            MPAY_EndPointOperation operation = context.getTransaction().getRefOperation();
            String currency = context.getTransaction().getCurrency().getStringISOCode();
            if (receiverMobile != null) {
                long receiverLanguageId;
                CashInNotificationContext receiverNotificationContext = new CashInNotificationContext();
                receiverNotificationContext.setCurrency(currency);
                receiverNotificationContext.setReference(reference);
                MPAY_Account receiverAccount = context.getTransaction().getReceiverMobileAccount().getRefAccount();
                context.getDataProvider().refreshEntity(receiverAccount);
                receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
                receiverLanguageId = receiverMobile.getRefCustomer().getPrefLang().getId();
                notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
                        receiverLanguageId, receiverMobile.getMobileNumber(), NotificationChannelsCode.SMS));
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateReversalNotificationMessages", e);
            return new ArrayList<>();
        }
    }
}
