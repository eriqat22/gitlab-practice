package com.progressoft.mpay.plugins.crm.customerinquiry;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;

public class CRMCustomerInquiryMessage extends MPayRequest {
	private static final String ID_TYPE_CODE_KEY = "idTypeCode";
	private static final String ID_NUMBER_KEY = "idNumber";
	private static final String MOBILE_NUMBER_KEY = "mobileNumber";
	private static final String ALIAS_KEY = "alias";

	private String idTypeCode;
	private String idNumber;
	private String mobileNumber;
	private String alias;

	public CRMCustomerInquiryMessage() {
		//
	}

	public CRMCustomerInquiryMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getIdTypeCode() {
		return idTypeCode;
	}

	public void setIdTypeCode(String idTypeCode) {
		this.idTypeCode = idTypeCode;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public static CRMCustomerInquiryMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		CRMCustomerInquiryMessage message = new CRMCustomerInquiryMessage(request);


		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);


		message.setIdTypeCode(message.getValue(ID_TYPE_CODE_KEY));
		message.setIdNumber(message.getValue(ID_NUMBER_KEY));
		message.setMobileNumber(message.getValue(MOBILE_NUMBER_KEY));
		message.setAlias(message.getValue(ALIAS_KEY));



		validateMessage(message);
		return message;
	}

	private static void validateMessage(CRMCustomerInquiryMessage message) throws MessageParsingException {
		if (StringUtils.isEmpty(message.getIdNumber()) && !StringUtils.isEmpty(message.getIdTypeCode()))
			throw new MessageParsingException(ID_NUMBER_KEY);
		if (!StringUtils.isEmpty(message.getIdNumber()) && StringUtils.isEmpty(message.getIdTypeCode()))
			throw new MessageParsingException(ID_TYPE_CODE_KEY);

		if (	StringUtils.isEmpty(message.getMobileNumber())&&
				StringUtils.isEmpty(message.getAlias()) &&
				StringUtils.isEmpty(message.getIdTypeCode()) &&
				StringUtils.isEmpty(message.getIdNumber()))
			throw new MessageParsingException("All Fields cannot be null or empty");
	}
}
