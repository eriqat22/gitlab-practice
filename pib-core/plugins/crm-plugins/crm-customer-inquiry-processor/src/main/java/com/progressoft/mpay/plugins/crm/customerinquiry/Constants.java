package com.progressoft.mpay.plugins.crm.customerinquiry;

public class Constants {
	public static final String CUSTOMER_KEY = "customer";
    public static final String BALANCE = "balance";

    private Constants() {

	}
}
