package com.progressoft.mpay.plugins.crm.customerinquiry.entity;

import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.math.BigDecimal;

public class Account {
    private  BigDecimal balance;
    private String bank;
    private boolean isBanked;
    private String externalAccount;
    private long mas;
    private boolean isActive;
    private boolean isRegistered;
    private String profile;
    private String accountNumber;
    private boolean isDefault;
    private boolean isSwitchDefault;
    private Boolean isDeleted;

    public Account() {
        //
    }

    public Account(MPAY_MobileAccount mobileAccount) {
        if (mobileAccount == null)
            throw new NullArgumentException("mobileAccount");
        this.bank = mobileAccount.getBank().getName();
        this.isBanked = mobileAccount.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED);
        this.externalAccount = mobileAccount.getExternalAcc();
        this.mas = mobileAccount.getMas();
        this.isActive = mobileAccount.getIsActive();
        this.isRegistered = mobileAccount.getIsRegistered();
        this.profile = mobileAccount.getRefProfile().getName();
        if (mobileAccount.getRefAccount() != null){
            this.accountNumber = mobileAccount.getRefAccount().getAccNumber();
            this.balance=mobileAccount.getRefAccount().getBalance();
        }
        this.isDefault = mobileAccount.getIsDefault();
        this.isSwitchDefault = mobileAccount.getIsSwitchDefault();
        this.isDeleted = mobileAccount.getDeletedFlag();
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public boolean isBanked() {
        return isBanked;
    }

    public void setBanked(boolean isBanked) {
        this.isBanked = isBanked;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }

    public long getMas() {
        return mas;
    }

    public void setMas(long mas) {
        this.mas = mas;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public boolean isSwitchDefault() {
        return isSwitchDefault;
    }

    public void setSwitchDefault(boolean isSwitchDefault) {
        this.isSwitchDefault = isSwitchDefault;
    }
}
