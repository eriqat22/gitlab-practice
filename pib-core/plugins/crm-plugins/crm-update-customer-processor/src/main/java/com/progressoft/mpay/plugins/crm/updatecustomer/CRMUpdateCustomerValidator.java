package com.progressoft.mpay.plugins.crm.updatecustomer;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import java.util.Calendar;

public class CRMUpdateCustomerValidator {
    private CRMUpdateCustomerValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");

        CRMUpdateCustomerMessage message = (CRMUpdateCustomerMessage) context.getRequest();

        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;


        result = validateBasicInfo(context, message);
        if (!result.isValid())
            return result;

        result = validateRegisteredMobile(context);
        if (!result.isValid())
            return result;

        if (!StringUtils.isBlank(message.getAlias())) {
            result = validateAlias(context, message);
            if (!result.isValid())
                return result;
        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }


    private static ValidationResult validateBasicInfo(MessageProcessingContext context, CRMUpdateCustomerMessage message) {
        //DONE
        ValidationResult result;
        if (!StringUtils.isBlank(message.getIdTypeCode())) {
            result = validateIdType(context);
            if (!result.isValid())
                return result;
        }
        if (message.getDateOfBirth() != null) {
            result = validateDateOfBirth(context, message);
            if (!result.isValid())
                return result;
        }

        if (!StringUtils.isBlank(message.getCountryCode())) {
            result = validateCountry(context);
            if (!result.isValid())
                return result;
        }
        if (!StringUtils.isBlank(message.getAreaNumber())) {
            result = validateCityAndCityArea(context);
            if (!result.isValid())
                return result;
        }
        if (message.getPrefLanguage() != null) {
            result = validateLanguage(context);
            if (!result.isValid())
                return result;
        }

        if (!StringUtils.isBlank(message.getProfileCode())) {
            result = validateProfile(context);
            if (!result.isValid())
                return result;
        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateDateOfBirth(ProcessingContext context, CRMUpdateCustomerMessage message) {
        Calendar now = Calendar.getInstance();

        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        MPAY_SysConfig r = context.getLookupsLoader().getSystemConfigurations("Min Age");
        long minAge = Integer.parseInt(r.getConfigValue());

        if (DateUtils.addYears(message.getDateOfBirth(), (int) minAge).after(now.getTime()))
            return new ValidationResult(ReasonCodes.CUSTOMER_AGE_NOT_ALLOWED, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateIdType(MessageProcessingContext context) {
        MPAY_IDType idType = (MPAY_IDType) context.getExtraData().get(Constants.IdTypeKey);
        if (idType == null || !idType.getIsCustomer())
            return new ValidationResult(ReasonCodes.INVALID_ID_TYPE, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCountry(MessageProcessingContext context) {
        JfwCountry country = (JfwCountry) context.getExtraData().get(Constants.CountryKey);
        if (country == null)
            return new ValidationResult(ReasonCodes.INVALID_NATIONALITY, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCityAndCityArea(MessageProcessingContext context) {
        MPAY_City city = (MPAY_City) context.getExtraData().get(Constants.CityKey);
        MPAY_CityArea area = (MPAY_CityArea) context.getExtraData().get(Constants.CityAreaKey);
        if (city == null || area == null)
            return new ValidationResult(ReasonCodes.CITY_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateLanguage(MessageProcessingContext context) {
        MPAY_Language language = (MPAY_Language) context.getExtraData().get(Constants.LanguageKey);
        if (language == null)
            return new ValidationResult(ReasonCodes.LANGUAGE_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateRegisteredMobile(ProcessingContext context) {
        MPAY_CustomerMobile mobile = (MPAY_CustomerMobile) context.getExtraData().get("customerMobile");
        if (mobile == null)
            return new ValidationResult(ReasonCodes.INVALID_MOBILE_NUMBER, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateAlias(ProcessingContext context, CRMUpdateCustomerMessage message) {
        MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(message.getAlias(), ReceiverInfoType.ALIAS);
        if (mobile != null)
            return new ValidationResult(ReasonCodes.ALIAS_IS_DEFINED, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateProfile(MessageProcessingContext context) {
        MPAY_Profile profile = (MPAY_Profile) context.getExtraData().get(Constants.ProfileKey);
        if (profile == null)
            return new ValidationResult(ReasonCodes.PROFILE_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

}
