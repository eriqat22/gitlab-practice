package com.progressoft.mpay.plugins.crm.updatecustomer;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CRMUpdateCustomerProcessor implements MessageProcessor {

    private static final String IS_NOT_BENEFICARY = "2";
    private static final String IS_BENEFICARY = "1";
    private static final Logger logger = LoggerFactory.getLogger(CRMUpdateCustomerProcessor.class);

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = CRMUpdateCustomerValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
                    e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }


    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }


    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatus, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatus);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        logger.debug("Inside AcceptMessage ...");
        MPAY_Customer customer = getCustomerFromRequest(context);
        context.getDataProvider().updateCustomer(customer);
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private boolean isNotEmpty(String value) {
        return !StringUtils.isBlank(value);
    }

    private MPAY_Customer getCustomerFromRequest(MessageProcessingContext context) {
        CRMUpdateCustomerMessage message = (CRMUpdateCustomerMessage) context.getRequest();
        MPAY_CustomerMobile customerMobile = (MPAY_CustomerMobile) context.getExtraData().get("customerMobile");
        MPAY_Customer customer = customerMobile.getRefCustomer();

        if (isNotEmpty(message.getFirstName()))
            customer.setFirstName(message.getFirstName());
        if (isNotEmpty(message.getMidName()))
            customer.setMiddleName(message.getMidName());
        if (isNotEmpty(message.getLastName()))
            customer.setLastName(message.getLastName());

        customer.setFullName(customer.getFirstName() + " " + customer.getMiddleName() + " " + customer.getLastName());

        customer.setIsActive(true);


        JfwCountry nationality = (JfwCountry) context.getExtraData().get(Constants.CountryKey);
        if (nationality != null)
            customer.setNationality(nationality);

        MPAY_IDType idType = (MPAY_IDType) context.getExtraData().get(Constants.IdTypeKey);
        if (idType != null)
            customer.setIdType(idType);

        if (isNotEmpty(message.getIdNumber()))
            customer.setIdNum(message.getIdNumber());

        if (message.getDateOfBirth() != null)
            customer.setDob(message.getDateOfBirth());

        if (isNotEmpty(message.getReference()))
            customer.setClientRef(message.getReference());


        MPAY_City city = (MPAY_City) context.getExtraData().get(Constants.CityKey);
        if (city != null)
            customer.setCity(city);

        MPAY_Language prefLang = (MPAY_Language) context.getExtraData().get(Constants.LanguageKey);
        if (prefLang != null)
            customer.setPrefLang(prefLang);

        if (isNotEmpty(message.getAlias()))
            customer.setAlias(message.getAlias());

        if (isNotEmpty(message.getExternalAccount()))
            customer.setExternalAcc(message.getExternalAccount());

        MPAY_Profile refProfile = (MPAY_Profile) context.getExtraData().get(Constants.ProfileKey);
        if (refProfile != null)
            customer.setRefProfile(refProfile);




        if (isNotEmpty(message.getIdentificationRefrence()))
            customer.setIdentificationReference(message.getIdentificationRefrence());
        if (isNotEmpty(message.getIdentificationCard()))
            customer.setIdentificationCard(message.getIdentificationCard());
        if (message.getIdentificationCardIssuanceDate() != null)
            customer.setIdCardIssuanceDate(message.getIdentificationCardIssuanceDate());
        if (isNotEmpty(message.getPassportId()))
            customer.setPassportId(message.getPassportId());
        if (message.getPassportIssuanceDate() != null)
            customer.setPassportIssuanceDate(message.getPassportIssuanceDate());
        if (isNotEmpty(message.getNote()))
            customer.setNote(message.getNote());
        if (isNotEmpty(message.getAddress()))
            customer.setAddress(message.getAddress());
        if (message.getMaxNumberOfDevices() != null)
            customer.setMaxNumberOfDevices(message.getMaxNumberOfDevices());

        if (isNotEmpty(message.getEmail()))
            customer.setEmail(message.getEmail());
        if (isNotEmpty(message.getFullArabicName()))
            customer.setArabicFullName(message.getFullArabicName());
        if (isNotEmpty(message.getFullEnglishName()))
            customer.setEnglishFullName(message.getFullEnglishName());
        if (isNotEmpty(message.getGender()))
            customer.setGender(message.getGender());
        if (isNotEmpty(message.getPhone1()))
            customer.setPhoneOne(message.getPhone1());
        if (isNotEmpty(message.getPhone2()))
            customer.setPhoneTwo(message.getPhone2());
        if (isNotEmpty(message.getZipCode()))
            customer.setZipCode(message.getZipCode());
        if (isNotEmpty(message.getBuildingNumber()))
            customer.setBuildingNum(message.getBuildingNumber());
        if (isNotEmpty(message.getStreetName()))
            customer.setStreetName(message.getStreetName());

        MPAY_CityArea area = (MPAY_CityArea) context.getExtraData().get(Constants.CityAreaKey);
        if (area != null)
            customer.setArea(area);

        if (isNotEmpty(message.getCustomerMobileBranch()))
            customer.setBankBranch(message.getCustomerMobileBranch());
        if (isNotEmpty(message.getExternalAccount()))
            customer.setExternalAcc(message.getExternalAccount());
        if (isNotEmpty(message.getNfcSerial()))
            customer.setNfcSerial(message.getNfcSerial());
        if (isNotEmpty(message.getIban()))
            customer.setIban(message.getIban());
        if (isNotEmpty(message.getReasonForResidence()))
            customer.setReasonForResidence(message.getReasonForResidence());
        return customer;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        CRMUpdateCustomerMessage message = CRMUpdateCustomerMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), ReceiverInfoType.CORPORATE));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        context.getExtraData().put("customerMobile", context.getDataProvider().getCustomerMobile(message.getMobileNumber()));
        loadLookups(context, message);
    }

    private void loadLookups(MessageProcessingContext context, CRMUpdateCustomerMessage message) {

        if (message.getIdTypeCode() != null && !StringUtils.isEmpty(message.getIdTypeCode())) {
            MPAY_IDType idType = context.getLookupsLoader().getIDType(message.getIdTypeCode());
            if (idType == null)
                return;
            context.getExtraData().put(Constants.IdTypeKey, idType);
        }

        if (message.getCountryCode() != null && !StringUtils.isEmpty(message.getCountryCode())) {
            JfwCountry country = context.getLookupsLoader().getCountry(message.getCountryCode());
            if (country == null)
                return;
            context.getExtraData().put(Constants.CountryKey, country);
        }
        if (message.getCityCode() != null && !StringUtils.isEmpty(message.getCityCode())) {
            MPAY_City city = context.getLookupsLoader().getCity(message.getCityCode());
            if (city == null)
                return;
            context.getExtraData().put(Constants.CityKey, city);
        }
        if (message.getAreaNumber() != null && !StringUtils.isEmpty(message.getAreaNumber())) {
            MPAY_CityArea area = context.getLookupsLoader().getAreaByCode(message.getAreaNumber());
            context.getExtraData().put(Constants.CityAreaKey, area);
        }

        if (message.getPrefLanguage() != null) {
            MPAY_Language language = context.getLookupsLoader().getLanguageByCode(String.valueOf(
                    LanguageMapper.getInstance().getLanguageLocaleCode(String.valueOf(message.getPrefLanguage()))));
            if (language == null)
                return;
            context.getExtraData().put(Constants.LanguageKey, language);
        }

        if (message.getProfileCode() != null && !StringUtils.isEmpty(message.getProfileCode())) {
            MPAY_Profile profile = context.getLookupsLoader().getProfile(message.getProfileCode());
            if (profile == null)
                return;
            context.getExtraData().put(Constants.ProfileKey, profile);
        }
        if (message.getIsBeneficiary() != null && !StringUtils.isEmpty(message.getIsBeneficiary())) {
            if (message.getIsBeneficiary().equals(IS_NOT_BENEFICARY)) {
                MPAY_IDType benefIdType = context.getLookupsLoader().getIDType(message.getBeneficiaryIdType());
                if (benefIdType == null)
                    return;
                context.getExtraData().put(Constants.beneficaryIdTypeKey, benefIdType);
            }
        }
    }


}
