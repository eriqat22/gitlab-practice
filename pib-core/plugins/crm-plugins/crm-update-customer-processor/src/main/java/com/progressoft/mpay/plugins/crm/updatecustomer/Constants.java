package com.progressoft.mpay.plugins.crm.updatecustomer;

public class Constants {
	public static final String IdTypeKey = "IdTypeKey";
	public static final String beneficaryIdTypeKey = "benefIdType";
	public static final String CountryKey = "CountryKey";
	public static final String CityKey = "CityKey";
	public static final String CityAreaKey = "CityAreaKey";
	public static final String LanguageKey = "LanguageKey";
	public static final String ProfileKey = "ProfileKey";
	public static final String BirthDateFormat = "dd/MM/yyyy";

}
