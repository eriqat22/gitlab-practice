package com.progressoft.mpay.plugins.crm.unsuspendcustomermobile;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

public class CRMUnSuspendCustomerMobileMessage extends MPayRequest {
    private static final String MOBILE_NUMBER_KEY = "mobileNumber";


    private String mobileNumber;

    public CRMUnSuspendCustomerMobileMessage() {
        //
    }

    public CRMUnSuspendCustomerMobileMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static CRMUnSuspendCustomerMobileMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CRMUnSuspendCustomerMobileMessage message = new CRMUnSuspendCustomerMobileMessage(request);


        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        message.setMobileNumber(message.getValue(MOBILE_NUMBER_KEY));
        if (message.getMobileNumber() == null || message.getMobileNumber().isEmpty())
            throw new MessageParsingException(MOBILE_NUMBER_KEY);
        return message;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}
