package com.progressoft.mpay.plugins.crm.customerregistration;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.*;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.registration.customers.CustomerWorkflowStatuses;
import com.progressoft.mpay.registration.customers.PostApproveCustomer;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class CRMCustomerRegistrationProcessor implements MessageProcessor {

    private static final String IS_NOT_BENEFICARY = "2";
    private static final String IS_BENEFICARY = "1";
    private static final Logger logger = LoggerFactory.getLogger(CRMCustomerRegistrationProcessor.class);

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = CRMCustomerRegistrationValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
                    e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }


    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    private MPAY_Bank loadBank(ProcessingContext context) {
        return context.getLookupsLoader().getBankByCode(context.getSystemParameters().getRegistrationBankCode());
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatus,
                                                  String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatus);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                 String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws InvalidActionException {
        logger.debug("Inside AcceptMessage ...");
        MPAY_Customer customer = getCustomerFromRequest(context);
        registerCustomer(customer, context);
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null,
                ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);

        if (SystemParameters.getInstance().getEnableRegistrationOTP().equals("1")) {
            MPAY_RegistrationOTP regOtp = context.getDataProvider()
                    .getRegOtpByCode((String) context.getExtraData().get(PluginsCommonConstants.OTP_KEY));
            DataProvider.instance().deleteRegOTP(regOtp);
        }
        return result;
    }

    private void registerCustomer(MPAY_Customer customer, ProcessingContext context) throws InvalidActionException {
        JfwHelper.createEntity(MPAYView.CUSTOMER, customer);
        Map<String, Object> v = new HashMap<>();
        v.put(WorkflowService.WF_ARG_BEAN, customer);
        context.getDataProvider().updateWorkflowStep(CustomerWorkflowStatuses.INTEGRATION, customer.getWorkflowId());
        customer.setStatusId(context.getDataProvider().getWorkflowStatus(CustomerWorkflowStatuses.INTEGRATION));
        context.getDataProvider().updateCustomer(customer);
        PostApproveCustomer approveCustomerPostFunction = AppContext.getApplicationContext()
                .getBean("PostApproveCustomer", PostApproveCustomer.class);
        try {
            approveCustomerPostFunction.execute(v, null, null);
            MPAY_MpClearIntegMsgLog refLastMsgLog = customer.getRefLastMsgLog();
            refLastMsgLog.setRefMessage(context.getMessage());
        } catch (WorkflowException e) {
            throw new BusinessException(e);
        }
    }

    private MPAY_Customer getCustomerFromRequest(MessageProcessingContext context) {
        CRMCustomerRegistrationMessage message = (CRMCustomerRegistrationMessage) context.getRequest();
        MPAY_Customer customer = new MPAY_Customer();
        customer.setFirstName(message.getFirstName());
        customer.setMiddleName(message.getMidName());
        customer.setLastName(message.getLastName());
        customer.setFullName(customer.getFirstName() + " " + customer.getMiddleName() + " " + customer.getLastName());
        customer.setIsActive(true);
        customer.setNationality((JfwCountry) context.getExtraData().get(Constants.CountryKey));
        customer.setIdType((MPAY_IDType) context.getExtraData().get(Constants.IdTypeKey));
        customer.setIdNum(message.getIdNumber());
        customer.setDob(message.getDateOfBirth());
        customer.setClientType(context.getLookupsLoader().getCustomerClientType());
        customer.setClientRef(message.getReference());
        customer.setIsRegistered(false);
        customer.setCity((MPAY_City) context.getExtraData().get(Constants.CityKey));
        customer.setPrefLang((MPAY_Language) context.getExtraData().get(Constants.LanguageKey));
        customer.setMobileNumber(message.getSender());
        customer.setAlias(message.getAlias());
        String bankedUnbanked = context.getSender().getBank().getSettlementParticipant().equals(BankType.PARTICIPANT)
                ? BankedUnbankedFlag.BANKED
                : BankedUnbankedFlag.UNBANKED;
        customer.setBankedUnbanked(bankedUnbanked);
        customer.setBank(context.getSender().getBank());
        customer.setExternalAcc("#####");
        customer.setRefProfile((MPAY_Profile) context.getExtraData().get(Constants.ProfileKey));
        customer.setNotificationShowType(NotificationShowTypeCodes.MOBILE_NUMBER);
        customer.setHint(String.valueOf(context.getMessage().getId()));
        customer.setIdentificationReference(message.getIdentificationReference());
        customer.setIdentificationCard(message.getIdentificationCard());
        customer.setIdCardIssuanceDate(message.getIdentificationCardIssuanceDate());
        customer.setPassportId(message.getPassportId());
        customer.setPassportIssuanceDate(message.getPassportIssuanceDate());
        customer.setNote(message.getNote());
        customer.setCreatedBy(message.getRequestedId());
        customer.setIsActive(false);
        customer.setMas(1L);
        customer.setAddress(message.getAddress());
        customer.setMaxNumberOfDevices(0L);

        if (context.getSystemParameters().getIsCustomerSetActiveOrNot())
            customer.setIsActive(true);

        customer.setEmail(message.getEmail());
        customer.setArabicFullName(message.getFullArabicName());
        customer.setEnglishFullName(message.getFullEnglishName());
        customer.setGender(message.getGender());
        customer.setPhoneOne(message.getPhone1());
        customer.setPhoneTwo(message.getPhone2());
        customer.setZipCode(message.getZipCode());
        customer.setBuildingNum(message.getBuildingNumber());
        customer.setStreetName(message.getStreetName());
        customer.setArea((MPAY_CityArea) context.getExtraData().get(Constants.CityAreaKey));
        customer.setCreatedBy(message.getRequestedId());
        customer.setBankBranch(message.getCustomerMobileBranch());
        customer.setExternalAcc(message.getExternalAccount());
        customer.setNfcSerial(message.getNfcSerial());
        customer.setIban(message.getIban());
        customer.setReasonForResidence(message.getReasonForResidence());
        customer.setIsBlackListed(false);
        return customer;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        CRMCustomerRegistrationMessage message = CRMCustomerRegistrationMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        OTPHanlder.loadOTP(context, message.getPin());
        context.getExtraData().put(PluginsCommonConstants.OTP_KEY, message.getOtp());
        MPAY_Bank bank = loadBank(context);
        if (bank == null)
            return;
        sender.setBank(bank);
        loadLookups(context, message);
    }

    private void loadLookups(MessageProcessingContext context, CRMCustomerRegistrationMessage message) {
        MPAY_IDType idType = context.getLookupsLoader().getIDType(message.getIdTypeCode());
        if (idType == null)
            return;
        context.getExtraData().put(Constants.IdTypeKey, idType);
        JfwCountry country = context.getLookupsLoader().getCountry(message.getCountryCode());
        if (country == null)
            return;
        context.getExtraData().put(Constants.CountryKey, country);
        MPAY_City city = context.getLookupsLoader().getCity(message.getCityCode());
        if (city == null)
            return;
        context.getExtraData().put(Constants.CityKey, city);
        MPAY_Language language = context.getLookupsLoader().getLanguageByCode(String.valueOf(
                LanguageMapper.getInstance().getLanguageLocaleCode(String.valueOf(message.getPrefLanguage()))));
        MPAY_CityArea area = context.getLookupsLoader().getAreaByCode(message.getAreaNumber());
        context.getExtraData().put(Constants.CityAreaKey, area);
        if (language == null)
            return;
        context.getExtraData().put(Constants.LanguageKey, language);
        MPAY_Profile profile = context.getLookupsLoader().getProfile(message.getProfileCode());
        if (profile == null)
            return;
        context.getExtraData().put(Constants.ProfileKey, profile);
        if (message.getIsBeneficiary().equals(IS_NOT_BENEFICARY)) {
            MPAY_IDType benefIdType = context.getLookupsLoader().getIDType(message.getBeneficiaryIdType());
            if (benefIdType == null)
                return;
            context.getExtraData().put(Constants.beneficaryIdTypeKey, benefIdType);
        }
    }


}
