package com.progressoft.mpay.plugins.crm.customerregistration;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CRMCustomerRegistrationMessage extends MPayRequest {
    private static final Logger logger = LoggerFactory.getLogger(CRMCustomerRegistrationMessage.class);
    private static final String IS_NOT_BENEFICIARY = "2";
    private static final String IS_BENEFICIARY = "1";

    private static final String FIRST_NAME_KEY = "firstName";
    private static final String MID_NAME_KEY = "midName";
    private static final String LAST_NAME_KEY = "lastName";
    private static final String ID_TYPE_CODE_KEY = "idTypeCode";
    private static final String ID_NUMBER_KEY = "idNumber";
    private static final String DATE_OF_BIRTH_KEY = "dateOfBirth";
    private static final String REFERENCE_KEY = "reference";
    private static final String COUNTRY_CODE_KEY = "countryCode";
    private static final String CITY_CODE_KEY = "cityCode";
    private static final String PREF_LANGUAGE_KEY = "prefLanguage";
    private static final String ALIAS_KEY = "alias";
    private static final String ADDRESS_KEY = "address";

    private static final String IDENTIFICATION_REFRENCE = "identificationRefrence";
    private static final String IDENTIFICATION_CARD = "identificationCard";
    private static final String IDENTIFICATION_CARD_ISSUANCE_DATE = "identificationCardIssuanceDate";
    private static final String PASSPORT_ID = "passportId";
    private static final String PASSPORT_ISSUANCE_DATE = "passportIssuanceDate";
    private static final String OTP = "otp";
    private static final String NOTE = "note";


    private static final String FULL_ENGLISH_NAME = "fullEnglishName";
    private static final String FULL_ARABIC_NAME = "fullArabicName";
    private static final String GENDER = "gender";
    private static final String PHONE1 = "phone1";
    private static final String PHONE2 = "phone2";
    private static final String AREA_NUMBER = "areaNumber";
    private static final String EMAIL = "email";
    private static final String ZIP_CODE = "zipCode";
    private static final String BUILDING_NUMBER = "buildingNumber";
    private static final String STREET_NAME = "streetName";
    private static final String CUSTOMER_MOBILE_BRANCH = "customerMobileBranch";
    private static final String EXTERNAL_ACCOUNT = "externalAccount";
    private static final String NFC_SERIAL_NUMBER = "nfcSerialNumber";
    private static final String MAX_NUMBER_OF_DEVICES = "maxNumberOfDevices";
    private static final String PROFILE_CODE = "profileCode";
    private static final String IBAN = "iban";
    private static final String TRANSACTION_SIZE = "transactionSize";
    private static final String CHARGE_AMOUNT_NUMBER = "chargeAmountNumber";
    private static final String CHARGE_AMOUNT_CHARACTERS = "chargeAmountCharacters";
    private static final String WALLET_ACCOUNT = "walletAccount";

    private static final String PERMIT_ID = "permitId";
    private static final String PERMIT_EXPIRY = "permitExpiry";
    private static final String OTHER_NUMBER = "otherNumber";
    private static final String WORK_NATURE = "workNature";
    private static final String RISK_PROFILE = "riskProfile";
    private static final String JOB_TITLE = "jobTitle";
    private static final String OTHER_BANK = "otherBank";
    private static final String WORK_PLACE = "workPlace";
    private static final String OTHER_INCOME_RESOURCE = "otherIncomeResource";
    private static final String REASON_FOR_RESIDENCE = "reasonForResidence";
    private static final String PSP_USES = "pspUses";
    private static final String PSP_USES_OTHER = "pspUsesOther";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(Constants.BirthDateFormat);
    private String permitId;
    private Date permitExpiry;
    private String otherNumber;
    private String workNature;
    private String riskProfile;
    private String jobTitle;
    private String otherBank;
    private String workPlace;
    private String otherIncomeResource;
    private String reasonForResidence;
    private String pspUses;
    private String pspUsesOther;
    private String permitNumber;
    private Date benefIdExpiry;
    private String iban;
    private String transactionSize;
    private BigDecimal chargeAmountNumber;
    private String chargeAmountCharacters;
    private String walletAccount;
    private String firstName;
    private String midName;
    private String lastName;
    private String idTypeCode;
    private String idNumber;
    private Date dateOfBirth;
    private String reference;
    private String countryCode;
    private String cityCode;
    private long prefLanguage;
    private String alias;
    private String address;
    private String otp;
    private String isBeneficiary;
    private String beneficiaryName;
    private String beneficiaryId;
    private String beneficiaryIdType;
    private String beneficiaryEmail;
    private String guardian;
    private String guardianEmail;
    private String identificationReference;
    private String identificationCard;
    private Date identificationCardIssuanceDate;
    private String passportId;
    private Date passportIssuanceDate;
    private String note;
    private String fullEnglishName;
    private String fullArabicName;
    private String gender;
    private String phone1;
    private String phone2;
    private String areaNumber;
    private String email;
    private String zipCode;
    private String buildingNumber;
    private String streetName;
    private String customerMobileBranch;
    private String externalAccount;
    private String nfcSerial;
    private Long maxNumberOfDevices;
    private String profileCode;

    public CRMCustomerRegistrationMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setRequestedId(request.getRequestedId());
        setExtraData(request.getExtraData());
    }

    public static CRMCustomerRegistrationMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CRMCustomerRegistrationMessage message = new CRMCustomerRegistrationMessage(request);
        fillGenericFields(message);
        fillCustomerInfo(message);
        message.setReference(message.getValue(REFERENCE_KEY));
        if (message.getReference() == null || message.getReference().trim().length() == 0)
            throw new MessageParsingException(REFERENCE_KEY);
        message.setCountryCode(message.getValue(COUNTRY_CODE_KEY));
        if (message.getCountryCode() == null || message.getCountryCode().trim().length() == 0)
            throw new MessageParsingException(COUNTRY_CODE_KEY);
        message.setCityCode(message.getValue(CITY_CODE_KEY));
        if (message.getCityCode() == null || message.getCityCode().trim().length() == 0)
            throw new MessageParsingException(CITY_CODE_KEY);
        try {
            message.setPrefLanguage(Long.parseLong(message.getValue(PREF_LANGUAGE_KEY)));
        } catch (Exception e) {
            logger.debug("Invalid prefLanguage value", e);
            throw new MessageParsingException(PREF_LANGUAGE_KEY);
        }
        message.setAlias(message.getValue(ALIAS_KEY));
        message.setAddress(message.getValue(ADDRESS_KEY));
        message.setOtp(message.getValue(OTP));
        checkIfOTPIsEnabled(message);


        if (message.getValue(PERMIT_EXPIRY) != null && !message.getValue(PERMIT_EXPIRY).isEmpty()) {
            try {
                message.setPermitExpiry(DATE_FORMAT.parse(message.getValue(PERMIT_EXPIRY)));
            } catch (Exception e) {
                logger.debug("Invalid PERMIT_EXPIRY date format", e);
                throw new MessageParsingException(PERMIT_EXPIRY);
            }
        }

        message.setPermitId(message.getValue(PERMIT_ID));
        message.setOtherNumber(message.getValue(OTHER_NUMBER));
        message.setWorkNature((message.getValue(WORK_NATURE)));
        message.setRiskProfile(message.getValue(RISK_PROFILE));
        message.setJobTitle(message.getValue(JOB_TITLE));
        message.setOtherBank(message.getValue(OTHER_BANK));
        message.setWorkPlace(message.getValue(WORK_PLACE));
        message.setOtherIncomeResource(message.getValue(OTHER_INCOME_RESOURCE));
        message.setReasonForResidence(message.getValue(REASON_FOR_RESIDENCE));
        message.setPspUses(message.getValue(PSP_USES));
        message.setPspUsesOther(message.getValue(PSP_USES_OTHER));

        return message;
    }


    private static void fillCustomerInfo(CRMCustomerRegistrationMessage message) throws MessageParsingException {
        message.setFirstName(message.getValue(FIRST_NAME_KEY));
        if (StringUtils.isEmpty(message.getFirstName()))
            throw new MessageParsingException(FIRST_NAME_KEY);
        message.setMidName(message.getValue(MID_NAME_KEY));
        if (StringUtils.isEmpty(message.getMidName()))
            throw new MessageParsingException(MID_NAME_KEY);
        message.setLastName(message.getValue(LAST_NAME_KEY));
        if (StringUtils.isEmpty(message.getLastName()))
            throw new MessageParsingException(LAST_NAME_KEY);
        message.setIdTypeCode(message.getValue(ID_TYPE_CODE_KEY));
        if (StringUtils.isEmpty(message.getIdTypeCode()))
            throw new MessageParsingException(ID_TYPE_CODE_KEY);
        message.setIdNumber(message.getValue(ID_NUMBER_KEY));
        if (StringUtils.isEmpty(message.getIdNumber()))
            throw new MessageParsingException(ID_NUMBER_KEY);
        try {
            message.setDateOfBirth(DATE_FORMAT.parse(message.getValue(DATE_OF_BIRTH_KEY)));
        } catch (Exception e) {
            logger.debug("Invalid Date of Birth format", e);
            throw new MessageParsingException(DATE_OF_BIRTH_KEY);
        }
        message.setIdentificationReference(message.getValue(IDENTIFICATION_REFRENCE));
        message.setIdentificationCard(message.getValue(IDENTIFICATION_CARD));
        if (message.getValue(IDENTIFICATION_CARD_ISSUANCE_DATE) != null && !message.getValue(IDENTIFICATION_CARD_ISSUANCE_DATE).equals(""))
            try {
                message.setIdentificationCardIssuanceDate(DATE_FORMAT.parse(message.getValue(IDENTIFICATION_CARD_ISSUANCE_DATE)));
            } catch (Exception e) {
                logger.debug("Invalid Identification card issuance date format", e);
                throw new MessageParsingException(IDENTIFICATION_CARD_ISSUANCE_DATE);
            }
        if (message.getValue(PASSPORT_ISSUANCE_DATE) != null && !message.getValue(PASSPORT_ISSUANCE_DATE).equals(""))
            try {
                message.setPassportIssuanceDate(DATE_FORMAT.parse(message.getValue(PASSPORT_ISSUANCE_DATE)));
            } catch (Exception e) {
                logger.debug("Invalid passport issuance date format", e);
                throw new MessageParsingException(PASSPORT_ISSUANCE_DATE);
            }
        message.setNote(message.getValue(NOTE));
        message.setPassportId((message.getValue(PASSPORT_ID)));
        message.setFullEnglishName(message.getValue(FULL_ENGLISH_NAME));
        message.setFullArabicName(message.getValue(FULL_ARABIC_NAME));
        message.setGender(message.getValue(GENDER));
        message.setPhone1(message.getValue(PHONE1));
        message.setPhone2(message.getValue(PHONE2));
        message.setAreaNumber(message.getValue(AREA_NUMBER));
        message.setEmail(message.getValue(EMAIL));
        message.setZipCode(message.getValue(ZIP_CODE));
        message.setBuildingNumber(message.getValue(BUILDING_NUMBER));
        message.setStreetName(message.getValue(STREET_NAME));
        message.setExternalAccount(message.getValue(EXTERNAL_ACCOUNT));
        message.setNfcSerial(message.getValue(NFC_SERIAL_NUMBER));
        message.setCustomerMobileBranch(message.getValue(CUSTOMER_MOBILE_BRANCH));
        String numberOfDevice = message.getValue(MAX_NUMBER_OF_DEVICES);
        if (numberOfDevice != null && !numberOfDevice.isEmpty()) {
            try {
                message.setMaxNumberOfDevices(Long.valueOf(numberOfDevice));
            } catch (Exception e) {
                logger.debug("Invalid MAX_NUMBER_OF_DEVICES format", e);
                throw new MessageParsingException(MAX_NUMBER_OF_DEVICES);
            }
        }
        String profileCode = message.getValue(PROFILE_CODE);
        if (profileCode == null)
            throw new MessageParsingException(PROFILE_CODE);
        message.setProfileCode(profileCode);
        message.setIban(message.getValue(IBAN));
        message.setChargeAmountCharacters(message.getValue(CHARGE_AMOUNT_CHARACTERS));

        String chargeAmountNumber = message.getValue(CHARGE_AMOUNT_NUMBER);
        if (chargeAmountNumber != null) {
            try {
                message.setChargeAmountNumber(BigDecimal.valueOf(Double.valueOf(chargeAmountNumber)));
            } catch (NumberFormatException ex) {
                throw new MessageParsingException(CHARGE_AMOUNT_NUMBER);
            }
        }

        message.setWalletAccount(message.getValue(WALLET_ACCOUNT));
        if (message.getValue(TRANSACTION_SIZE) != null && !message.getValue(TRANSACTION_SIZE).isEmpty()) {
            message.setTransactionSize(message.getValue(TRANSACTION_SIZE));
            String transactionSize = message.getTransactionSize();
            if (!transactionSize.equalsIgnoreCase("1") && !transactionSize.equalsIgnoreCase("2") && !transactionSize.equalsIgnoreCase("3"))
                throw new MessageParsingException("Invalid " + TRANSACTION_SIZE);
        }


    }

    private static void fillGenericFields(CRMCustomerRegistrationMessage message) throws MessageParsingException {
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
    }

    private static boolean isMatchedToEmailRegex(String emailString) {
        if (emailString != null && !emailString.isEmpty())
            return emailString.matches(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.EMAIL_REGEX).getConfigValue());
        return false;
    }

    private static void checkIfOTPIsEnabled(CRMCustomerRegistrationMessage message) throws MessageParsingException {
        if (message.getOtp() == null && SystemParameters.getInstance().getEnableRegistrationOTP().equals("1"))
            throw new MessageParsingException(OTP);
    }

    public String getPermitId() {
        return permitId;
    }

    public void setPermitId(String permitId) {
        this.permitId = permitId;
    }

    public Date getPermitExpiry() {
        return permitExpiry;
    }

    public void setPermitExpiry(Date permitExpiry) {
        this.permitExpiry = permitExpiry;
    }

    public String getOtherNumber() {
        return otherNumber;
    }

    public void setOtherNumber(String otherNumber) {
        this.otherNumber = otherNumber;
    }

    public String getWorkNature() {
        return workNature;
    }

    public void setWorkNature(String workNature) {
        this.workNature = workNature;
    }

    public String getRiskProfile() {
        return riskProfile;
    }

    public void setRiskProfile(String riskProfile) {
        this.riskProfile = riskProfile;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getOtherBank() {
        return otherBank;
    }

    public void setOtherBank(String otherBank) {
        this.otherBank = otherBank;
    }

    public String getWorkPlace() {
        return workPlace;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

    public String getOtherIncomeResource() {
        return otherIncomeResource;
    }

    public void setOtherIncomeResource(String otherIncomeResource) {
        this.otherIncomeResource = otherIncomeResource;
    }

    public String getReasonForResidence() {
        return reasonForResidence;
    }

    public void setReasonForResidence(String reasonForResidence) {
        this.reasonForResidence = reasonForResidence;
    }

    public String getPspUses() {
        return pspUses;
    }

    public void setPspUses(String pspUses) {
        this.pspUses = pspUses;
    }

    public String getPspUsesOther() {
        return pspUsesOther;
    }

    public void setPspUsesOther(String pspUsesOther) {
        this.pspUsesOther = pspUsesOther;
    }

    public String getPermitNumber() {
        return permitNumber;
    }

    public void setPermitNumber(String permitNumber) {
        this.permitNumber = permitNumber;
    }

    public Date getBenefIdExpiry() {
        return benefIdExpiry;
    }

    public void setBenefIdExpiry(Date benefIdExpiry) {
        this.benefIdExpiry = benefIdExpiry;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getTransactionSize() {
        return transactionSize;
    }

    public void setTransactionSize(String transactionSize) {
        this.transactionSize = transactionSize;
    }

    public BigDecimal getChargeAmountNumber() {
        return chargeAmountNumber;
    }

    public void setChargeAmountNumber(BigDecimal chargeAmountNumber) {
        this.chargeAmountNumber = chargeAmountNumber;
    }

    public String getChargeAmountCharacters() {
        return chargeAmountCharacters;
    }

    public void setChargeAmountCharacters(String chargeAmountCharacters) {
        this.chargeAmountCharacters = chargeAmountCharacters;
    }

    public String getWalletAccount() {
        return walletAccount;
    }

    public void setWalletAccount(String walletAccount) {
        this.walletAccount = walletAccount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMidName() {
        return midName;
    }

    public void setMidName(String midName) {
        this.midName = midName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdTypeCode() {
        return idTypeCode;
    }

    public void setIdTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public long getPrefLanguage() {
        return prefLanguage;
    }

    public void setPrefLanguage(long prefLanguage) {
        this.prefLanguage = prefLanguage;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getIsBeneficiary() {
        return isBeneficiary;
    }


    public String getBeneficiaryIdType() {
        return beneficiaryIdType;
    }



    public String getIdentificationReference() {
        return identificationReference;
    }

    public void setIdentificationReference(String identificationReference) {
        this.identificationReference = identificationReference;
    }

    public String getIdentificationCard() {
        return identificationCard;
    }

    public void setIdentificationCard(String identificationCard) {
        this.identificationCard = identificationCard;
    }

    public Date getIdentificationCardIssuanceDate() {
        return identificationCardIssuanceDate;
    }

    public void setIdentificationCardIssuanceDate(Date identificationCardIssuanceDate) {
        this.identificationCardIssuanceDate = identificationCardIssuanceDate;
    }

    public String getPassportId() {
        return passportId;
    }

    public void setPassportId(String passportId) {
        this.passportId = passportId;
    }

    public Date getPassportIssuanceDate() {
        return passportIssuanceDate;
    }

    public void setPassportIssuanceDate(Date passportIssuanceDate) {
        this.passportIssuanceDate = passportIssuanceDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getFullEnglishName() {
        return fullEnglishName;
    }

    public void setFullEnglishName(String fullEnglishName) {
        this.fullEnglishName = fullEnglishName;
    }

    public String getFullArabicName() {
        return fullArabicName;
    }

    public void setFullArabicName(String fullArabicName) {
        this.fullArabicName = fullArabicName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getAreaNumber() {
        return areaNumber;
    }

    public void setAreaNumber(String areaNumber) {
        this.areaNumber = areaNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCustomerMobileBranch() {
        return customerMobileBranch;
    }

    public void setCustomerMobileBranch(String customerMobileBranch) {
        this.customerMobileBranch = customerMobileBranch;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }

    public String getNfcSerial() {
        return nfcSerial;
    }

    public void setNfcSerial(String nfcSerial) {
        this.nfcSerial = nfcSerial;
    }

    public Long getMaxNumberOfDevices() {
        return maxNumberOfDevices;
    }

    public void setMaxNumberOfDevices(Long maxNumberOfDevices) {
        this.maxNumberOfDevices = maxNumberOfDevices;
    }

    public String getProfileCode() {
        return profileCode;
    }

    public void setProfileCode(String profileCode) {
        this.profileCode = profileCode;
    }
}
