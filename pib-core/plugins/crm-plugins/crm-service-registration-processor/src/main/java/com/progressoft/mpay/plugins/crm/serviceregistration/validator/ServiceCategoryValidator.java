package com.progressoft.mpay.plugins.crm.serviceregistration.validator;

import com.progressoft.mpay.entities.MPAY_ServicesCategory;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceCategoryValidator {
	private static final Logger logger = LoggerFactory.getLogger(ServiceCategoryValidator.class);

	private ServiceCategoryValidator() {

	}

	public static ValidationResult validate(MPAY_ServicesCategory servicesCategory) {
		logger.debug("Inside Validate ...");
		if (servicesCategory == null)
			return new ValidationResult(ReasonCodes.SERVICE_CATEGORY_NOT_SUPPORTED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
