package com.progressoft.mpay.plugins.crm.serviceregistration;

public class Constants {
    public static final String ID_TYPE="idType";
    public static final String CITY="city";
    public static final String LANGUAGE="lang";
    public static final String PROFILE_KEY="profileKey";
    public static final String BANK = "bank";
    public static final String MESSAGE_TYPE = "messageType";
    public static final String NOTIFICATION_CHANNEL = "notificationChannel";
    public static final String SERVICE_TYPE = "serviceType";
    public static final String SERVICES_CATEGRORY = "servicesCategorie";
    public static final String IS_CORPORATE_ALREADY_EXIST = "isCorporateDublicate";
    public static final String IS_CORPORATE_SERVICE_ALREADY_EXIST = "isCorporateService";
}

