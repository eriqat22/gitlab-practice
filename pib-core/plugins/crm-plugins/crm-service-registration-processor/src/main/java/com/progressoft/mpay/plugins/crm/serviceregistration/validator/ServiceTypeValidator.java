package com.progressoft.mpay.plugins.crm.serviceregistration.validator;

import com.progressoft.mpay.entities.MPAY_ServiceType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceTypeValidator {
	private static final Logger logger = LoggerFactory.getLogger(ServiceTypeValidator.class);

	private ServiceTypeValidator() {

	}

	public static ValidationResult validate(MPAY_ServiceType serviceType) {
		logger.debug("Inside Validate ...");
		if (serviceType == null)
			return new ValidationResult(ReasonCodes.INVALID_SERVICE_TYPE, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
