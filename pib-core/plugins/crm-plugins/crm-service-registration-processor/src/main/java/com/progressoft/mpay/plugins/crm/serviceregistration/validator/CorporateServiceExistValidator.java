package com.progressoft.mpay.plugins.crm.serviceregistration.validator;

import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CorporateServiceExistValidator {
	private static final Logger logger = LoggerFactory.getLogger(CorporateServiceExistValidator.class);

	private CorporateServiceExistValidator() {

	}

	public static ValidationResult validate(MPAY_CorpoarteService corpoarteService) {
		logger.debug("Inside Validate ...");
		if (corpoarteService == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		return new ValidationResult(ReasonCodes.CORPORATE_SERVICE_IS_ALREADY_REGISTERD, null, false);
	}
}
