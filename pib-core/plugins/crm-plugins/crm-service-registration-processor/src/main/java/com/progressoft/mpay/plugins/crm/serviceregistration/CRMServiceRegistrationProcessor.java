package com.progressoft.mpay.plugins.crm.serviceregistration;

import com.progressoft.jfw.integration.JfwFacade;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.registration.corporates.CorporateWorkflowStatuses;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class CRMServiceRegistrationProcessor implements MessageProcessor {
    private static final int APPROVE_CODE = 101;

    private static final Logger logger = LoggerFactory.getLogger(CRMServiceRegistrationProcessor.class);

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = CRMServiceRegistrationValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
                    e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }


    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }


    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatus, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatus);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws InvalidActionException {
        logger.debug("Inside AcceptMessage ...");
        CRMServiceRegistrationMessage message = (CRMServiceRegistrationMessage) context.getRequest();
        MPAY_CorpoarteService service = getCorporateServiceFromRequest(context, message);
        registerCorporateService(service, context);
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private void registerCorporateService(MPAY_CorpoarteService service, ProcessingContext context) throws InvalidActionException {
        JfwHelper.createEntity(MPAYView.CORPORATE_SERVICES, service);
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put(WorkflowService.WF_ARG_BEAN, service);
        context.getDataProvider().updateWorkflowStep(CorporateWorkflowStatuses.INTEGRATION, service.getWorkflowId());
        service.setStatusId(context.getDataProvider().getWorkflowStatus(CorporateWorkflowStatuses.INTEGRATION));
        JfwFacade facade = (JfwFacade) AppContext.getApplicationContext().getBean("defaultJfwFacade");
        facade.executeAction(MPAYView.CORPORATE_SERVICES.viewName, APPROVE_CODE, service, new HashMap<>(), new HashMap<>());
    }

    private MPAY_CorpoarteService getCorporateServiceFromRequest(MessageProcessingContext context, CRMServiceRegistrationMessage message) {
        MPAY_Corporate corporate = (MPAY_Corporate) context.getExtraData().get(Constants.IS_CORPORATE_ALREADY_EXIST);
        MPAY_CorpoarteService service = new MPAY_CorpoarteService();
        service.setRefCorporate(corporate);
        service.setName(message.getServiceName());
        service.setDescription(message.getDescription());
        service.setCity((MPAY_City) context.getExtraData().get(Constants.CITY));
        service.setBankedUnbanked("1");
        service.setBank((MPAY_Bank) context.getExtraData().get(Constants.BANK));
        service.setBranch(message.getBranch());
        service.setExternalAcc(message.getExternalAccount());
        service.setIban(message.getIban());
        service.setPaymentType((MPAY_MessageType) context.getExtraData().get(Constants.MESSAGE_TYPE));
        service.setNotificationChannel((MPAY_NotificationChannel) context.getExtraData().get(Constants.NOTIFICATION_CHANNEL));
        service.setNotificationReceiver(message.getNotificationReceiver());
        service.setRefProfile((MPAY_Profile) context.getExtraData().get(Constants.PROFILE_KEY));
        service.setServiceCategory((MPAY_ServicesCategory) context.getExtraData().get(Constants.SERVICES_CATEGRORY));
        service.setServiceType((MPAY_ServiceType) context.getExtraData().get(Constants.SERVICE_TYPE));
        service.setMaxNumberOfDevices(0L);
        service.setMas(1L);
        service.setAlias(message.getAlias());
        service.setCreatedBy(context.getMessage().getRequestedId());
        return service;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        CRMServiceRegistrationMessage message = CRMServiceRegistrationMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), ReceiverInfoType.CORPORATE));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());

        context.getExtraData().put(Constants.IS_CORPORATE_ALREADY_EXIST, context.getDataProvider().getCorporateByIdTypeAndRegistrationId(message.getIdTypeCode(), message.getIdNumber()));
        context.getExtraData().put(Constants.IS_CORPORATE_SERVICE_ALREADY_EXIST, context.getDataProvider().getCorporateService(message.getServiceName(), ReceiverInfoType.CORPORATE));
        loadLookups(context, message);
    }

    private void loadLookups(MessageProcessingContext context, CRMServiceRegistrationMessage message) {
        context.getExtraData().put(Constants.ID_TYPE, context.getLookupsLoader().getIDType(message.getIdTypeCode()));
        context.getExtraData().put(Constants.CITY, context.getLookupsLoader().getCity(message.getCityCode()));
        context.getExtraData().put(Constants.PROFILE_KEY, context.getLookupsLoader().getProfile(message.getProfileCode()));
        context.getExtraData().put(Constants.BANK, context.getLookupsLoader().getBankByCode(SystemParameters.getInstance().getDefaultBankCode()));
        context.getExtraData().put(Constants.MESSAGE_TYPE, context.getLookupsLoader().getMessageTypes(message.getPaymentTypeId()));
        context.getExtraData().put(Constants.NOTIFICATION_CHANNEL, context.getLookupsLoader().getNotificationChannel(message.getNotificationChannelCode()));
        context.getExtraData().put(Constants.SERVICE_TYPE, context.getLookupsLoader().getServiceType(message.getServiceTypeCode()));
        context.getExtraData().put(Constants.SERVICES_CATEGRORY, context.getLookupsLoader().getServiceCategory(message.getServiceCategoryCode()));
    }
}
