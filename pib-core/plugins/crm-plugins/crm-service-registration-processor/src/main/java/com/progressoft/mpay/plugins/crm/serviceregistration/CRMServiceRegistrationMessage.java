package com.progressoft.mpay.plugins.crm.serviceregistration;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class CRMServiceRegistrationMessage extends MPayRequest {
    private static final Logger logger = LoggerFactory.getLogger(CRMServiceRegistrationMessage.class);

    private static final String ID_NUMBER_KEY = "idNumber";
    private static final String ID_TYPE_CODE = "idTypeCode";
    private static final String SERVICE_NAME = "serviceName";
    private static final String DESCRIPTION = "description";
    private static final String ALIAS = "alias";
    private static final String CITY_CODE = "cityCode";
    private static final String BRANCH = "branch";
    private static final String EXTERNAL_ACCOUNT = "externalAccount";
    private static final String IBAN = "iban";
    private static final String PAYMENT_TYPE_ID = "paymentTypeId";
    private static final String WALLET_TYPE = "walletType";
    private static final String NOTIFICATION_CHANNEL_CODE = "notificationChanelCode";
    private static final String NOTIFICATION_RECIVER = "notificationReciver";
    private static final String PROFILE_CODE = "profileCode";
    private static final String CHARGE_AMOUNT_NUMBER = "chargeAmmountNumber";
    private static final String CHARGE_AMOUNT_CHARACTERS = "chargeAmmountCharacter";
    private static final String WALLET_ACCOUNT = "walletAccount";
    private static final String SERVICE_TYPE_CODE = "serviceTypeCode";
    private static final String SERVICE_CATEGORY_CODE = "serviceCategoryCode";


    private String idNumber;
    private String IdTypeCode;
    private String serviceName;
    private String description;
    private String alias;
    private String cityCode;
    private String branch;
    private String externalAccount;
    private String iban;
    private Long paymentTypeId;
    private String walletType;
    private String notificationChannelCode;
    private String notificationReceiver;
    private String profileCode;
    private BigDecimal chargeAmountNumber;
    private String chargeAmountCharacters;
    private String walletAccount;
    private String serviceTypeCode;
    private String serviceCategoryCode;

    public CRMServiceRegistrationMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setRequestedId(request.getRequestedId());
        setExtraData(request.getExtraData());
    }

    public static CRMServiceRegistrationMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CRMServiceRegistrationMessage message = new CRMServiceRegistrationMessage(request);
        fillGenericFields(message);


        message.setIdNumber(message.getValue(ID_NUMBER_KEY));
        if (message.getIdNumber() == null)
            throw new MessageParsingException(ID_NUMBER_KEY);
        message.setIdTypeCode(message.getValue(ID_TYPE_CODE));
        if (message.getIdTypeCode() == null)
            throw new MessageParsingException(ID_TYPE_CODE);
        message.setServiceName(message.getValue(SERVICE_NAME));
        if (message.getServiceName() == null)
            throw new MessageParsingException(SERVICE_NAME);
        message.setDescription(message.getValue(DESCRIPTION));
        if (message.getDescription() == null)
            throw new MessageParsingException(DESCRIPTION);
        message.setAlias(message.getValue(ALIAS));
        message.setCityCode(message.getValue(CITY_CODE));
        message.setBranch(message.getValue(BRANCH));
        message.setExternalAccount(message.getValue(EXTERNAL_ACCOUNT));
        message.setIban(message.getValue(IBAN));
        message.setPaymentTypeId(Long.valueOf(message.getValue(PAYMENT_TYPE_ID)));
        if (message.getPaymentTypeId() == null)
            throw new MessageParsingException(PAYMENT_TYPE_ID);
        message.setWalletType(message.getValue(WALLET_TYPE));
        message.setNotificationChannelCode(message.getValue(NOTIFICATION_CHANNEL_CODE));
        if (message.getNotificationChannelCode() == null)
            throw new MessageParsingException(NOTIFICATION_CHANNEL_CODE);
        message.setNotificationReceiver(message.getValue(NOTIFICATION_RECIVER));
        message.setProfileCode(message.getValue(PROFILE_CODE));
        if (message.getProfileCode() == null)
            throw new MessageParsingException(PROFILE_CODE);

        String chargeAmountNumber = message.getValue(CHARGE_AMOUNT_NUMBER);
        if (chargeAmountNumber != null && !chargeAmountNumber.isEmpty())
            message.setChargeAmountNumber(new BigDecimal(chargeAmountNumber));
        message.setChargeAmountCharacters(message.getValue(CHARGE_AMOUNT_CHARACTERS));
        message.setWalletAccount(message.getValue(WALLET_ACCOUNT));
        message.setServiceCategoryCode(message.getValue(SERVICE_CATEGORY_CODE));
        if (message.getServiceCategoryCode() == null)
            throw new MessageParsingException(SERVICE_CATEGORY_CODE);
        message.setServiceTypeCode(message.getValue(SERVICE_TYPE_CODE));
        if (message.getServiceTypeCode() == null)
            throw new MessageParsingException(SERVICE_TYPE_CODE);
        return message;
    }

    private static final void fillGenericFields(CRMServiceRegistrationMessage message) throws MessageParsingException {
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE) || message.getSenderType().trim().equals(ReceiverInfoType.ALIAS)))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
    }

    public BigDecimal getChargeAmountNumber() {
        return chargeAmountNumber;
    }

    public void setChargeAmountNumber(BigDecimal chargeAmountNumber) {
        this.chargeAmountNumber = chargeAmountNumber;
    }

    public String getServiceTypeCode() {
        return serviceTypeCode;
    }

    public void setServiceTypeCode(String serviceTypeCode) {
        this.serviceTypeCode = serviceTypeCode;
    }

    public String getServiceCategoryCode() {
        return serviceCategoryCode;
    }

    public void setServiceCategoryCode(String serviceCategoryCode) {
        this.serviceCategoryCode = serviceCategoryCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdTypeCode() {
        return IdTypeCode;
    }

    public void setIdTypeCode(String idTypeCode) {
        IdTypeCode = idTypeCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Long getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getNotificationChannelCode() {
        return notificationChannelCode;
    }

    public void setNotificationChannelCode(String notificationChannelCode) {
        this.notificationChannelCode = notificationChannelCode;
    }

    public String getNotificationReceiver() {
        return notificationReceiver;
    }

    public void setNotificationReceiver(String notificationReceiver) {
        this.notificationReceiver = notificationReceiver;
    }

    public String getProfileCode() {
        return profileCode;
    }

    public void setProfileCode(String profileCode) {
        this.profileCode = profileCode;
    }


    public String getChargeAmountCharacters() {
        return chargeAmountCharacters;
    }

    public void setChargeAmountCharacters(String chargeAmountCharacters) {
        this.chargeAmountCharacters = chargeAmountCharacters;
    }

    public String getWalletAccount() {
        return walletAccount;
    }

    public void setWalletAccount(String walletAccount) {
        this.walletAccount = walletAccount;
    }
}
