package com.progressoft.mpay.plugins.crm.serviceregistration.validator;

import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CorporateExistValidator {
    private static final Logger logger = LoggerFactory.getLogger(CorporateExistValidator.class);

    private CorporateExistValidator() {

    }

    public static ValidationResult validate(MPAY_Corporate corporate, String corporateName) {
        logger.debug("Inside Validate ...");
        if (corporate == null || corporate.getDeletedFlag() != null)
            return new ValidationResult(ReasonCodes.SENDER_CORPORATE_NOT_ACTIVE, null, false);

        else if (!corporate.getName().equalsIgnoreCase(corporateName))
            return new ValidationResult(ReasonCodes.SENDER_NAME_DOES_NOT_MATCH_ANY_CORPORATE, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
