package com.progressoft.mpay.plugins.crm.serviceregistration;

import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.crm.serviceregistration.validator.*;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import org.apache.commons.lang.NullArgumentException;

public class CRMServiceRegistrationValidator {
    private CRMServiceRegistrationValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = validateIdType(context);
        if (!result.isValid())
            return result;//done


        //validate sender
        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;


        MPAY_Corporate corporate = (MPAY_Corporate) context.getExtraData().get(Constants.IS_CORPORATE_ALREADY_EXIST);
        if (corporate == null)
            return new ValidationResult(ReasonCodes.RECEIVER_CORPORATE_NOT_ACTIVE, null, false);

        result = CorporateValidator.validate(corporate, false);
        if (!result.isValid())
            return result;

        result = CorporateServiceExistValidator.validate((MPAY_CorpoarteService) context.getExtraData().get(Constants.IS_CORPORATE_SERVICE_ALREADY_EXIST));
        if (!result.isValid())
            return result;//done

        result = validateAlias(context);
        if (!result.isValid())
            return result;//done

        result = PaymentTypeValidator.validate((MPAY_MessageType) context.getExtraData().get(Constants.MESSAGE_TYPE));
        if (!result.isValid())
            return result;//done

        result = ServiceNotificationChannelValidator.validate((MPAY_NotificationChannel) context.getExtraData().get(Constants.NOTIFICATION_CHANNEL));
        if (!result.isValid())
            return result;//done

        result = ServiceTypeValidator.validate((MPAY_ServiceType) context.getExtraData().get(Constants.SERVICE_TYPE));
        if (!result.isValid())
            return result;//done

        result = ServiceCategoryValidator.validate((MPAY_ServicesCategory) context.getExtraData().get(Constants.SERVICES_CATEGRORY));
        if (!result.isValid())
            return result;//done

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;//done

        result = validateCity(context);
        if (!result.isValid())
            return result;//done

        result = validateBank(context);
        if (!result.isValid())
            return result;//done

        result = validateProfile(context);
        if (!result.isValid())
            return result;//done

        result = validateMaxNumberOFAccounts(context, (MPAY_Corporate) context.getExtraData().get(Constants.IS_CORPORATE_ALREADY_EXIST));
        if (!result.isValid())
            return result;//done

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }


    private static ValidationResult validateIdType(MessageProcessingContext context) {
        MPAY_IDType idType = (MPAY_IDType) context.getExtraData().get(Constants.ID_TYPE);
        if (idType == null || idType.getIsCustomer())
            return new ValidationResult(ReasonCodes.INVALID_ID_TYPE, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateAlias(MessageProcessingContext context) {
        CRMServiceRegistrationMessage message = (CRMServiceRegistrationMessage) context.getRequest();
        if (context.getDataProvider().isAliasUsed(message.getAlias()))
            return new ValidationResult(ReasonCodes.ALIAS_IS_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCity(MessageProcessingContext context) {
        MPAY_City city = (MPAY_City) context.getExtraData().get(Constants.CITY);
        if (city == null)
            return new ValidationResult(ReasonCodes.CITY_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateBank(MessageProcessingContext context) {

        MPAY_Bank bank = (MPAY_Bank) context.getExtraData().get(Constants.BANK);
        if (bank == null)
            return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
        if (bank.getDeletedFlag() != null && bank.getDeletedFlag())
            return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
        if (!bank.getIsActive())
            return new ValidationResult(ReasonCodes.BANK_INACTIVE, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateProfile(MessageProcessingContext context) {
        MPAY_Profile profile = (MPAY_Profile) context.getExtraData().get(Constants.PROFILE_KEY);
        if (profile == null)
            return new ValidationResult(ReasonCodes.PROFILE_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }


    private static ValidationResult validateMaxNumberOFAccounts(MessageProcessingContext context, MPAY_Corporate corporate) {
        MPAY_ClientType clientType = context.getLookupsLoader().getCorporateClientType(corporate.getClientType().getCode());
        if (clientType.getMaxNumberOfOwners() == 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (clientType.getMaxNumberOfOwners() <= context.getDataProvider().getNumberOfCorporateServices(corporate.getId()))
            return new ValidationResult(ReasonCodes.MAX_NUMBER_OF_OWNERS_REACHED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
