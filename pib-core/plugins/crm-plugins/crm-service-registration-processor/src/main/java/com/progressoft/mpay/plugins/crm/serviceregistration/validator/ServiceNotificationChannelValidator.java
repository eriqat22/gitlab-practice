package com.progressoft.mpay.plugins.crm.serviceregistration.validator;

import com.progressoft.mpay.entities.MPAY_NotificationChannel;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceNotificationChannelValidator {

	private static final Logger logger = LoggerFactory.getLogger(ServiceNotificationChannelValidator.class);

	private ServiceNotificationChannelValidator() {

	}

	public static ValidationResult validate(MPAY_NotificationChannel notificationChannel) {
		logger.debug("Inside Validate ...");
		if (notificationChannel == null)
			return new ValidationResult(ReasonCodes.NOTIFICATION_CHANNEL_NOT_SUPPORTED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
