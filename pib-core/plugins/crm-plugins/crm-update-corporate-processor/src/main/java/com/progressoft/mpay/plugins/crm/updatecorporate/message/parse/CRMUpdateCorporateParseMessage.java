package com.progressoft.mpay.plugins.crm.updatecorporate.message.parse;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.plugins.crm.updatecorporate.message.CRMUpdateCorporateMessage;
import com.progressoft.mpay.plugins.crm.updatecorporate.message.metadata.CRMUpdateCorporateMessageMetaData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;

import static com.progressoft.mpay.plugins.crm.updatecorporate.message.metadata.CRMUpdateCorporateMessageMetaData.UpdateCorporateMessageExtraData.*;

public class CRMUpdateCorporateParseMessage {

    private static final Logger logger = LoggerFactory.getLogger(CRMUpdateCorporateParseMessage.class);
    private static SimpleDateFormat dateFormat = new SimpleDateFormat(CRMUpdateCorporateMessageMetaData.DATE_FORMAT);

    public static CRMUpdateCorporateMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CRMUpdateCorporateMessage message = new CRMUpdateCorporateMessage(request);
        fillGenericFields(message);
        fillCorporateInfo(message);
        return message;
    }

    private static void fillGenericFields(CRMUpdateCorporateMessage message) throws MessageParsingException {
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
        message.setIdType(message.getValue(ID_TYPE.getValue()));
        message.setRegistrationId(message.getValue(REGISTRATION_ID.getValue()));
        if (message.getIdType() == null || message.getIdType().isEmpty())
            throw new MessageParsingException(ID_TYPE.getValue());
        if (message.getRegistrationId() == null || message.getRegistrationId().isEmpty())
            throw new MessageParsingException(REGISTRATION_ID.getValue());
    }

    private static void fillCorporateInfo(CRMUpdateCorporateMessage message) throws MessageParsingException {

        message.setShortName(message.getValue(SHORT_NAME.getValue()));
        message.setFullName(message.getValue(FULL_NAME.getValue()));
        message.setExpectedActivities(message.getValue(EXPECTED_ACTIVITIES.getValue()));
        setTypeOfSector(message);
        message.setCorporateType(message.getValue(CORPORATE_TYPE.getValue()));
        message.setClientReference(message.getValue(CLIENT_REFERENCE.getValue()));
        message.setRegistrationAuthority(message.getValue(REGISTRATION_AUTHORITY.getValue()));
        message.setRegistrationLocation(message.getValue(REGISTRATION_LOCATION.getValue()));
        message.setIdInChamberOfECommerce(message.getValue(ID_IN_CHAMBER_OF_ECOMMERCE.getValue()));
        setNotificationLanguage(message);
        message.setLegalEntity(message.getValue(LEGAL_ENTITY.getValue()));
        message.setOther(message.getValue(OTHER.getValue()));
        message.setPhone1(message.getValue(PHONE1.getValue()));
        message.setPhone2(message.getValue(PHONE2.getValue()));
        message.setEmail(message.getValue(EMAIL.getValue()));
        message.setMobileNumber(message.getValue(MOBILENUMBER.getValue()));
        message.setPoBox(message.getValue(POBOX.getValue()));
        message.setZipCode(message.getValue(ZIPCODE.getValue()));
        message.setBuildingNumber(message.getValue(BUILDING_NUMBER.getValue()));
        message.setStreetName(message.getValue(STREET_NAME.getValue()));
        message.setLocation(message.getValue(LOCATION.getValue()));
        message.setCountry(message.getValue(COUNTRY.getValue()));
        message.setCity(message.getValue(CITY.getValue()));
        message.setConsigneeArabicName(message.getValue(CONSIGNEE_ARABIC_NAME.getValue()));
        message.setConsigneeEnglishName(message.getValue(CONSIGNEE_ENGLISH_NAME.getValue()));
        message.setConsigneeNationality(message.getValue(CONSIGNEE_NATIONALITY.getValue()));
        message.setConsigneeOtherNationality(message.getValue(CONSIGNEE_OTHER_NATIONALITY.getValue()));
        message.setConsigneePlaceOfBirth(message.getValue(CONSIGNEE_PLACE_OF_BIRTH.getValue()));
        message.setConsigneeEmail(message.getValue(CONSIGNEE_EMAIL.getValue()));
        message.setConsigneeNationalId(message.getValue(CONSIGNEE_NATIONAL_ID.getValue()));
        message.setConsigneeIdReference(message.getValue(CONSIGNEE_ID_REFERENCE.getValue()));
        message.setConsigneePassportId(message.getValue(CONSIGNEE_PASSPORT_ID.getValue()));
        message.setConsigneeIdentificationCard(message.getValue(CONSIGNEE_IDENTIFICATION_CARD.getValue()));
        message.setConsigneeAddress(message.getValue(CONSIGNEE_ADDRESS.getValue()));
        setConsigneeGender(message);
        checkConsigneeDateOfBirth(message, dateFormat);
        checkConsigneePassportIssuanceDate(message, dateFormat);
        checkConsigneeIdentificationCardIssuanceDate(message, dateFormat);

    }

    private static void setConsigneeGender(CRMUpdateCorporateMessage message) throws MessageParsingException {
        message.setConsigneeGender(message.getValue(CONSIGNEE_GENDER.getValue()));
        if (message.getConsigneeGender() != null && !message.getConsigneeGender().isEmpty())
            if (!message.getConsigneeGender().equals(CRMUpdateCorporateMessageMetaData.CorporateGender.MALE.getValue()) && !message.getConsigneeGender().equals(CRMUpdateCorporateMessageMetaData.CorporateGender.FEMALE.getValue()))
                throw new MessageParsingException(CONSIGNEE_GENDER.getValue());
    }

    private static void setTypeOfSector(CRMUpdateCorporateMessage message) throws MessageParsingException {
        message.setTypeOfSector(message.getValue(TYPE_OF_SECTOR.getValue()));
        if (message.getTypeOfSector() != null && !message.getTypeOfSector().isEmpty()) {
            if (!message.getTypeOfSector().equals(CRMUpdateCorporateMessageMetaData.CorporateSectorType.PRIVATE.getValue()) && !message.getTypeOfSector().equals(CRMUpdateCorporateMessageMetaData.CorporateSectorType.PUBLIC.getValue()))
                throw new MessageParsingException(TYPE_OF_SECTOR.getValue());
        }
    }

    private static void setNotificationLanguage(CRMUpdateCorporateMessage message) throws MessageParsingException {
        message.setNotificationsLanguage(message.getValue(NOTIFICATIONS_LANGUAGE.getValue()));
        if (message.getNotificationsLanguage() != null && !message.getNotificationsLanguage().isEmpty()) {
            if (!message.getNotificationsLanguage().equals(CRMUpdateCorporateMessageMetaData.NotificationLanguage.ARABIC.getValue()) && !message.getNotificationsLanguage().equals(CRMUpdateCorporateMessageMetaData.NotificationLanguage.ENGLISH.getValue()))
                throw new MessageParsingException(NOTIFICATIONS_LANGUAGE.getValue());
        }
    }


    private static void checkConsigneeIdentificationCardIssuanceDate(CRMUpdateCorporateMessage message, SimpleDateFormat format) throws MessageParsingException {
        if (message.getValue(CONSIGNEE_IDENTIFICATION_CARD_ISSUANCE_DATE.getValue()) == null)
            return;
        try {
            message.setConsigneeIdentificationCardIssuanceDate(format.parse(message.getValue(CONSIGNEE_IDENTIFICATION_CARD_ISSUANCE_DATE.getValue())));
        } catch (Exception e) {
            logger.debug("Invalid  Consignee Identification Card Issuance Date dateFormat", e);
            throw new MessageParsingException(CONSIGNEE_IDENTIFICATION_CARD_ISSUANCE_DATE.getValue());
        }
    }


    private static void checkConsigneePassportIssuanceDate(CRMUpdateCorporateMessage message, SimpleDateFormat format) throws MessageParsingException {
        if (message.getValue(CONSIGNEE_PASSPORT_ISSUANCE_DATE.getValue()) == null)
            return;
        try {
            message.setConsigneePassportIssuanceDate(format.parse(message.getValue(CONSIGNEE_PASSPORT_ISSUANCE_DATE.getValue())));
        } catch (Exception e) {
            logger.debug("Invalid  Consignee Passport Issuance Date dateFormat", e);
            throw new MessageParsingException(CONSIGNEE_PASSPORT_ISSUANCE_DATE.getValue());
        }
    }

    private static void checkConsigneeDateOfBirth(CRMUpdateCorporateMessage message, SimpleDateFormat format) throws MessageParsingException {
        if (message.getValue(CONSIGNEE_DATE_OF_BIRTH.getValue()) == null)
            return;
        try {
            message.setConsigneeDateOfBirth(format.parse(message.getValue(CONSIGNEE_DATE_OF_BIRTH.getValue())));
        } catch (Exception e) {
            logger.debug("Invalid Date of Birth dateFormat", e);
            throw new MessageParsingException(CONSIGNEE_DATE_OF_BIRTH.getValue());
        }
    }

}
