package com.progressoft.mpay.plugins.crm.updatecorporate.validator;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.crm.updatecorporate.message.CRMUpdateCorporateMessage;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;

import static com.progressoft.mpay.plugins.crm.updatecorporate.processor.CRMUpdateCorporateProcessor.*;

public class CRMUpdateCorporateValidator {

    private CRMUpdateCorporateValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");

        CRMUpdateCorporateMessage message = (CRMUpdateCorporateMessage) context.getRequest();

        //validate sender
        ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;//done

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;//done

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;//done


        result = validateBasicInfo(context, message);
        if (!result.isValid())
            return result;//done

        if (isValid(message.getCorporateType())) {
            result = CRMUpdateCorporateExposedCorporateValidator.validate((MPAY_ClientType) context.getExtraData().get(CORPORATE_TYPES));
            if (!result.isValid())
                return result;//done
        }


        result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;//done

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;//done


        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateBasicInfo(MessageProcessingContext context, CRMUpdateCorporateMessage message) {
        ValidationResult result;
        if (isValid(message.getCountry())) {
            result = validateCountry(context);
            if (!result.isValid())
                return result;
        }

        if (isValid(message.getCity())) {
            result = validateCity(context);
            if (!result.isValid())
                return result;
        }
        if (isValid(message.getNotificationsLanguage())) {
            result = validateLanguage(context);
            if (!result.isValid())
                return result;
        }
        if (isValid(message.getConsigneeNationality())) {
            result = validateConsigneeNationality(context);
            if (!result.isValid())
                return result;
        }
        if (isValid(message.getLegalEntity())) {
            result = validateLegalEntity(context);
            if (!result.isValid())
                return result;
        }

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }


    private static ValidationResult validateCountry(MessageProcessingContext context) {
        JfwCountry country = (JfwCountry) context.getExtraData().get(COUNTRY);
        if (country == null)
            return new ValidationResult(ReasonCodes.INVALID_NATIONALITY, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateConsigneeNationality(MessageProcessingContext context) {
        JfwCountry country = (JfwCountry) context.getExtraData().get(CONSIGNEE_NATIONALITY);
        if (country == null)
            return new ValidationResult(ReasonCodes.INVALID_NATIONALITY, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCity(MessageProcessingContext context) {
        MPAY_City city = (MPAY_City) context.getExtraData().get(CITY);
        if (city == null)
            return new ValidationResult(ReasonCodes.CITY_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateLanguage(MessageProcessingContext context) {
        MPAY_Language language = (MPAY_Language) context.getExtraData().get(LANGUAGE);
        if (language == null)
            return new ValidationResult(ReasonCodes.LANGUAGE_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateLegalEntity(MessageProcessingContext context) {
        MPAY_CorpLegalEntity legalEntity = (MPAY_CorpLegalEntity) context.getExtraData().get(CORP_LEGAL_ENTITY);
        if (legalEntity == null)
            return new ValidationResult(ReasonCodes.LEGAL_ENTITY_NOT_DEFINED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static boolean isValid(String value) {
        return value != null && !value.isEmpty();
    }
}
