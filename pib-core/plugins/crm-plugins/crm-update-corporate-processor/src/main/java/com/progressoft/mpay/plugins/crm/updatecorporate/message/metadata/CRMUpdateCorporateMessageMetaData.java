package com.progressoft.mpay.plugins.crm.updatecorporate.message.metadata;

public class CRMUpdateCorporateMessageMetaData {

    public static final String DATE_FORMAT = "dd/MM/yyyy";

    public enum UpdateCorporateMessageExtraData {
        SHORT_NAME("shortName"),
        FULL_NAME("fullName"),
        CORPORATE_TYPE("corporateType"),
        ID_TYPE("idType"),
        REGISTRATION_ID("registrationId"),

        NOTIFICATIONS_LANGUAGE("notificationsLanguage"),
        CITY("city"),
        COUNTRY("country"),
        TYPE_OF_SECTOR("typeOfSector"),

        EXPECTED_ACTIVITIES("expectedActivities"),
        CLIENT_REFERENCE("clientReference"),
        REGISTRATION_AUTHORITY("registrationAuthority"),

        REGISTRATION_LOCATION("registrationLocation"),
        ID_IN_CHAMBER_OF_ECOMMERCE("idInChamberOfECommerce"),
        LEGAL_ENTITY("legalEntity"),
        OTHER("other"),
        PHONE1("phone1"),

        PHONE2("phone2"),
        EMAIL("email"),
        MOBILENUMBER("mobileNumber"),
        POBOX("poBox"),
        ZIPCODE("zipCode"),
        BUILDING_NUMBER("buildingNumber"),
        STREET_NAME("streetName"),

        LOCATION("location"),
        CONSIGNEE_ARABIC_NAME("consigneeArabicName"),
        CONSIGNEE_ENGLISH_NAME("consigneeEnglishName"),
        CONSIGNEE_GENDER("consigneeGender"),

        CONSIGNEE_NATIONALITY("consigneeNationality"),
        CONSIGNEE_OTHER_NATIONALITY("consigneeOtherNationality"),
        CONSIGNEE_PLACE_OF_BIRTH("consigneePlaceOfBirth"),

        CONSIGNEE_DATE_OF_BIRTH("consigneeDateOfBirth"),
        CONSIGNEE_EMAIL("consigneeEmail"),
        CONSIGNEE_NATIONAL_ID("consigneeNationalId"),

        CONSIGNEE_ID_REFERENCE("consigneeIdReference"),


        CONSIGNEE_PASSPORT_ID("consigneePassportId"),

        CONSIGNEE_PASSPORT_ISSUANCE_DATE("consigneePassportIssuanceDate"),
        CONSIGNEE_IDENTIFICATION_CARD("consigneeIdentificationCard"),

        CONSIGNEE_IDENTIFICATION_CARD_ISSUANCE_DATE("consigneeIdentificationCardIssuanceDate"),
        CONSIGNEE_ADDRESS("consigneeAddress");


        private String value;

        UpdateCorporateMessageExtraData(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }


    public enum CorporateSectorType {
        PRIVATE("1"),
        PUBLIC("2");

        private String value;

        CorporateSectorType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    public enum NotificationLanguage {
        ENGLISH("1"),
        ARABIC("3");

        private String value;

        NotificationLanguage(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    public enum CorporateGender {

        MALE("1"),
        FEMALE("2");

        private String value;

        CorporateGender(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
