package com.progressoft.mpay.plugins.crm.updatecorporate.message;

import com.progressoft.mpay.messages.MPayRequest;
import org.apache.commons.lang.NullArgumentException;

import java.util.Date;

public class CRMUpdateCorporateMessage extends MPayRequest {
    private String shortName;//done
    private String fullName;//done
    private String expectedActivities;//done
    private String typeOfSector;//done
    private String corporateType;//done
    private String clientReference;//done
    private String registrationAuthority;//done
    private String registrationLocation;//done
    private String idType;//done should not be changed
    private String registrationId;//done should not be changed
    private String idInChamberOfECommerce;//done
    private String notificationsLanguage;//done
    private String legalEntity;//done
    private String other;//done
    private String phone1;//done
    private String phone2;//done
    private String email;//done
    private String mobileNumber;//done
    private String poBox;//done
    private String zipCode;//done
    private String buildingNumber;//done
    private String streetName;//done
    private String location;//done
    private String city;//done
    private String country;//done
    private String consigneeArabicName;//done
    private String consigneeEnglishName;//done
    private String consigneeGender;//done
    private String consigneeNationality;//done
    private String consigneeOtherNationality;//done
    private String consigneePlaceOfBirth;//done
    private Date consigneeDateOfBirth;//done
    private String consigneeEmail;//done
    private String consigneeNationalId;//done
    private String consigneeIdReference;//done
    private String consigneePassportId;//done
    private String consigneeAddress;//done
    private Date consigneePassportIssuanceDate;//done
    private String consigneeIdentificationCard;//done
    private Date consigneeIdentificationCardIssuanceDate;//done


    public CRMUpdateCorporateMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
        setRequestedId(request.getRequestedId());
    }

    public String getClientReference() {
        return clientReference;
    }

    public void setClientReference(String clientReference) {
        this.clientReference = clientReference;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public Date getConsigneeIdentificationCardIssuanceDate() {
        return consigneeIdentificationCardIssuanceDate;
    }

    public void setConsigneeIdentificationCardIssuanceDate(Date consigneeIdentificationCardIssuanceDate) {
        this.consigneeIdentificationCardIssuanceDate = consigneeIdentificationCardIssuanceDate;
    }

    public String getConsigneeIdentificationCard() {
        return consigneeIdentificationCard;
    }

    public void setConsigneeIdentificationCard(String consigneeIdentificationCard) {
        this.consigneeIdentificationCard = consigneeIdentificationCard;
    }

    public Date getConsigneePassportIssuanceDate() {
        return consigneePassportIssuanceDate;
    }

    public void setConsigneePassportIssuanceDate(Date consigneePassportIssuanceDate) {
        this.consigneePassportIssuanceDate = consigneePassportIssuanceDate;
    }

    public String getConsigneePassportId() {
        return consigneePassportId;
    }

    public void setConsigneePassportId(String consigneePassportId) {
        this.consigneePassportId = consigneePassportId;
    }

    public Date getConsigneeDateOfBirth() {
        return consigneeDateOfBirth;
    }

    public void setConsigneeDateOfBirth(Date consigneeDateOfBirth) {
        this.consigneeDateOfBirth = consigneeDateOfBirth;
    }


    public String getConsigneeIdReference() {
        return consigneeIdReference;
    }

    public void setConsigneeIdReference(String consigneeIdReference) {
        this.consigneeIdReference = consigneeIdReference;
    }

    public String getConsigneeNationalId() {
        return consigneeNationalId;
    }

    public void setConsigneeNationalId(String consigneeNationalId) {
        this.consigneeNationalId = consigneeNationalId;
    }

    public String getConsigneeEmail() {
        return consigneeEmail;
    }

    public void setConsigneeEmail(String consigneeEmail) {
        this.consigneeEmail = consigneeEmail;
    }

    public String getConsigneePlaceOfBirth() {
        return consigneePlaceOfBirth;
    }

    public void setConsigneePlaceOfBirth(String consigneePlaceOfBirth) {
        this.consigneePlaceOfBirth = consigneePlaceOfBirth;
    }

    public String getConsigneeOtherNationality() {
        return consigneeOtherNationality;
    }

    public void setConsigneeOtherNationality(String consigneeOtherNationality) {
        this.consigneeOtherNationality = consigneeOtherNationality;
    }

    public String getConsigneeNationality() {
        return consigneeNationality;
    }

    public void setConsigneeNationality(String consigneeNationality) {
        this.consigneeNationality = consigneeNationality;
    }

    public String getConsigneeGender() {
        return consigneeGender;
    }

    public void setConsigneeGender(String consigneeGender) {
        this.consigneeGender = consigneeGender;
    }

    public String getConsigneeEnglishName() {
        return consigneeEnglishName;
    }

    public void setConsigneeEnglishName(String consigneeEnglishName) {
        this.consigneeEnglishName = consigneeEnglishName;
    }

    public String getConsigneeArabicName() {
        return consigneeArabicName;
    }

    public void setConsigneeArabicName(String consigneeArabicName) {
        this.consigneeArabicName = consigneeArabicName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getIdInChamberOfECommerce() {
        return idInChamberOfECommerce;
    }

    public void setIdInChamberOfECommerce(String idInChamberOfECommerce) {
        this.idInChamberOfECommerce = idInChamberOfECommerce;
    }

    public String getRegistrationLocation() {
        return registrationLocation;
    }

    public void setRegistrationLocation(String registrationLocation) {
        this.registrationLocation = registrationLocation;
    }

    public String getRegistrationAuthority() {
        return registrationAuthority;
    }

    public void setRegistrationAuthority(String registrationAuthority) {
        this.registrationAuthority = registrationAuthority;
    }


    public String getExpectedActivities() {
        return expectedActivities;
    }

    public void setExpectedActivities(String expectedActivities) {
        this.expectedActivities = expectedActivities;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCorporateType() {
        return corporateType;
    }

    public void setCorporateType(String corporateType) {
        this.corporateType = corporateType;
    }


    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getNotificationsLanguage() {
        return notificationsLanguage;
    }

    public void setNotificationsLanguage(String notificationsLanguage) {
        this.notificationsLanguage = notificationsLanguage;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTypeOfSector() {
        return typeOfSector;
    }

    public void setTypeOfSector(String typeOfSelector) {
        this.typeOfSector = typeOfSelector;
    }


}
