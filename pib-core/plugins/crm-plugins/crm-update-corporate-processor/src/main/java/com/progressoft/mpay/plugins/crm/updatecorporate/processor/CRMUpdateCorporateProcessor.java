package com.progressoft.mpay.plugins.crm.updatecorporate.processor;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.crm.updatecorporate.message.CRMUpdateCorporateMessage;
import com.progressoft.mpay.plugins.crm.updatecorporate.message.parse.CRMUpdateCorporateParseMessage;
import com.progressoft.mpay.plugins.crm.updatecorporate.validator.CRMUpdateCorporateValidator;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class CRMUpdateCorporateProcessor implements MessageProcessor {
    public static final String COUNTRY = "country";
    public static final String CONSIGNEE_NATIONALITY = "consigneeNationality";
    public static final String IS_CORPORATE_EXIST = "isCorporateExist";
    public static final String CITY = "city";
    public static final String LANGUAGE = "language";
    public static final String CORPORATE_TYPES = "corporateType";
    public static final String CORP_LEGAL_ENTITY = "corporateLegalEntity";
    private static final Logger logger = LoggerFactory.getLogger(CRMUpdateCorporateProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ....");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = CRMUpdateCorporateValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside processIntegration ....");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside reverse ....");
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        logger.debug("Inside accept ....");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        logger.debug("Inside reject ....");
        return null;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        CRMUpdateCorporateMessage message = CRMUpdateCorporateParseMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), ReceiverInfoType.CORPORATE));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        context.getExtraData().put(IS_CORPORATE_EXIST, context.getDataProvider().getCorporateByIdTypeAndRegistrationId(message.getIdType(), message.getRegistrationId()));
        loadLookups(context, message);
    }


    private void loadLookups(MessageProcessingContext context, CRMUpdateCorporateMessage message) {
        JfwCountry country = context.getLookupsLoader().getCountry(message.getCountry());
        if (country == null)
            return;
        context.getExtraData().put(COUNTRY, country);

        JfwCountry nationality = context.getLookupsLoader().getCountry(message.getConsigneeNationality());
        if (nationality != null)
            context.getExtraData().put(CONSIGNEE_NATIONALITY, nationality);

        MPAY_City city = context.getLookupsLoader().getCity(message.getCity());
        if (city == null)
            return;
        context.getExtraData().put(CITY, city);

        MPAY_Language language = context.getLookupsLoader().getLanguageByCode(getNotificationsLanguage(message));
        if (language == null)
            return;
        context.getExtraData().put(LANGUAGE, language);


        List<String> corporatesCode = Arrays.asList(context.getLookupsLoader().getSystemConfigurations(SysConfigKeys.EXPOSED_CORPORATES_CODES).getConfigValue().split(";"));
        if (!corporatesCode.contains(message.getCorporateType()))
            return;
        context.getExtraData().put(CORPORATE_TYPES, context.getLookupsLoader().getClientType(message.getCorporateType()));


        MPAY_CorpLegalEntity corpLegalEntity = context.getLookupsLoader().getLegalEntityByCode(message.getLegalEntity());
        if (corpLegalEntity == null)
            return;
        context.getExtraData().put(CORP_LEGAL_ENTITY, corpLegalEntity);

    }

    private String getNotificationsLanguage(CRMUpdateCorporateMessage message) {
        return message.getNotificationsLanguage().equals("1") ? "en" : "ar";
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatus, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatus);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        logger.debug("Inside AcceptMessage ...");
        context.getDataProvider().updateCorporate(getCorporateFromRequest(context));
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MPAY_Corporate getCorporateFromRequest(MessageProcessingContext context) {
        CRMUpdateCorporateMessage message = (CRMUpdateCorporateMessage) context.getRequest();
        MPAY_Corporate corporate = (MPAY_Corporate) context.getExtraData().get(IS_CORPORATE_EXIST);
        if (isNotEmpty(message.getShortName()))
            corporate.setName(message.getShortName());
        if (isNotEmpty(message.getFullName()))
            corporate.setDescription(message.getFullName());
        if (isNotEmpty(message.getExpectedActivities()))
            corporate.setExpectedActivities(message.getExpectedActivities());
        if (isNotEmpty(message.getTypeOfSector()))
            corporate.setSectorType(message.getTypeOfSector());
        if (isNotEmpty(message.getCorporateType()))
            corporate.setClientType((MPAY_ClientType) context.getExtraData().get(CORPORATE_TYPES));
        if (isNotEmpty(message.getClientReference()))
            corporate.setClientRef(message.getClientReference());
        if (isNotEmpty(message.getRegistrationAuthority()))
            corporate.setRegistrationAuthority(message.getRegistrationAuthority());
        if (isNotEmpty(message.getRegistrationLocation()))
            corporate.setRegistrationLocation(message.getRegistrationLocation());
        if (isNotEmpty(message.getIdInChamberOfECommerce()))
            corporate.setChamberId(message.getIdInChamberOfECommerce());
        if (isNotEmpty(message.getNotificationsLanguage()))
            corporate.setPrefLang((MPAY_Language) context.getExtraData().get(LANGUAGE));
        if (isNotEmpty(message.getLegalEntity()))
            corporate.setLegalEntity((MPAY_CorpLegalEntity) context.getExtraData().get(CORP_LEGAL_ENTITY));
        if (isNotEmpty(message.getOther()))
            corporate.setOtherLegalEntity(message.getOther());
        if (isNotEmpty(message.getPhone1()))
            corporate.setPhoneOne(message.getPhone1());
        if (isNotEmpty(message.getPhone2()))
            corporate.setPhoneTwo(message.getPhone2());
        if (isNotEmpty(message.getEmail()))
            corporate.setEmail(message.getEmail());
        if (isNotEmpty(message.getMobileNumber()))
            corporate.setMobileNumber(message.getMobileNumber());
        if (isNotEmpty(message.getPoBox()))
            corporate.setPobox(message.getPoBox());
        if (isNotEmpty(message.getZipCode()))
            corporate.setZipCode(message.getZipCode());
        if (isNotEmpty(message.getBuildingNumber()))
            corporate.setBuildingNum(message.getBuildingNumber());
        if (isNotEmpty(message.getStreetName()))
            corporate.setStreetName(message.getStreetName());
        if (isNotEmpty(message.getLocation()))
            corporate.setLocation(message.getLocation());
        if (isNotEmpty(message.getCity()))
            corporate.setCity((MPAY_City) context.getExtraData().get(CITY));
        if (isNotEmpty(message.getCountry()))
            corporate.setRefCountry((JfwCountry) context.getExtraData().get(COUNTRY));
        if (isNotEmpty(message.getConsigneeArabicName()))
            corporate.setConsigneeArabicName(message.getConsigneeArabicName());
        if (isNotEmpty(message.getConsigneeEnglishName()))
            corporate.setConsigneeEnglishName(message.getConsigneeEnglishName());
        if (isNotEmpty(message.getConsigneeGender()))
            corporate.setGender(message.getConsigneeGender());
        if (isNotEmpty(message.getConsigneeNationality()))
            corporate.setNationality((JfwCountry) context.getExtraData().get(CONSIGNEE_NATIONALITY));
        if (isNotEmpty(message.getConsigneeOtherNationality()))
            corporate.setOtherNationality(message.getConsigneeOtherNationality());
        if (isNotEmpty(message.getConsigneePlaceOfBirth()))
            corporate.setPlaceOfBirth(message.getConsigneePlaceOfBirth());
        if (message.getConsigneeDateOfBirth() != null)
            corporate.setDateOfBirth(message.getConsigneeDateOfBirth());
        if (isNotEmpty(message.getConsigneeEmail()))
            corporate.setEmail(message.getConsigneeEmail());
        if (isNotEmpty(message.getConsigneeNationalId()))
            corporate.setNationalID(message.getConsigneeNationalId());
        if (isNotEmpty(message.getConsigneeIdReference()))
            corporate.setIdentificationReference(message.getConsigneeIdReference());
        if (isNotEmpty(message.getConsigneePassportId()))
            corporate.setPassportID(message.getConsigneePassportId());
        if (message.getConsigneePassportIssuanceDate() != null)
            corporate.setPassportIssuanceDate(message.getConsigneePassportIssuanceDate());
        if (isNotEmpty(message.getConsigneeIdentificationCard()))
            corporate.setIdentificationCard(message.getConsigneeIdentificationCard());
        if (message.getConsigneeIdentificationCardIssuanceDate() != null)
            corporate.setIdentificationCardIssuanceDate(message.getConsigneeIdentificationCardIssuanceDate());
        if (isNotEmpty(message.getConsigneeAddress()))
            corporate.setAddress(message.getConsigneeAddress());
        if (isNotEmpty(message.getConsigneeEmail()))
            corporate.setCosigneeEMail(message.getConsigneeEmail());
        return corporate;
    }


    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private boolean isNotEmpty(String value) {
        return !StringUtils.isBlank(value);
    }
}
