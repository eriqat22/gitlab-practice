package com.progressoft.mpay.plugins.madar.customermobileinquiry.message;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.plugins.madar.customermobileinquiry.message.view.CustomerMobileInquireMessageView;

public class CustomerMobileInquireMessage extends MPayRequest implements CustomerMobileInquireMessageView {
    private String mobileNumber;
    private String alias;

    private enum InquireCustomerMobileMessageExtraData {
        MOBILE_NUMBER("mobileNumber"), ALIAS("alias");

        private String value;

        private InquireCustomerMobileMessageExtraData(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public CustomerMobileInquireMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    @Override
    public String getMobileNumber() {
        return mobileNumber;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public static CustomerMobileInquireMessage parseAndvalidateMessage(MPayRequest request)
            throws MessageParsingException {
        if (request == null)
            return null;
        CustomerMobileInquireMessage message = new CustomerMobileInquireMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE)
                || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        message.setMobileNumber(request.getValue(InquireCustomerMobileMessageExtraData.MOBILE_NUMBER.getValue()));
        message.setAlias(request.getValue(InquireCustomerMobileMessageExtraData.ALIAS.getValue()));
        if (message.getAlias().isEmpty() && message.getMobileNumber().isEmpty())
            throw new MessageParsingException("All three fields are Null");
        return message;
    }
}
