package com.progressoft.mpay.plugins.madar.customermobileinquiry.message.view;

public interface CustomerMobileInquireMessageView {
    String getAlias();

    String getMobileNumber();
}
