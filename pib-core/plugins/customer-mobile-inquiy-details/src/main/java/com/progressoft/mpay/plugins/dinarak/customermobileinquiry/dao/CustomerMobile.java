package com.progressoft.mpay.plugins.madar.customermobileinquiry.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.math.BigDecimal;

public class CustomerMobile {
    private String mobileNumber;

    private String customerName;
    private String alias;
    private String nfcSerial;
    private boolean isActive;
    private boolean isRegistered;
    private String notificationShowType;
    private String channelType;
    private boolean isBlocked;
    private String mobileOperator;
    private String externalAccount;
    private String bankedUnbankedType;
    private long mas;
    private String profile;
    private String accountNumber;
    private String currency;
    private String nature;
    private String type;
    private String balanceType;
    private BigDecimal balance;

    public CustomerMobile(MPAY_CustomerMobile mobile) {
        if (mobile == null)
            throw new NullArgumentException("mobile");
        this.customerName = mobile.getRefCustomer().getFullName();
        this.mobileNumber = mobile.getMobileNumber();
        this.alias = mobile.getAlias();
        this.nfcSerial = mobile.getNfcSerial();
        this.isActive = mobile.getIsActive();
        this.isRegistered = mobile.getIsRegistered();

        this.notificationShowType = mobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS)
                ? NotificationShowTypeCodes.ALIAS : NotificationShowTypeCodes.MOBILE_NUMBER;
        this.channelType = mobile.getChannelType().getName();
        this.isBlocked = mobile.getIsBlocked();
        //this.mobileOperator = mobile.getRefOperator().getName();
        this.externalAccount = mobile.getExternalAcc();
        this.bankedUnbankedType = mobile.getBankedUnbanked();
        this.mas = mobile.getMas();
        this.profile = mobile.getRefProfile().getName();
        this.accountNumber = mobile.getMobileMobileAccounts().get(0).getRefAccount().getAccNumber();
        this.currency = mobile.getRefProfile().getCurrency().getCode();
        this.nature = mobile.getMobileMobileAccounts().get(0).getRefAccount().getNature();
        this.type = mobile.getMobileMobileAccounts().get(0).getRefAccount().getAccountType().getCode();
        this.balanceType = mobile.getMobileMobileAccounts().get(0).getRefAccount().getBalanceType().getCode();
        this.balance = mobile.getMobileMobileAccounts().get(0).getRefAccount().getBalance();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileOperator() {
        return mobileOperator;
    }

    public void setMobileOperator(String mobileOperator) {
        this.mobileOperator = mobileOperator;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }

    public String getBankedUnbankedType() {
        return bankedUnbankedType;
    }

    public void setBankedUnbankedType(String bankedUnbankedType) {
        this.bankedUnbankedType = bankedUnbankedType;
    }

    public long getMas() {
        return mas;
    }

    public void setMas(long mas) {
        this.mas = mas;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBalanceType() {
        return balanceType;
    }

    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNfcSerial() {
        return nfcSerial;
    }

    public void setNfcSerial(String nfcSerial) {
        this.nfcSerial = nfcSerial;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getNotificationShowType() {
        return notificationShowType;
    }

    public void setNotificationShowType(String notificationShowType) {
        this.notificationShowType = notificationShowType;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

}
