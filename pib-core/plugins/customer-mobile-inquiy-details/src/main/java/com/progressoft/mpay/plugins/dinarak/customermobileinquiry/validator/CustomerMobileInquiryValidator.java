package com.progressoft.mpay.plugins.madar.customermobileinquiry.validator;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerMobileInquiryValidator {
    private static final Logger logger = LoggerFactory.getLogger(CustomerMobileInquiryValidator.class);

    private CustomerMobileInquiryValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside validate ...");
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getService(),
                context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, false);
        if (!result.isValid())
            return result;

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
