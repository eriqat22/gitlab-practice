package com.progressoft.mpay.plugins.merchanttoadmin;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

import java.math.BigDecimal;

public class MerchantToAdminMessage extends MPayRequest {

    private static final String NOTES_KEY = "note";
    private static final String AMOUNT_KEY = "amount";
    private static final String SENDER_ACCOUNT_KEY = "senderAccount";

    private String notes;
    private BigDecimal amount;
    private String senderAccount;

    public MerchantToAdminMessage(){
    }

    public MerchantToAdminMessage(MPayRequest request){

        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    public static MerchantToAdminMessage parseMessage(MPayRequest request) throws MessageParsingException {
        MerchantToAdminMessage message = new MerchantToAdminMessage(request);

        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
        String amount = message.getValue(AMOUNT_KEY);
        if (amount == null)
            throw new MessageParsingException(AMOUNT_KEY);
        try {
            message.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
        } catch (NumberFormatException ex) {
            throw new MessageParsingException(AMOUNT_KEY);
        }
        message.setNotes(message.getValue(NOTES_KEY));

        return message;
    }

}
