package com.progressoft.mpay.plugins.merchanttoadmin;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MerchantToAdminValidator {
    private static final Logger logger = LoggerFactory.getLogger(MerchantToAdminValidator.class);

    public MerchantToAdminValidator(){

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ....");
        if (context == null)
            throw new NullArgumentException("context");

        MerchantToAdminMessage message = (MerchantToAdminMessage) context.getRequest();

        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),context.getOperation().getMessageType());
        if (!result.isValid())
            return result;

        if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
            result = validateCorporate(context, message);
        else
            return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);

        if (!result.isValid())
            return result;

        result = validateReceiver(context);

        return result;
    }

    private static ValidationResult validateReceiver(MessageProcessingContext context) {

        if(context.getReceiver().getService() == null)
            return new ValidationResult(ReasonCodes.HQ_SERVICE_IS_NOT_AVAILABLE, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCorporate(MessageProcessingContext context, MerchantToAdminMessage request) {
        ValidationResult result = validateCorporateInfo(context);
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getService(),
                context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = PinCodeValidator.validate(context, context.getSender().getService(), request.getPin());
        if (!result.isValid())
            return result;

        if(isSenderAdminService(context))
        {
            return new ValidationResult(ReasonCodes.UNSUPPORTED_REQUEST, null, false);
        }


        return result;
    }

    private static boolean isSenderAdminService(MessageProcessingContext context) {
        return context.getSender().getService().getPaymentType().getCode().equals(Constants.PAYMENT_TYPE_CODE);
    }

    private static ValidationResult validateCorporateInfo(MessageProcessingContext context) {
        ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(),
                context.getRequest().getDeviceId(), context.getSender().getService().getId()));
        if (!result.isValid())
            return result;

        return result;
    }

}
