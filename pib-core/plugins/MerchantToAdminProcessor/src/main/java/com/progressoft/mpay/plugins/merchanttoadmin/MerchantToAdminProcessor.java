package com.progressoft.mpay.plugins.merchanttoadmin;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.registration.corporates.CorporateServiceWorkflowStatuses;
import com.progressoft.mpay.registration.corporates.CorporateWorkflowStatuses;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Optional;

public class MerchantToAdminProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MerchantToAdminProcessor.class);
	public static final String SENDER_NOTIFICATION_TOKEN = "SENDER_NOTIFICATION_TOKEN";
	public static final String RECEIVER_NOTIFICATION_TOKEN = "RECEIVER_NOTIFICATION_TOKEN";

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = MerchantToAdminValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
					e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage", ex);
			if (context.getTransaction() != null)
				TransactionHelper.reverseTransaction(context);
			return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
					ex.getMessage());
		}
	}

	private void preProcessMessage(MessageProcessingContext context)
			throws MessageParsingException, WorkflowException, SQLException {
		logger.debug("Inside PreProcessMessage ...");
		MerchantToAdminMessage message = MerchantToAdminMessage.parseMessage(context.getRequest());

		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setAmount(message.getAmount());
		sender.setInfo(message.getSender());
		context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
		sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
		if (sender.getService() == null)
			return;
		sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(),
				message.getSenderAccount()));
		if (sender.getServiceAccount() == null)
			return;

		if (isAmountValueEqualZero(message))
			context.setAmount(sender.getServiceAccount().getRefAccount().getBalance());

		sender.setAccount(sender.getServiceAccount().getRefAccount());
		sender.setCorporate(sender.getService().getRefCorporate());
		sender.setBank(sender.getServiceAccount().getBank());
		sender.setProfile(sender.getServiceAccount().getRefProfile());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
				context.getOperation().getMessageType().getId()));

		receiver.setService(getAdminService(sender.getService().getRefCorporate()));
		receiver.setNotes(message.getNotes());
		if (receiver.getService() != null) {
			receiver.setCorporate(receiver.getService().getRefCorporate());
			receiver.setServiceAccount(
					SystemHelper.getServiceAccount(context.getSystemParameters(), receiver.getService(), null));
			if (receiver.getServiceAccount() == null)
				return;
			receiver.setAccount(receiver.getServiceAccount().getRefAccount());
			receiver.setBank(receiver.getServiceAccount().getBank());
			receiver.setProfile(receiver.getServiceAccount().getRefProfile());
			receiver.setBanked(receiver.getAccount().getIsBanked());
			context.setDirection(TransactionDirection.ONUS);
			context.setTransactionConfig(
					context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(),
							sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(),
							context.getOperation().getMessageType().getId(), context.getTransactionNature(),
							context.getOperation().getId()));
		}

		if (context.getTransactionConfig() != null)
			context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
		OTPHanlder.loadOTP(context, message.getPin());
		ChargesCalculator.calculate(context);
		TaxCalculator.claculate(context);
	}

	private boolean isAmountValueEqualZero(MerchantToAdminMessage message) {
		return message.getAmount().compareTo(new BigDecimal(0)) == 0;
	}

	private MPAY_CorpoarteService getAdminService(MPAY_Corporate corporate) {
		Optional<MPAY_CorpoarteService> corpoarteService = corporate.getRefCorporateCorpoarteServices().stream()
				.filter(service -> service.getPaymentType().getCode().equals(Constants.PAYMENT_TYPE_CODE)
						&& CorporateServiceWorkflowStatuses.APPROVED.toString().equals(service.getStatusId().getCode()))
				.findFirst();

		if (corpoarteService.isPresent())
			return corpoarteService.get();

		return null;
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext context) {
		return null;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context)
			throws SQLException, WorkflowException {
		logger.debug("Inside AcceptMessage ...");
		MerchantToAdminMessage message = (MerchantToAdminMessage) context.getRequest();
		MessageProcessingResult result = new MessageProcessingResult();
		MPAY_ProcessingStatus status;
		MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
		String reasonDescription = null;

		context.getMessage().setReason(reason);
		MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT,
				BigDecimal.ZERO, message.getNotes());
		result.setMessage(context.getMessage());
		context.setTransaction(transaction);
		result.setTransaction(transaction);
		context.getDataProvider().persistProcessingResult(result);
		JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(),
				context.getLookupsLoader(), transaction);
		if (postingResult.isSuccess()) {
			BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
			if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
				status = handleMPClear(context, result);
			} else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			} else {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			}
		} else {
			if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
				reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
				context.getMessage().setReasonDesc(postingResult.getReason());
			} else
				reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
		}
		context.getMessage().setProcessingStatus(status);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
		transaction.setReason(context.getMessage().getReason());
		transaction.setReasonDesc(context.getMessage().getReasonDesc());
		result.setStatus(status);
		result.setStatusDescription(context.getMessage().getReasonDesc());
		handleNotifications(context, result, status);

		handlePushNotification(context, message);
		OTPHanlder.removeOTP(context);
		handleLimits(context, status, transaction);
		return result;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext context) {
		return null;
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode,
			String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		if (context.getTransaction() != null) {
			BankIntegrationResult result = BankIntegrationHandler.reverse(context);
			if (result != null)
				TransactionHelper.reverseTransaction(context);
		}
		return MessageProcessingResult.create(context, status, reason, reasonDescription);
	}

	private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, MessageProcessingResult result)
			throws WorkflowException {
		MPAY_ProcessingStatus status;
		Integer mpClearIntegType;
		if (context.getSystemParameters().getOfflineModeEnabled()
				&& context.getDirection() == TransactionDirection.ONUS) {
			mpClearIntegType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
			result.setHandleMPClearOffline(true);
		} else {
			mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
		}
		IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType);
		MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage,
				Integer.toHexString(mpClearIntegType));
		messageLog.setRefMessage(context.getMessage());
		result.setMpClearMessage(messageLog);
		return status;
	}

	private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type) {
		logger.debug("Inside CreateMPClearRequest ...");
		try {
			MerchantToAdminMessage request = (MerchantToAdminMessage) context.getRequest();
			String messageID = context.getDataProvider().getNextMPClearMessageId();
			BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
			IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
					.newMessage(type);
			message.setBinary(false);
			if (context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_CREDIT))
				message.setField(MPClearCommonFields.PROCESSING_CODE, new IsoValue<String>(IsoType.NUMERIC,
						String.valueOf(ProcessingCodeRanges.CREDIT_START_VALUE), 6));
			else
				message.setField(MPClearCommonFields.PROCESSING_CODE, new IsoValue<String>(IsoType.NUMERIC,
						String.valueOf(ProcessingCodeRanges.DEBIT_START_VALUE), 6));
			String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(),
					context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
			String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge,
					AmountType.CHARGE);

			message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
			MPClearHelper.setMessageEncoded(message);
			message.setField(7, new IsoValue<String>(IsoType.DATE10,
					MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
			message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
			message.setField(49,
					new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

			String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getService(),
					context.getSender().getServiceAccount(), false);
			MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

			String account2;
			if (context.getReceiver().getService() == null)
				account2 = MPClearHelper.getAccount(context.getSystemParameters(),
						context.getReceiver().getService().getName(), ReceiverInfoType.CORPORATE);
			else
				account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getService(),
						context.getReceiver().getServiceAccount(), false);

			MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

			String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
			message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode != null ? messageTypeCode : "",
					messageTypeCode != null ? messageTypeCode.length() : 0));
			message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

			return message;
		} catch (Exception e) {
			logger.error("Error when CreateMPClearRequest", e);
			return null;
		}
	}

	private void handlePushNotification(MessageProcessingContext context, MerchantToAdminMessage message) {
		if (message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)) {
			String receiverDeviceToken = context.getReceiver().getService().getRefCorporateServiceCorporateDevices()
					.stream().filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
					.findFirst().map(MPAY_CorporateDevice::getExtraData).orElse(null);
			MPAY_CorporateDevice corporateDevice = context.getDataProvider().getCorporateDevice(message.getDeviceId());
			context.setSenderNotificationToken(corporateDevice != null ? corporateDevice.getExtraData() : null);
			context.setReceiverNotificationToken(receiverDeviceToken);
		}
	}

	private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result,
			MPAY_ProcessingStatus status) {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)) {
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		} else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
			result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
	}

	private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status,
			MPAY_Transaction transaction) throws SQLException {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)
				|| status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
			context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
					context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
					SystemHelper.getCurrentDateWithoutTime());
		}
	}
}
