package com.progressoft.mpay.plugins.ministatement;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class MiniStatementMessage extends MPayRequest {

	private static final String SENDER_ACCOUNT_KEY = "senderAccount";

	private String senderAccount;

	public MiniStatementMessage() {
		//
	}

	public MiniStatementMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public static MiniStatementMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		MiniStatementMessage message = new MiniStatementMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(SENDER_TYPE_KEY);
		message.setSenderAccount(message.getValue(SENDER_ACCOUNT_KEY));
		return message;
	}
}