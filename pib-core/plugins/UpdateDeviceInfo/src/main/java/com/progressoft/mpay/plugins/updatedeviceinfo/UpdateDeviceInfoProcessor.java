package com.progressoft.mpay.plugins.updatedeviceinfo;


import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateDeviceInfoProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(UpdateDeviceInfoProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside Processing Message ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = UpdateDeviceInfoValidator.validate(context);
            return validationResult.isValid() ? acceptMessage(context) :
                    rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception e) {
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage());
        }
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        UpdateDeviceInfoMessage message = (UpdateDeviceInfoMessage) context.getRequest();
        String deviceId = message.getDeviceId();
        String notificationToken = message.getNotificationToken();
        if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
            updateCustomerDevice(context, deviceId, notificationToken);

        } else {
            updateCorporateDevice(context, deviceId, notificationToken);
        }
        return result;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside ProcessIntegration ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside Reverse ...");
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        logger.debug("Inside Reject ...");
        return null;
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        return createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        UpdateDeviceInfoMessage message = UpdateDeviceInfoMessage.parseMessage(context.getRequest());
        context.setRequest(message);
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
            sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
            if (sender.getMobile() == null)
                return;
            sender.setCustomer(sender.getMobile().getRefCustomer());
            sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
            if (sender.getMobileAccount() == null)
                return;
            sender.setProfile(sender.getMobileAccount().getRefProfile());
        } else {
            sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
            if (sender.getService() == null)
                return;
            sender.setCorporate(sender.getService().getRefCorporate());
            sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
            if (sender.getServiceAccount() == null)
                return;
            sender.setProfile(sender.getServiceAccount().getRefProfile());
        }
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private void updateCustomerDevice(MessageProcessingContext context, String deviceId, String notificationToken) {
        MPAY_CustomerDevice customerDevice = context.getDataProvider().getCustomerDevice(deviceId);
        if(customerDevice !=null) {
            customerDevice.setExtraData(notificationToken);
            context.getDataProvider().mergeCustomerDevice(customerDevice);
        }
    }

    private void updateCorporateDevice(MessageProcessingContext context, String deviceId, String notificationToken) {
        MPAY_CorporateDevice corporateDevice = context.getDataProvider().getCorporateDevice(deviceId);
        if(corporateDevice !=null) {
            corporateDevice.setExtraData(notificationToken);
            context.getDataProvider().mergeCorporateDevice(corporateDevice);
        }
    }

}
