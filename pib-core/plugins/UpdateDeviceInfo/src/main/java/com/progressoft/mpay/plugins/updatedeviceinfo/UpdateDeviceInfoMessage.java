package com.progressoft.mpay.plugins.updatedeviceinfo;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

public class UpdateDeviceInfoMessage extends MPayRequest {

    private static final String NOTIFICATION_TOKEN = "notificationToken";

    private String notificationToken;

    public UpdateDeviceInfoMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setExtraData(request.getExtraData());
    }

    public static UpdateDeviceInfoMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        UpdateDeviceInfoMessage message = new UpdateDeviceInfoMessage(request);

        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null)
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        message.setNotificationToken(message.getValue(NOTIFICATION_TOKEN));
        return message;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }
}
