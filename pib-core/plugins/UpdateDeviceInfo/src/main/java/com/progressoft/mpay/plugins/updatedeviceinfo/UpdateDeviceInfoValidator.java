package com.progressoft.mpay.plugins.updatedeviceinfo;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateDeviceInfoValidator {
	private static final Logger logger = LoggerFactory.getLogger(UpdateDeviceInfoValidator.class);

	private UpdateDeviceInfoValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			return validateCustomer(context);
		else if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			return validateCorporate(context);
		else
			return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
	}

	private static ValidationResult validateCustomer(MessageProcessingContext context) {
		logger.debug("Inside ValidateCustomer ...");

		ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
		if (!result.isValid())
			return result;

		return SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context) {
		logger.debug("Inside ValidateCorporate ...");
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		return SignInValidator.validate(context, context.getSender().getService(), context.getRequest().getDeviceId());
	}
}
