package com.progressoft.mpay.plugins.transactionsinquiry;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;

public class TransactionInquiryValidator {
	private static final Logger logger = LoggerFactory.getLogger(TransactionInquiryValidator.class);

	private TransactionInquiryValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		return validateCorporate(context);
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context) {
		logger.debug("Inside ValidateCorporate ...");
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,
				context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(),
				context.getRequest().getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(),
				context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		if (!context.getMessage().getMessageType().getIsPinlessEnabled()) {
			result = PinCodeValidator.validate(context, context.getSender().getService(),
					context.getRequest().getPin());
			if (!result.isValid())
				return result;
		}

		return LimitsValidator.validate(context);
	}
}