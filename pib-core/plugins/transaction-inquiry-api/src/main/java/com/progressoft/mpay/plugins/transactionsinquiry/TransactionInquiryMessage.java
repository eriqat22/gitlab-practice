package com.progressoft.mpay.plugins.transactionsinquiry;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class TransactionInquiryMessage extends MPayRequest {

	private static final String REFERENCE = "reference";

	private String reference;

	public TransactionInquiryMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static TransactionInquiryMessage parseMessage(MPayRequest request) throws MessageParsingException {
		TransactionInquiryMessage message = new TransactionInquiryMessage(request);

		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
			throw new MessageParsingException(SENDER_TYPE_KEY);

		message.setReference(message.getValue(REFERENCE));
		if (message.getReference() == null || message.getReference().trim().isEmpty())
			throw new MessageParsingException(REFERENCE);

		return message;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

}
