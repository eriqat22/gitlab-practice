package com.progressoft.mpay.plugins.customerinformation;

import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.WF_Customer;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerInfoValidator {
    private static final Logger logger = LoggerFactory.getLogger(CustomerInfoValidator.class);

    public static ValidationResult validate(MessageProcessingContext context)
    {
        logger.debug("Inside validate ...");
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getService(),
                context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, false);
        if (!result.isValid())
            return result;

        result = validateReceiver(context);
        if(!result.isValid())
            return result;

        return new ValidationResult(ReasonCodes.VALID,null,true);
    }

    private static ValidationResult validateReceiver(MessageProcessingContext context)
    {
        MPAY_CustomerMobile mobile = context.getReceiver().getMobile();
        if( mobile == null)
            return new ValidationResult(ReasonCodes.CUSTOMER_DOES_NOT_EXISTS,null,false);

        MPAY_Customer customer = mobile.getRefCustomer();
        if(!customer.getIsLight())
            return new ValidationResult(ReasonCodes.CUSTOMER_ALREADY_REGISTERED,null,false);

        if(mobile.getRefCustomer().getStatusId().getCode().equals(WF_Customer.STEP_Deleting))
            return new ValidationResult(ReasonCodes.CUSTOMER_DOES_NOT_EXISTS,null,false);


        return new ValidationResult(ReasonCodes.VALID,null,true);
    }

}
