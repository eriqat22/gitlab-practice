package com.progressoft.mpay.plugins.customerinformation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.exceptions.NullArgumentException;

public class CustomerInfoResponse {

    private String firstName;
    private String middleName;
    private String lastName;
    private String dateOfBirth;
    private String idType;
    private String idNumber;
    private String mobileNumber;



    public CustomerInfoResponse(MPAY_CustomerMobile mobile) {
        if (mobile == null)
            throw new NullArgumentException("mobile");

        this.firstName = mobile.getRefCustomer().getFirstName();
        this.middleName = mobile.getRefCustomer().getMiddleName();
        this.lastName = mobile.getRefCustomer().getLastName();
        this.dateOfBirth = mobile.getRefCustomer().getDob().toString();
        this.idType = mobile.getRefCustomer().getIdType().getCode();
        this.idNumber = mobile.getRefCustomer().getIdNum();
        this.mobileNumber = mobile.getMobileNumber();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}
