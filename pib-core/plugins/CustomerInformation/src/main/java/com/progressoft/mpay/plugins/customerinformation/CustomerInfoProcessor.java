package com.progressoft.mpay.plugins.customerinformation;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerInfoProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(CustomerInfoProcessor.class);


    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");

        if(context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult result = CustomerInfoValidator.validate(context);
            if (result.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, result.getReasonCode(), result.getReasonDescription());
        }

        catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in CustomerInquiryProcessor", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws WorkflowException {
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context, true);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }


    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
        logger.debug("Inside PreProcessMessage ...");

        CustomerInfoRequest request = CustomerInfoRequest.parseMessage(context.getRequest());

        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();

        context.setSender(sender);
        context.setReceiver(receiver);
        context.setRequest(request);

        sender.setService(context.getDataProvider().getCorporateService(request.getSender(), ReceiverInfoType.CORPORATE));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(
                SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;

        receiver.setMobile(context.getDataProvider().getCustomerMobile(request.getMobileNumber()));

    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        logger.debug("Inside GenerateResponse ...");
        logger.debug("isAccepted: " + isAccepted);
        final String CUSTOMER_INFO = "custInfo";
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        String customerData = null;
        if (isAccepted) {
            CustomerInfoResponse customer = new CustomerInfoResponse(context.getReceiver().getMobile());
            if (customer != null) {
                customerData = customer.toString();
            }
        }
        response.getExtraData().add(new ExtraData(CUSTOMER_INFO, customerData));
        return response.toString();
    }

}
