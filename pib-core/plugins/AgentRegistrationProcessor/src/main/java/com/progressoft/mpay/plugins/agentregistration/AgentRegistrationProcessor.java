package com.progressoft.mpay.plugins.agentregistration;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.jfw.model.exception.BusinessException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.jfw.model.service.workflow.WorkflowService;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.*;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.registration.customers.CustomerWorkflowStatuses;
import com.progressoft.mpay.registration.customers.PostApproveCustomer;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class AgentRegistrationProcessor implements MessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(AgentRegistrationProcessor.class);

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = AgentRegistrationValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, processingStatusCode);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws InvalidActionException {
        logger.debug("Inside AcceptMessage ...");
        MPAY_Customer customer = getCustomerFromRequest(context);
        registerCustomer(customer, context);
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        String response = generateResponse(context);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private void registerCustomer(MPAY_Customer customer, ProcessingContext context) throws InvalidActionException {
        try {
            JfwHelper.createEntity(MPAYView.CUSTOMER, customer);
            Map<String, Object> v = new HashMap<>();
            v.put(WorkflowService.WF_ARG_BEAN, customer);
            context.getDataProvider().updateWorkflowStep(CustomerWorkflowStatuses.INTEGRATION, customer.getWorkflowId());
            customer.setStatusId(context.getDataProvider().getWorkflowStatus(CustomerWorkflowStatuses.INTEGRATION));
            context.getDataProvider().updateCustomer(customer);
            PostApproveCustomer approveCustomerPostFunction = AppContext.getApplicationContext().getBean("PostApproveCustomer", PostApproveCustomer.class);
            approveCustomerPostFunction.execute(v, null, null);
        } catch (WorkflowException e) {
            throw new BusinessException(e);
        }
    }

    private MPAY_Customer getCustomerFromRequest(MessageProcessingContext context) {
        AgentRegistrationMessage message = (AgentRegistrationMessage) context.getRequest();
        MPAY_Customer customer = new MPAY_Customer();
        customer.setFirstName(message.getFirstName());
        customer.setMiddleName(message.getMidName());
        customer.setLastName(message.getLastName());
        customer.setFullName(customer.getFirstName() + " " + customer.getMiddleName() + " " + customer.getLastName());
        customer.setIsActive(true);
        customer.setNationality((JfwCountry) context.getExtraData().get(Constants.COUNTRY_KEY));
        customer.setIdType((MPAY_IDType) context.getExtraData().get(Constants.ID_TYPE_KEY));
        customer.setIdNum(message.getIdNumber());
        customer.setDob(message.getDateOfBirth());
        customer.setClientType(context.getLookupsLoader().getCustomerClientType());
        customer.setClientRef(message.getReference());
        customer.setIsRegistered(false);
        customer.setCity((MPAY_City) context.getExtraData().get(Constants.CITY_KEY));
        customer.setPrefLang((MPAY_Language) context.getExtraData().get(Constants.LANGUAGE_KEY));
        customer.setMobileNumber(message.getMobileNumber());
        customer.setAlias(message.getAlias());
        String bankedUnbanked = context.getSender().getBank().getSettlementParticipant().equals(BankType.PARTICIPANT) ? BankedUnbankedFlag.BANKED : BankedUnbankedFlag.UNBANKED;
        customer.setBankedUnbanked(bankedUnbanked);
        customer.setBank(context.getSender().getBank());
        customer.setExternalAcc("Master Card");
        customer.setRefProfile((MPAY_Profile) context.getExtraData().get(Constants.PROFILE_KEY));
        customer.setNotificationShowType(NotificationShowTypeCodes.MOBILE_NUMBER);
        customer.setHint(String.valueOf(context.getMessage().getId()));
        customer.setMas(1L);
        customer.setNfcSerial("nfc");
        customer.setMaxNumberOfDevices(20l);

        return customer;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        AgentRegistrationMessage message = AgentRegistrationMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);

        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        OTPHanlder.loadOTP(context, message.getPin());
        MPAY_Bank bank = loadBank(context);
        if (bank == null)
            return;
        sender.setBank(bank);
        loadLookups(context, message);
    }

    private void loadLookups(MessageProcessingContext context, AgentRegistrationMessage message) {
        MPAY_IDType idType = context.getLookupsLoader().getIDType(message.getIdTypeCode());
        if (idType == null)
            return;
        context.getExtraData().put(Constants.ID_TYPE_KEY, idType);
        JfwCountry country = context.getLookupsLoader().getCountry(message.getCountryCode());
        if (country == null)
            return;
        context.getExtraData().put(Constants.COUNTRY_KEY, country);
        MPAY_City city = context.getLookupsLoader().getCity(message.getCityCode());
        if (city == null)
            return;
        context.getExtraData().put(Constants.CITY_KEY, city);
        MPAY_Language language = context.getLookupsLoader().getLanguage(message.getPrefLanguage());
        if (language == null)
            return;
        context.getExtraData().put(Constants.LANGUAGE_KEY, language);
        MPAY_Profile profile = context.getLookupsLoader().getProfile(context.getSystemParameters().getRegistrationProfileCode());
        if (profile == null)
            return;
        context.getExtraData().put(Constants.PROFILE_KEY, profile);
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    private MPAY_Bank loadBank(ProcessingContext context) {
        return context.getLookupsLoader().getBankByCode(context.getSystemParameters().getRegistrationBankCode());
    }

}
