package com.progressoft.mpay.plugins.agentregistration;

import java.util.Calendar;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.time.DateUtils;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.MPAY_City;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_IDType;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_SysConfig;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.MobileNumberValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;

public class AgentRegistrationValidator {

	private AgentRegistrationValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");

		AgentRegistrationMessage message = (AgentRegistrationMessage) context.getRequest();

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = validateSender(context, message);
		if (!result.isValid())
			return result;

		result = MobileNumberValidator.validate(context, message.getMobileNumber());
		if (!result.isValid())
			return result;

		result = validateCustomerData(context, message);
		if (!result.isValid())
			return result;

		result = validateLanguage(context);
		if (!result.isValid())
			return result;

		result = validateProfile(context);
		if (!result.isValid())
			return result;

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateCustomerData(MessageProcessingContext context, AgentRegistrationMessage message) {
		ValidationResult result = validateBank(context);
		if (!result.isValid())
			return result;

		result = validateIdType(context);
		if (!result.isValid())
			return result;

		result = validateDateOfBirth(context, message);
		if (!result.isValid())
			return result;

		result = validateCountry(context);
		if (!result.isValid())
			return result;

		result = validateCity(context);
		if (!result.isValid())
			return result;

		result = validateRegisteredMobile(context, message);
		if (!result.isValid())
			return result;

		result = validateRegisteredCustomer(context, message);
		if (!result.isValid())
			return result;

		result = validateAlias(context, message);
		if (!result.isValid())
			return result;

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateSender(MessageProcessingContext context, AgentRegistrationMessage message) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = PinCodeValidator.validate(context, context.getSender().getService(), message.getPin());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), message.getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(), message.getDeviceId());
		if (!result.isValid())
			return result;
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateDateOfBirth(ProcessingContext context, AgentRegistrationMessage message) {
		Calendar now = Calendar.getInstance();

		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		MPAY_SysConfig r = context.getLookupsLoader().getSystemConfigurations("Min Age");
		long minAge = Integer.parseInt(r.getConfigValue());

		if (DateUtils.addYears(message.getDateOfBirth(), (int) minAge).after(now.getTime()))
			return new ValidationResult(ReasonCodes.CUSTOMER_AGE_NOT_ALLOWED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateIdType(MessageProcessingContext context) {
		MPAY_IDType idType = (MPAY_IDType) context.getExtraData().get(Constants.ID_TYPE_KEY);
		if (idType == null || !idType.getIsCustomer())
			return new ValidationResult(ReasonCodes.INVALID_ID_TYPE, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateCountry(MessageProcessingContext context) {
		JfwCountry country = (JfwCountry) context.getExtraData().get(Constants.COUNTRY_KEY);
		if (country == null)
			return new ValidationResult(ReasonCodes.INVALID_NATIONALITY, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateCity(MessageProcessingContext context) {
		MPAY_City city = (MPAY_City) context.getExtraData().get(Constants.CITY_KEY);
		if (city == null)
			return new ValidationResult(ReasonCodes.CITY_NOT_DEFINED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateLanguage(MessageProcessingContext context) {
		MPAY_Language language = (MPAY_Language) context.getExtraData().get(Constants.LANGUAGE_KEY);
		if (language == null)
			return new ValidationResult(ReasonCodes.LANGUAGE_NOT_DEFINED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateRegisteredMobile(ProcessingContext context, AgentRegistrationMessage message) {
		MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(message.getMobileNumber());
		if (mobile == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		if (mobile.getDeletedFlag() == null || !mobile.getDeletedFlag())
			return new ValidationResult(ReasonCodes.MOBILE_ALREADY_REGISTERED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateRegisteredCustomer(ProcessingContext context, AgentRegistrationMessage message) {
		MPAY_Customer customer = context.getDataProvider().getCustomer(message.getIdTypeCode(), message.getIdNumber());
		if (customer == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		if (customer.getDeletedFlag() == null || !customer.getDeletedFlag())
			return new ValidationResult(ReasonCodes.CUSTOMER_ALREADY_REGISTERED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateAlias(ProcessingContext context, AgentRegistrationMessage message) {
		if (message.getAlias() == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(message.getAlias(), ReceiverInfoType.ALIAS);
		if (mobile != null)
			return new ValidationResult(ReasonCodes.ALIAS_IS_DEFINED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateBank(MessageProcessingContext context) {
		if (context.getSender().getBank() == null)
			return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
		if (context.getSender().getBank().getDeletedFlag() != null && context.getSender().getBank().getDeletedFlag())
			return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
		if (!context.getSender().getBank().getIsActive())
			return new ValidationResult(ReasonCodes.BANK_INACTIVE, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateProfile(MessageProcessingContext context) {
		MPAY_Profile profile = (MPAY_Profile) context.getExtraData().get(Constants.PROFILE_KEY);
		if (profile == null)
			return new ValidationResult(ReasonCodes.PROFILE_NOT_DEFINED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
