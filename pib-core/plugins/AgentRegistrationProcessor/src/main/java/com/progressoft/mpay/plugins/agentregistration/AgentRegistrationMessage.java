package com.progressoft.mpay.plugins.agentregistration;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class AgentRegistrationMessage extends MPayRequest {

	private static final Logger logger = LoggerFactory.getLogger(AgentRegistrationMessage.class);

	private static final String FIRST_NAME_KEY = "firstName";
	private static final String MID_NAME_KEY = "midName";
	private static final String LAST_NAME_KEY = "lastName";
	private static final String ID_TYPE_CODE_KEY = "idTypeCode";
	private static final String ID_NUMBER_KEY = "idNumber";
	private static final String DATE_OF_BIRTH_KEY = "dateOfBirth";
	private static final String REFERENCE_KEY = "reference";
	private static final String COUNTRY_CODE_KEY = "countryCode";
	private static final String CITY_CODE_KEY = "cityCode";
	private static final String PREF_LANGUAGE_KEY = "prefLanguage";
	private static final String ALIAS_KEY = "alias";
	private static final String ADDRESS_KEY = "address";
	private static final String MOBILE_NUMBER_KEY = "mobileNumber";

	private String mobileNumber;
	private String firstName;
	private String midName;
	private String lastName;
	private String idTypeCode;
	private String idNumber;
	private Date dateOfBirth;
	private String reference;
	private String countryCode;
	private String cityCode;
	private long prefLanguage;
	private String alias;
	private String address;

	public AgentRegistrationMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static AgentRegistrationMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		AgentRegistrationMessage message = new AgentRegistrationMessage(request);
		validateGenericFields(message);
		message.setMobileNumber(message.getValue(MOBILE_NUMBER_KEY));
		if (message.getMobileNumber() == null || message.getMobileNumber().trim().length() == 0)
			throw new MessageParsingException(MOBILE_NUMBER_KEY);
		message.setFirstName(message.getValue(FIRST_NAME_KEY));
		validateCustomerFields(message);
		message.setReference(message.getValue(REFERENCE_KEY));
		if (message.getReference() == null || message.getReference().trim().length() == 0)
			throw new MessageParsingException(REFERENCE_KEY);
		message.setCountryCode(message.getValue(COUNTRY_CODE_KEY));
		if (message.getCountryCode() == null || message.getCountryCode().trim().length() == 0)
			throw new MessageParsingException(COUNTRY_CODE_KEY);
		message.setCityCode(message.getValue(CITY_CODE_KEY));
		if (message.getCityCode() == null || message.getCityCode().trim().length() == 0)
			throw new MessageParsingException(CITY_CODE_KEY);
		try {
			message.setPrefLanguage(Long.parseLong(message.getValue(PREF_LANGUAGE_KEY)));
		} catch (Exception e) {
			logger.debug("Invalid Language Id Received", e);
			throw new MessageParsingException(PREF_LANGUAGE_KEY);
		}
		message.setAlias(message.getValue(ALIAS_KEY));
		message.setAddress(message.getValue(ADDRESS_KEY));
		return message;
	}

	private static void validateCustomerFields(AgentRegistrationMessage message) throws MessageParsingException {
		if (StringUtils.isEmpty(message.getFirstName()))
			throw new MessageParsingException(FIRST_NAME_KEY);
		message.setMidName(message.getValue(MID_NAME_KEY));
		if (StringUtils.isEmpty(message.getMidName()))
			throw new MessageParsingException(MID_NAME_KEY);
		message.setLastName(message.getValue(LAST_NAME_KEY));
		if (StringUtils.isEmpty(message.getLastName()))
			throw new MessageParsingException(LAST_NAME_KEY);
		message.setIdTypeCode(message.getValue(ID_TYPE_CODE_KEY));
		if (StringUtils.isEmpty(message.getIdTypeCode()))
			throw new MessageParsingException(ID_TYPE_CODE_KEY);
		message.setIdNumber(message.getValue(ID_NUMBER_KEY));
		if (StringUtils.isEmpty(message.getIdNumber()))
			throw new MessageParsingException(ID_NUMBER_KEY);
		try {
			SimpleDateFormat format = new SimpleDateFormat(Constants.BIRTH_DATE_FORMAT);
			message.setDateOfBirth(format.parse(message.getValue(DATE_OF_BIRTH_KEY)));
		} catch (Exception e) {
			logger.debug("Invalid Date Received", e);
			throw new MessageParsingException(DATE_OF_BIRTH_KEY);
		}
	}

	private static void validateGenericFields(AgentRegistrationMessage message) throws MessageParsingException {
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMidName() {
		return midName;
	}

	public void setMidName(String midName) {
		this.midName = midName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdTypeCode() {
		return idTypeCode;
	}

	public void setIdTypeCode(String idTypeCode) {
		this.idTypeCode = idTypeCode;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public long getPrefLanguage() {
		return prefLanguage;
	}

	public void setPrefLanguage(long prefLanguage) {
		this.prefLanguage = prefLanguage;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
