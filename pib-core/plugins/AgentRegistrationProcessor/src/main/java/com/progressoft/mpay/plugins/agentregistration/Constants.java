package com.progressoft.mpay.plugins.agentregistration;

public class Constants {
	public static final String ID_TYPE_KEY = "IdTypeKey";
	public static final String COUNTRY_KEY = "CountryKey";
	public static final String CITY_KEY = "CityKey";
	public static final String LANGUAGE_KEY = "LanguageKey";
	public static final String PROFILE_KEY = "ProfileKey";
	public static final String BIRTH_DATE_FORMAT = "dd/MM/yyyy";

	private Constants() {

	}
}
