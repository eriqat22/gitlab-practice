package com.progressoft.mpay.plugins.activation;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class DeviceValidator {
	private static final Logger logger = LoggerFactory.getLogger(DeviceValidator.class);

	private DeviceValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context, String deviceId) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		if (deviceId == null)
			throw new NullArgumentException("deviceId");
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			return validateCustomerDevice(context, deviceId);
		else if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			return validateCorporateDevice(context, deviceId);
		else
			throw new InvalidOperationException("Alias is not supported as a sender");
	}					

	private static ValidationResult validateCustomerDevice(MessageProcessingContext context, String deviceId) {
		MPAY_CustomerDevice device;
		if (context.getExtraData().containsKey(Constants.CLIENT_DEVICE_KEY))
			device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.CLIENT_DEVICE_KEY);
		else
			device = context.getDataProvider().getCustomerDevice(deviceId);
		if (device == null || SystemHelper.isDeleted(device) || device.getRefCustomerMobile().getRefCustomer().getId() == context.getSender().getCustomer().getId())
			return new ValidationResult(ReasonCodes.VALID, null, true);
		else if (device.getIsStolen())
			return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);
		return new ValidationResult(ReasonCodes.DEVICE_IS_ALREADY_IN_USE, null, false);
	}

	private static ValidationResult validateCorporateDevice(MessageProcessingContext context, String deviceId) {
		MPAY_CorporateDevice device;
		if (context.getExtraData().containsKey(Constants.CLIENT_DEVICE_KEY))
			device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.CLIENT_DEVICE_KEY);
		else
			device = context.getDataProvider().getCorporateDevice(deviceId);
		if (device == null || SystemHelper.isDeleted(device) || device.getRefCorporateService().getRefCorporate().getId() == context.getSender().getCorporate().getId())
			return new ValidationResult(ReasonCodes.VALID, null, true);
		else if (device.getIsStolen())
			return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);
		return new ValidationResult(ReasonCodes.DEVICE_IS_ALREADY_IN_USE, null, false);
	}
}
