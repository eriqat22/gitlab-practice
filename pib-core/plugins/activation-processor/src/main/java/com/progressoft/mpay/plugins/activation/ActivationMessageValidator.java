package com.progressoft.mpay.plugins.activation;


import com.progressoft.mpay.entities.WF_Customer;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;

public class ActivationMessageValidator {
    private static final Logger logger = LoggerFactory.getLogger(ActivationMessageValidator.class);

    private ActivationMessageValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ...");
        if (context == null)
            throw new NullArgumentException("context");
        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;
        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;
        if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
            return validateCustomer(context);
        else if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
            return validateCorporate(context);
        else
            return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
    }

    private static ValidationResult validateCustomer(MessageProcessingContext context) {
        logger.debug("Inside ValidateCustomer ...");
        ActivationMessage message = (ActivationMessage) context.getRequest();
        if (!context.getSender().getCustomer().getIdNum().equals(message.getId()))
            return new ValidationResult(ReasonCodes.INVALID_CLIENT_ID_NUMBER, null, false);

        ValidationResult result = validateActivationCode(context.getSender().getMobile(), message.getActivationCode());
        if (!result.isValid())
            return result;

        if (isCustomerOnCreatedStep(context))
            return new ValidationResult(ReasonCodes.VISIT_NEAREST_BRANCH, null, false);


        result = MobileValidator.validate(context.getSender().getMobile(), true);
        if (!result.isValid())
            return result;

        result = CustomerValidator.validate(context.getSender().getCustomer(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getMobileAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        result = DeviceValidator.validate(context, context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = validateMaxNumberOfCustomerDevices(context);
        if (!result.isValid())
            return result;

        return LimitsValidator.validate(context);
    }

    private static boolean isCustomerOnCreatedStep(MessageProcessingContext context) {
        return context.getSender().getCustomer().getStatusId().getCode().equals(WF_Customer.STEP_Created);
    }

    private static ValidationResult validateActivationCode(MPAY_CustomerMobile mobile, String activationCode) {
        if (activationCode == null)
            return new ValidationResult(ReasonCodes.INVALID_ACTIVATION_CODE, null, false);
        if (!activationCode.equals(mobile.getActivationCode()))
            return new ValidationResult(ReasonCodes.INVALID_ACTIVATION_CODE, null, false);
        if (mobile.getActivationCodeValidy() == null || SystemHelper.getSystemTimestamp().after(mobile.getActivationCodeValidy()))
            return new ValidationResult(ReasonCodes.ACTIVATION_CODE_EXPIRED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateActivationCode(MPAY_CorpoarteService service, String activationCode) {
        if (activationCode == null)
            return new ValidationResult(ReasonCodes.INVALID_ACTIVATION_CODE, null, false);
        if (!activationCode.equals(service.getActivationCode()))
            return new ValidationResult(ReasonCodes.INVALID_ACTIVATION_CODE, null, false);
        if (SystemHelper.getSystemTimestamp().after(service.getActivationCodeValidy()))
            return new ValidationResult(ReasonCodes.ACTIVATION_CODE_EXPIRED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateMaxNumberOfCustomerDevices(MessageProcessingContext context) {
        if (context.getSender().getMobile().getMaxNumberOfDevices() == 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (context.getDataProvider().listMobileDevices(context.getSender().getMobile().getId()).size() >= context.getSender().getMobile().getMaxNumberOfDevices())
            return new ValidationResult(ReasonCodes.MAX_NUMBER_OF_DEVICES_REACHED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateCorporate(MessageProcessingContext context) {
        logger.debug("Inside ValidateCorporate ...");
        ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        ActivationMessage message = (ActivationMessage) context.getRequest();
        if (!context.getSender().getCorporate().getRegistrationId().equals(message.getId()))
            return new ValidationResult(ReasonCodes.INVALID_CLIENT_ID_NUMBER, null, false);

        result = validateActivationCode(context.getSender().getService(), message.getActivationCode());
        if (!result.isValid())
            return result;

        result = DeviceValidator.validate(context, context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = validateMaxNumberOfCorporateDevices(context);
        if (!result.isValid())
            return result;

        return LimitsValidator.validate(context);
    }

    private static ValidationResult validateMaxNumberOfCorporateDevices(MessageProcessingContext context) {
        if (context.getSender().getService().getMaxNumberOfDevices() == 0)
            return new ValidationResult(ReasonCodes.VALID, null, true);
        if (context.getDataProvider().listServiceDevices(context.getSender().getService().getId()).size() >= context.getSender().getService().getMaxNumberOfDevices())
            return new ValidationResult(ReasonCodes.MAX_NUMBER_OF_DEVICES_REACHED, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
