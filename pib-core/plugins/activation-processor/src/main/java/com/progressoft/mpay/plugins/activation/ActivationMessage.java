package com.progressoft.mpay.plugins.activation;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class ActivationMessage extends MPayRequest {

	private static final String ACTIVATION_CODE_KEY = "activationCode";
	private static final String CLIENT_ID_KEY = "id";
	private static final String PASS_WORD_KEY = "pass";
	private static final String DEVICE_NAME_KEY = "deviceName";
	private static final String NOTIFICATION_TOKEN_KEY = "notificationToken";
	private static final String PUBLIC_KEY = "key";

	private String activationCode;
	private String id;
	private String pass;
	private String deviceName;
	private String notificationToken;
	private String publicKey;

	public ActivationMessage() {
		//Default
	}

	public ActivationMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getNotificationToken() {
		return notificationToken;
	}

	public void setNotificationToken(String notificationToken) {
		this.notificationToken = notificationToken;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public static ActivationMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		ActivationMessage message = new ActivationMessage(request);
		validateGenericFields(message);
		message.setActivationCode(message.getValue(ACTIVATION_CODE_KEY));
		if (message.getActivationCode() == null)
			throw new MessageParsingException(ACTIVATION_CODE_KEY);
		message.setId(message.getValue(CLIENT_ID_KEY));
		if (message.getId() == null)
			throw new MessageParsingException(CLIENT_ID_KEY);
		message.setPass(message.getValue(PASS_WORD_KEY));
		if (message.getPass() == null || message.getPass().trim().length() == 0)
			throw new MessageParsingException(PASS_WORD_KEY);
		message.setPin(message.getValue(PIN_CODE_KEY));
		if (message.getPin() == null || message.getPin().trim().length() == 0)
			throw new MessageParsingException(PIN_CODE_KEY);
		message.setDeviceName(message.getValue(DEVICE_NAME_KEY));
		if (message.getDeviceName() == null)
			throw new MessageParsingException(DEVICE_NAME_KEY);

		if (message.getValue(NOTIFICATION_TOKEN_KEY) != null)
			message.setNotificationToken(message.getValue(NOTIFICATION_TOKEN_KEY));

		message.setPublicKey(message.getValue(PUBLIC_KEY));
		if (message.getPublicKey() == null)
			throw new MessageParsingException(PUBLIC_KEY);
		return message;
	}

	private static void validateGenericFields(ActivationMessage message) throws MessageParsingException {
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
	}
}
