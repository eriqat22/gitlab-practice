package com.progressoft.mpay.plugins.activation;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;

public class ActivationProcessor implements MessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ActivationProcessor.class);

    private static final String DEVICE_APPROVED_STATUS = "1100105";

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
        logger.debug("Inside ProcessIntegration ...");
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult validationResult = ActivationMessageValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when processing message", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        return createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws InvalidActionException, WorkflowException {
        logger.debug("Inside AcceptMessage ...");
        ActivationMessage request = (ActivationMessage) context.getRequest();
        boolean isNewDevice = (boolean) context.getExtraData().get(Constants.IS_NEW_DEVICE_KEY);
        boolean isMobile = request.getSenderType().equals(ReceiverInfoType.MOBILE);
        if (isNewDevice)
            addNewDevice(context, request, isMobile);
        else
            updateDevice(context, request, isMobile);
        if (isMobile) {
            context.getSender().getMobile().setPin(request.getPin());
            context.getSender().getMobile().setPinLastChanged(SystemHelper.getSystemTimestamp());
            context.getSender().getMobile().setActivationCode(null);
            context.getSender().getMobile().setActivationCodeValidy(null);
            context.getDataProvider().mergeCustomerMobile(context.getSender().getMobile());
        } else {
            context.getSender().getService().setPin(request.getPin());
            context.getSender().getService().setPinLastChanged(SystemHelper.getSystemTimestamp());
            context.getSender().getService().setActivationCode(null);
            context.getSender().getService().setActivationCodeValidy(null);
            context.getDataProvider().mergeCorporateService(context.getSender().getService());
        }
        return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
    }

    private void updateDevice(MessageProcessingContext context, ActivationMessage request, boolean isMobile) throws WorkflowException, InvalidActionException {
        if (isMobile) {
            MPAY_CustomerDevice device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.CLIENT_DEVICE_KEY);
            device.setPassword(request.getPass());
            device.setPasswordLastChanged(SystemHelper.getSystemTimestamp());
            device.setActiveDevice(true);
            device.setDeletedFlag(false);
            device.setDeviceName(request.getDeviceName());
            if (request.getNotificationToken() != null)
                device.setExtraData(request.getNotificationToken());
            device.setPublicKey(request.getPublicKey());
            context.getDataProvider().updateWorkflowStep(DEVICE_APPROVED_STATUS, device.getWorkflowId());
            device.setStatusId(context.getDataProvider().getWorkflowStatus(DEVICE_APPROVED_STATUS));
            context.getDataProvider().mergeCustomerDevice(device);
        } else {
            MPAY_CorporateDevice device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.CLIENT_DEVICE_KEY);
            device.setPassword(request.getPass());
            device.setPasswordLastChanged(SystemHelper.getSystemTimestamp());
            device.setActiveDevice(true);
            device.setDeletedFlag(false);
            device.setDeviceName(request.getDeviceName());
            device.setPublicKey(request.getPublicKey());
            if (request.getNotificationToken() != null)
                device.setExtraData(request.getNotificationToken());
            device.setStatusId(context.getDataProvider().getWorkflowStatus(DEVICE_APPROVED_STATUS));
            context.getDataProvider().mergeCorporateDevice(device);
        }
    }

    private void addNewDevice(MessageProcessingContext context, ActivationMessage request, boolean isMobile) throws InvalidActionException {
        if (isMobile) {
            MPAY_CustomerDevice device = new MPAY_CustomerDevice();
            device.setRefCustomerMobile(context.getSender().getMobile());
            device.setDeviceID(request.getDeviceId());
            device.setDeviceName(request.getDeviceName());
            device.setIsStolen(false);
            device.setPassword(request.getPass());
            device.setPasswordLastChanged(SystemHelper.getSystemTimestamp());
            device.setActiveDevice(true);
            device.setIsOnline(false);
            device.setRetryCount(0L);
            device.setIsBlocked(false);
            device.setExtraData(request.getNotificationToken());
            device.setIsDefault(checkIfFirstCustomerDevice(context.getSender().getMobile().getId(), context.getDataProvider()));
            device.setPublicKey(request.getPublicKey());
            JfwHelper.createEntity(MPAYView.CUSTOMER_DEVICES, device);
            long workflowId = device.getWorkflowId();
            device = context.getDataProvider().getCustomerDevice(device.getDeviceID());
            context.getDataProvider().updateWorkflowStep(DEVICE_APPROVED_STATUS, workflowId);
            device.setStatusId(context.getDataProvider().getWorkflowStatus(DEVICE_APPROVED_STATUS));
            device.setWorkflowId(workflowId);
            context.getDataProvider().mergeCustomerDevice(device);
        } else {
            MPAY_CorporateDevice device = new MPAY_CorporateDevice();
            device.setRefCorporateService(context.getSender().getService());
            device.setDeviceID(request.getDeviceId());
            device.setDeviceName(request.getDeviceName());
            device.setIsStolen(false);
            device.setPassword(request.getPass());
            device.setPasswordLastChanged(SystemHelper.getSystemTimestamp());
            device.setActiveDevice(true);
            device.setIsOnline(false);
            device.setRetryCount(0L);
            device.setIsBlocked(false);
            device.setIsDefault(checkIfFirstCorporateDevice(context.getSender().getService().getId(), context.getDataProvider()));
            device.setPublicKey(request.getPublicKey());

            if (request.getNotificationToken() != null)
                device.setExtraData(request.getNotificationToken());

            JfwHelper.createEntity(MPAYView.CORPORATE_DEVICES, device);
            long workflowId = device.getWorkflowId();
            device = context.getDataProvider().getCorporateDevice(device.getDeviceID());
            context.getDataProvider().updateWorkflowStep(DEVICE_APPROVED_STATUS, workflowId);
            device.setStatusId(context.getDataProvider().getWorkflowStatus(DEVICE_APPROVED_STATUS));
            device.setWorkflowId(workflowId);
            context.getDataProvider().mergeCorporateDevice(device);
        }
    }

    private boolean checkIfFirstCustomerDevice(long mobileId, IDataProvider dataProvider) {
        return dataProvider.listMobileDevices(mobileId).isEmpty();
    }

    private boolean checkIfFirstCorporateDevice(long serviceId, IDataProvider dataProvider) {
        return dataProvider.listServiceDevices(serviceId).isEmpty();
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
        logger.debug("Inside PreProcessMessage ...");
        ActivationMessage message = ActivationMessage.parseMessage(context.getRequest());
        context.setRequest(message);
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setReceiver(receiver);
        if (message.getSenderType().equals(ReceiverInfoType.MOBILE))
            context.setSender(loadSenderMobile(context, message));
        else
            context.setSender(loadSenderService(context, message));
    }

    private ProcessingContextSide loadSenderService(MessageProcessingContext context, ActivationMessage message) {
        ProcessingContextSide sender = new ProcessingContextSide();
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return sender;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return sender;
        sender.setProfile(sender.getServiceAccount().getRefProfile());

        MPAY_CorporateDevice device = context.getDataProvider().getCorporateDevice(message.getDeviceId());
        if (device == null)
            context.getExtraData().put(Constants.IS_NEW_DEVICE_KEY, true);
        else {
            context.getExtraData().put(Constants.IS_NEW_DEVICE_KEY, false);
            context.getExtraData().put(Constants.CLIENT_DEVICE_KEY, device);
        }
        return sender;
    }

    private ProcessingContextSide loadSenderMobile(MessageProcessingContext context, ActivationMessage message) {
        ProcessingContextSide sender = new ProcessingContextSide();
        sender.setMobile(context.getDataProvider().getCustomerMobile(message.getSender()));
        if (sender.getMobile() == null)
            return sender;
        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
        if (sender.getMobileAccount() == null)
            return sender;
        sender.setProfile(sender.getMobileAccount().getRefProfile());
        MPAY_CustomerDevice device = context.getDataProvider().getCustomerDevice(message.getDeviceId());
        if (device == null)
            context.getExtraData().put(Constants.IS_NEW_DEVICE_KEY, true);
        else {
            context.getExtraData().put(Constants.IS_NEW_DEVICE_KEY, false);
            context.getExtraData().put(Constants.CLIENT_DEVICE_KEY, device);
        }
        return sender;
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext arg0) {
        logger.debug("Inside Reverse ...");
        return null;
    }

    @Override
    public boolean isNeedToSessionId() {
        return false;
    }
}
