package com.progressoft.mpay.plugins.activation;

public class Constants {
	public static final String IS_NEW_DEVICE_KEY = "IsNewDevice";
	public static final String CLIENT_DEVICE_KEY = "ClientDevice";

	private Constants() {

	}
}
