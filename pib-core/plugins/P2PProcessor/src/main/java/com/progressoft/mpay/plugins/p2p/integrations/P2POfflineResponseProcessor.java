package com.progressoft.mpay.plugins.p2p.integrations;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class P2POfflineResponseProcessor extends IntegrationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(P2POfflineResponseProcessor.class);

    @Override
    public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside ProcessIntegration ...");
        if (context.getMpClearIsoMessage().getField(44).getValue().equals("00"))
            return Accept(context);
        else
            return Reject(context);
    }

    private IntegrationProcessingResult Accept(IntegrationProcessingContext context) {
        logger.debug("Inside Accept ...");
        return IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
    }

    private IntegrationProcessingResult Reject(IntegrationProcessingContext context) {
        logger.debug("Inside Reject ...");
        return IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
                context.getMpClearIsoMessage().getField(44).getValue() + " " + context.getMpClearIsoMessage().getField(47).getValue(), null, false);
    }
}
