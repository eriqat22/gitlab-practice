package com.progressoft.mpay.plugins.p2p;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.Constants;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.plugins.p2p.integrations.IntegrationProcessor;
import com.progressoft.mpay.plugins.p2p.integrations.IntegrationProcessorFactory;

import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class P2PProcessor implements MessageProcessor {
    public static final String REASON_DESC = "reasonDesc";
    private static final Logger logger = LoggerFactory.getLogger(P2PProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = P2PMessageValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
                    e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside ProcessIntegration ...");
        IsoMessage mpClearIsoMessage = context.getMpClearIsoMessage();
        try {
            IntegrationProcessor processor = IntegrationProcessorFactory.createProcessor(mpClearIsoMessage.getType());
            IntegrationProcessingResult result = processor.ProcessIntegration(context);
            handleCommissions(context, result.getProcessingStatus());
            return result;
        } catch (Exception e) {
            logger.error("Error when ProcessIntegration", e);
            IntegrationProcessingResult result = IntegrationProcessingResult.create(context,
                    ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null,
                    context.isRequest());
            if (context.isRequest())
                result.setMpClearOutMessage(MPClearHelper.createMPClearResponse(context,
                        mpClearIsoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(),
                        MPClearHelper.getResponseType(mpClearIsoMessage.getType()), result.getReason().getCode(),
                        result.getReasonDescription(), String.valueOf(ProcessingCodeRanges.CREDIT_START_VALUE)));
            return result;
        }
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside Reverse ...");
        try {
            P2PMessage message = P2PMessage.parseMessage(context.getRequest());
            JVPostingResult result = TransactionHelper.reverseTransaction(context);
            context.getDataProvider().updateAccountLimit(
                    SystemHelper.getMobileAccount(context.getSystemParameters(), context.getSender().getMobile(),
                            message.getSenderAccount()).getRefAccount().getId(),
                    context.getMessage().getRefOperation().getMessageType().getId(),
                    context.getTransaction().getTotalAmount().negate(), -1, SystemHelper.getCurrentDateWithoutTime());
            context.getDataProvider().mergeTransaction(context.getTransaction());
            if (result.isSuccess())
                return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
            return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
        } catch (Exception ex) {
            logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
            return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(),
                    ProcessingStatusCodes.FAILED);
        }
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException, WorkflowException {
        logger.debug("Inside AcceptMessage ...");
        P2PMessage message = (P2PMessage) context.getRequest();
        MessageProcessingResult result = new MessageProcessingResult();
        MPAY_ProcessingStatus status;
        MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
        String reasonDescription = null;

        context.getMessage().setReason(reason);
        MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT,
                BigDecimal.ZERO, message.getNotes());
        result.setMessage(context.getMessage());
        context.setTransaction(transaction);
        result.setTransaction(transaction);
        context.getDataProvider().persistProcessingResult(result);
        JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(),
                context.getLookupsLoader(), transaction);
        if (postingResult.isSuccess()) {
            BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
            if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
                status = handleMPClear(context, message, result);
//                handleSubscriptionFees(context);
            } else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
                reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
                reasonDescription = integResult.getReasonDescription();
                context.getMessage().setProcessingStatus(status);
                context.getMessage().setReason(reason);
                context.getMessage().setReasonDesc(reasonDescription);
                MPayResponse mPayResponse = generateResponse(context);
                mPayResponse.getExtraData().add(new ExtraData(REASON_DESC, reasonDescription));
                result.getMessage().setResponseContent(mPayResponse.toString());
                result.setResponse(mPayResponse.toString());
                TransactionHelper.reverseTransaction(context);
            } else {
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
                reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
                reasonDescription = integResult.getReasonDescription();
                TransactionHelper.reverseTransaction(context);
            }
        } else {
            if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
                context.getMessage().setReasonDesc(postingResult.getReason());
            } else
                reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
        }
        context.getMessage().setProcessingStatus(status);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
        transaction.setReason(context.getMessage().getReason());
        transaction.setReasonDesc(context.getMessage().getReasonDesc());
        result.setStatus(status);
        result.setStatusDescription(context.getMessage().getReasonDesc());
        handleNotifications(context, result, status);
        OTPHanlder.removeOTP(context);
        handleCommissions(context, status);
        handleLimits(context, status, transaction);
        return result;
    }

//    private void handleSubscriptionFees(MessageProcessingContext context) {
//        MPAY_MobileAccount account = context.getReceiver().getMobileAccount();
//        MPAY_Account refAccount = account.getRefAccount();
//        if (refAccount != null)
//            if (!refAccount.getIsPayMonthlyFees()) {
//                BigDecimal balance = context.getDataProvider().getBalanceByAccountId(refAccount.getId());
//                if (refAccount.getDueAmount().compareTo(balance) == 0 || refAccount.getDueAmount().compareTo(balance) < 0) {
//                    HandleSubscriptionFees.handleSubscriptionFees(account,
//                            AppContext.getCurrentTenant(),
//                            AppContext.getCurrentUser().getPrefOrg().getOrgId(),
//                            account.getRefAccount().getDueAmount());
//                }
//            }
//    }

    private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
        if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            return;
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.OUTWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(), CommissionDirections.SENDER);
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.INWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(), CommissionDirections.RECEIVER);
    }

    private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, P2PMessage message, MessageProcessingResult result) throws WorkflowException {
        MPAY_ProcessingStatus status;
        Integer mpClearIntegType;
        if (context.getSystemParameters().getOfflineModeEnabled() && context.getDirection() == TransactionDirection.ONUS) {
            mpClearIntegType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
            result.setHandleMPClearOffline(true);
        } else {
            mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        }
        boolean isSenderAlias = context.getSender().getMobile().getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS)
                && context.getSender().getMobile().getAlias() != null
                && context.getSender().getMobile().getAlias().length() > 0;
        IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, isSenderAlias, message.getReceiverType().equals(ReceiverInfoType.ALIAS));
        MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegType));
        messageLog.setRefMessage(context.getMessage());
        result.setMpClearMessage(messageLog);
        return status;
    }

    private void handlePushNotification(MessageProcessingContext context, P2PMessage message) {
        String receiverDeviceToken = null;
        if (context.getReceiver().getMobile() != null) {
            receiverDeviceToken = context.getReceiver().getMobile().getRefCustomerMobileCustomerDevices()
                    .stream()
                    .filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                    .findFirst()
                    .map(MPAY_CustomerDevice::getExtraData).orElse(null);
        }
        MPAY_CustomerDevice senderCustomerDevice = context.getDataProvider().getCustomerDevice(message.getDeviceId());
        context.setSenderNotificationToken(senderCustomerDevice != null ? senderCustomerDevice.getExtraData() : null);
        context.setReceiverNotificationToken(receiverDeviceToken);
    }

    private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result, MPAY_ProcessingStatus status) {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)) {
            result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
        } else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
            result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
    }

    private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status, MPAY_Transaction transaction) throws SQLException {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED) || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
            context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
                    SystemHelper.getCurrentDateWithoutTime());
        }
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException, SQLException {
        logger.debug("Inside PreProcessMessage ...");
        P2PMessage message = P2PMessage.parseMessage(context.getRequest());

        context.setRequest(message);
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setAmount(message.getAmount());
        sender.setInfo(message.getSender());
        context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        sender.setMobile(context.getDataProvider().getCustomerMobile(message.getSender()));
        if (sender.getMobile() == null)
            return;
        sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(),
                message.getSenderAccount()));
        if (sender.getMobileAccount() == null)
            return;
        sender.setAccount(sender.getMobileAccount().getRefAccount());
        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setBank(sender.getMobileAccount().getBank());
        sender.setProfile(sender.getMobileAccount().getRefProfile());
        if (sender.getAccount() == null)
            return;
        sender.setBanked(sender.getAccount().getIsBanked());
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
                context.getOperation().getMessageType().getId()));
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
                context.getOperation().getMessageType().getId()));
        sender.setCommission(context.getDataProvider()
                .getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
        if (message.getCardPIN() != null && !message.getCardPIN().trim().equals(""))
            context.getExtraData().put(Constants.CARD_PIN_KEY, message.getCardPIN());
        receiver.setInfo(message.getReceiverInfo());
        receiver.setMobile(
                context.getDataProvider().getCustomerMobile(message.getReceiverInfo(), message.getReceiverType()));
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        OTPHanlder.loadOTP(context, message.getPin());
        receiver.setNotes(message.getNotes());
        if (receiver.getMobile() != null) {
            receiver.setCustomer(receiver.getMobile().getRefCustomer());
            receiver.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), receiver.getMobile(),
                    message.getReceiverAccount()));
            if (receiver.getMobileAccount() == null)
                return;
            receiver.setAccount(receiver.getMobileAccount().getRefAccount());
            receiver.setBank(receiver.getMobileAccount().getBank());
            receiver.setProfile(receiver.getMobileAccount().getRefProfile());
            if (receiver.getAccount() == null)
                return;
            receiver.setBanked(receiver.getAccount().getIsBanked());
            receiver.setCommission(context.getDataProvider().getClientCommission(
                    context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
            context.setDirection(TransactionDirection.ONUS);
            context.setTransactionConfig(
                    context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(),
                            personClientType, receiver.isBanked(), context.getOperation().getMessageType().getId(),
                            context.getTransactionNature(), context.getOperation().getId()));
        } else {

            context.getReceiver().setService(context.getLookupsLoader().getPSP().getPspMPClearService());
            context.getReceiver().setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(),
                    context.getReceiver().getService(), null));
            if (context.getReceiver().getServiceAccount() != null)
                context.getReceiver().setBank(context.getReceiver().getServiceAccount().getBank());
            context.setDirection(TransactionDirection.OUTWARD);
            context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
                    sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
                    context.getOperation().getMessageType().getId(), context.getTransactionNature(),
                    context.getOperation().getId()));
        }
        if (context.getTransactionConfig() != null)
            context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
        handlePushNotification(context, message);
        ChargesCalculator.calculate(context);
        TaxCalculator.claculate(context);
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        if (context.getTransaction() != null) {
            BankIntegrationResult result = BankIntegrationHandler.reverse(context);
            if (result != null) {
                TransactionHelper.reverseTransaction(context);
            }
        }
        return MessageProcessingResult.create(context, status, reason, reasonDescription);
    }

    private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isSenderAlias, boolean isReceiverAlias) throws WorkflowException {
        logger.debug("Inside CreateMPClearRequest ...");
        P2PMessage request = (P2PMessage) context.getRequest();
        String messageID = context.getDataProvider().getNextMPClearMessageId();
        BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
        IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
                .newMessage(type);
        message.setBinary(false);

        String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(),
                context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
        String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

        message.setField(4, new IsoValue<>(IsoType.NUMERIC, formatedAmount, 12));
        MPClearHelper.setMessageEncoded(message);
        message.setField(7, new IsoValue<>(IsoType.DATE10,
                MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
        message.setField(46, new IsoValue<>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
        message.setField(49,
                new IsoValue<>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

        String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getMobile(),
                context.getSender().getMobileAccount(), isSenderAlias);
        MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

        String account2;
        if (context.getReceiver().getMobile() == null)
            account2 = MPClearHelper.getAccount(context.getSystemParameters(), request.getReceiverInfo(),
                    request.getReceiverType());
        else
            account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getMobile(),
                    context.getReceiver().getMobileAccount(), isReceiverAlias);

        MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

        String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
        message.setField(104, new IsoValue<>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
        message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));

        return message;
    }


    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                 String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        if (context.getMessage() != null) {
            context.getMessage().setReason(reason);
            context.getMessage().setReasonDesc(reasonDescription);
            context.getMessage().setProcessingStatus(status);
        }
        if (context.getTransaction() != null) {
            context.getTransaction().setReason(reason);
            context.getTransaction().setReasonDesc(reasonDescription);
            context.getTransaction().setProcessingStatus(status);
        }
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setTransaction(context.getTransaction());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        if (reasonCode.equals(ReasonCodes.VALID)) {
            result.setNotifications(NotificationProcessor.createReversalNotificationMessages(context));
        }
        return result;
    }

    private MPayResponse generateResponse(MessageProcessingContext context) {
        logger.debug("Inside GenerateResponse ...");
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response;
    }
}
