package com.progressoft.mpay.plugins.p2p.integrations;

import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.NotSupportedException;

public class IntegrationProcessorFactory {
    private static final Logger logger = LoggerFactory.getLogger(IntegrationProcessorFactory.class);

    private IntegrationProcessorFactory() {

    }

    public static IntegrationProcessor createProcessor(int messageType) throws NotSupportedException {
        logger.debug("Inside CreateProcessor ...");
        logger.debug("messageType: " + messageType);
        if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)
            return new P2PInwardProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE)
            return new P2PResponseProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_RESPONSE)
            return new P2POfflineResponseProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST)
            return new P2PReversalAdviseProcessor();
        else
            throw new NotSupportedException("messageType == " + messageType);
    }
}
