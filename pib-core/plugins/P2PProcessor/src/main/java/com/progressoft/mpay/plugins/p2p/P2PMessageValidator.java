package com.progressoft.mpay.plugins.p2p;

import com.progressoft.mpay.Constants;
import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class P2PMessageValidator {
    private static final Logger logger = LoggerFactory.getLogger(P2PMessageValidator.class);

    private P2PMessageValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ...");
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        P2PMessage request = (P2PMessage) context.getRequest();


        result = validateSender(context, request);
        if (!result.isValid())
            return result;

        result = P2PReceiverValidator.validate(context.getSender().getMobile().getMobileNumber(), request.getReceiverInfo());
        if (!result.isValid())
            return result;

        result = validateReceiverData(context, request);
        if (!result.isValid())
            return result;

        return validateSettings(context, request);
    }

    private static ValidationResult validateReceiverData(MessageProcessingContext context, P2PMessage request) {
        ValidationResult result;
        if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
            result = MobileNumberValidator.validate(context, request.getReceiverInfo());
            if (!result.isValid())
                return result;
        }

        if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getMobile() != null) {
            result = validateReceiver(context, request);
            if (!result.isValid())
                return result;

            result = validatePayment(context);
            if (!result.isValid())
                return result;
        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validatePayment(MessageProcessingContext context) {
        if (context.getReceiver().getMobile() != null && (context.getReceiver().getMobile().getMobileNumber().equals(context.getSender().getMobile().getMobileNumber())))
            new ValidationResult(ReasonCodes.SENDER_IS_SAME_AS_RECEIVER, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static ValidationResult validateSettings(MessageProcessingContext context, P2PMessage request) {
        ValidationResult result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
        if (!result.isValid())
            return result;

        result = AmountValidator.validate(context);
        if (!result.isValid())
            return result;

        result = LimitsValidator.validate(context);
        if (!result.isValid())
            return result;

        return PSPValidator.validate(context, false);
    }

    private static ValidationResult validateSender(MessageProcessingContext context, P2PMessage request) {
        ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
        if (!result.isValid())
            return result;

        result = CustomerValidator.validate(context.getSender().getCustomer(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getMobileAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        if (context.getExtraData().get(Constants.CARD_PIN_KEY) != null)
            return result;

        return PinCodeValidator.validate(context, context.getSender().getMobile(), request.getPin());
    }

    private static ValidationResult validateReceiver(MessageProcessingContext context, P2PMessage request) {
        logger.debug("Inside ValidateReceiver ...");
        ValidationResult result;

        if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
            result = MobileValidator.validate(context.getReceiver().getMobile(), false);
            if (!result.isValid())
                return result;
        } else if (request.getReceiverType().equals(ReceiverInfoType.ALIAS)) {
            result = AliasValidator.validate(context.getReceiver().getMobile(), false);
            if (!result.isValid())
                return result;
        }

        result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getReceiver().getMobileAccount(), false, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        return WalletCapValidator.validate(context);
    }
}
