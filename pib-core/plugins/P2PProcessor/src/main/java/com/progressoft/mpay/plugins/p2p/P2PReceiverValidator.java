package com.progressoft.mpay.plugins.p2p;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class P2PReceiverValidator {

    private static final Logger logger = LoggerFactory.getLogger(P2PReceiverValidator.class);

    private P2PReceiverValidator() {

    }

    public static ValidationResult validate(String senderMobileNumber, String receiverMobileNumber) {
        logger.debug("Inside Validate ....");

        if (senderMobileNumber.equalsIgnoreCase(receiverMobileNumber))
            return new ValidationResult(ReasonCodes.SENDER_IS_SAME_AS_RECEIVER, null, false);
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
