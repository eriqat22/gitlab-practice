package com.progressoft.mpay.plugins.listCities;

import com.progressoft.mpay.messages.MPayRequest;
import org.apache.commons.lang.NullArgumentException;

public class ListCitiesMessage extends MPayRequest {


	public ListCitiesMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}


	public static ListCitiesMessage parseMessage(MPayRequest request) {
		if (request == null)
			return null;
		ListCitiesMessage message = new ListCitiesMessage(request);

		return message;
	}
}
