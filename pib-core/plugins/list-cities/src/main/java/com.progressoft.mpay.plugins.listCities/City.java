package com.progressoft.mpay.plugins.listCities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class City {
    private String code;
    private  String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public City(String code, String name) {
        this.code = code;
        this.name = name;
    }
    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}
