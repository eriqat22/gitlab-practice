package com.progressoft.mpay.plugins.listCities;

import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import org.apache.commons.lang.NullArgumentException;

public class ListSettingsValidator {
	private ListSettingsValidator() {

	}
	public static ValidationResult validate(MessageProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");

		return LanguageValidator.validate(context.getLanguage());
	}
}
