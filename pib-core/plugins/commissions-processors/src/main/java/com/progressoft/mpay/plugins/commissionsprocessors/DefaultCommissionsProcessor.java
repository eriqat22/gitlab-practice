package com.progressoft.mpay.plugins.commissionsprocessors;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.progressoft.jfw.shared.JfwCurrencyInfo;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.ICommissionProcessor;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_ClientsCommission;
import com.progressoft.mpay.entities.MPAY_Commission;
import com.progressoft.mpay.entities.MPAY_CommissionParameter;
import com.progressoft.mpay.entities.MPAY_CommissionScheme;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entity.JournalVoucherFlags;
import com.progressoft.mpay.entity.JvDetailsTypes;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.progressoft.mpay.transactions.TransactionCreator;
public class DefaultCommissionsProcessor implements ICommissionProcessor {
    private static final String INVALID_COMMISSION_PARAMETER = "invalid CommissionParameter value, value should be decimal, ParamName: ";
    private static final Logger logger = LoggerFactory.getLogger(DefaultCommissionsProcessor.class);
    private static final String COMMISSION_PARAMETER_NOT_FOUND_NAME = "CommissionParameter not found, Name: ";
    private static final String TOTAL_COUNT_KEY = "TOTAL_COUNT";
    private static final String TOTAL_AMOUNT_KEY = "TOTAL_AMOUNT";
    private static final String COMMISSION_AMOUNT_KEY = "COMMISSION_AMOUNT";
    private static final String DIRECTION = "DIRECTION";
    private static final String SENDER_PERCENTAGE = "SENDER_PERCENTAGE";
    private static final String RECEIVER_PERCENTAGE = "RECEIVER_PERCENTAGE";
    @Override
    public void process(ProcessingContext context, ProcessingContextSide side, CommissionDirections direction) {
        if (context.getTransaction() == null || context.getAmount() == null || direction == null)
            return;
        if (side.getProfile() != null)
            handleCommission(context, side, direction);
    }
    private void handleCommission(ProcessingContext context, ProcessingContextSide side, CommissionDirections direction) {
        MPAY_CommissionScheme scheme = side.getProfile().getCommissionsScheme();
        if (!scheme.getIsActive())
            return;
        MPAY_Commission commission = getCommission(context.getOperation().getMessageType(), scheme);
        if (commission == null || !commission.getIsActive())
            return;
        MPAY_ClientsCommission clientCommission = getClientCommission(side, context.getAmount(), context.getOperation().getMessageType());
        if (checkCommission(context, side, commission, clientCommission, direction))
            context.getDataProvider().updateClientCommission(clientCommission);
    }
    private boolean checkCommission(ProcessingContext context, ProcessingContextSide side, MPAY_Commission commission, MPAY_ClientsCommission clientCommission, CommissionDirections direction) {
        CommissionDirections commissionDirection = getDirection(commission);
        BigDecimal totalCommissionAmount = getTotalAmount(commission);
        Long totalCommissionCount = getTotalCount(commission);
        if (!commissionDirection.equals(CommissionDirections.BOTH) && !direction.equals(commissionDirection))
            return false;
        if (clientCommission.getTotalAmount().compareTo(totalCommissionAmount) >= 0) {
            addCommissionJv(context, side.getAccount(), commission, direction);
            clientCommission.setTotalAmount(clientCommission.getTotalAmount().subtract(totalCommissionAmount));
        }
        if (clientCommission.getTotalCount().compareTo(getTotalCount(commission)) >= 0) {
            addCommissionJv(context, side.getAccount(), commission, direction);
            clientCommission.setTotalCount(clientCommission.getTotalCount() - totalCommissionCount);
        }
        return true;
    }
    private void addCommissionJv(ProcessingContext context, MPAY_Account account, MPAY_Commission commission, CommissionDirections direction) {
        MPAY_Account commissionsAccount = context.getLookupsLoader().getPSP().getPspCommissionsAccount();
        BigDecimal amount = getCommissionAmount(commission);
        if (direction.equals(CommissionDirections.SENDER))
            amount = getCommissionBearerAmount(amount, getSenderPercentage(commission), context);
        else if (direction.equals(CommissionDirections.RECEIVER))
            amount = getCommissionBearerAmount(amount, getReceiverPercentage(commission), context);
        else
            throw new NotSupportedException("CommissionDirection not supported for percentage calculation, direction: " + direction.getValue());
        TransactionCreator.createJVDetail(context.getTransaction(), JournalVoucherFlags.DEBIT, commissionsAccount, amount, context.getLookupsLoader().getJvTypes(JvDetailsTypes.COMMISSIONS.toString()),
                null);
        TransactionCreator.createJVDetail(context.getTransaction(), JournalVoucherFlags.CREDIT, account, amount, context.getLookupsLoader().getJvTypes(JvDetailsTypes.COMMISSIONS.toString()), null);
        context.getDataProvider().persistTransaction(context.getTransaction());
        AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), context.getTransaction());
    }
    private BigDecimal getCommissionBearerAmount(BigDecimal amount, BigDecimal percentage, ProcessingContext context) {
        JfwCurrencyInfo currency = context.getDataProvider().getCurrencyInfo(context.getTransaction().getCurrency());
        return amount.multiply(percentage).setScale(currency.getFractionDigits(), RoundingMode.HALF_DOWN);
    }
    private MPAY_ClientsCommission getClientCommission(ProcessingContextSide side, BigDecimal amount, MPAY_MessageType type) {
        MPAY_ClientsCommission clientCommission = side.getCommission();
        if (clientCommission == null) {
            clientCommission = new MPAY_ClientsCommission();
            clientCommission.setTotalAmount(BigDecimal.ZERO);
            clientCommission.setTotalCount(0l);
            clientCommission.setAccount(side.getAccount());
            clientCommission.setMessageType(type);
        }
        clientCommission.setTotalAmount(clientCommission.getTotalAmount().add(amount));
        clientCommission.setTotalCount(clientCommission.getTotalCount() + 1);
        return clientCommission;
    }
    private MPAY_Commission getCommission(MPAY_MessageType type, MPAY_CommissionScheme scheme) {
        Optional<MPAY_Commission> commission = scheme.getRefCommissionSchemeCommissions().stream().filter(c -> c.getMsgType().getId() == type.getId()).findFirst();
        if (!commission.isPresent())
            return null;
        return commission.get();
    }
    private Long getTotalCount(MPAY_Commission commission) {
//    Optional<MPAY_CommissionParameter> param = commission.getRefCommissionCommissionParameters().stream().filter(p -> p.getName().equalsIgnoreCase(TOTAL_COUNT_KEY)).findFirst();
//    if (!param.isPresent())
//       throw new InvalidOperationException(COMMISSION_PARAMETER_NOT_FOUND_NAME + TOTAL_COUNT_KEY);
//    try {
//       return Long.parseLong(param.get().getValue());
//    } catch (NumberFormatException ex) {
//       throw new NotSupportedException("invalid CommissionParameter value, value should be number, Name: " + TOTAL_COUNT_KEY);
//    }
        return 0L;
    }
    private BigDecimal getTotalAmount(MPAY_Commission commission) {
//    Optional<MPAY_CommissionParameter> param = commission.getRefCommissionCommissionParameters().stream().filter(p -> p.getName().equalsIgnoreCase(TOTAL_AMOUNT_KEY)).findFirst();
//    if (!param.isPresent())
//       throw new InvalidOperationException(COMMISSION_PARAMETER_NOT_FOUND_NAME + TOTAL_AMOUNT_KEY);
//    try {
//       return new BigDecimal(param.get().getValue());
//    } catch (NumberFormatException ex) {
//       throw new NotSupportedException(INVALID_COMMISSION_PARAMETER + TOTAL_AMOUNT_KEY);
//    }
        return BigDecimal.ZERO;
    }
    private BigDecimal getCommissionAmount(MPAY_Commission commission) {
//    Optional<MPAY_CommissionParameter> param = commission.getRefCommissionCommissionParameters().stream().filter(p -> p.getName().equalsIgnoreCase(COMMISSION_AMOUNT_KEY)).findFirst();
//    if (!param.isPresent())
//       throw new InvalidOperationException(COMMISSION_PARAMETER_NOT_FOUND_NAME + COMMISSION_AMOUNT_KEY);
//    try {
//       return new BigDecimal(param.get().getValue());
//    } catch (NumberFormatException ex) {
//       throw new NotSupportedException(INVALID_COMMISSION_PARAMETER + COMMISSION_AMOUNT_KEY);
//    }
        return BigDecimal.ZERO;
    }
    private CommissionDirections getDirection(MPAY_Commission commission) {
//    Optional<MPAY_CommissionParameter> param = commission.getRefCommissionCommissionParameters().stream().filter(p -> p.getName().equalsIgnoreCase(DIRECTION)).findFirst();
//    if (!param.isPresent())
//       throw new InvalidOperationException(COMMISSION_PARAMETER_NOT_FOUND_NAME + DIRECTION);
//    try {
//       return CommissionDirections.valueOf(param.get().getValue().toUpperCase());
//    } catch (Exception ex) {
//       logger.error("Failed to getCommissionDirection", ex);
//       throw new NotSupportedException("invalid CommissionParameter value, value should be between 'SENDER', 'RECEIVER' or 'BOTH', ParamName: " + DIRECTION);
//    }
        return null;
    }
    private BigDecimal getSenderPercentage(MPAY_Commission commission) {
//    Optional<MPAY_CommissionParameter> param = commission.getRefCommissionCommissionParameters().stream().filter(p -> p.getName().equalsIgnoreCase(SENDER_PERCENTAGE)).findFirst();
//    if (!param.isPresent())
//       throw new InvalidOperationException(COMMISSION_PARAMETER_NOT_FOUND_NAME + SENDER_PERCENTAGE);
//    try {
//       return new BigDecimal(param.get().getValue().toUpperCase()).divide(new BigDecimal(100));
//    } catch (NumberFormatException ex) {
//       throw new NotSupportedException(INVALID_COMMISSION_PARAMETER + SENDER_PERCENTAGE);
//    }
        return BigDecimal.ZERO;
    }
    private BigDecimal getReceiverPercentage(MPAY_Commission commission) {
//    Optional<MPAY_CommissionParameter> param = commission.getRefCommissionCommissionParameters().stream().filter(p -> p.getName().equalsIgnoreCase(RECEIVER_PERCENTAGE)).findFirst();
//    if (!param.isPresent())
//       throw new InvalidOperationException(COMMISSION_PARAMETER_NOT_FOUND_NAME + RECEIVER_PERCENTAGE);
//    try {
//       return new BigDecimal(param.get().getValue().toUpperCase()).divide(new BigDecimal(100));
//    } catch (NumberFormatException ex) {
//       throw new NotSupportedException(INVALID_COMMISSION_PARAMETER + RECEIVER_PERCENTAGE);
//    }
        return BigDecimal.ZERO;
    }
}