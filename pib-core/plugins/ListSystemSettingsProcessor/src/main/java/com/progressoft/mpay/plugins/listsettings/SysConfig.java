package com.progressoft.mpay.plugins.listsettings;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_SysConfig;

public class SysConfig {
	private String section;
	private String configKey;
	private String configValue;
	private String hint;

	public SysConfig() {

	}

	public SysConfig(MPAY_SysConfig entity) {
		if (entity == null)
			throw new NullArgumentException("entity");

		section = entity.getSection();
		configKey = entity.getConfigKey();
		configValue = entity.getConfigValue();
		hint = entity.getHint();
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}
}
