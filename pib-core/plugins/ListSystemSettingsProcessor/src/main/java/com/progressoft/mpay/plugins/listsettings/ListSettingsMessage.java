package com.progressoft.mpay.plugins.listsettings;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class ListSettingsMessage extends MPayRequest {
	private static final String SECTION_KEY = "section";

	private String section;

	public ListSettingsMessage() {
		//
	}

	public ListSettingsMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public static ListSettingsMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		ListSettingsMessage message = new ListSettingsMessage(request);
		message.setSection(message.getValue(SECTION_KEY));
		return message;
	}
}
