package com.progressoft.mpay.processor;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.progressoft.mpay.sms.NotificationResponse;

public class PrepareMpayNotificationResponse implements Processor {

	private static JAXBContext jaxbContext = null;

	@Override
	public void process(Exchange exchange) throws Exception {
		if(jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(NotificationResponse.class);
		}
		Marshaller marshaller = jaxbContext.createMarshaller();
		NotificationResponse response = createNotificationResponse(exchange);
		StringWriter writer = new StringWriter();
		marshaller.marshal(response, writer);
		exchange.getIn().setBody(writer.toString());
	}

	private NotificationResponse createNotificationResponse(Exchange exchange) {
		String result = (String)exchange.getIn().getBody();
		NotificationResponse notificationResponse = new NotificationResponse();
		notificationResponse.setStatus(result);
		if("FAIL".equals(result)) {
			notificationResponse.setErrorDescription((String)exchange.getProperty("errorDescription"));
		}
		return notificationResponse;
	}

}
