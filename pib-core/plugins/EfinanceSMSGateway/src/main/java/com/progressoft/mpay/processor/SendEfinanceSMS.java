package com.progressoft.mpay.processor;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.progressoft.jfw.integration.IntegrationUtils;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.sms.NotificationRequest;
import com.progressoft.mpay.sms.NotificationResponse;

public class SendEfinanceSMS implements Processor {

	@Value("${SMS_URL}")
	private String SMSUrl;

	@Value("${Charset}")
	private String charSet;

	@Value("${BytesCharset}")
	private String bytesCharset;

	@Value("${smsUserName}")
	private String smsUserName;

	@Value("${smsPwd}")
	private String smsPwd;

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateSMSWebServiceNotificationRequest.class);

	private static JAXBContext jaxbContext = null;

	@Override
	public void process(Exchange exchange) throws Exception {

		String tenantId = (String) exchange.getIn().getHeader("tenantId");
		if(tenantId == null) {
			LOGGER.info("Tenant is null while sending sms");
			return ;
		}

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(NotificationRequest.class, NotificationResponse.class);
		}
		boolean isServiceUserEnabled = false;

		try {
			isServiceUserEnabled = IntegrationUtils.enableServiceUser(tenantId);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			NotificationRequest notificationRequest = (NotificationRequest) unmarshaller
					.unmarshal(new StringReader((String) exchange.getIn().getBody()));
			boolean status = sendSMS(notificationRequest.getReciever(), notificationRequest.getContent());
			exchange.getIn().setBody(status?"SUCCESS":"FAIL");
			if(!status) {
				exchange.setProperty("errorDescription", "An error occure while sending sms to gateway");
			}

		} catch (Exception e) {
			exchange.setProperty("errorDescription", e.getMessage());
			exchange.getIn().setBody("FAIL");
		}
		finally{
			if(isServiceUserEnabled)
				IntegrationUtils.disableServiceUser();
		}
	}

	private boolean sendSMS(String receiver, String message) throws MalformedURLException, IOException {

		boolean status = false;
		String url = generateURL(receiver, message);
		LOGGER.debug("URL of SMS getway / " + url);
		HttpURLConnection connection = (HttpURLConnection) new java.net.URL(url).openConnection();
		InputStream is;

		HttpURLConnection conn = (HttpURLConnection) connection;
		if (conn.getResponseCode() >= 400) {
			is = conn.getErrorStream();
		} else {
			is = conn.getInputStream();
			status = true;
		}

		String result = convertStreamToString(is);
		LOGGER.debug("Communication result  / " + result);
		return status;
	}

	private String generateURL(String receiver, String message) throws UnsupportedEncodingException {
		String smsMessage = URLEncoder.encode(new String(message.getBytes(), bytesCharset), charSet);

		if (receiver.subSequence(0, 2).equals("00")) {
			receiver = receiver.substring(2);
			receiver = "+" + receiver;
		}

		String URL = SMSUrl;

		URL = URL.replace("{1}", smsUserName);
		URL = URL.replace("{2}", smsPwd);
		URL = URL.replace("{3}", receiver);
		URL = URL.replace("{4}", smsMessage);
		URL = URL.replace("{5}", getApplicationName());

		return URL;
	}

	private String getApplicationName() {
		return SystemParameters.getInstance().getApplicationNameEn();
	}

	@SuppressWarnings("resource")
	private String convertStreamToString(InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

}
