package com.progressoft.mpay.processor;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.sms.NotificationRequest;
import com.progressoft.mpay.sms.NotificationResponse;

public class CreateSMSWebServiceNotificationRequest implements Processor {

	@Value("${SMS_URL}")
	private String SMSUrl;

	@Value("${Charset}")
	private String charSet;

	@Value("${BytesCharset}")
	private String bytesCharset;

	@Value("${smsUserName}")
	private String smsUserName;

	@Value("${smsPwd}")
	private String smsPwd;


	private static final Logger LOGGER = LoggerFactory.getLogger(CreateSMSWebServiceNotificationRequest.class);

	private static JAXBContext jaxbContext = null;

	@Override
	public void process(Exchange exchange) throws Exception {

		if (jaxbContext == null) {
			jaxbContext = JAXBContext.newInstance(NotificationRequest.class, NotificationResponse.class);
		}

		try {
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			NotificationRequest notificationRequest = (NotificationRequest) unmarshaller.unmarshal(new StringReader((String) exchange.getIn().getBody()));

			LOGGER.debug("Send SMS to  " + notificationRequest.getReciever());
			LOGGER.debug("SMS body  " + notificationRequest.getContent());

			boolean status = sendSPMS(notificationRequest.getReciever(), notificationRequest.getContent());
			exchange.getOut().setBody(setMpayNotificationResponse(exchange, status));
		} catch (Exception e) {
			LOGGER.error("Failed to communicate with SMS gatway ", e);
			throw new Exception("Failed to communicate with SMS gatway ", e);
		}
	}

	private Object setMpayNotificationResponse(Exchange exchange, boolean status) throws JAXBException {
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		NotificationResponse response = new NotificationResponse();
		response.setStatus(status ? "true" : "false");
		marshaller.marshal(response, writer);
		return writer.toString();
	}

	private boolean sendSPMS(String receiver, String message) throws MalformedURLException, IOException {

		boolean status = false;
		String url = generateURL(receiver, message);
		LOGGER.debug("URL of SMS getway / " + url);
		HttpURLConnection connection = (HttpURLConnection) new java.net.URL(url).openConnection();
		InputStream is;

		HttpURLConnection conn = (HttpURLConnection) connection;
		if (conn.getResponseCode() >= 400) {
			is = conn.getErrorStream();
		} else {
			is = conn.getInputStream();
			status = true;
		}

		String result = convertStreamToString(is);
		LOGGER.debug("Communication result  / " + result);
		return status;
	}

	private String generateURL(String receiver, String message) throws UnsupportedEncodingException {
		String smsMessage = URLEncoder.encode(new String(message.getBytes(), bytesCharset), charSet);

		if (receiver.subSequence(0, 2).equals("00")) {
			receiver = receiver.substring(2);
			receiver = "+" + receiver;
		}

		String URL = SMSUrl;

		URL = URL.replace("{1}", smsUserName);
		URL = URL.replace("{2}", smsPwd);
		URL = URL.replace("{3}", receiver);
		URL = URL.replace("{4}", smsMessage);
		URL = URL.replace("{5}", getAppName());

		return URL;

	}

	@SuppressWarnings("resource")
	private String convertStreamToString(InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	private String getAppName() {
		return SystemParameters.getInstance().getApplicationNameEn();
	}
}