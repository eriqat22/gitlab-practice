package com.progressoft.mpay.plugins.feescalculation;

import org.apache.commons.lang.NullArgumentException;

import java.math.BigDecimal;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class FeesCalculationMessage extends MPayRequest {
	protected static final String TARGET_OPERATION_KEY = "targetOperation";
	protected static final String AMOUNT_KEY = "amount";

	private String targetOperation;
	private BigDecimal amount;

	public FeesCalculationMessage() {
		//
	}

	public FeesCalculationMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getTargetOperation() {
		return targetOperation;
	}

	public void setTargetOperation(String targetOperation) {
		this.targetOperation = targetOperation;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public static FeesCalculationMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		FeesCalculationMessage message = new FeesCalculationMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null
				|| !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

		message.setTargetOperation(message.getValue(TARGET_OPERATION_KEY));
		if (message.getTargetOperation() == null)
			throw new MessageParsingException(TARGET_OPERATION_KEY);

		String amount = message.getValue(AMOUNT_KEY);
		if (amount == null)
			throw new MessageParsingException(AMOUNT_KEY);
		try {
			message.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
		} catch (NumberFormatException ex) {
			throw new MessageParsingException(AMOUNT_KEY);
		}
		return message;
	}
}
