package com.progressoft.mpay.plugins.feescalculation;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;

public class FeesCalculationValidator {
	private static final Logger logger = LoggerFactory.getLogger(FeesCalculationValidator.class);

	private FeesCalculationValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			return validateCustomer(context);
		else if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			return validateCorporate(context);
		else
			return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
	}

	private static ValidationResult validateCustomer(MessageProcessingContext context) {
		logger.debug("Inside ValidateCustomer ...");

		ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		result = validateOperation(context);
		if (!result.isValid())
			return result;

		return LimitsValidator.validate(context);
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context) {
		logger.debug("Inside ValidateCorporate ...");
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		result = validateOperation(context);
		if (!result.isValid())
			return result;

		return LimitsValidator.validate(context);
	}

	private static ValidationResult validateOperation(MessageProcessingContext context) {
		MPAY_EndPointOperation operation = (MPAY_EndPointOperation) context.getExtraData().get(Constants.TARGET_OPERATION_KEY);
		if (operation == null)
			return new ValidationResult(ReasonCodes.INVALID_OPERATION, null, false);
		if (!operation.getIsActive())
			return new ValidationResult(ReasonCodes.INACTIVE_OPERATION, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
