package com.progressoft.mpay.plugins.feescalculation;

public class Constants {
	public static final String TARGET_OPERATION_KEY = "targetOperation";
	public static final String FEES_KEY = "fees";
	public static final String TAX_KEY = "tax";
	public static final String PINLESS_ENABLED_KEY = "pinlessEnabled";
	public static final String PINLESS_AMOUNT_KEY = "pinlessAmount";

	private Constants() {

	}
}
