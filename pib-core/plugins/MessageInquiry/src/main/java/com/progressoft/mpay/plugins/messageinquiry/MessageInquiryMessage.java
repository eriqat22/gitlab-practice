package com.progressoft.mpay.plugins.messageinquiry;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class MessageInquiryMessage extends MPayRequest {
	private static final String REFERENCE_KEY = "reference";

	private String reference;

	public MessageInquiryMessage() {
		//
	}

	public MessageInquiryMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public static MessageInquiryMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		MessageInquiryMessage message = new MessageInquiryMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setReference(message.getValue(REFERENCE_KEY));
		if (message.getReference() == null || message.getReference().trim().length() == 0)
			throw new MessageParsingException(REFERENCE_KEY);
		return message;
	}
}
