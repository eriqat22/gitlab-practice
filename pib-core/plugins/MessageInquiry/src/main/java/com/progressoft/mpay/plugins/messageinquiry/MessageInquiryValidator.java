package com.progressoft.mpay.plugins.messageinquiry;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.LanguageValidator;

public class MessageInquiryValidator {
	private MessageInquiryValidator() {

	}
	public static ValidationResult validate(MessageProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		MPAY_MPayMessage message = (MPAY_MPayMessage) context.getExtraData().get(Constants.MessageKey);
		if (message == null)
			return new ValidationResult(ReasonCodes.MESSAGE_NOT_FOUND, null, false);

		if (!message.getSender().equals(context.getRequest().getSender()))
			return new ValidationResult(ReasonCodes.MESSAGE_NOT_FOUND, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
