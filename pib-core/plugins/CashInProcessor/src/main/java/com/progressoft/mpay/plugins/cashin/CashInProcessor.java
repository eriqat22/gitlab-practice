package com.progressoft.mpay.plugins.cashin;

import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.cashin.integrations.CashInResponseProcessor;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.tax.TaxCalculator;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class CashInProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(CashInProcessor.class);

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        try {
            logger.debug("Inside ProcessIntegration ...");
            IntegrationProcessingResult result = CashInResponseProcessor.processIntegration(context);
            handleCommissions(context, result.getProcessingStatus());
            return result;
        } catch (Exception e) {
            logger.error("Error when ProcessIntegration in CashInProcessor", e);
            return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null, false);
        }
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult validationResult = CashInValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in CashInProcessor", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        if (context.getTransaction() != null) {
            TransactionHelper.reverseTransaction(context);
        }
        return MessageProcessingResult.create(context, status, reason, reasonDescription);
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException {
        logger.debug("Inside AcceptMessage ...");
        CashInMessage message = (CashInMessage) context.getRequest();
        MessageProcessingResult result = new MessageProcessingResult();
        MPAY_ProcessingStatus status;
        MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
        String reasonDescription = null;

        context.getMessage().setReason(reason);
        MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO, message.getNotes());
        transaction.setTotalAmount(context.getAmount());
        transaction.setOriginalAmount(transaction.getTotalAmount().subtract(transaction.getReceiverCharge().add(transaction.getReceiverTax())));
        result.setMessage(context.getMessage());
        context.setTransaction(transaction);
        result.setTransaction(transaction);
        context.getDataProvider().persistProcessingResult(result);

        Integer mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
        status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
        IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, message.getReceiverType().equals(ReceiverInfoType.ALIAS));
        MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegType));
        messageLog.setRefMessage(context.getMessage());
        result.setMpClearMessage(messageLog);

        context.getMessage().setProcessingStatus(status);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
        transaction.setReason(context.getMessage().getReason());
        transaction.setReasonDesc(context.getMessage().getReasonDesc());
        transaction.setIsReversed(false);
        if (context.getExtraData().get(CashinConstants.SOURCE) != null)
            transaction.setShopId((String) context.getExtraData().get(CashinConstants.SOURCE));
        result.setStatus(status);
        result.setStatusDescription(context.getMessage().getReasonDesc());
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED) || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED))
            result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
        else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
            result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
        OTPHanlder.removeOTP(context);
        handleCommissions(context, status);
        handleLimits(context, status, transaction);
        return result;
    }

    private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status, MPAY_Transaction transaction) throws SQLException {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED) || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
            context.getDataProvider().updateAccountLimit(context.getReceiver().getAccount().getId(), context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
                    SystemHelper.getCurrentDateWithoutTime());
        }
    }

    private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
        if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            return;
        CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
                CommissionDirections.SENDER);
        if (context.getDirection() == TransactionDirection.ONUS)
            CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
                    CommissionDirections.RECEIVER);
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, SQLException {
        logger.debug("Inside PreProcessMessage ...");
        CashInMessage message = CashInMessage.parseMessage(context.getRequest());
        context.setRequest(message);
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setAmount(message.getAmount());
        sender.setInfo(message.getSender());
        context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return;
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), message.getSenderAccount()));
        if (sender.getServiceAccount() == null)
            return;
        OTPHanlder.loadOTP(context, message.getPin());
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        sender.setBank(sender.getServiceAccount().getBank());
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        sender.setBanked(sender.getAccount().getIsBanked());
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
        receiver.setInfo(message.getReceiver());
        receiver.setMobile(context.getDataProvider().getCustomerMobile(message.getReceiver(), message.getReceiverType()));
        if (receiver.getMobile() == null)
            return;
        receiver.setCustomer(receiver.getMobile().getRefCustomer());
        receiver.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), receiver.getMobile(), message.getReceiverAccount()));
        if (receiver.getMobileAccount() == null)
            return;
        receiver.setAccount(receiver.getMobileAccount().getRefAccount());
        receiver.setBank(receiver.getMobileAccount().getBank());
        receiver.setProfile(receiver.getMobileAccount().getRefProfile());
        receiver.setBanked(receiver.getAccount().getIsBanked());
        receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
        receiver.setLimits(context.getDataProvider().getAccountLimits(receiver.getAccount().getId(), context.getOperation().getMessageType().getId()));
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        context.setDirection(TransactionDirection.ONUS);
        context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(), sender.isBanked(), personClientType, receiver.isBanked(),
                context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
        if (context.getTransactionConfig() != null)
            context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
        if (message.getChargesAmount() == null)
            ChargesCalculator.calculate(context);
        else
            receiver.setCharge(message.getChargesAmount());
        if (message.getTaxesAmount() == null)
            TaxCalculator.claculate(context);
        else receiver.setTax(message.getTaxesAmount());
        context.getExtraData().put(CashinConstants.SOURCE, message.getSource());
        handlePushNotification(context,message);
    }

    private void handlePushNotification(MessageProcessingContext context, CashInMessage message) {
        final String receiverDeviceToken = context.getReceiver().getMobile().getRefCustomerMobileCustomerDevices()
                .stream()
                .filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CustomerDevice::getExtraData).orElse(null);
        context.setReceiverNotificationToken(receiverDeviceToken);
    }

    private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isReceiverAlias) {
        logger.debug("Inside CreateMPClearRequest ...");
        try {
            String messageID = context.getDataProvider().getNextMPClearMessageId();
            BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
            IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(type);
            message.setBinary(false);

            String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
            String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

            message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
            MPClearHelper.setMessageEncoded(message);
            message.setField(7, new IsoValue<String>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
            message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
            message.setField(49, new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

            String account1 = MPClearHelper.getPSPAccount(context.getSystemParameters(), context.getSender().getService(), context.getSender().getServiceAccount());
            MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

            String account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getMobile(), context.getReceiver().getMobileAccount(), isReceiverAlias);
            MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

            String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
            message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
            message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

            return message;
        } catch (Exception e) {
            logger.error("Error when CreateMPClearRequest in CashInProcessor", e);
            return null;
        }
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside Reverse ...");
        try {
            JVPostingResult result = TransactionHelper.reverseTransaction(context);
            CashInMessage message = CashInMessage.parseMessage(context.getRequest());
            ProcessingContextSide receiver = new ProcessingContextSide();
            receiver.setMobile(context.getDataProvider().getCustomerMobile(message.getReceiver(), message.getReceiverType()));
            receiver.setCustomer(receiver.getMobile().getRefCustomer());
            receiver.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), receiver.getMobile(), message.getReceiverAccount()));
            context.setReceiver(receiver);

            context.getDataProvider().updateAccountLimit(
                    SystemHelper.getMobileAccount(context.getSystemParameters(), context.getReceiver().getMobile(), message.getReceiverAccount()).getRefAccount().getId(),
                    context.getMessage().getRefOperation().getMessageType().getId(), context.getTransaction().getTotalAmount().negate(), -1, SystemHelper.getCurrentDateWithoutTime());

            context.getDataProvider().mergeTransaction(context.getTransaction());
            if (result.isSuccess())
                return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

            return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
        } catch (Exception ex) {
            logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
            return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), ProcessingStatusCodes.FAILED);
        }
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        if (context.getMessage() != null) {
            context.getMessage().setReason(reason);
            context.getMessage().setReasonDesc(reasonDescription);
            context.getMessage().setProcessingStatus(status);
        }
        if (context.getTransaction() != null) {
            context.getTransaction().setReason(reason);
            context.getTransaction().setReasonDesc(reasonDescription);
            context.getTransaction().setProcessingStatus(status);
        }
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setTransaction(context.getTransaction());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        if (reasonCode.equals(ReasonCodes.VALID)) {
            result.setNotifications(NotificationProcessor.createReversalNotificationMessages(context));
        }
        return result;
    }
}
