package com.progressoft.mpay.plugins.cashin;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    private NotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateAcceptanceNotificationMessages ...");
            String reference;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            List<MPAY_Notification> notifications = new ArrayList<>();
            MPAY_CustomerMobile receiverMobile = context.getReceiver().getMobile();
            if (receiverMobile != null) {
                final MPAY_Language prefLang = receiverMobile.getRefCustomer().getPrefLang();
                CashInNotificationContext notificationContext = new CashInNotificationContext();
                notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
                notificationContext.setReference(reference);
                context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getAccount().getBalance()));
                notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
                notificationContext.setReceiverTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getTax()));
                notificationContext.setExtraData1(receiverMobile.getMobileNumber());

                addNotification(context.getMessage(), notifications, receiverMobile.getEmail(), prefLang,
                        receiverMobile.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, receiverMobile.getMobileNumber(), prefLang,
                        receiverMobile.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                        receiverMobile.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_RECEIVER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error in CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            CashInMessage request = CashInMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            String receiverMobile = request.getReceiver();
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();
            MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(receiverMobile);
            if (mobile != null) {
                language = mobile.getRefCustomer().getPrefLang();
            }
            CashInNotificationContext notificationContext = new CashInNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());

            if (mobile != null) {
                notificationContext.setExtraData1(mobile.getMobileNumber());
                final MPAY_Language prefLang = mobile.getRefCustomer().getPrefLang();
                addNotification(context.getMessage(), notifications, mobile.getEmail(), prefLang,
                        mobile.getEnableEmail(), notificationContext, NotificationType.REJECTED_RECEIVER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, mobile.getMobileNumber(), prefLang,
                        mobile.getEnableSMS(), notificationContext, NotificationType.REJECTED_RECEIVER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                        mobile.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_RECEIVER, NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error in CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(MessageProcessingContext context) {
        logger.debug("Inside CreateReversalNotificationMessages ...");
        try {
            String reference;
            List<MPAY_Notification> notifications = new ArrayList<>();

            reference = context.getTransaction().getReference();
            MPAY_CustomerMobile receiverMobile = context.getTransaction().getReceiverMobile();

            String currency = context.getTransaction().getCurrency().getStringISOCode();
            if (receiverMobile != null) {

                CashInNotificationContext receiverNotificationContext = new CashInNotificationContext();
                receiverNotificationContext.setCurrency(currency);
                receiverNotificationContext.setReference(reference);
                MPAY_Account receiverAccount = context.getTransaction().getReceiverMobileAccount().getRefAccount();
                context.getDataProvider().refreshEntity(receiverAccount);
                receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
                receiverNotificationContext.setExtraData1(receiverMobile.getMobileNumber());

                final MPAY_Language prefLang = receiverMobile.getRefCustomer().getPrefLang();
                addNotification(context.getMessage(), notifications, receiverMobile.getEmail(), prefLang,
                        receiverMobile.getEnableEmail(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, receiverMobile.getMobileNumber(), prefLang,
                        receiverMobile.getEnableSMS(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                        receiverMobile.getEnablePushNotification(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.PUSH_NOTIFICATION);

            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateReversalNotificationMessages", e);
            return new ArrayList<>();
        }
    }
}
