package com.progressoft.mpay.plugins.b2p;

public class Constants {
	public static final String NOTIFICATION_TOKEN_KEY = "NotificationToken";

	private Constants() {

	}
}
