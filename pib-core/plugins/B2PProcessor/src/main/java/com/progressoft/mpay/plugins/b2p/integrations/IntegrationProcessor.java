package com.progressoft.mpay.plugins.b2p.integrations;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

@FunctionalInterface
public interface IntegrationProcessor {
	IntegrationProcessingResult processIntegration(IntegrationProcessingContext context);
}
