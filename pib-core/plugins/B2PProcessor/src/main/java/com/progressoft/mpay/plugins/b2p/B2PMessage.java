package com.progressoft.mpay.plugins.b2p;

import java.math.BigDecimal;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class B2PMessage extends MPayRequest {
	private static final String RECEIVER_INFO_KEY = "rcvId";
	private static final String RECEIVER_TYPE_KEY = "rcvType";
	private static final String NOTES_KEY = "data";
	private static final String AMOUNT_KEY = "amnt";
	private static final String SENDER_ACCOUNT_KEY = "senderAccount";
	private static final String RECEIVER_ACCOUNT_KEY = "receiverAccount";

	private String receiverInfo;
	private String receiverType;
	private String notes;
	private BigDecimal amount;
	private String senderAccount;
	private String receiverAccount;

	public B2PMessage() {
		//
	}

	public B2PMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getReceiverInfo() {
		return receiverInfo;
	}

	public void setReceiverInfo(String receiverInfo) {
		this.receiverInfo = receiverInfo;
	}

	public String getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(String receiverType) {
		this.receiverType = receiverType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public String getReceiverAccount() {
		return receiverAccount;
	}

	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	public static B2PMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		B2PMessage message = new B2PMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		String amount = message.getValue(AMOUNT_KEY);
		if (amount == null)
			throw new MessageParsingException(AMOUNT_KEY);
		try {
			message.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
		} catch (NumberFormatException ex) {
			throw new MessageParsingException(AMOUNT_KEY);
		}
		message.setReceiverInfo(message.getValue(RECEIVER_INFO_KEY));
		if (message.getReceiverInfo() == null)
			throw new MessageParsingException(RECEIVER_INFO_KEY);
		message.setReceiverType(message.getValue(RECEIVER_TYPE_KEY));
		if (message.getReceiverType() == null)
			throw new MessageParsingException(RECEIVER_TYPE_KEY);
		message.setSenderAccount(message.getValue(SENDER_ACCOUNT_KEY));
		message.setReceiverAccount(message.getValue(RECEIVER_ACCOUNT_KEY));
		message.setNotes(message.getValue(NOTES_KEY));
		return message;
	}
}
