package com.progressoft.mpay.plugins.b2p.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class B2PReversalAdviseProcessor implements IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(B2PReversalAdviseProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

}
