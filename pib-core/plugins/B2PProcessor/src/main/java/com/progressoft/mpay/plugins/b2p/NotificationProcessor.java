package com.progressoft.mpay.plugins.b2p;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    private NotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateAcceptanceNotificationMessages ...");
        try {
            String receiver;
            String sender;
            String reference;
            long senderLanguageId;
            long receiverLanguageId;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            MPAY_CorpoarteService senderService = context.getSender().getService();
            MPAY_CustomerMobile receiverMobile = context.getReceiver().getMobile();
            MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

            B2PNotificationContext notificationContext = new B2PNotificationContext();
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setReference(reference);

            if (senderService != null) {
                sender = senderService.getName();
                context.getDataProvider().refreshEntity(context.getSender().getAccount());
                notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getAccount().getBalance()));
                notificationContext.setSenderBanked(context.getSender().getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
                senderLanguageId = context.getSender().getCorporate().getPrefLang().getId();
            } else {
                sender = context.getSender().getInfo();
                senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
            }
            if (receiverMobile != null) {
                receiver = receiverMobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) && receiverMobile.getAlias() != null ? receiverMobile.getAlias() : receiverMobile
                        .getMobileNumber();
                context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getAccount().getBalance()));
                notificationContext.setReceiverBanked(context.getReceiver().getMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
                receiverLanguageId = context.getReceiver().getCustomer().getPrefLang().getId();
            } else {
                receiver = context.getReceiver().getInfo();
                receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
            }
            if (context.getReceiver().getNotes() != null) {
                notificationContext.setNotes(context.getReceiver().getNotes());
                notificationContext.setHasNotes(true);
            }
            notificationContext.setReceiver(receiver);
            notificationContext.setSender(sender);
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (receiverMobile != null) {
                notificationContext.setExtraData1(receiverMobile.getMobileNumber());
                String deviceToken = context.getReceiverNotificationToken();
                final MPAY_Language prefLang = receiverMobile.getRefCustomer().getPrefLang();
                addNotification(context.getMessage(), notifications, receiverMobile.getEmail(), prefLang,
                        receiverMobile.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, receiverMobile.getMobileNumber(), prefLang,
                        receiverMobile.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, deviceToken, prefLang,
                        receiverMobile.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            if (senderService != null) {
                notificationContext.setExtraData1(senderService.getName());
                String deviceToken = context.getReceiverNotificationToken();
                final MPAY_Language prefLang = senderService.getRefCorporate().getPrefLang();
                addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
                        senderService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
                        senderService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, deviceToken, prefLang,
                        senderService.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION);

            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            B2PMessage request = B2PMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();
            MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
            if (service != null) {
                language = service.getRefCorporate().getPrefLang();
            }
            B2PNotificationContext notificationContext = new B2PNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            if (context.getTransaction() == null)
                notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
            else
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
            notificationContext.setReceiver(request.getReceiverInfo());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(Objects.requireNonNull(MPayHelper.getReasonNLS(context.getMessage().getReason(), language)).getDescription());

            if (service != null) {
                final MPAY_CorpoarteService senderCorporateService = context.getSender().getService();
                final String deviceToken = context.getSenderNotificationToken();
                notificationContext.setExtraData1(senderCorporateService.getName());
                addNotification(context.getMessage(), notifications, service.getEmail(), language,
                        service.getEnableEmail(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, service.getMobileNumber(), language,
                        service.getEnableSMS(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, deviceToken, language,
                        service.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION);
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateReversalNotificationMessages ...");
        try {
            String reference;
            List<MPAY_Notification> notifications = new ArrayList<>();

            reference = context.getTransaction().getReference();
            MPAY_CustomerMobile receiverMobile = context.getTransaction().getReceiverMobile();
            MPAY_CustomerMobile senderMobile = context.getTransaction().getSenderMobile();
            String currency = context.getTransaction().getCurrency().getStringISOCode();
            if (receiverMobile != null) {

                B2PNotificationContext receiverNotificationContext = new B2PNotificationContext();
                receiverNotificationContext.setCurrency(currency);
                receiverNotificationContext.setReference(reference);
                MPAY_Account receiverAccount = context.getTransaction().getReceiverMobileAccount().getRefAccount();
                context.getDataProvider().refreshEntity(receiverAccount);
                receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
                receiverNotificationContext.setReceiverBanked(context.getTransaction().getReceiverMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));

                final MPAY_Language prefLang = receiverMobile.getRefCustomer().getPrefLang();
                final String deviceToken = context.getReceiverNotificationToken();
                receiverNotificationContext.setExtraData1(receiverMobile.getMobileNumber());
                addNotification(context.getMessage(), notifications, receiverMobile.getMobileNumber(), prefLang,
                        receiverMobile.getEnableSMS(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, receiverMobile.getEmail(), prefLang,
                        receiverMobile.getEnableEmail(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, deviceToken, prefLang,
                        receiverMobile.getEnablePushNotification(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.PUSH_NOTIFICATION);

            }
            if (senderMobile != null) {

                B2PNotificationContext senderNotificationContext = new B2PNotificationContext();
                senderNotificationContext.setCurrency(currency);
                senderNotificationContext.setReference(reference);
                MPAY_Account senderAccount = context.getTransaction().getSenderMobileAccount().getRefAccount();
                context.getDataProvider().refreshEntity(senderAccount);
                senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
                senderNotificationContext.setReceiverBanked(context.getTransaction().getSenderMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                senderNotificationContext.setExtraData1(senderMobile.getMobileNumber());

                final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
                final String deviceToken = context.getSenderNotificationToken();
                addNotification(context.getMessage(), notifications, senderMobile.getEmail(), prefLang,
                        senderMobile.getEnableEmail(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
                        senderMobile.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, deviceToken, prefLang,
                        senderMobile.getEnablePushNotification(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.PUSH_NOTIFICATION);

            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateReversalNotificationMessages", e);
            return new ArrayList<>();
        }
    }
}
