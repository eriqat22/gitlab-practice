package com.progressoft.mpay.plugins.b2p;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.AmountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileNumberValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PSPValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;
import com.progressoft.mpay.plugins.validators.WalletCapValidator;

public class B2PValidator {
	private static final Logger logger = LoggerFactory.getLogger(B2PValidator.class);

	private B2PValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		B2PMessage request = (B2PMessage) context.getRequest();

		result = validateSender(context, request);
		if (!result.isValid())
			return result;

		if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
			result = MobileNumberValidator.validate(context, request.getReceiverInfo());
			if (!result.isValid())
				return result;
		}

		if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getMobile() != null) {
			result = validateReceiver(context);
			if (!result.isValid())
				return result;
		}

		return validateSettings(context, request);
	}

	private static ValidationResult validateSettings(MessageProcessingContext context, B2PMessage request) {
		ValidationResult result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
		if (!result.isValid())
			return result;

		result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context);
		if (!result.isValid())
			return result;

		return PSPValidator.validate(context, false);
	}

	private static ValidationResult validateSender(MessageProcessingContext context, B2PMessage request) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		return PinCodeValidator.validate(context, context.getSender().getService(), request.getPin());
	}

	private static ValidationResult validateReceiver(MessageProcessingContext context) {
		logger.debug("Inside ValidateReceiver ...");

		ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getMobileAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return WalletCapValidator.validate(context);
	}
}
