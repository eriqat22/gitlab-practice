package com.progressoft.mpay.plugins.epay.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class BillAdviseReversalAdviseProcessor implements IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(BillAdviseReversalAdviseProcessor.class);
	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

}
