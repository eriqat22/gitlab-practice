
package com.progressoft.mpay.plugins.epay.common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the billpaymentservice.types package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private static final String URN_BILL_PAYMENT_SERVICE_TYPES = "urn:BillPaymentService/types";
	private static final QName _ConfirmPaymentsResponse_QNAME = new QName(URN_BILL_PAYMENT_SERVICE_TYPES, "confirmPaymentsResponse");
    private static final QName _CalculateCommissionResponse_QNAME = new QName(URN_BILL_PAYMENT_SERVICE_TYPES, "calculateCommissionResponse");
    private static final QName _EnquireBills_QNAME = new QName(URN_BILL_PAYMENT_SERVICE_TYPES, "enquireBills");
    private static final QName _CalculateCommission_QNAME = new QName(URN_BILL_PAYMENT_SERVICE_TYPES, "calculateCommission");
    private static final QName _ConfirmPayments_QNAME = new QName(URN_BILL_PAYMENT_SERVICE_TYPES, "confirmPayments");
    private static final QName _EnquireBillsResponse_QNAME = new QName(URN_BILL_PAYMENT_SERVICE_TYPES, "enquireBillsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: billpaymentservice.types
     *
     */
    public ObjectFactory() {
    	//
    }

    /**
     * Create an instance of {@link ConfirmPaymentsResponse }
     *
     */
    public ConfirmPaymentsResponse createConfirmPaymentsResponse() {
        return new ConfirmPaymentsResponse();
    }

    /**
     * Create an instance of {@link EnquireBillsResponse }
     *
     */
    public EnquireBillsResponse createEnquireBillsResponse() {
        return new EnquireBillsResponse();
    }

    /**
     * Create an instance of {@link ConfirmPayments }
     *
     */
    public ConfirmPayments createConfirmPayments() {
        return new ConfirmPayments();
    }

    /**
     * Create an instance of {@link CalculateCommission }
     *
     */
    public CalculateCommission createCalculateCommission() {
        return new CalculateCommission();
    }

    /**
     * Create an instance of {@link EnquireBills }
     *
     */
    public EnquireBills createEnquireBills() {
        return new EnquireBills();
    }

    /**
     * Create an instance of {@link CalculateCommissionResponse }
     *
     */
    public CalculateCommissionResponse createCalculateCommissionResponse() {
        return new CalculateCommissionResponse();
    }

    /**
     * Create an instance of {@link Response }
     *
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link Request }
     *
     */
    public Request createRequest() {
        return new Request();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmPaymentsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = URN_BILL_PAYMENT_SERVICE_TYPES, name = "confirmPaymentsResponse")
    public JAXBElement<ConfirmPaymentsResponse> createConfirmPaymentsResponse(ConfirmPaymentsResponse value) {
        return new JAXBElement<>(_ConfirmPaymentsResponse_QNAME, ConfirmPaymentsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateCommissionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = URN_BILL_PAYMENT_SERVICE_TYPES, name = "calculateCommissionResponse")
    public JAXBElement<CalculateCommissionResponse> createCalculateCommissionResponse(CalculateCommissionResponse value) {
        return new JAXBElement<>(_CalculateCommissionResponse_QNAME, CalculateCommissionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnquireBills }{@code >}}
     *
     */
    @XmlElementDecl(namespace = URN_BILL_PAYMENT_SERVICE_TYPES, name = "enquireBills")
    public JAXBElement<EnquireBills> createEnquireBills(EnquireBills value) {
        return new JAXBElement<>(_EnquireBills_QNAME, EnquireBills.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateCommission }{@code >}}
     *
     */
    @XmlElementDecl(namespace = URN_BILL_PAYMENT_SERVICE_TYPES, name = "calculateCommission")
    public JAXBElement<CalculateCommission> createCalculateCommission(CalculateCommission value) {
        return new JAXBElement<>(_CalculateCommission_QNAME, CalculateCommission.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmPayments }{@code >}}
     *
     */
    @XmlElementDecl(namespace = URN_BILL_PAYMENT_SERVICE_TYPES, name = "confirmPayments")
    public JAXBElement<ConfirmPayments> createConfirmPayments(ConfirmPayments value) {
        return new JAXBElement<>(_ConfirmPayments_QNAME, ConfirmPayments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnquireBillsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = URN_BILL_PAYMENT_SERVICE_TYPES, name = "enquireBillsResponse")
    public JAXBElement<EnquireBillsResponse> createEnquireBillsResponse(EnquireBillsResponse value) {
        return new JAXBElement<>(_EnquireBillsResponse_QNAME, EnquireBillsResponse.class, null, value);
    }

}
