package com.progressoft.mpay.plugins.epay.billadvise;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateAcceptanceNotificationMessages ...");
			String receiver;
			String reference;
			long senderLanguageId = 0;
			long receiverLanguageId = 0;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();
			MPAY_CustomerMobile senderMobile = context.getSender().getMobile();
			MPAY_CorpoarteService receiverService = context.getReceiver().getService();
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

			BillAdviseNotificationContext notificationContext = new BillAdviseNotificationContext();
			notificationContext
					.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);
			notificationContext.setSender(context.getSender().getInfo());
			notificationContext.setReceiver(context.getReceiver().getInfo());
			if (senderMobile != null) {
				context.getDataProvider().refreshEntity(context.getSender().getAccount());
				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getSender().getAccount().getBalance()));
				notificationContext.setSenderBanked(senderMobile.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setSenderCharges(
						SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge().add(context.getTransaction().getExternalFees())));
				if (context.getSender().getCustomer() != null)
					senderLanguageId = context.getSender().getCustomer().getPrefLang().getId();
				else
					senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
				notificationContext.setSender(senderMobile.getMobileNumber());
			}
			if (receiverService != null) {
				receiver = receiverService.getDescription();
				if (receiver == null)
					receiver = receiverService.getName();
				if(context.getReceiver() != null)
					receiverLanguageId = context.getReceiver().getCorporate().getPrefLang().getId();
				else
					receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			} else
				receiver = context.getReceiver().getInfo();
			notificationContext.setReceiver(receiver);
			return generateNotifications(context, senderLanguageId, receiverLanguageId, senderMobile, receiverService, operation, notificationContext);
		} catch (Exception e) {
			logger.error("Error in CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static List<MPAY_Notification> generateNotifications(ProcessingContext context, long senderLanguageId, long receiverLanguageId, MPAY_CustomerMobile senderMobile, MPAY_CorpoarteService receiverService, MPAY_EndPointOperation operation, BillAdviseNotificationContext notificationContext) {
		List<MPAY_Notification> notifications = new ArrayList<>();
		if (senderMobile != null) {
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(),
					notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
					senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		if (receiverService != null) {
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(),
					notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId,
					receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
		}
		return notifications;
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			BillAdviseMessage request = BillAdviseMessage
					.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String senderMobile = request.getSender();
			long languageId = request.getLang();
			MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
			if (mobile != null) {
				senderMobile = mobile.getMobileNumber();
				languageId = mobile.getRefCustomer().getPrefLang().getId();
			}
			BillAdviseNotificationContext notificationContext = new BillAdviseNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setAmount(String.valueOf(request.getAmnt()));
			notificationContext.setReceiver(request.getRcvSvc());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(
					context.getMessage().getReason().getReasonsNLS().get(languageId).getDescription());
			notifications
					.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext,
							NotificationType.REJECTED_SENDER, context.getMessage().getRefOperation().getId(), languageId,
							senderMobile, NotificationChannelsCode.SMS));
			return notifications;
		} catch (Exception e) {
			logger.error("Error in CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}
}
