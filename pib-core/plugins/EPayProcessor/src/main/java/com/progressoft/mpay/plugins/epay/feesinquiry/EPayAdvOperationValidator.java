package com.progressoft.mpay.plugins.epay.feesinquiry;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class EPayAdvOperationValidator {
	private static final Logger logger = LoggerFactory.getLogger(EPayAdvOperationValidator.class);

	private EPayAdvOperationValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		MPAY_EndPointOperation operation = (MPAY_EndPointOperation) context.getExtraData().get(FeesInquiryConstants.EPAY_ADVISE_OPERATRION_KEY);
		if (operation == null)
			return new ValidationResult(ReasonCodes.INVALID_OPERATION, null, false);
		if (!operation.getIsActive())
			return new ValidationResult(ReasonCodes.INACTIVE_OPERATION, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
