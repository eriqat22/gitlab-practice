package com.progressoft.mpay.plugins.epay.wsdl;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class was generated by Apache CXF 2.7.3 2015-06-27T18:26:37.283+03:00
 * Generated source version: 2.7.3
 *
 */
@WebServiceClient(name = "BillPaymentService", wsdlLocation = "BillPaymentService.wsdl", targetNamespace = "urn:BillPaymentService/wsdl")
public class BillPaymentService extends Service {

	private static final Logger LOG = LoggerFactory.getLogger(BillPaymentService.class);
	private static URL wsdlLocation;

	public static final QName SERVICE = new QName("urn:BillPaymentService/wsdl", "BillPaymentService");
	public static final QName BillPaymentServiceSEIPort = new QName("urn:BillPaymentService/wsdl",
			"BillPaymentServiceSEIPort");
	static {

		wsdlLocation = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try {
			URL url = classLoader.getResource("/config/app/wsdl/BillPaymentService.wsdl");
			if (url == null) {
				java.util.logging.Logger.getLogger(BillPaymentService.class.getName()).log(
						java.util.logging.Level.INFO, "Can not initialize the default wsdl from {0}", "epay.wsdl");
			}
			wsdlLocation = url;
		} catch (Exception e) {
			LOG.error("error... change me later", e);
		}
	}

	public BillPaymentService(URL wsdlLocation) {
		super(wsdlLocation, SERVICE);
	}

	public BillPaymentService(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public BillPaymentService() {
		super(wsdlLocation, SERVICE);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public BillPaymentService(WebServiceFeature... features) {
		super(wsdlLocation, SERVICE, features);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public BillPaymentService(URL wsdlLocation, WebServiceFeature... features) {
		super(wsdlLocation, SERVICE, features);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public BillPaymentService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	/**
	 *
	 * @return returns BillPaymentServiceSEI
	 */
	@WebEndpoint(name = "BillPaymentServiceSEIPort")
	public BillPaymentServiceSEI getBillPaymentServiceSEIPort() {
		return super.getPort(BillPaymentServiceSEIPort, BillPaymentServiceSEI.class);
	}

	/**
	 *
	 * @param features
	 *            A list of {@link WebServiceFeature} to configure
	 *            on the proxy. Supported features not in the
	 *            <code>features</code> parameter will have their default
	 *            values.
	 * @return returns BillPaymentServiceSEI
	 */
	@WebEndpoint(name = "BillPaymentServiceSEIPort")
	public BillPaymentServiceSEI getBillPaymentServiceSEIPort(WebServiceFeature... features) {
		return super.getPort(BillPaymentServiceSEIPort, BillPaymentServiceSEI.class, features);
	}

	public static URL getWsdlLocation() {
		return wsdlLocation;
	}
}
