package com.progressoft.mpay.plugins.epay.feesinquiry;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class FeesInquiryMessage extends MPayRequest {
	private static final Logger logger = LoggerFactory.getLogger(FeesInquiryMessage.class);

	private static final String BILLER_ID_KEY = "billerId";
	private static final String BILL_INQ_REQ_UID_KEY = "billInqRqUId";
	private static final String EPAY_BILL_REC_ID_KEY = "epayBillRecID";
	private static final String BILL_INFO_KEY = "billInfo";
	private static final String TYPE_KEY = "type";
	private static final String SENDER_ACCOUNT_KEY = "senderAccount";
	private static final String RECEIVER_ACCOUNT_KEY = "receiverAccount";

	private String billerId;
	private String billInqRqUId;
	private String epayBillRecID;
	private String senderAccount;
	private String receiverAccount;
	private String type = "type";
	private List<BillFeeInfo> billInfo;

	public FeesInquiryMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public FeesInquiryMessage(){
		billInfo = new ArrayList<>();
	}

	public String getBillInqRqUId() {
		return billInqRqUId;
	}

	public void setBillInqRqUId(String rqUId) {
		this.billInqRqUId = rqUId;
	}

	public String getEpayBillRecID() {
		return epayBillRecID;
	}

	public void setEpayBillRecID(String epayBillRecID) {
		this.epayBillRecID = epayBillRecID;
	}

	public List<BillFeeInfo> getBillInfo() {
		return billInfo;
	}

	public void setBillInfo(List<BillFeeInfo> billInfo) {
		this.billInfo = billInfo;
	}

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public String getReceiverAccount() {
		return receiverAccount;
	}

	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	public void setBillInfoFromJson(String json) throws MessageParsingException {
		try {
			Type listType = new TypeToken<List<BillFeeInfo>>() {
			}.getType();
			Gson gson = new GsonBuilder().create();
			this.billInfo = gson.fromJson(json, listType);
			if (this.billInfo == null || this.billInfo.isEmpty())
				throw new MessageParsingException(BILL_INFO_KEY);
		} catch (Exception ex) {
			logger.debug("Invalid json received", ex);
			throw new MessageParsingException(BILL_INFO_KEY);
		}
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static FeesInquiryMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		FeesInquiryMessage message = new FeesInquiryMessage(request);
		if (StringUtils.isEmpty(message.getSender()))
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

		message.setBillerId(message.getValue(BILLER_ID_KEY));
		if (StringUtils.isEmpty(message.getBillerId()))
			throw new MessageParsingException(BILLER_ID_KEY);
		message.setBillInqRqUId(message.getValue(BILL_INQ_REQ_UID_KEY));
		if (StringUtils.isEmpty(message.getBillInqRqUId()))
			throw new MessageParsingException(BILL_INQ_REQ_UID_KEY);
		message.setEpayBillRecID(message.getValue(EPAY_BILL_REC_ID_KEY));
		if (StringUtils.isEmpty(message.getEpayBillRecID()))
			throw new MessageParsingException(EPAY_BILL_REC_ID_KEY);
		message.setBillInfoFromJson(message.getValue(BILL_INFO_KEY));
		message.setSenderAccount(message.getValue(SENDER_ACCOUNT_KEY));
		message.setReceiverAccount(message.getValue(RECEIVER_ACCOUNT_KEY));
		message.setType(message.getValue(TYPE_KEY));
		return message;
	}
}
