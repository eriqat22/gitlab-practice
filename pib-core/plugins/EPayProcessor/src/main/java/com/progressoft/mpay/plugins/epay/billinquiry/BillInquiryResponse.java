package com.progressoft.mpay.plugins.epay.billinquiry;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BillInquiryResponse {
	private String billInqRqUId;
	private String epayBillRecID;
	private String pmtRefInfo;
	private String billNumber;
	private String billCategory;
	private String serviceType;
	private String billingAcct;
	private String billerId;
	private String customerName;
	private String address;
	private Date dueDate;

	List<BillInfo> billInfo;

	public BillInquiryResponse(){
		this.billInfo = new ArrayList<>();
	}

	public List<BillInfo> getBillInfo() {
		return billInfo;
	}

	public void setBillInfo(List<BillInfo> billInfo) {
		this.billInfo = billInfo;
	}

	public String getEpayBillRecID() {
		return epayBillRecID;
	}

	public void setEpayBillRecID(String epayBillRecID) {
		this.epayBillRecID = epayBillRecID;
	}

	public String getBillInqRqUId() {
		return billInqRqUId;
	}

	public void setBillInqRqUId(String billInqRqUId) {
		this.billInqRqUId = billInqRqUId;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getPmtRefInfo() {
		return pmtRefInfo;
	}

	public void setPmtRefInfo(String pmtRefInfo) {
		this.pmtRefInfo = pmtRefInfo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getBillCategory() {
		return billCategory;
	}

	public void setBillCategory(String billCategory) {
		this.billCategory = billCategory;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getBillingAcct() {
		return billingAcct;
	}

	public void setBillingAcct(String billingAcct) {
		this.billingAcct = billingAcct;
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}
}
