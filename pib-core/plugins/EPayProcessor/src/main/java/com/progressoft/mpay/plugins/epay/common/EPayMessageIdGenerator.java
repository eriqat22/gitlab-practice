package com.progressoft.mpay.plugins.epay.common;

import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;
import org.hibernate.internal.SessionImpl;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

@Component
public class EPayMessageIdGenerator {

    private EntityManager em;

    protected EPayMessageIdGenerator() {
        //
    }

    public static EPayMessageIdGenerator getInstance() {
        return (EPayMessageIdGenerator) AppContext.getApplicationContext().getBean("EPayMessageIdGenerator");
    }

    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    private long getNextMessageIdSequence() throws SQLException {
        long sequence = -1;
        CallableStatement callableStatement = null;
        try {
            String statement = "{call GetNewSeqVal_ePayMessageId(?)}";
            Connection cc = ((SessionImpl) em.getDelegate()).connection();
            callableStatement = cc.prepareCall(statement);
            callableStatement.registerOutParameter(1, Types.BIGINT);
            callableStatement.execute();

            sequence = callableStatement.getLong(1);
            callableStatement.close();
        } finally {
            if (callableStatement != null)
                callableStatement.close();
        }
        return sequence;
    }

    public String getNextId() throws SQLException {
        long sequence = getNextMessageIdSequence();
        return String.format("%s%s%023d", LookupsLoader.getInstance().getPSP().getNationalID(), SystemHelper.formatDate(SystemHelper.getSystemTimestamp(), "ddMMyyyy"), sequence);
    }
}
