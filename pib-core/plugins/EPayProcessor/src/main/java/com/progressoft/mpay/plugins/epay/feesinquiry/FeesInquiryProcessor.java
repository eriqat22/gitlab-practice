package com.progressoft.mpay.plugins.epay.feesinquiry;

import java.math.BigDecimal;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_ServiceIntegMessage;
import com.progressoft.mpay.entities.MPAY_ServiceIntegReason;
import com.progressoft.mpay.entities.MPAY_ServiceIntegReasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.epay.common.Constants;
import com.progressoft.mpay.plugins.epay.common.EPayRequestsHelper;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;

public class FeesInquiryProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(FeesInquiryProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = FeesInquiryValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Error while parsing message", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in FeesInquiryProcessor", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
		String response = generateResponse(context, false);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription, String replacementErrorCode) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
		if (replacementErrorCode == null) {
			String response = generateResponse(context, false);
			result.getMessage().setResponseContent(response);
			result.setResponse(response);
		} else {
			String response = generateResponse(context, replacementErrorCode);
			result.getMessage().setResponseContent(response);
			result.setResponse(response);
		}
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

		MPAY_ServiceIntegMessage integMessage = EPayRequestsHelper.generateFeesInquiryRequest(context);
		if (integMessage == null)
			return rejectMessage(context, ReasonCodes.SERVICE_INTEGRATION_FAILURE, "Cannot generate integration message");

		context.setServiceIntegMessage(integMessage);
		context.getDataProvider().persistServiceIntegrationMessage(integMessage);
		integMessage = EPayRequestsHelper.sendBillFee(integMessage, context);
		context.getDataProvider().updateServiceIntegrationMessage(integMessage);

		if (!integMessage.getRefStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return rejectMessage(context, context.getMessage().getReason().getCode(), context.getMessage().getReasonDesc(), integMessage.getReasonCode());

		String response = generateResponse(context, true);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		OTPHanlder.removeOTP(context);
		return result;
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
		logger.debug("Inside GenerateResponse ...");
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		if (isAccepted)
			response.getExtraData().add(new ExtraData(Constants.FEES_INQUIRY_RESPONSE_FIELD_KEY, EPayRequestsHelper.createFeesInquiryResponse(context)));
		return response.toString();
	}

	private String generateResponse(MessageProcessingContext context, String epayResponseCode) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("reasonCode: " + epayResponseCode);
		MPayResponse response = new MPayResponse();
		response.setErrorCd(epayResponseCode);
		MPAY_ServiceIntegReason reason = context.getLookupsLoader().getServiceIntegReason(epayResponseCode);
		if (reason == null)
			reason = context.getLookupsLoader().getServiceIntegReason(Constants.EPAY_GENERIC_ERROR_CODE);
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_ServiceIntegReasons_NLS nls = context.getLookupsLoader().getServiceIntegReasonNLS(reason.getId(), context.getLanguage().getId());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(reason.getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response.toString();
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
		logger.debug("Inside PreProcessMessage ...");
		FeesInquiryMessage message = FeesInquiryMessage.parseMessage(context.getRequest());
		BigDecimal amount = BigDecimal.ZERO;
		String adviseType = message.getType();
		for (BillFeeInfo billInfo : message.getBillInfo())
			amount = amount.add(BigDecimal.valueOf(billInfo.getAmtDue()));
		context.setAmount(amount);
		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
		if (sender.getMobile() == null)
			return;
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), message.getSenderAccount()));
		if (sender.getMobileAccount() == null)
			return;
		OTPHanlder.loadOTP(context, message.getPin());
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		MPAY_ServiceIntegMessage originalRequest = context.getDataProvider().getServiceIntegrationMessageByRequestId(message.getBillInqRqUId());
		if (originalRequest == null)
			return;
		context.setOriginalServiceIntegMessage(originalRequest);
		receiver.setService(originalRequest.getRefService());
		receiver.setCorporate(receiver.getService().getRefCorporate());
		if (adviseType == null || adviseType.trim().length() == 0)
			adviseType = "epayadv";

		MPAY_EndPointOperation epayAdviseOperation = context.getLookupsLoader().getEndPointOperation(adviseType);
		if (epayAdviseOperation == null)
			return;
		context.getExtraData().put(FeesInquiryConstants.EPAY_ADVISE_OPERATRION_KEY, epayAdviseOperation);
		ChargesCalculator.calculateSenderCharge(epayAdviseOperation.getMessageType().getCode(), context, sender.getProfile());
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		logger.debug("Inside Reverse ...");
		return null;
	}
}
