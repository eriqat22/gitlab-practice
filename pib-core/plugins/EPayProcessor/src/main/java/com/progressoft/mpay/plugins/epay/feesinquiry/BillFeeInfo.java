package com.progressoft.mpay.plugins.epay.feesinquiry;


public class BillFeeInfo {
	private String sequence;
	private double amtDue;
	private String curCode;

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public Double getAmtDue() {
		return amtDue;
	}

	public void setAmtDue(Double amtDue) {
		this.amtDue = amtDue;
	}

	public String getCurCode() {
		return curCode;
	}

	public void setCurCode(String curCode) {
		this.curCode = curCode;
	}
}
