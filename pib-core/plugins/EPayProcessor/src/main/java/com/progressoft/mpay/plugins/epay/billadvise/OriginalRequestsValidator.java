package com.progressoft.mpay.plugins.epay.billadvise;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_ServiceIntegMessage;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.epay.common.Constants;

public class OriginalRequestsValidator {
	private static final Logger logger = LoggerFactory.getLogger(OriginalRequestsValidator.class);

	private OriginalRequestsValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		MPAY_ServiceIntegMessage billInquiryRequest = null;
		if (context.getExtraData().containsKey(Constants.BILL_INQUERY_REQUEST_KEY))
			billInquiryRequest = (MPAY_ServiceIntegMessage) context.getExtraData().get(Constants.BILL_INQUERY_REQUEST_KEY);
		MPAY_ServiceIntegMessage feesInquiryRequest = null;
		if (context.getExtraData().containsKey(Constants.FEES_INQUIRY_REQUEST_KEY))
			feesInquiryRequest = (MPAY_ServiceIntegMessage) context.getExtraData().get(Constants.FEES_INQUIRY_REQUEST_KEY);

		if (billInquiryRequest == null)
			return new ValidationResult(ReasonCodes.BILL_INQ_NOT_VALID, null, false);
		if (feesInquiryRequest == null)
			return new ValidationResult(ReasonCodes.FEE_INQ_NOT_VALID, null, false);

		if (context.getReceiver().getService() == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);

		if (billInquiryRequest.getRefService().getId() != context.getReceiver().getService().getId())
			return new ValidationResult(ReasonCodes.BILL_INQ_NOT_VALID, null, false);
		if (feesInquiryRequest.getRefService().getId() != context.getReceiver().getService().getId())
			return new ValidationResult(ReasonCodes.FEE_INQ_NOT_VALID, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
