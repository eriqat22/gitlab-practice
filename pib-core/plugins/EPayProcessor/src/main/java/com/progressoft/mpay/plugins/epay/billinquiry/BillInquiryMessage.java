package com.progressoft.mpay.plugins.epay.billinquiry;

import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class BillInquiryMessage extends MPayRequest {
	private static final Logger logger = LoggerFactory.getLogger(BillInquiryMessage.class);

	private static final String BILLER_ID_KEY = "billerId";
	private static final String RECEIVER_TYPE_KEY = "rcvSvcType";
	private static final String BILLING_ACCOUNT_KEY = "billingAcc";
	private static final String SERVICE_TYPE_KEY = "serviceType";
	private static final String INC_PAID_BILLS_KEY = "incPaidBills";
	private static final String CHARGE_RECEIVER_KEY = "chargeReceiver";

	private String billerId;
	private String rcvSvcType;
	private String billingAcc;
	private String serviceType;
	private String chargeReceiver;
	private Boolean incPaidBills;

	public BillInquiryMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getChargeReceiver() {
		return chargeReceiver;
	}

	public void setChargeReceiver(String chargeReceiver) {
		this.chargeReceiver = chargeReceiver;
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public String getRcvSvcType() {
		return rcvSvcType;
	}

	public void setRcvSvcType(String rcvSvcType) {
		this.rcvSvcType = rcvSvcType;
	}

	public String getBillingAcc() {
		return billingAcc;
	}

	public void setBillingAcc(String billingAcc) {
		this.billingAcc = billingAcc;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Boolean getIncPaidBills() {
		return incPaidBills;
	}

	public void setIncPaidBills(Boolean incPaidBills) {
		this.incPaidBills = incPaidBills;
	}

	public static BillInquiryMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		BillInquiryMessage message = new BillInquiryMessage(request);
		if (StringUtils.isEmpty(message.getSender()))
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

		if (message.getValue(CHARGE_RECEIVER_KEY) == null || message.getValue(CHARGE_RECEIVER_KEY).trim().length() == 0)
			message.setChargeReceiver(message.getSender());
		else
			message.setChargeReceiver(message.getValue(CHARGE_RECEIVER_KEY).trim());

		message.setBillerId(message.getValue(BILLER_ID_KEY));
		if (StringUtils.isEmpty(message.getBillerId()))
			throw new MessageParsingException(BILLER_ID_KEY);
		message.setRcvSvcType(message.getValue(RECEIVER_TYPE_KEY));
		fillReceiverInfo(message);
		if (StringUtils.isEmpty(message.getBillingAcc()))
			throw new MessageParsingException(BILLING_ACCOUNT_KEY);
		message.setServiceType(message.getValue(SERVICE_TYPE_KEY));
		if (StringUtils.isEmpty(message.getServiceType()))
			throw new MessageParsingException(SERVICE_TYPE_KEY);
		try {
			message.setIncPaidBills(Boolean.parseBoolean(message.getValue(INC_PAID_BILLS_KEY)));
		} catch (Exception e) {
			logger.debug("Invalid value in incPaidBills", e);
			throw new MessageParsingException(INC_PAID_BILLS_KEY);
		}
		return message;
	}

	private static void fillReceiverInfo(BillInquiryMessage message) throws MessageParsingException {
		if (StringUtils.isEmpty(message.getRcvSvcType()))
			throw new MessageParsingException(RECEIVER_TYPE_KEY);
		if (!message.getRcvSvcType().equals(ReceiverInfoType.CORPORATE) && !message.getRcvSvcType().equals(ReceiverInfoType.ALIAS))
			throw new MessageParsingException(RECEIVER_TYPE_KEY);
		message.setBillingAcc(message.getValue(BILLING_ACCOUNT_KEY));
	}
}
