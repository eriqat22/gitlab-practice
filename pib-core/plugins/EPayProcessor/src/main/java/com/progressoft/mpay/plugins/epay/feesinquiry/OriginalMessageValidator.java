package com.progressoft.mpay.plugins.epay.feesinquiry;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class OriginalMessageValidator {
	private static final Logger logger = LoggerFactory.getLogger(OriginalMessageValidator.class);

	private OriginalMessageValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		if (context.getOriginalServiceIntegMessage() == null)
			return new ValidationResult(ReasonCodes.BILL_INQ_NOT_VALID, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
