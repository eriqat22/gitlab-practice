package com.progressoft.mpay.plugins.epay.common;

public class EPayStatusCodes {
	public static final String VALID = "0";
	public static final String FAILED_CONNECT = "2";
	public static final String INVALID_MESSAGE_SIGNITURE = "3";

	private EPayStatusCodes() {

	}
}
