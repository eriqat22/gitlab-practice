package com.progressoft.mpay.plugins.epay.common;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by user on 01/11/18.
 */
public class GenerateDate {
    public static String generateDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        return sdf.format(timestamp);
    }
}
