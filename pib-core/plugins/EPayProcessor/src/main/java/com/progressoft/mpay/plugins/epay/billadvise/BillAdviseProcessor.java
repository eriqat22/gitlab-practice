package com.progressoft.mpay.plugins.epay.billadvise;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.epay.common.Constants;
import com.progressoft.mpay.plugins.epay.common.EPayRequestsHelper;
import com.progressoft.mpay.plugins.epay.feesinquiry.FeesInquiryResponse;
import com.progressoft.mpay.plugins.epay.integrations.IntegrationProcessor;
import com.progressoft.mpay.plugins.epay.integrations.IntegrationProcessorFactory;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class BillAdviseProcessor implements MessageProcessor {
	public static final String REASON_DESC = "reasonDesc";
	private static final Logger logger = LoggerFactory.getLogger(BillAdviseProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		IsoMessage mpClearIsoMessage = context.getMpClearIsoMessage();
		try {
			IntegrationProcessor processor = IntegrationProcessorFactory.createProcessor(mpClearIsoMessage.getType());
			IntegrationProcessingResult result = processor.processIntegration(context);
			handleCommissions(context, result.getProcessingStatus());
			return result;
		} catch (Exception e) {
			logger.error("Error when ProcessIntegration", e);
			IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null,
					context.isRequest());
			if (context.isRequest())
				result.setMpClearOutMessage(MPClearHelper.createMPClearResponse(context, mpClearIsoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(),
						MPClearHelper.getResponseType(mpClearIsoMessage.getType()), result.getReason().getCode(), result.getReasonDescription(),
						String.valueOf(ProcessingCodeRanges.CREDIT_START_VALUE)));
			return result;
		}
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = BillAdviseValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Error while parsing message", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in BillAdviseProcessor", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription, String replacementErrorCode) {
		logger.debug("Inside RejectMessage ...");
		if (context.getTransaction() != null) {
			BankIntegrationHandler.reverse(context);
			TransactionHelper.reverseTransaction(context);
		}

		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
		if (replacementErrorCode == null) {
			String response = generateResponse(context, false);
			result.getMessage().setResponseContent(response);
			result.setResponse(response);
		} else {
			String response = generateResponse(context, replacementErrorCode);
			result.getMessage().setResponseContent(response);
			result.setResponse(response);
		}
		return result;
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		if (context.getTransaction() != null) {
			BankIntegrationHandler.reverse(context);
			TransactionHelper.reverseTransaction(context);
		}

		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
		String response = generateResponse(context, false);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = new MessageProcessingResult();
		MPAY_ProcessingStatus status;
		MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
		String reasonDescription = null;
		context.getMessage().setReason(reason);
		BigDecimal externalFees = getExternalFees((BigDecimal) context.getExtraData().get(Constants.TOTAL_FEES_KEY), context);
		MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, externalFees, null);
		result.setMessage(context.getMessage());
		context.setTransaction(transaction);
		result.setTransaction(transaction);
		context.getDataProvider().persistProcessingResult(result);
		JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), transaction);
		if (postingResult.isSuccess()) {
			BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
			if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
				if (context.getDirection() == TransactionDirection.ONUS) {
					MessageProcessingResult onusResult = handleOnus(context);
					if (onusResult != null)
						return onusResult;
				}
				status = handleMPClear(context, result);
			} else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
			} else {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			}
		} else {
			if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
				reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
				context.getMessage().setReasonDesc(postingResult.getReason());
			} else
				reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
		}
		context.getMessage().setProcessingStatus(status);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
		transaction.setReason(context.getMessage().getReason());
		transaction.setReasonDesc(context.getMessage().getReasonDesc());
		result.setStatus(status);
		result.setStatusDescription(context.getMessage().getReasonDesc());
		handleNotifications(context, result, status);
		String response = generateResponse(context, true);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		OTPHanlder.removeOTP(context);
		handleCommissions(context, status);
		handleLimits(context, status, transaction);
		return result;
	}

	private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
		if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return;
		if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.OUTWARD)
			CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
					CommissionDirections.SENDER);
		if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.INWARD)
			CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
					CommissionDirections.RECEIVER);
	}

	private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, MessageProcessingResult result) {
		MPAY_ProcessingStatus status;
		Integer mpClearIntegType;
		if (context.getSystemParameters().getOfflineModeEnabled() && context.getDirection() == TransactionDirection.ONUS) {
			mpClearIntegType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
			result.setHandleMPClearOffline(true);
		} else {
			mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
		}
		boolean isSenderAlias = context.getSender().getMobile().getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) && context.getSender().getMobile().getAlias() != null
				&& context.getSender().getMobile().getAlias().length() > 0;
		IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, isSenderAlias);
		MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegType));
		messageLog.setRefMessage(context.getMessage());
		result.setMpClearMessage(messageLog);
		return status;
	}

	private MessageProcessingResult handleOnus(MessageProcessingContext context) {
		MPAY_ServiceIntegMessage serviceIntegMessage;
		serviceIntegMessage = EPayRequestsHelper.generateEPayAdvRequest(context);
		if (serviceIntegMessage == null)
			return rejectMessage(context, ReasonCodes.SERVICE_INTEGRATION_FAILURE, null);
		context.getDataProvider().persistServiceIntegrationMessage(serviceIntegMessage);
		serviceIntegMessage = EPayRequestsHelper.sendBillAdvise(serviceIntegMessage, context);
		context.getDataProvider().persistServiceIntegrationMessage(serviceIntegMessage);
		if (!serviceIntegMessage.getRefStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return rejectMessage(context, context.getMessage().getReason().getCode(), context.getMessage().getReasonDesc(), serviceIntegMessage.getReasonCode());
		return null;
	}

	private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result, MPAY_ProcessingStatus status) {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
			result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
	}

	private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status, MPAY_Transaction transaction) throws SQLException {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED) || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
			context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
					SystemHelper.getCurrentDateWithoutTime());
		}
	}

	private BigDecimal getExternalFees(BigDecimal totalFees, MessageProcessingContext context) {
		if (totalFees.compareTo(BigDecimal.ZERO) == 0)
			return BigDecimal.ZERO;
		return totalFees.subtract(context.getSender().getCharge());
	}

	private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isSenderAlias) {
		logger.debug("Inside CreateMPClearRequest ...");
		try {
			BillAdviseMessage request = (BillAdviseMessage) context.getRequest();
			String messageID = context.getDataProvider().getNextMPClearMessageId();
			BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
			IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(type);
			message.setBinary(false);

			String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
			String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

			message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
			MPClearHelper.setMessageEncoded(message);
			message.setField(7, new IsoValue<String>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
			message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
			message.setField(49, new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

			String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getMobile(), context.getSender().getMobileAccount(), isSenderAlias);
			MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

			String account2;
			if (context.getReceiver().getService() == null)
				account2 = MPClearHelper.getAccount(context.getSystemParameters(), request.getRcvSvc(), request.getRcvSvcType());
			else
				account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getService(), context.getReceiver().getServiceAccount(),
						request.getRcvSvcType().equals(ReceiverInfoType.ALIAS));

			MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

			String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
			message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
			message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

			return message;
		} catch (Exception e) {
			logger.error("Error when CreateMPClearRequest in BillAdviseProcessor", e);
			return null;
		}
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("isAccepted:" + isAccepted);
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description = context.getMessage().getReasonDesc();
		if (description == null) {
			if (context.getLanguage() == null)
				context.setLanguage(context.getSystemParameters().getSystemLanguage());
			MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
			if (nls == null)
				description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
			else
				description = nls.getDescription();
		}
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response.toString();
	}

	private String generateResponse(MessageProcessingContext context, String epayResponseCode) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("reasonCode: " + epayResponseCode);
		MPayResponse response = new MPayResponse();
		response.setErrorCd(epayResponseCode);
		MPAY_ServiceIntegReason reason = context.getLookupsLoader().getServiceIntegReason(epayResponseCode);
		if (reason == null)
			reason = context.getLookupsLoader().getServiceIntegReason(Constants.EPAY_GENERIC_ERROR_CODE);
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description = context.getMessage().getReasonDesc();
		if (description == null) {
			if (context.getLanguage() == null)
				context.setLanguage(context.getSystemParameters().getSystemLanguage());
			MPAY_ServiceIntegReasons_NLS nls = context.getLookupsLoader().getServiceIntegReasonNLS(reason.getId(), context.getLanguage().getId());
			if (nls == null)
				description = context.getLookupsLoader().getReasonNLS(reason.getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
			else
				description = nls.getDescription();
		}
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response.toString();
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException, SQLException {
		logger.debug("Inside PreProcessMessage ...");
		BillAdviseMessage message = BillAdviseMessage.parseMessage(context.getRequest());
		context.setRequest(message);
		context.setAmount(BigDecimal.valueOf(message.getAmnt()));
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
		sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
		sender.setInfo(message.getSender());
		if (sender.getMobile() == null)
			return;
		sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), message.getSenderAccount()));
		if (sender.getMobileAccount() == null)
			return;
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setBank(sender.getMobileAccount().getBank());
		sender.setAccount(sender.getMobileAccount().getRefAccount());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
		sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
		if (message.getCardPIN() != null && !message.getCardPIN().trim().equals(""))
			context.getExtraData().put(com.progressoft.mpay.Constants.CARD_PIN_KEY, message.getCardPIN());
		MPAY_ServiceIntegMessage billInquiryIntegrationMessage = context.getDataProvider().getServiceIntegrationMessageByRequestId(message.getbillInqRqUId());
		if (billInquiryIntegrationMessage == null)
			return;
		context.getExtraData().put(Constants.BILL_INQUERY_REQUEST_KEY, billInquiryIntegrationMessage);
		MPAY_ServiceIntegMessage feesInquiryIntegrationMessage = context.getDataProvider().getServiceIntegrationMessageByRequestId(message.getfeeInqRqUId());
		if (feesInquiryIntegrationMessage == null)
			return;
		context.getExtraData().put(Constants.FEES_INQUIRY_REQUEST_KEY, feesInquiryIntegrationMessage);
		context.getExtraData().put(Constants.TOTAL_FEES_KEY, getTotalFees(feesInquiryIntegrationMessage));
		long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
		receiver.setService(context.getDataProvider().getCorporateService(message.getRcvSvc(), message.getRcvSvcType()));
		receiver.setInfo(message.getRcvSvc());
		ChargesCalculator.calculateSenderCharge(context.getOperation().getMessageType().getCode(), context, context.getSender().getProfile());
		if (receiver.getService() == null) {
			context.getReceiver().setService(context.getLookupsLoader().getPSP().getPspMPClearService());
			context.getReceiver().setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), context.getReceiver().getService(), null));
			if (context.getReceiver().getServiceAccount() != null)
				context.getReceiver().setBank(context.getReceiver().getServiceAccount().getBank());
			context.setDirection(TransactionDirection.OUTWARD);
			context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
					context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
		} else {
			OTPHanlder.loadOTP(context, message.getPin());
			receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), receiver.getService(), message.getReceiverAccount()));
			if (receiver.getServiceAccount() == null)
				return;
			context.setDirection(TransactionDirection.ONUS);
			receiver.setCorporate(receiver.getService().getRefCorporate());
			receiver.setBank(receiver.getServiceAccount().getBank());
			receiver.setAccount(receiver.getServiceAccount().getRefAccount());
			receiver.setBanked(receiver.getAccount().getIsBanked());
			receiver.setProfile(receiver.getServiceAccount().getRefProfile());
			receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
			context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(),
					context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
			if (context.getTransactionConfig() != null)
				context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
			ChargesCalculator.calculateReceiverCharge(context.getOperation().getMessageType().getCode(), context, context.getReceiver().getProfile());
		}
		TaxCalculator.claculate(context);
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		if (context.getTransaction() != null) {
			context.getTransaction().setReason(reason);
			context.getTransaction().setReasonDesc(reasonDescription);
			context.getTransaction().setProcessingStatus(status);
		}
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setTransaction(context.getTransaction());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		logger.debug("Inside Reverse ...");
		try {
			JVPostingResult result = TransactionHelper.reverseTransaction(context);
			BillAdviseMessage message = BillAdviseMessage.parseMessage(context.getRequest());
			context.getDataProvider().updateAccountLimit(
					SystemHelper.getServiceAccount(context.getSystemParameters(), context.getSender().getService(),
							message.getSenderAccount()).getRefAccount().getId(),
					context.getMessage().getRefOperation().getMessageType().getId(),
					context.getTransaction().getTotalAmount().negate(), -1, SystemHelper.getCurrentDateWithoutTime());
			context.getDataProvider().mergeTransaction(context.getTransaction());
			if (result.isSuccess())
				return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

			return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
		} catch (Exception ex) {
			logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
			return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), ProcessingStatusCodes.FAILED);
		}
	}

	private BigDecimal getTotalFees(MPAY_ServiceIntegMessage feesInquiryIntegrationMessage) {
		MPAY_MPayMessage feesInquiryMPayMessage = feesInquiryIntegrationMessage.getRefMessage();
		MPayResponse feesInquiryResponse = MPayResponse.fromJson(feesInquiryMPayMessage.getResponseContent());
		String feesKeyValue = feesInquiryResponse.getValue(Constants.FEES_INQUIRY_FEES_KEY);
		FeesInquiryResponse feesInquiryResponseDetails = FeesInquiryResponse.fromJson(feesKeyValue);
		if (feesInquiryResponseDetails == null)
			return BigDecimal.ZERO;
		double value = feesInquiryResponseDetails.getTotalFeeAmt();
		return BigDecimal.valueOf(value);
	}
}
