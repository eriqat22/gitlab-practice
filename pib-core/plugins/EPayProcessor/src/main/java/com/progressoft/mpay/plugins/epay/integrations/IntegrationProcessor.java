package com.progressoft.mpay.plugins.epay.integrations;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

@FunctionalInterface
public interface IntegrationProcessor {
	IntegrationProcessingResult processIntegration(IntegrationProcessingContext context);
}
