package com.progressoft.mpay.plugins.epay.billinquiry;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_ServiceIntegMessage;
import com.progressoft.mpay.entities.MPAY_ServiceIntegReason;
import com.progressoft.mpay.entities.MPAY_ServiceIntegReasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.epay.common.Constants;
import com.progressoft.mpay.plugins.epay.common.EPayRequestsHelper;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;

public class BillInquiryProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(BillInquiryProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = BillInquiryValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription(), null);
		} catch (MessageParsingException e) {
			logger.debug("Error while parsing message", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided", null);
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in BillInquiryProcessor", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), null);
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription, String replacementErrorCode) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
		if (replacementErrorCode == null) {
			String response = generateResponse(context, false);
			result.getMessage().setResponseContent(response);
			result.setResponse(response);
		} else {
			String response = generateResponse(context, replacementErrorCode);
			result.getMessage().setResponseContent(response);
			result.setResponse(response);
		}
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage...");
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

		MPAY_ServiceIntegMessage integMessage = EPayRequestsHelper.generateBillInquireRequest(context);
		if (integMessage == null)
			return rejectMessage(context, ReasonCodes.SERVICE_INTEGRATION_FAILURE, "Cannot generate integration message", null);

		context.getDataProvider().persistServiceIntegrationMessage(integMessage);
		integMessage = EPayRequestsHelper.sendBillInquiry(integMessage, context);
		context.getDataProvider().updateServiceIntegrationMessage(integMessage);

		if (!integMessage.getRefStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return rejectMessage(context, context.getMessage().getReason().getCode(), context.getMessage().getReasonDesc(), integMessage.getReasonCode());

		String response = generateResponse(context, true);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		OTPHanlder.removeOTP(context);
		return result;
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("isAccepted: " + isAccepted);
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
		if (nls == null)
			description = Constants.EPAY_GENERIC_ERROR_DESCRIPTION;
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		if (isAccepted)
			response.getExtraData().add(new ExtraData(Constants.BILL_INQUERY_RESPONSE_FIELD_KEY, EPayRequestsHelper.createBillInquiryResponse(context)));
		return response.toString();
	}

	private String generateResponse(MessageProcessingContext context, String epayResponseCode) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("reasonCode: " + epayResponseCode);
		MPayResponse response = new MPayResponse();
		response.setErrorCd(epayResponseCode);
		MPAY_ServiceIntegReason reason = context.getLookupsLoader().getServiceIntegReason(epayResponseCode);
		if (reason == null)
			reason = context.getLookupsLoader().getServiceIntegReason(Constants.EPAY_GENERIC_ERROR_CODE);
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_ServiceIntegReasons_NLS nls = context.getLookupsLoader().getServiceIntegReasonNLS(reason.getId(), context.getLanguage().getId());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(reason.getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response.toString();
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
		logger.debug("Inside PreProcessMessage ...");
		BillInquiryMessage message = BillInquiryMessage.parseMessage(context.getRequest());
		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
		if (sender.getMobile() == null)
			return;
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
		if(sender.getMobileAccount() == null)
			return;
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		OTPHanlder.loadOTP(context, message.getPin());
		receiver.setService(context.getDataProvider().getCorporateServiceByTypeWithoutTenant(message.getBillerId(), message.getRcvSvcType()));
		if (receiver.getService() == null)
			return;
		receiver.setCorporate(receiver.getService().getRefCorporate());

//		context.getExtraData().put(Constants.IS_TAHYA_MISR_KEY, message.getServiceType().equals(context.getSystemParameters().getTahyaMasrServiceTypeCode()));
		context.getExtraData().put(Constants.IS_AIRE_TIME_CHARGE_KEY, message.getServiceType().equals(context.getSystemParameters().getAirTimeServiceTypeCode()));
		context.getExtraData().put(Constants.IS_DONATION_KEY, message.getServiceType().equals(context.getSystemParameters().getDonationsServiceTypeCode()));
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		logger.debug("Inside Reverse ...");
		return null;
	}
}