package com.progressoft.mpay.plugins.epay.common;

public class Constants {
	public static final String BILL_INQUIRY_RESPONSE_KEY = "BillInquiryResponseKey";
	public static final String BILL_INQUERY_REQUEST_KEY = "BillInquiryRequestKey";
	public static final String BILL_INQUERY_RESPONSE_FIELD_KEY = "bills";

	public static final String FEES_INQUIRY_RESPONSE_KEY = "FeesInquiryResponseKey";
	public static final String FEES_INQUIRY_REQUEST_KEY = "FeesInquiryRequestKey";
	public static final String FEES_INQUIRY_RESPONSE_FIELD_KEY = "Fees";

	public static final String BILL_ADVISE_RESPONSE_KEY = "BillAdviseResponseKey";
	public static final String BILL_ADVISE_REQUEST_KEY = "BillAdviseRequestKey";
	public static final String BILL_ADVISE_RESPONSE_FIELD_KEY = "advise";

	public static final String IS_TAHYA_MISR_KEY = "IsTahyaMasrKey";
	public static final String IS_AIRE_TIME_CHARGE_KEY = "IsAirTimeChargeKey";
	public static final String IS_DONATION_KEY = "IsDonationKey";

	public static final String EPAY_GENERIC_ERROR_CODE = "9999";
	public static final String EPAY_GENERIC_ERROR_DESCRIPTION = "Generic Error";
	public static final String FEES_INQUIRY_FEES_KEY = "Fees";
	public static final String FEES_INQUIRY_FEES_AMOUNT_KEY = "totalFeeAmt";
	public static final String TOTAL_FEES_KEY = "TotalFees";

	private Constants() {

	}
}
