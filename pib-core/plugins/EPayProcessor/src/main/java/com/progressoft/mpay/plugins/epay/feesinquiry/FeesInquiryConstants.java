package com.progressoft.mpay.plugins.epay.feesinquiry;

public class FeesInquiryConstants {
	public static final String EPAY_ADVISE_OPERATRION_KEY = "EPayAdviseOperation";

	private FeesInquiryConstants() {

	}
}
