package com.progressoft.mpay.plugins.epay.billadvise;

public class BillAdviseSequence {
	private double amtDue;
	private int sequence;

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public double getAmtDue() {
		return amtDue;
	}

	public void setAmtDue(double amtDue) {
		this.amtDue = amtDue;
	}
}
