package com.progressoft.mpay.plugins.epay.common;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_ServiceIntegMessage;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.LanguageCode;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.epay.billadvise.BillAdviseMessage;
import com.progressoft.mpay.plugins.epay.billadvise.BillAdviseSequence;
import com.progressoft.mpay.plugins.epay.billinquiry.BillInfo;
import com.progressoft.mpay.plugins.epay.billinquiry.BillInquiryMessage;
import com.progressoft.mpay.plugins.epay.billinquiry.BillInquiryResponse;
import com.progressoft.mpay.plugins.epay.entities.*;
import com.progressoft.mpay.plugins.epay.feesinquiry.BillFeeInfo;
import com.progressoft.mpay.plugins.epay.feesinquiry.FeesInquiryMessage;
import com.progressoft.mpay.plugins.epay.feesinquiry.FeesInquiryResponse;
import com.progressoft.mpay.plugins.epay.wsdl.BillPaymentServicePortClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.ws.WebServiceException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class EPayRequestsHelper {
    private static final String INVALID_RESPONSE_RECEIVED = "Invalid response received";
    private static final String SENDER_ID = "Sender Id";
    private static final String RECEIVER = "Receiver";
    private static final Logger logger = LoggerFactory.getLogger(EPayRequestsHelper.class);

    private EPayRequestsHelper() {

    }

    public static MPAY_ServiceIntegMessage generateBillInquireRequest(MessageProcessingContext context) {
        logger.debug("Inside GenerateBillInquireRequest ...");
        try {
            BillInquiryMessage request = (BillInquiryMessage) context.getRequest();
            MPAY_MPayMessage mpayMessage = context.getMessage();
            SignonProfileType signOnProfile = new SignonProfileType();
            signOnProfile.setMsgCode(MessageCodeType.RBINQRQ);
            signOnProfile.setReceiver(context.getLookupsLoader().getSystemConfigurations(RECEIVER).getConfigValue());
            signOnProfile.setSender(context.getLookupsLoader().getSystemConfigurations(SENDER_ID).getConfigValue());

            EFBPS billInqRequest = new EFBPS();
            billInqRequest.setSignonRq(new SignonRqType());
            billInqRequest.getSignonRq().setClientDt(GenerateDate.generateDateFormat());
            billInqRequest.getSignonRq().setLanguagePref(LanguagePrefType.EN_GB);
            billInqRequest.getSignonRq().setSignonProfile(signOnProfile);
            billInqRequest.setBankSvcRq(new BankSvcRqType());
            billInqRequest.getBankSvcRq().setRqUID(mpayMessage.getReference());

            AccountIdType accId = new AccountIdType();
            accId.setBillerId(request.getBillerId());
            boolean isTahyaMasr = (boolean) context.getExtraData().get(Constants.IS_TAHYA_MISR_KEY);
            boolean isDonation = (boolean) context.getExtraData().get(Constants.IS_DONATION_KEY);
            if (isTahyaMasr || isDonation) {
                String mobileNumber;
                mobileNumber = context.getSender().getMobile().getMobileNumber();

                if (mobileNumber.startsWith("00"))
                    mobileNumber = "0" + mobileNumber.substring(2 + context.getSystemParameters().getCountryCode().length());
                else
                    mobileNumber = "0" + mobileNumber.substring(context.getSystemParameters().getCountryCode().length());
                accId.setBillingAcct(mobileNumber);
            } else
                accId.setBillingAcct(request.getBillingAcc());

            BillInqRqType billInqType = new BillInqRqType();
            billInqType.setBankId(context.getLookupsLoader().getSystemConfigurations("Bank Id").getConfigValue());
            billInqType.setAccessChannel(AccessChannelType.fromValue(context.getLookupsLoader().getSystemConfigurations("Access Channel").getConfigValue()));
            billInqType.setAccountId(accId);
            billInqType.setServiceType(context.getLookupsLoader().getServiceType(request.getServiceType()).getName());
            billInqType.setIncPaidBills(request.getIncPaidBills());
            billInqRequest.getBankSvcRq().setBillInqRq(billInqType);

            String messageContent = getStringMessage(billInqRequest);
            if (messageContent == null)
                return null;
            MPAY_ServiceIntegMessage integMessage = new MPAY_ServiceIntegMessage();
            // set request message
            EnquireBills enquireBillsType = new EnquireBills();
            enquireBillsType.setRequest1(new Request());
            enquireBillsType.getRequest1().setMessage(messageContent);
            String token = context.getCryptographer().generateToken(context.getSystemParameters(), enquireBillsType.getRequest1().getMessage(), "", IntegMessagesSource.SYSTEM);
            enquireBillsType.getRequest1().setSignature(token);
            enquireBillsType.getRequest1().setSenderID(context.getLookupsLoader().getSystemConfigurations(SENDER_ID).getConfigValue());

            integMessage.setRequestContent(messageContent);
            integMessage.setRequestDate(SystemHelper.getSystemTimestamp());
            integMessage.setRequestToken(token);
            integMessage.setRequestID(billInqRequest.getBankSvcRq().getRqUID());
            integMessage.setRefMessage(mpayMessage);
            integMessage.setRefService(context.getReceiver().getService());
            integMessage.setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
            context.getExtraData().put(Constants.BILL_INQUERY_REQUEST_KEY, billInqRequest);
            return integMessage;
        } catch (Exception ex) {
            logger.error("Error when GenerateBillInquireRequest in EPayRequestsHelper", ex);
            return null;
        }
    }

    public static MPAY_ServiceIntegMessage sendBillInquiry(MPAY_ServiceIntegMessage integMessage, MessageProcessingContext context) {
        logger.debug("Inside SendBillInquiry ...");
        BillPaymentServicePortClient client = new BillPaymentServicePortClient();
        client.initClient();
        Response response = null;

        try {
            Request request = new Request();
            request.setMessage(integMessage.getRequestContent());
            request.setSignature(integMessage.getRequestToken());
            request.setSenderID(context.getLookupsLoader().getSystemConfigurations(SENDER_ID).getConfigValue());
            response = client.enquireBills(request);
            EFBPS billInqResponse = unmarshallResponse(response.getMessage());
            if (billInqResponse == null)
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, EPayStatusCodes.FAILED_CONNECT,
                        INVALID_RESPONSE_RECEIVED);
//			if (!verifySigniture(context, response))
//				return generateResponse(context, integMessage, response, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION, EPayStatusCodes.INVALID_MESSAGE_SIGNITURE,
//						ReasonCodes.INVALID_TOKEN + " - " + context.getLookupsLoader().getReason(ReasonCodes.INVALID_TOKEN).getDescription());

            String responseCode = String.valueOf(billInqResponse.getBankSvcRs().getStatus().getStatusCode());
            if (responseCode.equals(EPayStatusCodes.VALID)) {
                context.getExtraData().put(Constants.BILL_INQUIRY_RESPONSE_KEY, billInqResponse);
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, EPayStatusCodes.VALID, null);
            } else
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION, responseCode, billInqResponse.getBankSvcRs()
                        .getStatus().getShortDesc());
        } catch (Exception e) {
            logger.error("Error when SendBillInquiry in EPayRequestsHelper", e);
            return generateResponse(context, integMessage, response, ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, EPayStatusCodes.FAILED_CONNECT, e.getMessage());
        }
    }

    public static MPAY_ServiceIntegMessage sendBillFee(MPAY_ServiceIntegMessage integMessage, MessageProcessingContext context) {
        logger.debug("Inside SendBillFee ...");
        BillPaymentServicePortClient client = new BillPaymentServicePortClient();
        client.initClient();

        Response response = null;

        try {
            Request request = new Request();
            request.setMessage(integMessage.getRequestContent());
            request.setSignature(integMessage.getRequestToken());
            request.setSenderID(context.getLookupsLoader().getSystemConfigurations(SENDER_ID).getConfigValue());
            response = client.calculateCommission(request);
            EFBPS feesInqResponse = unmarshallResponse(response.getMessage());
            if (feesInqResponse == null)
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, EPayStatusCodes.FAILED_CONNECT,
                        INVALID_RESPONSE_RECEIVED);
//			if (!verifySigniture(context, response))
//				return generateResponse(context, integMessage, response, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION, EPayStatusCodes.INVALID_MESSAGE_SIGNITURE,
//						ReasonCodes.INVALID_TOKEN + " - " + context.getLookupsLoader().getReason(ReasonCodes.INVALID_TOKEN).getDescription());

            String responseCode = String.valueOf(feesInqResponse.getBankSvcRs().getStatus().getStatusCode());
            if (responseCode.equals(EPayStatusCodes.VALID)) {
                context.getExtraData().put(Constants.FEES_INQUIRY_RESPONSE_KEY, feesInqResponse);
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, EPayStatusCodes.VALID, null);
            } else
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION, responseCode, feesInqResponse
                        .getBankSvcRs().getStatus().getShortDesc());
        } catch (Exception e) {
            logger.error("Error when SendBillFee in EPayRequestsHelper", e);
            return generateResponse(context, integMessage, response, ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, EPayStatusCodes.FAILED_CONNECT, e.getMessage());
        }
    }

    public static MPAY_ServiceIntegMessage generateFeesInquiryRequest(MessageProcessingContext context) {
        logger.debug("Inside SendBillFee ...");
        try {
            FeesInquiryMessage message = (FeesInquiryMessage) context.getRequest();
            EFBPS feeInqRequest = new EFBPS();

            feeInqRequest.setSignonRq(new SignonRqType());
            feeInqRequest.getSignonRq().setClientDt(GenerateDate.generateDateFormat());
            feeInqRequest.getSignonRq().setLanguagePref(LanguagePrefType.EN_GB);

            SignonProfileType signOnProfileFeeInq = new SignonProfileType();
            signOnProfileFeeInq.setMsgCode(MessageCodeType.RFINQRQ);
            signOnProfileFeeInq.setReceiver(context.getLookupsLoader().getSystemConfigurations(RECEIVER).getConfigValue());
            signOnProfileFeeInq.setSender(context.getLookupsLoader().getSystemConfigurations(SENDER_ID).getConfigValue());
            feeInqRequest.getSignonRq().setSignonProfile(signOnProfileFeeInq);

            feeInqRequest.setBankSvcRq(new BankSvcRqType());
            feeInqRequest.getBankSvcRq().setRqUID(context.getMessage().getReference());

            FeeInqRqType feeInqRq = new FeeInqRqType();
            feeInqRq.setEPayBillRecID(message.getEpayBillRecID());
            for (BillFeeInfo billInfo : message.getBillInfo()) {
                PmtAmtType pmtAmtType = new PmtAmtType();
                pmtAmtType.setAmt(BigDecimal.valueOf(billInfo.getAmtDue()));
                pmtAmtType.setSequence(billInfo.getSequence());
                pmtAmtType.setCurCode(billInfo.getCurCode() != null ? BigInteger.valueOf(Long.parseLong(billInfo.getCurCode())) : null);
                feeInqRq.getPayAmt().add(pmtAmtType);
            }

            feeInqRequest.getBankSvcRq().setFeeInqRq(feeInqRq); //

            // set request message
            CalculateCommission calcCommissionType = new CalculateCommission();
            calcCommissionType.setRequest1(new Request());
            calcCommissionType.getRequest1().setMessage(getStringMessage(feeInqRequest));

            // set request signature
            String token = context.getCryptographer().generateToken(context.getSystemParameters(), calcCommissionType.getRequest1().getMessage(), "", IntegMessagesSource.SYSTEM);
            calcCommissionType.getRequest1().setSignature(token);

            // set request sender
            calcCommissionType.getRequest1().setSenderID(context.getLookupsLoader().getSystemConfigurations(SENDER_ID).getConfigValue());
            MPAY_ServiceIntegMessage integMessage = new MPAY_ServiceIntegMessage();
            integMessage.setRequestID(feeInqRequest.getBankSvcRq().getRqUID());
            integMessage.setRequestDate(SystemHelper.getSystemTimestamp());
            integMessage.setRequestContent(calcCommissionType.getRequest1().getMessage());
            integMessage.setRequestToken(calcCommissionType.getRequest1().getSignature());
            integMessage.setRefMessage(context.getMessage());
            integMessage.setRefService(context.getReceiver().getService());
            integMessage.setRefStatus(context.getMessage().getProcessingStatus());
            return integMessage;
        } catch (Exception ex) {
            logger.error("Error when GenerateFeesInquiryRequest in EPayRequestsHelper", ex);
            return null;
        }
    }

    private static MPAY_ServiceIntegMessage generateResponse(ProcessingContext context, MPAY_ServiceIntegMessage integMessage, Response response, String processingStatusCode, String reasonCode,
                                                             String epayStatus, String reasonDescription) {
        logger.debug("Inside GenerateResponse ...");
        MPAY_ProcessingStatus processingStatus = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);

        integMessage.setReasonCode(epayStatus);
        integMessage.setReasonDescription(reasonDescription);
        integMessage.setRefStatus(processingStatus);
        integMessage.setResponseDate(SystemHelper.getSystemTimestamp());
        if (response != null) {
            integMessage.setResponseContent(response.getMessage());
            integMessage.setResponseToken(response.getSignature());
        }
        if (!processingStatusCode.equals(ProcessingStatusCodes.ACCEPTED)) {
            context.getMessage().setReason(reason);
            context.getMessage().setProcessingStatus(processingStatus);
            context.getMessage().setReasonDesc(reasonDescription);
        }
        return integMessage;
    }

    private static String getStringMessage(EFBPS request) {
        logger.debug("Inside GetStringMessage ...");
        StringWriter writer = new StringWriter();

        JAXBContext jbContext;
        try {
            jbContext = JAXBContext.newInstance(EFBPS.class);

            Marshaller feeMarshallerObj;
            feeMarshallerObj = jbContext.createMarshaller();
            feeMarshallerObj.marshal(request, writer);
            return writer.toString();
        } catch (Exception e) {
            logger.error("Error when GetStringMessage in EPayRequestsHelper", e);
            return null;
        }
    }

//	public static boolean verifySigniture(ProcessingContext context, Response enquireBillsResponse) {
//		logger.debug("Inside verifySigniture ...");
//		try {
//			return context.getCryptographer().verifyToken(context.getSystemParameters(), enquireBillsResponse.getMessage(), enquireBillsResponse.getSignature(), null, IntegMessagesSource.E_PAY);
//		} catch (Exception ex) {
//			logger.error("Error when verifySigniture in EPayRequestsHelper", ex);
//			return false;
//		}
//	}

    private static EFBPS unmarshallResponse(String response) {
        logger.debug("Inside unmarshallResponse ...");
        JAXBContext jbContext = null;
        Unmarshaller unMarshallerObj = null;
        try {
            jbContext = JAXBContext.newInstance(EFBPS.class);
            unMarshallerObj = jbContext.createUnmarshaller();
            return (EFBPS) unMarshallerObj.unmarshal(new StringReader(response));
        } catch (JAXBException e) {
            logger.error("Error when unmarshallResponse in EPayRequestsHelper", e);
            return null;
        }
    }

    public static String createBillInquiryResponse(MessageProcessingContext context) {
        logger.debug("Inside CreateBillInquiryResponse ...");
        boolean isTahyaMasr = (boolean) context.getExtraData().get(Constants.IS_TAHYA_MISR_KEY);
        boolean isDonation = (boolean) context.getExtraData().get(Constants.IS_DONATION_KEY);

        EFBPS request = null;
        EFBPS billInqResponse = null;

        if (context.getExtraData().containsKey(Constants.BILL_INQUERY_REQUEST_KEY))
            request = (EFBPS) context.getExtraData().get(Constants.BILL_INQUERY_REQUEST_KEY);
        if (context.getExtraData().containsKey(Constants.BILL_INQUIRY_RESPONSE_KEY))
            billInqResponse = (EFBPS) context.getExtraData().get(Constants.BILL_INQUIRY_RESPONSE_KEY);

        if (request == null || billInqResponse == null)
            return null;

        BillInqRsType billInqRs = billInqResponse.getBankSvcRs().getBillInqRs();
        BillInquiryResponse response = new BillInquiryResponse();
        response.setBillInqRqUId(request.getBankSvcRq().getRqUID());
        List<BillRecType> billRec = billInqRs.getBillRec();
        response.setEpayBillRecID(billRec.get(0).getEPayBillRecID());
        response.setBillNumber(billRec.get(0).getBillInfo().getBillNumber());
        response.setBillCategory(billRec.get(0).getBillInfo().getBillCategory());
        response.setBillingAcct(billRec.get(0).getBillInfo().getAccountId().getBillingAcct());
        response.setDueDate(billRec.get(0).getBillInfo().getDueDt().toGregorianCalendar().getTime());
        response.setBillerId(billRec.get(0).getBillInfo().getAccountId().getBillerId());
        response.setPmtRefInfo(billRec.get(0).getBillInfo().getBillRefInfo());
        response.setServiceType(billRec.get(0).getBillInfo().getServiceType());
        String userLanguage = context.getLanguage().getCode().equals(String.valueOf(LanguageCode.ARBIC)) ? LanguagePrefType.AR_EG.value() : LanguagePrefType.EN_GB.value();
        fillAddress(response, billRec, userLanguage);

        if (isTahyaMasr || isDonation) {
            BillInquiryMessage message = (BillInquiryMessage) context.getRequest();
            AmountDueType amnt = billRec.get(0).getBillInfo().getCurAmt().get(0);
            amnt.setExactPmt(true);
            amnt.setPymtMode(PaymentModeType.INSTALLMENT);
            amnt.setAmtDue(BigDecimal.valueOf(Double.parseDouble(message.getBillingAcc())));
            amnt.setOriginalAmt(amnt.getAmtDue());
            RangeInfoType type = new RangeInfoType();
            type.setMinAmt(amnt.getAmtDue());
            type.setMaxAmt(amnt.getAmtDue());
            amnt.setRangeInfo(type);
            response.getBillInfo().add(BillInfo.create(amnt, userLanguage));
        } else {
            for (AmountDueType amnt : billRec.get(0).getBillInfo().getCurAmt()) {
                response.getBillInfo().add(BillInfo.create(amnt, userLanguage));
            }
        }
        return response.toString();
    }

    private static void fillAddress(BillInquiryResponse response, List<BillRecType> billRec, String userLanguage) {
        for (MsgType address : billRec.get(0).getMsg()) {
            if (address.getLanguagePref().value().equalsIgnoreCase(userLanguage)) {
                response.setAddress(address.getText());
                break;
            }
        }
        if (response.getAddress() == null && !billRec.get(0).getMsg().isEmpty())
            response.setAddress(billRec.get(0).getMsg().get(0).getText());
    }

    public static String createFeesInquiryResponse(MessageProcessingContext context) {
        logger.debug("Inside CreateFeesInquiryResponse ...");
        BigDecimal totalFeesAmnt = new BigDecimal(0);
        BigDecimal totalBillsAmnt = new BigDecimal(0);
        BigDecimal totalAmntWithFee;

        FeesInquiryMessage message = (FeesInquiryMessage) context.getRequest();
        EFBPS feesInquiryResponse = null;

        if (context.getExtraData().containsKey(Constants.FEES_INQUIRY_RESPONSE_KEY))
            feesInquiryResponse = (EFBPS) context.getExtraData().get(Constants.FEES_INQUIRY_RESPONSE_KEY);

        if (message == null || feesInquiryResponse == null)
            return null;

        FeeInqRsType feeInqRs = feesInquiryResponse.getBankSvcRs().getFeeInqRs();

        if (feeInqRs != null) {
            totalFeesAmnt = feeInqRs.getFeesAmt().get(0).getAmt().add(context.getSender().getCharge());
        }

        for (BillFeeInfo billInfo : message.getBillInfo()) {
            totalBillsAmnt = totalBillsAmnt.add(billInfo.getAmtDue() == null ? new BigDecimal(0) : BigDecimal.valueOf(billInfo.getAmtDue()));
        }
        totalAmntWithFee = totalBillsAmnt.add(totalFeesAmnt);

        FeesInquiryResponse jsonResponse = new FeesInquiryResponse();

        jsonResponse.setTotalAmtWithFee(totalAmntWithFee.doubleValue());
        jsonResponse.setTotalBillsAmt(totalBillsAmnt.doubleValue());
        jsonResponse.setTotalFeeAmt(totalFeesAmnt.doubleValue());
        jsonResponse.setFeeInqRqUId(context.getServiceIntegMessage().getRequestID());

        return jsonResponse.toString();
    }

    public static MPAY_ServiceIntegMessage generateEPayAdvRequest(MessageProcessingContext context) {
        logger.debug("Inside GenerateEPayAdvRequest ...");
        BillAdviseMessage payAdvicReq = (BillAdviseMessage) context.getRequest();
        EFBPS payAdvMessage = new EFBPS();

        try {

            MPAY_ServiceIntegMessage billInquiryIntegMessage = (MPAY_ServiceIntegMessage) context.getExtraData().get(Constants.BILL_INQUERY_REQUEST_KEY);
            EFBPS billInqResponse = unmarshallResponse(billInquiryIntegMessage.getResponseContent());
            if (billInqResponse == null)
                throw new InvalidOperationException("Failed to unmarshall response");
            MPAY_ServiceIntegMessage feesInquiryIntegMessage = (MPAY_ServiceIntegMessage) context.getExtraData().get(Constants.FEES_INQUIRY_REQUEST_KEY);
            EFBPS feeResponse = unmarshallResponse(feesInquiryIntegMessage.getResponseContent());

            payAdvMessage.setSignonRq(new SignonRqType());
            payAdvMessage.getSignonRq().setClientDt(GenerateDate.generateDateFormat());
            payAdvMessage.getSignonRq().setLanguagePref(LanguagePrefType.EN_GB);

            SignonProfileType signOnProfile = new SignonProfileType();
            signOnProfile.setMsgCode(MessageCodeType.RPADVRQ);
            // get values according system configuration
            signOnProfile.setReceiver(context.getLookupsLoader().getSystemConfigurations(RECEIVER).getConfigValue());
            signOnProfile.setSender(context.getLookupsLoader().getSystemConfigurations(SENDER_ID).getConfigValue());

            payAdvMessage.getSignonRq().setSignonProfile(signOnProfile);
            payAdvMessage.setBankSvcRq(new BankSvcRqType());

            payAdvMessage.getBankSvcRq().setRqUID(context.getMessage().getReference());
            payAdvMessage.getBankSvcRq().setPmtAdviceRq(new PmtAdviceRqType());

            payAdvMessage.getBankSvcRq().getPmtAdviceRq().setPmtRec(new PmtRecType());
            payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().setPmtInfo(new PmtInfoType());

            List<BillRecType> billRecords = billInqResponse.getBankSvcRs().getBillInqRs().getBillRec();
            payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtInfo().setPmtRefInfo(billRecords.get(0).getBillInfo().getBillRefInfo());
            payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtInfo().setBillNumber(billRecords.get(0).getBillInfo().getBillNumber());
            payAdvMessage.getBankSvcRq().getPmtAdviceRq().setEPayBillRecID(billRecords.get(0).getEPayBillRecID());

            if (feeResponse != null) {
                FeesAmountType oFeesAmountType = new FeesAmountType();
                oFeesAmountType.setAmt(feeResponse.getBankSvcRs().getFeeInqRs().getFeesAmt().get(0).getAmt());
                oFeesAmountType.setCurCode(BigInteger.valueOf(feeResponse.getBankSvcRs().getFeeInqRs().getFeesAmt().get(0).getCurCode().longValue()));
                payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtInfo().getFeesAmt().add(oFeesAmountType);
            }

            PmtTransIdType objPmtTransIdType = new PmtTransIdType();
            objPmtTransIdType.setPmtId(EPayMessageIdGenerator.getInstance().getNextId());
            objPmtTransIdType.setPmtIdType(PmtIdTypeType.BNKPTN);
            payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtTransId().add(objPmtTransIdType);

            payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtInfo().setPrcDt(GenerateDate.generateDateFormat());

            AccountIdType objAccIdType = new AccountIdType();
            objAccIdType.setBillingAcct(billInqResponse.getBankSvcRs().getBillInqRs().getBillRec().get(0).getBillInfo().getAccountId().getBillingAcct());
            if (context.getDirection() == TransactionDirection.ONUS)// validation
                objAccIdType.setBillerId(context.getReceiver().getService().getName());

            payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtInfo().setAccountId(objAccIdType);

            // get values according system configuration
            payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtInfo()
                    .setAccessChannel(AccessChannelType.valueOf(context.getLookupsLoader().getSystemConfigurations("Access Channel").getConfigValue()));
            payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtInfo().setBankId(context.getLookupsLoader().getSystemConfigurations("Bank Id").getConfigValue());

            for (BillAdviseSequence payAdvInfo : payAdvicReq.getSequences()) {
                PmtAmtType amt = new PmtAmtType();
                amt.setAmt(BigDecimal.valueOf(payAdvInfo.getAmtDue()));
                amt.setSequence(String.valueOf(payAdvInfo.getSequence()));
                amt.setCurCode(billRecords.get(0).getBillInfo().getCurAmt().get(0).getCurCode());
                payAdvMessage.getBankSvcRq().getPmtAdviceRq().getPmtRec().getPmtInfo().getPayAmt().add(amt);
            }
            String payAdviseMessageContent = getStringMessage(payAdvMessage);
            String token = context.getCryptographer().generateToken(context.getSystemParameters(), payAdviseMessageContent, "", IntegMessagesSource.SYSTEM);

            MPAY_ServiceIntegMessage integMessage = new MPAY_ServiceIntegMessage();
            integMessage.setRequestContent(payAdviseMessageContent);
            integMessage.setRequestToken(token);
            integMessage.setRequestID(context.getMessage().getReference());
            integMessage.setRequestDate(SystemHelper.getSystemTimestamp());
            integMessage.setRefMessage(context.getMessage());
            integMessage.setRefService(context.getReceiver().getService());
            integMessage.setRefStatus(context.getMessage().getProcessingStatus());

            return integMessage;
        } catch (Exception e) {
            logger.error("Error when GenerateEPayAdvRequest in EPayRequestsHelper", e);
            return null;
        }
    }

    public static MPAY_ServiceIntegMessage sendBillAdvise(MPAY_ServiceIntegMessage integMessage, MessageProcessingContext context) {
        logger.debug("Inside SendBillAdvise ...");
        ConfirmPayments payAdviceType = new ConfirmPayments();
        payAdviceType.setRequest1(new Request());
        payAdviceType.getRequest1().setMessage(integMessage.getRequestContent());

        // set request signature
        payAdviceType.getRequest1().setSignature(integMessage.getRequestToken());
        // set request sender
        payAdviceType.getRequest1().setSenderID(context.getLookupsLoader().getSystemConfigurations(SENDER_ID).getConfigValue());

        BillPaymentServicePortClient client = new BillPaymentServicePortClient();
        client.initClient();
        Response response = null;
        // sending request message
        try {
            response = client.confirmPayments(payAdviceType.getRequest1());

            EFBPS payAdviceResponse = unmarshallResponse(response.getMessage());
            if (payAdviceResponse == null)
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, EPayStatusCodes.FAILED_CONNECT,
                        INVALID_RESPONSE_RECEIVED);
//			if (!verifySigniture(context, response))
//				return generateResponse(context, integMessage, response, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION, EPayStatusCodes.INVALID_MESSAGE_SIGNITURE,
//						ReasonCodes.INVALID_TOKEN + " - " + context.getLookupsLoader().getReason(ReasonCodes.INVALID_TOKEN).getDescription());

            String responseCode = String.valueOf(payAdviceResponse.getBankSvcRs().getStatus().getStatusCode());
            if (responseCode.equals(EPayStatusCodes.VALID)) {
                context.getExtraData().put(Constants.BILL_ADVISE_RESPONSE_KEY, payAdviceResponse);
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, EPayStatusCodes.VALID, null);
            } else
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION, responseCode, payAdviceResponse
                        .getBankSvcRs().getStatus().getShortDesc());
        } catch (WebServiceException e) {
            logger.debug("Error when sendBillAdvise", e);
            if (e.getCause().toString().contains("Read timed out")) {
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.ACCEPTED, ReasonCodes.REQUEST_TIMED_OUT, EPayStatusCodes.FAILED_CONNECT,
                        ReasonCodes.REQUEST_TIMED_OUT + " - " + context.getLookupsLoader().getReason(ReasonCodes.REQUEST_TIMED_OUT).getDescription());
            } else {
                return generateResponse(context, integMessage, response, ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, EPayStatusCodes.FAILED_CONNECT,
                        e.getMessage());
            }
        } catch (Exception e) {
            logger.error("Error when SendBillAdvise in EPayRequestsHelper", e);
            return generateResponse(context, integMessage, response, ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, EPayStatusCodes.FAILED_CONNECT, e.getMessage());
        }
    }
}