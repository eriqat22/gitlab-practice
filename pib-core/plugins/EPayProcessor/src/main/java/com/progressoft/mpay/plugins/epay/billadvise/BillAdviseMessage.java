package com.progressoft.mpay.plugins.epay.billadvise;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.List;

public class BillAdviseMessage extends MPayRequest {
	private static final Logger logger = LoggerFactory.getLogger(BillAdviseMessage.class);

	public static final String AMOUNT_KEY = "amnt";
	public static final String RECEIVER_SERVICE_KEY = "rcvSvc";
	public static final String RECEIVER_TYPE_KEY = "rcvSvcType";
	public static final String BILLING_ACCOUNT_KEY = "billingAcc";
	public static final String BILL_INQ_REQ_UID_KEY = "billInqRqUId";
	public static final String FEE_INQ_REQ_UID_KEY = "feeInqRqUId";
	public static final String SEQUENCES_KEY = "sequences";
	private static final String SENDER_ACCOUNT_KEY = "senderAccount";
	private static final String RECEIVER_ACCOUNT_KEY = "receiverAccount";
	private static final String CARD_PIN_KEY= "cardPIN";

	private double amnt;
	private String rcvSvc;
	private String rcvSvcType;
	private String billingAcc;
	private String billInqRqUId;
	private String feeInqRqUId;
	private String senderAccount;
	private String receiverAccount;
	private String cardPIN;
	private List<BillAdviseSequence> sequences;

	public BillAdviseMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getCardPIN() {
		return cardPIN;
	}

	public void setCardPIN(String cardPIN) {
		this.cardPIN = cardPIN;
	}

	public String getbillInqRqUId() {
		return billInqRqUId;
	}

	public void setbillInqRqUId(String billInqRqUId) {
		this.billInqRqUId = billInqRqUId;
	}

	public String getBillingAcc() {
		return billingAcc;
	}

	public void setBillingAcc(String billingAcc) {
		this.billingAcc = billingAcc;
	}

	public List<BillAdviseSequence> getSequences() {
		return sequences;
	}

	public void setSequences(List<BillAdviseSequence> sequences) {
		this.sequences = sequences;
	}

	public void setSequencesFromJson(String json) throws MessageParsingException {
		try {
			Type listType = new TypeToken<List<BillAdviseSequence>>() {
			}.getType();
			Gson gson = new GsonBuilder().create();
			this.sequences = gson.fromJson(json, listType);
			if (this.sequences == null || this.sequences.isEmpty())
				throw new MessageParsingException(SEQUENCES_KEY);
		} catch (Exception ex) {
			logger.debug("Invalid Message Received", ex);
			throw new MessageParsingException(SEQUENCES_KEY);
		}
	}

	public String getRcvSvcType() {
		return rcvSvcType;
	}

	public void setRcvSvcType(String rcvSvcType) {
		this.rcvSvcType = rcvSvcType;
	}

	public double getAmnt() {
		return amnt;
	}

	public void setAmnt(double amnt) {
		this.amnt = amnt;
	}

	public String getRcvSvc() {
		return rcvSvc;
	}

	public void setRcvSvc(String rcvSvc) {
		this.rcvSvc = rcvSvc;
	}

	public String getfeeInqRqUId() {
		return feeInqRqUId;
	}

	public void setfeeInqRqUId(String feeInqRqUId) {
		this.feeInqRqUId = feeInqRqUId;
	}

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public String getReceiverAccount() {
		return receiverAccount;
	}

	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	public static BillAdviseMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		BillAdviseMessage message = new BillAdviseMessage(request);
		fillGenericFields(message);

		String amount = message.getValue(AMOUNT_KEY);
		if (amount == null || amount.trim().length() == 0)
			throw new MessageParsingException(AMOUNT_KEY);
		try {
			message.setAmnt(Double.parseDouble(amount));
		} catch (Exception e) {
			logger.debug("Invalid Amount", e);
			throw new MessageParsingException(AMOUNT_KEY);
		}
		fillReceiverInfo(message);
		message.setBillingAcc(message.getValue(BILLING_ACCOUNT_KEY));
		if (StringUtils.isEmpty(message.getBillingAcc()))
			throw new MessageParsingException(BILLING_ACCOUNT_KEY);
		message.setbillInqRqUId(message.getValue(BILL_INQ_REQ_UID_KEY));
		if (StringUtils.isEmpty(message.getbillInqRqUId()))
			throw new MessageParsingException(BILL_INQ_REQ_UID_KEY);
		message.setfeeInqRqUId(message.getValue(FEE_INQ_REQ_UID_KEY));
		if (StringUtils.isEmpty(message.getfeeInqRqUId()))
			throw new MessageParsingException(FEE_INQ_REQ_UID_KEY);
		message.setSequencesFromJson(message.getValue(SEQUENCES_KEY));
		message.setSenderAccount(message.getValue(SENDER_ACCOUNT_KEY));
		message.setReceiverAccount(message.getValue(RECEIVER_ACCOUNT_KEY));
		message.setCardPIN(message.getValue(CARD_PIN_KEY));
		return message;
	}

	private static void fillReceiverInfo(BillAdviseMessage message) throws MessageParsingException {
		message.setRcvSvc(message.getValue(RECEIVER_SERVICE_KEY));
		if (StringUtils.isEmpty(message.getRcvSvc()))
			throw new MessageParsingException(RECEIVER_SERVICE_KEY);
		message.setRcvSvcType(message.getValue(RECEIVER_TYPE_KEY));
		if (message.getRcvSvcType() == null || (!message.getRcvSvcType().equals(ReceiverInfoType.CORPORATE) && !message.getRcvSvcType().equals(ReceiverInfoType.ALIAS)))
			throw new MessageParsingException(RECEIVER_TYPE_KEY);
	}

	private static void fillGenericFields(BillAdviseMessage message) throws MessageParsingException {
		if (StringUtils.isEmpty(message.getSender()))
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
	}
}
