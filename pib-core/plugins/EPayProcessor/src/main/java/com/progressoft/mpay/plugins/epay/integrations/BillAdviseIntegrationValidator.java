package com.progressoft.mpay.plugins.epay.integrations;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;

public class BillAdviseIntegrationValidator {
	private static final Logger logger = LoggerFactory.getLogger(BillAdviseIntegrationValidator.class);

	private BillAdviseIntegrationValidator() {

	}

	public static ValidationResult validate(IntegrationProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
		if (!result.isValid())
			return result;

		result = TransactionConfigValidator.validate(context.getTransactionConfig(), context.getAmount());
		if (!result.isValid())
			return result;

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
