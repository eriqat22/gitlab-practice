package com.progressoft.mpay.plugins.epay.billadvise;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.Constants;
import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.AmountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;
import com.progressoft.mpay.plugins.validators.WalletCapValidator;

public class BillAdviseValidator {
	private static final Logger logger = LoggerFactory.getLogger(BillAdviseValidator.class);

	private BillAdviseValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = validateSenderInfo(context);
		if (!result.isValid())
			return result;

		result = OriginalRequestsValidator.validate(context);
		if (!result.isValid())
			return result;

		if (context.getSystemParameters().getOfflineModeEnabled()) {
			result = validateReceiver(context);
			if (!result.isValid())
				return result;
		}

		result = TransactionConfigValidator.validate(context.getTransactionConfig(), context.getAmount());
		if (!result.isValid())
			return result;

		result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		return LimitsValidator.validate(context);
	}

	private static ValidationResult validateSenderInfo(MessageProcessingContext context) {
		ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		if (context.getExtraData().get(Constants.CARD_PIN_KEY) == null) {
			result = PinCodeValidator.validate(context, context.getSender().getMobile(), context.getRequest().getPin());
			if (!result.isValid())
				return result;
		}

		result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(),
				context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
		if (!result.isValid())
			return result;

		return SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
	}

	private static ValidationResult validateReceiver(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getServiceAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return WalletCapValidator.validate(context);
	}
}
