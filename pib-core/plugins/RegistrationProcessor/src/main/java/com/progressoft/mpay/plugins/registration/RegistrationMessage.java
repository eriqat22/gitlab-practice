package com.progressoft.mpay.plugins.registration;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RegistrationMessage extends MPayRequest {
    private static final Logger logger = LoggerFactory.getLogger(RegistrationMessage.class);
    private static final String IS_NOT_BENEFICARY = "2";
    private static final String IS_BENEFICARY = "1";

    private static final String FIRST_NAME_KEY = "firstName";
    private static final String MID_NAME_KEY = "midName";
    private static final String LAST_NAME_KEY = "lastName";
    private static final String ID_TYPE_CODE_KEY = "idTypeCode";
    private static final String ID_NUMBER_KEY = "idNumber";
    private static final String DATE_OF_BIRTH_KEY = "dateOfBirth";
    private static final String REFERENCE_KEY = "reference";
    private static final String COUNTRY_CODE_KEY = "countryCode";
    private static final String CITY_CODE_KEY = "cityCode";
    private static final String PREF_LANGUAGE_KEY = "prefLanguage";
    private static final String ALIAS_KEY = "alias";
    private static final String ADDRESS_KEY = "address";
    private static final String IS_BENF_KEY = "isBenef";
    private static final String GENDER = "gender";

    private static final String BENEFICAIARY_NAME_KEY = "benefName";
    private static final String BENEFICAIARY_ID_KEY = "benefId";
    private static final String BENEFICAIARY_ID_TYPE_KEY = "benefIdType";
    private static final String BENEFICAIARY_EMAIL_KEY = "benefEmail";
    private static final String GUARDIAN_NAME_KEY = "guardianName";
    private static final String GUARDIAN_EMAIL_KEY = "guardianEmail";

    private static final String IDENTIFICATION_REFRENCE = "identificationRefrence";
    private static final String IDENTIFICATION_CARD = "identificationCard";
    private static final String IDENTIFICATION_CARD_ISSUANCE_DATE = "identificationCardIssuanceDate";
    private static final String PASSPORT_ID = "passportId";
    private static final String PASSPORT_ISSUANCE_DATE = "passportIssuanceDate";
    private static final String OTP = "otp";
    private static final String NOTE = "note";

    private String firstName;
    private String midName;
    private String lastName;
    private String idTypeCode;
    private String idNumber;
    private Date dateOfBirth;
    private String reference;
    private String countryCode;
    private String cityCode;
    private long prefLanguage;
    private String alias;
    private String address;
    private String otp;
    private String isBeneficiary;
    private String beneficiaryName;
    private String beneficiaryId;
    private String beneficiaryIdType;
    private String beneficiaryEmail;
    private String guardian;
    private String guardianEmail;
    private String identificationrefrence;
    private String identificationcard;
    private Date identificationcardissuancedate;
    private String passportid;
    private Date passportissuancedate;
    private String note;
    private String gender;

    public RegistrationMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static RegistrationMessage parseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        RegistrationMessage message = new RegistrationMessage(request);
        fillGenericFields(message);
        fillCustomerInfo(message);
        message.setReference(message.getValue(REFERENCE_KEY));
        if (message.getReference() == null || message.getReference().trim().length() == 0)
            throw new MessageParsingException(REFERENCE_KEY);
        message.setCountryCode(message.getValue(COUNTRY_CODE_KEY));
        if (message.getCountryCode() == null || message.getCountryCode().trim().length() == 0)
            throw new MessageParsingException(COUNTRY_CODE_KEY);
        message.setCityCode(message.getValue(CITY_CODE_KEY));

        if (message.getCityCode() == null || message.getCityCode().trim().length() == 0)
            throw new MessageParsingException(CITY_CODE_KEY);

        try {
            message.setPrefLanguage(Long.parseLong(message.getValue(PREF_LANGUAGE_KEY)));
        } catch (Exception e) {
            throw new MessageParsingException(PREF_LANGUAGE_KEY);
        }
        message.setAlias(message.getValue(ALIAS_KEY));
        message.setAddress(message.getValue(ADDRESS_KEY));

        message.setOtp(message.getValue(OTP));
        message.setGender(request.getValue(GENDER));
        if (message.getGender() == null || (!message.getGender().equals("1") && !message.getGender().equals("2")))
            throw new MessageParsingException(GENDER);


        checkIfOTPIsEnabled(message);

        checkCustomerIsBeneficaryOrNot(message);
        return message;
    }

    private static void checkCustomerIsBeneficaryOrNot(RegistrationMessage message) throws MessageParsingException {
        String isBenefValue = message.getValue(IS_BENF_KEY);
        if (isBenefValue == null)
            throw new MessageParsingException(IS_BENF_KEY);
        return;
        /*if (isBenefValue.equals(IS_BENEFICARY) || isBenefValue.equals(IS_NOT_BENEFICARY)) {
            if (message.getIsBeneficiary().equals(IS_NOT_BENEFICARY)) {
                if (isMatchedtoEmailRegex(message.getValue(BENEFICAIARY_EMAIL_KEY)))
                    message.setBeneficiaryEmail(message.getValue(BENEFICAIARY_EMAIL_KEY));
                else {
                    throw new MessageParsingException(BENEFICAIARY_EMAIL_KEY);
                }
                message.setBeneficiaryId(message.getValue(BENEFICAIARY_ID_KEY));
                message.setBeneficiaryIdType(message.getValue(BENEFICAIARY_ID_TYPE_KEY));
                message.setBeneficiaryName(message.getValue(BENEFICAIARY_NAME_KEY));
                message.setGuardian(message.getValue(GUARDIAN_NAME_KEY));
                if (isMatchedtoEmailRegex(message.getValue(GUARDIAN_EMAIL_KEY)))
                    message.setGuardianEmail(message.getValue(GUARDIAN_EMAIL_KEY));
                else {
                    throw new MessageParsingException(GUARDIAN_EMAIL_KEY);
                }
            }

        } else {
            throw new MessageParsingException(IS_BENF_KEY + "Not valid");
        }*/
    }

    private static void fillCustomerInfo(RegistrationMessage message) throws MessageParsingException {
        SimpleDateFormat format = new SimpleDateFormat(Constants.BirthDateFormat);

        message.setFirstName(message.getValue(FIRST_NAME_KEY));
        if (StringUtils.isEmpty(message.getFirstName()))
            throw new MessageParsingException(FIRST_NAME_KEY);
        message.setMidName(message.getValue(MID_NAME_KEY));
        if (StringUtils.isEmpty(message.getMidName()))
            throw new MessageParsingException(MID_NAME_KEY);
        message.setLastName(message.getValue(LAST_NAME_KEY));
        if (StringUtils.isEmpty(message.getLastName()))
            throw new MessageParsingException(LAST_NAME_KEY);
        message.setIdTypeCode(message.getValue(ID_TYPE_CODE_KEY));
        if (StringUtils.isEmpty(message.getIdTypeCode()))
            throw new MessageParsingException(ID_TYPE_CODE_KEY);
        message.setIdNumber(message.getValue(ID_NUMBER_KEY));
        if (StringUtils.isEmpty(message.getIdNumber()))
            throw new MessageParsingException(ID_NUMBER_KEY);
        try {
            message.setDateOfBirth(format.parse(message.getValue(DATE_OF_BIRTH_KEY)));
        } catch (Exception e) {
            logger.debug("Invalid Date of Birth format", e);
            throw new MessageParsingException(DATE_OF_BIRTH_KEY);
        }
        message.setIdentificationrefrence(message.getValue(IDENTIFICATION_REFRENCE));
        message.setIdentificationcard(message.getValue(IDENTIFICATION_CARD));
        if (message.getValue(IDENTIFICATION_CARD_ISSUANCE_DATE) != null
                && !message.getValue(PASSPORT_ISSUANCE_DATE).equals(""))
            try {
                message.setIdentificationcardissuancedate(
                        format.parse(message.getValue(IDENTIFICATION_CARD_ISSUANCE_DATE)));
            } catch (Exception e) {
                logger.debug("Invalid Identification card issuance date format", e);
                throw new MessageParsingException(IDENTIFICATION_CARD_ISSUANCE_DATE);
            }
        message.setPassportId((message.getValue(PASSPORT_ID)));
        if (message.getValue(PASSPORT_ISSUANCE_DATE) != null && !message.getValue(PASSPORT_ISSUANCE_DATE).equals(""))
            try {
                message.setPassportissuancedate(format.parse(message.getValue(PASSPORT_ISSUANCE_DATE)));
            } catch (Exception e) {
                logger.debug("Invalid passport issuance date format", e);
                throw new MessageParsingException(PASSPORT_ISSUANCE_DATE);
            }
        message.setNote(message.getValue(NOTE));

    }

    private static void fillGenericFields(RegistrationMessage message) throws MessageParsingException {
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
    }

    public static boolean isMatchedtoEmailRegex(String emailString) {
        return emailString.matches(
                LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.EMAIL_REGEX).getConfigValue());

    }

    public static void checkIfOTPIsEnabled(RegistrationMessage message) throws MessageParsingException {
        if (SystemParameters.getInstance().getEnableRegistrationOTP().equals("1")) {
            if (message.getOtp() == null) {
                throw new MessageParsingException(OTP);
            }
        }
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIdentificationRefrence() {
        return identificationrefrence;
    }

    public String getIdentificationCard() {
        return identificationcard;
    }

    public Date getIdentificationCardIssuanceDate() {
        return identificationcardissuancedate;
    }

    public String getPassportId() {
        return passportid;
    }

    public void setPassportId(String passportId) {
        this.passportid = passportId;
    }

    public Date getPassportIssuanceDate() {
        return passportissuancedate;
    }

    public void setIdentificationrefrence(String identificationrefrence) {
        this.identificationrefrence = identificationrefrence;
    }

    public void setIdentificationcard(String identificationcard) {
        this.identificationcard = identificationcard;
    }

    public void setIdentificationcardissuancedate(Date identificationcardissuancedate) {
        this.identificationcardissuancedate = identificationcardissuancedate;
    }

    public void setPassportid(String passportid) {
        this.passportid = passportid;
    }

    public void setPassportissuancedate(Date passportissuancedate) {
        this.passportissuancedate = passportissuancedate;
    }

    public String getIsBeneficiary() {
        return isBeneficiary;
    }


    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public String getBeneficiaryIdType() {
        return beneficiaryIdType;
    }

    public void setBeneficiaryIdType(String beneficiaryIdType) {
        this.beneficiaryIdType = beneficiaryIdType;
    }

    public String getBeneficiaryEmail() {
        return beneficiaryEmail;
    }

    public void setBeneficiaryEmail(String beneficiaryEmail) {
        this.beneficiaryEmail = beneficiaryEmail;
    }

    public String getGuardian() {
        return guardian;
    }

    public void setGuardian(String guardian) {
        this.guardian = guardian;
    }

    public String getGuardianEmail() {
        return guardianEmail;
    }

    public void setGuardianEmail(String guardianEmail) {
        this.guardianEmail = guardianEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMidName() {
        return midName;
    }

    public void setMidName(String midName) {
        this.midName = midName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdTypeCode() {
        return idTypeCode;
    }

    public void setIdTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public long getPrefLanguage() {
        return prefLanguage;
    }

    public void setPrefLanguage(long prefLanguage) {
        this.prefLanguage = prefLanguage;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
