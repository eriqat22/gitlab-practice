package com.progressoft.mpay.plugins.registration;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.time.DateUtils;

import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_City;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_IDType;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_Profile;
import com.progressoft.mpay.entities.MPAY_RegistrationOTP;
import com.progressoft.mpay.entities.MPAY_SysConfig;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.PluginsCommonConstants;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.MobileNumberValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;

public class RegistrationValidator {
	private RegistrationValidator() {

 	}

	public static ValidationResult validate(MessageProcessingContext context) {
		if (context == null)
			throw new NullArgumentException("context");

		RegistrationMessage message = (RegistrationMessage) context.getRequest();

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = MobileNumberValidator.validate(context, message.getSender());
		if (!result.isValid())
			return result;

		result = validateBank(context);
		if (!result.isValid())
			return result;

		result = validateBasicInfo(context, message);
		if (!result.isValid())
			return result;

		result = validateRegisteredMobile(context, message);
		if (!result.isValid())
			return result;

		result = validateRegisteredCustomer(context, message);
		if (!result.isValid())
			return result;

		result = validateAlias(context, message);
		if (!result.isValid())
			return result;

		result = validateRegOTP(context);
		if (!result.isValid())
			return result;

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateRegOTP(MessageProcessingContext context) {
		String otp = (String) context.getExtraData().get(PluginsCommonConstants.OTP_KEY);
		if ((otp == null || otp.isEmpty()) && SystemParameters.getInstance().getEnableRegistrationOTP().equals("0"))
			return new ValidationResult(ReasonCodes.VALID, null, true);
		MPAY_RegistrationOTP regOtpByCode = DataProvider.instance().getRegOtpByCode(otp);
		if (regOtpByCode == null)
			return new ValidationResult(ReasonCodes.INVALID_OTP, null, false);

		String mobileNumber = context.getMessage().getSender();
		if (!regOtpByCode.getMobile().equals(mobileNumber))
			return new ValidationResult(ReasonCodes.INVALID_MOBILE_NUMBER, null, false);

		if (isExpiredOtp(regOtpByCode, SystemParameters.getInstance()))
			return new ValidationResult(ReasonCodes.EXPIRED_OTP, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateBasicInfo(MessageProcessingContext context, RegistrationMessage message) {
		ValidationResult result = validateIdType(context);
		if (!result.isValid())
			return result;

		result = validateDateOfBirth(context, message);
		if (!result.isValid())
			return result;

		result = validateCountry(context);
		if (!result.isValid())
			return result;

		result = validateCity(context);
		if (!result.isValid())
			return result;

		result = validateLanguage(context);
		if (!result.isValid())
			return result;

		return validateProfile(context);
	}

	private static ValidationResult validateDateOfBirth(ProcessingContext context, RegistrationMessage message) {
		Calendar now = Calendar.getInstance();

		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		MPAY_SysConfig r = context.getLookupsLoader().getSystemConfigurations("Min Age");
		long minAge = Integer.parseInt(r.getConfigValue());

		if (DateUtils.addYears(message.getDateOfBirth(), (int) minAge).after(now.getTime()))
			return new ValidationResult(ReasonCodes.CUSTOMER_AGE_NOT_ALLOWED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateIdType(MessageProcessingContext context) {
		MPAY_IDType idType = (MPAY_IDType) context.getExtraData().get(Constants.IdTypeKey);
		if (idType == null || !idType.getIsCustomer())
			return new ValidationResult(ReasonCodes.INVALID_ID_TYPE, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateCountry(MessageProcessingContext context) {
		JfwCountry country = (JfwCountry) context.getExtraData().get(Constants.CountryKey);
		if (country == null)
			return new ValidationResult(ReasonCodes.INVALID_NATIONALITY, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateCity(MessageProcessingContext context) {
		MPAY_City city = (MPAY_City) context.getExtraData().get(Constants.CityKey);
		if (city == null)
			return new ValidationResult(ReasonCodes.CITY_NOT_DEFINED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateLanguage(MessageProcessingContext context) {
		MPAY_Language language = (MPAY_Language) context.getExtraData().get(Constants.LanguageKey);
		if (language == null)
			return new ValidationResult(ReasonCodes.LANGUAGE_NOT_DEFINED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateRegisteredMobile(ProcessingContext context, RegistrationMessage message) {
		MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(message.getSender());
		if (mobile == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		if (mobile.getDeletedFlag() == null || !mobile.getDeletedFlag())
			return new ValidationResult(ReasonCodes.MOBILE_ALREADY_REGISTERED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateRegisteredCustomer(ProcessingContext context, RegistrationMessage message) {
		MPAY_Customer customer = context.getDataProvider().getCustomer(message.getIdTypeCode(), message.getIdNumber());
		if (customer == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		if (customer.getDeletedFlag() == null || !customer.getDeletedFlag())
			return new ValidationResult(ReasonCodes.CUSTOMER_ALREADY_REGISTERED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateAlias(ProcessingContext context, RegistrationMessage message) {
		if (message.getAlias() == null)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(message.getAlias(),
				ReceiverInfoType.ALIAS);
		if (mobile != null)
			return new ValidationResult(ReasonCodes.ALIAS_IS_DEFINED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateBank(MessageProcessingContext context) {
		if (context.getSender().getBank() == null)
			return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
		if (context.getSender().getBank().getDeletedFlag() != null && context.getSender().getBank().getDeletedFlag())
			return new ValidationResult(ReasonCodes.BANK_NOT_DEFINED, null, false);
		if (!context.getSender().getBank().getIsActive())
			return new ValidationResult(ReasonCodes.BANK_INACTIVE, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateProfile(MessageProcessingContext context) {
		MPAY_Profile profile = (MPAY_Profile) context.getExtraData().get(Constants.ProfileKey);
		if (profile == null)
			return new ValidationResult(ReasonCodes.PROFILE_NOT_DEFINED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static boolean isExpiredOtp(MPAY_RegistrationOTP regOtp, ISystemParameters systemParameters) {
		long hourInMellSec = (long) systemParameters.getOtpValidityInMinutes() * 60 * 1000;
		Timestamp requestTime = regOtp.getRequestTime();
		requestTime.setTime(requestTime.getTime() + hourInMellSec);
		if (requestTime.before(new Timestamp(new Date().getTime()))) {
			return true;
		}
		return false;
	}
}
