package com.progressoft.mpay.plugins.resetpin.validators;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.resetpin.Constants;
import com.progressoft.mpay.plugins.resetpin.ResetPinMessage;

public class PasswordValidator {
	private static final Logger logger = LoggerFactory.getLogger(PasswordValidator.class);

	private PasswordValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context, boolean isCustomer) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		if (!context.getExtraData().containsKey(Constants.DEVICE_KEY))
			new ValidationResult(ReasonCodes.VALID, null, true);
		if (isCustomer) {
			MPAY_CustomerDevice device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DEVICE_KEY);
			if (device.getIsBlocked())
				return new ValidationResult(ReasonCodes.DEVICE_BLOCKED, null, false);
			ResetPinMessage message = (ResetPinMessage) context.getRequest();
			boolean isValid = device.getPassword().equals(message.getPassword());
			handleDeviceSignIn(context, device, isValid);
			if (!isValid)
				return new ValidationResult(ReasonCodes.INVALID_PASSKEY, null, false);
		} else {
			MPAY_CorporateDevice device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DEVICE_KEY);
			if (device.getIsBlocked())
				return new ValidationResult(ReasonCodes.DEVICE_BLOCKED, null, false);
			ResetPinMessage message = (ResetPinMessage) context.getRequest();
			boolean isValid = device.getPassword().equals(message.getPassword());
			handleDeviceSignIn(context, device, isValid);
			if (!isValid)
				return new ValidationResult(ReasonCodes.INVALID_PASSKEY, null, false);
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static void handleDeviceSignIn(ProcessingContext context, MPAY_CustomerDevice device, boolean isValid) {
		if (isValid) {
			device.setRetryCount(0l);
		} else {
			device.setRetryCount(device.getRetryCount() + 1);
			if (device.getRetryCount() >= context.getSystemParameters().getPasswordMaxRetryCount()) {
				device.setIsBlocked(true);
			}
		}
		context.getDataProvider().mergeCustomerDevice(device);
	}

	private static void handleDeviceSignIn(ProcessingContext context, MPAY_CorporateDevice device, boolean isValid) {
		if (isValid) {
			device.setRetryCount(0l);
		} else {
			device.setRetryCount(device.getRetryCount() + 1);
			if (device.getRetryCount() >= context.getSystemParameters().getPasswordMaxRetryCount()) {
				device.setIsBlocked(true);
			}
		}
		context.getDataProvider().mergeCorporateDevice(device);
	}
}
