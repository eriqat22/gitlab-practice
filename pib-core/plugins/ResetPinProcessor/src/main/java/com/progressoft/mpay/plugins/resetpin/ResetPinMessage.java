package com.progressoft.mpay.plugins.resetpin;

import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class ResetPinMessage extends MPayRequest {
	private static final String PASS_WORD_KEY = "pass";

	private String password;

	public ResetPinMessage() {
		//
	}

	public ResetPinMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static ResetPinMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		ResetPinMessage message = new ResetPinMessage(request);
		if (StringUtils.isEmpty(message.getSender()))
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		if(StringUtils.isEmpty(message.getPin()))
			throw new MessageParsingException(MPayRequest.PIN_CODE_KEY);
		message.setPassword(message.getValue(PASS_WORD_KEY));
		if (message.getPassword() == null)
			throw new MessageParsingException(PASS_WORD_KEY);
		return message;
	}
}
