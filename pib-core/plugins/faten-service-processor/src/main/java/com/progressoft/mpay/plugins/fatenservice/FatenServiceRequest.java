package com.progressoft.mpay.plugins.fatenservice;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

import java.math.BigDecimal;

public class FatenServiceRequest extends MPayRequest {
    private static final String RECEIVER_INFO_KEY = "rcvId";
    private static final String RECEIVER_TYPE_KEY = "rcvType";
    private static final String NOTES_KEY = "data";
    private static final String AMOUNT_KEY = "amnt";
    private static final String BORROWER_NATIONAL_ID_KEY="borrowerNationalId";
    private static final String BORROWER_NAME_KEY="borrowerName";

    private String receiverInfo;
    private String receiverType;
    private String notes;
    private BigDecimal amount;
    private String borrowerNationalId;
    private String borrowerName;


    public FatenServiceRequest(MPayRequest request)
    {
        if(request == null)
            throw new NullArgumentException("request");

        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static FatenServiceRequest parseMessage(MPayRequest request) throws MessageParsingException {
        FatenServiceRequest messageRequest = new FatenServiceRequest(request);

        if (messageRequest.getSender() == null || messageRequest.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (messageRequest.getSenderType() == null || !messageRequest.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        messageRequest.setReceiverInfo(messageRequest.getValue(RECEIVER_INFO_KEY));
        if (messageRequest.getReceiverInfo() == null || messageRequest.getReceiverInfo().length() ==0)
            throw new MessageParsingException(RECEIVER_INFO_KEY);

        messageRequest.setReceiverType(messageRequest.getValue(RECEIVER_TYPE_KEY));
        if (messageRequest.getReceiverType() == null || !messageRequest.getReceiverType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(RECEIVER_TYPE_KEY);

        String amount = messageRequest.getValue(AMOUNT_KEY);
        if (amount == null)
            throw new MessageParsingException(AMOUNT_KEY);
        try {
            messageRequest.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
        } catch (NumberFormatException ex) {
            throw new MessageParsingException(AMOUNT_KEY);
        }

        messageRequest.setNotes(messageRequest.getValue(NOTES_KEY));
        messageRequest.setBorrowerNationalId(messageRequest.getValue(BORROWER_NATIONAL_ID_KEY));
        messageRequest.setBorrowerName(messageRequest.getValue(BORROWER_NAME_KEY));

        return messageRequest;
    }

    public String getReceiverInfo() {
        return receiverInfo;
    }

    public void setReceiverInfo(String receiverInfo) {
        this.receiverInfo = receiverInfo;
    }

    @Override
    public String getReceiverType() {
        return receiverType;
    }

    @Override
    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    @Override
    public String getNotes() {
        return notes;
    }

    @Override
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBorrowerNationalId() {
        return borrowerNationalId;
    }

    public void setBorrowerNationalId(String borrowerNationalId) {
        this.borrowerNationalId = borrowerNationalId;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }
}

