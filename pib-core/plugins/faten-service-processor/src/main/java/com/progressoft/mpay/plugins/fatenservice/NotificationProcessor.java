package com.progressoft.mpay.plugins.fatenservice;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateAcceptanceNotificationMessages ...");
		try {
			String receiver;
			String sender;
			String reference;
			long senderLanguageId = 0;
			long receiverLanguageId = 0;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();
			MPAY_CustomerMobile senderMobile = context.getSender().getMobile();
			MPAY_CorpoarteService receiverService = context.getReceiver().getService();
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

			FatenServiceNotificationContext notificationContext = new FatenServiceNotificationContext();
			notificationContext
					.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);
			if (senderMobile != null) {
				sender = senderMobile.getMobileNumber();
				context.getDataProvider().refreshEntity(context.getSender().getAccount());
				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getSender().getAccount().getBalance()));
				notificationContext.setSenderBanked(senderMobile.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setSenderCharges(
						SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
				if (context.getSender().getCustomer() != null)
					senderLanguageId = context.getSender().getCustomer().getPrefLang().getId();
				else
					senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
				if (senderMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS)
						&& senderMobile.getAlias() != null && !senderMobile.getAlias().isEmpty())
					sender = senderMobile.getAlias();
			} else
				sender = context.getSender().getInfo();

			if (receiverService != null) {
				receiver = receiverService.getName();
				context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getReceiver().getAccount().getBalance()));
				notificationContext
						.setReceiverBanked(receiverService.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setReceiverCharges(
						SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
				receiverLanguageId = context.getReceiver().getCorporate().getPrefLang().getId();
			} else
				receiver = context.getReceiver().getInfo();

			notificationContext.setReceiver(receiver);
			notificationContext.setSender(sender);
			MPAY_Notification notification = null;
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (senderMobile != null) {
				notification = NotificationHelper.getInstance().createNotification(context.getMessage(),
						notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
						senderMobile.getMobileNumber(), NotificationChannelsCode.SMS);
				if (notification != null)
					notifications.add(notification);
			}
			if (receiverService != null) {
				notification = NotificationHelper.getInstance().createNotification(context.getMessage(),
						notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId,
						receiverService.getName(), NotificationChannelsCode.EMAIL);
				if (notification != null)
					notifications.add(notification);
			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			FatenServiceRequest request = FatenServiceRequest
					.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String senderMobile = request.getSender();
			MPAY_Language language = context.getSystemParameters().getSystemLanguage();
			MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
			if (mobile != null) {
				senderMobile = mobile.getMobileNumber();
				language = mobile.getRefCustomer().getPrefLang();
			}
			FatenServiceNotificationContext notificationContext = new FatenServiceNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext
					.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
			notificationContext.setReceiver(request.getReceiverInfo());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(
					MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
			notifications
					.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext,
							NotificationType.REJECTED_SENDER, context.getMessage().getRefOperation().getId(),
							language.getId(), senderMobile, NotificationChannelsCode.SMS));
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}
}
