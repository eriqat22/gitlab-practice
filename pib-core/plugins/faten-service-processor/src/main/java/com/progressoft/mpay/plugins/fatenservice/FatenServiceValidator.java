package com.progressoft.mpay.plugins.fatenservice;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FatenServiceValidator {
    private static final Logger logger = LoggerFactory.getLogger(FatenServiceValidator.class);

    private FatenServiceValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {

        logger.debug("Inside validate ...");
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
        if (!result.isValid())
            return result;

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        FatenServiceRequest request = (FatenServiceRequest) context.getRequest();

        result = validateSenderInfo(context, request);
        if (!result.isValid())
            return result;

        result = validateReceiver(context);
        if (!result.isValid())
            return result;

        result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
        if (!result.isValid())
            return result;

        result = AmountValidator.validate(context);
        if (!result.isValid())
            return result;

        result = LimitsValidator.validate(context);
        if (!result.isValid())
            return result;

        return new ValidationResult(ReasonCodes.VALID,null,true);
    }

    private static ValidationResult validateSenderInfo(MessageProcessingContext context, FatenServiceRequest request) {
        logger.debug("Inside validateSenderInfo ...");

        ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
        if (!result.isValid())
            return result;

        result = CustomerValidator.validate(context.getSender().getCustomer(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        return PinCodeValidator.validate(context, context.getSender().getMobile(), request.getPin());
    }

    private static ValidationResult validateReceiver(MessageProcessingContext context) {
        logger.debug("Inside ValidateReceiver ...");

        ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getReceiver().getServiceAccount(), false,context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        return WalletCapValidator.validate(context);
    }

}
