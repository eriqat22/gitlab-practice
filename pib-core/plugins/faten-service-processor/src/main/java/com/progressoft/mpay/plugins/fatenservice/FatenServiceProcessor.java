package com.progressoft.mpay.plugins.fatenservice;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entities.MPAY_Installment;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.transactions.AccountingHelper;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class FatenServiceProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(FatenServiceProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if(context == null)
            throw new NullArgumentException("context");
        try {
                preProcessMessage(context);
                ValidationResult validationResult = FatenServiceValidator.validate(context);
                if (validationResult.isValid())
                    return acceptMessage(context);
                else
                    return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                            validationResult.getReasonDescription());
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }


    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, SQLException {
        FatenServiceRequest request = FatenServiceRequest.parseMessage(context.getRequest());

        context.setRequest(request);
        context.setSender(loadSenderInfo(context,request));
        context.setReceiver(loadReceiverInfo(context,request));
        context.setAmount(request.getAmount());
        context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        if(!isSenderAndReceiverValid(context))
            return;

        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        context.setDirection(TransactionDirection.ONUS);
        context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
                context.getSender().isBanked(),
                context.getReceiver().getCorporate().getClientType().getId(),
                context.getReceiver().isBanked(),
                context.getOperation().getMessageType().getId(),
                context.getTransactionNature(),
                context.getOperation().getId()));
    }

    private boolean isSenderAndReceiverValid(MessageProcessingContext context) {
        return context.getSender().getMobile()!=null && context.getSender().getMobileAccount()!=null &&
               context.getReceiver().getService()!=null && context.getReceiver().getServiceAccount()!=null;
    }

    private ProcessingContextSide loadReceiverInfo(MessageProcessingContext context,FatenServiceRequest request) {
        ProcessingContextSide receiver = new ProcessingContextSide();
        receiver.setInfo(request.getReceiverInfo());
        receiver.setService(context.getDataProvider().getCorporateService(request.getReceiverInfo(),request.getReceiverType()));
        if(receiver.getService()==null)return receiver;
        receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(),receiver.getService(),request.getReceiverAccount()));
        if(receiver.getServiceAccount()==null) return  receiver;

        receiver.setCorporate(receiver.getService().getRefCorporate());
        receiver.setAccount(receiver.getServiceAccount().getRefAccount());
        receiver.setBank(receiver.getServiceAccount().getBank());
        receiver.setProfile(receiver.getServiceAccount().getRefProfile());
        receiver.setBanked(receiver.getAccount().getIsBanked());
        receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

        return receiver;
    }

    private ProcessingContextSide loadSenderInfo(MessageProcessingContext context,FatenServiceRequest request) throws SQLException {
        ProcessingContextSide sender = new ProcessingContextSide();
        sender.setInfo(request.getSender());
        sender.setMobile(context.getDataProvider().getCustomerMobile(request.getSender()));
        if(sender.getMobile() == null) return sender;
        sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(),sender.getMobile(),request.getSenderAccount()));
        if(sender.getMobileAccount() == null) return sender;

        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setAccount(sender.getMobileAccount().getRefAccount());
        sender.setBank(sender.getMobileAccount().getBank());
        sender.setProfile(sender.getMobileAccount().getRefProfile());
        sender.setBanked(sender.getAccount().getIsBanked());
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getMobile().getId(),
                context.getOperation().getMessageType().getId()));
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

        return sender;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException {

        logger.debug("Inside AcceptMessage ...");
        FatenServiceRequest request = (FatenServiceRequest) context.getRequest();
        MessageProcessingResult result = new MessageProcessingResult();
        MPAY_ProcessingStatus status;
        MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
        String reasonDescription = null;

        context.getMessage().setReason(reason);
        MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT,
                BigDecimal.ZERO, request.getNotes());
        result.setMessage(context.getMessage());
        context.setTransaction(transaction);
        result.setTransaction(transaction);
        context.getDataProvider().persistProcessingResult(result);
        JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(),
                context.getLookupsLoader(), transaction);
        if (postingResult.isSuccess()) {
            BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
            if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
            } else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
                reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
                reasonDescription = integResult.getReasonDescription();
                TransactionHelper.reverseTransaction(context);
            } else {
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
                reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
                reasonDescription = integResult.getReasonDescription();
                TransactionHelper.reverseTransaction(context);
            }
        } else {
            if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
                context.getMessage().setReasonDesc(postingResult.getReason());
            } else
                reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
        }
        context.getMessage().setProcessingStatus(status);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
        transaction.setReason(context.getMessage().getReason());
        transaction.setReasonDesc(context.getMessage().getReasonDesc());
        result.setStatus(status);
        result.setStatusDescription(context.getMessage().getReasonDesc());
        handleNotifications(context, result, status);
        OTPHanlder.removeOTP(context);
        handleCommissions(context, status);
        handleLimits(context, status, transaction);
        persistInstallmentData(context, request,status);

        return result;
    }

    private void persistInstallmentData(MessageProcessingContext context, FatenServiceRequest request,MPAY_ProcessingStatus status) {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)) {
            MPAY_Installment installmentData = new MPAY_Installment();
            installmentData.setCustomerName(context.getSender().getCustomer().getFullName());
            installmentData.setCustNationalId(context.getSender().getCustomer().getIdNum());
            installmentData.setBorrowerName(request.getBorrowerName());
            installmentData.setBorrowerNationalId(request.getBorrowerNationalId());
            installmentData.setReceiverName(request.getReceiverInfo());
            installmentData.setAmount(context.getAmount());
            installmentData.setCurrency(context.getLookupsLoader().getDefaultCurrency());
            installmentData.setTranDate(context.getTransaction().getTransDate());
            installmentData.setTranReference(context.getTransaction().getReference());

            context.getDataProvider().persistInstallment(installmentData);
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode,
                                                  String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        if (context.getTransaction() != null) {
            BankIntegrationResult result = BankIntegrationHandler.reverse(context);
            if (result != null) {
                TransactionHelper.reverseTransaction(context);
                try {
                    FatenServiceRequest message = (FatenServiceRequest) context.getRequest();
                    context.getDataProvider()
                            .updateAccountLimit(
                                    SystemHelper
                                            .getMobileAccount(context.getSystemParameters(),
                                                    context.getSender().getMobile(), message.getSenderAccount())
                                            .getRefAccount().getId(),
                                    context.getMessage().getRefOperation().getMessageType().getId(),
                                    context.getTransaction().getTotalAmount().negate(), -1,
                                    SystemHelper.getCurrentDateWithoutTime());
                } catch (Exception e) {
                    logger.error("Inside RejectMessage Reverse Limits", e);
                }
            }
        }
        return MessageProcessingResult.create(context, status, reason, reasonDescription);
    }

    private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
        if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            return;
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.OUTWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
                    CommissionDirections.SENDER);
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.INWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
                    CommissionDirections.RECEIVER);
    }

    private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result, MPAY_ProcessingStatus status) {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
        else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
            result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
    }

    private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status,
                              MPAY_Transaction transaction) throws SQLException {
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)
                || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
            context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
                    context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
                    SystemHelper.getCurrentDateWithoutTime());
        }
    }
}
