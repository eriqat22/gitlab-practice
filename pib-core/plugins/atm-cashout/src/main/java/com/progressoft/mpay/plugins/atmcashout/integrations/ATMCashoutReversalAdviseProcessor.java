package com.progressoft.mpay.plugins.atmcashout.integrations;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATMCashoutReversalAdviseProcessor extends IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(ATMCashoutReversalAdviseProcessor.class);
	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

}
