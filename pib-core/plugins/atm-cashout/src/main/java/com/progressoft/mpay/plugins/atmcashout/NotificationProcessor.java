package com.progressoft.mpay.plugins.atmcashout;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    public static List<MPAY_Notification> CreateAcceptanceNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateAcceptanceNotificationMessages ...");
        try {
            String receiver = null;
            String reference = null;
            long receiverLanguageId = 0;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();

            MPAY_CustomerMobile receiverMobile = null;
            MPAY_Account receiverAccount = null;

            if (context.getReceiver() != null) {
                receiverMobile = context.getReceiver().getMobile();
                receiverAccount = context.getReceiver().getMobileAccount().getRefAccount();
            }
            context.getDataProvider().refreshEntity(receiverAccount);
            MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();
            ATMCashoutNotificationContext notificationContext = new ATMCashoutNotificationContext();
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setOperation(operation.getOperation());
            notificationContext.setReference(reference);
            if (receiverMobile != null) {
                receiver = receiverMobile.getMobileNumber();

                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
                notificationContext.setReceiverBanked(receiverAccount.getIsBanked());
                notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
                if (receiverMobile.getRefCustomer() != null)
                    receiverLanguageId = receiverMobile.getRefCustomer().getPrefLang().getId();
                else
                    receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
                if (receiverMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS)
                        && receiverMobile.getAlias() != null
                        && !receiverMobile.getAlias().isEmpty())
                    receiver = receiverMobile.getAlias();
            }

            notificationContext.setReceiver(receiver);
            List<MPAY_Notification> notifications = new ArrayList<MPAY_Notification>();
            notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId, receiverMobile.getMobileNumber(), NotificationChannelsCode.SMS));
            return notifications;

        } catch (Exception e) {
            logger.error("Error when CreateAcceptanceNotificationMessages", e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private static void HandlePushNotifications(ATMCashoutNotificationContext notificationContext, ProcessingContext context, MPAY_EndPointOperation operation, MPAY_CorpoarteService receiverService, long receiverLanguageId, List<MPAY_Notification> notifications) throws Exception {
        List<String> tokens = (List<String>) context.getExtraData().get(ATMCashoutProcessor.NOTIFICATION_TOKEN);
        if (tokens == null)
            return;
        for (String token : tokens) {
            notificationContext.setExtraData1(token);
            notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId, receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
        }
    }

    @SuppressWarnings("unchecked")
    public static List<MPAY_Notification> CreateRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<MPAY_Notification>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            String notificationChannel = NotificationChannelsCode.SMS;
            ATMCashoutMessage request = ATMCashoutMessage.ParseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            String sender = request.getSender();
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();
            if (request.getSenderType().equals(ReceiverInfoType.MOBILE)) {
                MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
                if (mobile != null) {
                    language = mobile.getRefCustomer().getPrefLang();
                }
            } else {
                notificationChannel = NotificationChannelsCode.EMAIL;
                MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
                if (service != null) {
                    language = service.getRefCorporate().getPrefLang();
                }
            }

            ATMCashoutNotificationContext notificationContext = new ATMCashoutNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            if (context.getTransaction() == null)
                notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
            else
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
            notificationContext.setReceiver(request.getReceiverInfo());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
            List<String> tokens = (List<String>) context.getExtraData().get(ATMCashoutProcessor.NOTIFICATION_TOKEN);
            if (tokens != null && tokens.size() > 0)
                notificationContext.setExtraData1(tokens.get(0));

            notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_SENDER, context.getMessage().getRefOperation().getId(), language.getId(), sender, notificationChannel));
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateRejectionNotificationMessages", e);
            return null;
        }
    }
}
