package com.progressoft.mpay.plugins.atmcashout.integrations;

import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.NotSupportedException;

public class IntegrationProcessorFactory {
    private static final Logger logger = LoggerFactory.getLogger(IntegrationProcessorFactory.class);

    public static IntegrationProcessor CreateProcessor(int messageType) throws NotSupportedException {
        logger.debug("Inside CreateProcessor ...");
        logger.debug("messageType: " + messageType);
        if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)
            return new ATMCashoutInwardProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE)
            return new ATMCashoutResponseProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_RESPONSE)
            return new ATMCashoutOfflineResponseProcessor();
        else if (messageType == MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST)
            return new ATMCashoutReversalAdviseProcessor();
        else
            throw new NotSupportedException("messageType == " + messageType);
    }
}
