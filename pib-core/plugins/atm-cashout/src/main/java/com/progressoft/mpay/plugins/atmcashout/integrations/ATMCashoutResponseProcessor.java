package com.progressoft.mpay.plugins.atmcashout.integrations;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.atmcashout.ATMCashoutMessage;
import com.progressoft.mpay.plugins.atmcashout.NotificationProcessor;
import com.solab.iso8583.IsoMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class ATMCashoutResponseProcessor extends IntegrationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(ATMCashoutResponseProcessor.class);

    @Override
    public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
        try {
            logger.debug("Inside ProcessIntegration ...");
            PreProcessIntegration(context);
            if (context.getMpClearIsoMessage().getField(44).getValue().equals("99"))
                return null;
            if (context.getMpClearIsoMessage().getField(44).getValue().equals("00"))
                return Accept(context);
            else
                return Reject(context);
        } catch (Exception ex) {
            logger.error("Error in ProcessIntegration", ex);
            return Reject(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private void PreProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
        logger.debug("Inside PreProcessIntegration ...");
        if (context.getMessage() != null)
            context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
        if (GetTransactionType(context.getMpClearIsoMessage()).equals(TransactionTypeCodes.DIRECT_CREDIT))
            PreProcessIntegrationCredit(context);
        else
            PreProcessIntegrationDebit(context);
    }

    private String GetTransactionCodeType(String processingCode) {
        logger.debug("Inside GetTransactionCodeType....");
        logger.debug("processingCode: " + processingCode);
        if (processingCode == null || processingCode.trim().isEmpty())
            return TransactionTypeCodes.DIRECT_CREDIT;
        try {
            Integer value = Integer.valueOf(processingCode);
            if (value >= ProcessingCodeRanges.DEBIT_START_VALUE && value < ProcessingCodeRanges.DEBIT_END_VALUE)
                return TransactionTypeCodes.DIRECT_DEBIT;
            else if (value >= ProcessingCodeRanges.CREDIT_START_VALUE && value < ProcessingCodeRanges.CREDIT_END_VALUE)
                return TransactionTypeCodes.DIRECT_CREDIT;
            else {
                return null;
            }
        } catch (Exception ex) {
            return null;
        }
    }

    private String GetTransactionType(IsoMessage message) {
        if (message.getField(MPClearCommonFields.PROCESSING_CODE) == null)
            return TransactionTypeCodes.DIRECT_CREDIT;
        return GetTransactionCodeType(message.getField(MPClearCommonFields.PROCESSING_CODE).getValue().toString());
    }

    private void PreProcessIntegrationCredit(IntegrationProcessingContext context) throws WorkflowException {

        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        ATMCashoutMessage request = null;
        try {
            request = ATMCashoutMessage.ParseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
        } catch (MessageParsingException ex) {
            logger.error("Error while parsing message", ex);
            throw new WorkflowException(ex);
        }
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setAmount(request.getAmount());
        sender.setMobile(context.getTransaction().getSenderMobile());
        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setMobileAccount(context.getTransaction().getSenderMobileAccount());
        sender.setAccount(sender.getMobileAccount().getRefAccount());
        sender.setBanked(sender.getAccount().getIsBanked());
        sender.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
                sender.getAccount().getAccNumber()));
        sender.setInfo(sender.getMobile().getMobileNumber());
        context.setOperation(context.getMessage().getRefOperation());
        receiver.setMobile(context.getTransaction().getReceiverMobile());
        context.setDirection(context.getTransaction().getDirection().getId());
        receiver.setService(context.getTransaction().getReceiverService());
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        if (receiver.getService() == null) {
            context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
                    sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
                    context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT,
                    context.getOperation().getId()));
            context.getReceiver().setInfo(request.getReceiverInfo());
            return;
        }
        receiver.setServiceAccount(context.getTransaction().getReceiverServiceAccount());
        receiver.setAccount(receiver.getServiceAccount().getRefAccount());
        receiver.setCorporate(receiver.getService().getRefCorporate());
        receiver.setBank(receiver.getServiceAccount().getBank());
        receiver.setBanked(receiver.getAccount().getIsBanked());
        receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
                receiver.getAccount().getAccNumber()));
        context.setDirection(context.getTransaction().getDirection().getId());
        context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
                sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(),
                context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT,
                context.getOperation().getId()));
    }

    private void PreProcessIntegrationDebit(IntegrationProcessingContext context) throws WorkflowException {
        logger.debug("Inside PreProcessIntegration ...");
        if (context.getMessage() != null)
            context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        ATMCashoutMessage request = null;
        try {
            request = ATMCashoutMessage.ParseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
        } catch (MessageParsingException ex) {
            logger.error("Error while parsing message", ex);
            throw new WorkflowException(ex);
        }
        if (context.getTransaction().getSenderMobile() == null && context.getTransaction().getSenderService() != null) {
            context.setTransactionNature(TransactionTypeCodes.DIRECT_DEBIT);
        } else if (context.getTransaction().getSenderMobile() != null
                && context.getTransaction().getSenderService() == null) {
            context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        } else
            throw new WorkflowException(new InvalidOperationException("Invalid Transaction sender and receiver"));
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setAmount(request.getAmount());
        sender.setService(context.getTransaction().getSenderService());
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(context.getTransaction().getSenderServiceAccount());
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        sender.setBanked(sender.getService().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
        sender.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
                sender.getAccount().getAccNumber()));
        sender.setInfo(sender.getService().getName());
        context.setOperation(context.getMessage().getRefOperation());
        receiver.setMobile(context.getTransaction().getReceiverMobile());
        context.setDirection(context.getTransaction().getDirection().getId());
        receiver.setMobile(context.getTransaction().getReceiverMobile());
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        if (receiver.getMobile() == null) {
            context.setTransactionConfig(
                    context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(),
                            sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
                            context.getOperation().getMessageType().getId(), context.getTransactionNature(),
                            context.getOperation().getId()));
            context.getReceiver().setInfo(request.getReceiverInfo());
            return;
        }
        receiver.setMobileAccount(context.getTransaction().getReceiverMobileAccount());
        receiver.setAccount(receiver.getMobileAccount().getRefAccount());
        receiver.setCustomer(receiver.getMobile().getRefCustomer());
        receiver.setBank(receiver.getMobile().getBank());
        receiver.setBanked(receiver.getMobile().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
        receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
                receiver.getAccount().getAccNumber()));
        context.setDirection(context.getTransaction().getDirection().getId());
        context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(
                sender.getCorporate().getClientType().getId(), sender.isBanked(), personClientType, receiver.isBanked(),
                context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_DEBIT,
                context.getOperation().getId()));
    }

    private IntegrationProcessingResult Accept(IntegrationProcessingContext context) {
        logger.debug("Inside Accept ...");
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED,
                ReasonCodes.VALID, null, null, false);
        result.setNotifications(NotificationProcessor.CreateAcceptanceNotificationMessages(context));
        return result;
    }

    private IntegrationProcessingResult Reject(IntegrationProcessingContext context) throws SQLException {
        logger.debug("Inside Reject ...");
        BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
        if (integrationResult != null)
            TransactionHelper.reverseTransaction(context.getTransaction(), context);
        String reasonDescription = null;
        if (context.getMpClearIsoMessage().getField(47) != null)
            reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED,
                ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
                context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
        context.setTransactionNature(context.getTransactionConfig().getRefType().getCode());
        ReverseLimits(context);
        result.setNotifications(NotificationProcessor.CreateRejectionNotificationMessages(context));
        return result;
    }

    private IntegrationProcessingResult Reject(IntegrationProcessingContext context, String processingCode,
                                               String reasonCode, String reasonDescription) {
        logger.debug("Inside Reject ...");
        BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
        if (integrationResult != null)
            TransactionHelper.reverseTransaction(context.getTransaction(), context);
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, processingCode, reasonCode,
                reasonDescription, null, false);
        try {
            ReverseLimits(context);
        } catch (SQLException e) {
            logger.error("Failed to reverse limits", e);
        }
        result.setNotifications(NotificationProcessor.CreateRejectionNotificationMessages(context));
        return result;
    }

    private void ReverseLimits(ProcessingContext context) throws SQLException {
        if (context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_CREDIT)) {
            context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
                    context.getTransactionConfig().getMessageType().getId(),
                    BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1,
                    SystemHelper.getCurrentDateWithoutTime());
        } else {
            context.getDataProvider().updateAccountLimit(context.getReceiver().getAccount().getId(),
                    context.getTransactionConfig().getMessageType().getId(),
                    BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1,
                    SystemHelper.getCurrentDateWithoutTime());
        }
    }
}
