package com.progressoft.mpay.plugins.atmcashout.integrations;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.atmcashout.ATMCashoutConstants;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATMCashoutIntegrationValidator {
	private static final Logger logger = LoggerFactory.getLogger(ATMCashoutIntegrationValidator.class);

	public static ValidationResult validate(IntegrationProcessingContext context) {
		logger.debug("Inside validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = new ValidationResult(ReasonCodes.VALID, null, true);

		if (context.getTransactionNature() == null)
			return new ValidationResult(ReasonCodes.UNSUPPORTED_REQUEST, null, false);

		if (context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_DEBIT)) {
			result = MobileValidator.validate(context.getReceiver().getMobile(), false);
			if (!result.isValid())
				return result;

			result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
			if (!result.isValid())
				return result;

			result = PinCodeValidator.validate(context, context.getReceiver().getMobile()
					, (String)context.getExtraData().get(ATMCashoutConstants.PIN));
			if (!result.isValid())
				return result;
		} else {
			result = ServiceValidator.validate(context.getReceiver().getService(), false);
			if (!result.isValid())
				return result;

			result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
			if (!result.isValid())
				return result;
		}
		return TransactionConfigValidator.validate(context.getTransactionConfig(), context.getAmount());
	}
}
