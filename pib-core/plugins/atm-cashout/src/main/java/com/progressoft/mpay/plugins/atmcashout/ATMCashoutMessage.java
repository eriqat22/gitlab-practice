package com.progressoft.mpay.plugins.atmcashout;

import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

import java.math.BigDecimal;

public class ATMCashoutMessage extends MPayRequest {

    private static final String ReceiverInfoKey = "rcvId";
    private static final String ReceiverTypeKey = "rcvType";
    private static final String NotesKey = "data";
    private static final String AmountKey = "amnt";
    private static final String ReceiverPinKey = "receiverPin";
    private static final String senderAccountKey = "senderAccount";
    private static final String receiverAccountKey = "receiverAccount";
    private static final String receiverDeviceIdKey = "receiverDeviceId";

    private String receiverInfo;
    private String receiverType;
    private String notes;
    private BigDecimal amount;
    private String receiverPin;
    private String senderAccount;
    private String receiverAccount;
    private String receiverDeviceId;

    public ATMCashoutMessage() {

    }

    public ATMCashoutMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static ATMCashoutMessage ParseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        ATMCashoutMessage message = new ATMCashoutMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || message.getSenderType().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
        String amount = message.getValue(ATMCashoutMessage.AmountKey);
        if (amount == null)
            throw new MessageParsingException(ATMCashoutMessage.AmountKey);
        try {
            message.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
        } catch (NumberFormatException ex) {
            throw new MessageParsingException(ATMCashoutMessage.AmountKey);
        }
        message.setReceiverInfo(message.getValue(ATMCashoutMessage.ReceiverInfoKey));
        if (message.getReceiverInfo() == null)
            throw new MessageParsingException(ATMCashoutMessage.ReceiverInfoKey);
        message.setReceiverType(message.getValue(ATMCashoutMessage.ReceiverTypeKey));
        if (message.getReceiverType() == null)
            throw new MessageParsingException(ATMCashoutMessage.ReceiverTypeKey);
        message.setReceiverPin(message.getValue(ReceiverPinKey));
        message.setNotes(message.getValue(ATMCashoutMessage.NotesKey));
        message.setSenderAccount(message.getValue(senderAccountKey));
        message.setReceiverAccount(message.getValue(receiverAccountKey));
        message.setReceiverDeviceId(message.getValue(receiverDeviceIdKey));
        return message;
    }

    public String getReceiverInfo() {
        return receiverInfo;
    }

    public void setReceiverInfo(String receiverInfo) {
        this.receiverInfo = receiverInfo;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReceiverPin() {
        return receiverPin;
    }

    public void setReceiverPin(String receiverPin) {
        this.receiverPin = receiverPin;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    public String getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public String getReceiverDeviceId() {
        return receiverDeviceId;
    }

    public void setReceiverDeviceId(String receiverDeviceId) {
        this.receiverDeviceId = receiverDeviceId;
    }
}