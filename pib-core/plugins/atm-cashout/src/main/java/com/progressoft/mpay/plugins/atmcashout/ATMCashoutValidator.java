package com.progressoft.mpay.plugins.atmcashout;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATMCashoutValidator {
    private static final Logger logger = LoggerFactory.getLogger(ATMCashoutValidator.class);

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside validate ...");
        if (context == null)
            throw new NullArgumentException("context");

        if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
            return validateCustomer(context);
        else if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
            return validateCorporate(context);
        else
            return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
    }

    private static ValidationResult validateCustomer(MessageProcessingContext context) {

        ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
        if (!result.isValid())
            return result;

        result = CustomerValidator.validate(context.getSender().getCustomer(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getMobileAccount(), true, true);
        if (!result.isValid())
            return result;

        ATMCashoutMessage request = (ATMCashoutMessage) context.getRequest();

        result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = PinCodeValidator.validate(context, context.getSender().getMobile(), request.getPin());
        if (!result.isValid())
            return result;

        if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
            result = MobileNumberValidator.validate(context, request.getReceiverInfo());
            if (!result.isValid())
                return result;
        }

        if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getService() != null) {
            result = validateReceiver(context, request.getSenderType(), null);
            if (!result.isValid())
                return result;
        }

        result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
        if (!result.isValid())
            return result;

        result = AmountValidator.validate(context);
        if (!result.isValid())
            return result;

        result = LimitsValidator.validate(context);
        if (!result.isValid())
            return result;

        return PSPValidator.validate(context, false);
    }

    private static ValidationResult validateCorporate(MessageProcessingContext context) {
        ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, true);
        if (!result.isValid())
            return result;

        ATMCashoutMessage request = (ATMCashoutMessage) context.getRequest();

        result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(),
                context.getRequest().getDeviceId(),
                context.getSender().getService().getId()));
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getService(),
                context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = PinCodeValidator.validate(context, context.getSender().getService(), request.getPin());
        if (!result.isValid())
            return result;

        if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
            result = MobileNumberValidator.validate(context, request.getReceiverInfo());
            if (!result.isValid())
                return result;
        }

        if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getMobile() != null) {
            result = validateReceiver(context, request.getSenderType(), request.getReceiverPin());
            if (!result.isValid())
                return result;
        }

        result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
        if (!result.isValid())
            return result;

        result = AmountValidator.validate(context);
        if (!result.isValid())
            return result;

        result = LimitsValidator.validate(context);
        if (!result.isValid())
            return result;

        return PSPValidator.validate(context, false);
    }

    private static ValidationResult validateReceiver(MessageProcessingContext context, String senderType,
                                                     String receiverPin) {
        logger.debug("Inside validateReceiver ...");
        if (senderType.equals(ReceiverInfoType.CORPORATE)) {
            ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
            if (!result.isValid())
                return result;
            result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
            if (!result.isValid())
                return result;

            result = AccountValidator.validate(context.getReceiver().getMobileAccount(), false, true);
            if (!result.isValid())
                return result;

            result = PinCodeValidator.validate(context, context.getReceiver().getMobile(), receiverPin);
            if (!result.isValid())
                return result;
        } else {
            ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
            if (!result.isValid())
                return result;

            result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
            if (!result.isValid())
                return result;

        }
        return new ValidationResult(ReasonCodes.VALID, null, true);
    }
}
