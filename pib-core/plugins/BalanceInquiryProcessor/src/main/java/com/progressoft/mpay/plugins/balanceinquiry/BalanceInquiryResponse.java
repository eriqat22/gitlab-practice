package com.progressoft.mpay.plugins.balanceinquiry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.messages.MPayResponse;

public class BalanceInquiryResponse extends MPayResponse {
	private String balance;

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}
}
