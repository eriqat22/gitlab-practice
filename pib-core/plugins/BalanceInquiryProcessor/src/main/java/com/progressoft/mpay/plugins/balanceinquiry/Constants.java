package com.progressoft.mpay.plugins.balanceinquiry;

public class Constants {
	public static final String BALANCE_KEY = "bal";
	public static final String CURRENCY_KEY = "curr";

	private Constants() {

	}
}
