package com.progressoft.mpay.plugins.deactivatedevice;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class DeviceValidator {
	private static final Logger logger = LoggerFactory.getLogger(DeviceValidator.class);

	private DeviceValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context, String deviceId) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		if (deviceId == null)
			throw new NullArgumentException("deviceId");
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			return validateCustomerDevice(context);
		else
			return validateCorporateDevice(context);
	}

	private static ValidationResult validateCorporateDevice(MessageProcessingContext context) {
		MPAY_CorporateDevice device = null;
		if (context.getExtraData().containsKey(Constants.DEVICE_KEY))
			device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DEVICE_KEY);

		if (device == null || SystemHelper.isDeleted(device))
			return new ValidationResult(ReasonCodes.DEVICE_NOT_FOUND, null, false);
		if (device.getIsStolen())
			return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateCustomerDevice(MessageProcessingContext context) {
		MPAY_CustomerDevice device = null;
		if (context.getExtraData().containsKey(Constants.DEVICE_KEY))
			device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DEVICE_KEY);

		if (device == null || SystemHelper.isDeleted(device))
			return new ValidationResult(ReasonCodes.DEVICE_NOT_FOUND, null, false);
		if (device.getIsStolen())
			return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
