package com.progressoft.mpay.plugins.deactivatedevice;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class PasswordValidator {
	private static final Logger logger = LoggerFactory.getLogger(PasswordValidator.class);

	private PasswordValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		if (!context.getExtraData().containsKey(Constants.DEVICE_KEY))
			new ValidationResult(ReasonCodes.VALID, null, true);

		DeactivateDeviceMessage message = (DeactivateDeviceMessage) context.getRequest();
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)) {
			MPAY_CustomerDevice device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DEVICE_KEY);
			if (!device.getPassword().equals(message.getId()))
				new ValidationResult(ReasonCodes.INVALID_PASSKEY, null, false);
		} else {
			MPAY_CorporateDevice device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DEVICE_KEY);
			if (!device.getPassword().equals(message.getId()))
				new ValidationResult(ReasonCodes.INVALID_PASSKEY, null, false);
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
