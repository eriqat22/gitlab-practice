package com.progressoft.mpay.plugins.deactivatedevice;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class DeactivateDeviceMessage extends MPayRequest {
	private static final String ID_KEY = "id";

	private String id;

	public DeactivateDeviceMessage() {
		//
	}

	public DeactivateDeviceMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public static DeactivateDeviceMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		DeactivateDeviceMessage message = new DeactivateDeviceMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setId(message.getValue(ID_KEY));
		if (message.getId() == null || message.getId().trim().length() == 0)
			throw new MessageParsingException(ID_KEY);
		return message;
	}
}
