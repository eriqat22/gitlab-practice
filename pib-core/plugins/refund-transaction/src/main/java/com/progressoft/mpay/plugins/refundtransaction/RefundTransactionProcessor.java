package com.progressoft.mpay.plugins.refundtransaction;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class RefundTransactionProcessor implements MessageProcessor {
	public static final String REASON_DESC = "reasonDesc";
	private static final Logger logger = LoggerFactory.getLogger(RefundTransactionProcessor.class);

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessMessage(context);
			ValidationResult validationResult = RefundTransactionValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
					e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage", ex);
			return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
					ex.getMessage());
		}
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext context) {
		return null;
	}

	private void preProcessMessage(MessageProcessingContext context)
			throws MessageParsingException, WorkflowException, SQLException {
		logger.debug("Inside PreProcessMessage ...");

		RefundTransactionMessage message = RefundTransactionMessage.parseMessage(context.getRequest());
		context.setRequest(message);
		context.setSender(loadSender(context, message));
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setReceiver(receiver);
		context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
		long personClientType = context.getLookupsLoader().getCustomerClientType().getId();

		if (context.getSender().getService() == null)
			return;

		context.setDirection(TransactionDirection.ONUS);
		context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(
				context.getSender().getCorporate().getClientType().getId(), context.getSender().isBanked(),
				personClientType, false, context.getOperation().getMessageType().getId(),
				context.getTransactionNature(), context.getOperation().getId()));

		if (context.getTransactionConfig() != null)
			context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());

		ChargesCalculator.calculate(context);
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context)
			throws SQLException, WorkflowException {
		logger.debug("Inside AcceptMessage ...");

		RefundTransactionMessage message = (RefundTransactionMessage) context.getRequest();
		MPAY_Transaction originalTransaction = context.getDataProvider()
				.getTransactionByTransactionRef(message.getReference());
		context.setAmount(originalTransaction.getOriginalAmount());
		if (originalTransaction.getRefType().equals(TransactionTypeCodes.DIRECT_DEBIT))
			fillDirectDebitInfo(context, originalTransaction);
		else
			fillDirectCreditInfo(context, originalTransaction);

		MessageProcessingResult result = new MessageProcessingResult();
		MPAY_ProcessingStatus status;
		MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
		String reasonDescription = null;

		context.getMessage().setReason(reason);
		MPAY_Transaction transaction = TransactionHelper.createRefundTransaction(context,
				TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO, originalTransaction);

		result.setMessage(context.getMessage());
		context.setTransaction(transaction);
		result.setTransaction(transaction);
		context.getDataProvider().persistProcessingResult(result);
		JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(),
				context.getLookupsLoader(), transaction);

		if (postingResult.isSuccess()) {
			BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
			if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
				status = handleMPClear(context, message, result);
				originalTransaction.setIsReversed(true);
				originalTransaction.setReversedBy(transaction.getReference());
				updateLimitAccording(context, originalTransaction);
			} else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				context.getMessage().setProcessingStatus(status);
				context.getMessage().setReason(reason);
				context.getMessage().setReasonDesc(reasonDescription);
				MPayResponse mPayResponse = generateResponse(context);
				mPayResponse.getExtraData().add(new ExtraData(REASON_DESC, reasonDescription));
				result.getMessage().setResponseContent(mPayResponse.toString());
				result.setResponse(mPayResponse.toString());
				TransactionHelper.reverseTransaction(context);
			} else {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			}

		} else {
			if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
				reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
				context.getMessage().setReasonDesc(postingResult.getReason());
			} else
				reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
		}

		context.getMessage().setProcessingStatus(status);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
		transaction.setReason(context.getMessage().getReason());
		transaction.setReasonDesc(context.getMessage().getReasonDesc());
		result.setStatus(status);
		result.setStatusDescription(context.getMessage().getReasonDesc());
		handleNotifications(context, result, status);
		return result;
	}

	private void updateLimitAccording(MessageProcessingContext context,
			MPAY_Transaction originalTransaction) throws SQLException {
		Long msgTypeId = originalTransaction.getRefMessage().getMessageType().getId();
		BigDecimal amount = originalTransaction.getTotalAmount().negate();
		Date currentDate = SystemHelper.getCurrentDateWithoutTime();
		long accountId = context.getReceiver().getAccount().getId();
		
		context.getDataProvider().updateAccountLimit(accountId, msgTypeId, amount, -1, currentDate);
	}

	private void fillDirectCreditInfo(MessageProcessingContext context, MPAY_Transaction originalTransaction) {
		context.getReceiver().setInfo(originalTransaction.getSenderInfo());
		context.getReceiver().setMobile(originalTransaction.getSenderMobile());
		context.getReceiver().setMobileAccount(originalTransaction.getSenderMobileAccount());
		context.getReceiver().setService(originalTransaction.getSenderService());
		context.getReceiver().setServiceAccount(originalTransaction.getSenderServiceAccount());
		context.getReceiver().setBank(originalTransaction.getSenderBank());
		if (originalTransaction.getSenderMobile() != null)
			context.getReceiver().setAccount(originalTransaction.getSenderMobileAccount().getRefAccount());
		else
			context.getReceiver().setAccount(originalTransaction.getSenderServiceAccount().getRefAccount());
	}

	private void fillDirectDebitInfo(MessageProcessingContext context, MPAY_Transaction originalTransaction) {
		context.getReceiver().setInfo(originalTransaction.getReceiverInfo());
		context.getReceiver().setMobile(originalTransaction.getReceiverMobile());
		context.getReceiver().setMobileAccount(originalTransaction.getReceiverMobileAccount());
		context.getReceiver().setService(originalTransaction.getReceiverService());
		context.getReceiver().setServiceAccount(originalTransaction.getReceiverServiceAccount());
		context.getReceiver().setBank(originalTransaction.getReceiverBank());
		if (originalTransaction.getReceiverMobile() != null)
			context.getReceiver().setAccount(originalTransaction.getReceiverMobileAccount().getRefAccount());
		else
			context.getReceiver().setAccount(originalTransaction.getReceiverServiceAccount().getRefAccount());
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode,
			String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		if (context.getTransaction() != null) {
			BankIntegrationResult result = BankIntegrationHandler.reverse(context);
			if (result != null) {
				TransactionHelper.reverseTransaction(context);
			}
		}
		return MessageProcessingResult.create(context, status, reason, reasonDescription);
	}

	private ProcessingContextSide loadSender(MessageProcessingContext context, RefundTransactionMessage request)
			throws SQLException {
		ProcessingContextSide sender = new ProcessingContextSide();
		sender.setInfo(request.getSender());
		sender.setService(context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType()));
		if (sender.getService() != null) {
			sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(),
					request.getSenderAccount()));
			if (sender.getServiceAccount() == null)
				return sender;
			sender.setAccount(sender.getServiceAccount().getRefAccount());
			sender.setCorporate(sender.getService().getRefCorporate());
			sender.setBank(sender.getServiceAccount().getBank());
			sender.setProfile(sender.getServiceAccount().getRefProfile());
			sender.setBanked(sender.getAccount().getIsBanked());
			sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
					context.getOperation().getMessageType().getId()));
			sender.setCommission(context.getDataProvider()
					.getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
		}

		return sender;
	}

	private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, RefundTransactionMessage message,
			MessageProcessingResult result) throws WorkflowException {
		MPAY_ProcessingStatus status;
		Integer mpClearIntegType;
		if (context.getSystemParameters().getOfflineModeEnabled()
				&& context.getDirection() == TransactionDirection.ONUS) {
			mpClearIntegType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
			result.setHandleMPClearOffline(true);
		} else {
			mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
		}
		IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, false, false);
		MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage,
				Integer.toHexString(mpClearIntegType));
		messageLog.setRefMessage(context.getMessage());
		result.setMpClearMessage(messageLog);
		return status;
	}

	private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isSenderAlias,
			boolean isReceiverAlias) throws WorkflowException {
		logger.debug("Inside CreateMPClearRequest ...");
		RefundTransactionMessage request = (RefundTransactionMessage) context.getRequest();
		String messageID = context.getDataProvider().getNextMPClearMessageId();
		BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
		IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
				.newMessage(type);
		message.setBinary(false);

		String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(),
				context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
		String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

		message.setField(4, new IsoValue<>(IsoType.NUMERIC, formatedAmount, 12));
		MPClearHelper.setMessageEncoded(message);
		message.setField(7,
				new IsoValue<>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
		message.setField(46, new IsoValue<>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
		message.setField(49,
				new IsoValue<>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

		String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getService(),
				context.getSender().getServiceAccount(), isSenderAlias);
		MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

		String account2;

		if (context.getTransaction().getReceiverMobile() == null)
			account2 = MPClearHelper.getAccount(context.getSystemParameters(),
					context.getTransaction().getReceiverInfo(), request.getReceiverType());
		else
			account2 = MPClearHelper.getAccount(context.getSystemParameters(),
					context.getTransaction().getReceiverMobile(), context.getTransaction().getReceiverMobileAccount(),
					isReceiverAlias);

		MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

		String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
		message.setField(104, new IsoValue<>(IsoType.LLLVAR, messageTypeCode != null ? messageTypeCode : "",
				messageTypeCode != null ? messageTypeCode.length() : 0));
		message.setField(115, new IsoValue<>(IsoType.LLLVAR, messageID, messageID.length()));

		return message;
	}

	private MPayResponse generateResponse(MessageProcessingContext context) {
		logger.debug("Inside GenerateResponse ...");
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
				context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
					context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response;
	}

	private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result,
			MPAY_ProcessingStatus status) {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
	}

}
