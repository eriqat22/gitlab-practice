package com.progressoft.mpay.plugins.refundtransaction;

import com.progressoft.mpay.plugins.NotificationContext;

public class RefundTransactionNotificationContext extends NotificationContext {

    private String receiver;
    private String currency;
    private String amount;
    private String senderBalance;
    private String receiverBalance;

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSenderBalance() {
        return senderBalance;
    }

    public void setSenderBalance(String senderBalance) {
        this.senderBalance = senderBalance;
    }

    public String getReceiverBalance() {
        return receiverBalance;
    }

    public void setReceiverBalance(String receiverBalance) {
        this.receiverBalance = receiverBalance;
    }
}
