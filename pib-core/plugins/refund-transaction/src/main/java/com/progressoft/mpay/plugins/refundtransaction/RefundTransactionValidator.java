package com.progressoft.mpay.plugins.refundtransaction;

import java.util.Date;

import org.apache.commons.lang.NullArgumentException;
import org.joda.time.DurationFieldType;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;

public class RefundTransactionValidator {
	private static final Logger logger = LoggerFactory.getLogger(RefundTransactionValidator.class);

	public RefundTransactionValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");

		if (context == null)
			throw new NullArgumentException("context");

		RefundTransactionMessage request = (RefundTransactionMessage) context.getRequest();

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = validateCorporate(context, request);
		if (!result.isValid())
			return result;

		if (!result.isValid())
			return result;

		result = validateTransaction(context, request);
		if (!result.isValid())
			return result;

		result = validateRefundPeriod(context, request);
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context);
		if (!result.isValid())
			return result;

		return result;
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context,
			RefundTransactionMessage request) {
		ValidationResult result = validateCorporateInfo(context, request);
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(),
				context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		result = PinCodeValidator.validate(context, context.getSender().getService(), request.getPin());
		if (!result.isValid())
			return result;

		result = CorporateAgainstSenderValidator.validate(context);
		if (!result.isValid())
			return result;

		return result;
	}

	private static ValidationResult validateCorporateInfo(MessageProcessingContext context,
			RefundTransactionMessage request) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,
				context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(),
				context.getRequest().getDeviceId(), context.getSender().getService().getId()));
	}

	private static ValidationResult validateTransaction(MessageProcessingContext context,
			RefundTransactionMessage request) {
		MPAY_Transaction originalTransaction = getOriginalTransaction(context, request);
		if (originalTransaction == null)
			return new ValidationResult(ReasonCodes.NO_TRANSACTION_FOUND, null, false);
		if (validateIfOperationNotAllowed(originalTransaction, context)) {
			return new ValidationResult(ReasonCodes.OPERATION_NOT_ALLOWED_TO_REFUND, null, false);
		}
		if (!originalTransaction.getProcessingStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return new ValidationResult(ReasonCodes.INVALID_TRANSACTION_STATUS, null, false);
		if (originalTransaction.getIsReversed())
			return new ValidationResult(ReasonCodes.TRANSACTION_ALREADY_REFUNDED, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static boolean validateIfOperationNotAllowed(MPAY_Transaction originalTransaction,
			MessageProcessingContext context) {
		return !context.getDataProvider().isOperationAllowedToRefund(originalTransaction.getRefOperation().getId());
	}

	private static MPAY_Transaction getOriginalTransaction(MessageProcessingContext context,
			RefundTransactionMessage request) {
		return context.getDataProvider().getTransactionByTransactionRef(request.getReference());
	}

	private static ValidationResult validateRefundPeriod(MessageProcessingContext context,
			RefundTransactionMessage request) {
		int refundPeriodInDays = context.getSystemParameters().getRefundPeriodInDays();
		Date transactionDate = context.getDataProvider().getTransactionByTransactionRef(request.getReference())
				.getTransDate();

		if (refundPeriodInDays > 0) {
			LocalDateTime allowRefundPeriodDate = new LocalDateTime(transactionDate)
					.withFieldAdded(DurationFieldType.days(), refundPeriodInDays);
			if (allowRefundPeriodDate.toDateTime().isBeforeNow()) {
				return new ValidationResult(ReasonCodes.EXCEEDED_REFUND_PERIOD, null, false);
			}
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

}
