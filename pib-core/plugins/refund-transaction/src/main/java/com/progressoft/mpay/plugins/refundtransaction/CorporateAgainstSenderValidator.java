package com.progressoft.mpay.plugins.refundtransaction;

import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CorporateAgainstSenderValidator {

    private static final Logger logger = LoggerFactory.getLogger(RefundTransactionValidator.class);

    public CorporateAgainstSenderValidator()
    {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ...");

        RefundTransactionMessage request = (RefundTransactionMessage) context.getRequest();

        MPAY_CorpoarteService service = getCorporateService(context, request);

        if(service == null)
            return new ValidationResult(ReasonCodes.CORPORATE_SERVICE_DOES_NOT_MATCH, null, false);

        if(isSenderServiceMatchOneWithOriginalTransaction(context, service))
            return new ValidationResult(ReasonCodes.CORPORATE_SERVICE_DOES_NOT_MATCH, null, false);

        return new ValidationResult(ReasonCodes.VALID, null, true);
    }

    private static boolean isSenderServiceMatchOneWithOriginalTransaction(MessageProcessingContext context, MPAY_CorpoarteService service) {
        return !context.getSender().getService().getId().equals(service.getId());
    }

    private static MPAY_CorpoarteService getCorporateService(MessageProcessingContext context, RefundTransactionMessage request) {

        MPAY_Transaction originalTransaction = getOriginalTransaction(context, request);
        MPAY_CorpoarteService service= null;
        if(isTransactionTypeDirectCredit(originalTransaction))
            service = originalTransaction.getReceiverService();
        else
			service = originalTransaction.getSenderService();

        return service;
    }

    private static boolean isTransactionTypeDirectCredit(MPAY_Transaction originalTransaction) {
        return originalTransaction.getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT);
    }

    private static MPAY_Transaction getOriginalTransaction(MessageProcessingContext context, RefundTransactionMessage request) {
        return context.getDataProvider().getTransactionByTransactionRef(request.getReference());
    }
}
