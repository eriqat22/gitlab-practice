package com.progressoft.mpay.plugins.refundtransaction;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.plugins.ProcessingContext;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		try {
			String receiver;
			String sender;
			String reference;
			long senderLanguageId;
			long receiverLanguageId;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();
			MPAY_CorpoarteService senderService = context.getSender().getService();
			MPAY_CustomerMobile receiverMobile = context.getReceiver().getMobile();
			MPAY_CorpoarteService receiverService = context.getReceiver().getService();
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();
			RefundTransactionNotificationContext notificationContext = new RefundTransactionNotificationContext();
			notificationContext
					.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);

			if (senderService != null) {
				sender = senderService.getName();
				context.getDataProvider().refreshEntity(context.getSender().getAccount());
				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getSender().getAccount().getBalance()));
				senderLanguageId = context.getSender().getCorporate().getPrefLang().getId();
			} else {
				sender = context.getSender().getInfo();
				senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			if (receiverMobile != null) {
				receiver = SystemHelper.maskMobileNumber(context.getSystemParameters(),
						receiverMobile.getMobileNumber());
				context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getReceiver().getAccount().getBalance()));

				receiverLanguageId = context.getReceiver().getCustomer().getPrefLang().getId();
				if (receiverMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS)
						&& !StringUtils.isEmpty(receiverMobile.getAlias()))
					receiver = receiverMobile.getAlias();
			} else {
				receiver = SystemHelper.maskMobileNumber(context.getSystemParameters(),
						context.getReceiver().getInfo());
				receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}

			if (receiverService != null) {
				receiver = receiverService.getNotificationReceiver();
				context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getReceiver().getAccount().getBalance()));
				senderLanguageId = context.getSender().getCorporate().getPrefLang().getId();
			} else {
				receiver = context.getReceiver().getInfo();
				receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}

			if (context.getReceiver().getNotes() != null) {
				notificationContext.setNotes(context.getReceiver().getNotes());
				notificationContext.setHasNotes(true);
			}
			notificationContext.setReceiver(receiver);
			notificationContext.setSender(sender);
			return generateNotifications(context, senderLanguageId, receiverLanguageId, senderService, receiverMobile,
					receiverService, operation, notificationContext);
		} catch (Exception e) {
			logger.error("Error when CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static List<MPAY_Notification> generateNotifications(ProcessingContext context, long senderLanguageId,
			long receiverLanguageId, MPAY_CorpoarteService senderService, MPAY_CustomerMobile receiverMobile,
			MPAY_CorpoarteService receiverService, MPAY_EndPointOperation operation,
			RefundTransactionNotificationContext notificationContext) {
		List<MPAY_Notification> notifications = new ArrayList<>();
		if (senderService != null) {
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(),
					notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
					senderService.getName(), NotificationChannelsCode.SMS));
		}
		if (receiverMobile != null) {
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(),
					notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId,
					receiverMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		if (receiverService != null) {
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(),
					notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId,
					receiverService.getName(), NotificationChannelsCode.SMS));
		}

		return notifications;
	}

}
