package com.progressoft.mpay.plugins.hokomty.billadvise;

public class BillAdviseSequence {
	private double amnt;
	private int sequence;

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public double getAmnt() {
		return amnt;
	}

	public void setAmnt(double amnt) {
		this.amnt = amnt;
	}
}
