package com.progressoft.mpay.plugins.hokomty.billadvise;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class BillAdviseMessage extends MPayRequest {
	public static final String amntKey = "amnt";
	public static final String rcvSvcKey = "rcvSvc";
	public static final String rcvSvcTypeKey = "rcvSvcType";
	public static final String billingAccKey = "billingAcc";
	public static final String billInqRqUIdKey = "billInqRqUId";
	public static final String feeInqRqUIdKey = "feeInqRqUId";
	public static final String sequencesKey = "sequences";

	private double amnt;
	private String rcvSvc;
	private String rcvSvcType;
	private String billingAcc;
	private String billInqRqUId;
	private String feeInqRqUId;
	private ArrayList<BillAdviseSequence> sequences;

	public String getbillInqRqUId() {
		return billInqRqUId;
	}

	public void setbillInqRqUId(String billInqRqUId) {
		this.billInqRqUId = billInqRqUId;
	}

	public String getBillingAcc() {
		return billingAcc;
	}

	public void setBillingAcc(String billingAcc) {
		this.billingAcc = billingAcc;
	}

	public ArrayList<BillAdviseSequence> getSequences() {
		return sequences;
	}

	public void setSequences(ArrayList<BillAdviseSequence> sequences) {
		this.sequences = sequences;
	}

	public void setSequencesFromJson(String json) throws MessageParsingException {
		try {
			Type listType = new TypeToken<List<BillAdviseSequence>>() {
			}.getType();
			Gson gson = new GsonBuilder().create();
			this.sequences = gson.fromJson(json, listType);
			if (this.sequences == null || this.sequences.size() == 0)
				throw new MessageParsingException(sequencesKey);
		} catch (Exception ex) {
			throw new MessageParsingException(sequencesKey);
		}
	}

	public String getRcvSvcType() {
		return rcvSvcType;
	}

	public void setRcvSvcType(String rcvSvcType) {
		this.rcvSvcType = rcvSvcType;
	}

	public double getAmnt() {
		return amnt;
	}

	public void setAmnt(double amnt) {
		this.amnt = amnt;
	}

	public String getRcvSvc() {
		return rcvSvc;
	}

	public void setRcvSvc(String rcvSvc) {
		this.rcvSvc = rcvSvc;
	}

	public String getfeeInqRqUId() {
		return feeInqRqUId;
	}

	public void setfeeInqRqUId(String feeInqRqUId) {
		this.feeInqRqUId = feeInqRqUId;
	}

	public BillAdviseMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static BillAdviseMessage ParseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		BillAdviseMessage message = new BillAdviseMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

		String amount = message.getValue(amntKey);
		if (amount == null || amount.trim().length() == 0)
			throw new MessageParsingException(amntKey);
		try {
			message.setAmnt(Double.parseDouble(amount));
		} catch (Exception e) {
			throw new MessageParsingException(amntKey);
		}
		message.setRcvSvc(message.getValue(rcvSvcKey));
		if (message.getRcvSvc() == null || message.getRcvSvc().trim().length() == 0)
			throw new MessageParsingException(rcvSvcKey);
		message.setRcvSvcType(message.getValue(rcvSvcTypeKey));
		if (message.getRcvSvcType() == null || (!message.getRcvSvcType().equals(ReceiverInfoType.CORPORATE) && !message.getRcvSvcType().equals(ReceiverInfoType.ALIAS)))
			throw new MessageParsingException(rcvSvcTypeKey);
		message.setBillingAcc(message.getValue(billingAccKey));
		if (message.getBillingAcc() == null || message.getBillingAcc().trim().length() == 0)
			throw new MessageParsingException(billingAccKey);
		message.setbillInqRqUId(message.getValue(billInqRqUIdKey));
		if (message.getbillInqRqUId() == null || message.getbillInqRqUId().trim().length() == 0)
			throw new MessageParsingException(billInqRqUIdKey);
		message.setfeeInqRqUId(message.getValue(feeInqRqUIdKey));
		if (message.getfeeInqRqUId() == null || message.getfeeInqRqUId().trim().length() == 0)
			throw new MessageParsingException(feeInqRqUIdKey);
		message.setSequencesFromJson(message.getValue(sequencesKey));
		return message;
	}
}
