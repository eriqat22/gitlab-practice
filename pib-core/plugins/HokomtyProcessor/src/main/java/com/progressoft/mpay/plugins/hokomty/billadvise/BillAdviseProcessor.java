package com.progressoft.mpay.plugins.hokomty.billadvise;

import java.math.BigDecimal;
import java.sql.SQLException;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_ServiceIntegMessage;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MPayResponseEnvelope;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.hokomty.common.Constants;
import com.progressoft.mpay.plugins.hokomty.common.EPayRequestsHelper;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class BillAdviseProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(BillAdviseProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = BillAdviseValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in BillAdviseProcessor", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
		if (result.getTransaction() != null) {
			BankIntegrationHandler.reverse(context);
			TransactionHelper.reverseTransaction(context);
		}
		String response = generateResponse(context, false);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = new MessageProcessingResult();
		MPAY_ProcessingStatus status;
		MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
		String reasonDescription = null;
		MPAY_ServiceIntegMessage serviceIntegMessage;
		context.getMessage().setReason(reason);
		MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO, null);
		result.setMessage(context.getMessage());
		context.setTransaction(transaction);
		result.setTransaction(transaction);
		context.getDataProvider().persistProcessingResult(result);
		JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), transaction);
		if (postingResult.isSuccess()) {
			BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
			if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {

				serviceIntegMessage = EPayRequestsHelper.GenerateEPayAdvRequest(context);
				if (serviceIntegMessage == null)
					return rejectMessage(context, ReasonCodes.SERVICE_INTEGRATION_FAILURE, null);
				context.getDataProvider().persistServiceIntegrationMessage(serviceIntegMessage);
				serviceIntegMessage = EPayRequestsHelper.SendBillAdvise(serviceIntegMessage, context);
				context.getDataProvider().persistServiceIntegrationMessage(serviceIntegMessage);
				if (!serviceIntegMessage.getRefStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
					return rejectMessage(context, context.getMessage().getReason().getCode(), context.getMessage().getReasonDesc());

				Integer mpClearIntegType;
				if (SystemParameters.getInstance().getOfflineModeEnabled() && context.getDirection() == TransactionDirection.ONUS) {
					mpClearIntegType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
					status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
					result.setHandleMPClearOffline(true);
				} else {
					mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
					status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
				}
				boolean isSenderAlias = context.getSender().getMobile().getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) && context.getSender().getMobile().getAlias() != null
						&& context.getSender().getMobile().getAlias().length() > 0;
				IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, isSenderAlias);
				MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegType));
				messageLog.setRefMessage(context.getMessage());
				result.setMpClearMessage(messageLog);
			} else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			} else {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			}
		} else {
			if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
				reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
				context.getMessage().setReasonDesc(postingResult.getReason());
			} else
				reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
		}
		context.getMessage().setProcessingStatus(status);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
		transaction.setReason(context.getMessage().getReason());
		transaction.setReasonDesc(context.getMessage().getReasonDesc());
		result.setStatus(status);
		result.setStatusDescription(context.getMessage().getReasonDesc());
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
			result.setNotifications(NotificationProcessor.CreateAcceptanceNotificationMessages(context));
		else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
			result.setNotifications(NotificationProcessor.CreateRejectionNotificationMessages(context));
		String response = generateResponse(context, true);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isSenderAlias) {
		logger.debug("Inside CreateMPClearRequest ...");
		try {
			BillAdviseMessage request = (BillAdviseMessage) context.getRequest();
			BigDecimal amount = context.getTransaction().getTotalAmount();
			amount = amount.subtract(context.getSender().getCharge()).subtract(context.getReceiver().getCharge());
			String messageID = context.getDataProvider().getNextMPClearMessageId();
			BigDecimal charge = context.getTransaction().getTotalAmount().subtract(amount);
			IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(type);
			message.setBinary(false);

			String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), amount, AmountType.AMOUNT);
			String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

			message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
			MPClearHelper.setMessageEncoded(message);
			message.setField(7, new IsoValue<String>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
			message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
			message.setField(49, new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

			String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getMobile(), context.getSender().getMobileAccount(), isSenderAlias);
			MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

			String account2;
			if (context.getReceiver().getService() == null)
				account2 = MPClearHelper.getAccount(context.getSystemParameters(), request.getRcvSvc(), request.getRcvSvcType());
			else
				account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getService(), context.getReceiver().getServiceAccount(), request.getRcvSvcType().equals(ReceiverInfoType.ALIAS));

			MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

			String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
			message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
			message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

			return message;
		} catch (Exception e) {
			logger.error("Error when CreateMPClearRequest in BillAdviseProcessor", e);
			return null;
		}
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("isAccepted:" + isAccepted);
		MPayResponseEnvelope responseEnvelope = new MPayResponseEnvelope();
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), SystemParameters.getInstance().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());

		responseEnvelope.setResponse(response);
		responseEnvelope.setToken(context.getCryptographer().createJsonToken(context.getSystemParameters(), response.toValues()));
		return responseEnvelope.toString();
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException, SQLException {
		logger.debug("Inside PreProcessMessage ...");
		BillAdviseMessage message = BillAdviseMessage.ParseMessage(context.getRequest());
		context.setRequest(message);
		context.setAmount(BigDecimal.valueOf(message.getAmnt()));
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
		if (sender.getMobile() == null)
			return;
		sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
		if (sender.getMobileAccount() == null)
			return;
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setBank(sender.getMobileAccount().getBank());
		sender.setAccount(sender.getMobileAccount().getRefAccount());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
		ChargesCalculator.calculateSenderCharge(context.getOperation().getMessageType().getCode(), context, context.getSender().getProfile());

		MPAY_ServiceIntegMessage billInquiryIntegrationMessage = context.getDataProvider().getServiceIntegrationMessageByRequestId(message.getbillInqRqUId());
		if (billInquiryIntegrationMessage == null)
			return;
		context.getExtraData().put(Constants.BillInquiryRequestKey, billInquiryIntegrationMessage);
		MPAY_ServiceIntegMessage feesInquiryIntegrationMessage = context.getDataProvider().getServiceIntegrationMessageByRequestId(message.getfeeInqRqUId());
		if (feesInquiryIntegrationMessage == null)
			return;
		context.getExtraData().put(Constants.FeesInquiryRequestKey, feesInquiryIntegrationMessage);

		receiver.setService(context.getDataProvider().getCorporateService(message.getRcvSvc(), message.getRcvSvcType()));
		if (receiver.getService() == null) {
			context.getReceiver().setService(context.getLookupsLoader().getPSP().getPspMPClearService());
			context.getReceiver().setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), context.getReceiver().getService(), null));
			if (context.getReceiver().getServiceAccount() != null)
				context.getReceiver().setBank(context.getReceiver().getServiceAccount().getBank());
			context.setDirection(TransactionDirection.OUTWARD);
			return;
		}
		receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), receiver.getService(), null));
		if (receiver.getServiceAccount() == null)
			return;
		long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
		context.setDirection(TransactionDirection.ONUS);
		receiver.setCorporate(receiver.getService().getRefCorporate());
		receiver.setBank(receiver.getServiceAccount().getBank());
		receiver.setAccount(receiver.getServiceAccount().getRefAccount());
		receiver.setBanked(receiver.getAccount().getIsBanked());
		receiver.setProfile(receiver.getServiceAccount().getRefProfile());
		context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(),
				context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
		if (context.getTransactionConfig() != null)
			context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
		ChargesCalculator.calculateReceiverCharge(context.getOperation().getMessageType().getCode(), context, context.getReceiver().getProfile());
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		if (context.getTransaction() != null) {
			context.getTransaction().setReason(reason);
			context.getTransaction().setReasonDesc(reasonDescription);
			context.getTransaction().setProcessingStatus(status);
		}
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setTransaction(context.getTransaction());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		logger.debug("Inside Reverse ...");
		return null;
	}
}