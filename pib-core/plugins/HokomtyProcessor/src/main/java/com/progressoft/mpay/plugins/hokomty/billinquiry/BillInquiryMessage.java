package com.progressoft.mpay.plugins.hokomty.billinquiry;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class BillInquiryMessage extends MPayRequest {

	private static final String billerIdKey = "billerId";
	private static final String rcvSvcTypeKey = "rcvSvcType";
	private static final String billingAccKey = "billingAcc";
	private static final String serviceTypeKey = "serviceType";
	private static final String incPaidBillsKey = "incPaidBills";

	private String billerId;
	private String rcvSvcType;
	private String billingAcc;
	private String serviceType;
	private Boolean incPaidBills;

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public String getRcvSvcType() {
		return rcvSvcType;
	}

	public void setRcvSvcType(String rcvSvcType) {
		this.rcvSvcType = rcvSvcType;
	}

	public String getBillingAcc() {
		return billingAcc;
	}

	public void setBillingAcc(String billingAcc) {
		this.billingAcc = billingAcc;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Boolean getIncPaidBills() {
		return incPaidBills;
	}

	public void setIncPaidBills(Boolean incPaidBills) {
		this.incPaidBills = incPaidBills;
	}

	public BillInquiryMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static BillInquiryMessage ParseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		BillInquiryMessage message = new BillInquiryMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

		message.setBillerId(message.getValue(billerIdKey));
		if (message.getBillerId() == null || message.getBillerId().trim().length() == 0)
			throw new MessageParsingException(billerIdKey);
		message.setRcvSvcType(message.getValue(rcvSvcTypeKey));
		if (message.getRcvSvcType() == null || message.getRcvSvcType().trim().length() == 0)
			throw new MessageParsingException(rcvSvcTypeKey);
		if (!message.getRcvSvcType().equals(ReceiverInfoType.CORPORATE) && !message.getRcvSvcType().equals(ReceiverInfoType.ALIAS))
			throw new MessageParsingException(rcvSvcTypeKey);
		message.setBillingAcc(message.getValue(billingAccKey));
		if (message.getBillingAcc() == null || message.getBillingAcc().trim().length() == 0)
			throw new MessageParsingException(billingAccKey);
		message.setServiceType(message.getValue(serviceTypeKey));
		if (message.getServiceType() == null || message.getServiceType().trim().length() == 0)
			throw new MessageParsingException(serviceTypeKey);
		try {
			message.setIncPaidBills(Boolean.parseBoolean(message.getValue(incPaidBillsKey)));
		} catch (Exception e) {
			throw new MessageParsingException(incPaidBillsKey);
		}
		return message;
	}
}
