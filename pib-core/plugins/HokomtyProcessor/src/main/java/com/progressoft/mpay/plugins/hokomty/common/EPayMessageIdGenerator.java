package com.progressoft.mpay.plugins.hokomty.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.internal.SessionImpl;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;

public class EPayMessageIdGenerator {

	private EntityManager em;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}



	protected EPayMessageIdGenerator() {

	}


	private long getNextMessageIdSequence() throws WorkflowException {
		long sequence = -1;

		try {
			Connection cc = ((SessionImpl) em.getDelegate()).connection();
			CallableStatement callableStatement = cc.prepareCall("{call GetNewSeqVal_ePayMessageId(?)}");
			callableStatement.registerOutParameter(1, Types.BIGINT);
			callableStatement.execute();

			sequence = callableStatement.getLong(1);
			callableStatement.close();
		} catch (SQLException ex) {
			throw new WorkflowException("An error occurd while getting next message sequence", ex);
		}

		return sequence;
	}

	public String getNextId() throws WorkflowException {
		long sequence = getNextMessageIdSequence();
		String id = String.format("%s%s%023d", LookupsLoader.getInstance().getPSP().getNationalID(),
				SystemHelper.formatDate(SystemHelper.getSystemTimestamp(), "ddMMyyyy"), (sequence));
		return id;

	}

	public static EPayMessageIdGenerator getInstance() {
		EPayMessageIdGenerator generator = (EPayMessageIdGenerator) AppContext.getApplicationContext().getBean(
				"EPayMessageIdGenerator");
		return generator;
	}
}
