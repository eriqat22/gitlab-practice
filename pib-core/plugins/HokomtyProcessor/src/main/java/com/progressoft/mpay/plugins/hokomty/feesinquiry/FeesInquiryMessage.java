package com.progressoft.mpay.plugins.hokomty.feesinquiry;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class FeesInquiryMessage extends MPayRequest {
	private static final String billerIdKey = "billerId";
	private static final String billInqRqUIdKey = "billInqRqUId";
	private static final String epayBillRecIDKey = "epayBillRecID";
	private static final String billInfoKey = "billInfo";

	private String billerId;
	private String billInqRqUId;
	private String epayBillRecID;

	public FeesInquiryMessage(){
		billInfo = new ArrayList<BillFeeInfo>();
	}

	List<BillFeeInfo> billInfo;

	public String getBillInqRqUId() {
		return billInqRqUId;
	}

	public void setBillInqRqUId(String rqUId) {
		this.billInqRqUId = rqUId;
	}

	public String getEpayBillRecID() {
		return epayBillRecID;
	}

	public void setEpayBillRecID(String epayBillRecID) {
		this.epayBillRecID = epayBillRecID;
	}

	public List<BillFeeInfo> getBillInfo() {
		return billInfo;
	}

	public void setBillInfo(List<BillFeeInfo> billInfo) {
		this.billInfo = billInfo;
	}

	public void setBillInfoFromJson(String json) throws MessageParsingException {
		try {
			Type listType = new TypeToken<List<BillFeeInfo>>() {
			}.getType();
			Gson gson = new GsonBuilder().create();
			this.billInfo = gson.fromJson(json, listType);
			if (this.billInfo == null || this.billInfo.size() == 0)
				throw new MessageParsingException(billInfoKey);
		} catch (Exception ex) {
			throw new MessageParsingException(billInfoKey);
		}
	}

	public String getBillerId() {
		return billerId;
	}

	public void setBillerId(String billerId) {
		this.billerId = billerId;
	}

	public FeesInquiryMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static FeesInquiryMessage ParseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		FeesInquiryMessage message = new FeesInquiryMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

		message.setBillerId(message.getValue(billerIdKey));
		if (message.getBillerId() == null || message.getBillerId().trim().length() == 0)
			throw new MessageParsingException(billerIdKey);
		message.setBillInqRqUId(message.getValue(billInqRqUIdKey));
		if (message.getBillInqRqUId() == null || message.getBillInqRqUId().trim().length() == 0)
			throw new MessageParsingException(billInqRqUIdKey);
		message.setEpayBillRecID(message.getValue(epayBillRecIDKey));
		if (message.getEpayBillRecID() == null || message.getEpayBillRecID().trim().length() == 0)
			throw new MessageParsingException(epayBillRecIDKey);
		message.setBillInfoFromJson(message.getValue(billInfoKey));

		return message;
	}
}
