package com.progressoft.mpay.plugins.hokomty.common;

public class Constants {
	public static final String BillInquiryResponseKey = "BillInquiryResponseKey";
	public static final String BillInquiryRequestKey = "BillInquiryRequestKey";
	public static final String BillInquiryResponseFieldKey = "bills";

	public static final String FeesInquiryResponseKey = "FeesInquiryResponseKey";
	public static final String FeesInquiryRequestKey = "FeesInquiryRequestKey";
	public static final String FeesInquiryResponseFieldKey = "Fees";

	public static final String BillAdviseResponseKey = "BillAdviseResponseKey";
	public static final String BillAdviseRequestKey = "BillAdviseRequestKey";
	public static final String BillAdviseResponseFieldKey = "advise";
}
