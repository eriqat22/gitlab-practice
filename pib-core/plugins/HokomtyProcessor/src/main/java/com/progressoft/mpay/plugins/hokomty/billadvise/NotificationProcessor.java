package com.progressoft.mpay.plugins.hokomty.billadvise;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	public static List<MPAY_Notification> CreateAcceptanceNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateAcceptanceNotificationMessages ...");
			String receiver = null;
			String reference = null;
			long senderLanguageId = 0;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();
			MPAY_CustomerMobile senderMobile = context.getSender().getMobile();
			MPAY_CorpoarteService receiverService = context.getReceiver().getService();
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

			BillAdviseNotificationContext notificationContext = new BillAdviseNotificationContext();
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);
			if (senderMobile != null) {
				context.getDataProvider().refreshEntity(context.getSender().getAccount());
				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getAccount().getBalance()));
				notificationContext.setSenderBanked(senderMobile.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
				if (context.getSender().getCustomer() != null)
					senderLanguageId = context.getSender().getCustomer().getPrefLang().getId();
				else
					senderLanguageId = SystemParameters.getInstance().getSystemLanguage().getId();
			}
			if (receiverService != null) {
				receiver = receiverService.getName();
			} else
				receiver = context.getReceiver().getInfo();
			notificationContext.setReceiver(receiver);
			List<MPAY_Notification> notifications = new ArrayList<MPAY_Notification>();
			if (senderMobile != null) {
				notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
						senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error in CreateAcceptanceNotificationMessages", e);
			return null;
		}
	}

	public static List<MPAY_Notification> CreateRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<MPAY_Notification>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			BillAdviseMessage request = BillAdviseMessage.ParseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String senderMobile = request.getSender();
			long languageId = request.getLang();
			MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
			if (mobile != null) {
				senderMobile = mobile.getMobileNumber();
				languageId = mobile.getRefCustomer().getPrefLang().getId();
			}
			BillAdviseNotificationContext notificationContext = new BillAdviseNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(SystemParameters.getInstance().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setAmount(String.valueOf(request.getAmnt()));
			notificationContext.setReceiver(request.getRcvSvc());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(context.getMessage().getReason().getReasonsNLS().get(languageId).getDescription());
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_SENDER,
					context.getMessage().getRefOperation().getId(), languageId, senderMobile, NotificationChannelsCode.SMS));
			return notifications;
		} catch (Exception e) {
			logger.error("Error in CreateRejectionNotificationMessages", e);
			return null;
		}
	}
}
