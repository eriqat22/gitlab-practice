package com.progressoft.mpay.plugins.hokomty.billinquiry;

import org.apache.commons.lang.StringUtils;

import com.progressoft.mpay.plugins.hokomty.entities.AmountDueType;
import com.progressoft.mpay.plugins.hokomty.entities.PaymentModeType;
import com.progressoft.mpay.plugins.hokomty.entities.ShortDescType;

public class BillInfo {

	private String sequence;
	private Double amtDue;
	private boolean exactPmt;
	private Double minAmt;
	private Double maxAmt;
	private String pymtMode;
	private String curCode;
	private String shortDesc;

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public Double getAmtDue() {
		return amtDue;
	}

	public void setAmtDue(Double amtDue) {
		this.amtDue = amtDue;
	}

	public boolean isExactPmt() {
		return exactPmt;
	}

	public void setExactPmt(boolean exactPmt) {
		this.exactPmt = exactPmt;
	}

	public Double getMinAmt() {
		return minAmt;
	}

	public void setMinAmt(Double minAmt) {
		this.minAmt = minAmt;
	}

	public Double getMaxAmt() {
		return maxAmt;
	}

	public void setMaxAmt(Double maxAmt) {
		this.maxAmt = maxAmt;
	}

	public String getPymtMode() {
		return pymtMode;
	}

	public void setPymtMode(String pymtMode) {
		this.pymtMode = pymtMode;
	}

	public String getCurCode() {
		return curCode;
	}

	public void setCurCode(String curCode) {
		this.curCode = curCode;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public static BillInfo Create(AmountDueType amnt, String userLanguage) {
		Double minAmount = Double.MIN_VALUE;
		Double maxAmount = Double.MAX_VALUE;
		if (StringUtils.equals(PaymentModeType.INSTALLMENT.value(), amnt.getPymtMode().value())) {
			minAmount = amnt.getAmtDue().doubleValue();
			maxAmount = amnt.getAmtDue().doubleValue();
		} else {
			minAmount = amnt.getRangeInfo().getMinAmt().doubleValue();
			maxAmount = amnt.getRangeInfo().getMaxAmt().doubleValue();
		}

		BillInfo info = new BillInfo();

		info.setAmtDue(amnt.getAmtDue().doubleValue());
		info.setMinAmt(minAmount);
		info.setMaxAmt(maxAmount);
		info.setExactPmt(amnt.isExactPmt());
		info.setPymtMode(amnt.getPymtMode().value());
		info.setSequence(amnt.getSequence());
		info.setCurCode(amnt.getCurCode() == null ? "" : amnt.getCurCode().toString());

		for (ShortDescType des : amnt.getShortDesc()) {
			if (des.getLanguagePref().value().equalsIgnoreCase(userLanguage)) {
				info.setShortDesc(des.getText());
				break;
			}
		}
		if (info.getShortDesc() == null && amnt.getShortDesc().size() > 0)
			info.setShortDesc(amnt.getShortDesc().get(0).getText());
		return info;
	}
}