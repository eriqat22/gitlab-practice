package com.progressoft.mpay.plugins.hokomty.billadvise;

import com.progressoft.mpay.plugins.NotificationContext;

public class BillAdviseNotificationContext extends NotificationContext {
	private String receiver;
	private String currency;
	private String amount;
	private String senderBalance;
	private boolean isSenderBanked;
	private String senderCharges;
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSenderBalance() {
		return senderBalance;
	}
	public void setSenderBalance(String balance) {
		this.senderBalance = balance;
	}
	public boolean isSenderBanked() {
		return isSenderBanked;
	}
	public void setSenderBanked(boolean isBanked) {
		this.isSenderBanked = isBanked;
	}

	public String getSenderCharges() {
		return this.senderCharges;
	}
	public void setSenderCharges(String senderCharges) {
		this.senderCharges = senderCharges;
	}
}
