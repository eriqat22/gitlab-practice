package com.progressoft.mpay.plugins.hokomty.feesinquiry;

import java.math.BigDecimal;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_ServiceIntegMessage;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MPayResponseEnvelope;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.plugins.hokomty.common.Constants;
import com.progressoft.mpay.plugins.hokomty.common.EPayRequestsHelper;

public class FeesInquiryProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(FeesInquiryProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = FeesInquiryValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in FeesInquiryProcessor", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
		String response = generateResponse(context, false);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

		MPAY_ServiceIntegMessage integMessage = EPayRequestsHelper.GenerateFeesInquiryRequest(context);
		if (integMessage == null)
			return rejectMessage(context, ReasonCodes.SERVICE_INTEGRATION_FAILURE, "Cannot generate integration message");

		context.setServiceIntegMessage(integMessage);
		context.getDataProvider().persistServiceIntegrationMessage(integMessage);
		integMessage = EPayRequestsHelper.SendBillFee(integMessage, context);
		context.getDataProvider().updateServiceIntegrationMessage(integMessage);

		if (!integMessage.getRefStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return rejectMessage(context, context.getMessage().getReason().getCode(), context.getMessage().getReasonDesc());

		String response = generateResponse(context, true);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
		logger.debug("Inside GenerateResponse ...");
		MPayResponseEnvelope responseEnvelope = new MPayResponseEnvelope();
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), SystemParameters.getInstance().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		if (isAccepted)
			response.getExtraData().add(new ExtraData(Constants.FeesInquiryResponseFieldKey, EPayRequestsHelper.CreateFeesInquiryResponse(context)));

		responseEnvelope.setResponse(response);
		responseEnvelope.setToken(context.getCryptographer().createJsonToken(context.getSystemParameters(), response.toValues()));
		return responseEnvelope.toString();
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
		logger.debug("Inside PreProcessMessage ...");
		FeesInquiryMessage message = FeesInquiryMessage.ParseMessage(context.getRequest());
		BigDecimal amount = BigDecimal.ZERO;
		for (BillFeeInfo billInfo : message.getBillInfo())
			amount.add(BigDecimal.valueOf(billInfo.getAmtDue()));
		context.setAmount(amount);
		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
		if (sender.getMobile() == null)
			return;
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
		if(sender.getMobileAccount() == null)
			return;
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		OTPHanlder.loadOTP(context, message.getPin());
		MPAY_ServiceIntegMessage originalRequest = context.getDataProvider().getServiceIntegrationMessageByRequestId(message.getBillInqRqUId());
		if (originalRequest == null)
			return;
		context.setOriginalServiceIntegMessage(originalRequest);
		receiver.setService(originalRequest.getRefService());
		receiver.setCorporate(receiver.getService().getRefCorporate());
		receiver.setProfile(receiver.getService().getRefProfile());
		ChargesCalculator.calculateSenderCharge("EPAYADV", context, receiver.getProfile());
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		logger.debug("Inside Reverse ...");
		return null;
	}
}
