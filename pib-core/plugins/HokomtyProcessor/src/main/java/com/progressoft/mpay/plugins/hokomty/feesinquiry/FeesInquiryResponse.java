package com.progressoft.mpay.plugins.hokomty.feesinquiry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FeesInquiryResponse {
	private double totalBillsAmt;
	private double totalFeeAmt;
	private double totalAmtWithFee;
	private String feeInqRqUId;

	public String getFeeInqRqUId() {
		return feeInqRqUId;
	}

	public void setFeeInqRqUId(String feeInqRqUId) {
		this.feeInqRqUId = feeInqRqUId;
	}

	public double getTotalBillsAmt() {
		return totalBillsAmt;
	}

	public void setTotalBillsAmt(double totalBillsAmt) {
		this.totalBillsAmt = totalBillsAmt;
	}

	public double getTotalFeeAmt() {
		return totalFeeAmt;
	}

	public void setTotalFeeAmt(double totalFeeAmt) {
		this.totalFeeAmt = totalFeeAmt;
	}

	public double getTotalAmtWithFee() {
		return totalAmtWithFee;
	}

	public void setTotalAmtWithFee(double totalAmtWithFee) {
		this.totalAmtWithFee = totalAmtWithFee;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(this);
		return json;
	}
}
