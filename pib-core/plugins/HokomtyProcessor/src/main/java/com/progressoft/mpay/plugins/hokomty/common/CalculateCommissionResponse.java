
package com.progressoft.mpay.plugins.hokomty.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for calculateCommissionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calculateCommissionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{urn:BillPaymentService/types}Response"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calculateCommissionResponse", propOrder = {
    "result"
})
public class CalculateCommissionResponse {

    @XmlElement(required = true, nillable = true)
    protected Response result;

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Response }
     *     
     */
    public Response getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Response }
     *     
     */
    public void setResult(Response value) {
        this.result = value;
    }

}
