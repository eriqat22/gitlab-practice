package com.progressoft.mpay.plugins.agentcashin;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.progressoft.mpay.entities.*;
import org.apache.commons.lang.NullArgumentException;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.agentcashin.integrations.AgentCashInResponseProcessor;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class AgentCashInProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(AgentCashInProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = AgentCashInValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());

        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        if (context.getTransaction() != null) {
            TransactionHelper.reverseTransaction(context);
        }
        return MessageProcessingResult.create(context, status, reason, reasonDescription);
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException, WorkflowException {
        MessageProcessingResult result = new MessageProcessingResult();
        MPAY_ProcessingStatus status;
        MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
        String reasonDescription = null;

        context.getMessage().setReason(reason);
        MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO,
                context.getRequest().getNotes());
        result.setMessage(context.getMessage());
        context.setTransaction(transaction);
        result.setTransaction(transaction);
        context.getDataProvider().persistProcessingResult(result);

        JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), transaction);
        if (postingResult.isSuccess())
            status = handleMPClear(context, result);
        else {
            if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
                context.getMessage().setReasonDesc(postingResult.getReason());
            } else
                reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
        }

        context.getMessage().setProcessingStatus(status);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
        transaction.setReason(context.getMessage().getReason());
        transaction.setReasonDesc(context.getMessage().getReasonDesc());
        result.setStatus(status);
        result.setStatusDescription(context.getMessage().getReasonDesc());
        if (status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
            context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
                    SystemHelper.getCurrentDateWithoutTime());
        } else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
            result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
        OTPHanlder.removeOTP(context);
        handleCommissions(context, status);
        return result;
    }

    private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
        if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            return;
        CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
                CommissionDirections.SENDER);
        if (context.getDirection() == TransactionDirection.ONUS)
            CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
                    CommissionDirections.RECEIVER);
    }

    private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, MessageProcessingResult result) throws WorkflowException {
        MPAY_ProcessingStatus status;
        Integer mpClearIntegType;
        mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
        status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);

        IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, ((AgentCashInMessage) context.getRequest()).getReceiverType().equals(ReceiverInfoType.ALIAS));
        MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegType));
        messageLog.setRefMessage(context.getMessage());
        result.setMpClearMessage(messageLog);
        return status;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, SQLException, WorkflowException {
        logger.debug("inside PreProcessMessage -----------------------------");
        AgentCashInMessage message = AgentCashInMessage.parseMessage(context.getRequest());
        context.setRequest(message);
        context.setAmount(message.getAmount());
        context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        loadSender(context);
        if (context.getSender().getService() == null || context.getSender().getServiceAccount() == null)
            return;
        loadReceiver(context);
        if (context.getReceiver().getMobile() == null || context.getReceiver().getMobileAccount() == null)
            return;
        OTPHanlder.loadOTP(context, message.getPin());
        loadTransactionConfig(context);
        ChargesCalculator.calculate(context);
        TaxCalculator.claculate(context);
        handlePushNotificationToken(message, context);
    }

    private void handlePushNotificationToken(AgentCashInMessage message, ProcessingContext context) {
        MPAY_CorporateDevice senderDevice = context.getDataProvider().getCorporateDevice(message.getDeviceId());
        final String receiverDeviceToken = context.getReceiver().getMobile().getRefCustomerMobileCustomerDevices()
                .stream()
                .filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CustomerDevice::getExtraData).orElse(null);
        context.setSenderNotificationToken(senderDevice != null ? senderDevice.getExtraData() : null);
        context.setReceiverNotificationToken(receiverDeviceToken);
    }

    private void loadTransactionConfig(MessageProcessingContext context) {
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        context.setDirection(TransactionDirection.ONUS);
        context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(context.getSender().getCorporate().getClientType().getId(), context.getSender().isBanked(), personClientType,
                context.getReceiver().isBanked(), context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
        if (context.getTransactionConfig() != null)
            context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
    }

    private void loadReceiver(MessageProcessingContext context) throws SQLException {
        AgentCashInMessage message = (AgentCashInMessage) context.getRequest();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setReceiver(receiver);
        receiver.setInfo(message.getReceiverInfo());
        receiver.setMobile(context.getDataProvider().getCustomerMobile(message.getReceiverInfo(), message.getReceiverType()));
        if (receiver.getMobile() == null)
            return;
        receiver.setNotes(message.getNotes());
        receiver.setCustomer(receiver.getMobile().getRefCustomer());
        receiver.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), receiver.getMobile(), message.getReceiverAccount()));
        if (receiver.getMobileAccount() == null)
            return;
        receiver.setAccount(receiver.getMobileAccount().getRefAccount());
        receiver.setBank(receiver.getMobileAccount().getBank());
        receiver.setProfile(receiver.getMobile().getRefProfile());
        receiver.setBanked(receiver.getAccount().getIsBanked());
        receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
        receiver.setLimits(context.getDataProvider().getAccountLimits(receiver.getAccount().getId(), context.getOperation().getMessageType().getId()));
    }

    private void loadSender(MessageProcessingContext context) throws SQLException {
        AgentCashInMessage message = (AgentCashInMessage) context.getRequest();
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        sender.setInfo(message.getSender());
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return;
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), message.getSenderAccount()));
        if (sender.getServiceAccount() == null)
            return;
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setBank(sender.getServiceAccount().getBank());
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        sender.setBanked(sender.getAccount().getIsBanked());
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        try {
            logger.debug("Inside ProcessIntegration ...");
            if (context.getMpClearIsoMessage().getType() == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE) {
                IntegrationProcessingResult result = AgentCashInResponseProcessor.processIntegration(context);
                handleCommissions(context, result.getProcessingStatus());
                return result;
            } else
                throw new WorkflowException("Operation Not Supported");
        } catch (Exception e) {
            logger.error("Error when ProcessIntegration in AgentCashInProcessor", e);
            return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null, false);
        }
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside Reverse ...");
        try {
            AgentCashInMessage message = AgentCashInMessage.parseMessage(context.getRequest());
            JVPostingResult result = TransactionHelper.reverseTransaction(context);
            context.getDataProvider().updateAccountLimit(
                    SystemHelper.getMobileAccount(context.getSystemParameters(), context.getReceiver().getMobile(),
                            message.getReceiverAccount()).getRefAccount().getId(),
                    context.getMessage().getRefOperation().getMessageType().getId(),
                    context.getTransaction().getTotalAmount().negate(),
                    -1,
                    SystemHelper.getCurrentDateWithoutTime());

            context.getDataProvider().mergeTransaction(context.getTransaction());
            if (result.isSuccess())
                return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
            return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
        } catch (Exception ex) {
            logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
            return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(),
                    ProcessingStatusCodes.FAILED);
        }
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                 String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        if (context.getMessage() != null) {
            context.getMessage().setReason(reason);
            context.getMessage().setReasonDesc(reasonDescription);
            context.getMessage().setProcessingStatus(status);
        }
        if (context.getTransaction() != null) {
            context.getTransaction().setReason(reason);
            context.getTransaction().setReasonDesc(reasonDescription);
            context.getTransaction().setProcessingStatus(status);
        }
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setTransaction(context.getTransaction());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        if (reasonCode.equals(ReasonCodes.VALID)) {
            result.setNotifications(NotificationProcessor.createReversalNotificationMessages(context));
        }
        return result;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isReceiverAlias) {
        logger.debug("Inside CreateMPClearRequest ...");
        try {
            String messageID = context.getDataProvider().getNextMPClearMessageId();
            BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
            IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(type);
            message.setBinary(false);

            String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
            String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

            message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
            MPClearHelper.setMessageEncoded(message);
            message.setField(7, new IsoValue<String>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
            message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
            message.setField(49, new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

            String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getService(), context.getSender().getServiceAccount(), false);
            MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

            String account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getMobile(), context.getReceiver().getMobileAccount(), isReceiverAlias);
            MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

            String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
            message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
            message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

            return message;
        } catch (Exception e) {
            logger.error("Error when CreateMPClearRequest in AgentCashInProcessor", e);
            return null;
        }
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }
}
