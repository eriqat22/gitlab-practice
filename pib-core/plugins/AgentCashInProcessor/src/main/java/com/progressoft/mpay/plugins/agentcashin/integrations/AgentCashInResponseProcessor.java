package com.progressoft.mpay.plugins.agentcashin.integrations;

import static ch.lambdaj.Lambda.having;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.jfw.shared.exception.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entities.MPAY_ClientsOTP;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.PluginsCommonConstants;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.agentcashin.AgentCashInMessage;
import com.progressoft.mpay.plugins.agentcashin.NotificationProcessor;

public class AgentCashInResponseProcessor {
    private static final Logger logger = LoggerFactory.getLogger(AgentCashInResponseProcessor.class);

    private AgentCashInResponseProcessor() {

    }

    public static IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) throws WorkflowException {
        try {
            logger.debug("Inside ProcessIntegration ...");
            preProcessIntegration(context);
            if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return null;
            if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return accept(context);
            else
                return reject(context);
        } catch (Exception ex) {
            logger.error("Error when ProcessIntegration in AgentCashInResponseProcessor", ex);
            return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), null, false);
        }
    }

    private static void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
        logger.debug("Inside PreProcessIntegration ...");
        context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        AgentCashInMessage request = null;
        try {
            request = AgentCashInMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
        } catch (MessageParsingException ex) {
            logger.error("Error while parsing message", ex);
            throw new WorkflowException(ex);
        }
        context.setAmount(request.getAmount());
        context.setSender(sender);
        context.setReceiver(receiver);
        sender.setService(context.getTransaction().getSenderService());
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(context.getTransaction().getSenderServiceAccount());
        sender.setAccount(context.getSender().getServiceAccount().getRefAccount());
        sender.setBanked(sender.getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
        sender.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(), sender.getAccount().getAccNumber()));
        sender.setInfo(sender.getService().getName());
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

        receiver.setMobile(context.getTransaction().getReceiverMobile());
        receiver.setMobileAccount(context.getTransaction().getReceiverMobileAccount());
        receiver.setAccount(context.getReceiver().getMobileAccount().getRefAccount());
        receiver.setCustomer(receiver.getMobile().getRefCustomer());
        receiver.setBanked(receiver.getMobileAccount().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
        receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(), receiver.getAccount().getAccNumber()));
        receiver.setProfile(receiver.getMobileAccount().getRefProfile());
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
        context.setTransactionConfig(
                context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(), sender.isBanked(), context.getLookupsLoader().getCustomerClientType().getId(), receiver.isBanked(), context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT, context.getOperation().getId()));
        if (request.getReceiverPin() != null)
            context.getExtraData().put(PluginsCommonConstants.OTP_KEY, context.getDataProvider().getOtpClientByCode(request.getReceiverPin()));

        handlePushNotificationToken(request, context);
    }


    private static void handlePushNotificationToken(AgentCashInMessage message, ProcessingContext context) {
        MPAY_CorporateDevice senderDevice = context.getDataProvider().getCorporateDevice(message.getDeviceId());
        final String receiverDeviceToken = context.getReceiver().getMobile().getRefCustomerMobileCustomerDevices()
                .stream()
                .filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
                .findFirst()
                .map(MPAY_CustomerDevice::getExtraData).orElse(null);
        context.setSenderNotificationToken(senderDevice != null ? senderDevice.getExtraData() : null);
        context.setReceiverNotificationToken(receiverDeviceToken);
    }

    private static IntegrationProcessingResult accept(IntegrationProcessingContext context) throws WorkflowException, SQLException {
        logger.debug("Inside Accept ...");
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
        result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
        removeOTP(context);
        return result;
    }

    private static void removeOTP(IntegrationProcessingContext context) {
        MPAY_ClientsOTP otp = (MPAY_ClientsOTP) context.getExtraData().get(PluginsCommonConstants.OTP_KEY);
        if (otp == null)
            return;
        context.getDataProvider().deleteClientOTP(otp);
    }

    private static IntegrationProcessingResult reject(IntegrationProcessingContext context) throws SQLException {
        logger.debug("Inside Reject ...");
        BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
        if (integrationResult != null)
            TransactionHelper.reverseTransaction(context);

        context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(), BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
        String reasonDescription = null;
        if (context.getMpClearIsoMessage().getField(47) != null)
            reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, ReasonCodes.REJECTED_BY_NATIONAL_SWITCH, context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
        result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
        return result;
    }
}