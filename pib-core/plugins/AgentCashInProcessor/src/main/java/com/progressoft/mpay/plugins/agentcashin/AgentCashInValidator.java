package com.progressoft.mpay.plugins.agentcashin;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.AmountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileNumberValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PSPValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;
import com.progressoft.mpay.plugins.validators.WalletCapValidator;

public class AgentCashInValidator {
	private static final Logger logger = LoggerFactory.getLogger(AgentCashInValidator.class);

	private AgentCashInValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;
		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		AgentCashInMessage request = (AgentCashInMessage) context.getRequest();

		result = validateSenderInfo(context, request);
		if (!result.isValid())
			return result;

		if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
			result = MobileNumberValidator.validate(context, request.getReceiverInfo());
			if (!result.isValid())
				return result;
		}

		result = validateReceiver(context);
		if (!result.isValid())
			return result;

		return validateSystemSettings(context, request);
	}

	private static ValidationResult validateSystemSettings(MessageProcessingContext context, AgentCashInMessage request) {
		ValidationResult result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context.getReceiver(), context.getAmount(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		return PSPValidator.validate(context, false);
	}

	private static ValidationResult validateSenderInfo(MessageProcessingContext context, AgentCashInMessage request) {
		logger.debug("Inside validateSenderInfo ...");
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = PinCodeValidator.validate(context, context.getSender().getService(), context.getRequest().getPin());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), request.getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		return SignInValidator.validate(context, context.getSender().getService(), context.getRequest().getDeviceId());
	}

	private static ValidationResult validateReceiver(MessageProcessingContext context) {
		logger.debug("Inside ValidateReceiver ...");

		ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getMobileAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return WalletCapValidator.validate(context);
	}
}
