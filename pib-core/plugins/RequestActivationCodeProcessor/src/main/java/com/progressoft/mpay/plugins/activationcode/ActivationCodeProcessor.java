package com.progressoft.mpay.plugins.activationcode;

import com.progressoft.mpay.HashingAlgorithm;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.OTPGenerator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.common.DateUtils;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class ActivationCodeProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(ActivationCodeProcessor.class);

    @Override
    public IntegrationProcessingResult processIntegration( IntegrationProcessingContext arg0 ) {
        logger.debug("Inside ProcessIntegration ...");
        return null;
    }

    @Override
    public MessageProcessingResult processMessage( MessageProcessingContext context ) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = RequestActivationMessageValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Invalid Message Received", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error in ProcessMessage", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage( MessageProcessingContext context, String reasonCode,
                                                   String reasonDescription ) {
        logger.debug("Inside RejectMessage ...");
        return createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
    }

    private MessageProcessingResult acceptMessage( MessageProcessingContext context ) {
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        int activationCodeLength = context.getSystemParameters().getActivationCodeLength();
        String activationCode = OTPGenerator.generateCode(activationCodeLength);
        Timestamp validityTime = DateUtils.getValidityTime(context.getSystemParameters().getActivationCodeValidity());
        if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)) {
            context.getSender().getMobile()
                    .setActivationCode(HashingAlgorithm.hash(context.getSystemParameters(), activationCode));
            context.getSender().getMobile().setActivationCodeValidy(validityTime);
            context.getDataProvider().mergeCustomerMobile(context.getSender().getMobile());
        } else {
            context.getSender().getService()
                    .setActivationCode(HashingAlgorithm.hash(context.getSystemParameters(), activationCode));
            context.getSender().getService().setActivationCodeValidy(validityTime);
            context.getDataProvider().mergeCorporateService(context.getSender().getService());
        }
        result.getNotifications().addAll(createNotification(context, activationCode));
        OTPHanlder.removeOTP(context);
        return result;
    }

    private List<MPAY_Notification> createNotification( MessageProcessingContext context, String activationCode ) {
        logger.debug("Inside CreateNotification ...");
        RequestActivationNotificationContext notificationContext = new RequestActivationNotificationContext();
        notificationContext.setReference(context.getMessage().getReference());
        notificationContext.setActivationCode(activationCode);
        List<MPAY_Notification> list = new ArrayList<>();
        try {
            String expiry;
            if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)) {
                expiry = getValidityTime(context.getSender().getMobile().getActivationCodeValidy());
                notificationContext.setExtraData1(context.getSender().getMobile().getMobileNumber());
                notificationContext.setExtraData3(expiry);

                MPAY_Language prefLang = context.getSender().getCustomer().getPrefLang();
                addNotification(context.getMessage(), list, context.getSender().getMobile().getEmail(), prefLang,
                        context.getSender().getMobile().getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER,
                        NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), list, context.getSender().getMobile().getMobileNumber(), prefLang,
                        context.getSender().getMobile().getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER,
                        NotificationChannelsCode.SMS);
            } else {
                final MPAY_Language prefLang = context.getSender().getCorporate().getPrefLang();
                notificationContext.setExtraData1(context.getSender().getService().getName());
                notificationContext.setExtraData2(String.valueOf(prefLang.getId()));
                expiry = getValidityTime(context.getSender().getService().getActivationCodeValidy());
                notificationContext.setExtraData3(expiry);
                addNotification(context.getMessage(), list, context.getSender().getService().getEmail(), prefLang,
                        context.getSender().getService().getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), list, context.getSender().getService().getMobileNumber(), prefLang,
                        context.getSender().getService().getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);

            }
            return list;
        } catch (Exception e) {
            logger.error("Error in CreateNotification", e);
        }
        return list;
    }

    private String getValidityTime( Timestamp activationCodeValidy ) {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(activationCodeValidy);
    }

    private void preProcessMessage( MessageProcessingContext context ) throws MessageParsingException, SQLException {
        logger.debug("Inside PreProcessMessage ...");
        RequestActivationMessage message = RequestActivationMessage.parseMessage(context.getRequest());
        context.setRequest(message);
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
            sender.setMobile(context.getDataProvider().getCustomerMobile(message.getSender()));
            if (sender.getMobile() == null)
                return;
            sender.setCustomer(sender.getMobile().getRefCustomer());
            sender.setMobileAccount(
                    SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
            if (sender.getMobileAccount() == null)
                return;
            sender.setProfile(sender.getMobileAccount().getRefProfile());
            sender.setAccount(sender.getMobileAccount().getRefAccount());
            if (sender.getAccount() != null)
                sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
                        context.getOperation().getMessageType().getId()));
        } else {
            sender.setService(
                    context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
            if (sender.getService() == null)
                return;
            sender.setCorporate(sender.getService().getRefCorporate());
            sender.setServiceAccount(
                    SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
            if (sender.getServiceAccount() == null)
                return;
            sender.setAccount(sender.getServiceAccount().getRefAccount());
            sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
                    context.getOperation().getMessageType().getId()));
            sender.setProfile(sender.getServiceAccount().getRefProfile());
        }
        OTPHanlder.loadOTP(context, message.getPin());
        context.getExtraData().put(ActivationCodeConstants.NotificationTokenKey, message.getNotificationToken());
    }

    public MessageProcessingResult createResult( MessageProcessingContext context, String reasonCode,
                                                 String reasonDescription, String processingStatus ) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    @Override
    public MessageProcessingResult accept( MessageProcessingContext arg0 ) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject( MessageProcessingContext arg0 ) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse( MessageProcessingContext arg0 ) {
        logger.debug("Inside Reverse ...");
        return null;
    }

    @Override
    public boolean isNeedToSessionId() {
        return false;
    }
}