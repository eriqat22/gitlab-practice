package com.progressoft.mpay.plugins.activationcode;

import com.progressoft.mpay.plugins.NotificationContext;

public class RequestActivationNotificationContext extends NotificationContext {
	private String activationCode;

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
}
