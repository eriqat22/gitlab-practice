package com.progressoft.mpay.plugins.activationcode;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class ClientIDValidator {
	private static final Logger logger = LoggerFactory.getLogger(ClientIDValidator.class);
	private ClientIDValidator() {

	}
	public static ValidationResult validate(MessageProcessingContext context, String clientId) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		if (clientId == null)
			throw new NullArgumentException("clientId");
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)) {
			if (context.getSender().getCustomer() == null)
				return new ValidationResult(ReasonCodes.VALID, null, true);
			if (!context.getSender().getCustomer().getIdNum().equals(clientId))
				return new ValidationResult(ReasonCodes.INVALID_CLIENT_ID_NUMBER, null, false);
		} else {
			if (context.getSender().getCorporate() == null)
				return new ValidationResult(ReasonCodes.VALID, null, true);
			if (!context.getSender().getCorporate().getRegistrationId().equals(clientId))
				return new ValidationResult(ReasonCodes.INVALID_CLIENT_ID_NUMBER, null, false);
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
