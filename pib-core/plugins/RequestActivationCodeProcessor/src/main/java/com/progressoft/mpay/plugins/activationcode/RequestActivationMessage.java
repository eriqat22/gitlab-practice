package com.progressoft.mpay.plugins.activationcode;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class RequestActivationMessage extends MPayRequest {
	private static final String CLIENT_ID_KEY = "id";
	private static final String NOTIFICATION_TOKEN_KEY = "notificationToken";

	private String id;
	private String notificationToken;

	public RequestActivationMessage() {
		//
	}

	public RequestActivationMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNotificationToken() {
		return notificationToken;
	}

	public void setNotificationToken(String notificationToken) {
		this.notificationToken = notificationToken;
	}

	public static RequestActivationMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		RequestActivationMessage message = new RequestActivationMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setId(message.getValue(CLIENT_ID_KEY));
		if (message.getId() == null)
			throw new MessageParsingException(CLIENT_ID_KEY);
		message.setNotificationToken(message.getValue(NOTIFICATION_TOKEN_KEY));
		return message;
	}
}
