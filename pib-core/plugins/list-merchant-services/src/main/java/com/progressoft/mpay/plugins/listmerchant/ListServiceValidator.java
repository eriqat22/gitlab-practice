package com.progressoft.mpay.plugins.listmerchant;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;

public class ListServiceValidator {
	private static final Logger logger = LoggerFactory.getLogger(ListServiceValidator.class);

	private ListServiceValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,
				context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(),
				context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
		if (!result.isValid())
			return result;

		if (!context.getMessage().getMessageType().getIsPinlessEnabled()) {
			result = PinCodeValidator.validate(context, context.getSender().getMobile(), context.getRequest().getPin());
			if (!result.isValid())
				return result;
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}