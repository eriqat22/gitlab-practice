package com.progressoft.mpay.plugins.listmerchant;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class ListServicesMessage extends MPayRequest {

	private static final String FIRST_THREE_LETTERS = "firstThreeLetters";

	private String firstThreeLetters;

	public ListServicesMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static ListServicesMessage parseMessage(MPayRequest request) throws MessageParsingException {
		ListServicesMessage message = new ListServicesMessage(request);

		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
			throw new MessageParsingException(SENDER_TYPE_KEY);

		message.setFirstThreeLetters(message.getValue(FIRST_THREE_LETTERS));
		if (message.getFirstThreeLetters() == null || message.getFirstThreeLetters().trim().isEmpty()
				|| message.getFirstThreeLetters().length() != 3)
			throw new MessageParsingException(FIRST_THREE_LETTERS);

		return message;
	}

	public String getFirstThreeLetters() {
		return firstThreeLetters;
	}

	public void setFirstThreeLetters(String firstThreeLetters) {
		this.firstThreeLetters = firstThreeLetters;
	}

}