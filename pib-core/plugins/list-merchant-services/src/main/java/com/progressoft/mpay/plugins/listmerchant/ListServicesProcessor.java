package com.progressoft.mpay.plugins.listmerchant;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;

public class ListServicesProcessor implements MessageProcessor {

	private static final Logger logger = LoggerFactory.getLogger(ListServicesProcessor.class);

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessMessage(context);
			ValidationResult validationResult = ListServiceValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Error While parse message", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}

	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
			String reasonDescription) {
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
				ProcessingStatusCodes.REJECTED);
		String response = generateResponse(context, false, null);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		ListServicesMessage message = (ListServicesMessage) context.getRequest();
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
		List<MPAY_CorpoarteService> servicesList = context.getDataProvider()
				.listServiceByFirstThreeLetters(message.getFirstThreeLetters());
		String response = "";
		response = generateResponse(context, true, servicesList);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted,
			List<MPAY_CorpoarteService> servicesList) {
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
				context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
					context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		if (isAccepted)
			response.setExtraData(generateExtraData(context, servicesList));
		return response.toString();
	}

	private List<ExtraData> generateExtraData(MessageProcessingContext context,
			List<MPAY_CorpoarteService> servicesList) {
		List<ExtraData> list = new ArrayList<ExtraData>();
		List<ListServiceResponse> services = new ArrayList<ListServiceResponse>();
		ExtraData serviceList = new ExtraData("serviceList", "");
		if (!servicesList.isEmpty()) {
			for (MPAY_CorpoarteService service : servicesList) {
				ListServiceResponse listServiceResponse = new ListServiceResponse();
				listServiceResponse.setName(service.getName());
				listServiceResponse.setDescription(service.getDescription());
				services.add(listServiceResponse);
			}
		}

		String servicesJson = new Gson().toJson(services);
		serviceList.setValue(servicesJson);
		list.add(serviceList);
		return list;
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
		logger.debug("Inside PreProcessMessage ...");
		ListServicesMessage message = ListServicesMessage.parseMessage(context.getRequest());
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setRequest(message);
		
		
		sender.setMobile(context.getDataProvider().getCustomerMobile(message.getSender()));
        if (sender.getMobile() == null)
            return;
        sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), message.getSenderAccount()));
        if (sender.getMobileAccount() == null)
            return;
        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setAccount(sender.getMobileAccount().getRefAccount());
        sender.setProfile(sender.getMobileAccount().getRefProfile());
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
			String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext context) {
		return null;
	}

}