package com.progressoft.mpay.plugins.merchantpay;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.messages.TransactionTypeCodes;

import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;


public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    private NotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateAcceptanceNotificationMessages ...");
        try {
            String receiver;
            String sender;
            String reference;
            long senderLanguageId;
            long receiverLanguageId;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();

            MPAY_CustomerMobile senderMobile;
            MPAY_CorpoarteService receiverService;
            MPAY_Account senderAccount;
            MPAY_Account receiverAccount;

            if (context.getTransaction().getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT)) {
                senderMobile = context.getSender().getMobile();
                senderAccount = context.getSender().getMobileAccount().getRefAccount();
                receiverService = context.getReceiver().getService();
                receiverAccount = context.getReceiver().getServiceAccount().getRefAccount();
            } else {
                senderMobile = context.getReceiver().getMobile();
                senderAccount = context.getReceiver().getMobileAccount().getRefAccount();
                receiverService = context.getSender().getService();
                receiverAccount = context.getSender().getServiceAccount().getRefAccount();
            }
            context.getDataProvider().refreshEntity(senderAccount);
            context.getDataProvider().refreshEntity(receiverAccount);
            MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

            MerchantPayNotificationContext notificationContext = new MerchantPayNotificationContext();
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setOperation(operation.getOperation());
            notificationContext.setReference(reference);
            if (senderMobile != null) {
                sender = SystemHelper.maskMobileNumber(context.getSystemParameters(), senderMobile.getMobileNumber());
                notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
                notificationContext.setSenderBanked(senderAccount.getIsBanked());
                notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
                notificationContext.setSenderTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getTax()));
                senderLanguageId = senderMobile.getRefCustomer().getPrefLang().getId();
                if (senderMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS) && senderMobile.getAlias() != null && !senderMobile.getAlias().isEmpty())
                    sender = senderMobile.getAlias();
            } else {
                sender = SystemHelper.maskMobileNumber(context.getSystemParameters(), context.getSender().getInfo());
                senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
            }
            if (receiverService != null) {
                receiver = receiverService.getName();

                notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
                notificationContext.setReceiverBanked(receiverAccount.getIsBanked());
                notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
                notificationContext.setReceiverTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getTax()));
                receiverLanguageId = receiverService.getRefCorporate().getPrefLang().getId();
            } else {
                receiver = context.getReceiver().getInfo();
                receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
            }
            notificationContext.setReceiver(receiver);
            notificationContext.setSender(sender);
            return generateNotifications(context, senderLanguageId, receiverLanguageId, senderMobile, receiverService, notificationContext);

        } catch (Exception e) {
            logger.error("Error when CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    private static List<MPAY_Notification> generateNotifications(ProcessingContext context, long senderLanguageId, long receiverLanguageId, MPAY_CustomerMobile senderMobile,
                                                                 MPAY_CorpoarteService receiverService, MerchantPayNotificationContext notificationContext) {
        List<MPAY_Notification> notifications = new ArrayList<>();
        if (senderMobile != null) {
            List<String> deviceTokens = context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_CREDIT)
                    ? context.getSenderDeviceTokens() : context.getReceiverDeviceTokens();

            notificationContext.setExtraData1(senderMobile.getMobileNumber());
            notificationContext.setExtraData2(String.valueOf(senderLanguageId));
            final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
            addNotification(context.getMessage(), notifications, senderMobile.getEmail(), prefLang,
                    senderMobile.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
            addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
                    senderMobile.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
            deviceTokens.forEach(token -> addNotification(context.getMessage(), notifications, token, prefLang,
                    senderMobile.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION));

        }
        if (receiverService != null) {
            notificationContext.setExtraData1(receiverService.getName());
            List<String> deviceTokens = context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_CREDIT)
                    ? context.getReceiverDeviceTokens() : context.getSenderDeviceTokens();
            final MPAY_Language prefLang = receiverService.getRefCorporate().getPrefLang();
            notificationContext.setExtraData2(String.valueOf(receiverLanguageId));
            addNotification(context.getMessage(), notifications, receiverService.getEmail(), prefLang,
                    receiverService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.EMAIL);
            addNotification(context.getMessage(), notifications, receiverService.getMobileNumber(), prefLang,
                    receiverService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.SMS);
            deviceTokens.forEach(token -> addNotification(context.getMessage(), notifications, token, prefLang,
                    receiverService.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_RECEIVER,
                    NotificationChannelsCode.PUSH_NOTIFICATION));
        }
        return notifications;
    }

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<String> deviceTokens = context.getSenderDeviceTokens();
            String senderMobileNumber;
            String senderEmail;
            Boolean enableSms;
            Boolean enableEmail;
            Boolean enablePushNotification;
            String extraData;
            MPAY_Language prefLang;

            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            MerchantPayMessage request = MerchantPayMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();

            if (request.getSenderType().equals(ReceiverInfoType.MOBILE)) {
                final MPAY_CustomerMobile customerMobile = context.getSender().getMobile();
                senderMobileNumber = customerMobile.getMobileNumber();
                senderEmail = customerMobile.getEmail();
                enableEmail = customerMobile.getEnableEmail();
                enablePushNotification = customerMobile.getEnablePushNotification();
                enableSms = customerMobile.getEnableSMS();
                prefLang = customerMobile.getRefCustomer().getPrefLang();
                extraData = customerMobile.getMobileNumber();
                MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
                if (mobile != null) {
                    language = mobile.getRefCustomer().getPrefLang();
                }
            } else {
                MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
                final MPAY_CorpoarteService corporateService = context.getSender().getService();
                senderEmail = corporateService.getEmail();
                senderMobileNumber = corporateService.getMobileNumber();
                enableEmail = corporateService.getEnableEmail();
                enablePushNotification = corporateService.getEnablePushNotification();
                enableSms = corporateService.getEnableSMS();
                prefLang = corporateService.getRefCorporate().getPrefLang();
                extraData = corporateService.getName();
                if (service == null)
                    return new ArrayList<>();
                language = service.getRefCorporate().getPrefLang();
            }

            MerchantPayNotificationContext notificationContext = new MerchantPayNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            if (context.getTransaction() == null)
                notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
            else
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
            notificationContext.setReceiver(request.getReceiverInfo());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(Objects.requireNonNull(MPayHelper.getReasonNLS(context.getMessage().getReason(), language)).getDescription());
            notificationContext.setExtraData1(extraData);
            notificationContext.setExtraData2(String.valueOf(language.getId()));

            addNotification(context.getMessage(), notifications, senderEmail, prefLang,
                    enableEmail, notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
            addNotification(context.getMessage(), notifications, senderMobileNumber, prefLang,
                    enableSms, notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
            deviceTokens.forEach(token -> addNotification(context.getMessage(), notifications, token, prefLang,
                    enablePushNotification, notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION));

            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(MessageProcessingContext context) {
        try {
            logger.debug("Inside CreateReversalNotificationMessages ...");
            if (TransactionTypeCodes.DIRECT_CREDIT.equals(context.getTransaction().getRefType().getCode()))
                return createCreditReversalNotificationMessages(context);
            else if (TransactionTypeCodes.DIRECT_DEBIT.equals(context.getTransaction().getRefType().getCode()))
                return createDebitReversalNotificationMessages(context);
            else
                throw new NotSupportedException("transaction type not supported, code: " + context.getTransaction().getRefType().getCode());
        } catch (Exception e) {
            logger.error("Error when CreateReversalNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    private static List<MPAY_Notification> createDebitReversalNotificationMessages(MessageProcessingContext context) {
        String reference;
        List<MPAY_Notification> notifications = new ArrayList<>();

        reference = context.getTransaction().getReference();
        MPAY_CustomerMobile receiverMobile = context.getTransaction().getReceiverMobile();
        MPAY_CorpoarteService senderService = context.getTransaction().getSenderService();
        String currency = context.getTransaction().getCurrency().getStringISOCode();

        if (senderService != null) {
            MerchantPayNotificationContext senderNotificationContext = new MerchantPayNotificationContext();
            senderNotificationContext.setCurrency(currency);
            senderNotificationContext.setReference(reference);
            MPAY_Account senderAccount = context.getTransaction().getSenderServiceAccount().getRefAccount();
            context.getDataProvider().refreshEntity(senderAccount);
            senderNotificationContext.setReceiverBanked(senderAccount.getIsBanked());
            senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
            final MPAY_Language prefLang = senderService.getRefCorporate().getPrefLang();
            final long senderLanguageId = prefLang.getId();

            senderNotificationContext.setExtraData1(senderService.getName());
            senderNotificationContext.setExtraData2(String.valueOf(senderLanguageId));
            addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
                    senderService.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
            addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
                    senderService.getEnableEmail(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
            context.getSenderDeviceTokens().forEach(token -> addNotification(context.getMessage(), notifications, token, prefLang,
                    senderService.getEnablePushNotification(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL,
                    NotificationChannelsCode.PUSH_NOTIFICATION));

        }
        if (receiverMobile != null) {
            MerchantPayNotificationContext receiverNotificationContext = new MerchantPayNotificationContext();
            receiverNotificationContext.setCurrency(currency);
            receiverNotificationContext.setReference(reference);
            MPAY_Account receiverAccount = context.getTransaction().getReceiverMobileAccount().getRefAccount();
            context.getDataProvider().refreshEntity(receiverAccount);
            receiverNotificationContext.setReceiverBanked(receiverAccount.getIsBanked());
            receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
            receiverNotificationContext.setExtraData1(receiverMobile.getMobileNumber());

            final MPAY_Language prefLang = Objects.requireNonNull(senderService).getRefCorporate().getPrefLang();
            addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
                    senderService.getEnableEmail(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
            addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
                    senderService.getEnableSMS(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
            context.getReceiverDeviceTokens().forEach(token -> addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
                    senderService.getEnablePushNotification(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL,
                    NotificationChannelsCode.PUSH_NOTIFICATION));

        }
        return notifications;
    }

    private static List<MPAY_Notification> createCreditReversalNotificationMessages(MessageProcessingContext context) {
        String reference;
        List<MPAY_Notification> notifications = new ArrayList<>();

        reference = context.getTransaction().getReference();
        MPAY_CustomerMobile senderMobile = context.getTransaction().getSenderMobile();
        MPAY_CorpoarteService receiverService = context.getTransaction().getReceiverService();
        String currency = context.getTransaction().getCurrency().getStringISOCode();

        if (senderMobile != null) {
            MerchantPayNotificationContext senderNotificationContext = new MerchantPayNotificationContext();
            senderNotificationContext.setCurrency(currency);
            senderNotificationContext.setReference(reference);
            MPAY_Account senderAccount = context.getTransaction().getSenderMobileAccount().getRefAccount();
            context.getDataProvider().refreshEntity(senderAccount);
            senderNotificationContext.setReceiverBanked(senderAccount.getIsBanked());
            senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
            senderNotificationContext.setExtraData1(senderMobile.getMobileNumber());

            final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
            addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
                    senderMobile.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
            addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
                    senderMobile.getEnablePushNotification(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.PUSH_NOTIFICATION);
            addNotification(context.getMessage(), notifications, senderMobile.getEmail(), prefLang,
                    senderMobile.getEnableEmail(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);

        }
        if (receiverService != null) {
            final String deviceToken = context.getReceiverNotificationToken();
            MerchantPayNotificationContext receiverNotificationContext = new MerchantPayNotificationContext();
            receiverNotificationContext.setCurrency(currency);
            receiverNotificationContext.setReference(reference);
            MPAY_Account receiverAccount = context.getTransaction().getReceiverServiceAccount().getRefAccount();
            receiverNotificationContext.setReceiverBanked(receiverAccount.getIsBanked());
            context.getDataProvider().refreshEntity(receiverAccount);
            receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
            receiverNotificationContext.setExtraData1(receiverService.getName());

            final MPAY_Language prefLang = receiverService.getRefCorporate().getPrefLang();
            receiverNotificationContext.setExtraData2(String.valueOf(prefLang.getId()));
            addNotification(context.getMessage(), notifications, deviceToken, prefLang,
                    receiverService.getEnablePushNotification(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.PUSH_NOTIFICATION);
            addNotification(context.getMessage(), notifications, receiverService.getMobileNumber(), prefLang,
                    receiverService.getEnableSMS(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
            addNotification(context.getMessage(), notifications, receiverService.getEmail(), prefLang,
                    receiverService.getEnableEmail(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);

        }
        return notifications;
    }
}
