package com.progressoft.mpay.plugins.merchantpay.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class MerchantPayReversalAdviseProcessor extends IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MerchantPayReversalAdviseProcessor.class);
	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

}
