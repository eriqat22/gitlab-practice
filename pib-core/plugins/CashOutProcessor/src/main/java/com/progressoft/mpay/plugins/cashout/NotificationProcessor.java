package com.progressoft.mpay.plugins.cashout;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateAcceptanceNotificationMessages ...");
			String reference;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();
			MPAY_CustomerMobile senderMobile = context.getSender().getMobile();

			CashOutNotificationContext notificationContext = new CashOutNotificationContext();
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setReference(reference);
			context.getDataProvider().refreshEntity(context.getSender().getAccount());
			notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getAccount().getBalance()));
			notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
			notificationContext.setSenderTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getTax()));
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (senderMobile != null) {
				notificationContext.setExtraData1(senderMobile.getMobileNumber());
				final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
				addNotification(context.getMessage(), notifications, senderMobile.getEmail(), prefLang,
						senderMobile.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
				addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
						senderMobile.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
				addNotification(context.getMessage(), notifications,  context.getSenderNotificationToken(), prefLang,
						senderMobile.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER,
						NotificationChannelsCode.PUSH_NOTIFICATION);

			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error in CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			CashOutMessage request = CashOutMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String senderMobile = request.getSender();
			String customerEmail = null;
			boolean enableSms = false;
			boolean enableEmail = false;
			boolean enablePushNotification = false;
			MPAY_Language language = context.getSystemParameters().getSystemLanguage();
			MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(senderMobile);
			if (mobile != null) {
				customerEmail = mobile.getEmail();
				enableEmail = mobile.getEnableEmail();
				enablePushNotification = mobile.getEnablePushNotification();
				enableSms = mobile.getEnableSMS();
				senderMobile = mobile.getMobileNumber();
				language = mobile.getRefCustomer().getPrefLang();
			}
			final String deviceToken = context.getSenderNotificationToken();
			CashOutNotificationContext notificationContext = new CashOutNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
			notificationContext.setExtraData1(senderMobile);

			addNotification(context.getMessage(), notifications, customerEmail, language,
					enableEmail, notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
			addNotification(context.getMessage(), notifications, senderMobile, language,
					enableSms, notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
			addNotification(context.getMessage(), notifications, deviceToken, language,
					enablePushNotification, notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.PUSH_NOTIFICATION);
			return notifications;
		} catch (Exception e) {
			logger.error("Error in CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	public static List<MPAY_Notification> createReversalNotificationMessages(MessageProcessingContext context) {
		logger.debug("Inside CreateReversalNotificationMessages ...");
		try {
			String reference;
			List<MPAY_Notification> notifications = new ArrayList<>();

			reference = context.getTransaction().getReference();
			MPAY_CustomerMobile senderMobile = context.getTransaction().getSenderMobile();
			String currency = context.getTransaction().getCurrency().getStringISOCode();
			if (senderMobile != null) {
				CashOutNotificationContext senderNotificationContext = new CashOutNotificationContext();
				senderNotificationContext.setCurrency(currency);
				senderNotificationContext.setReference(reference);
				MPAY_Account senderAccount = context.getTransaction().getSenderMobileAccount().getRefAccount();
				context.getDataProvider().refreshEntity(senderAccount);
				senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
				senderNotificationContext.setExtraData1(senderMobile.getMobileNumber());

				final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
				addNotification(context.getMessage(), notifications, senderMobile.getEmail(), prefLang,
						senderMobile.getEnableEmail(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
				addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
						senderMobile.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
				addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
						senderMobile.getEnablePushNotification(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL,
						NotificationChannelsCode.PUSH_NOTIFICATION);
			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateReversalNotificationMessages", e);
			return new ArrayList<>();
		}
	}
}
