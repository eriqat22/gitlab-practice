package com.progressoft.mpay.plugins.cashout;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemNetworkStatus;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.AmountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;
import com.progressoft.mpay.plugins.validators.WalletCapValidator;

public class CashOutValidator {
	private static final Logger logger = LoggerFactory.getLogger(CashOutValidator.class);

	private CashOutValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = validateSenderInfo(context);
		if (!result.isValid())
			return result;

		result = validateReceiverInfo(context);
		if (!result.isValid())
			return result;

		result = TransactionConfigValidator.validate(context.getTransactionConfig(), context.getAmount());
		if (!result.isValid())
			return result;

		result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		if (!SystemNetworkStatus.isSystemOnline(context.getDataProvider()))
			return new ValidationResult(ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE, null, false);

		return LimitsValidator.validate(context);
	}

	private static ValidationResult validateReceiverInfo(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getServiceAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return WalletCapValidator.validate(context);
	}

	private static ValidationResult validateSenderInfo(MessageProcessingContext context) {
		ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		return AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
	}
}
