package com.progressoft.mpay.plugins.cashout;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.progressoft.mpay.entities.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.cashout.integrations.CashOutResponseProcessor;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class CashOutProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(CashOutProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		try {
			logger.debug("Inside ProcessIntegration ...");
			IntegrationProcessingResult result = CashOutResponseProcessor.processIntegration(context);
			handleCommissions(context, result.getProcessingStatus());
			return result;
		} catch (Exception e) {
			logger.error("Error when ProcessIntegration in CashOutProcessor", e);
			return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null, false);
		}
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessMessage(context);
			ValidationResult validationResult = CashOutValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());

		} catch (MessageParsingException e) {
			logger.debug("Error while parsing message", e);
			return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in CashOutProcessor", ex);
			if (context.getTransaction() != null) {
				TransactionHelper.reverseTransaction(context);
			}
			return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		if (context.getTransaction() != null) {
			TransactionHelper.reverseTransaction(context);
		}
		return MessageProcessingResult.create(context, status, reason, reasonDescription);
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException {
		logger.debug("Inside AcceptMessage ...");
		CashOutMessage message = (CashOutMessage) context.getRequest();
		MessageProcessingResult result = new MessageProcessingResult();
		MPAY_ProcessingStatus status;
		MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
		String reasonDescription = null;
		context.getMessage().setReason(reason);
		MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO, message.getNotes());
		result.setMessage(context.getMessage());
		context.setTransaction(transaction);
		result.setTransaction(transaction);
		context.getDataProvider().persistProcessingResult(result);
		JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), context.getTransaction());
		if (postingResult.isSuccess()) {
			Integer mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
			boolean isSenderAlias = context.getSender().getMobile().getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) && context.getSender().getMobile().getAlias() != null && context.getSender().getMobile().getAlias().length() > 0;
			IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, isSenderAlias);
			MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegType));
			messageLog.setRefMessage(context.getMessage());
			result.setMpClearMessage(messageLog);
		} else {
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
			reason = context.getLookupsLoader().getReason(postingResult.getReason());
		}

		context.getMessage().setProcessingStatus(status);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
		transaction.setReason(context.getMessage().getReason());
		transaction.setReasonDesc(context.getMessage().getReasonDesc());
		if(context.getExtraData().get(CashOutConstants.SOURCE)!=null)
			transaction.setShopId((String) context.getExtraData().get(CashOutConstants.SOURCE));
		result.setStatus(status);
		result.setStatusDescription(context.getMessage().getReasonDesc());

		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
			result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED) || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
			context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1, SystemHelper.getCurrentDateWithoutTime());
		}
		OTPHanlder.removeOTP(context);
		handleCommissions(context, status);
		return result;
	}

	private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
		if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return;
		CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
				CommissionDirections.SENDER);
		if (context.getDirection() == TransactionDirection.ONUS)
			CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
					CommissionDirections.RECEIVER);
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, SQLException {
		logger.debug("Inside PreProcessMessage ...");
		CashOutMessage message = CashOutMessage.parseMessage(context.getRequest());

		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setAmount(message.getAmount());
		context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
		sender.setInfo(message.getSender());
		sender.setMobile(context.getDataProvider().getCustomerMobile(message.getSender(), message.getSenderType()));
		if (sender.getMobile() == null)
			return;
		sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), message.getSenderAccount()));
		if (sender.getMobileAccount() == null)
			return;
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setAccount(sender.getMobileAccount().getRefAccount());
		sender.setBank(sender.getMobileAccount().getBank());
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setCharge(message.getChargesAmount());
		sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
		sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
		if (message.getChargesAmount() == null)
			ChargesCalculator.calculateSenderCharge(context.getOperation().getMessageType().getCode(), context, context.getSender().getProfile());
		receiver.setInfo(message.getReceiver());
		receiver.setService(context.getDataProvider().getCorporateService(message.getReceiver(), message.getReceiverType()));
		if (receiver.getService() == null)
			return;
		receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), receiver.getService(), message.getReceiverAccount()));
		if (receiver.getServiceAccount() == null)
			return;
		receiver.setCorporate(receiver.getService().getRefCorporate());
		receiver.setAccount(receiver.getServiceAccount().getRefAccount());
		receiver.setBank(receiver.getServiceAccount().getBank());
		receiver.setProfile(receiver.getServiceAccount().getRefProfile());
		receiver.setBanked(receiver.getAccount().getIsBanked());
		receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

		long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
		context.setDirection(TransactionDirection.ONUS);
		context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(), context.getOperation().getMessageType().getId(), context.getTransactionNature()));
		if (context.getTransactionConfig() != null)
			context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
		TaxCalculator.claculate(context);
		OTPHanlder.loadOTP(context, message.getPin());
		context.getExtraData().put(CashOutConstants.SOURCE,message.getSource());
		handlePushNotification(context);
	}

	private void handlePushNotification(MessageProcessingContext context) {
		final String senderDeviceToken = context.getSender().getMobile().getRefCustomerMobileCustomerDevices()
				.stream()
				.filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
				.findFirst()
				.map(MPAY_CustomerDevice::getExtraData).orElse(null);
		context.setSenderNotificationToken(senderDeviceToken);
	}

	private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isSenderAlias) {
		try {
			logger.debug("Inside CreateMPClearRequest ...");
			String messageID = context.getDataProvider().getNextMPClearMessageId();
			BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
			IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(type);
			message.setBinary(false);

			String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
			String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

			message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
			MPClearHelper.setMessageEncoded(message);
			message.setField(7, new IsoValue<String>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
			message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
			message.setField(49, new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

			String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getMobile(), context.getSender().getMobileAccount(), isSenderAlias);
			MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

			String account2 = MPClearHelper.getPSPAccount(context.getSystemParameters(), context.getReceiver().getService(), context.getReceiver().getServiceAccount());
			MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

			String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
			message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
			message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

			return message;
		} catch (Exception e) {
			logger.error("Error when CreateMPClearRequest in CashOutProcessor", e);
			return null;
		}
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		logger.debug("Inside Reverse ...");
		try {
			JVPostingResult result = TransactionHelper.reverseTransaction(context);
			CashOutMessage message = CashOutMessage.parseMessage(context.getRequest());
			context.getDataProvider().updateAccountLimit(
					SystemHelper.getMobileAccount(context.getSystemParameters(), context.getSender().getMobile(),
							message.getSenderAccount()).getRefAccount().getId(),
					context.getMessage().getRefOperation().getMessageType().getId(),
					context.getTransaction().getTotalAmount().negate(), -1, SystemHelper.getCurrentDateWithoutTime());
			context.getDataProvider().mergeTransaction(context.getTransaction());
			if (result.isSuccess())
				return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

			return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
		} catch (Exception ex) {
			logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
			return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(),
					ProcessingStatusCodes.FAILED);
		}
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
			String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		if (context.getMessage() != null) {
			context.getMessage().setReason(reason);
			context.getMessage().setReasonDesc(reasonDescription);
			context.getMessage().setProcessingStatus(status);
		}
		if (context.getTransaction() != null) {
			context.getTransaction().setReason(reason);
			context.getTransaction().setReasonDesc(reasonDescription);
			context.getTransaction().setProcessingStatus(status);
		}
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setTransaction(context.getTransaction());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		if (reasonCode.equals(ReasonCodes.VALID)) {
			result.setNotifications(NotificationProcessor.createReversalNotificationMessages(context));
		}
		return result;
	}
}