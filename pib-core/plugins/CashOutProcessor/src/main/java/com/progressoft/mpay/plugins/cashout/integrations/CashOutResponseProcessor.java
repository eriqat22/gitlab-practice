package com.progressoft.mpay.plugins.cashout.integrations;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entities.MPAY_CashOut;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.cashout.CashOutMessage;
import com.progressoft.mpay.plugins.cashout.NotificationProcessor;

public class CashOutResponseProcessor {
	private static final Logger logger = LoggerFactory.getLogger(CashOutResponseProcessor.class);

	private CashOutResponseProcessor() {

	}

	public static IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside ProcessIntegration ...");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessIntegration(context);
			if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return null;
			if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return accept(context);
			else
				return reject(context);
		} catch (Exception ex) {
			logger.error("Error when ProcessIntegration in CashOutResponseProcessor", ex);
			if (!context.getMessage().getRefMessageCashOut().isEmpty())
				JfwHelper.executeActionWithServiceUser(MPAYView.CASH_OUT, context.getMessage().getRefMessageCashOut().get(0).getId(), "SVC_Reject");
			try {
				context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(), BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
			} catch (SQLException e) {
				logger.error("Failed to reverse limits", e);
			}
			if (context.getTransaction() != null)
				TransactionHelper.reverseTransaction(context);
			return IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), null, false);
		}
	}

	private static void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside PreProcessIntegration ...");
		if (context.getMessage() != null)
			context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		CashOutMessage request = null;
		try {
			request = CashOutMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
		} catch (MessageParsingException ex) {
			logger.error("Error while parsing message", ex);
			throw new WorkflowException(ex);
		}
		context.setAmount(request.getAmount());
		context.setSender(sender);
		context.setReceiver(receiver);

		sender.setMobile(context.getTransaction().getSenderMobile());
		sender.setMobileAccount(context.getTransaction().getSenderMobileAccount());
		sender.setAccount(context.getSender().getMobileAccount().getRefAccount());
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setBank(sender.getMobile().getBank());
		sender.setBanked(sender.getMobileAccount().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
		sender.setCharge(context.getTransaction().getSenderCharge());
		sender.setTax(context.getTransaction().getSenderTax());
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

		receiver.setService(context.getTransaction().getReceiverService());
		receiver.setCorporate(receiver.getService().getRefCorporate());
		receiver.setServiceAccount(context.getTransaction().getReceiverServiceAccount());
		receiver.setAccount(receiver.getServiceAccount().getRefAccount());
		receiver.setBanked(receiver.getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
		receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(), receiver.getAccount().getAccNumber()));
		receiver.setInfo(receiver.getService().getName());
		receiver.setProfile(receiver.getServiceAccount().getRefProfile());
		receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

		context.setTransactionConfig(
				context.getLookupsLoader().getTransactionConfig(context.getLookupsLoader().getCustomerClientType().getId(), sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(), context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT, context.getOperation().getId()));
		context.setDirection(Long.valueOf(context.getTransaction().getDirection().getCode()));
		handlePushNotification(context);
	}

	private static void handlePushNotification(IntegrationProcessingContext context) {
		final String senderDeviceToken = context.getSender().getMobile().getRefCustomerMobileCustomerDevices()
				.stream()
				.filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
				.findFirst()
				.map(MPAY_CustomerDevice::getExtraData).orElse(null);
		context.setSenderNotificationToken(senderDeviceToken);
	}

	private static IntegrationProcessingResult accept(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside Accept ...");
		IntegrationProcessingResult result;
		result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
		result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		if (!context.getMessage().getRefMessageCashOut().isEmpty()) {
			MPAY_CashOut cashOut = context.getMessage().getRefMessageCashOut().get(0);
			JfwHelper.executeActionWithServiceUser(MPAYView.CASH_OUT, cashOut.getId(), "SVC_Accept");
			cashOut.setProcessingStatus(result.getMpayMessage().getProcessingStatus());
			cashOut.setReason(result.getMpayMessage().getReason());
			cashOut.setReasonDesc(result.getMpayMessage().getReasonDesc());
			context.getDataProvider().mergeCashOut(cashOut);
		}
		return result;
	}

	private static IntegrationProcessingResult reject(IntegrationProcessingContext context) throws WorkflowException, SQLException {
		logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(), BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
		String reasonDescription = null;
		if (context.getMpClearIsoMessage().getField(47) != null)
			reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, ReasonCodes.REJECTED_BY_NATIONAL_SWITCH, context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
		result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
		if (!context.getMessage().getRefMessageCashOut().isEmpty()) {
			MPAY_CashOut cashOut = context.getMessage().getRefMessageCashOut().get(0);
			JfwHelper.executeActionWithServiceUser(MPAYView.CASH_OUT, cashOut.getId(), "SVC_Reject");
			cashOut.setProcessingStatus(result.getMpayMessage().getProcessingStatus());
			cashOut.setReason(result.getMpayMessage().getReason());
			cashOut.setReasonDesc(result.getMpayMessage().getReasonDesc());
			context.getDataProvider().mergeCashOut(cashOut);
		}
		return result;
	}
}