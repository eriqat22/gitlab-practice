package com.progressoft.mpay.plugins.madar.message.view;

public interface CustomerInquireMessageView {
    String getMobileNumber();

    String getFullName();

    String getIdNumber();
}
