package com.progressoft.mpay.plugins.madar.message;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.plugins.madar.message.view.CustomerInquireMessageView;

public class CustomerInquireMessage extends MPayRequest implements CustomerInquireMessageView {
    private String mobileNumber;
    private String fullName;
    private String idNumber;

    private enum InquireCustomerMessageExtraData {
        MOBILE_NUMBER("mobileNumber"), FULL_NAME("fullName"), ID_NUMBER("idNumber");

        private String value;

        private InquireCustomerMessageExtraData(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }

    public CustomerInquireMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    @Override
    public String getMobileNumber() {
        return mobileNumber;
    }

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public String getIdNumber() {
        return idNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public static CustomerInquireMessage parseAndvalidate(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CustomerInquireMessage message = new CustomerInquireMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE)
                || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        message.setMobileNumber(request.getValue(InquireCustomerMessageExtraData.MOBILE_NUMBER.getValue()));
        message.setIdNumber(request.getValue(InquireCustomerMessageExtraData.ID_NUMBER.getValue()));
        message.setFullName(request.getValue(InquireCustomerMessageExtraData.FULL_NAME.getValue()));

        String idNumber = message.getIdNumber();
        String mobileNumber = message.getMobileNumber();
        String fullName = message.getFullName();

        if (idNumber != null) {
            if (idNumber.isEmpty())
                throw new MessageParsingException("Id Number is Null");
        } else if (mobileNumber != null) {
            if (mobileNumber.isEmpty())
                throw new MessageParsingException("Mobile Number is Null");
        } else if (fullName != null) {
            if (fullName.isEmpty())
                throw new MessageParsingException("Full Name is Null");
        }


        return message;
    }
}
