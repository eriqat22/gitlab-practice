package com.progressoft.mpay.plugins.madar.processor;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.madar.dao.Customer;
import com.progressoft.mpay.plugins.madar.message.CustomerInquireMessage;
import com.progressoft.mpay.plugins.madar.validator.CustomerInquireValidator;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerInquireProcessor implements MessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(CustomerInquireProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult result = CustomerInquireValidator.validate(context);
            if (result.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, result.getReasonCode(), result.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getMessage() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in CustomerInquiryProcessor", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
        logger.debug("Inside PreProcessMessage ...");
        CustomerInquireMessage message = CustomerInquireMessage.parseAndvalidate(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        context.setRequest(message);
        sender.setService(
                context.getDataProvider().getCorporateService(message.getSender(), ReceiverInfoType.CORPORATE));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(
                SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;
        sender.setProfile(sender.getServiceAccount().getRefProfile());
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws WorkflowException {
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context, true);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        logger.debug("Inside GenerateResponse ...");
        logger.debug("isAccepted: " + isAccepted);
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        String customerData = null;
        if (isAccepted) {
            Customer customer = loadCustomer(context);
            if (customer != null)
                customerData = customer.toString();
        }
        response.getExtraData().add(new ExtraData("Customer", customerData));
        return response.toString();
    }

    private Customer loadCustomer(MessageProcessingContext context) {

        CustomerInquireMessage message = (CustomerInquireMessage) context.getRequest();
        MPAY_Customer customer = null;

        String idNumber = message.getIdNumber();
        String mobileNumber = message.getMobileNumber();
        String fullName = message.getFullName();
        if (idNumber != null) {
            if (idNumber.trim().length() != 0)
                customer = context.getDataProvider().getCustomerByIdNumber(message.getIdNumber());
        } else if (mobileNumber != null) {
            if (mobileNumber.trim().length() != 0) {
                MPAY_CustomerMobile mobile = loadCustomerMobile(context.getDataProvider(), message);
                if (mobile == null)
                    return null;
                customer = mobile.getRefCustomer();
            }
        } else if (fullName != null) {
            if (fullName.trim().length() != 0)
                customer = context.getDataProvider().getCustomerByFullName(message.getFullName());
        }
        if (customer == null)
            return null;
        return new Customer(customer);
    }

    private MPAY_CustomerMobile loadCustomerMobile(IDataProvider dataProvider, CustomerInquireMessage message) {
        if (!StringUtils.isEmpty(message.getMobileNumber()))
            return dataProvider.getCustomerMobile(message.getMobileNumber(), ReceiverInfoType.MOBILE);
        return null;
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
                                                  String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
                ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }
}
