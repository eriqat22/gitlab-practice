package com.progressoft.mpay.plugins.madar.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_IDType;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.util.Date;

public class Customer {
    private String fullName;
    private String firstName;
    private String middleName;
    private String lastName;
    private boolean isActive;
    private String nationality;
    private String idType;
    private String idValue;
    private Date dateOfBirth;
    private String reference;
    private boolean isRegistered;
    private String phoneOne;
    private String phoneTwo;
    private String city;
    private String preferedLanguage;
    private String email;
    private String pobox;
    private String zipCode;
    private String buildingNum;
    private String streetName;
    private String customerStatus;


    public Customer(MPAY_Customer customer) {
        if (customer == null)
            throw new NullArgumentException("customer");
        this.fullName = customer.getFullName();
        this.firstName = customer.getFirstName();
        this.middleName = customer.getMiddleName();
        this.lastName = customer.getLastName();
        this.isActive = customer.getIsActive();
        JfwCountry country = customer.getNationality();
        if (country != null)
            this.nationality = country.getName();
        MPAY_IDType idType = customer.getIdType();
        if (idType != null)
            this.idType = idType.getName();
        this.idValue = customer.getIdNum();
        this.dateOfBirth = customer.getDob();
        this.reference = customer.getClientRef();
        this.isRegistered = customer.getIsRegistered();
        this.phoneOne = customer.getPhoneOne();
        this.phoneTwo = customer.getPhoneTwo();
        this.city = customer.getCity().getName();
        MPAY_Language prefLang = customer.getPrefLang();
        if (prefLang != null)
            this.preferedLanguage = prefLang.getName();
        this.email = customer.getEmail();
        this.pobox = customer.getPobox();
        this.zipCode = customer.getZipCode();
        this.buildingNum = customer.getBuildingNum();
        this.streetName = customer.getStreetName();
        this.customerStatus=customer.getStatusId().getName();

    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdValue() {
        return idValue;
    }

    public void setIdValue(String idValue) {
        this.idValue = idValue;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getPhoneOne() {
        return phoneOne;
    }

    public void setPhoneOne(String phoneOne) {
        this.phoneOne = phoneOne;
    }

    public String getPhoneTwo() {
        return phoneTwo;
    }

    public void setPhoneTwo(String phoneTwo) {
        this.phoneTwo = phoneTwo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPreferedLanguage() {
        return preferedLanguage;
    }

    public void setPreferedLanguage(String preferedLanguage) {
        this.preferedLanguage = preferedLanguage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPobox() {
        return pobox;
    }

    public void setPobox(String pobox) {
        this.pobox = pobox;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getBuildingNum() {
        return buildingNum;
    }

    public void setBuildingNum(String buildingNum) {
        this.buildingNum = buildingNum;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}
