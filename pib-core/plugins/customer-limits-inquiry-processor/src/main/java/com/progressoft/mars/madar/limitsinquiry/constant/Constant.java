package com.progressoft.mars.madar.limitsinquiry.constant;

public class Constant {

    public static final String LIMITS_DATA = "limitsData";
    public static final String CLIENT_LIMIT_DATA = "clientLimitData";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String BALANCE_KEY = "balance";
    public static final String CURRENCY_KEY = "currency";

    private Constant() {

    }
}
