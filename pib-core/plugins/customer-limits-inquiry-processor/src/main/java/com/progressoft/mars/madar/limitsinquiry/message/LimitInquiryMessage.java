package com.progressoft.mars.madar.limitsinquiry.message;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;

public class LimitInquiryMessage extends MPayRequest {

    private static final String MOBILE_NUMBER = "mobileNumber";
    private String mobileNumber;

    public LimitInquiryMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static LimitInquiryMessage parseAndValidateMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        LimitInquiryMessage message = new LimitInquiryMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(SENDER_KEY);
        if (message.getSenderType() == null || message.getSenderType().trim().length() == 0)
            throw new MessageParsingException(SENDER_TYPE_KEY);

        if (!message.getSenderType().equals(ReceiverInfoType.MOBILE)
                && !message.getSenderType().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(SENDER_TYPE_KEY);

        message.setMobileNumber(message.getValue(MOBILE_NUMBER));
        if (StringUtils.isEmpty(message.getMobileNumber()))
            throw new MessageParsingException(MOBILE_NUMBER);
        return message;

    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}
