package com.progressoft.mars.madar.limitsinquiry.dao.clientlimit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.entities.MPAY_ClientsLimit;

import java.math.BigDecimal;

public class ClientLimits {

    private Long dailyCount;
    private BigDecimal dailyAmount;
    private Long weeklyCount;
    private BigDecimal weeklyAmount;
    private Long monthlyCount;
    private BigDecimal monthlyAmount;
    private Long yearlyCount;
    private BigDecimal yearlyAmount;
    private String messageTypeName;

    public ClientLimits(MPAY_ClientsLimit clientsLimit) {
        if (clientsLimit == null)
        throw new IllegalArgumentException("clientsLimit");
        this.dailyCount = clientsLimit.getDailyCount();
        this.dailyAmount = clientsLimit.getDailyAmount();
        this.weeklyCount = clientsLimit.getWeeklyCount();
        this.weeklyAmount = clientsLimit.getWeeklyAmount();
        this.monthlyCount = clientsLimit.getMonthlyCount();
        this.monthlyAmount = clientsLimit.getMonthlyAmount();
        this.yearlyCount = clientsLimit.getYearlyCount();
        this.yearlyAmount = clientsLimit.getYearlyAmount();
        this.messageTypeName=clientsLimit.getMessageType().getName();
    }

    public void setMessageTypeName(String messageTypeName) {
        this.messageTypeName = messageTypeName;
    }

    public String getMessageTypeName() {
        return messageTypeName;
    }

    public Long getDailyCount() {
        return this.dailyCount;
    }

    public void setDailyCount(Long dailyCount) {
        this.dailyCount = dailyCount;
    }

    public BigDecimal getDailyAmount() {
        return this.dailyAmount;
    }

    public void setDailyAmount(BigDecimal dailyAmount) {
        this.dailyAmount = dailyAmount;
    }


    public Long getWeeklyCount() {
        return this.weeklyCount;
    }

    public void setWeeklyCount(Long weeklyCount) {
        this.weeklyCount = weeklyCount;
    }

    public BigDecimal getWeeklyAmount() {
        return this.weeklyAmount;
    }

    public void setWeeklyAmount(BigDecimal weeklyAmount) {
        this.weeklyAmount = weeklyAmount;
    }


    public Long getMonthlyCount() {
        return this.monthlyCount;
    }

    public void setMonthlyCount(Long monthlyCount) {
        this.monthlyCount = monthlyCount;
    }

    public BigDecimal getMonthlyAmount() {
        return this.monthlyAmount;
    }

    public void setMonthlyAmount(BigDecimal monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

    public Long getYearlyCount() {
        return this.yearlyCount;
    }

    public void setYearlyCount(Long yearlyCount) {
        this.yearlyCount = yearlyCount;
    }

    public BigDecimal getYearlyAmount() {
        return this.yearlyAmount;
    }

    public void setYearlyAmount(BigDecimal yearlyAmount) {
        this.yearlyAmount = yearlyAmount;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }
}
