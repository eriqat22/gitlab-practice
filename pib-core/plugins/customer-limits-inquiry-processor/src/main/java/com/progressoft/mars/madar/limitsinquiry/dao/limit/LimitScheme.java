package com.progressoft.mars.madar.limitsinquiry.dao.limit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class LimitScheme {

	private boolean isActive;
	private BigDecimal pinlessAmount;
	private BigDecimal walletCap;
	private List<Limit> refSchemeLimits;

	public LimitScheme(MPAY_LimitsScheme limitsScheme) {
		if (limitsScheme == null)
			throw new IllegalArgumentException("limitScheme");
		this.isActive = limitsScheme.getIsActive();
		this.pinlessAmount = limitsScheme.getPinlessAmount();
		this.walletCap = limitsScheme.getWalletCap();
		this.refSchemeLimits = new ArrayList<>();
		for (MPAY_Limit limit : limitsScheme.getRefSchemeLimits()) {
			this.refSchemeLimits.add(new Limit(limit));
		}
	}

	public boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(boolean paramBoolean) {
		this.isActive = paramBoolean;
	}

	public BigDecimal getPinlessAmount() {
		return this.pinlessAmount;
	}

	public void setPinlessAmount(BigDecimal paramBigDecimal) {
		this.pinlessAmount = paramBigDecimal;
	}

	public BigDecimal getWalletCap() {
		return this.walletCap;
	}

	public void setWalletCap(BigDecimal paramBigDecimal) {
		this.walletCap = paramBigDecimal;
	}

	public List<Limit> getRefSchemeLimits() {
		return this.refSchemeLimits;
	}

	public void setRefSchemeLimits(List<Limit> paramList) {
		this.refSchemeLimits = paramList;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}
}
