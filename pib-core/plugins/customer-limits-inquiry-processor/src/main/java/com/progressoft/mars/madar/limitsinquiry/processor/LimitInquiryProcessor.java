package com.progressoft.mars.madar.limitsinquiry.processor;

import com.google.gson.Gson;
import com.progressoft.mars.madar.limitsinquiry.constant.Constant;
import com.progressoft.mars.madar.limitsinquiry.dao.Profile;
import com.progressoft.mars.madar.limitsinquiry.dao.clientlimit.ClientLimits;
import com.progressoft.mars.madar.limitsinquiry.dao.limit.Limit;
import com.progressoft.mars.madar.limitsinquiry.dao.limit.LimitScheme;
import com.progressoft.mars.madar.limitsinquiry.dao.limit.LimitsDetail;
import com.progressoft.mars.madar.limitsinquiry.message.LimitInquiryMessage;
import com.progressoft.mars.madar.limitsinquiry.validator.LimitInquiryValidator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LimitInquiryProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(LimitInquiryProcessor.class);

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult result = LimitInquiryValidator.validate(context);
            if (result.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, result.getReasonCode(), result.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in CorporateInquiryProcessor", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
        logger.debug("Inside PreProcessMessage ...");
        LimitInquiryMessage message = LimitInquiryMessage.parseAndValidateMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setRequest(message);
        sender.setService(context.getDataProvider().getCorporateService(message.getSender(), ReceiverInfoType.CORPORATE));
        if (sender.getService() == null)
            return;
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        if (sender.getServiceAccount() == null)
            return;
        MPAY_Profile corporateProfile = sender.getServiceAccount().getRefProfile();
        sender.setProfile(corporateProfile);

        MPAY_CustomerMobile customerMobile = context.getDataProvider().getCustomerMobile(message.getMobileNumber());
        if (customerMobile != null) {
            MPAY_MobileAccount mobileAccount = customerMobile.getMobileMobileAccounts().get(0);
            if (mobileAccount != null) {
                context.getExtraData().put(Constant.LIMITS_DATA, mobileAccount.getRefProfile());
                context.getExtraData().put(Constant.CLIENT_LIMIT_DATA, mobileAccount.getRefAccount().getAccountClientsLimits());
                context.getExtraData().put(Constant.BALANCE_KEY, SystemHelper.formatAmount(context.getSystemParameters(), context.getDataProvider().getBalance(mobileAccount.getRefAccount().getId())));
                context.getExtraData().put(Constant.CURRENCY_KEY, mobileAccount.getRefAccount().getCurrency().getStringISOCode());
            }
        }
        context.getExtraData().put(Constant.MOBILE_NUMBER, customerMobile);
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context, true);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                 String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        logger.debug("Inside GenerateResponse ...");
        logger.debug("isAccepted: ", isAccepted);
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        if (isAccepted) {
            String profileResponseData = null;
            MPAY_Profile mpayProfile = (MPAY_Profile) context.getExtraData().get(Constant.LIMITS_DATA);
            List<MPAY_ClientsLimit> mpayClientsLimits = (List<MPAY_ClientsLimit>) context.getExtraData().get(Constant.CLIENT_LIMIT_DATA);
            List<ClientLimits> clientLimitsArray = new ArrayList<>();

            for (MPAY_ClientsLimit clientLimits : mpayClientsLimits) {
                if (clientLimits != null) {
                    ClientLimits clientLimitsBeforeHandleJson = new ClientLimits(clientLimits);
                    clientLimitsArray.add(clientLimitsBeforeHandleJson);
                }
            }
            if (mpayProfile != null) {
                Profile profileBeforeHandleJson = new Profile(mpayProfile);
                Profile profile = manageProfile(profileBeforeHandleJson);
                profileResponseData = profile.toString();
            }
            response.getExtraData().add(new ExtraData(Constant.LIMITS_DATA, profileResponseData));
            response.getExtraData().add(new ExtraData(Constant.CLIENT_LIMIT_DATA, new Gson().toJson(clientLimitsArray)));
            response.getExtraData().add(new ExtraData(Constant.BALANCE_KEY, context.getExtraData().get(Constant.BALANCE_KEY).toString()));
            response.getExtraData().add(new ExtraData(Constant.CURRENCY_KEY, context.getExtraData().get(Constant.CURRENCY_KEY).toString()));
        }
        return response.toString();
    }

    private Profile manageProfile(Profile profile) {
        LimitScheme limitsScheme = profile.getLimitsScheme();
        if (limitsScheme.getIsActive()) {
            Iterator<Limit> refSchemeLimits = limitsScheme.getRefSchemeLimits().iterator();
            while (refSchemeLimits.hasNext()) {
                Limit next = refSchemeLimits.next();
                if (!next.getIsActive()) {
                    refSchemeLimits.remove();
                } else {
                    Iterator<LimitsDetail> refLimitLimitsDetails = next.getRefLimitLimitsDetails().iterator();
                    while (refLimitLimitsDetails.hasNext())
                        if (!refLimitLimitsDetails.next().getIsActive())
                            refLimitLimitsDetails.remove();
                }
            }
        }
        return profile;
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
                                                  String reasonDescription) {
        logger.debug("Inside rejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
                ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

}
