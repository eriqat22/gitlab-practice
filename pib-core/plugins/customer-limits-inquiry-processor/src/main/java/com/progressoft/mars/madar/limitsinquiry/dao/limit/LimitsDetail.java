package com.progressoft.mars.madar.limitsinquiry.dao.limit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;

import java.math.BigDecimal;

public class LimitsDetail {
	private boolean isActive;
	private long txCountLimit;
	private BigDecimal txAmountLimit;
	private String msgType;

	public LimitsDetail(MPAY_LimitsDetail detail) {
		if (detail == null)
			throw new IllegalArgumentException("LimitDetails");
		this.isActive = detail.getIsActive();
		this.txCountLimit = detail.getTxCountLimit();
		this.txAmountLimit = detail.getTxAmountLimit();
		this.msgType = detail.getMsgType().getName();
	}

	public boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(boolean paramBoolean) {
		this.isActive = paramBoolean;
	}

	public long getTxCountLimit() {
		return this.txCountLimit;
	}

	public void setTxCountLimit(long paramLong) {
		this.txCountLimit = paramLong;
	}

	public BigDecimal getTxAmountLimit() {
		return this.txAmountLimit;
	}

	public void setTxAmountLimit(BigDecimal paramBigDecimal) {
		this.txAmountLimit = paramBigDecimal;
	}

	public String getMsgType() {
		return this.msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}

}
