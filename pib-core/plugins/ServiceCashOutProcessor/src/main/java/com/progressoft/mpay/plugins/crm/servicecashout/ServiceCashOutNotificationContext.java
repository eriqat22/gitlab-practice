package com.progressoft.mpay.plugins.crm.servicecashout;

import com.progressoft.mpay.plugins.NotificationContext;

public class ServiceCashOutNotificationContext extends NotificationContext {
	private String currency;
	private String amount;
	private String senderBalance;
	private String senderCharges;
	private String senderTax;
	private String receiverBalance;

	public String getReceiverBalance() {
		return receiverBalance;
	}

	public void setReceiverBalance(String receiverBalance) {
		this.receiverBalance = receiverBalance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getSenderBalance() {
		return senderBalance;
	}

	public void setSenderBalance(String senderBalance) {
		this.senderBalance = senderBalance;
	}

	public String getSenderCharges() {
		return senderCharges;
	}

	public void setSenderCharges(String senderCharges) {
		this.senderCharges = senderCharges;
	}

	public boolean hasSenderCharges() {
		if (this.senderCharges == null || this.senderCharges.trim().length() == 0)
			return false;
		return Double.parseDouble(this.senderCharges) > 0;
	}

	public boolean hasSenderTax() {
		if (this.senderTax == null || this.senderTax.trim().length() == 0)
			return false;
		return Double.parseDouble(this.senderTax) > 0;
	}

	public String getSenderTax() {
		return senderTax;
	}

	public void setSenderTax(String senderTax) {
		this.senderTax = senderTax;
	}
}
