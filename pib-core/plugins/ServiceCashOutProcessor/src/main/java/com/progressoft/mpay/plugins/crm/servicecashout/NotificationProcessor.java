package com.progressoft.mpay.plugins.crm.servicecashout;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    private NotificationProcessor() {

    }

    public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateAcceptanceNotificationMessages ...");
            String reference;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            MPAY_CorpoarteService senderService = context.getSender().getService();
//            MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();
            long languageId = context.getSender().getCorporate().getPrefLang().getId();
            ServiceCashOutNotificationContext notificationContext = new ServiceCashOutNotificationContext();
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setReference(reference);
            context.getDataProvider().refreshEntity(context.getSender().getAccount());
            notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getAccount().getBalance()));
            notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
            notificationContext.setSenderTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getTax()));
            notificationContext.setExtraData2(String.valueOf(languageId));
            notificationContext.setExtraData1(senderService.getName());

            List<MPAY_Notification> notifications = new ArrayList<>();
            if (senderService != null) {
                final MPAY_Language prefLang = senderService.getRefCorporate().getPrefLang();
                addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
                        senderService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
                        senderService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
                        senderService.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);

//				notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(),
//						languageId, senderService.getNotificationReceiver(), senderService.getNotificationChannel().getCode()));
            }
            return notifications;
        } catch (Exception e) {
            logger.error("Error in CreateAcceptanceNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            ServiceCashOutMessage request = ServiceCashOutMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            String senderService = request.getSender();
//            String senderChannelCode = null;
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();
            MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(senderService, request.getSenderType());
            if (service != null) {
                senderService = service.getNotificationReceiver();
//                senderChannelCode = service.getNotificationChannel().getCode();
                if (senderService == null)
                    return new ArrayList<>();
                language = service.getRefCorporate().getPrefLang();
            }
            ServiceCashOutNotificationContext notificationContext = new ServiceCashOutNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
            notificationContext.setExtraData2(String.valueOf(language));
            notificationContext.setExtraData1(service.getName());

            if (service != null) {
                addNotification(context.getMessage(), notifications, service.getEmail(), language,
                        service.getEnableEmail(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
                addNotification(context.getMessage(), notifications, service.getMobileNumber(), language,
                        service.getEnableSMS(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
                addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), language,
                        service.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_SENDER,
                        NotificationChannelsCode.PUSH_NOTIFICATION);
            }

//			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_SENDER,
//					context.getMessage().getRefOperation().getId(), language.getId(), senderService, senderChannelCode));
            return notifications;
        } catch (Exception e) {
            logger.error("Error in CreateRejectionNotificationMessages", e);
            return new ArrayList<>();
        }
    }

    public static List<MPAY_Notification> createReversalNotificationMessages(MessageProcessingContext context) {
        String reference;
        List<MPAY_Notification> notifications = new ArrayList<>();

        reference = context.getTransaction().getReference();
        MPAY_CorpoarteService senderService = context.getTransaction().getSenderService();
        MPAY_EndPointOperation operation = context.getTransaction().getRefOperation();
        String currency = context.getTransaction().getCurrency().getStringISOCode();
        if (senderService != null) {
            ServiceCashOutNotificationContext senderNotificationContext = new ServiceCashOutNotificationContext();
            senderNotificationContext.setCurrency(currency);
            senderNotificationContext.setReference(reference);
            MPAY_Account senderAccount = context.getTransaction().getSenderServiceAccount().getRefAccount();
            context.getDataProvider().refreshEntity(senderAccount);
            senderNotificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
            final MPAY_Language prefLang = senderService.getRefCorporate().getPrefLang();
            long senderLanguageId = prefLang.getId();
            senderNotificationContext.setExtraData2(String.valueOf(senderLanguageId));
            senderNotificationContext.setExtraData1(senderService.getName());

            addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
                    senderService.getEnableEmail(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
            addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
                    senderService.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
            addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
                    senderService.getEnablePushNotification(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL,
                    NotificationChannelsCode.PUSH_NOTIFICATION);

//            notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
//                    senderLanguageId, senderService.getNotificationReceiver(), senderService.getNotificationChannel().getCode()));
        }
        return notifications;
    }
}
