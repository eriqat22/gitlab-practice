package com.progressoft.mpay.plugins.madar.merchantpayment.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class MadarMerchantPayReversalAdviseProcessor extends MadarIntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MadarMerchantPayReversalAdviseProcessor.class);
	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

}
