package com.progressoft.mpay.plugins.madar.merchantpayment.integrations;

import javax.transaction.NotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;

public class MadarIntegrationProcessorFactory {
	private static final Logger logger = LoggerFactory.getLogger(MadarIntegrationProcessorFactory.class);

	private MadarIntegrationProcessorFactory() {

	}

	public static MadarIntegrationProcessor createProcessor(int messageType) throws NotSupportedException {
		logger.debug("Inside CreateProcessor ...");
		logger.debug("messageType: " + messageType);
		if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)
			return new MadarMerchantPayInwardProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE)
			return new MadarMerchantPayResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_RESPONSE)
			return new MadarMerchantPayOfflineResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST)
			return new MadarMerchantPayReversalAdviseProcessor();
		else
			throw new NotSupportedException("messageType == " + messageType);
	}
}
