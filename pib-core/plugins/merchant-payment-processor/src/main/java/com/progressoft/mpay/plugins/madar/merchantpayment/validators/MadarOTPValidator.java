package com.progressoft.mpay.plugins.madar.merchantpayment.validators;

import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.entities.MPAY_ClientsOTP;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class MadarOTPValidator {

	private static final Logger LOGGER = LoggerFactory.getLogger(com.progressoft.mpay.plugins.validators.OTPValidator.class);

	private MadarOTPValidator() {

	}

	public static ValidationResult validate(ISystemParameters systemParameters, MPAY_ClientsOTP clientOtp, MPAY_CustomerMobile mobile, BigDecimal amount, MPAY_MessageType messageType) {
		LOGGER.debug("Inside validate");
		if(systemParameters == null)
			throw new NullArgumentException("systemParameters");
		if(mobile == null)
			throw new NullArgumentException("mobile");
		if(amount == null)
			throw new NullArgumentException("amount");
		if(messageType == null)
			throw new NullArgumentException("messageType");

		if (clientOtp == null || !clientOtp.getSenderMobile().getMobileNumber().equals(mobile.getMobileNumber()))
			return new ValidationResult(ReasonCodes.INVALID_OTP, null, false);

		if (isExpiredOtp(clientOtp, systemParameters))
			return new ValidationResult(ReasonCodes.EXPIRED_OTP, null, false);

		if (!isAllowedAmount(clientOtp, amount))
			return new ValidationResult(ReasonCodes.INVALID_AMOUNT, null, false);

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static boolean isAllowedAmount(MPAY_ClientsOTP clienttOtp, BigDecimal amount) {
		if (clienttOtp.getMaxAmount().compareTo(amount) < 0) {
			return false;
		}
		return true;
	}

	private static boolean isExpiredOtp(MPAY_ClientsOTP clientOtp, ISystemParameters systemParameters) {
		long hourInMellSec = (long) systemParameters.getOtpValidityInMinutes() * 60 * 1000;
		Timestamp requestTime = clientOtp.getRequestTime();
		requestTime.setTime(requestTime.getTime() + hourInMellSec);
		if (requestTime.before(new Timestamp(new Date().getTime()))) {
			return true;
		}
		return false;
	}
}
