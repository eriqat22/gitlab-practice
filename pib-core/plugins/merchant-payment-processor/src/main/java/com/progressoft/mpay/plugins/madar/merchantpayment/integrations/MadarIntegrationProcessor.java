package com.progressoft.mpay.plugins.madar.merchantpayment.integrations;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public abstract class MadarIntegrationProcessor {
	public abstract IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context);
}
