package com.progressoft.mpay.plugins.madar.merchantpayment.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class MadarMerchantPayOfflineResponseProcessor extends MadarIntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MadarMerchantPayOfflineResponseProcessor.class);
	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		if (context.getMpClearIsoMessage().getField(44).getValue().equals("00"))
			return Accept(context);
		else
			return Reject(context);
	}

	private IntegrationProcessingResult Accept(IntegrationProcessingContext context) {
		logger.debug("Inside Accept ...");
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
		return result;
	}

	private IntegrationProcessingResult Reject(IntegrationProcessingContext context) {
		logger.debug("Inside Reject ...");
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
				context.getMpClearIsoMessage().getField(44).getValue() + " " + context.getMpClearIsoMessage().getField(47).getValue(), null, false);
		return result;
	}
}
