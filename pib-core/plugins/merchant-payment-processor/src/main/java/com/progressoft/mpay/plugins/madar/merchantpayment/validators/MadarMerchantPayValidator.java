package com.progressoft.mpay.plugins.madar.merchantpayment.validators;

import com.progressoft.mpay.plugins.madar.merchantpayment.MadarMerchantPayMessage;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.MPAY_LimitsScheme;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.AmountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileNumberValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PSPValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;
import com.progressoft.mpay.plugins.validators.WalletCapValidator;

public class MadarMerchantPayValidator {
	private static final Logger logger = LoggerFactory.getLogger(MadarMerchantPayValidator.class);

	private MadarMerchantPayValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		MadarMerchantPayMessage request = (MadarMerchantPayMessage) context.getRequest();
		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			result = validateCustomer(context, request);
		else if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			result = validateCorporate(context, request);
		else
			return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
		if (result.isValid())
			return validateSettings(context, request);
		return result;
	}

	private static ValidationResult validateSettings(MessageProcessingContext context, MadarMerchantPayMessage request) {

		ValidationResult result = TransactionConfigValidator.validate(context.getTransactionConfig(),
				request.getAmount());
		if (!result.isValid())
			return result;

		result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context);
		if (!result.isValid())
			return result;

		return PSPValidator.validate(context, false);
	}

	private static ValidationResult validateCustomer(MessageProcessingContext context, MadarMerchantPayMessage request) {

		ValidationResult result = validateCustomerInfo(context);
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		MPAY_LimitsScheme scheme = context.getSender().getProfile().getLimitsScheme();
		if (scheme.getPinlessAmount().compareTo(context.getAmount()) < 0) {
			result = MadarPinCodeValidator.validate(context, context.getSender().getMobile(), request.getPin());
			if (!result.isValid())
				return result;
		}

		if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getService() != null)
			result = validateReceiver(context, request.getReceiverType(), request.getReceiverPin());

		return result;
	}

	private static ValidationResult validateCustomerInfo(MessageProcessingContext context) {
		ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;
		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;
		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;
		return DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(),
				context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context, MadarMerchantPayMessage request) {
		ValidationResult result = validateCorporateInfo(context);
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(),
				context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		result = MadarPinCodeValidator.validate(context, context.getSender().getService(), request.getPin());
		if (!result.isValid())
			return result;

		if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
			result = MobileNumberValidator.validate(context, request.getReceiverInfo());
			if (!result.isValid())
				return result;
		}

		if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getMobile() != null)
			result = validateReceiver(context, request.getReceiverType(), request.getReceiverPin());
		return result;
	}

	private static ValidationResult validateCorporateInfo(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(),
				context.getRequest().getDeviceId(), context.getSender().getService().getId()));
	}

	private static ValidationResult validateReceiver(MessageProcessingContext context, String receiverType,
			String receiverPin) {
		logger.debug("Inside validateReceiver ...");
		if (receiverType.equals(ReceiverInfoType.MOBILE))
			return validateReceiverMobile(context, receiverPin);
		else
			return validateReceiverService(context);
	}

	private static ValidationResult validateReceiverService(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getServiceAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return WalletCapValidator.validate(context);
	}

	private static ValidationResult validateReceiverMobile(MessageProcessingContext context, String receiverPin) {
		ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
		if (!result.isValid())
			return result;
		result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getMobileAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		MPAY_LimitsScheme scheme = context.getReceiver().getProfile().getLimitsScheme();
		if (scheme.getPinlessAmount().compareTo(context.getAmount()) >= 0)
			return new ValidationResult(ReasonCodes.VALID, null, true);
		result = MadarPinCodeValidator.validate(context, context.getReceiver().getMobile(), receiverPin);
		if (!result.isValid())
			return result;
		return WalletCapValidator.validate(context);
	}
}
