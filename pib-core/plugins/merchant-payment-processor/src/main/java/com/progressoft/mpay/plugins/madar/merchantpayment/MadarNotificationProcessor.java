package com.progressoft.mpay.plugins.madar.merchantpayment;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;

public class MadarNotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MadarNotificationProcessor.class);

	private MadarNotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateAcceptanceNotificationMessages ...");
		try {
			String receiver;
			String sender;
			String reference;
			long senderLanguageId;
			long receiverLanguageId;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();

			MPAY_CustomerMobile senderMobile;
			MPAY_CorpoarteService receiverService;
			MPAY_Account senderAccount;
			MPAY_Account receiverAccount;

			if (context.getTransaction().getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT)) {
				senderMobile = context.getSender().getMobile();
				senderAccount = context.getSender().getMobileAccount().getRefAccount();
				receiverService = context.getReceiver().getService();
				receiverAccount = context.getReceiver().getServiceAccount().getRefAccount();
			} else {
				senderMobile = context.getReceiver().getMobile();
				senderAccount = context.getReceiver().getMobileAccount().getRefAccount();
				receiverService = context.getSender().getService();
				receiverAccount = context.getSender().getServiceAccount().getRefAccount();
			}
			context.getDataProvider().refreshEntity(senderAccount);
			context.getDataProvider().refreshEntity(receiverAccount);
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

			MadarMerchantPayNotificationContext notificationContext = new MadarMerchantPayNotificationContext();
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);
			if (senderMobile != null) {
				sender = SystemHelper.maskMobileNumber(context.getSystemParameters(), senderMobile.getMobileNumber());
				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
				notificationContext.setSenderBanked(senderAccount.getIsBanked());
				notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
				notificationContext.setSenderTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getTax()));
				senderLanguageId = senderMobile.getRefCustomer().getPrefLang().getId();
				if (senderMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS) && senderMobile.getAlias() != null && !senderMobile.getAlias().isEmpty())
					sender = senderMobile.getAlias();
			} else {
				sender = SystemHelper.maskMobileNumber(context.getSystemParameters(), context.getSender().getInfo());
				senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			if (receiverService != null) {
				receiver = receiverService.getName();

				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
				notificationContext.setReceiverBanked(receiverAccount.getIsBanked());
				notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
				notificationContext.setReceiverTax(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getTax()));
				receiverLanguageId = receiverService.getRefCorporate().getPrefLang().getId();
			} else {
				receiver = context.getReceiver().getInfo();
				receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			notificationContext.setReceiver(receiver);
			notificationContext.setSender(sender);
			notificationContext.setExtraData1((String)context.getExtraData().get(MadarMerchantPaymentProcessor.NOTIFICATION_TOKEN));
			return generateNotifications(context, senderLanguageId, receiverLanguageId, senderMobile, receiverService, operation, notificationContext);

		} catch (Exception e) {
			logger.error("Error when CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static List<MPAY_Notification> generateNotifications(ProcessingContext context, long senderLanguageId, long receiverLanguageId, MPAY_CustomerMobile senderMobile,
			MPAY_CorpoarteService receiverService, MPAY_EndPointOperation operation, MadarMerchantPayNotificationContext notificationContext) {
		List<MPAY_Notification> notifications = new ArrayList<>();
		if (senderMobile != null) {
			notificationContext.setExtraData2(String.valueOf(senderLanguageId));
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
					senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		if (receiverService != null) {
			notificationContext.setExtraData2(String.valueOf(receiverLanguageId));
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId,
					receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
		}
		return notifications;
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			String notificationChannel = NotificationChannelsCode.SMS;
			MadarMerchantPayMessage request = MadarMerchantPayMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String sender = request.getSender();
			MPAY_Language language = context.getSystemParameters().getSystemLanguage();
			if (request.getSenderType().equals(ReceiverInfoType.MOBILE)) {
				MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
				if (mobile != null) {
					language = mobile.getRefCustomer().getPrefLang();
				}
			} else {
				MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
				if (service == null)
					return new ArrayList<>();
				language = service.getRefCorporate().getPrefLang();
				notificationChannel = service.getNotificationChannel().getCode();
				sender = service.getNotificationReceiver();
			}

			MadarMerchantPayNotificationContext notificationContext = new MadarMerchantPayNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
			notificationContext.setReceiver(request.getReceiverInfo());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
			notificationContext.setExtraData1((String) context.getExtraData().get(MadarMerchantPaymentProcessor.NOTIFICATION_TOKEN));
			notificationContext.setExtraData2(String.valueOf(language));
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_SENDER,
					context.getMessage().getRefOperation().getId(), language.getId(), sender, notificationChannel));
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	public static List<MPAY_Notification> createReversalNotificationMessages(MessageProcessingContext context) {
		try {
			logger.debug("Inside CreateReversalNotificationMessages ...");
			if (TransactionTypeCodes.DIRECT_CREDIT.equals(context.getTransaction().getRefType().getCode()))
				return createCreditReversalNotificationMessages(context);
			else if (TransactionTypeCodes.DIRECT_DEBIT.equals(context.getTransaction().getRefType().getCode()))
				return createDebitReversalNotificationMessages(context);
			else
				throw new NotSupportedException("transaction type not supported, code: " + context.getTransaction().getRefType().getCode());
		} catch (Exception e) {
			logger.error("Error when CreateReversalNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static List<MPAY_Notification> createDebitReversalNotificationMessages(MessageProcessingContext context) {
		String reference;
		List<MPAY_Notification> notifications = new ArrayList<>();

		reference = context.getTransaction().getReference();
		MPAY_CustomerMobile receiverMobile = context.getTransaction().getReceiverMobile();
		MPAY_CorpoarteService senderService = context.getTransaction().getSenderService();
		MPAY_EndPointOperation operation = context.getTransaction().getRefOperation();
		String currency = context.getTransaction().getCurrency().getStringISOCode();
		if (senderService != null) {
			MadarMerchantPayNotificationContext senderNotificationContext = new MadarMerchantPayNotificationContext();
			senderNotificationContext.setCurrency(currency);
			senderNotificationContext.setReference(reference);
			MPAY_Account senderAccount = context.getTransaction().getSenderServiceAccount().getRefAccount();
			context.getDataProvider().refreshEntity(senderAccount);
			senderNotificationContext.setReceiverBanked(senderAccount.getIsBanked());
			senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
			long senderLanguageId = senderService.getRefCorporate().getPrefLang().getId();
			senderNotificationContext.setExtraData1((String) context.getExtraData().get(MadarMerchantPaymentProcessor.NOTIFICATION_TOKEN));
			senderNotificationContext.setExtraData2(String.valueOf(senderLanguageId));
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
					senderLanguageId, senderService.getNotificationReceiver(), senderService.getNotificationChannel().getCode()));
		}
		if (receiverMobile != null) {
			MadarMerchantPayNotificationContext receiverNotificationContext = new MadarMerchantPayNotificationContext();
			receiverNotificationContext.setCurrency(currency);
			receiverNotificationContext.setReference(reference);
			MPAY_Account receiverAccount = context.getTransaction().getReceiverMobileAccount().getRefAccount();
			context.getDataProvider().refreshEntity(receiverAccount);
			receiverNotificationContext.setReceiverBanked(receiverAccount.getIsBanked());
			receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
			long receiverLanguageId = receiverMobile.getRefCustomer().getPrefLang().getId();
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
					receiverLanguageId, receiverMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		return notifications;
	}

	private static List<MPAY_Notification> createCreditReversalNotificationMessages(MessageProcessingContext context) {
		String reference;
		List<MPAY_Notification> notifications = new ArrayList<>();

		reference = context.getTransaction().getReference();
		MPAY_CustomerMobile senderMobile = context.getTransaction().getSenderMobile();
		MPAY_CorpoarteService receiverService = context.getTransaction().getReceiverService();
		MPAY_EndPointOperation operation = context.getTransaction().getRefOperation();
		String currency = context.getTransaction().getCurrency().getStringISOCode();
		if (senderMobile != null) {
			MadarMerchantPayNotificationContext senderNotificationContext = new MadarMerchantPayNotificationContext();
			senderNotificationContext.setCurrency(currency);
			senderNotificationContext.setReference(reference);
			MPAY_Account senderAccount = context.getTransaction().getSenderMobileAccount().getRefAccount();
			context.getDataProvider().refreshEntity(senderAccount);
			senderNotificationContext.setReceiverBanked(senderAccount.getIsBanked());
			senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
			long senderLanguageId = senderMobile.getRefCustomer().getPrefLang().getId();
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
					senderLanguageId, senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		if (receiverService != null) {
			MadarMerchantPayNotificationContext receiverNotificationContext = new MadarMerchantPayNotificationContext();
			receiverNotificationContext.setCurrency(currency);
			receiverNotificationContext.setReference(reference);
			MPAY_Account receiverAccount = context.getTransaction().getReceiverServiceAccount().getRefAccount();
			receiverNotificationContext.setReceiverBanked(receiverAccount.getIsBanked());
			context.getDataProvider().refreshEntity(receiverAccount);
			receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
			receiverNotificationContext.setExtraData1((String) context.getExtraData().get(MadarMerchantPaymentProcessor.NOTIFICATION_TOKEN));
			long receiverLanguageId = receiverService.getRefCorporate().getPrefLang().getId();
			receiverNotificationContext.setExtraData2(String.valueOf(receiverLanguageId));
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
					receiverLanguageId, receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
		}
		return notifications;
	}
}
