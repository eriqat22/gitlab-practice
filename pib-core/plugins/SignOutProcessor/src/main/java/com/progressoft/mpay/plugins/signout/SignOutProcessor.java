package com.progressoft.mpay.plugins.signout;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;

public class SignOutProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(SignOutProcessor.class);

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("inside processMessage ========");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessMessage(context);
			ValidationResult validationResult = SignOutValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
		} catch (Exception ex) {
			logger.error("Error in ProcessMessage", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private void preProcessMessage(MessageProcessingContext context) {
		logger.debug("inside preProcessMessage ========");
		MPayRequest message = context.getRequest();
		if (ReceiverInfoType.MOBILE.equals(message.getSenderType()))
			context.setSender(loadSenderMobileInfo(context, message));
		else if (ReceiverInfoType.CORPORATE.equals(message.getSenderType()))
			context.setSender(loadSenderServiceInfo(context, message));
		else
			throw new NotSupportedException("message.getSenderType() = " + message.getSenderType() + " not supported");
	}

	private ProcessingContextSide loadSenderServiceInfo(ProcessingContext context, MPayRequest message) {
		logger.debug("inside loadSenderServiceInfo ========");
		ProcessingContextSide side = new ProcessingContextSide();
		context.setSender(side);
		side.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
		if (side.getService() == null)
			return side;
		side.setCorporate(side.getService().getRefCorporate());
		side.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), side.getService(), null));
		if(side.getServiceAccount() == null)
			return side;
		side.setProfile(side.getServiceAccount().getRefProfile());
		MPAY_CorporateDevice device = context.getDataProvider().getCorporateDevice(message.getDeviceId());
//		selectFirst(side.getService().getRefCorporateServiceCorporateDevices(), having(on(MPAY_CorporateDevice.class).getDeviceID(), Matchers.equalTo(message.getDeviceId())))
		if (device != null)
			context.getExtraData().put(Constants.DEVICE_KEY, device);
		return side;
	}

	private ProcessingContextSide loadSenderMobileInfo(ProcessingContext context, MPayRequest message) {
		logger.debug("inside loadSenderMobileInfo ========");
		ProcessingContextSide side = new ProcessingContextSide();
		context.setSender(side);
		side.setMobile(context.getDataProvider().getCustomerMobile(message.getSender()));
		if (side.getMobile() == null)
			return side;
		side.setCustomer(side.getMobile().getRefCustomer());
		side.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), side.getMobile(), null));
		if(side.getMobileAccount() == null)
			return side;
		side.setProfile(side.getMobileAccount().getRefProfile());
		MPAY_CustomerDevice device = context.getDataProvider().getCustomerDevice(message.getDeviceId());
		if (device != null)
			context.getExtraData().put(Constants.DEVICE_KEY, device);
		return side;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		MPayRequest message = context.getRequest();
		logger.debug("Inside acceptMessage ...");
		if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
			MPAY_CustomerDevice device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DEVICE_KEY);
			device.setIsOnline(false);
			context.getDataProvider().mergeCustomerDevice(device);

		} else {
			MPAY_CorporateDevice device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DEVICE_KEY);
			device.setIsOnline(false);
			context.getDataProvider().mergeCorporateDevice(device);
		}

		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
		result.setResponse(generateResponse(context));
		return result;
	}

	private String generateResponse(MessageProcessingContext context) {
		logger.debug("Inside generateResponse ...");
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response.toString();
	}

	private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside createResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		return createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext context) {
		return null;
	}

}
