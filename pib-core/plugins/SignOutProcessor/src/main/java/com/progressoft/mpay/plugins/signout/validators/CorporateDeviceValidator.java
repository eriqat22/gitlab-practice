package com.progressoft.mpay.plugins.signout.validators;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.signout.Constants;

public class CorporateDeviceValidator {
	private static final Logger logger = LoggerFactory.getLogger(CustomerDeviceValidator.class);

	private CorporateDeviceValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		MPAY_CorporateDevice device = null;
		if (context.getExtraData().containsKey(Constants.DEVICE_KEY))
			device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DEVICE_KEY);
		if (device == null || (device.getDeletedFlag() != null && device.getDeletedFlag()))
			return new ValidationResult(ReasonCodes.DEVICE_NOT_FOUND, null, false);
		if (device.getIsBlocked())
			return new ValidationResult(ReasonCodes.DEVICE_BLOCKED, null, false);
		if (device.getIsStolen())
			return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
