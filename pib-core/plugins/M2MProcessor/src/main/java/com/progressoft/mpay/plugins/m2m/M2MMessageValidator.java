package com.progressoft.mpay.plugins.m2m;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class M2MMessageValidator {
    private static final Logger logger = LoggerFactory.getLogger(M2MMessageValidator.class);

    private M2MMessageValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ....");
        if (context == null)
            throw new NullArgumentException("context");

        M2MMessage m2MMessage = (M2MMessage) context.getRequest();
        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
                context.getOperation().getMessageType());
        if (!result.isValid())
            return result;
        if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
            result = validateCorporate(context, m2MMessage);
        else
            return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
        if (result.isValid())
            return validateSettings(context, m2MMessage);
        return result;
    }

    private static ValidationResult validateSettings(MessageProcessingContext context, M2MMessage request) {

        ValidationResult result = TransactionConfigValidator.validate(context.getTransactionConfig(),
                request.getAmount());
        if (!result.isValid())
            return result;

        result = AmountValidator.validate(context);
        if (!result.isValid())
            return result;

        result = LimitsValidator.validate(context);
        if (!result.isValid())
            return result;

        return PSPValidator.validate(context, false);
    }

    private static ValidationResult validateCorporate(MessageProcessingContext context, M2MMessage m2MMessage) {
        ValidationResult result = validateCorporateInfo(context);
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getService(),
                context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = PinCodeValidator.validate(context, context.getSender().getService(), m2MMessage.getPin());
        if (!result.isValid())
            return result;

        if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getService() != null) {
            if (!context.getReceiver().getService().getName().equalsIgnoreCase(m2MMessage.getReceiverInfo()))
                return new ValidationResult(ReasonCodes.RECEIVER_SERVICE_NOT_REGISTERED, null, false);
            if (m2MMessage.getSender().equalsIgnoreCase(m2MMessage.getReceiverInfo()))
                return new ValidationResult(ReasonCodes.SENDER_IS_SAME_AS_RECEIVER, null, false);
            result = validateReceiverService(context);
            if (!result.isValid())
                return result;
        }

        return result;
    }

    private static ValidationResult validateCorporateInfo(MessageProcessingContext context) {
        ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(),
                context.getRequest().getDeviceId(), context.getSender().getService().getId()));
        if (!result.isValid())
            return result;

        return result;
    }

    private static ValidationResult validateReceiverService(MessageProcessingContext context) {
        ValidationResult result = ServiceValidator.validate(context.getReceiver().getService(), false);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getReceiver().getCorporate(), false);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getReceiver().getServiceAccount(), false, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        return WalletCapValidator.validate(context);
    }
}
