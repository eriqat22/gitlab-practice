package com.progressoft.mpay.plugins.m2m;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.plugins.ProcessingContext;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateAcceptanceNotificationMessages ...");
		try {
			String receiver;
			String sender;
			String reference;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();
			MPAY_CorpoarteService senderService = context.getSender().getService();
			MPAY_CorpoarteService receiverService = context.getReceiver().getService();
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

			M2MNotificationContext notificationContext = new M2MNotificationContext();
			notificationContext
					.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);
			notificationContext.setFromService(context.getSender().getInfo());
			notificationContext.setToService(context.getReceiver().getInfo());
			if (senderService != null) {
				sender = senderService.getNotificationReceiver();
				context.getDataProvider().refreshEntity(context.getSender().getAccount());
				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getSender().getAccount().getBalance()));
				notificationContext.setSenderBanked(
						context.getSender().getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setSenderCharges(
						SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
			} else {
				sender = context.getSender().getInfo();
			}
			if (receiverService != null) {
				receiver = receiverService.getNotificationReceiver();
				context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getReceiver().getAccount().getBalance()));
				notificationContext.setReceiverBanked(context.getReceiver().getServiceAccount().getBankedUnbanked()
						.equals(BankedUnbankedFlag.BANKED));
				notificationContext.setReceiverCharges(
						SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
			} else {
				receiver = context.getReceiver().getInfo();
			}
			if (context.getReceiver().getNotes() != null) {
				notificationContext.setNotes(context.getReceiver().getNotes());
				notificationContext.setHasNotes(true);
			}
			notificationContext.setReceiver(receiver);
			notificationContext.setSender(sender);
			return generateNotifications(context, senderService, receiverService, notificationContext);
		} catch (Exception e) {
			logger.error("Error when CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static List<MPAY_Notification> generateNotifications(ProcessingContext context,
			MPAY_CorpoarteService senderService, MPAY_CorpoarteService receiverService,
			M2MNotificationContext notificationContext) {
		List<MPAY_Notification> notifications = new ArrayList<>();
		if (senderService != null) {
			notificationContext.setExtraData1(senderService.getName());
			final MPAY_Language prefLang = senderService.getRefCorporate().getPrefLang();
			
			addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
					senderService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER,
					NotificationChannelsCode.EMAIL);
			addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
					senderService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER,
					NotificationChannelsCode.SMS);
			addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
					senderService.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER,
					NotificationChannelsCode.PUSH_NOTIFICATION);
		}
		if (receiverService != null) {
			notificationContext.setExtraData1(receiverService.getName());
			final MPAY_Language prefLang = receiverService.getRefCorporate().getPrefLang();
			addNotification(context.getMessage(), notifications, receiverService.getEmail(), prefLang,
					receiverService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_RECEIVER,
					NotificationChannelsCode.EMAIL);
			addNotification(context.getMessage(), notifications, receiverService.getMobileNumber(), prefLang,
					receiverService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_RECEIVER,
					NotificationChannelsCode.SMS);
			if (receiverHasDeviceTokenToGeneratePushNotification(context)) {
				addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
						receiverService.getEnablePushNotification(), notificationContext,
						NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.PUSH_NOTIFICATION);
			}

		}
		notifications.isEmpty();
		return notifications;
	}

	private static boolean receiverHasDeviceTokenToGeneratePushNotification(ProcessingContext context) {
		return context.getReceiverNotificationToken() == null ? false : true;
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			M2MMessage request = M2MMessage
					.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));

			MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(),
					request.getSenderType());
			if (service == null)
				return new ArrayList<>();

			MPAY_Language language = service.getRefCorporate().getPrefLang();
			M2MNotificationContext notificationContext = new M2MNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext
					.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
			notificationContext.setReceiver(request.getReceiverInfo());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(
					MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
			notificationContext.setExtraData1(service.getName());

			addNotification(context.getMessage(), notifications, service.getEmail(), language, service.getEnableEmail(),
					notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
			addNotification(context.getMessage(), notifications, service.getMobileNumber(), language,
					service.getEnableSMS(), notificationContext, NotificationType.REJECTED_SENDER,
					NotificationChannelsCode.SMS);
			addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), language,
					service.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_SENDER,
					NotificationChannelsCode.PUSH_NOTIFICATION);
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	public static List<MPAY_Notification> createReversalNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateReversalNotificationMessages ...");
		try {

			String reference;
			List<MPAY_Notification> notifications = new ArrayList<>();

			reference = context.getTransaction().getReference();
			MPAY_CorpoarteService senderService = context.getTransaction().getSenderService();
			MPAY_CorpoarteService receiverService = context.getTransaction().getReceiverService();
			String currency = context.getTransaction().getCurrency().getStringISOCode();
			if (senderService != null) {
				M2MNotificationContext senderNotificationContext = new M2MNotificationContext();
				senderNotificationContext.setCurrency(currency);
				senderNotificationContext.setReference(reference);
				MPAY_Account senderAccount = context.getTransaction().getSenderServiceAccount().getRefAccount();
				context.getDataProvider().refreshEntity(senderAccount);
				senderNotificationContext.setReceiverBalance(
						SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
				senderNotificationContext.setReceiverBanked(context.getTransaction().getSenderServiceAccount()
						.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				senderNotificationContext.setExtraData1(senderService.getName());

				final MPAY_Language prefLang = senderService.getRefCorporate().getPrefLang();
				addNotification(context.getMessage(), notifications, senderService.getEmail(), prefLang,
						senderService.getEnableEmail(), senderNotificationContext,
						NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
				addNotification(context.getMessage(), notifications, senderService.getMobileNumber(), prefLang,
						senderService.getEnableSMS(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL,
						NotificationChannelsCode.SMS);
				addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
						senderService.getEnablePushNotification(), senderNotificationContext,
						NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.PUSH_NOTIFICATION);

			}
			if (receiverService != null) {
				M2MNotificationContext receiverNotificationContext = new M2MNotificationContext();
				receiverNotificationContext.setCurrency(currency);
				receiverNotificationContext.setReference(reference);
				MPAY_Account receiverAccount = context.getTransaction().getReceiverServiceAccount().getRefAccount();
				context.getDataProvider().refreshEntity(receiverAccount);
				receiverNotificationContext.setReceiverBalance(
						SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
				receiverNotificationContext.setReceiverBanked(context.getTransaction().getReceiverServiceAccount()
						.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				receiverNotificationContext.setExtraData1(receiverService.getName());

				final MPAY_Language prefLang = receiverService.getRefCorporate().getPrefLang();
				addNotification(context.getMessage(), notifications, receiverService.getEmail(), prefLang,
						receiverService.getEnableEmail(), receiverNotificationContext,
						NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.EMAIL);
				addNotification(context.getMessage(), notifications, receiverService.getMobileNumber(), prefLang,
						receiverService.getEnableSMS(), receiverNotificationContext,
						NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.SMS);
				addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
						receiverService.getEnablePushNotification(), receiverNotificationContext,
						NotificationType.TRANSACTION_REVERSAL, NotificationChannelsCode.PUSH_NOTIFICATION);
			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateReversalNotificationMessages", e);
			return new ArrayList<>();
		}
	}
}