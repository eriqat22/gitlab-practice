package com.progressoft.mpay.plugins.otp;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.time.DateUtils;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.HashingAlgorithm;
import com.progressoft.mpay.MPayCryptographer.DefaultCryptographer;
import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.OTPGenerator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ClientsOTP;
import com.progressoft.mpay.entities.MPAY_Language;
import com.progressoft.mpay.entities.MPAY_MessageType;
import com.progressoft.mpay.entities.MPAY_MessageTypes_NLS;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;

public class OTPProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(OTPProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = OTPValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in OTPProcessor", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
			String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
				ProcessingStatusCodes.REJECTED);
		String response = generateResponse(context, false);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
		OTPMessage message = (OTPMessage) context.getRequest();
		int pinCodeLength = context.getSystemParameters().getPinCodeLength();
		String otp = OTPGenerator.generateCode(pinCodeLength);
		MPAY_MessageType messageType = (MPAY_MessageType) context.getExtraData().get(Constants.MessageTypeKey);
		String encryptedOTP = DefaultCryptographer.INSTANCE.get().encrypt(otp, false);
		context.getExtraData().put(Constants.OTPCodeKey, "\"" + encryptedOTP + "\"");
		MPAY_ClientsOTP clientOTP = new MPAY_ClientsOTP();
		clientOTP.setMessageType(messageType);
		clientOTP.setRequestTime(SystemHelper.getSystemTimestamp());
		clientOTP.setCode(HashingAlgorithm.hash(context.getSystemParameters(), otp));
		if (context.getSender().getMobile() == null)
			clientOTP.setSenderService(context.getSender().getService());
		else
			clientOTP.setSenderMobile(context.getSender().getMobile());
		clientOTP.setMaxAmount(message.getAmount());
		clientOTP.setCurrency(context.getSystemParameters().getDefaultCurrency());
		context.getDataProvider().persistOTP(clientOTP);

		String response = generateResponse(context, true);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		result.getNotifications().add(createNotification(context));
		return result;
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("isAccepted: " + isAccepted);
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
				context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
					context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response.toString();
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException{
		logger.debug("Inside PreProcessMessage ...");
		OTPMessage message = OTPMessage.parseMessage(context.getRequest());
		context.setAmount(message.getAmount());
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setRequest(message);
		if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
			sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
			if (sender.getMobile() == null)
				return;
			sender.setCustomer(sender.getMobile().getRefCustomer());
			sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(),
					message.getSenderAccount()));
			if (sender.getMobileAccount() == null)
				return;
			sender.setProfile(sender.getMobileAccount().getRefProfile());
		} else {
			sender.setService(
					context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
			if (sender.getService() == null)
				return;
			sender.setCorporate(sender.getService().getRefCorporate());
			sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(),
					message.getSenderAccount()));
			if (sender.getServiceAccount() == null)
				return;
			sender.setProfile(sender.getServiceAccount().getRefProfile());
		}
		MPAY_MessageType messageType = context.getLookupsLoader().getMessageTypes(message.getMessageType());
		if (messageType == null)
			return;
		context.getExtraData().put(Constants.MessageTypeKey, messageType);
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
			String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		logger.debug("Inside Reverse ...");
		return null;
	}

	private MPAY_Notification createNotification(MessageProcessingContext context) {
		logger.debug("Inside CreateNotification ...");

		OTPNotificationContext notificationContext = new OTPNotificationContext();
		notificationContext.setReference(context.getMessage().getReference());
		notificationContext.setOTP((String) context.getExtraData().get(Constants.OTPCodeKey));
		notificationContext
				.setExpiryTime(SystemHelper.formatDate(DateUtils.addMinutes(SystemHelper.getCurrentDateTime(),
						context.getSystemParameters().getOtpValidityInMinutes()), "dd-MM-yyyy HH:mm:ss"));
		notificationContext.setAmount(context.getAmount());
		notificationContext.setCurrency(context.getLookupsLoader().getDefaultCurrency().getStringISOCode());
		MPAY_Notification notification = null;
		MPAY_Language language = null;
		try {
			String sender;
			String notificationChannelCode;
			if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)) {
				sender = context.getSender().getMobile().getMobileNumber();
				language = context.getSender().getCustomer().getPrefLang();
				notificationChannelCode = NotificationChannelsCode.SMS;
			} else {
				sender = context.getSender().getService().getName();
				language = context.getSender().getCorporate().getPrefLang();
				notificationChannelCode = NotificationChannelsCode.EMAIL;
			}
			MPAY_MessageType messageType = (MPAY_MessageType) context.getExtraData().get(Constants.MessageTypeKey);
			notificationContext.setMessageType(getDescriptionTransalated(messageType, language.getCode()));
			notification = NotificationHelper.getInstance().createNotification(context.getMessage(),
					notificationContext, NotificationType.ACCEPTED_SENDER, context.getOperation().getId(),
					language.getId(), sender, notificationChannelCode);
		} catch (Exception e) {
			logger.error("Error in CreateNotification", e);
		}
		return notification;
	}

	private String getDescriptionTransalated(MPAY_MessageType messageType, String languageCode) {
		MPAY_MessageTypes_NLS nls = selectFirst(messageType.getMessageTypesNLS(),
				having(on(MPAY_MessageTypes_NLS.class).getLanguageCode(), Matchers.equalTo(languageCode)));
		if (nls == null)
			return messageType.getDescription();
		return nls.getDescription();
	}
}
