package com.progressoft.mpay.plugins.otp;

import java.math.BigDecimal;

import com.progressoft.mpay.plugins.NotificationContext;

public class OTPNotificationContext extends NotificationContext {
	private String otp;
	private String expiryTime;
	private BigDecimal amount;
	private String currency;
	private String messageType;

	public String getOTP() {
		return otp;
	}

	public void setOTP(String otp) {
		this.otp = otp;
	}

	public String getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
}
