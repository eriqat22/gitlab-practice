package com.progressoft.mpay.plugins.p2b.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class P2BReversalAdviseProcessor extends IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(P2BReversalAdviseProcessor.class);
	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

}
