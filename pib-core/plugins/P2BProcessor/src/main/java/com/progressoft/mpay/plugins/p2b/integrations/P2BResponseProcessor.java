package com.progressoft.mpay.plugins.p2b.integrations;

import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.p2b.NotificationProcessor;
import com.progressoft.mpay.plugins.p2b.P2BMessage;

public class P2BResponseProcessor extends IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(P2BResponseProcessor.class);

	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		try {
			logger.debug("Inside ProcessIntegration ...");
			preProcessIntegration(context);
			if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return null;
			if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
				return accept(context);
			else
				return reject(context);
		} catch (Exception ex) {
			logger.error("Error in ProcessIntegration", ex);
			return reject(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside PreProcessIntegration ...");
		if (context.getMessage() != null)
			context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		P2BMessage request = null;
		try {
			request = P2BMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
		} catch (MessageParsingException ex) {
			logger.error("Error while parsing message", ex);
			throw new WorkflowException(ex);
		}
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setAmount(request.getAmount());
		sender.setMobile(context.getTransaction().getSenderMobile());
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setMobileAccount(context.getTransaction().getSenderMobileAccount());
		sender.setAccount(sender.getMobileAccount().getRefAccount());
		sender.setBanked(sender.getMobile().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
		sender.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(), sender.getAccount().getAccNumber()));
		sender.setInfo(sender.getMobile().getMobileNumber());
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

		context.setOperation(context.getMessage().getRefOperation());
		context.setDirection(context.getTransaction().getDirection().getId());
		receiver.setService(context.getTransaction().getReceiverService());
		long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
		if (receiver.getService() == null) {
			context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false, context.getOperation()
					.getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT, context.getOperation().getId()));
			context.getReceiver().setInfo(request.getReceiverInfo());
			return;
		}
		receiver.setServiceAccount(context.getTransaction().getReceiverServiceAccount());
		receiver.setAccount(receiver.getServiceAccount().getRefAccount());
		receiver.setCorporate(receiver.getService().getRefCorporate());
		receiver.setBank(receiver.getService().getBank());
		receiver.setBanked(receiver.getService().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
		receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(), receiver.getAccount().getAccNumber()));
		receiver.setProfile(receiver.getServiceAccount().getRefProfile());
		receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

		context.setDirection(context.getTransaction().getDirection().getId());
		context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType, sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(), context
				.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT, context.getOperation().getId()));
		handlePushNotification(context,request);
	}

	private void handlePushNotification(IntegrationProcessingContext context, P2BMessage message) {
		MPAY_CustomerDevice senderCustomerDevice = context.getDataProvider().getCustomerDevice(message.getDeviceId());
		String receiverDeviceToken = context.getReceiver().getService().getRefCorporateServiceCorporateDevices()
				.stream()
				.filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
				.findFirst()
				.map(MPAY_CorporateDevice::getExtraData).orElse(null);
		context.setSenderNotificationToken(senderCustomerDevice != null ? senderCustomerDevice.getExtraData() : null);
		context.setReceiverNotificationToken(receiverDeviceToken);
	}

	private IntegrationProcessingResult accept(IntegrationProcessingContext context) {
		logger.debug("Inside Accept ...");
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
		result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		return result;
	}

	private IntegrationProcessingResult reject(IntegrationProcessingContext context) {
		logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		String reasonDescription = null;
		if (context.getMpClearIsoMessage().getField(47) != null)
			reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
				context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
		result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
		return result;
	}

	private IntegrationProcessingResult reject(IntegrationProcessingContext context, String processingCode, String reasonCode, String reasonDescription) {
		logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, processingCode, reasonCode, reasonDescription, null, false);
		result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
		return result;
	}
}
