package com.progressoft.mpay.plugins.p2b.integrations;

import javax.transaction.NotSupportedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;

public class IntegrationProcessorFactory {
	private static final Logger logger = LoggerFactory.getLogger(IntegrationProcessorFactory.class);

	private IntegrationProcessorFactory() {

	}

	public static IntegrationProcessor createProcessor(int messageType) throws NotSupportedException {
		logger.debug("Inside CreateProcessor ...");
		logger.debug("messageType: " + messageType);
		if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)
			return new P2BInwardProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE)
			return new P2BResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_RESPONSE)
			return new P2BOfflineResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST)
			return new P2BReversalAdviseProcessor();
		else
			throw new NotSupportedException("messageType == " + messageType);
	}
}
