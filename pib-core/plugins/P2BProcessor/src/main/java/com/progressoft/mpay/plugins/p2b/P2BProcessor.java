package com.progressoft.mpay.plugins.p2b;

import java.math.BigDecimal;
import java.sql.SQLException;

import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.messages.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.plugins.p2b.integrations.IntegrationProcessor;
import com.progressoft.mpay.plugins.p2b.integrations.IntegrationProcessorFactory;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class P2BProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(P2BProcessor.class);

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessMessage(context);
			ValidationResult validationResult = P2BMessageValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
						validationResult.getReasonDescription());

		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
					e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage", ex);
			return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
					ex.getMessage());
		}
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException {
		logger.debug("Inside AcceptMessage ...");
		P2BMessage message = (P2BMessage) context.getRequest();
		MessageProcessingResult result = new MessageProcessingResult();
		MPAY_ProcessingStatus status;
		MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
		String reasonDescription = null;

		context.getMessage().setReason(reason);
		MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT,
				BigDecimal.ZERO, message.getNotes());
		result.setMessage(context.getMessage());
		context.setTransaction(transaction);
		result.setTransaction(transaction);
		context.getDataProvider().persistProcessingResult(result);
		JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(),
				context.getLookupsLoader(), transaction);
		if (postingResult.isSuccess()) {
			BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
			if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
				status = handleMPClear(context, message, result);
			} else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			} else {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			}
		} else {
			if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
				reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
				context.getMessage().setReasonDesc(postingResult.getReason());
			} else
				reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
		}
		context.getMessage().setProcessingStatus(status);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
		transaction.setReason(context.getMessage().getReason());
		transaction.setReasonDesc(context.getMessage().getReasonDesc());
		result.setStatus(status);
		result.setStatusDescription(context.getMessage().getReasonDesc());
		handleNotifications(context, result, status);
		OTPHanlder.removeOTP(context);
		handleCommissions(context, status);
		handleLimits(context, status, transaction);
		return result;
	}

	private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
		if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return;
		if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.OUTWARD)
			CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
					CommissionDirections.SENDER);
		if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.INWARD)
			CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
					CommissionDirections.RECEIVER);
	}

	private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result, MPAY_ProcessingStatus status) {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
			result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
	}

	private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status,
			MPAY_Transaction transaction) throws SQLException {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)
				|| status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
			context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
					context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
					SystemHelper.getCurrentDateWithoutTime());
		}
	}

	private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, P2BMessage message,
			MessageProcessingResult result) {
		MPAY_ProcessingStatus status;
		Integer mpClearIntegType;
		if (context.getSystemParameters().getOfflineModeEnabled()
				&& context.getDirection() == TransactionDirection.ONUS) {
			mpClearIntegType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
			result.setHandleMPClearOffline(true);
		} else {
			mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
		}
		boolean isSenderAlias = context.getSender().getMobile().getNotificationShowType()
				.equals(NotificationShowTypeCodes.ALIAS) && context.getSender().getMobile().getAlias() != null
				&& context.getSender().getMobile().getAlias().length() > 0;
		IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, isSenderAlias,
				message.getReceiverType().equals(ReceiverInfoType.ALIAS));
		MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage,
				Integer.toHexString(mpClearIntegType));
		messageLog.setRefMessage(context.getMessage());
		result.setMpClearMessage(messageLog);
		return status;
	}

	private void preProcessMessage(MessageProcessingContext context)
			throws MessageParsingException, WorkflowException, SQLException {
		logger.debug("Inside PreProcessMessage ...");
		P2BMessage message = P2BMessage.parseMessage(context.getRequest());

		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setAmount(message.getAmount());
		sender.setInfo(message.getSender());
		context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
		sender.setMobile(context.getDataProvider().getCustomerMobile(message.getSender()));
		if (sender.getMobile() == null)
			return;
		sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(),
				message.getSenderAccount()));
		if (sender.getMobileAccount() == null)
			return;
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setAccount(sender.getMobileAccount().getRefAccount());
		sender.setBank(sender.getMobileAccount().getBank());
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setLimits(context.getDataProvider().getAccountLimits(sender.getMobile().getId(),
				context.getOperation().getMessageType().getId()));
		sender.setLimits(context.getDataProvider().getAccountLimits(sender.getMobile().getId(), context.getOperation().getMessageType().getId()));
		sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));

		receiver.setInfo(message.getReceiverInfo());
		receiver.setService(
				context.getDataProvider().getCorporateService(message.getReceiverInfo(), message.getReceiverType()));
		long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
		if (receiver.getService() != null) {
			receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(),
					receiver.getService(), message.getReceiverAccount()));
			if (receiver.getServiceAccount() == null)
				return;
			receiver.setCorporate(receiver.getService().getRefCorporate());
			receiver.setAccount(receiver.getServiceAccount().getRefAccount());
			receiver.setBank(receiver.getServiceAccount().getBank());
			receiver.setProfile(receiver.getServiceAccount().getRefProfile());
			receiver.setBanked(receiver.getAccount().getIsBanked());
			receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

			context.setDirection(TransactionDirection.ONUS);
			context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
					sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(),
					context.getOperation().getMessageType().getId(), context.getTransactionNature(),
					context.getOperation().getId()));
		} else {
			context.getReceiver().setService(context.getLookupsLoader().getPSP().getPspMPClearService());
			context.getReceiver().setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(),
					context.getReceiver().getService(), null));
			if (context.getReceiver().getServiceAccount() != null)
				context.getReceiver().setBank(context.getReceiver().getServiceAccount().getBank());
			context.setDirection(TransactionDirection.OUTWARD);
			context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
					sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
					context.getOperation().getMessageType().getId(), context.getTransactionNature(),
					context.getOperation().getId()));
		}
		if (context.getTransactionConfig() != null)
			context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
		OTPHanlder.loadOTP(context, message.getPin());
		ChargesCalculator.calculate(context);
		TaxCalculator.claculate(context);
		handlePushNotification(context,message);
	}

	private void handlePushNotification(MessageProcessingContext context, P2BMessage message) {
		String receiverDeviceToken = context.getReceiver().getService().getRefCorporateServiceCorporateDevices()
				.stream()
				.filter(d -> d.getIsDefault() && (d.getDeletedFlag() == null || !d.getDeletedFlag()))
				.findFirst()
				.map(MPAY_CorporateDevice::getExtraData).orElse(null);
		MPAY_CustomerDevice senderCustomerDevice = context.getDataProvider().getCustomerDevice(message.getDeviceId());
		context.setSenderNotificationToken(senderCustomerDevice != null ? senderCustomerDevice.getExtraData() : null);
		context.setReceiverNotificationToken(receiverDeviceToken);
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode,
			String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		if (context.getTransaction() != null) {
			BankIntegrationResult result = BankIntegrationHandler.reverse(context);
			if (result != null) {
				TransactionHelper.reverseTransaction(context);
				try {
					P2BMessage message = P2BMessage.parseMessage(context.getRequest());
					context.getDataProvider()
							.updateAccountLimit(
									SystemHelper
											.getMobileAccount(context.getSystemParameters(),
													context.getSender().getMobile(), message.getSenderAccount())
											.getRefAccount().getId(),
									context.getMessage().getRefOperation().getMessageType().getId(),
									context.getTransaction().getTotalAmount().negate(), -1,
									SystemHelper.getCurrentDateWithoutTime());
				} catch (Exception e) {
					logger.error("Inside RejectMessage Reverse Limits", e);
				}
			}
		}
		return MessageProcessingResult.create(context, status, reason, reasonDescription);
	}

	private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isSenderAlias,
			boolean isReceiverAlias) {
		logger.debug("Inside CreateMPClearRequest ...");
		try {
			P2BMessage request = (P2BMessage) context.getRequest();
			String messageID = context.getDataProvider().getNextMPClearMessageId();
			BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
			IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
					.newMessage(type);
			message.setBinary(false);

			String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(),
					context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
			String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge,
					AmountType.CHARGE);

			message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
			MPClearHelper.setMessageEncoded(message);
			message.setField(7, new IsoValue<String>(IsoType.DATE10,
					MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
			message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
			message.setField(49,
					new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

			String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getMobile(),
					context.getSender().getMobileAccount(), isSenderAlias);
			MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

			String account2;
			if (context.getReceiver().getService() == null)
				account2 = MPClearHelper.getAccount(context.getSystemParameters(), request.getReceiverInfo(),
						request.getReceiverType());
			else
				account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getService(),
						context.getReceiver().getServiceAccount(), isReceiverAlias);

			MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

			String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
			message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
			message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

			return message;
		} catch (Exception e) {
			logger.error("Error when CreateMPClearRequest", e);
			return null;
		}
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		IsoMessage mpClearIsoMessage = context.getMpClearIsoMessage();
		try {
			IntegrationProcessor processor = IntegrationProcessorFactory.createProcessor(mpClearIsoMessage.getType());
			IntegrationProcessingResult result = processor.ProcessIntegration(context);
			handleCommissions(context, result.getProcessingStatus());
			return result;
		} catch (Exception e) {
			logger.error("Error when ProcessIntegration", e);
			IntegrationProcessingResult result = IntegrationProcessingResult.create(context,
					ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null,
					context.isRequest());
			if (context.isRequest())
				result.setMpClearOutMessage(MPClearHelper.createMPClearResponse(context,
						mpClearIsoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(),
						MPClearHelper.getResponseType(mpClearIsoMessage.getType()), result.getReason().getCode(),
						result.getReasonDescription(), String.valueOf(ProcessingCodeRanges.CREDIT_START_VALUE)));
			return result;
		}
	}

	private MPayResponse generateResponse(MessageProcessingContext context) {
		logger.debug("Inside GenerateResponse ...");
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
				context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
					context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response;
	}
	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		logger.debug("Inside Reverse ...");
		return null;
	}
}
