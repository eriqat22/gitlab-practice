package com.progressoft.mpay.plugins.p2b;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;

import static com.progressoft.mpay.notifications.NotificationHelper.addNotification;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateAcceptanceNotificationMessages ...");
		try {
			String receiver;
			String sender;
			String reference;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();
			MPAY_CustomerMobile senderMobile = context.getSender().getMobile();
			MPAY_CorpoarteService receiverService = context.getReceiver().getService();
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

			P2BNotificationContext notificationContext = new P2BNotificationContext();
			notificationContext
					.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);
			if (senderMobile != null) {
				sender = senderMobile.getMobileNumber();
				context.getDataProvider().refreshEntity(context.getSender().getAccount());
				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getSender().getAccount().getBalance()));
				notificationContext.setSenderBanked(senderMobile.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setSenderCharges(
						SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
				if (senderMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS)
						&& senderMobile.getAlias() != null && !senderMobile.getAlias().isEmpty())
					sender = senderMobile.getAlias();
			} else
				sender = context.getSender().getInfo();
			if (receiverService != null) {
				receiver = receiverService.getName();
				context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(),
						context.getReceiver().getAccount().getBalance()));
				notificationContext
						.setReceiverBanked(receiverService.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setReceiverCharges(
						SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
			} else
				receiver = context.getReceiver().getInfo();
			notificationContext.setReceiver(receiver);
			notificationContext.setSender(sender);
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (senderMobile != null) {
				notificationContext.setExtraData1(senderMobile.getMobileNumber());
				final MPAY_Language prefLang = senderMobile.getRefCustomer().getPrefLang();
				addNotification(context.getMessage(), notifications, senderMobile.getEmail(), prefLang,
						senderMobile.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.EMAIL);
				addNotification(context.getMessage(), notifications, senderMobile.getMobileNumber(), prefLang,
						senderMobile.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_SENDER, NotificationChannelsCode.SMS);
				addNotification(context.getMessage(), notifications, context.getSenderNotificationToken(), prefLang,
						senderMobile.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_SENDER,
						NotificationChannelsCode.PUSH_NOTIFICATION);
			}
			if (receiverService != null) {
				notificationContext.setExtraData1(receiverService.getName());
				final MPAY_Language prefLang = receiverService.getRefCorporate().getPrefLang();
				addNotification(context.getMessage(), notifications, receiverService.getEmail(), prefLang,
						receiverService.getEnableEmail(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.EMAIL);
				addNotification(context.getMessage(), notifications, receiverService.getMobileNumber(), prefLang,
						receiverService.getEnableSMS(), notificationContext, NotificationType.ACCEPTED_RECEIVER, NotificationChannelsCode.SMS);
				addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), prefLang,
						receiverService.getEnablePushNotification(), notificationContext, NotificationType.ACCEPTED_RECEIVER,
						NotificationChannelsCode.PUSH_NOTIFICATION);
			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			P2BMessage request = P2BMessage
					.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			MPAY_Language language = context.getSystemParameters().getSystemLanguage();
			MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
			if (mobile != null) {
				language = mobile.getRefCustomer().getPrefLang();
			}
			P2BNotificationContext notificationContext = new P2BNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext
					.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
			notificationContext.setReceiver(request.getReceiverInfo());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(
					MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());

			if(mobile !=null) {
				notificationContext.setExtraData1(mobile.getMobileNumber());
				addNotification(context.getMessage(), notifications, mobile.getEmail(), language,
						mobile.getEnableEmail(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.EMAIL);
				addNotification(context.getMessage(), notifications, mobile.getMobileNumber(), language,
						mobile.getEnableSMS(), notificationContext, NotificationType.REJECTED_SENDER, NotificationChannelsCode.SMS);
				addNotification(context.getMessage(), notifications, context.getReceiverNotificationToken(), language,
						mobile.getEnablePushNotification(), notificationContext, NotificationType.REJECTED_SENDER,
						NotificationChannelsCode.PUSH_NOTIFICATION);
			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}
}
