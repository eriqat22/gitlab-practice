package com.progressoft.mpay.plugins.jvsdetails;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entities.MPAY_JvDetail;

public class JVDetails {
	private long id;
	private long transactionId;
	private long serial;
	private String description;
	private String type;
	private String flag;
	private String accountNo;
	private String currency;
	private double amount;
	private boolean isPosted;

	public JVDetails() {
		//
	}

	public JVDetails(MPAY_JvDetail jvDetails) {
		if (jvDetails == null)
			throw new NullArgumentException("jvDetails");
		this.id = jvDetails.getId();
		this.transactionId = jvDetails.getRefTrsansaction().getId();
		this.serial = jvDetails.getSerial();
		this.description = jvDetails.getDescription();
		this.type = jvDetails.getJvType().getCode();
		this.flag = jvDetails.getDebitCreditFlag();
		this.accountNo = jvDetails.getRefAccount().getAccNumber();
		this.currency = jvDetails.getCurrency().getStringISOCode();
		this.amount = jvDetails.getAmount().doubleValue();
		this.isPosted = jvDetails.getIsPosted();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getSerial() {
		return serial;
	}

	public void setSerial(long serial) {
		this.serial = serial;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isPosted() {
		return isPosted;
	}

	public void setPosted(boolean isPosted) {
		this.isPosted = isPosted;
	}
}
