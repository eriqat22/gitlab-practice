package com.progressoft.mpay.plugins.jvsdetails;

import java.sql.Timestamp;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class JVsDetailsMessage extends MPayRequest {
	private static final Logger logger = LoggerFactory.getLogger(JVsDetailsMessage.class);

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String ACCOUNT_NO_KEY = "accountNo";
	private static final String TYPE_KEY = "type";
	private static final String FLAG_KEY = "flag";
	private static final String TRANSACTION_ID_KEY = "transactionId";
	private static final String FROM_TIME_KEY = "fromTime";
	private static final String TO_TIME_KEY = "toTime";

	private String accountNo;
	private String type;
	private String flag;
	private long transactionId;
	private Timestamp fromTime;
	private Timestamp toTime;

	public JVsDetailsMessage() {
		//
	}

	public JVsDetailsMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public Timestamp getFromTime() {
		return fromTime;
	}

	public void setFromTime(Timestamp fromTime) {
		this.fromTime = fromTime;
	}

	public Timestamp getToTime() {
		return toTime;
	}

	public void setToTime(Timestamp toTime) {
		this.toTime = toTime;
	}

	public static JVsDetailsMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		JVsDetailsMessage message = new JVsDetailsMessage(request);

		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(SENDER_KEY);
		if (message.getSenderType() == null
				|| !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(SENDER_TYPE_KEY);

		message.setAccountNo(message.getValue(ACCOUNT_NO_KEY));
		message.setType(message.getValue(TYPE_KEY));
		message.setFlag(message.getValue(FLAG_KEY));

		if (message.getValue(TRANSACTION_ID_KEY) != null) {
			try {
				message.setTransactionId(Long.parseLong(message.getValue(TRANSACTION_ID_KEY)));
			} catch (Exception ex) {
				logger.debug("Invalid transactionId", ex);
				throw new MessageParsingException(TRANSACTION_ID_KEY);
			}
		}

		try {
			message.setFromTime(new Timestamp(SystemHelper.parseDate(message.getValue(FROM_TIME_KEY), DATE_FORMAT).getTime()));
		} catch (Exception ex) {
			logger.debug("Invalid fromDate", ex);
			throw new MessageParsingException(FROM_TIME_KEY);
		}

		try {
			message.setToTime(new Timestamp(SystemHelper.parseDate(message.getValue(TO_TIME_KEY), DATE_FORMAT).getTime()));
		} catch (Exception ex) {
			logger.debug("Invalid toDate", ex);
			throw new MessageParsingException(TO_TIME_KEY);
		}
		return message;
	}
}