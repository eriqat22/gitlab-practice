package com.progressoft.mars.madar.corporatelimitsinquiry.dao;

import com.google.gson.GsonBuilder;
import com.progressoft.mars.madar.corporatelimitsinquiry.dao.limit.LimitScheme;
import com.progressoft.mpay.entities.MPAY_Profile;

public class Profile {

    private LimitScheme limitsScheme;

    public Profile(MPAY_Profile profile) {
        if (profile == null)
            throw new IllegalArgumentException("profile");
        this.limitsScheme = new LimitScheme(profile.getLimitsScheme());
    }


    public LimitScheme getLimitsScheme() {
        return this.limitsScheme;
    }

    public void setLimitsScheme(LimitScheme limitsScheme) {
        this.limitsScheme = limitsScheme;
    }


    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }

}
