package com.progressoft.mars.madar.corporatelimitsinquiry.message;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.MPayRequest;
import org.apache.commons.lang.StringUtils;

public class LimitInquiryMessage extends MPayRequest {

    private static final String SERVICE_NAME = "serviceName";
    private String serviceName;

    public LimitInquiryMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static LimitInquiryMessage parseAndValidateMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        LimitInquiryMessage message = new LimitInquiryMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(SENDER_KEY);
        if (message.getSenderType() == null || message.getSenderType().trim().length() == 0)
            throw new MessageParsingException(SENDER_TYPE_KEY);

        if (!message.getSenderType().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(SENDER_TYPE_KEY);

        message.setServiceName(message.getValue(SERVICE_NAME));
        if (StringUtils.isEmpty(message.getServiceName()))
            throw new MessageParsingException(SERVICE_NAME);
        return message;

    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

}
