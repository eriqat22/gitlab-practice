package com.progressoft.mars.madar.corporatelimitsinquiry.validator;

import com.progressoft.mars.madar.corporatelimitsinquiry.constant.Constant;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LimitInquiryValidator {


    private static final Logger logger = LoggerFactory.getLogger(LimitInquiryValidator.class);

    private LimitInquiryValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ...");
        if (context == null)
            throw new NullArgumentException("context");


        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
                context.getOperation().getMessageType());
        if (!result.isValid())
            return result;

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
            return validateCorporate(context);
        else
            return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);


    }


    private static ValidationResult validateCorporate(MessageProcessingContext context) {
        ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        result = LimitsValidator.validate(context);
        if (!result.isValid())
            return result;

        MPAY_CorpoarteService corporateService = (MPAY_CorpoarteService) context.getExtraData().get(Constant.SERVICE_NAME);
        if (corporateService == null)
            return new ValidationResult(ReasonCodes.RECEIVER_SERVICE_NOT_REGISTERED, (String) null, false);

        return result;

    }

}
