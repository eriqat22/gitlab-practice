package com.progressoft.mars.madar.corporatelimitsinquiry.constant;

public class Constant {

    public static final String LIMITS_DATA = "limitsData";
    public static final String CLIENT_LIMIT_DATA = "clientLimitData";
    public static final String SERVICE_NAME = "serviceName";
    public static final String BALANCE_KEY = "balance";
    public static final String CURRENCY_KEY = "currency";

}
