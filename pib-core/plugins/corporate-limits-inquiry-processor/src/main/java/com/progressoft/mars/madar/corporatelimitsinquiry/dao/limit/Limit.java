package com.progressoft.mars.madar.corporatelimitsinquiry.dao.limit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.entities.MPAY_Limit;
import com.progressoft.mpay.entities.MPAY_LimitsDetail;

import java.util.ArrayList;
import java.util.List;

public class Limit {
    private String description;
    private boolean isActive;
    private LimitScheme refScheme;
    private List<LimitsDetail> refLimitLimitsDetails;

    public Limit(MPAY_Limit limit) {
        if (limit == null)
            throw new IllegalArgumentException("Limit");

        this.description = limit.getDescription();
        this.isActive = limit.getIsActive();
        this.refLimitLimitsDetails = new ArrayList<>();
        for (MPAY_LimitsDetail detail : limit.getRefLimitLimitsDetails()) {
            this.refLimitLimitsDetails.add(new LimitsDetail(detail));
        }
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String paramString) {
        this.description = paramString;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(boolean paramBoolean) {
        this.isActive = paramBoolean;
    }

    public LimitScheme getRefScheme() {
        return this.refScheme;
    }

    public void setRefScheme(LimitScheme refScheme) {
        this.refScheme = refScheme;
    }

    public List<LimitsDetail> getRefLimitLimitsDetails() {
        return this.refLimitLimitsDetails;
    }

    public void setRefLimitLimitsDetails(List<LimitsDetail> paramList) {
        this.refLimitLimitsDetails = paramList;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

}
