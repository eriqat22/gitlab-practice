package com.progressoft.mpay.plugins.customerinformationupdate;

import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.WF_Customer;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerInfoUpdateValidator {

    private static final Logger logger = LoggerFactory.getLogger(CustomerInfoUpdateValidator.class);

    public static ValidationResult validate(MessageProcessingContext context)
    {
        logger.debug("Inside validate ...");
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getService(),
                context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, false);
        if (!result.isValid())
            return result;

        result = PinCodeValidator.validate(context, context.getSender().getService(), context.getRequest().getPin());
        if (!result.isValid())
            return result;

        result = validateReceiver(context);
        if(!result.isValid())
            return result;

        return new ValidationResult(ReasonCodes.VALID,null,true);
    }

    private static ValidationResult validateReceiver(MessageProcessingContext context) {

        if(context.getReceiver().getMobile() == null)
            return new ValidationResult(ReasonCodes.CUSTOMER_DOES_NOT_EXISTS,null,false);

        MPAY_Customer customer = context.getReceiver().getMobile().getRefCustomer();
        if(!customer.getIsLight())
            return new ValidationResult(ReasonCodes.CUSTOMER_ALREADY_REGISTERED,null,false);

        if (isCustomerStepAllowToMakeUpdate(customer, WF_Customer.STEP_Suspended)||
            isCustomerStepAllowToMakeUpdate(customer, WF_Customer.STEP_SuspensionApproval))
            return new ValidationResult(ReasonCodes.RECEIVER_CUSTOMER_SUSPENDED,null,false);

        if(isCustomerStepAllowToMakeUpdate(customer, WF_Customer.STEP_Rejected)||
           isCustomerStepAllowToMakeUpdate(customer, WF_Customer.STEP_DeletionApproval))
            return new ValidationResult(ReasonCodes.CUSTOMER_DOES_NOT_EXISTS,null,false);


        return new ValidationResult(ReasonCodes.VALID,null,true);
    }

    private static boolean isCustomerStepAllowToMakeUpdate(MPAY_Customer customer, String step_suspended) {
        return customer.getStatusId().getCode().equals(step_suspended);
    }


}
