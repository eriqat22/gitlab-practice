package com.progressoft.mpay.plugins.customerinformationupdate;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomerInfoUpdateRequest extends MPayRequest {
    private static final Logger logger = LoggerFactory.getLogger(CustomerInfoUpdateRequest.class);

    private static final String FIRST_NAME_KEY = "firstName";
    private static final String MID_NAME_KEY = "midName";
    private static final String LAST_NAME_KEY = "lastName";
    private static final String DATE_OF_BIRTH_KEY = "dateOfBirth";
    private static final String ID_TYPE_KEY = "idType";
    private static final String ID_NUMBER_KEY ="idNumber";
    private static final String MOBILE_NUMBER_KEY = "mobileNumber";
    private static final String ADDRESS_KEY = "address";
    private static final String PHOTO_KEY = "photo";


    private String firstName;
    private String middleName;
    private String lastName;
    private Date   dateOfBirth;
    private String idType;
    private String idNumber;
    private String mobileNumber;
    private String address;
    private String photo;


    public CustomerInfoUpdateRequest(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setRequestedId(request.getRequestedId());
        setExtraData(request.getExtraData());
    }

    public static CustomerInfoUpdateRequest parseMessage(MPayRequest request) throws MessageParsingException
    {
        final String BirthDateFormat = "yyyy-mm-dd";


        if(request == null)
            return null;

        CustomerInfoUpdateRequest messageRequest = new CustomerInfoUpdateRequest(request);

        if (messageRequest.getSender() == null || messageRequest.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (messageRequest.getSenderType() == null || !messageRequest.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        messageRequest.setFirstName(messageRequest.getValue(FIRST_NAME_KEY));
        if(messageRequest.getFirstName() == null || messageRequest.getFirstName().trim().length() == 0)
            throw new MessageParsingException(FIRST_NAME_KEY);

        messageRequest.setMiddleName(messageRequest.getValue(MID_NAME_KEY));
        if(messageRequest.getMiddleName() == null || messageRequest.getMiddleName().trim().length() == 0)
            throw new MessageParsingException(MID_NAME_KEY);

        messageRequest.setLastName(messageRequest.getValue(LAST_NAME_KEY));
        if(messageRequest.getLastName() == null || messageRequest.getLastName().trim().length() == 0)
            throw new MessageParsingException(LAST_NAME_KEY);

        SimpleDateFormat format = new SimpleDateFormat(BirthDateFormat);
        try {
                messageRequest.setDateOfBirth(format.parse(messageRequest.getValue(DATE_OF_BIRTH_KEY)));
        } catch (ParseException e) {
            logger.debug("Invalid Date of Birth format", e);
            throw new MessageParsingException(DATE_OF_BIRTH_KEY);
        }

        messageRequest.setIdType(messageRequest.getValue(ID_TYPE_KEY));
        if(messageRequest.getIdType() == null || messageRequest.getIdType().trim().length() == 0)
            throw new MessageParsingException(ID_TYPE_KEY);

        messageRequest.setIdNumber(messageRequest.getValue(ID_NUMBER_KEY));
        if(messageRequest.getIdNumber() == null || messageRequest.getIdNumber().trim().length()== 0)
            throw new MessageParsingException(ID_NUMBER_KEY);

        messageRequest.setMobileNumber(messageRequest.getValue(MOBILE_NUMBER_KEY));
        if (messageRequest.getMobileNumber() == null || messageRequest.getMobileNumber().trim().length() == 0)
            throw new MessageParsingException(MOBILE_NUMBER_KEY);

        messageRequest.setAddress(messageRequest.getValue(ADDRESS_KEY));
        messageRequest.setPhoto(messageRequest.getValue(PHOTO_KEY));

        return messageRequest;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
