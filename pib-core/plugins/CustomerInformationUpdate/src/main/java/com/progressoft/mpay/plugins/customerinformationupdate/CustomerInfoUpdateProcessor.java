package com.progressoft.mpay.plugins.customerinformationupdate;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.Jfw;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Base64;

public class CustomerInfoUpdateProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(CustomerInfoUpdateProcessor.class);
    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {

        logger.debug("Inside ProcessMessage ...");

        if(context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult result = CustomerInfoUpdateValidator.validate(context);
            if (result.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, result.getReasonCode(), result.getReasonDescription());
        }

        catch (MessageParsingException e) {
            logger.debug("Error while parsing message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in CustomerInquiryProcessor", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws WorkflowException, MessageParsingException {
        logger.debug("Inside AcceptMessage ...");
        updateCustomerInformation(context);
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context, true);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private void updateCustomerInformation(MessageProcessingContext context) throws MessageParsingException {
        CustomerInfoUpdateRequest request = CustomerInfoUpdateRequest.parseMessage(context.getRequest());
        MPAY_CustomerMobile mobile = context.getReceiver().getMobile();
        MPAY_Customer customer = mobile.getRefCustomer();//context.getDataProvider().getCustomer(request.getIdType(),request.getIdNumber());
        MPAY_MobileAccount account = mobile.getMobileMobileAccounts().stream().filter(MPAY_MobileAccount::getIsDefault).findAny().orElse(null);

        customer.setFirstName(request.getFirstName());
        customer.setMiddleName(request.getMiddleName());
        customer.setLastName(request.getLastName());
        customer.setDob(request.getDateOfBirth());
        customer.setAddress(request.getAddress());
        customer.setRegAgent(context.getSender().getService());
        MPAY_Profile profile = getFullWalletProfile(context);

        customer.setRefProfile(profile);
        mobile.setRefProfile(profile);

        customer.setIsLight(false);
        customer.setKycTemplate(getCustomerKyc(context));
        customer.setFullName(request.getFirstName() + " " + request.getMiddleName() + " " + request.getLastName());

        if(account != null) {
            context.getDataProvider().updateMobileAccount(account);
            account.setRefProfile(profile);
        }

        context.getDataProvider().updateCustomerMobile(mobile);
        context.getDataProvider().updateCustomer(customer);

        addCustomerAttachment(context, request, customer);

        if(!customer.getStatusId().getCode().equals(WF_Customer.STEP_Approval)) {
            Jfw jfw = (Jfw) AppContext.getApplicationContext().getBean("jfw");
            jfw.executeAction(MPAYView.CUSTOMER.viewName, customer, WF_Customer.ACTION_NAME_SVC_Approval);
            jfw.executeAction(MPAYView.CUSTOMER.viewName, customer, WF_Customer.ACTION_NAME_Approve);
        }
    }

    private void addCustomerAttachment(MessageProcessingContext context, CustomerInfoUpdateRequest request, MPAY_Customer customer) {
        MPAY_CustomerAtt customerAtt = new MPAY_CustomerAtt();
        customerAtt.setRecordId(customer.getId().toString());
        customerAtt.setContentType("image/jpeg");
        customerAtt.setAttFile(Base64.getMimeDecoder().decode(request.getPhoto()));
        customerAtt.setName("PhotoId_"+customer.getId()+".JPG");
        customerAtt.setEntityId("com.progressoft.mpay.entities.MPAY_Customer");
        customerAtt.setSize(new BigDecimal(request.getPhoto().length()));
        customerAtt.setTenantId(request.getTenant());
        customerAtt.setImageType("23");
        customerAtt.setOrgId(customer.getOrgId());

        context.getDataProvider().persistCustomerAtt(customerAtt);
    }

    private MPAY_CustomerKyc getCustomerKyc(MessageProcessingContext context) {
        return context.getLookupsLoader().getCustomerKyc(SystemParameters.getInstance().getKycFullWalletId());
    }

    private MPAY_Profile getFullWalletProfile(MessageProcessingContext context) {
        return context.getLookupsLoader().getProfile(LookupsLoader.getInstance().getSystemConfigurations(SysConfigKeys.FULL_WALLET_PROFILE).getConfigValue());
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
        logger.debug("Inside PreProcessMessage ...");

        CustomerInfoUpdateRequest request = CustomerInfoUpdateRequest.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);

        sender.setService(context.getDataProvider().getCorporateService(request.getSender(), ReceiverInfoType.CORPORATE));
        if(sender.getService() == null)
            return;

        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setServiceAccount(
                SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
        if (sender.getServiceAccount() == null)
            return;

        receiver.setMobile(context.getDataProvider().getCustomerMobile(request.getMobileNumber()));
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        logger.debug("Inside GenerateResponse ...");
        logger.debug("isAccepted: " + isAccepted);

        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());


        return response.toString();
    }
}
