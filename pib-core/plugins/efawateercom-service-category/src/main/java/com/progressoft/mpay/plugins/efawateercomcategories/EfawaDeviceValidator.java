package com.progressoft.mpay.plugins.efawateercomcategories;

import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.plugins.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

/**
 * Created by user on 29/10/18.
 */
public class EfawaDeviceValidator {
    private static final Logger logger = LoggerFactory.getLogger(EfawaDeviceValidator.class);

    private EfawaDeviceValidator() {
    }

    public static ValidationResult validate(MPAY_CustomerMobile mobile, String deviceId) {
        logger.debug("Inside validate ...");
        MPAY_CustomerDevice device = getApprovedCustomerDevice(mobile.getRefCustomerMobileCustomerDevices(), deviceId);
        return device == null || device.getDeletedFlag() != null && device.getDeletedFlag().booleanValue()?new ValidationResult("45", (String)null, false):(device.getIsBlocked()?new ValidationResult("90", (String)null, false):(device.getIsStolen()?new ValidationResult("51", (String)null, false):new ValidationResult("00", (String)null, true)));
    }

    private static MPAY_CustomerDevice getApprovedCustomerDevice(List<MPAY_CustomerDevice> refCustomerMobileCustomerDevices, String deviceId) {
        Iterator var3 = refCustomerMobileCustomerDevices.iterator();

        MPAY_CustomerDevice device;
        do {
            if(!var3.hasNext()) {
                return null;
            }

            device = (MPAY_CustomerDevice)var3.next();
        } while(!"1100105".equals(device.getStatusId().getCode()) || !deviceId.equals(device.getDeviceID()));

        return device;
    }
}
