package com.progressoft.mpay.plugins.efawateercomcategories;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.plugins.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by user on 29/10/18.
 */
public class EfawateercomGetCategoriesProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(EfawateercomGetCategoriesProcessor.class);

    public EfawateercomGetCategoriesProcessor() {
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null) {
            throw new NullArgumentException("context");
        } else {
            try {
                this.PreProcessMessage(context);
                ValidationResult ex = EfawateercomGetCategoriesValidator.validate(context);
                return ex.isValid() ? this.AcceptMessage(context) : this.RejectMessage(context, ex.getReasonCode(), ex.getReasonDescription());
            } catch (MessageParsingException var3) {
                return this.RejectMessage(context, "37", var3.getField() + " Not Provided");
            } catch (Exception var4) {
                logger.error("Error when ProcessMessage in EfawateercomGetCategoriesProcessor", var4);
                return this.RejectMessage(context, "01", var4.getMessage());
            }
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext integrationProcessingContext) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext arg0) {
        logger.debug("Inside Reverse ...");
        return null;
    }

    private MessageProcessingResult RejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = this.CreateResult(context, reasonCode, reasonDescription, "3");
        String response = this.GenerateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult AcceptMessage(MessageProcessingContext context) {
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = this.CreateResult(context, "00", (String) null, "1");
        String response = this.GenerateResponse(context, true);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private String GenerateResponse(MessageProcessingContext context, boolean isAccepted) {
        logger.debug("Inside GenerateResponse ...");
        logger.debug("isAccepted: " + isAccepted);
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description = null;
        if (context.getLanguage() == null) {
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        }

        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null) {
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        } else {
            description = nls.getDescription();
        }

        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        if (isAccepted) {
            response.getExtraData().add(new ExtraData("serviceTypes", this.ListServiceType(context)));
        }

        return response.toString();
    }

    private String ListServiceType(MessageProcessingContext context) {
        logger.debug("Inside ListServiceType ...");
        String localeCode = String.valueOf(context.getRequest().getLang());

        String languageCode;
        if (Objects.equals(localeCode, "3"))
            languageCode = LanguageMapper.getInstance().getLanguageCodeByLocaleCode("ar");
        else
            languageCode = LanguageMapper.getInstance().getLanguageCodeByLocaleCode("en");


        JsonArray jsonArray = new JsonArray();
        List serviceTypesCategory = context.getDataProvider().getAllServiceTypesCategory();
        if (serviceTypesCategory.isEmpty()) {
            return "Service Category is empty";
        } else {

            for (Object aServiceTypesCategory : serviceTypesCategory) {
                MPAY_ServiceType serviceType = (MPAY_ServiceType) aServiceTypesCategory;
                this.iterateServiceTypeNls(getLanguageByCode(languageCode), serviceType);
                this.buildJsonObject(jsonArray, serviceType, this.iterateServiceTypeNls(getLanguageByCode(languageCode), serviceType));
            }

            return jsonArray.toString();
        }
    }

    private String iterateServiceTypeNls(String languageCode, MPAY_ServiceType serviceType) {
        String description = serviceType.getDescription();

        for (Object obj : serviceType.getServiceTypesNLS().entrySet()) {
            Map.Entry entry = (Map.Entry) obj;
            if (((MPAY_ServiceTypes_NLS) entry.getValue()).getLanguageCode().equalsIgnoreCase(languageCode))
                description = ((MPAY_ServiceTypes_NLS) entry.getValue()).getDescription();
        }

        return description;
    }

    private void buildJsonObject(JsonArray jsonArray, MPAY_ServiceType serviceType, String description) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("Code", serviceType.getCode());
        jsonObject.addProperty("Desc", description);
        jsonArray.add(jsonObject);
    }

    private void PreProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
        logger.debug("Inside PreProcessMessage ...");
        ProcessingContextSide sender = new ProcessingContextSide();
        context.setSender(sender);
        if (context.getRequest().getSenderType().equals("M")) {
            sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
            if (sender.getMobile() == null) {
                return;
            }

            sender.setCustomer(sender.getMobile().getRefCustomer());
        }

    }

    private MessageProcessingResult CreateResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String getLanguageByCode(String langCode) {
        return langCode.equalsIgnoreCase("3") ? "ar" : "en";

    }

}

