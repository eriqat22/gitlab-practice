package com.progressoft.mpay.plugins.efawateercomcategories;

import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by user on 29/10/18.
 */

public class EfawateercomGetCategoriesValidator {
    private static final Logger logger = LoggerFactory.getLogger(EfawateercomGetCategoriesValidator.class);

    public EfawateercomGetCategoriesValidator() {
    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside validate ...");
        if(context == null) {
            throw new NullArgumentException("context");
        } else {
            ValidationResult result = LanguageValidator.validate(context.getLanguage());
            return !result.isValid()?result:(context.getRequest().getSenderType().equals("M")?validateCustomer(context):new ValidationResult("37", (String)null, false));
        }
    }

    private static ValidationResult validateCustomer(MessageProcessingContext context) {
        logger.debug("Inside validateCustomer ...");
        ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
        if(!result.isValid()) {
            return result;
        } else {
            result = CustomerValidator.validate(context.getSender().getCustomer(), true);
            if(!result.isValid()) {
                return result;
            } else {
                result = EfawaDeviceValidator.validate(context.getSender().getMobile(), context.getRequest().getDeviceId());
                if(!result.isValid()) {
                    return result;
                } else {
                    result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
                    return !result.isValid()?result:result;
                }
            }
        }
    }
}

