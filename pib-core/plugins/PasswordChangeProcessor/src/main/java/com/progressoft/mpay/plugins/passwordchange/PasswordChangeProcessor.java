package com.progressoft.mpay.plugins.passwordchange;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.service.utils.AppContext;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;

public class PasswordChangeProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(PasswordChangeProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = PasswordChangeValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error in ProcessMessage", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
			String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		return createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
		PasswordChangeMessage message = (PasswordChangeMessage) context.getRequest();
		if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
			MPAY_CustomerDevice device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DeviceKey);
			device.setPassword(message.getPassword());
			device.setPasswordLastChanged(SystemHelper.getSystemTimestamp());
			if ("SAIB".equals(AppContext.getCurrentTenant().trim())) {
				device.setIsBlocked(false);
				device.setRetryCount(0l);
			}
			context.getDataProvider().mergeCustomerDevice(device);
		} else {
			MPAY_CorporateDevice device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DeviceKey);
			device.setPassword(message.getPassword());
			device.setPasswordLastChanged(SystemHelper.getSystemTimestamp());
			context.getDataProvider().mergeCorporateDevice(device);
		}
		OTPHanlder.removeOTP(context);
		return result;
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
		logger.debug("Inside PreProcessMessage ...");
		PasswordChangeMessage message = PasswordChangeMessage.parseMessage(context.getRequest());
		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
			sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
			if (sender.getMobile() == null)
				return;
			sender.setCustomer(sender.getMobile().getRefCustomer());
			sender.setMobileAccount(
					SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
			if (sender.getMobileAccount() == null)
				return;
			sender.setProfile(sender.getMobileAccount().getRefProfile());
			MPAY_CustomerDevice device = context.getDataProvider()
					.getCustomerDevice(context.getRequest().getDeviceId());
			if (device != null && device.getRefCustomerMobile().getId() == sender.getMobile().getId())
				context.getExtraData().put(Constants.DeviceKey, device);
		} else {
			sender.setService(
					context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
			if (sender.getService() == null)
				return;
			sender.setCorporate(sender.getService().getRefCorporate());
			sender.setServiceAccount(
					SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
			if (sender.getServiceAccount() == null)
				return;
			sender.setProfile(sender.getServiceAccount().getRefProfile());
			MPAY_CorporateDevice device = context.getDataProvider()
					.getCorporateDevice(context.getRequest().getDeviceId());
			if (device != null && device.getRefCorporateService().getId() == sender.getService().getId())
				context.getExtraData().put(Constants.DeviceKey, device);
		}
		OTPHanlder.loadOTP(context, message.getPin());
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
			String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		return null;
	}
}
