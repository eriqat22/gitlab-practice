package com.progressoft.mpay.plugins.passwordchange;

import com.progressoft.mpay.entities.WF_Customer;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PinCodeValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;

public class PasswordChangeValidator {
	private static final Logger logger = LoggerFactory.getLogger(PasswordChangeValidator.class);

	private PasswordChangeValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			return validateCustomer(context);
		else if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			return validateCorporate(context);
		else
			return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
	}

	private static ValidationResult validateCustomer(MessageProcessingContext context) {
		logger.debug("Inside ValidateCustomer ...");

		ValidationResult result = PinCodeValidator.validate(context, context.getSender().getMobile(), context.getRequest().getPin());
		if(!result.isValid())
			return result;

		PasswordChangeMessage message = (PasswordChangeMessage) context.getRequest();
		if (!context.getSender().getCustomer().getIdNum().equals(message.getId()))
			return new ValidationResult(ReasonCodes.INVALID_CLIENT_ID_NUMBER, null, false);

		if(isCustomerOnCreatedStep(context))
			return new ValidationResult(ReasonCodes.VALID, null, true);

		result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

//		MPayHelper.getCustomerDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getMobile().getId())
		result = DeviceValidator.validate((MPAY_CustomerDevice)context.getExtraData().get(Constants.DeviceKey));
		if (!result.isValid())
			return result;

		return LimitsValidator.validate(context);
	}

	private static boolean isCustomerOnCreatedStep(MessageProcessingContext context) {
		return context.getSender().getCustomer().getStatusId().getCode().equals(WF_Customer.STEP_Created);
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context) {
		logger.debug("Inside ValidateCorporate ...");
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		result = PinCodeValidator.validate(context, context.getSender().getService(), context.getRequest().getPin());
		if(!result.isValid())
			return result;

		PasswordChangeMessage message = (PasswordChangeMessage) context.getRequest();
		if (!context.getSender().getCorporate().getRegistrationId().equals(message.getId()))
			return new ValidationResult(ReasonCodes.INVALID_CLIENT_ID_NUMBER, null, false);

		return LimitsValidator.validate(context);
	}
}
