package com.progressoft.mpay.plugins.passwordchange;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class PasswordChangeMessage extends MPayRequest {
	private static final String PASS_WORD_KEY = "pass";
	private static final String ID_KEY = "id";

	private String pass;
	private String id;

	public PasswordChangeMessage() {
		//
	}

	public PasswordChangeMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getPassword() {
		return pass;
	}

	public void setPassword(String pass) {
		this.pass = pass;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public static PasswordChangeMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		PasswordChangeMessage message = new PasswordChangeMessage(request);
		if (StringUtils.isEmpty(message.getSender()))
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (StringUtils.isEmpty(message.getSenderType()) || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setPassword(message.getValue(PASS_WORD_KEY));
		if (StringUtils.isEmpty(message.getPassword()))
			throw new MessageParsingException(PASS_WORD_KEY);
		message.setId(message.getValue(ID_KEY));
		if (StringUtils.isEmpty(message.getId()))
			throw new MessageParsingException(ID_KEY);
		return message;
	}
}
