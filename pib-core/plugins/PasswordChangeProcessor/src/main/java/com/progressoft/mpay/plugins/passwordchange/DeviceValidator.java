package com.progressoft.mpay.plugins.passwordchange;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class DeviceValidator {
	private static final Logger logger = LoggerFactory.getLogger(DeviceValidator.class);

	public static ValidationResult Validate(MessageProcessingContext context, String deviceId) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");
		if (deviceId == null)
			throw new NullArgumentException("deviceId");

		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)) {
			MPAY_CustomerDevice device = null;
			if (context.getExtraData().containsKey(Constants.DeviceKey))
				device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DeviceKey);

			if (device == null || SystemHelper.isDeleted(device))
				return new ValidationResult(ReasonCodes.DEVICE_NOT_FOUND, null, false);
			if (device.getIsStolen())
				return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);
		} else {
			MPAY_CorporateDevice device = null;
			if (context.getExtraData().containsKey(Constants.DeviceKey))
				device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DeviceKey);

			if (device == null || SystemHelper.isDeleted(device))
				return new ValidationResult(ReasonCodes.DEVICE_NOT_FOUND, null, false);
			if (device.getIsStolen())
				return new ValidationResult(ReasonCodes.DEVICE_IS_STOLEN, null, false);
		}
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
