package com.progressoft.mpay.plugins.meps;

import org.apache.camel.component.netty4.NettyConsumer;
import org.apache.camel.component.netty4.ServerInitializerFactory;

import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;

public class MepsChangeHandlerFactory extends ServerInitializerFactory {

	@Override
	public ServerInitializerFactory createPipelineFactory(NettyConsumer consumer) {
		return new MepsChangeHandlerFactory();
	}

	@Override
	protected void initChannel(Channel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
		pipeline.addLast("decoder-SD",new MepsMessageDecoder());
		pipeline.addLast("encoder-SD",new MepsMessageEncoder());
	}
}
