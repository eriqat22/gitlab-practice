package com.progressoft.mpay.plugins.meps;

import com.progressoft.jfw.integration.comm.Communicator;
import com.progressoft.mpay.ICryptographer;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.IServiceUserManager;
import com.progressoft.mpay.ISystemParameters;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.notifications.INotificationHelper;
import com.solab.iso8583.IsoMessage;

public class MEPSProcessingContext {
	private String messageHeader;
	private IsoMessage message;
	private IDataProvider dataProvider;
	private ISystemParameters systemParameters;
	private ILookupsLoader lookupsLoader;
	private ICryptographer cryptographer;
	private INotificationHelper notificationHelper;
	private IServiceUserManager serviceUserManager;
	private Communicator communicator;
	private String tenantId;

	public MEPSProcessingContext(IDataProvider dataProvider, ISystemParameters systemParameters, ILookupsLoader lookupsLoader, ICryptographer cryptographer, INotificationHelper notificationHelper, IServiceUserManager serviceUserManager, Communicator communicator) {
		if(dataProvider == null)
			throw new NullArgumentException("dataProvider");
		if(systemParameters == null)
			throw new NullArgumentException("systemParameters");
		if(lookupsLoader == null)
			throw new NullArgumentException("lookupsLoader");
		if(cryptographer == null)
			throw new NullArgumentException("cryptographer");
		if(notificationHelper == null)
			throw new NullArgumentException("notificationHelper");
		if(serviceUserManager == null)
			throw new NullArgumentException("serviceUserManager");
		if(communicator == null)
			throw new NullArgumentException("communicator");

		this.dataProvider = dataProvider;
		this.systemParameters = systemParameters;
		this.lookupsLoader = lookupsLoader;
		this.cryptographer = cryptographer;
		this.notificationHelper = notificationHelper;
		this.serviceUserManager = serviceUserManager;
		this.communicator = communicator;
	}

	public IsoMessage getMessage() {
		return message;
	}

	public void setMessage(IsoMessage message) {
		this.message = message;
	}

	public IDataProvider getDataProvider() {
		return dataProvider;
	}

	public void setDataProvider(IDataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public ISystemParameters getSystemParameters() {
		return systemParameters;
	}

	public void setSystemParameters(ISystemParameters systemParameters) {
		this.systemParameters = systemParameters;
	}

	public ILookupsLoader getLookupsLoader() {
		return lookupsLoader;
	}

	public void setLookupsLoader(ILookupsLoader lookupsLoader) {
		this.lookupsLoader = lookupsLoader;
	}

	public ICryptographer getCryptographer() {
		return cryptographer;
	}

	public void setCryptographer(ICryptographer cryptographer) {
		this.cryptographer = cryptographer;
	}

	public INotificationHelper getNotificationHelper() {
		return notificationHelper;
	}

	public void setNotificationHelper(INotificationHelper notificationHelper) {
		this.notificationHelper = notificationHelper;
	}

	public Communicator getCommunicator() {
		return communicator;
	}

	public void setCommunicator(Communicator communicator) {
		this.communicator = communicator;
	}

	public String getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(String messageHeader) {
		this.messageHeader = messageHeader;
	}

	public IServiceUserManager getServiceUserManager() {
		return serviceUserManager;
	}

	public void setServiceUserManager(IServiceUserManager serviceUserManager) {
		this.serviceUserManager = serviceUserManager;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

}
