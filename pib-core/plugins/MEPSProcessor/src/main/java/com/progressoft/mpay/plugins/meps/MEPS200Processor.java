package com.progressoft.mpay.plugins.meps;

import java.math.BigDecimal;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.entities.MPAY_BulkPayment;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.mepspayment.MEPSPayMessage;
import com.progressoft.mpay.plugins.utils.MepsCommon;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class MEPS200Processor extends MEPSProcessor {
	private static final String FINANCIALREQUESTTYPE = "1200";
	private static final Logger LOGGER = LoggerFactory.getLogger(MEPS200Processor.class);

	@Override
	public MEPSProcessingResult processMessage(MEPSProcessingContext context) {
		LOGGER.debug("Inside MEPS200Processor ...");
		if (context == null)
			throw new NullArgumentException("context");
		MessageProcessingContext mpayContext = null;
		try {
			MPayRequest mpayRequest = prepareMPayRequest(context);
			mpayContext = prepareMessageProcessingContext(mpayRequest, context);
			MPAY_BulkPayment bulkPayment = context.getDataProvider().getBulkPayment(context.getSystemParameters().getBulkPaymentDefaultId());
			if (bulkPayment == null)
				throw new InvalidOperationException("Default Bulk Payment ID Not Found");
			MPAY_MPayMessage message = MessageProcessorHelper.createMessage(context.getServiceUserManager(), mpayContext, ProcessingStatusCodes.PENDING, null, null, bulkPayment);
			mpayContext.setMessage(message);
			context.getDataProvider().persistMPayMessage(message);
			MessageProcessor processor = MessageProcessorHelper.getProcessor(mpayContext.getOperation());
			MessageProcessingResult result = processor.processMessage(mpayContext);
			context.getDataProvider().mergeMEPSProcessingResult(result);
			sendNotifications(context.getNotificationHelper(), result);
			IsoMessage isoMessage = generateResponse(context, result.getReasonCode(), result);
			return generateResult(isoMessage, context);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Duplicate message received");
			LOGGER.debug("Duplicate message received", e);
			return generateResult(generateResponse(context, ReasonCodes.MESSAGE_IS_DUPLICATED, null), context);
		} catch (Exception ex) {
			LOGGER.error("Error while processMessage ...", ex);
			return rejectMessage(mpayContext, context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MPayRequest prepareMPayRequest(MEPSProcessingContext context) {
		MPayRequest request = new MPayRequest(context.getSystemParameters().getMEPSPaymentEndpoint(), context.getSystemParameters().getMEPSServiceName(), ReceiverInfoType.CORPORATE, null,
				LanguageMapper.getInstance().getLanguageLongByLocaleCode(context.getSystemParameters().getSystemLanguage().getCode()), createMessageId(context), context.getTenantId());
		request.getExtraData().add(new ExtraData(MEPSPayMessage.AMOUNT_KEY,
				String.valueOf(BigDecimal.valueOf(Double.parseDouble((String) context.getMessage().getField(MEPS200ProcessorConstants.TRANSACTION_AMOUNT).getValue()) / 1000))));
		request.getExtraData().add(new ExtraData(MEPSPayMessage.RECEIVER_INFO_KEY, getMobileNumber(context.getMessage(), MEPS200ProcessorConstants.ACCOUNT_ID_CODE_1)));
		request.getExtraData().add(new ExtraData(MEPSPayMessage.RECEIVER_TYPE_KEY, ReceiverInfoType.MOBILE));
		request.getExtraData().add(new ExtraData(MEPSPayMessage.CARD_NUMBER_KEY,MepsCommon.showLastThreeDigitInCardNumber((String) context.getMessage().getField(MEPS200ProcessorConstants.PAN).getValue())));
		request.getExtraData().add(new ExtraData(MEPSPayMessage.TERMINAL_LOCATION_KEY, (String) context.getMessage().getField(MEPS200ProcessorConstants.TERMINAL_LOCATION).getValue()));
		request.getExtraData().add(new ExtraData(MEPSPayMessage.RETRIEVAL_REFERENCE_NUMBER_KEY , (String) context.getMessage().getField(MEPS200ProcessorConstants.RETRIEVAL_REFERENCE_NUMBER).getValue()));
		return request;
	}

	private String createMessageId(MEPSProcessingContext context) {
		StringBuilder messageId = new StringBuilder();
		messageId.append(FINANCIALREQUESTTYPE);
		messageId.append(context.getMessage().getField(MEPS200ProcessorConstants.SYSTEM_TRACE_AUDIT_NUMBER).toString());
		messageId.append(context.getMessage().getField(MEPS200ProcessorConstants.TRANSMISSION_DATE_TIME).toString());
		messageId.append(context.getMessage().getField(MEPS200ProcessorConstants.RETRIEVAL_REFERENCE_NUMBER).toString());
		return messageId.toString();
	}

	@Override
	protected IsoMessage generateResponse(MEPSProcessingContext context, String reasonCode, MessageProcessingResult result) {
		IsoMessage isoMessage = MEPSISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE);
		isoMessage.setBinary(false);
		Random randomNumber = new Random();
		String authorizationNumber;
		if (result == null || result.getTransaction() == null)
			authorizationNumber = String.valueOf(randomNumber.nextInt(999999));
		else
			authorizationNumber = result.getTransaction().getExternalReference();

		for (int i = 2; i <= 128; i++) {
			if (context.getMessage().getField(i) != null)
				isoMessage.setField(i, context.getMessage().getField(i));
		}
		isoMessage.setField(MEPS200ProcessorConstants.AUTHORIZATION_NUMBER, new IsoValue<>(IsoType.ALPHA, authorizationNumber, 6));
		isoMessage.setField(MEPS200ProcessorConstants.RESPONSE_CODE, new IsoValue<>(IsoType.NUMERIC,
				MEPSReasonsMapper.getMappedReason(reasonCode, context.getSystemParameters().getSystemLanguage().getId(), context.getDataProvider(), context.getLookupsLoader()).getCode(), 3));
		return isoMessage;
	}
}