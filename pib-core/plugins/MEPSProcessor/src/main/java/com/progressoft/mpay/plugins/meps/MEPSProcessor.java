package com.progressoft.mpay.plugins.meps;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.notifications.INotificationHelper;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.solab.iso8583.IsoMessage;

public abstract class MEPSProcessor {
	public abstract MEPSProcessingResult processMessage(MEPSProcessingContext context);
	protected abstract IsoMessage generateResponse(MEPSProcessingContext context, String reasonCode, MessageProcessingResult result);

	protected MessageProcessingContext prepareMessageProcessingContext(MPayRequest mpayRequest,
			MEPSProcessingContext context) {
		MessageProcessingContext mpayContext = new MessageProcessingContext(context.getDataProvider(),
				context.getLookupsLoader(), context.getSystemParameters(), context.getCryptographer(),
				context.getNotificationHelper());
		mpayContext.setRequest(mpayRequest);
		mpayContext.setOriginalRequest(mpayRequest.toString());
		mpayContext.setOperation(context.getLookupsLoader().getEndPointOperation(mpayRequest.getOperation()));
		mpayContext.setLanguage(context.getSystemParameters().getSystemLanguage());
		return mpayContext;
	}

	protected MEPSProcessingResult rejectMessage(MessageProcessingContext context, MEPSProcessingContext mepsContext,
			String reasonCode, String reasonDescription) {
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
		updateMessageData(context, reasonDescription, reason, status);
		return generateResult(generateResponse(mepsContext, reasonCode, null), mepsContext);
	}

	private void updateMessageData(MessageProcessingContext context, String reasonDescription, MPAY_Reason reason,
			MPAY_ProcessingStatus status) {
		if (context.getMessage() == null)
			return;
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		context.getDataProvider().mergeMEPSMPayMessage(context.getMessage());
	}

	protected MEPSProcessingResult generateResult(IsoMessage response, MEPSProcessingContext context) {
		try {
			MEPSProcessingResult mepsResult = new MEPSProcessingResult();
			byte[] decodeHex = Hex.decodeHex(context.getMessageHeader().toCharArray());
			String headerDecoded = new String(decodeHex);
			response.setIsoHeader(headerDecoded);
			mepsResult.setIsoMessage(response);
			mepsResult.setResponse(new String(response.writeData()));
			return mepsResult;
		} catch (DecoderException e) {
			throw new MPayGenericException("Failed to generate response", e);
		}
	}

	protected String getMobileNumber(IsoMessage isoMessage, int fieldNumber) {
		String mobileNumber = String.valueOf(isoMessage.getField(fieldNumber).getValue());
		if(!mobileNumber.startsWith("00"))
			mobileNumber = "00" + mobileNumber;
		return mobileNumber;
	}

	protected void sendNotifications(INotificationHelper notificationHelper, MessageProcessingResult result) {
		if(result.getNotifications() == null || result.getNotifications().isEmpty())
			return;

		for(MPAY_Notification notification : result.getNotifications())
			notificationHelper.sendNotifications(notification);
	}
}
