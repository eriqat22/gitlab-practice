package com.progressoft.mpay.plugins.meps;

public class MEPSProcessorConstants {
	public static final int FINANCIAL_REQUEST = 0x1200;
	public static final int FINANCIAL_RESPONSE = 0x1210;
	public static final int REVERSAL_REQUEST = 0x1420;
	public static final int REVERSAL_RESPONSE = 0x1430;
	public static final int NETWORK_REQUEST = 0x1804;
	public static final int NETWORK_RESPONSE = 0x1814;
	private MEPSProcessorConstants(){}
}
