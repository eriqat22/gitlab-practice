package com.progressoft.mpay.plugins.mepspaymentreversal;

import java.math.BigDecimal;
import java.sql.SQLException;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.AmountType;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.JVPostingResult;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclear.ProcessingCodeRanges;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.plugins.mepspaymentreversal.integrations.MEPSPayReversalOfflineResponseProcessor;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class MEPSPayReversalProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MEPSPayReversalProcessor.class);

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage...");
		if (context == null)
			throw new NullArgumentException("context");
		try {
			preProcessMessage(context);
			ValidationResult validationResult = MEPSPayReversalValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(), validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Invalid Message Received", e);
			return rejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage", ex);
			if (context.getTransaction() != null)
				TransactionHelper.reverseTransaction(context);
			return rejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws SQLException, WorkflowException {
		logger.debug("Inside AcceptMessage ...");
		MEPSPayReversalMessage message = (MEPSPayReversalMessage) context.getRequest();
		MessageProcessingResult result = new MessageProcessingResult();
		MPAY_ProcessingStatus status;
		MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
		String reasonDescription = null;

		context.getMessage().setReason(reason);
		MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO, message.getNotes());
		result.setMessage(context.getMessage());
		context.setTransaction(transaction);
		result.setTransaction(transaction);
		context.getDataProvider().persistMEPSProcessingResult(result);
		JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), transaction);
		if (postingResult.isSuccess()) {
			BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
			if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
				status = handleMPClear(context, message, result);
			} else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			} else {
				status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
				reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
				reasonDescription = integResult.getReasonDescription();
				TransactionHelper.reverseTransaction(context);
			}
		} else {
			if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
				reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
				context.getMessage().setReasonDesc(postingResult.getReason());
			} else
				reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
			status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
		}
		context.getMessage().setProcessingStatus(status);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
		transaction.setReason(context.getMessage().getReason());
		transaction.setReasonDesc(context.getMessage().getReasonDesc());
		result.setReasonCode(context.getMessage().getReason().getCode());
		result.setStatus(status);
		result.setStatusDescription(context.getMessage().getReasonDesc());
		handleLimits(context, status, transaction);
		handleNotifications(context, result, status);
		OTPHanlder.removeOTP(context);
		handleOriginalTransaction(context);
		return result;
	}

	private void handleOriginalTransaction(MessageProcessingContext context) {
		MPAY_Transaction originalTransaction = (MPAY_Transaction)context.getExtraData().get(Constants.ORIGINAL_TRANSACTION_KEY);
//		originalTransaction.setIsReversed(true);
//		originalTransaction.setReversedBy(context.getMessage().getMessageID());
		context.getDataProvider().reverseTransaction(originalTransaction.getId(), context.getMessage().getMessageID());
	}

	private MPAY_ProcessingStatus handleMPClear(MessageProcessingContext context, MEPSPayReversalMessage message, MessageProcessingResult result) throws WorkflowException {
		MPAY_ProcessingStatus status;
		Integer mpClearIntegType;
		mpClearIntegType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
		status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
		result.setHandleMPClearOffline(true);
		IsoMessage mpClearIsoMessage = createMPClearRequest(context, mpClearIntegType, message.getReceiverType().equals(ReceiverInfoType.ALIAS));
		MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage, Integer.toHexString(mpClearIntegType));
		messageLog.setRefMessage(context.getMessage());
		result.setMpClearMessage(messageLog);
		return status;
	}

	private void handleNotifications(MessageProcessingContext context, MessageProcessingResult result, MPAY_ProcessingStatus status) {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED)) {
			result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		} else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
			result.setNotifications(NotificationProcessor.createRejectionNotificationMessages(context));
	}

	private void handleLimits(MessageProcessingContext context, MPAY_ProcessingStatus status, MPAY_Transaction transaction) throws SQLException {
		if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED) || status.getCode().equals(ProcessingStatusCodes.PARTIALLY_ACCEPTED)) {
			context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(), 1,
					SystemHelper.getCurrentDateWithoutTime());
		}
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException, SQLException {
		logger.debug("Inside PreProcessMessage ...");
		MEPSPayReversalMessage message = MEPSPayReversalMessage.parseMessage(context.getRequest());

		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		MPAY_MPayMessage originalMessage = context.getDataProvider().getMPayMessageByMessageId(message.getOriginalMessageId());

		if (originalMessage != null) {
			context.getExtraData().put(Constants.ORIGINAL_MESSAGE_KEY, originalMessage);
			context.getExtraData().put(Constants.ORIGINAL_TRANSACTION_KEY, context.getDataProvider().getTransactionByMessageId(originalMessage.getId()));
		}
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setAmount(message.getAmount());
		sender.setInfo(message.getSender());
		context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
		sender.setService(context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
		if (sender.getService() == null)
			return;
		sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), message.getSenderAccount()));
		if (sender.getServiceAccount() == null)
			return;
		sender.setAccount(sender.getServiceAccount().getRefAccount());
		sender.setCorporate(sender.getService().getRefCorporate());
		sender.setBank(sender.getServiceAccount().getBank());
		sender.setProfile(sender.getServiceAccount().getRefProfile());
		sender.setBanked(sender.getAccount().getIsBanked());
		sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(), context.getOperation().getMessageType().getId()));
		receiver.setInfo(message.getReceiverInfo());
		receiver.setMobile(context.getDataProvider().getCustomerMobile(message.getReceiverInfo(), message.getReceiverType()));
		long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
		OTPHanlder.loadOTP(context, message.getPin());
		receiver.setNotes(message.getNotes());
		if (receiver.getMobile() != null) {
			receiver.setCustomer(receiver.getMobile().getRefCustomer());
			receiver.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), receiver.getMobile(), message.getReceiverAccount()));
			if (receiver.getMobileAccount() == null)
				return;
			receiver.setAccount(receiver.getMobileAccount().getRefAccount());
			receiver.setBank(receiver.getMobileAccount().getBank());
			receiver.setProfile(receiver.getMobile().getRefProfile());
			receiver.setBanked(receiver.getAccount().getIsBanked());
			context.setDirection(TransactionDirection.ONUS);
			context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(), sender.isBanked(), personClientType, receiver.isBanked(),
					context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
		} else {
			context.getReceiver().setService(context.getLookupsLoader().getPSP().getPspMPClearService());
			context.getReceiver().setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), context.getReceiver().getService(), null));
			if (context.getReceiver().getServiceAccount() != null)
				context.getReceiver().setBank(context.getReceiver().getServiceAccount().getBank());
			context.setDirection(TransactionDirection.OUTWARD);
			context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(), sender.isBanked(),
					context.getLookupsLoader().getMPClear().getClientType(), false, context.getOperation().getMessageType().getId(), context.getTransactionNature(), context.getOperation().getId()));
		}
		if (context.getTransactionConfig() != null)
			context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());
		ChargesCalculator.calculate(context);
		TaxCalculator.claculate(context);
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String processingStatusCode, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		if (context.getTransaction() != null) {
			BankIntegrationResult result = BankIntegrationHandler.reverse(context);
			if (result != null)
				TransactionHelper.reverseTransaction(context);
		}
		return MessageProcessingResult.create(context, status, reason, reasonDescription);
	}

	private IsoMessage createMPClearRequest(MessageProcessingContext context, Integer type, boolean isReceiverAlias) throws WorkflowException {
		logger.debug("Inside CreateMPClearRequest ...");
		MEPSPayReversalMessage request = (MEPSPayReversalMessage) context.getRequest();
		String messageID = context.getDataProvider().getNextMPClearMessageId();
		BigDecimal charge = context.getTransaction().getTotalAmount().subtract(context.getAmount());
		IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(type);
		message.setBinary(false);

		String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), context.getTransaction().getTotalAmount().subtract(charge), AmountType.AMOUNT);
		String chargesAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), charge, AmountType.CHARGE);

		message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
		MPClearHelper.setMessageEncoded(message);
		message.setField(7, new IsoValue<String>(IsoType.DATE10, MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
		message.setField(46, new IsoValue<String>(IsoType.LLLVAR, chargesAmount, chargesAmount.length()));
		message.setField(49, new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));

		String account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getService(), context.getSender().getServiceAccount(), false);
		MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

		String account2;
		if (context.getReceiver().getMobile() == null)
			account2 = MPClearHelper.getAccount(context.getSystemParameters(), request.getReceiverInfo(), request.getReceiverType());
		else
			account2 = MPClearHelper.getAccount(context.getSystemParameters(), context.getReceiver().getMobile(), context.getReceiver().getMobileAccount(), isReceiverAlias);

		MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

		String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
		message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
		message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

		return message;
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		IsoMessage mpClearIsoMessage = context.getMpClearIsoMessage();
		try {
			return new MEPSPayReversalOfflineResponseProcessor().processIntegration(context);
		} catch (Exception e) {
			logger.error("Error when ProcessIntegration", e);
			IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null,
					context.isRequest());
			if (context.isRequest())
				result.setMpClearOutMessage(MPClearHelper.createMPClearResponse(context, mpClearIsoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(),
						MPClearHelper.getResponseType(mpClearIsoMessage.getType()), result.getReason().getCode(), result.getReasonDescription(),
						String.valueOf(ProcessingCodeRanges.CREDIT_START_VALUE)));
			return result;
		}
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		logger.debug("Inside Reverse ...");
		try {
			JVPostingResult result = TransactionHelper.reverseTransaction(context);
			context.getDataProvider().mergeTransaction(context.getTransaction());
			if (result.isSuccess())
				return createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

			return createResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
		} catch (Exception ex) {
			logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
			return createResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(), ProcessingStatusCodes.FAILED);
		}
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		if (context.getMessage() != null) {
			context.getMessage().setReason(reason);
			context.getMessage().setReasonDesc(reasonDescription);
			context.getMessage().setProcessingStatus(status);
		}
		if (context.getTransaction() != null) {
			context.getTransaction().setReason(reason);
			context.getTransaction().setReasonDesc(reasonDescription);
			context.getTransaction().setProcessingStatus(status);
		}
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setTransaction(context.getTransaction());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		if (reasonCode.equals(ReasonCodes.VALID)) {
			result.setNotifications(NotificationProcessor.createReversalNotificationMessages(context));
		}
		return result;
	}
}
