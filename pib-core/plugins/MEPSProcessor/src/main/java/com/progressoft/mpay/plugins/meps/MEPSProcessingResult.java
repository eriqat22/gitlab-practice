package com.progressoft.mpay.plugins.meps;

import com.solab.iso8583.IsoMessage;

public class MEPSProcessingResult {
	private String response;
	private IsoMessage isoMessage;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public IsoMessage getIsoMessage() {
		return isoMessage;
	}

	public void setIsoMessage(IsoMessage isoMessage) {
		this.isoMessage = isoMessage;
	}
}
