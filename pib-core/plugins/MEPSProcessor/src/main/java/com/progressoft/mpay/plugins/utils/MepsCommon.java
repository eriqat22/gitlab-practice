package com.progressoft.mpay.plugins.utils;

public class MepsCommon {

	public static String showLastThreeDigitInCardNumber(String cardNumber) {
		String firstTwoDigit = cardNumber.substring(0, 2);
		String replaceFirst = cardNumber.substring(2, cardNumber.length()-4).replaceAll("\\d{1}","*");
		replaceFirst +=cardNumber.substring(cardNumber.length()-4, cardNumber.length());
		return firstTwoDigit + replaceFirst;
	}
}