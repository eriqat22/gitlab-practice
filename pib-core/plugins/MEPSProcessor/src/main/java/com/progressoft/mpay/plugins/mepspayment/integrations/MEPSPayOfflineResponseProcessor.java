package com.progressoft.mpay.plugins.mepspayment.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class MEPSPayOfflineResponseProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MEPSPayOfflineResponseProcessor.class);

	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return accept(context);
	}

	private IntegrationProcessingResult accept(IntegrationProcessingContext context) {
		logger.debug("Inside Accept ...");
		return IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
	}
}
