package com.progressoft.mpay.plugins.meps;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayContext;
import com.solab.iso8583.IsoMessage;

public class MEPSMessageHandler implements Processor {

	private static final Logger LOGGER = LoggerFactory.getLogger(MEPSMessageHandler.class);
	public static final String SERVICE_USER = "SERVICE_USER";

	@Override
	public void process(Exchange exchange) throws Exception {
		boolean serviceUserEnabled = false;
		try {
			String request = (String) exchange.getIn().getBody();
			LOGGER.info("Received message : " + request);
			String tenantId = (String) exchange.getProperty("tenantId");
			serviceUserEnabled = MPayContext.getInstance().getServiceUser().enableServiceUser(tenantId);
			IsoMessage message = S2MIsoMessageParser.parse(request, MPayContext.getInstance().getSystemParameters());
			if (message == null) {
				LOGGER.info("Failed to parse message");
				return;
			}
			MEPSProcessingContext context = prepareContext(message, request.replace(":", "").replace(" ", ""));
			context.setTenantId(tenantId);
			MEPSProcessor mepsProcessor = MEPSProcessorFactory.createProcessor(message.getType());
			MEPSProcessingResult result = mepsProcessor.processMessage(context);
			Message exchangeMessage = new DefaultMessage();
			exchangeMessage.setBody(new SimpleMepsMessageEncoder().generateResponse(result));
			exchange.setOut(exchangeMessage);
		} catch (Exception ex) {
			LOGGER.error("Error while ProcessMEPSMessageHandler message", ex);
		} finally {
			if (serviceUserEnabled) {
				MPayContext.getInstance().getServiceUser().disableServiceUser();
			}
		}
	}

	private MEPSProcessingContext prepareContext(IsoMessage message, String request) {
		MEPSProcessingContext context = new MEPSProcessingContext(MPayContext.getInstance().getDataProvider(), MPayContext.getInstance().getSystemParameters(),
				MPayContext.getInstance().getLookupsLoader(), MPayContext.getInstance().getCryptographer(), MPayContext.getInstance().getNotificationHelper(),
				MPayContext.getInstance().getServiceUser(), MPayContext.getInstance().getCommunicator());
		context.setMessageHeader(request.substring(8, 44));
		context.setCommunicator(MPayContext.getInstance().getCommunicator());
		context.setCryptographer(MPayContext.getInstance().getCryptographer());
		context.setDataProvider(MPayContext.getInstance().getDataProvider());
		context.setLookupsLoader(MPayContext.getInstance().getLookupsLoader());
		context.setMessage(message);
		context.setNotificationHelper(MPayContext.getInstance().getNotificationHelper());
		context.setSystemParameters(MPayContext.getInstance().getSystemParameters());
		return context;
	}
}