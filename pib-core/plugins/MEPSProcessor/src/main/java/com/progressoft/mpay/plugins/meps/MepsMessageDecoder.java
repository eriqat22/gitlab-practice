package com.progressoft.mpay.plugins.meps;

import java.util.List;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

@Sharable
public class MepsMessageDecoder extends MessageToMessageDecoder<ByteBuf> {

	private static final Logger LOGGER= LoggerFactory.getLogger(MepsMessageDecoder.class);

	@Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
		byte[] array = new byte[msg.readableBytes()];
        msg.getBytes(0, array);
        String hexString = Hex.encodeHexString(array);
        LOGGER.info("The message un decoder is :- " + hexString);
        out.add(hexString);
    }
}