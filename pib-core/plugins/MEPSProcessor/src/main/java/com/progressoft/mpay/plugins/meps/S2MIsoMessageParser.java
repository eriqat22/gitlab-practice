package com.progressoft.mpay.plugins.meps;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.ISystemParameters;
import com.solab.iso8583.IsoMessage;

public class S2MIsoMessageParser {
	private static final Logger LOGGER = LoggerFactory.getLogger(MEPSMessageHandler.class);

	private S2MIsoMessageParser() {
	}

	public static IsoMessage parse(String message, ISystemParameters systemParameters) {
		LOGGER.debug("Inside S2MIsoMessageParser....");
		LOGGER.info("received message: " + message);
		try {
			if (message == null || message.isEmpty())
				return null;
			String messageToParse = message.replace(":", "").replace(" ", "");
			String messageBody;
			messageBody = messageToParse.substring(44);
			String messageType = convertAsciiToString(messageBody.substring(0, 8));
			String bitmapString = extractBitmap(messageBody.substring(8));
			String messageElements;
			if (bitmapString.length() == 32) {
				messageElements = messageBody.substring(40);
			} else {
				messageElements = messageBody.substring(24);
			}
			String originalMessage = messageType + bitmapString + convertAsciiToString(messageElements);
			return MEPSISO8583Parser.getInstance(systemParameters.getMEPSISO8583ConfigFilePath())
					.parseMessage(originalMessage);
		} catch (Exception e) {
			LOGGER.info("Error while parsing message", e);
			return null;
		}
	}

	private static String extractBitmap(String messageBody) {
		String bitmap;
		String hexBitmap;
		String bitmap1String = messageBody.substring(0, 16);
		StringBuilder bitmapBuilder = new StringBuilder();
		char[] bitmapChars = bitmap1String.toCharArray();
		for (char chr : bitmapChars)
			bitmapBuilder.append(hexToBinary(Character.toString(chr)));

		bitmap = bitmapBuilder.toString();
		hexBitmap = binaryToHex(bitmap);
		if (bitmap.startsWith("1")) {
			hexBitmap += extractBitmap2(messageBody.substring(16, 32));
		}
		return hexBitmap;
	}

	private static String extractBitmap2(String bitmapString) {
		StringBuilder bitmapBuilder = new StringBuilder();
		char[] bitmapChars = bitmapString.toCharArray();

		for (char chr : bitmapChars)
			bitmapBuilder.append(hexToBinary(Character.toString(chr)));
		return binaryToHex(bitmapBuilder.toString());
	}

	private static String hexToBinary(String hexString) {
		String binaryval;
		binaryval = String.valueOf(Integer.toString(Integer.parseInt(hexString, 16), 2));
		if (binaryval.length() != hexString.length() * 4) {
			int bitsdifference = hexString.length() * 4 - binaryval.length();
			for (int i = 0; i < bitsdifference; i++) {
				binaryval = "0" + binaryval;
			}
		}
		return binaryval;
	}

	public static String convertAsciiToString(String hex) {
		byte[] raw = new byte[hex.length() / 2];
		for (int i = 0; i < raw.length; i++) {
			int index = i * 2;
			raw[i] = (byte) Integer.parseInt(hex.substring(index, index + 2), 16);
		}
		return new String(raw, StandardCharsets.UTF_8);
	}

	public static String binaryToHex(String binary) {
		String valueToConvert = binary;
		StringBuilder result = new StringBuilder(valueToConvert.length() / 8 + 1);
		int mod4Len = valueToConvert.length() % 8;
		if (mod4Len != 0) {
			valueToConvert = StringUtils.leftPad(binary, 8, "0");
		}
		for (int i = 0; i < valueToConvert.length(); i += 8) {
			String eightBits = valueToConvert.substring(i, i + 8);
			result.append(String.format("%02x", (byte) Integer.parseInt(eightBits, 2)));
		}
		return result.toString();
	}

	public static String stringtoHex(byte[] buf) {
		StringBuilder strBuilder = new StringBuilder();
		for (int i = 0; i < buf.length; i++) {
			strBuilder.append(String.format("%x", buf[i]));
		}
		return strBuilder.toString();
	}
}
