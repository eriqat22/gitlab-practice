package com.progressoft.mpay.plugins.meps;

import java.io.IOException;
import java.net.URL;

import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.parse.ConfigParser;

public final class MEPSISO8583Parser {

	private static MEPSISO8583Parser instance;
	private MessageFactory<IsoMessage> mf = null;

	private MEPSISO8583Parser(String filePath) throws IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL url = classLoader.getResource(filePath);
		if (url == null)
			throw new IOException(filePath + "is null");
		mf = ConfigParser.createFromUrl(url);
	}

	public static MEPSISO8583Parser getInstance(String filePath) {
		if (instance == null)
			try {
				instance = new MEPSISO8583Parser(filePath);
			} catch (IOException e) {
				throw new MPayGenericException("Failed to initialize MEPSISO8583Parser", e);
			}

		return instance;
	}

	public IsoMessage parseMessage(String contents) {
		try {
			return mf.parseMessage(contents.getBytes(), 0);
		} catch (Exception e1) {
			throw new MessageParsingException(contents, e1);
		}
	}

	public IsoMessage newMessage(int type) {
		mf.setAssignDate(false);
		mf.setUseBinaryMessages(false);
		return mf.newMessage(type);
	}

	public static void flush() {
		instance = null;
	}
}
