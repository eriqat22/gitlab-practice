package com.progressoft.mpay.plugins.meps;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;

import org.hamcrest.Matchers;

import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_ServiceIntegReason;
import com.progressoft.mpay.entities.MPAY_ServiceIntegReasons_NLS;

public class MEPSReasonsMapper {

	private MEPSReasonsMapper() {

	}

	public static MappedReason getMappedReason(String reasonCode, long languageId, IDataProvider provider,
			ILookupsLoader lookupsLoader) {
		MPAY_ServiceIntegReason reason = provider.getSeviceIntegReasonByMappingCode(reasonCode,
				MEPSConstants.MEPS_PROCESSOR_CODE);
		if (reason == null)
			return getMPayReason(reasonCode, languageId, lookupsLoader);
		return getTranslatedReason(reason, languageId);
	}

	private static MappedReason getTranslatedReason(MPAY_ServiceIntegReason reason, long languageId) {
		MPAY_Reasons_NLS nls = selectFirst(reason.getServiceIntegReasonsNLS(),
				having(on(MPAY_ServiceIntegReasons_NLS.class).getLanguageCode(), Matchers.equalTo(languageId)));
		if (nls == null)
			return new MappedReason(reason.getCode(), reason.getDescription());
		return new MappedReason(reason.getCode(), nls.getDescription());
	}

	private static MappedReason getMPayReason(String reasonCode, long languageId, ILookupsLoader lookupsLoader) {
		MPAY_Reason reason = lookupsLoader.getReason(reasonCode);
		if (reason == null)
			return new MappedReason(reasonCode, null);
		MPAY_Reasons_NLS nls = selectFirst(reason.getReasonsNLS(),
				having(on(MPAY_Reasons_NLS.class).getLanguageCode(), Matchers.equalTo(languageId)));
		if (nls == null)
			return new MappedReason(reason.getCode(), reason.getDescription());
		return new MappedReason(reason.getCode(), nls.getDescription());
	}
}
