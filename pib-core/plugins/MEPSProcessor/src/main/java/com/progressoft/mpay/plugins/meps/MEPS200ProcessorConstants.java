package com.progressoft.mpay.plugins.meps;

public class MEPS200ProcessorConstants {
	public static final int PAN = 2;
	public static final int PROCESSING_CODE = 3;
	public static final int TRANSACTION_AMOUNT = 4;
	public static final int BILLING_AMOUNT = 6;
	public static final int TRANSMISSION_DATE_TIME = 7;
	public static final int BILLING_AMOUNT_FEES = 8;
	public static final int BILLING_CONVERSION_RATE = 10;
	public static final int SYSTEM_TRACE_AUDIT_NUMBER = 11;
	public static final int TRANSACTION_DATE_TIME = 12;
	public static final int MERCHANT_CATEGORY_CODE = 18;
	public static final int ACQUIRER_COUNTRY_CODE = 19;
	public static final int POS_ENTRY_MODE_CODE = 22;
	public static final int FUNCTION_CODE = 24;
	public static final int ACQUIRER_INSTITUTION_ID_CODE = 32;
	public static final int FORWARDING_INSTITUTION_ID_CODE = 33;
	public static final int RETRIEVAL_REFERENCE_NUMBER = 37;
	public static final int AUTHORIZATION_NUMBER = 38;
	public static final int RESPONSE_CODE = 39;
	public static final int CARD_ACCEPTOR_TERMINAL_ID_CODE = 41;
	public static final int CARD_ACCEPTOR_ID_CODE = 42;
	public static final int TERMINAL_LOCATION = 43;
	public static final int TRANSACTION_CURRENCY_CODE = 49;
	public static final int BILLING_CURRENCY_CODE = 51;
	public static final int ACCOUNT_ID_CODE_1 = 102;
	private MEPS200ProcessorConstants(){}
}
