package com.progressoft.mpay.plugins.mepspayment;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.AmountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileNumberValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PSPValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.SignInValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;
import com.progressoft.mpay.plugins.validators.WalletCapValidator;

public class MEPSPayValidator {
	private static final Logger logger = LoggerFactory.getLogger(MEPSPayValidator.class);

	private MEPSPayValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		MEPSPayMessage request = (MEPSPayMessage) context.getRequest();
		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			result = validateCorporate(context, request);
		else
			return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
		if (result.isValid())
			return validateSettings(context, request);
		return result;
	}

	private static ValidationResult validateSettings(MessageProcessingContext context, MEPSPayMessage request) {

		ValidationResult result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
		if (!result.isValid())
			return result;

		result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context);
		if (!result.isValid())
			return result;

		return PSPValidator.validate(context, false);
	}

	private static ValidationResult validateCorporate(MessageProcessingContext context, MEPSPayMessage request) {
		ValidationResult result = validateCorporateInfo(context);
		if (!result.isValid())
			return result;

		result = SignInValidator.validate(context, context.getSender().getService(), context.getRequest().getDeviceId());
		if (!result.isValid())
			return result;

		if (request.getReceiverType().equals(ReceiverInfoType.MOBILE)) {
			result = MobileNumberValidator.validate(context, request.getReceiverInfo());
			if (!result.isValid())
				return result;
		}

		if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getMobile() != null)
			result = validateReceiverMobile(context);
		if (!result.isValid())
			return result;

		return result;
	}

	private static ValidationResult validateCorporateInfo(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		return AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
	}

	private static ValidationResult validateReceiverMobile(MessageProcessingContext context) {
		ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
		if (!result.isValid())
			return result;
		result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getMobileAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context);
		if(!result.isValid())
			return result;

		return WalletCapValidator.validate(context);
	}
}
