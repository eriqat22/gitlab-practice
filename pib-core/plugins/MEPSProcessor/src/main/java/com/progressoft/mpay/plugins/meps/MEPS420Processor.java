package com.progressoft.mpay.plugins.meps;

import java.math.BigDecimal;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

import com.progressoft.mpay.LanguageMapper;
import com.progressoft.mpay.entities.MPAY_BulkPayment;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.InvalidOperationException;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.MessageProcessorHelper;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.mepspayment.MEPSPayMessage;
import com.progressoft.mpay.plugins.mepspaymentreversal.MEPSPayReversalMessage;
import com.progressoft.mpay.plugins.utils.MepsCommon;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class MEPS420Processor extends MEPSProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MEPS420Processor.class);

	@Override
	public MEPSProcessingResult processMessage(MEPSProcessingContext context) {
		LOGGER.debug("Inside MEPS420Processor....");
		MessageProcessingContext mpayContext = null;
		try {
			MEPSPayReversalMessage mpayRequest = prepareMPayRequest(context);
			mpayContext = prepareMessageProcessingContext(mpayRequest, context);

			MPAY_BulkPayment bulkPayment = context.getDataProvider().getBulkPayment(context.getSystemParameters().getBulkPaymentDefaultId());
			if (bulkPayment == null)
				throw new InvalidOperationException("Default Bulk Payment ID Not Found");
			MPAY_MPayMessage message = MessageProcessorHelper.createMessage(context.getServiceUserManager(), mpayContext, ProcessingStatusCodes.PENDING, null, null, bulkPayment);
			mpayContext.setMessage(message);
			context.getDataProvider().persistMPayMessage(message);
			MessageProcessor processor = MessageProcessorHelper.getProcessor(context.getLookupsLoader().getEndPointOperation(mpayRequest.getOperation()));
			MessageProcessingResult result = processor.processMessage(mpayContext);
			context.getDataProvider().mergeMEPSProcessingResult(result);
			sendNotifications(context.getNotificationHelper(), result);
			IsoMessage isoMessage = generateResponse(context, result.getReasonCode(), result);
			return generateResult(isoMessage, context);
		} catch (DataIntegrityViolationException e) {
			LOGGER.info("Duplicate message received");
			LOGGER.debug("Duplicate message received", e);
			return generateResult(generateResponse(context, ReasonCodes.MESSAGE_IS_DUPLICATED, null), context);
		} catch (Exception ex) {
			LOGGER.error("Error while processMessage", ex);
			return rejectMessage(mpayContext, context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MEPSPayReversalMessage prepareMPayRequest(MEPSProcessingContext context) throws MessageParsingException {
		MEPSPayReversalMessage mpayRequest = new MEPSPayReversalMessage();
		mpayRequest.setLang(LanguageMapper.getInstance().getLanguageLongByLocaleCode(context.getSystemParameters().getSystemLanguage().getCode()));
		mpayRequest.setMsgId(context.getDataProvider().getNextMPClearMessageId());
		mpayRequest.setOperation(context.getSystemParameters().getMEPSReversalEndpoint());
		mpayRequest.setSender(context.getSystemParameters().getMEPSServiceName());
		mpayRequest.setSenderType(ReceiverInfoType.CORPORATE);
		mpayRequest.setTenant(context.getTenantId());
		mpayRequest.getExtraData().add(new ExtraData(MEPSPayMessage.RECEIVER_TYPE_KEY, ReceiverInfoType.MOBILE));
		mpayRequest.getExtraData().add(new ExtraData(MEPSPayReversalMessage.RECEIVER_INFO_KEY, getMobileNumber(context.getMessage(), MEPS420ProcessorConstants.ACCOUNT_ID_CODE_1)));
		mpayRequest.getExtraData().add(new ExtraData(MEPSPayReversalMessage.AMOUNT_KEY,String.valueOf(BigDecimal.valueOf(Double.parseDouble((String) context.getMessage().getField(MEPS200ProcessorConstants.TRANSACTION_AMOUNT).getValue()) / 1000))));
		mpayRequest.getExtraData().add(new ExtraData(MEPSPayReversalMessage.ORIGINAL_MESSAGE_ID_KEY, String.valueOf(context.getMessage().getField(MEPS420ProcessorConstants.ORIGINAL_DATA).getValue()).substring(0,20) +String.valueOf(context.getMessage().getField(MEPS420ProcessorConstants.RETRIEVAL_REFERENCE_NUMBER).getValue())));
		if(context.getMessage().getField(MEPS420ProcessorConstants.PAN)!=null)
			mpayRequest.getExtraData().add(new ExtraData(MEPSPayReversalMessage.CARD_NUMBER_KEY,MepsCommon.showLastThreeDigitInCardNumber((String) context.getMessage().getField(MEPS200ProcessorConstants.PAN).getValue())));
		if(context.getMessage().getField(MEPS420ProcessorConstants.TERMINAL_LOCATION)!=null)
			mpayRequest.getExtraData().add(new ExtraData(MEPSPayReversalMessage.TERMINAL_LOCATION_KEY, (String) context.getMessage().getField(MEPS420ProcessorConstants.TERMINAL_LOCATION).getValue()));
		mpayRequest.getExtraData().add(new ExtraData(MEPSPayReversalMessage.RETRIEVAL_REFERENCE_NUMBER_KEY , (String) context.getMessage().getField(MEPS420ProcessorConstants.RETRIEVAL_REFERENCE_NUMBER).getValue()));
		return mpayRequest;
	}

	@Override
	protected IsoMessage generateResponse(MEPSProcessingContext context, String reasonCode, MessageProcessingResult result) {
		IsoMessage isoMessage = MEPSISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(MPClearIntegrationMessageTypes.REVERSAL_ADVISE_RESPONSE);
		isoMessage.setBinary(false);
		Random randomNumber = new Random();
		String authorizationNumber;
		if (result == null || result.getTransaction() == null)
			authorizationNumber = String.valueOf(randomNumber.nextInt(999999));
		else
			authorizationNumber = result.getTransaction().getExternalReference();

		for (int i = 2; i <= 128; i++) {
			if (context.getMessage().getField(i) != null)
				isoMessage.setField(i, context.getMessage().getField(i));
		}
		isoMessage.setField(MEPS420ProcessorConstants.AUTHORIZATION_NUMBER, new IsoValue<>(IsoType.ALPHA, authorizationNumber, 6));
		isoMessage.setField(MEPS420ProcessorConstants.RESPONSE_CODE, new IsoValue<>(IsoType.NUMERIC,
				MEPSReasonsMapper.getMappedReason(reasonCode, context.getSystemParameters().getSystemLanguage().getId(), context.getDataProvider(), context.getLookupsLoader()).getCode(), 3));
		return isoMessage;
	}
}
