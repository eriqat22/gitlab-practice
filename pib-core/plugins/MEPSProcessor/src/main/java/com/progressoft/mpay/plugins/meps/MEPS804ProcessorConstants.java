package com.progressoft.mpay.plugins.meps;

public class MEPS804ProcessorConstants {
	public static final int TRANSMISSION_DATE_TIME = 7;
	public static final int SYSTEM_TRACE_AUDIT_NUMBER = 11;
	public static final int TRANSACTION_DATE_TIME = 12;
	public static final int FUNCTION_CODE = 24;
	public static final int INSTITUTION_ID_CODE = 33;
	public static final int RETRIEVAL_REFERENCE_NUMBER = 37;
	public static final int RESPONSE_CODE = 39;

	private MEPS804ProcessorConstants() {
	}
}
