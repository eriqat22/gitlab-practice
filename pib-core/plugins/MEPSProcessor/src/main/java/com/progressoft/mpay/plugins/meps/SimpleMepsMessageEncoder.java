package com.progressoft.mpay.plugins.meps;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoMessage;

public class SimpleMepsMessageEncoder {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleMepsMessageEncoder.class);

	public byte[] generateResponse(MEPSProcessingResult result) throws IOException {
		LOGGER.info("Result message is: " + result.getResponse());
		String header = extractMessageHeader(result.getIsoMessage());
		String isoType = extractMessageType(result.getIsoMessage());
		String bitmap = extractBitmap(result.getIsoMessage());
		String messageBody = result.getResponse().substring(header.length() + isoType.length() + bitmap.length());
		String messageHeader = header + isoType;
		byte[] headerBytes = messageHeader.getBytes(StandardCharsets.US_ASCII);
		byte[] bitmapBytes = hexStringToByteArray(bitmap);
		byte[] messageBodyBytes = messageBody.getBytes(StandardCharsets.US_ASCII);
		int length = headerBytes.length + bitmapBytes.length + messageBodyBytes.length;
		DecimalFormat formatter = new DecimalFormat("0000");
		byte[] formattedLength = formatter.format(length).getBytes(StandardCharsets.US_ASCII);
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		buffer.write(formattedLength);
		buffer.write(headerBytes);
		buffer.write(bitmapBytes);
		buffer.write(messageBodyBytes);
//		byte[] copiedArray = new byte[formattedLength.length + headerBytes.length + bitmapBytes.length + messageBody.length()];
//		int currentOffset = 0;
//		System.arraycopy(formattedLength, 0, copiedArray, currentOffset, formattedLength.length);
//		currentOffset+= formattedLength.length;
//
//		System.arraycopy(headerBytes, 0, copiedArray, currentOffset, headerBytes.length);
//		currentOffset+= headerBytes.length;
//
//		System.arraycopy(bitmapBytes, 0, copiedArray, currentOffset, bitmapBytes.length);
//		currentOffset+= bitmapBytes.length;
//
//		System.arraycopy(messageBodyBytes, 0, copiedArray, currentOffset, messageBodyBytes.length);
//		currentOffset+= messageBodyBytes.length;

		//return copiedArray;
		return buffer.toByteArray();
	}

	private String extractMessageHeader(IsoMessage isoMessage) {
		return isoMessage.getIsoHeader();
	}

	private String extractMessageType(IsoMessage isoMessage) {
		return Integer.toHexString(isoMessage.getType());
	}

	private String extractBitmap(IsoMessage isoMessage) {
		StringBuilder binaryBitmap = new StringBuilder();
		boolean hasSecondaryBitmap = false;
		for (int i = 2; i <= 128; i++) {
			if (isoMessage.getField(i) == null)
				binaryBitmap.append("0");
			else {
				binaryBitmap.append("1");
				if (i > 64)
					hasSecondaryBitmap = true;
			}
		}
		if (hasSecondaryBitmap)
			binaryBitmap.insert(0, "1");
		else
			binaryBitmap.insert(0, "0");
		String binaryBitmapString = binaryBitmap.toString();
		if (binaryBitmapString.startsWith("0"))
			binaryBitmapString = binaryBitmapString.substring(0, 64);
		return S2MIsoMessageParser.binaryToHex(binaryBitmapString);
	}

	private byte[] hexStringToByteArray(String input) {
		int length = input.length();
		byte[] data = new byte[length / 2];
		for (int i = 0; i < length; i += 2) {
			data[i / 2] = (byte) ((Character.digit(input.charAt(i), 16) << 4) + (Character.digit(input.charAt(i + 1), 16)));
		}
		return data;
	}

}
