package com.progressoft.mpay.plugins.meps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;

public class MEPS804Processor extends MEPSProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MEPS804Processor.class);

	@Override
	public MEPSProcessingResult processMessage(MEPSProcessingContext context) {
		LOGGER.debug("Inside processMessage....");
		MEPSProcessingResult mepsResult = new MEPSProcessingResult();
		try {
			return generateResult(generateResponse(context, ReasonCodes.VALID, null), context);
		} catch (Exception ex) {
			LOGGER.error("Error while processMessage", ex);
			mepsResult.setResponse(generateResponse(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, null).toString());
			return mepsResult;
		}
	}

	@Override
	protected IsoMessage generateResponse(MEPSProcessingContext context, String reasonCode, MessageProcessingResult result) {
		IsoMessage isoMessage = MEPSISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath()).newMessage(MEPSConstants.MEPS_NETWORK_MANAGEMENT_RESPONSE);
		isoMessage.setBinary(false);
		for (int i = 2; i <= 128; i++) {
			if (context.getMessage().getField(i) != null)
				isoMessage.setField(i, context.getMessage().getField(i));
		}
		isoMessage.setField(MEPS804ProcessorConstants.RESPONSE_CODE, new IsoValue<>(IsoType.NUMERIC,
				MEPSReasonsMapper.getMappedReason(reasonCode, context.getSystemParameters().getSystemLanguage().getId(), context.getDataProvider(), context.getLookupsLoader()).getCode(), 3));
		return isoMessage;
	}
}