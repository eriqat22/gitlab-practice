package com.progressoft.mpay.plugins.meps;

public class MEPSConstants {
	public static final String OPERATION_NAME = "MEPS";
	public static final Integer MEPS_NETWORK_MANAGEMENT_REQUEST = 0x1804;
	public static final Integer MEPS_NETWORK_MANAGEMENT_RESPONSE = 0x1814;
	public static final String MEPS_PROCESSOR_CODE = "2";

	private MEPSConstants() {
	}
}
