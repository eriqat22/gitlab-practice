package com.progressoft.mpay.plugins.mepspaymentreversal;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.AmountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileNumberValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.PSPValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;
import com.progressoft.mpay.plugins.validators.TransactionConfigValidator;

public class MEPSPayReversalValidator {
	private static final Logger logger = LoggerFactory.getLogger(MEPSPayReversalValidator.class);

	private MEPSPayReversalValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		MEPSPayReversalMessage request = (MEPSPayReversalMessage) context.getRequest();

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = validateSender(context);
		if (!result.isValid())
			return result;

		result = MobileNumberValidator.validate(context, request.getReceiverInfo());
		if (!result.isValid())
			return result;

		if (context.getSystemParameters().getOfflineModeEnabled() || context.getReceiver().getMobile() != null) {
			result = validateReceiver(context);
			if (!result.isValid())
				return result;
		}

		result = validateOriginalRequest(context);
		if (!result.isValid())
			return result;

		return validateSettings(context, request);
	}

	private static ValidationResult validateSettings(MessageProcessingContext context, MEPSPayReversalMessage request) {
		ValidationResult result = TransactionConfigValidator.validate(context.getTransactionConfig(), request.getAmount());
		if (!result.isValid())
			return result;

		result = AmountValidator.validate(context);
		if (!result.isValid())
			return result;

		result = LimitsValidator.validate(context);
		if (!result.isValid())
			return result;

		return PSPValidator.validate(context, false);
	}

	private static ValidationResult validateOriginalRequest(MessageProcessingContext context) {
		MPAY_MPayMessage originalMessage = (MPAY_MPayMessage) context.getExtraData().get(Constants.ORIGINAL_MESSAGE_KEY);
		MPAY_Transaction originalTransaction = (MPAY_Transaction) context.getExtraData().get(Constants.ORIGINAL_TRANSACTION_KEY);
		if (originalMessage == null || originalTransaction == null)
			return new ValidationResult(ReasonCodes.TRANSACTION_NOT_FOUND, null, false);
		if (!originalMessage.getProcessingStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return new ValidationResult(ReasonCodes.INVALID_TRANSACTION_STATUS, null, false);
		if (!originalTransaction.getReceiverMobile().getMobileNumber().equals(((MEPSPayReversalMessage) context.getRequest()).getReceiverInfo()))
			return new ValidationResult(ReasonCodes.INVALID_MOBILE_NUMBER, null, false);
		if(originalTransaction.getIsReversed())
			return new ValidationResult(ReasonCodes.TRANSACTION_ALREADY_REVERSED, null, false);
		return new ValidationResult(ReasonCodes.VALID, null, true);
	}

	private static ValidationResult validateSender(MessageProcessingContext context) {
		ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		return AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
	}

	private static ValidationResult validateReceiver(MessageProcessingContext context) {
		logger.debug("Inside ValidateReceiver ...");

		ValidationResult result = MobileValidator.validate(context.getReceiver().getMobile(), false);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getReceiver().getCustomer(), false);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getReceiver().getMobileAccount(), false,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
