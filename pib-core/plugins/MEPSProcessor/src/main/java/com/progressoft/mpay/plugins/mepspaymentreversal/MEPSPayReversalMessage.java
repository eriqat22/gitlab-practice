package com.progressoft.mpay.plugins.mepspaymentreversal;

import java.math.BigDecimal;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang3.StringUtils;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class MEPSPayReversalMessage extends MPayRequest {
	public static final String RECEIVER_INFO_KEY = "rcvId";
	public static final String RECEIVER_TYPE_KEY = "rcvType";
	public static final String NOTES_KEY = "data";
	public static final String AMOUNT_KEY = "amnt";
	public static final String SENDER_ACCOUNT_KEY = "senderAccount";
	public static final String RECEIVER_ACCOUNT_KEY = "receiverAccount";
	public static final String ORIGINAL_MESSAGE_ID_KEY = "originalMessageId";
	public static final String CARD_NUMBER_KEY = "cardNumber";
	public static final String TERMINAL_LOCATION_KEY = "terminalLocation";
	public static final String RETRIEVAL_REFERENCE_NUMBER_KEY = "retrievalReferenceNumber";

	private String receiverInfo;
	private String receiverType;
	private String notes;
	private BigDecimal amount;
	private String senderAccount;
	private String receiverAccount;
	private String originalMessageId;
	private String cardNumber;
	private String terminalLocation;
	private String retrievalReferenceNumber;

	public MEPSPayReversalMessage() {
	}

	public MEPSPayReversalMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getOriginalMessageId() {
		return originalMessageId;
	}

	public void setOriginalMessageId(String originalMessageId) {
		this.originalMessageId = originalMessageId;
	}

	public String getReceiverInfo() {
		return receiverInfo;
	}

	public void setReceiverInfo(String receiverInfo) {
		this.receiverInfo = receiverInfo;
	}

	public String getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(String receiverType) {
		this.receiverType = receiverType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public String getReceiverAccount() {
		return receiverAccount;
	}

	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getTerminalLocation() {
		return terminalLocation;
	}

	public void setTerminalLocation(String terminalLocation) {
		this.terminalLocation = terminalLocation;
	}

	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}

	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}

	public static MEPSPayReversalMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		MEPSPayReversalMessage message = new MEPSPayReversalMessage(request);
		if (StringUtils.isEmpty(message.getSender()))
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setOriginalMessageId(message.getValue(ORIGINAL_MESSAGE_ID_KEY));
		if (message.getOriginalMessageId() == null || message.getOriginalMessageId().trim().length() == 0)
			throw new MessageParsingException(ORIGINAL_MESSAGE_ID_KEY);
		String amount = message.getValue(AMOUNT_KEY);
		if (amount == null)
			throw new MessageParsingException(AMOUNT_KEY);
		try {
			message.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
		} catch (NumberFormatException ex) {
			throw new MessageParsingException(AMOUNT_KEY);
		}
		message.setReceiverInfo(message.getValue(RECEIVER_INFO_KEY));
		if (message.getReceiverInfo() == null)
			throw new MessageParsingException(RECEIVER_INFO_KEY);
		message.setReceiverType(message.getValue(RECEIVER_TYPE_KEY));
		if (message.getReceiverType() == null)
			throw new MessageParsingException(RECEIVER_TYPE_KEY);
		message.setSenderAccount(message.getValue(SENDER_ACCOUNT_KEY));
		message.setReceiverAccount(message.getValue(RECEIVER_ACCOUNT_KEY));
		message.setNotes(message.getValue(NOTES_KEY));
		message.setCardNumber(message.getValue(CARD_NUMBER_KEY));
		message.setTerminalLocation(message.getValue(TERMINAL_LOCATION_KEY));
		message.setRetrievalReferenceNumber(message.getValue(RETRIEVAL_REFERENCE_NUMBER_KEY));
		return message;
	}
}
