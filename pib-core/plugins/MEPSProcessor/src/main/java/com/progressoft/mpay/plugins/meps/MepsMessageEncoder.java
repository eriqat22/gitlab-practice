package com.progressoft.mpay.plugins.meps;

import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoMessage;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

@Sharable
public class MepsMessageEncoder extends MessageToMessageEncoder<MEPSProcessingResult> {

	private static final Logger LOGGER = LoggerFactory.getLogger(MepsMessageEncoder.class);

	@Override
	protected void encode(ChannelHandlerContext context, MEPSProcessingResult result, List<Object> out)
			throws Exception {
		LOGGER.info("Result message is: " + result.getResponse());
		if (!validateIfMessageEmpty(result.getResponse()))
			return;
		String header = extractMessageHeader(result.getIsoMessage());
		String isoType = extractMessageType(result.getIsoMessage());
		String bitmap = extractBitmap(result.getIsoMessage());
		String messageBody = result.getResponse().substring(header.length() + isoType.length() + bitmap.length());
		String messageHeader = header + isoType;
		byte[] headerBytes = messageHeader.getBytes(StandardCharsets.US_ASCII);
		byte[] bitmapBytes = hexStringToByteArray(bitmap);
		byte[] messageBodyBytes = messageBody.getBytes(StandardCharsets.US_ASCII);
		int length = headerBytes.length + bitmapBytes.length + messageBodyBytes.length;
		DecimalFormat formatter = new DecimalFormat("0000");
		byte[] formattedLength = formatter.format(length).getBytes(StandardCharsets.US_ASCII);
		ByteBuf buffer = context.alloc().buffer();
		buffer.writeBytes(formattedLength);
		buffer.writeBytes(headerBytes);
		buffer.writeBytes(bitmapBytes);
		buffer.writeBytes(messageBodyBytes);
		out.add(buffer);
	}

	private boolean validateIfMessageEmpty(String message) {
		if (message.length() == 0)
			return false;
		return true;
	}

	private String extractMessageHeader(IsoMessage isoMessage) {
		return isoMessage.getIsoHeader();
	}

	private String extractMessageType(IsoMessage isoMessage) {
		return Integer.toHexString(isoMessage.getType());
	}

	private String extractBitmap(IsoMessage isoMessage) {
		StringBuilder binaryBitmap = new StringBuilder();
		boolean hasSecondaryBitmap = false;
		for (int i = 2; i <= 128; i++) {
			if (isoMessage.getField(i) == null)
				binaryBitmap.append("0");
			else {
				binaryBitmap.append("1");
				if (i > 64)
					hasSecondaryBitmap = true;
			}
		}
		if (hasSecondaryBitmap)
			binaryBitmap.insert(0, "1");
		else
			binaryBitmap.insert(0, "0");
		String binaryBitmapString = binaryBitmap.toString();
		if (binaryBitmapString.startsWith("0"))
			binaryBitmapString = binaryBitmapString.substring(0, 64);
		return S2MIsoMessageParser.binaryToHex(binaryBitmapString);
	}

	private byte[] hexStringToByteArray(String input) {
		int length = input.length();
		byte[] data = new byte[length / 2];
		for (int i = 0; i < length; i += 2) {
			data[i / 2] = (byte) ((Character.digit(input.charAt(i), 16) << 4)
					+ (Character.digit(input.charAt(i + 1), 16)));
		}
		return data;
	}
}