package com.progressoft.mpay.plugins.meps;

import com.progressoft.mpay.exceptions.NotSupportedException;

public class MEPSProcessorFactory {
	private MEPSProcessorFactory() {
	}

	public static MEPSProcessor createProcessor(int messageType) {
		if (MEPSProcessorConstants.FINANCIAL_REQUEST == messageType)
			return new MEPS200Processor();
		else if (MEPSProcessorConstants.REVERSAL_REQUEST == messageType)
			return new MEPS420Processor();
		else if (MEPSProcessorConstants.NETWORK_REQUEST == messageType)
			return new MEPS804Processor();
		else
			throw new NotSupportedException("messageType: " + messageType);
	}
}
