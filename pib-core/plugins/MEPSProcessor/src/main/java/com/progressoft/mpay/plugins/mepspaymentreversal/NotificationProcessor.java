package com.progressoft.mpay.plugins.mepspaymentreversal;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_EndPointOperation;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_Notification;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.mepspayment.MEPSPayMessage;

public class NotificationProcessor {
	private static final String MEPS_DATE_FORMAT = "dd/MM/YYYY HH:mm:ss";
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateAcceptanceNotificationMessages ...");
		try {
			String receiver;
			String sender;
			String reference;
			long senderLanguageId;
			long receiverLanguageId;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();
			MPAY_MPayMessage originalMessage = (MPAY_MPayMessage) context.getExtraData().get(Constants.ORIGINAL_MESSAGE_KEY);
			MEPSPayMessage mepsPayMessage = MEPSPayMessage.parseMessage((MPayRequest) MEPSPayMessage.fromJson(originalMessage.getRequestContent()));
			MPAY_CorpoarteService senderService = context.getSender().getService();
			MPAY_CustomerMobile receiverMobile = context.getReceiver().getMobile();
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();
			MEPSPayReversalMessage request = MEPSPayReversalMessage
					.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			MEPSReversalNotificationContext notificationContext = new MEPSReversalNotificationContext();
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setReference(reference);

			if (senderService != null) {
				sender = senderService.getName();
				context.getDataProvider().refreshEntity(context.getSender().getAccount());
				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getAccount().getBalance()));
				notificationContext.setSenderBanked(context.getSender().getServiceAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
				senderLanguageId = context.getSender().getCorporate().getPrefLang().getId();
			} else {
				sender = context.getSender().getInfo();
				senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			if (receiverMobile != null) {
				receiver = receiverMobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) && receiverMobile.getAlias() != null ? receiverMobile.getAlias() : receiverMobile
						.getMobileNumber();
				context.getDataProvider().refreshEntity(context.getReceiver().getAccount());
				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getAccount().getBalance()));
				notificationContext.setReceiverBanked(context.getReceiver().getMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
				receiverLanguageId = context.getReceiver().getCustomer().getPrefLang().getId();
			} else {
				receiver = context.getReceiver().getInfo();
				receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			if (context.getReceiver().getNotes() != null) {
				notificationContext.setNotes(context.getReceiver().getNotes());
				notificationContext.setHasNotes(true);
			}
			notificationContext.setExtraData1(SystemHelper.formatDate(context.getTransaction().getTransDate(), MEPS_DATE_FORMAT));
			notificationContext.setExtraData2(request.getCardNumber());
			notificationContext.setExtraData3(mepsPayMessage.getTerminalLocation());
			notificationContext.setRetrievalReferenceNumber(mepsPayMessage.getRetrievalReferenceNumber());
			notificationContext.setReceiver(receiver);
			notificationContext.setSender(sender);
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (receiverMobile != null) {
				notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(),
						receiverLanguageId, receiverMobile.getMobileNumber(), NotificationChannelsCode.SMS));
			}
			notificationContext.setExtraData1(SystemHelper.formatDate(context.getTransaction().getTransDate(), MEPS_DATE_FORMAT));
			notificationContext.setExtraData2(request.getCardNumber());
			notificationContext.setExtraData3(mepsPayMessage.getTerminalLocation());
			notificationContext.setRetrievalReferenceNumber(mepsPayMessage.getRetrievalReferenceNumber());
			if (senderService != null) {
				notificationContext.setExtraData1((String)context.getExtraData().get(Constants.NOTIFICATION_TOKEN_KEY));
				notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
						senderService.getNotificationReceiver(), senderService.getNotificationChannel().getCode()));
			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			MEPSPayReversalMessage request = MEPSPayReversalMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String senderService = request.getSender();
			String notificationChannel = NotificationChannelsCode.EMAIL;
			long languageId = request.getLang();
			MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
			if (service != null) {
				senderService = service.getNotificationReceiver();
				notificationChannel = service.getNotificationChannel().getCode();
				languageId = service.getRefCorporate().getPrefLang().getId();
			}
			MEPSReversalNotificationContext notificationContext = new MEPSReversalNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
			notificationContext.setReceiver(request.getReceiverInfo());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(context.getMessage().getReason().getReasonsNLS().get(languageId).getDescription());
			notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_SENDER,
					context.getMessage().getRefOperation().getId(), languageId, senderService, notificationChannel));
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	public static List<MPAY_Notification> createReversalNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateReversalNotificationMessages ...");
		try {
			String receiver = null;
			String reference;
			List<MPAY_Notification> notifications = new ArrayList<>();

			reference = context.getTransaction().getReference();
			MPAY_CustomerMobile receiverMobile = context.getTransaction().getReceiverMobile();
			MPAY_CustomerMobile senderMobile = context.getTransaction().getSenderMobile();
			MPAY_EndPointOperation operation = context.getTransaction().getRefOperation();
			String currency = context.getTransaction().getCurrency().getStringISOCode();
			if (receiverMobile != null) {
				long receiverLanguageId;
				MEPSReversalNotificationContext receiverNotificationContext = new MEPSReversalNotificationContext();
				receiverNotificationContext.setCurrency(currency);
				receiverNotificationContext.setReference(reference);
				MPAY_Account receiverAccount = context.getTransaction().getReceiverMobileAccount().getRefAccount();
				context.getDataProvider().refreshEntity(receiverAccount);
				receiverNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
				receiverNotificationContext.setReceiverBanked(context.getTransaction().getReceiverMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				receiverLanguageId = receiverMobile.getRefCustomer().getPrefLang().getId();
				receiverNotificationContext.setReceiver(receiver);
				notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), receiverNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
						receiverLanguageId, receiverMobile.getMobileNumber(), NotificationChannelsCode.SMS));
			}
			if (senderMobile != null) {
				long senderLanguageId;
				MEPSReversalNotificationContext senderNotificationContext = new MEPSReversalNotificationContext();
				senderNotificationContext.setCurrency(currency);
				senderNotificationContext.setReference(reference);
				MPAY_Account senderAccount = context.getTransaction().getSenderMobileAccount().getRefAccount();
				context.getDataProvider().refreshEntity(senderAccount);
				senderNotificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
				senderNotificationContext.setReceiverBanked(context.getTransaction().getSenderMobileAccount().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
				senderLanguageId = senderMobile.getRefCustomer().getPrefLang().getId();
				senderNotificationContext.setReceiver(receiver);
				notifications.add(context.getNotificationHelper().createNotification(context.getMessage(), senderNotificationContext, NotificationType.TRANSACTION_REVERSAL, operation.getId(),
						senderLanguageId, senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
			}
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateReversalNotificationMessages", e);
			return new ArrayList<>();
		}
	}
}
