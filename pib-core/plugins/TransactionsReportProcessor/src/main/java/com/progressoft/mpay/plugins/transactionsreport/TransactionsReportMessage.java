package com.progressoft.mpay.plugins.transactionsreport;

import java.sql.Timestamp;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class TransactionsReportMessage extends MPayRequest {

	private static final String dateFormat = "yyyy-MM-dd HH:mm:ss";
	private static final String senderAccountKey = "senderAccount";
	private static final String fromTimeKey = "fromTime";
	private static final String toTimeKey = "toTime";

	private String senderAccount;
	private Timestamp fromTime;
	private Timestamp toTime;

	public String getSenderAccount() {
		return senderAccount;
	}

	public void setSenderAccount(String senderAccount) {
		this.senderAccount = senderAccount;
	}

	public Timestamp getFromTime() {
		return fromTime;
	}

	public void setFromTime(Timestamp fromTime) {
		this.fromTime = fromTime;
	}

	public Timestamp getToTime() {
		return toTime;
	}

	public void setToTime(Timestamp toTime) {
		this.toTime = toTime;
	}

	public TransactionsReportMessage() {

	}

	public TransactionsReportMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static TransactionsReportMessage ParseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		TransactionsReportMessage message = new TransactionsReportMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(SENDER_TYPE_KEY);
		message.setSenderAccount(message.getValue(senderAccountKey));
		try {
			message.setFromTime(new Timestamp(SystemHelper.parseDate(message.getValue(fromTimeKey), dateFormat).getTime()));
		} catch (Exception ex) {
			throw new MessageParsingException(fromTimeKey);
		}
		try {
			message.setToTime(new Timestamp(SystemHelper.parseDate(message.getValue(toTimeKey), dateFormat).getTime()));
		} catch (Exception ex) {
			throw new MessageParsingException(toTimeKey);
		}
		return message;
	}
}