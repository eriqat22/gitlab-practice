package com.progressoft.mpay.plugins.transactionsreport;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;

public class TransactionsReportProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(TransactionsReportProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = TransactionsReportValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Error While parse message", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
			String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
				ProcessingStatusCodes.REJECTED);
		String response = generateResponse(context, false, new ArrayList<MPAY_Transaction>());
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		TransactionsReportMessage message = (TransactionsReportMessage) context.getRequest();
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
		List<MPAY_Transaction> transactions;
		if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			transactions = context.getDataProvider().listMiniTransactions(context.getSender().getMobile(),
					message.getFromTime(), message.getToTime());
		else
			transactions = context.getDataProvider().listMiniTransactions(context.getSender().getService(),
					message.getFromTime(), message.getToTime());
		String response = generateResponse(context, true, transactions);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted,
			List<MPAY_Transaction> transactions) {
		logger.debug("Inside GenerateResponse ...");
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
				context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
					context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		if (isAccepted) {
			handleAcceptance(context, transactions, response);
		} else {
			response.getExtraData().add(new ExtraData(Constants.BalanceKey,
					SystemHelper.formatAmount(context.getSystemParameters(), BigDecimal.ZERO)));
			response.getExtraData().add(new ExtraData(Constants.TransactionsKey,
					getListJson(new ArrayList<TransactionsReportTransaction>())));
		}
		return response.toString();
	}

	private void handleAcceptance(MessageProcessingContext context, List<MPAY_Transaction> transactions,
			MPayResponse response) {
		context.getDataProvider().refreshEntity(context.getSender().getAccount());
		response.getExtraData()
				.add(new ExtraData(Constants.BalanceKey, SystemHelper.formatAmount(context.getSystemParameters(),
						context.getDataProvider().getBalance(context.getSender().getAccount().getId()))));
		List<TransactionsReportTransaction> miniTransactions = new ArrayList<>();
		if (!transactions.isEmpty()) {
			String sender = null;
			if (context.getSender().getMobile() == null && context.getSender().getService() == null)
				sender = context.getSender().getInfo();
			else if (context.getSender().getMobile() != null)
				sender = context.getSender().getMobile().getMobileNumber();
			else if (context.getSender().getService() != null)
				sender = context.getSender().getService().getName();
			for (MPAY_Transaction transaction : transactions) {
				miniTransactions.add(new TransactionsReportTransaction(transaction, sender, context));
			}
		}
		response.getExtraData().add(new ExtraData(Constants.TransactionsKey, getListJson(miniTransactions)));
		response.getExtraData()
		.add(new ExtraData(Constants.BalanceKey, SystemHelper.formatAmount(context.getSystemParameters(), context.getDataProvider().getBalance(context.getSender().getAccount().getId()))));
	}

	private String getListJson(List<TransactionsReportTransaction> transactions) {
		logger.debug("Inside GetListJson ...");
		Gson gson = new GsonBuilder().create();
		return gson.toJson(transactions);
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
		logger.debug("Inside PreProcessMessage ...");
		TransactionsReportMessage message = TransactionsReportMessage.ParseMessage(context.getRequest());
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setRequest(message);
		if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
			sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
			if (sender.getMobile() == null)
				return;
			sender.setCustomer(sender.getMobile().getRefCustomer());
			sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(),
					message.getSenderAccount()));
			if (sender.getMobileAccount() == null)
				return;
			sender.setAccount(sender.getMobileAccount().getRefAccount());
			sender.setProfile(sender.getMobileAccount().getRefProfile());
		} else {
			sender.setService(context.getDataProvider().getCorporateService(context.getRequest().getSender(),
					context.getRequest().getSenderType()));
			if (sender.getService() == null)
				return;
			sender.setCorporate(sender.getService().getRefCorporate());
			sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(),
					message.getSenderAccount()));
			if (sender.getServiceAccount() == null)
				return;
			sender.setAccount(sender.getServiceAccount().getRefAccount());
			sender.setProfile(sender.getServiceAccount().getRefProfile());
		}
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
			String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		logger.debug("Inside Accept ...");
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		logger.debug("Inside Reject ...");
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		logger.debug("Inside Reverse ...");
		return null;
	}
}
