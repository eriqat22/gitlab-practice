package com.progressoft.mpay.plugins.transactionsreport;

public class Constants {
	public static final String BalanceKey = "bal";
	public static final String TransactionsKey = "txs";
}
