package com.progressoft.mpay.plugins.transactionsreport;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.messages.MPayResponse;

public class TransactionsReportResponse extends MPayResponse {
	public static final String BalanceKey = "bal";
	public static final String TransactionsKey = "txs";
	private String balance;

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(this);
		return json;
	}
}
