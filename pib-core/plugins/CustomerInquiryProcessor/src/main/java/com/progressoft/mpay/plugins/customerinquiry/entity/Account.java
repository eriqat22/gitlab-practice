package com.progressoft.mpay.plugins.customerinquiry.entity;

import com.progressoft.mpay.entities.MPAY_Account;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.exceptions.NullArgumentException;

public class Account {
    private MPAY_Account refAccount;
    private String bank;
    private boolean isBanked;
    private String externalAccount;
    private long mas;
    private boolean isActive;
    private boolean isRegistered;
    private String profile;
    private String accountNumber;
    private boolean isDefault;
    private boolean isSwitchDefault;
    private Boolean isDeleted;

    public Account() {
        //
    }

    public Account(MPAY_MobileAccount account) {
        if (account == null)
            throw new NullArgumentException("account");
        this.bank = account.getBank().getName();
        this.isBanked = account.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED);
        this.externalAccount = account.getExternalAcc();
        this.mas = account.getMas();
        this.isActive = account.getIsActive();
        this.isRegistered = account.getIsRegistered();
        this.profile = account.getRefProfile().getName();
        if (account.getRefAccount() != null) {
            this.accountNumber = account.getRefAccount().getAccNumber();
            this.refAccount = account.getRefAccount();
        }
        this.isDefault = account.getIsDefault();
        this.isSwitchDefault = account.getIsSwitchDefault();
        this.isDeleted = account.getDeletedFlag();

    }

    public MPAY_Account getRefAccount() {
        return refAccount;
    }

    public void setRefAccount(MPAY_Account refAccount) {
        this.refAccount = refAccount;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public boolean isBanked() {
        return isBanked;
    }

    public void setBanked(boolean isBanked) {
        this.isBanked = isBanked;
    }

    public String getExternalAccount() {
        return externalAccount;
    }

    public void setExternalAccount(String externalAccount) {
        this.externalAccount = externalAccount;
    }

    public long getMas() {
        return mas;
    }

    public void setMas(long mas) {
        this.mas = mas;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public boolean isSwitchDefault() {
        return isSwitchDefault;
    }

    public void setSwitchDefault(boolean isSwitchDefault) {
        this.isSwitchDefault = isSwitchDefault;
    }
}
