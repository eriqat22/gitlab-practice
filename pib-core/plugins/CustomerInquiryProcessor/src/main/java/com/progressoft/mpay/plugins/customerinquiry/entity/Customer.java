package com.progressoft.mpay.plugins.customerinquiry.entity;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Customer {
	private static SimpleDateFormat FORMAT = new SimpleDateFormat("dd/MM/yyyy");

	private String fullName;
	private String firstName;
	private String middleName;
	private String lastName;
	private boolean isActive;
	private String nationality;
	private String idType;
	private String idValue;
	private String dateOfBirth;
	private String reference;
	private boolean isRegistered;
	private String phoneOne;
	private String phoneTwo;
	private String city;
	private String preferedLanguage;
	private String email;
	private String pobox;
	private String zipCode;
	private String buildingNum;
	private String streetName;

	private String englishFullName;
	private String arabicFullName;
	private String gender;
	private String nationalId;
	private String idenNumber;
	private String idenCard;
	private Date idenCardIssunceDate;
	private String passportId;
	private Date passportIssuanceDate;
	private String clientType;
	private String clientReference;
	private String cityArea;
	private String address;
	private String note;
	private String rejectionNote;
	private String approvalNote;
	private String typeOfOccupation;
	private boolean isBlackListed;
	private Date idExpiryDate;
	private String mobileNumber;
	private String alias;
	private String type;
	private String bank;
	private String branch;
	private String externalAccount;
	private String iban;
	private String walletType;
	private String transActionSize;
	private Long accountSelector;
	private BigDecimal chargeAmountNumber;
	private String chargeAmountChar;
	private String walletAccount;
	private String chargeDuration;
	private String nfcSerial;
	private String profile;
	private Long maxNumberOfDevices;

	private Long permitId;
	private String permitExpiry;
	private String otherNumber;
	private String workNature;
	private String riskProfile;
	private String jobTitle;
	private String otherBank;
	private String workPlace;
	private String otherIncomeResource;
	private String reasonForResidence;
	private String pspUses1;
	private String pspUsesOther;
	private Long permitNumber;
	private String beneficiaryIdExpiry;

	public Customer(MPAY_Customer customer) {
		if (customer == null)
			throw new NullArgumentException("customer");
		this.fullName = customer.getFullName();
		this.firstName = customer.getFirstName();
		this.middleName = customer.getMiddleName();
		this.lastName = customer.getLastName();
		this.isActive = customer.getIsActive();
		this.nationality = customer.getNationality().getName();
		this.idType = customer.getIdType().getName();
		this.idValue = customer.getIdNum();
		this.dateOfBirth = FORMAT.format(customer.getDob());
		this.reference = customer.getClientRef();
		this.isRegistered = customer.getIsRegistered();
		this.phoneOne = customer.getPhoneOne();
		this.phoneTwo = customer.getPhoneTwo();
		this.city = customer.getCity().getName();
		this.preferedLanguage = customer.getPrefLang().getName();
		this.email = customer.getEmail();
		this.pobox = customer.getPobox();
		this.zipCode = customer.getZipCode();
		this.buildingNum = customer.getBuildingNum();
		this.streetName = customer.getStreetName();

		englishFullName = customer.getEnglishFullName();
		arabicFullName = customer.getArabicFullName();
		gender = customer.getGender();
		nationalId = customer.getPassportId();
		idenNumber = customer.getIdNum();
		idenCard = customer.getIdentificationCard();
		idenCardIssunceDate = customer.getIdCardIssuanceDate();
		passportId = customer.getPassportId();
		passportIssuanceDate = customer.getPassportIssuanceDate();
		clientType = customer.getClientType().getName();
		clientReference = customer.getClientRef();
		if (Objects.nonNull(customer.getArea()))
			cityArea = customer.getArea().getName();
		address = customer.getAddress();
		note = customer.getNote();
		rejectionNote = customer.getRejectionNote();
		approvalNote = customer.getApprovalNote();
		typeOfOccupation = customer.getOccupation();
		isBlackListed = customer.getIsBlackListed();
		idExpiryDate = customer.getIdExpiryDate();
		mobileNumber = customer.getMobileNumber();
		alias = customer.getAlias();
		type = customer.getBankedUnbanked();
		bank = customer.getBank().getName();
		branch = customer.getBankBranch();
		externalAccount = customer.getExternalAcc();
		iban = customer.getIban();

		accountSelector = customer.getMas();
		nfcSerial = customer.getNfcSerial();
		profile = customer.getRefProfile().getName();
		maxNumberOfDevices = customer.getMaxNumberOfDevices();
		reasonForResidence = customer.getReasonForResidence();

	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdValue() {
		return idValue;
	}

	public void setIdValue(String idValue) {
		this.idValue = idValue;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public String getPhoneOne() {
		return phoneOne;
	}

	public void setPhoneOne(String phoneOne) {
		this.phoneOne = phoneOne;
	}

	public String getPhoneTwo() {
		return phoneTwo;
	}

	public void setPhoneTwo(String phoneTwo) {
		this.phoneTwo = phoneTwo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPreferedLanguage() {
		return preferedLanguage;
	}

	public void setPreferedLanguage(String preferedLanguage) {
		this.preferedLanguage = preferedLanguage;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPobox() {
		return pobox;
	}

	public void setPobox(String pobox) {
		this.pobox = pobox;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getBuildingNum() {
		return buildingNum;
	}

	public void setBuildingNum(String buildingNum) {
		this.buildingNum = buildingNum;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}


	public String getEnglishFullName() {
		return englishFullName;
	}

	public void setEnglishFullName(String englishFullName) {
		this.englishFullName = englishFullName;
	}

	public String getArabicFullName() {
		return arabicFullName;
	}

	public void setArabicFullName(String arabicFullName) {
		this.arabicFullName = arabicFullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getIdenNumber() {
		return idenNumber;
	}

	public void setIdenNumber(String idenNumber) {
		this.idenNumber = idenNumber;
	}

	public String getIdenCard() {
		return idenCard;
	}

	public void setIdenCard(String idenCard) {
		this.idenCard = idenCard;
	}

	public Date getIdenCardIssunceDate() {
		return idenCardIssunceDate;
	}

	public void setIdenCardIssunceDate(Date idenCardIssunceDate) {
		this.idenCardIssunceDate = idenCardIssunceDate;
	}

	public String getPassportId() {
		return passportId;
	}

	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}

	public Date getPassportIssuanceDate() {
		return passportIssuanceDate;
	}

	public void setPassportIssuanceDate(Date passportIssuanceDate) {
		this.passportIssuanceDate = passportIssuanceDate;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getClientReference() {
		return clientReference;
	}

	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}

	public String getCityArea() {
		return cityArea;
	}

	public void setCityArea(String cityArea) {
		this.cityArea = cityArea;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRejectionNote() {
		return rejectionNote;
	}

	public void setRejectionNote(String rejectionNote) {
		this.rejectionNote = rejectionNote;
	}

	public String getApprovalNote() {
		return approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public String getTypeOfOccupation() {
		return typeOfOccupation;
	}

	public void setTypeOfOccupation(String typeOfOccupation) {
		this.typeOfOccupation = typeOfOccupation;
	}

	public boolean isBlackListed() {
		return isBlackListed;
	}

	public void setBlackListed(boolean blackListed) {
		isBlackListed = blackListed;
	}

	public Date getIdExpiryDate() {
		return idExpiryDate;
	}

	public void setIdExpiryDate(Date idExpiryDate) {
		this.idExpiryDate = idExpiryDate;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getExternalAccount() {
		return externalAccount;
	}

	public void setExternalAccount(String externalAccount) {
		this.externalAccount = externalAccount;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getWalletType() {
		return walletType;
	}

	public void setWalletType(String walletType) {
		this.walletType = walletType;
	}

	public String getTransActionSize() {
		return transActionSize;
	}

	public void setTransActionSize(String transActionSize) {
		this.transActionSize = transActionSize;
	}

	public String getChargeAmountChar() {
		return chargeAmountChar;
	}

	public void setChargeAmountChar(String chargeAmountChar) {
		this.chargeAmountChar = chargeAmountChar;
	}

	public String getWalletAccount() {
		return walletAccount;
	}

	public void setWalletAccount(String walletAccount) {
		this.walletAccount = walletAccount;
	}

	public String getChargeDuration() {
		return chargeDuration;
	}

	public void setChargeDuration(String chargeDuration) {
		this.chargeDuration = chargeDuration;
	}

	public String getNfcSerial() {
		return nfcSerial;
	}

	public void setNfcSerial(String nfcSerial) {
		this.nfcSerial = nfcSerial;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public Long getAccountSelector() {
		return accountSelector;
	}

	public void setAccountSelector(Long accountSelector) {
		this.accountSelector = accountSelector;
	}

	public BigDecimal getChargeAmountNumber() {
		return chargeAmountNumber;
	}

	public void setChargeAmountNumber(BigDecimal chargeAmountNumber) {
		this.chargeAmountNumber = chargeAmountNumber;
	}

	public Long getMaxNumberOfDevices() {
		return maxNumberOfDevices;
	}

	public void setMaxNumberOfDevices(Long maxNumberOfDevices) {
		this.maxNumberOfDevices = maxNumberOfDevices;
	}

	public Long getPermitId() {
		return permitId;
	}

	public void setPermitId(Long permitId) {
		this.permitId = permitId;
	}

	public String getPermitExpiry() {
		return permitExpiry;
	}

	public void setPermitExpiry(String permitExpiry) {
		this.permitExpiry = permitExpiry;
	}

	public String getOtherNumber() {
		return otherNumber;
	}

	public void setOtherNumber(String otherNumber) {
		this.otherNumber = otherNumber;
	}

	public String getWorkNature() {
		return workNature;
	}

	public void setWorkNature(String workNature) {
		this.workNature = workNature;
	}

	public String getRiskProfile() {
		return riskProfile;
	}

	public void setRiskProfile(String riskProfile) {
		this.riskProfile = riskProfile;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getOtherBank() {
		return otherBank;
	}

	public void setOtherBank(String otherBank) {
		this.otherBank = otherBank;
	}

	public String getWorkPlace() {
		return workPlace;
	}

	public void setWorkPlace(String workPlace) {
		this.workPlace = workPlace;
	}

	public String getOtherIncomeResource() {
		return otherIncomeResource;
	}

	public void setOtherIncomeResource(String otherIncomeResource) {
		this.otherIncomeResource = otherIncomeResource;
	}

	public String getReasonForResidence() {
		return reasonForResidence;
	}

	public void setReasonForResidence(String reasonForResidence) {
		this.reasonForResidence = reasonForResidence;
	}

	public String getPspUses1() {
		return pspUses1;
	}

	public void setPspUses1(String pspUses1) {
		this.pspUses1 = pspUses1;
	}

	public String getPspUsesOther() {
		return pspUsesOther;
	}

	public void setPspUsesOther(String pspUsesOther) {
		this.pspUsesOther = pspUsesOther;
	}

	public Long getPermitNumber() {
		return permitNumber;
	}

	public void setPermitNumber(Long permitNumber) {
		this.permitNumber = permitNumber;
	}

	public String getBeneficiaryIdExpiry() {
		return beneficiaryIdExpiry;
	}

	public void setBeneficiaryIdExpiry(String beneficiaryIdExpiry) {
		this.beneficiaryIdExpiry = beneficiaryIdExpiry;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}
}
