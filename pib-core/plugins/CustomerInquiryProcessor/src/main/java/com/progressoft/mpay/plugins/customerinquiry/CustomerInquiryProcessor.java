package com.progressoft.mpay.plugins.customerinquiry;

import com.progressoft.mpay.plugins.customerinquiry.entity.*;
import org.apache.commons.lang.NullArgumentException;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_Customer;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;

import java.math.BigDecimal;

public class CustomerInquiryProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(CustomerInquiryProcessor.class);

	@Override
	public MessageProcessingResult accept(MessageProcessingContext arg0) {
		return null;
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext arg0) {
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult result = CustomerInquiryValidator.validate(context);
			if (result.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, result.getReasonCode(), result.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.debug("Error while parsing message", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in CustomerInquiryProcessor", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
		logger.debug("Inside PreProcessMessage ...");
		CustomerInquiryMessage message = CustomerInquiryMessage.parseMessage(context.getRequest());
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setRequest(message);
		sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
		if (sender.getMobile() == null)
			return;
		sender.setCustomer(sender.getMobile().getRefCustomer());
		sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
		if (sender.getMobileAccount() == null)
			return;
		sender.setProfile(sender.getMobileAccount().getRefProfile());
		OTPHanlder.loadOTP(context, message.getPin());
	}

	private Customer loadCustomer(MessageProcessingContext context) {
		CustomerInquiryMessage message = (CustomerInquiryMessage) context.getRequest();
		MPAY_Customer customer;
		if (!StringUtils.isEmpty(message.getIdNumber()) && !StringUtils.isEmpty(message.getIdTypeCode()))
			customer = context.getDataProvider().getCustomer(message.getIdTypeCode(), message.getIdNumber());
		else {
			MPAY_CustomerMobile mobile = loadCustomerMobile(context.getDataProvider(), message);
			if (mobile == null)
				return null;
			customer = mobile.getRefCustomer();
		}
		if (customer == null)
			return null;
		return new Customer(customer);
	}

	private MPAY_CustomerMobile loadCustomerMobile(IDataProvider dataProvider, CustomerInquiryMessage message) {
		if (!StringUtils.isEmpty(message.getMobileNumber()))
			return dataProvider.getCustomerMobile(message.getMobileNumber(), ReceiverInfoType.MOBILE);
		else if (!StringUtils.isEmpty(message.getAlias()))
			return dataProvider.getCustomerMobile(message.getAlias(), ReceiverInfoType.ALIAS);
		return null;
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
		String response = generateResponse(context, false);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) throws WorkflowException {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
		String response = generateResponse(context, true);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		OTPHanlder.removeOTP(context);
		return result;
	}

	private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("isAccepted: " + isAccepted);
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		String customerData = null;
		if (isAccepted) {
			Customer customer = loadCustomer(context);
			if (customer != null) {
				customerData = customer.toString();
			}
		}
		response.getExtraData().add(new ExtraData(Constants.CUSTOMER_KEY, customerData));
		return response.toString();
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext arg0) {
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext arg0) {
		return null;
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}
}
