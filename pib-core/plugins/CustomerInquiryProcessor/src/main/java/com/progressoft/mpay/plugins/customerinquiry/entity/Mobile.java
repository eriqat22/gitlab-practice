package com.progressoft.mpay.plugins.customerinquiry.entity;

import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MobileAccount;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.NotificationShowTypeNames;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.util.ArrayList;
import java.util.List;

public class Mobile {
	private String mobileNumber;
	private String alias;
	private String nfcSerial;
	private boolean isActive;
	private boolean isRegistered;
	private String notificationShowType;
	private String channelType;
	private boolean isBlocked;

	private List<Account> accounts;

	public Mobile() {
		accounts = new ArrayList<>();
	}

	public Mobile(MPAY_CustomerMobile mobile) {
		if (mobile == null)
			throw new NullArgumentException("mobile");
		this.accounts = new ArrayList<>();
		this.mobileNumber = mobile.getMobileNumber();
		this.alias = mobile.getAlias();
		this.nfcSerial = mobile.getNfcSerial();
		this.isActive = mobile.getIsActive();
		this.isRegistered = mobile.getIsRegistered();
		this.notificationShowType = mobile.getNotificationShowType().equals(NotificationShowTypeCodes.ALIAS) ? NotificationShowTypeNames.ALIAS : NotificationShowTypeNames.MOBILE_NUMBER;
		this.channelType = mobile.getChannelType().getName();
		this.isBlocked = mobile.getIsBlocked();

		for(MPAY_MobileAccount account : mobile.getMobileMobileAccounts()) {
			this.accounts.add(new Account(account));
		}
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getNfcSerial() {
		return nfcSerial;
	}

	public void setNfcSerial(String nfcSerial) {
		this.nfcSerial = nfcSerial;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public String getNotificationShowType() {
		return notificationShowType;
	}

	public void setNotificationShowType(String notificationShowType) {
		this.notificationShowType = notificationShowType;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public List<Account> getAccounts() {
		return accounts;
	}
}
