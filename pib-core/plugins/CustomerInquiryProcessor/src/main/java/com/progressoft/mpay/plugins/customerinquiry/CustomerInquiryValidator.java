package com.progressoft.mpay.plugins.customerinquiry;

import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;

public class CustomerInquiryValidator {
	private static final Logger logger = LoggerFactory.getLogger(CustomerInquiryValidator.class);

	private CustomerInquiryValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");
		if (context == null)
			throw new NullArgumentException("context");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(), context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

//		result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(), context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
//		if (!result.isValid())
//			return result;
//
//		result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
//		if (!result.isValid())
//			return result;

//		result = PinCodeValidator.validate(context, context.getSender().getMobile(), context.getRequest().getPin());
//		if (!result.isValid())
//			return result;

		return new ValidationResult(ReasonCodes.VALID, null, true);
	}
}
