package com.progressoft.mpay.plugins.externalcorporates.payment.postpaid;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

public class PostPaidTransactionJaxbSerilizer implements PostPaidTransactionSerilizer {

	private JAXBContext context = null;

	@Override
	public String serilieze(Transactions transactions) throws JAXBException {
		if(context == null)
			context = JAXBContext.newInstance(Transactions.class);
		StringWriter writer = new StringWriter();
		Marshaller marshaller = context.createMarshaller();
		marshaller.marshal(transactions, writer);
		return writer.toString();
	}
}