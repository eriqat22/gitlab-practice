package com.progressoft.mpay.plugins.externalcorporates.profile.removecustomerbilling.processor;

import com.google.gson.Gson;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ExternalIntegMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.externalcorporates.common.Constants;
import com.progressoft.mpay.plugins.externalcorporates.common.ExternalCorporatesValidator;
import com.progressoft.mpay.plugins.externalcorporates.profile.removecustomerbilling.RemoveCustomerBillingMessage;
import com.progressoft.mpay.plugins.externalcorporates.profile.removecustomerbilling.processor.exception.RemoveCusotmerBillingParseMessageException;
import com.progressoft.mpay.plugins.externalcorporates.profile.removecustomerbilling.processor.exception.RemoveCustomerBillingSigningException;
import com.progressoft.psrouterremovecustomerbilling.client.RemoveCustomerBillingClient;
import com.progressoft.psrouterremovecustomerbilling.convertor.RemoveCustomerBillingConvertor;
import com.progressoft.psrouterremovecustomerbilling.model.*;
import com.progressoft.psrouterremovecustomerbilling.request.RemoveCustomerBilling;
import com.progressoft.psrouterremovecustomerbilling.response.PSRouterResponse;
import com.progressoft.psrouterremovecustomerbilling.response.PSRouterResponse.ResBody.MsgBody.Acct.BillingsRec.BillingRec;
import com.progressoft.psrouterremovecustomerbilling.response.PSRouterResponse.ResBody.MsgBody.Acct.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RemoveCustomerBillingProcessor implements MessageProcessor {
    private static final String MESSAGE_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String SUCCESS_CODE = "0";
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveCustomerBillingProcessor.class);
    private static JAXBContext JAXB_CONTEXT_INSTANCE;
    private XMLGregorianCalendar generateGregorienDate = null;
    private PSRouterResponse routerResponse;

    private static XMLGregorianCalendar generateGregorienDate() {
        try {
            SimpleDateFormat gregorianFormat = new SimpleDateFormat(MESSAGE_DATE_TIME_FORMAT);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianFormat.format(new Date()));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        try {
            preProcessMessage(context);
            return getResultAccordingToValidation(context, ExternalCorporatesValidator.validate(context));
        } catch (Exception exception) {
            return rejectMpayMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, exception.getMessage());
        }
    }

    private void preProcessMessage(MessageProcessingContext context) {
        generateGregorienDate = generateGregorienDate();
        RemoveCustomerBillingMessage removeCustomerBillingMessage = RemoveCustomerBillingMessage
                .parseMessage(context.getRequest());

        context.setRequest(removeCustomerBillingMessage);
        ProcessingContextSide sender = new ProcessingContextSide();

        String senderType = removeCustomerBillingMessage.getSenderType();
        if (senderType.equals(ReceiverInfoType.MOBILE)) {
            sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
            if (sender.getMobile() == null)
                return;
            context.setSender(sender);
            context.setReceiver(new ProcessingContextSide());
            sender.setCustomer(sender.getMobile().getRefCustomer());
            sender.setMobileAccount(
                    SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
            if (sender.getMobileAccount() == null)
                return;
            sender.setProfile(sender.getMobileAccount().getRefProfile());

        } else if (senderType.equals(ReceiverInfoType.CORPORATE))

        {
            sender.setService(context.getDataProvider().getCorporateService(removeCustomerBillingMessage.getSender(),
                    removeCustomerBillingMessage.getSenderType()));
            if (sender.getService() == null)
                return;
            context.setSender(sender);
            sender.setCorporate(sender.getService().getRefCorporate());
            sender.setServiceAccount(
                    SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
            if (sender.getServiceAccount() == null)
                return;
            sender.setProfile(sender.getServiceAccount().getRefProfile());
        }
    }

    private MessageProcessingResult getResultAccordingToValidation(MessageProcessingContext context,
                                                                   ValidationResult validate) {
        return validate.isValid() ? acceptMessage(context)
                : rejectMpayMessage(context, validate.getReasonCode(), validate.getReasonDescription());
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        RemoveCustomerBillingModel model = prepareRemoveCustomerModel(context);
        RemoveCustomerBilling customerBilling = RemoveCustomerBillingConvertor.getInstance()
                .toRemoveCustomerBilling(model);

        MPAY_ExternalIntegMessage integrationMessage = createExternalIntegrationMessage(customerBilling, context);
        String responseMessage = RemoveCustomerBillingClient.sendAccountUploadRequest(model).getResponseMessage();
        if (responseMessage.contains("MPAY-"))
            return rejectMpayMessage(context, ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE,
                    "Service Currently Unavilable");

        routerResponse = PSRouterResponse.toObject(responseMessage.replaceAll("&lt;", "<").replace("&gt;", ">"));
        if (SUCCESS_CODE.equals(routerResponse.getResCode())) {
            if (!isVerifiedRouterSignature(context, generateSignedData(routerResponse, responseMessage),
                    routerResponse.getResSignature())) {
                integrationMessage
                        .setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED));
                context.getDataProvider().updateExternalIntegrationMessage(integrationMessage);
                return rejectMpayMessage(context, ReasonCodes.INVALID_TOKEN, "Invalid Signature from router");
            }
        }

        return reflectStatusToExternalMessageAccordingToResponse(context, integrationMessage, routerResponse,
                responseMessage);
    }

    private RemoveCustomerBillingModel prepareRemoveCustomerModel(MessageProcessingContext context) {
        return new RemoveCustomerBillingModel.Builder().requestMessageTypeModel(prepareMessageTypeModel(context))
                .build();
    }

    private RequestMessageTypeModel prepareMessageTypeModel(MessageProcessingContext context) {
        return new RequestMessageTypeModel.Builder().messageDateTime(generateGregorienDate)
                .pspSignature(generatePspSignature(context))
                .pspId(((RemoveCustomerBillingMessage) context.getRequest()).getPspId())
                .messageId(context.getRequest().getMsgId()).messageBodyTypeModel(prepareMessageBodyType(context))
                .build();
    }

    private MessageBodyTypeModel prepareMessageBodyType(MessageProcessingContext context) {
        return new MessageBodyTypeModel.Builder().acctTypeModel(generateAcctTypeModel(context))
                .billingsRecTypeModel(generateBillingsRecTypeModel(context)).build();
    }

    private BillingsRecTypeModel generateBillingsRecTypeModel(MessageProcessingContext context) {
        return new BillingsRecTypeModel.Builder().billingRecTypeModel(generateBillingRecTypeModel(context)).build();
    }

    private BillingRecTypeModel generateBillingRecTypeModel(MessageProcessingContext context) {
        return new BillingRecTypeModel.Builder()
                .billerCode(((RemoveCustomerBillingMessage) context.getRequest()).getBillerCode())
                .billingNo(((RemoveCustomerBillingMessage) context.getRequest()).getBillingCode())
                .serviceType(((RemoveCustomerBillingMessage) context.getRequest()).getServiceType()).build();
    }

    private AcctTypeModel generateAcctTypeModel(MessageProcessingContext context) {
        // TODO because Router bug
        return new AcctTypeModel.Builder().joebppsNo(new Integer(0)).build();
    }

    private String generatePspSignature(MessageProcessingContext context) {
        StringBuilder builder = new StringBuilder();
        RemoveCustomerBillingMessage removeCustomerBillingMessage = (RemoveCustomerBillingMessage) context.getRequest();

        builder.append(removeCustomerBillingMessage.getPspId()).append(removeCustomerBillingMessage.getMsgId())
                .append(generateGregorienDate.toString()).append(removeCustomerBillingMessage.getEfawtercomProfileId())
                .append(removeCustomerBillingMessage.getBillerCode())
                .append(removeCustomerBillingMessage.getBillingCode())
                .append(removeCustomerBillingMessage.getServiceType());

        LOGGER.info("The values to sign is " + builder.toString());
        return context.getCryptographer().generateToken(context.getSystemParameters(), builder.toString(), "",
                IntegMessagesSource.SYSTEM);
    }

    private boolean isVerifiedRouterSignature(MessageProcessingContext context, String response, String resSignature) {
        return context.getCryptographer().verifyToken(context.getSystemParameters(), response, resSignature, null,
                IntegMessagesSource.E_FAWATERCOM);
    }

    private String generateSignedData(PSRouterResponse response, String accInquiryResponse) {
        String message = response.getOriginaleMsgId() + response.getResDateTime().toString() + response.getResCode()
                + response.getResDesc() + collectMessageBodyData(response);
        LOGGER.info("The values to sign is " + message);
        return message;
    }

    private String collectMessageBodyData(PSRouterResponse response) {
        try {

            String message;
            String joebppsNo = response.getResBody().getMsgBody().getAcct().getJOEBPPSNo();
            Result result = response.getResBody().getMsgBody().getAcct().getResult();
            message = joebppsNo + result.getErrorCode() + result.getErrorDesc() + result.getSeverity();
            for (BillingRec billRecord : response.getResBody().getMsgBody().getAcct().getBillingsRec()
                    .getBillingRec()) {
                message += billRecord.getBillerCode() + billRecord.getBillingNo() + billRecord.getServiceType()
                        + billRecord.getResult().getErrorCode() + billRecord.getResult().getErrorDesc()
                        + billRecord.getResult().getSeverity();
            }
            return message;
        } catch (Exception e) {
            throw new RemoveCustomerBillingSigningException(e);
        }
    }

    private MessageProcessingResult reflectStatusToExternalMessageAccordingToResponse(MessageProcessingContext context,
                                                                                      MPAY_ExternalIntegMessage integrationMessage, PSRouterResponse response, String responseContent) {
        integrationMessage.setResponseContent(responseContent);
        if (SUCCESS_CODE.equals(response.getResCode())) {
            integrationMessage
                    .setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED));
            context.getDataProvider().updateExternalIntegrationMessage(integrationMessage);
            return acceptMpayMessage(context, response);
        } else {
            integrationMessage
                    .setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED));
            context.getDataProvider().updateExternalIntegrationMessage(integrationMessage);
            return rejectMpayMessage(context, ReasonCodes.SERVICE_INTEGRATION_REJECTION, response.getResDesc());
        }
    }

    private MessageProcessingResult acceptMpayMessage(MessageProcessingContext context, PSRouterResponse response) {
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID,
                new Gson().toJson(response).toString(), ProcessingStatusCodes.ACCEPTED);
        result.setResponse(generateResponse(context, true));
        return result;
    }

    private MessageProcessingResult rejectMpayMessage(MessageProcessingContext context, String reasonCode,
                                                      String reasonDescription) {
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
                ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        String description;
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        checkIsContextLanguageIsNull(context);
        description = prepareMpayReasonsNls(context);
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private void checkIsContextLanguageIsNull(MessageProcessingContext context) {
        if (context.getLanguage() == null)
            context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
    }

    private String prepareMpayReasonsNls(MessageProcessingContext context) {
        String description;
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    SystemParameters.getInstance().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        return description;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                 String reasonDescription, String processingStatus) {
        MPAY_ProcessingStatus status = prepareMessageContext(context, reasonCode, reasonDescription, processingStatus);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private MPAY_ProcessingStatus prepareMessageContext(MessageProcessingContext context, String reasonCode,
                                                        String reasonDescription, String processingStatus) {
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        return status;
    }

    private MPAY_ExternalIntegMessage createExternalIntegrationMessage(RemoveCustomerBilling removeCustomerBilling,
                                                                       MessageProcessingContext context) {
        MPAY_ExternalIntegMessage integrationMessage = new MPAY_ExternalIntegMessage();
        integrationMessage.setRequestContent(createXmlContent(removeCustomerBilling));
        integrationMessage.setRequestDate(SystemHelper.getSystemTimestamp());
        integrationMessage.setRequestToken(removeCustomerBilling.getRequestMessage().getPspSignature());
        integrationMessage.setRequestID(removeCustomerBilling.getRequestMessage().getMessageId());
        integrationMessage.setReqField1(removeCustomerBilling.getRequestMessage().getPspId());
        integrationMessage.setReqField2(Constants.PS_ROUTER_NAME);
        integrationMessage.setRefMessage(context.getMessage());
        integrationMessage.setRefService(context.getReceiver().getService());
        integrationMessage.setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
        context.getDataProvider().persistExternalIntegrationMessage(integrationMessage);
        return integrationMessage;
    }

    private String createXmlContent(RemoveCustomerBilling removeCustomerBilling) {
        try {
            if (JAXB_CONTEXT_INSTANCE == null)
                JAXB_CONTEXT_INSTANCE = JAXBContext.newInstance(RemoveCustomerBilling.class);
            StringWriter writer = new StringWriter();
            JAXB_CONTEXT_INSTANCE.createMarshaller().marshal(removeCustomerBilling, writer);
            return writer.toString();
        } catch (JAXBException e) {
            throw new RemoveCusotmerBillingParseMessageException("Error while Create Xml Content");
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

}
