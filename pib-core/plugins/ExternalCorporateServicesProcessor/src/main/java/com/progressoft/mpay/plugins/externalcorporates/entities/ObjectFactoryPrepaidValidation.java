//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2016.12.03 at 12:22:11 PM EET
//


package com.progressoft.mpay.plugins.externalcorporates.entities;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the generated package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactoryPrepaidValidation {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     *
     */
    public ObjectFactoryPrepaidValidation() {
    }

    /**
     * Create an instance of {@link PSRouterResponse }
     *
     */
    public PSRouterResponse createPSRouterResponse() {
        return new PSRouterResponse();
    }

    /**
     * Create an instance of {@link PSRouterResponse.ResBody }
     *
     */
    public PSRouterResponse.ResBody createPSRouterResponseResBody() {
        return new PSRouterResponse.ResBody();
    }

    /**
     * Create an instance of {@link PSRouterResponseBillInq.ResBody.MsgBody }
     *
     */
    public PSRouterResponsePrepaidValidation.ResBody.MsgBody createPSRouterResponseResBodyMsgBody() {
        return new PSRouterResponsePrepaidValidation.ResBody.MsgBody();
    }

    /**
     * Create an instance of {@link PSRouterResponseBillInq.ResBody.MsgBody.BillingInfo }
     *
     */
    public PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo createPSRouterResponseResBodyMsgBodyBillingInfo() {
        return new PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo();
    }

    /**
     * Create an instance of {@link PSRouterResponseBillInq.ResBody.MsgBody.BillingInfo.Result }
     *
     */
    public PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo.Result createPSRouterResponseResBodyMsgBodyBillingInfoResult() {
        return new PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo.Result();
    }

    /**
     * Create an instance of {@link PSRouterResponseBillInq.ResBody.MsgBody.BillingInfo.AcctInfo }
     *
     */
    public PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo.AcctInfo createPSRouterResponseResBodyMsgBodyBillingInfoAcctInfo() {
        return new PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo.AcctInfo();
    }

    /**
     * Create an instance of {@link PSRouterResponseBillInq.ResBody.MsgBody.BillingInfo.ServiceTypeDetails }
     *
     */
    public PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo.ServiceTypeDetails createPSRouterResponseResBodyMsgBodyBillingInfoServiceTypeDetails() {
        return new PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo.ServiceTypeDetails();
    }

}
