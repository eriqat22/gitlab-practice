package com.progressoft.mpay.plugins.externalcorporates.profile.addcustomerbilling.view;

public interface AddCustomerBillingMessageView {

	String getPspId();

	String getMessageDate();

	String getBillerCode();

	String getBillingCode();

	String getServiceType();

	String getEfawtercomProfileId();

}