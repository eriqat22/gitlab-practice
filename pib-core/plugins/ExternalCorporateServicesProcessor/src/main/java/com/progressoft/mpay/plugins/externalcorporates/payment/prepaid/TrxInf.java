package com.progressoft.mpay.plugins.externalcorporates.payment.prepaid;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "acctInfo",
    "bankTrxID",
    "validationCode",
    "pmtStatus",
    "dueAmt",
    "paidAmt",
    "processDate",
    "accessChannel",
    "paymentMethod",
    "paymentType",
    "currency",
    "serviceTypeDetails",
    "payerInfo"
})
@XmlRootElement(name = "TrxInf")
public class TrxInf {

    @XmlElement(name = "AcctInfo", required = true)
    protected TrxInf.AcctInfo acctInfo;
    @XmlElement(name = "BankTrxID")
    protected String bankTrxID;
    @XmlElement(name = "ValidationCode")
    protected String validationCode;
    @XmlElement(name = "PmtStatus", required = true)
    protected String pmtStatus;
    @XmlElement(name = "DueAmt")
    protected String dueAmt;
    @XmlElement(name = "PaidAmt")
    protected String paidAmt;
    @XmlElement(name = "ProcessDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar processDate;
    @XmlElement(name = "AccessChannel", required = true)
    protected String accessChannel;
    @XmlElement(name = "PaymentMethod", required = true)
    protected String paymentMethod;
    @XmlElement(name = "PaymentType", required = true)
    protected String paymentType;
    @XmlElement(name = "Currency", required = true)
    protected String currency;
    @XmlElement(name = "ServiceTypeDetails", required = true)
    protected TrxInf.ServiceTypeDetails serviceTypeDetails;
    @XmlElement(name = "PayerInfo", required = true)
    protected TrxInf.PayerInfo payerInfo;

    public TrxInf.AcctInfo getAcctInfo() {
        return acctInfo;
    }

    public void setAcctInfo(TrxInf.AcctInfo value) {
        this.acctInfo = value;
    }

    public String getBankTrxID() {
        return bankTrxID;
    }

    public void setBankTrxID(String value) {
        this.bankTrxID = value;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String value) {
        this.validationCode = value;
    }

    public String getPmtStatus() {
        return pmtStatus;
    }

    public void setPmtStatus(String value) {
        this.pmtStatus = value;
    }

    public String getDueAmt() {
        return dueAmt;
    }

    public void setDueAmt(String value) {
        this.dueAmt = value;
    }

    public String getPaidAmt() {
        return paidAmt;
    }

    public void setPaidAmt(String value) {
        this.paidAmt = value;
    }

    public XMLGregorianCalendar getProcessDate() {
        return processDate;
    }

    public void setProcessDate(XMLGregorianCalendar value) {
        this.processDate = value;
    }

    public String getAccessChannel() {
        return accessChannel;
    }

    public void setAccessChannel(String value) {
        this.accessChannel = value;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String value) {
        this.currency = value;
    }

    public TrxInf.ServiceTypeDetails getServiceTypeDetails() {
        return serviceTypeDetails;
    }

    public void setServiceTypeDetails(TrxInf.ServiceTypeDetails value) {
        this.serviceTypeDetails = value;
    }

    public TrxInf.PayerInfo getPayerInfo() {
        return payerInfo;
    }

    public void setPayerInfo(TrxInf.PayerInfo value) {
        this.payerInfo = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "billingNo",
        "billerCode"
    })
    public static class AcctInfo {

        @XmlElement(name = "BillingNo")
        protected String billingNo;
        @XmlElement(name = "BillerCode")
        protected String billerCode;

        public String getBillingNo() {
            return billingNo;
        }

        public void setBillingNo(String value) {
            this.billingNo = value;
        }

        public String getBillerCode() {
            return billerCode;
        }

        public void setBillerCode(String value) {
            this.billerCode = value;
        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idType",
        "id",
        "nation",
        "name",
        "phone",
        "address"
    })
    public static class PayerInfo {

        @XmlElement(name = "IdType", required = true)
        protected String idType;
        @XmlElement(name = "Id")
        protected String id;
        @XmlElement(name = "Nation", required = true)
        protected String nation;
        @XmlElement(name = "Name", required = true)
        protected String name;
        @XmlElement(name = "Phone")
        protected int phone;
        @XmlElement(name = "Address", required = true)
        protected String address;

        public String getIdType() {
            return idType;
        }

        public void setIdType(String value) {
            this.idType = value;
        }

        public String getId() {
            return id;
        }

        public void setId(String value) {
            this.id = value;
        }

        public String getNation() {
            return nation;
        }

        public void setNation(String value) {
            this.nation = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String value) {
            this.name = value;
        }

        public int getPhone() {
            return phone;
        }

        public void setPhone(int value) {
            this.phone = value;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String value) {
            this.address = value;
        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceType",
        "prepaidCat"
    })
    public static class ServiceTypeDetails {

        @XmlElement(name = "ServiceType", required = true)
        protected String serviceType;
        @XmlElement(name = "PrepaidCat")
        protected String prepaidCat;

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String value) {
            this.serviceType = value;
        }

        public String getPrepaidCat() {
            return prepaidCat;
        }

        public void setPrepaidCat(String value) {
            this.prepaidCat = value;
        }
    }
}
