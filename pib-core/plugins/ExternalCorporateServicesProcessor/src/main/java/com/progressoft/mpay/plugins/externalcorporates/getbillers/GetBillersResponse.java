package com.progressoft.mpay.plugins.externalcorporates.getbillers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class GetBillersResponse {

	private int billerCode;
	private String engName;
	private String arName;
	private String website;
	private String email;
	private String phone;
	private String category;
	private int stmtBankCode;
	private List<BillerService> Services;

	public int getBillerCode() {
		return billerCode;
	}

	public void setBillerCode(int billerCode) {
		this.billerCode = billerCode;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public String getArName() {
		return arName;
	}

	public void setArName(String arName) {
		this.arName = arName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getStmtBankCode() {
		return stmtBankCode;
	}

	public void setStmtBankCode(int stmtBankCode) {
		this.stmtBankCode = stmtBankCode;
	}

	public List<BillerService> getServices() {
		return Services;
	}

	public void setServices(List<BillerService> services) {
		Services = services;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(this);
		return json;
	}
}
