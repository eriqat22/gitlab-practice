package com.progressoft.mpay.plugins.externalcorporates.payment.prepaid;

import javax.xml.bind.JAXBException;

interface PrePaidTransactionSerilizer {

	String serilieze(TrxInf prePaidTransaction) throws JAXBException ;

}
