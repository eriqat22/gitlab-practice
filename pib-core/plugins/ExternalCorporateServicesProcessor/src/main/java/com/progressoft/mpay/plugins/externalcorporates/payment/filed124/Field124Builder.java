package com.progressoft.mpay.plugins.externalcorporates.payment.filed124;

import com.progressoft.mpay.plugins.externalcorporates.payment.PaymentMessage;

public interface Field124Builder {

	public String createFiled124Xml(PaymentMessage message) throws Exception;
}
