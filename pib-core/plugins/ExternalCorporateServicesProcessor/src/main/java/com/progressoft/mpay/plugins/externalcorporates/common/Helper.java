package com.progressoft.mpay.plugins.externalcorporates.common;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class Helper {

	public static String collectXMLElements(String xmlMessage)
			throws ParserConfigurationException, SAXException, IOException {
		String message = "";
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		ByteArrayInputStream input = new ByteArrayInputStream(xmlMessage.getBytes("UTF-8"));
		NodeList elementsList = getNodeList(builder, input);
		message = getMessage(message, elementsList);
		return message;
	}

	private static NodeList getNodeList(DocumentBuilder builder, ByteArrayInputStream input)
			throws SAXException, IOException {
		return builder.parse(input).getChildNodes();
	}

	private static String getMessage(String message, NodeList elementsList) {
		for (int i = 0; i < elementsList.getLength(); i++)
			message += elementsList.item(i).getTextContent();

		return message;
	}
}
