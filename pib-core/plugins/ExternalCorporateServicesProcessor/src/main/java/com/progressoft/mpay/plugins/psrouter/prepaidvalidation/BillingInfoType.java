
package com.progressoft.mpay.plugins.psrouter.prepaidvalidation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Java class for BillingInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BillingInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctInfo" type="{http://www.progressoft.com/PSRouterPrepaidValidation/}AcctInfoType"/>
 *         &lt;element name="DueAmt" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ServiceTypeDetails" type="{http://www.progressoft.com/PSRouterPrepaidValidation/}ServiceTypeDetailsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingInfoType", propOrder = {
    "acctInfo",
    "dueAmt",
    "serviceTypeDetails"
})
public class BillingInfoType {

    @XmlElement(name = "AcctInfo", required = true)
    protected AcctInfoType acctInfo;
    @XmlElement(name = "DueAmt")
    protected BigDecimal dueAmt;
    @XmlElement(name = "ServiceTypeDetails", required = true)
    protected ServiceTypeDetailsType serviceTypeDetails;

    /**
     * Gets the value of the acctInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AcctInfoType }
     *     
     */
    public AcctInfoType getAcctInfo() {
        return acctInfo;
    }

    /**
     * Sets the value of the acctInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcctInfoType }
     *     
     */
    public void setAcctInfo(AcctInfoType value) {
        this.acctInfo = value;
    }

    /**
     * Gets the value of the dueAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDueAmt() {
        return dueAmt;
    }

    /**
     * Sets the value of the dueAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDueAmt(BigDecimal value) {
        this.dueAmt = value;
    }

    /**
     * Gets the value of the serviceTypeDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceTypeDetailsType }
     *     
     */
    public ServiceTypeDetailsType getServiceTypeDetails() {
        return serviceTypeDetails;
    }

    /**
     * Sets the value of the serviceTypeDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceTypeDetailsType }
     *     
     */
    public void setServiceTypeDetails(ServiceTypeDetailsType value) {
        this.serviceTypeDetails = value;
    }

}
