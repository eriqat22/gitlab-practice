
package com.progressoft.mpay.plugins.psrouter.billers;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pspId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pspSignature" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messageId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messageDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="messageBody" type="{http://www.progressoft.com/PSRouterBillers2/}messageBodyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pspId",
    "pspSignature",
    "messageId",
    "messageDateTime",
    "messageBody"
})
@XmlRootElement(name = "GetBillers")
public class GetBillers {

    @XmlElement(required = true)
    protected String pspId;
    @XmlElement(required = true)
    protected String pspSignature;
    @XmlElement(required = true)
    protected String messageId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar messageDateTime;
    @XmlElement(required = true)
    protected MessageBodyType messageBody;

    /**
     * Gets the value of the pspId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPspId() {
        return pspId;
    }

    /**
     * Sets the value of the pspId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPspId(String value) {
        this.pspId = value;
    }

    /**
     * Gets the value of the pspSignature property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPspSignature() {
        return pspSignature;
    }

    /**
     * Sets the value of the pspSignature property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPspSignature(String value) {
        this.pspSignature = value;
    }

    /**
     * Gets the value of the messageId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the messageDateTime property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getMessageDateTime() {
        return messageDateTime;
    }

    /**
     * Sets the value of the messageDateTime property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setMessageDateTime(XMLGregorianCalendar value) {
        this.messageDateTime = value;
    }

    /**
     * Gets the value of the messageBody property.
     *
     * @return
     *     possible object is
     *     {@link MessageBodyType }
     *
     */
    public MessageBodyType getMessageBody() {
        return messageBody;
    }

    /**
     * Sets the value of the messageBody property.
     *
     * @param value
     *     allowed object is
     *     {@link MessageBodyType }
     *
     */
    public void setMessageBody(MessageBodyType value) {
        this.messageBody = value;
    }

	@Override
	public String toString() {
		return pspId + messageId + messageDateTime + messageBody.categoryCode;
	}
}
