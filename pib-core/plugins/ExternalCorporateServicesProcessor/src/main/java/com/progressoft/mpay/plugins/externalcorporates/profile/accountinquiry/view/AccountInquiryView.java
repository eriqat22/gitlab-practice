package com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry.view;

public interface AccountInquiryView {

	String getEfawatercomCustomerId();
	String getPspId();
	String getPspSignature();
}
