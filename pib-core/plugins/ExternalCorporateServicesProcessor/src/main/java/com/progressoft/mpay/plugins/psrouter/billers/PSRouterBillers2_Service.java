package com.progressoft.mpay.plugins.psrouter.billers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import java.net.URL;

@WebServiceClient(name = "PSRouterBillers2", wsdlLocation = "PSRouterBillers2.wsdl", targetNamespace = "http://www.progressoft.com/PSRouterBillers2/")
public class PSRouterBillers2_Service extends Service {
	public final static QName SERVICE = new QName("http://www.progressoft.com/PSRouterBillers2/", "PSRouterBillers2");
	public final static QName PSRouterBillers2SOAP = new QName("http://www.progressoft.com/PSRouterBillers2/",
			"PSRouterBillers2SOAP");
    private static final Logger logger = LoggerFactory.getLogger(PSRouterBillers2_Service.class);
    public static URL WSDL_LOCATION;

	static {

		// TODO: change location to be in properties file.
		WSDL_LOCATION = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try {
			URL url = classLoader.getResource("/config/app/wsdl/PSRouterBillers2.wsdl");
			if (url == null) {
				java.util.logging.Logger.getLogger(PSRouterBillers2_Service.class.getName()).log(
						java.util.logging.Level.INFO, "Can not initialize the default wsdl from {0}",
						"PSRouterBillers2.wsdl");
			}
			WSDL_LOCATION = url;
		} catch (Exception e) {
			logger.error("error... change me later", e);
		}

	}

	public PSRouterBillers2_Service(URL wsdlLocation) {
		super(wsdlLocation, SERVICE);
	}

	public PSRouterBillers2_Service(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public PSRouterBillers2_Service() {
		super(WSDL_LOCATION, SERVICE);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterBillers2_Service(WebServiceFeature... features) {
		super(WSDL_LOCATION, SERVICE, features);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterBillers2_Service(URL wsdlLocation, WebServiceFeature... features) {
		super(wsdlLocation, SERVICE, features);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterBillers2_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	/**
	 *
	 * @return returns PSRouterBillers2
	 */
	@WebEndpoint(name = "PSRouterBillers2SOAP")
	public PSRouterBillers2 getPSRouterBillers2SOAP() {
		return super.getPort(PSRouterBillers2SOAP, PSRouterBillers2.class);
	}

	/**
	 *
	 * @param features
	 *            A list of {@link WebServiceFeature} to configure
	 *            on the proxy. Supported features not in the
	 *            <code>features</code> parameter will have their default
	 *            values.
	 * @return returns PSRouterBillers2
	 */
	@WebEndpoint(name = "PSRouterBillers2SOAP")
	public PSRouterBillers2 getPSRouterBillers2SOAP(WebServiceFeature... features) {
		return super.getPort(PSRouterBillers2SOAP, PSRouterBillers2.class, features);
	}

}
