package com.progressoft.mpay.plugins.externalcorporates.common;

public class Constants {
    public static final String GetBillersRequestKey = "GetBillersRequest";
    public static final String GetBillersResponseKey = "GetBillers";
    public static final String BillInquiryRequestKey = "BillInquiryRequest";
    public static final String BillInquiryResponseKey = "eFawBillInq";
    public static final String PaymentResponseKey = "payment";
    public static final String PrePaidValidationRequestKey = "PrePaidValidationRequest";
    public static final String PrePaidValidationResponseKey = "PrePaidValidationResponse";
    public static final String PS_ROUTER_NAME = "PSROUTER";
    public static final String Corporate = "C";
    public static final String certificateAlias = "eFawatercom Certificate Alias";
    public static final String Valid = "0";
    public static final String Failedconnect = "2";
    public static final String InvalidMessageSignature = "014";
    public static final String InvalidPSPID = "003";
    public static final String BillPaymentType = "1";

    public static final String billingNoKey = "billingNo";
    public static final String billerCodeKey = "billerCode";
    public static final String billNoKey = "billNo";
    public static final String serviceTypeKey = "serviceType";
    public static final String billStatusKey = "billStatus";
    public static final String dueAmntKey = "dueAmnt";
    public static final String feesAmntKey = "feesAmnt";
    public static final String issueDateKey = "issueDate";
    public static final String dueDateKey = "dueDate";
    public static final String allowPartKey = "allowPart";
    public static final String lowerKey = "lower";
    public static final String upperKey = "upper";
    public static final String billPmtsKey = "billPmts";
    public static final String validationCodeKey = "validationCode";
    public static final String msgLabelKey = "msgLabel";
    public static final String prepaidCatKey = "prepaidCat";
    public static final String AccountUploadRequestKey = "AccountUploadRequest";
    public static final String AccountUploadResponseKey = "AccountUploadResponse";
    public static final String AccountInquiryResponseKey = "AccountInquiryResponse";
    public static final String AccountInquiryRequestKey = "AccountInquiryRequest";

}
