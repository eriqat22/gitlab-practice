package com.progressoft.mpay.plugins.externalcorporates.payment.prepaid;

import com.progressoft.mpay.plugins.externalcorporates.payment.PaymentMessage;

public interface PrePaidObjectBuilder {

	TrxInf buildPostPaidTransaction(PaymentMessage message);
}