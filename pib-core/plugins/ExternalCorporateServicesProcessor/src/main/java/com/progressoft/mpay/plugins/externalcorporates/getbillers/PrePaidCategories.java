package com.progressoft.mpay.plugins.externalcorporates.getbillers;

public class PrePaidCategories {
	private String name;
	private String engDesc;
	private String arDesc;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEngDesc() {
		return engDesc;
	}

	public void setEngDesc(String engDesc) {
		this.engDesc = engDesc;
	}

	public String getArDesc() {
		return arDesc;
	}

	public void setArDesc(String arDesc) {
		this.arDesc = arDesc;
	}
}
