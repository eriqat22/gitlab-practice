package com.progressoft.mpay.plugins.externalcorporates.payment.prepaid;

import com.progressoft.mpay.plugins.externalcorporates.payment.PaymentMessage;
import com.progressoft.mpay.plugins.externalcorporates.payment.filed124.Field124Builder;

import javax.xml.bind.JAXBException;

public class PrepaidField124Builder implements Field124Builder {

	@Override
	public String createFiled124Xml(PaymentMessage message) throws JAXBException {
		TrxInf prePaidTransaction = PrePaidTransactionObjectBuilderFactory.getObjectBuilder().buildPostPaidTransaction(message);
		return PrePaidTransactionSerilizerFactory.getSeriliezer().serilieze(prePaidTransaction);
	}

}
