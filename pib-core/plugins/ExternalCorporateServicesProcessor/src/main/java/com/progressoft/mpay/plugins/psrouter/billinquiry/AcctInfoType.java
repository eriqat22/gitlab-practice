
package com.progressoft.mpay.plugins.psrouter.billinquiry;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for AcctInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AcctInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BillerCode" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctInfoType", propOrder = {
    "billingNo",
    "billNo",
    "billerCode"
})
public class AcctInfoType {

    @XmlElement(name = "BillingNo", required = true)
    protected String billingNo;
    @XmlElement(name = "BillNo", required = true)
    protected String billNo;
    @XmlElement(name = "BillerCode")
    @XmlSchemaType(name = "unsignedByte")
    protected String billerCode;

    /**
     * Gets the value of the billingNo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBillingNo() {
        return billingNo;
    }

    /**
     * Sets the value of the billingNo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBillingNo(String value) {
        this.billingNo = value;
    }

    /**
     * Gets the value of the billNo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBillNo() {
        return billNo;
    }

    /**
     * Sets the value of the billNo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBillNo(String value) {
        this.billNo = value;
    }

    /**
     * Gets the value of the billerCode property.
     *
     */
    public String getBillerCode() {
        return billerCode;
    }

    /**
     * Sets the value of the billerCode property.
     *
     */
    public void setBillerCode(String string) {
        this.billerCode = string;
    }

}
