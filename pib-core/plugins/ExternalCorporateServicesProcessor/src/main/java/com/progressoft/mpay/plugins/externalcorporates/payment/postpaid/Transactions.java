package com.progressoft.mpay.plugins.externalcorporates.payment.postpaid;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"trxInf"})
@XmlRootElement(name = "Transactions")
public class Transactions {

    @XmlElement(name = "TrxInf", required = true)
    protected Transactions.TrxInf trxInf;

    public Transactions.TrxInf getTrxInf() {
        return trxInf;
    }

    public void setTrxInf(Transactions.TrxInf value) {
        this.trxInf = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"acctInfo", "bankTrxID", "pmtStatus", "dueAmt", "paidAmt", "processDate",
            "accessChannel", "paymentMethod", "serviceTypeDetails", "payerInfo"})
    public static class TrxInf {

        @XmlElement(name = "AcctInfo", required = true)
        protected Transactions.TrxInf.AcctInfo acctInfo;
        @XmlElement(name = "BankTrxID")
        protected String bankTrxID;
        @XmlElement(name = "PmtStatus", required = true)
        protected String pmtStatus;
        @XmlElement(name = "DueAmt")
        protected String dueAmt;
        @XmlElement(name = "PaidAmt")
        protected String paidAmt;
        @XmlElement(name = "ProcessDate", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar processDate;
        @XmlElement(name = "AccessChannel", required = true)
        protected String accessChannel;
        @XmlElement(name = "PaymentMethod", required = true)
        protected String paymentMethod;
        @XmlElement(name = "ServiceTypeDetails", required = true)
        protected Transactions.TrxInf.ServiceTypeDetails serviceTypeDetails;
        @XmlElement(name = "PayerInfo", required = true)
        protected TrxInf.PayerInfo payerInfo;

        public TrxInf.PayerInfo getPayerInfo() {
            return payerInfo;
        }

        public void setPayerInfo(TrxInf.PayerInfo payerInfo) {
            this.payerInfo = payerInfo;
        }

        public Transactions.TrxInf.AcctInfo getAcctInfo() {
            return acctInfo;
        }

        public void setAcctInfo(Transactions.TrxInf.AcctInfo value) {
            this.acctInfo = value;
        }

        public String getBankTrxID() {
            return bankTrxID;
        }

        public void setBankTrxID(String value) {
            this.bankTrxID = value;
        }

        public String getPmtStatus() {
            return pmtStatus;
        }

        public void setPmtStatus(String value) {
            this.pmtStatus = value;
        }

        public String getDueAmt() {
            return dueAmt;
        }

        public void setDueAmt(String value) {
            this.dueAmt = value;
        }

        public String getPaidAmt() {
            return paidAmt;
        }

        public void setPaidAmt(String value) {
            this.paidAmt = value;
        }

        public XMLGregorianCalendar getProcessDate() {
            return processDate;
        }

        public void setProcessDate(XMLGregorianCalendar value) {
            this.processDate = value;
        }

        public String getAccessChannel() {
            return accessChannel;
        }

        public void setAccessChannel(String value) {
            this.accessChannel = value;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String value) {
            this.paymentMethod = value;
        }

        public Transactions.TrxInf.ServiceTypeDetails getServiceTypeDetails() {
            return serviceTypeDetails;
        }

        public void setServiceTypeDetails(Transactions.TrxInf.ServiceTypeDetails value) {
            this.serviceTypeDetails = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {"billingNo", "billNo", "billerCode"})
        public static class AcctInfo {

            @XmlElement(name = "BillingNo")
            protected String billingNo;
            @XmlElement(name = "BillNo")
            protected String billNo;
            @XmlElement(name = "BillerCode")
            protected String billerCode;

            public String getBillingNo() {
                return billingNo;
            }

            public void setBillingNo(String value) {
                this.billingNo = value;
            }

            public String getBillNo() {
                return billNo;
            }

            public void setBillNo(String value) {
                this.billNo = value;
            }

            public String getBillerCode() {
                return billerCode;
            }

            public void setBillerCode(String value) {
                this.billerCode = value;
            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {"serviceType"})
        public static class ServiceTypeDetails {

            @XmlElement(name = "ServiceType", required = true)
            protected String serviceType;

            public String getServiceType() {
                return serviceType;
            }

            public void setServiceType(String value) {
                this.serviceType = value;
            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {"idType", "id", "nation", "name", "phone", "address"})
        public static class PayerInfo {
            @XmlElement(name = "IdType", required = true)
            protected String idType;

            @XmlElement(name = "Id", required = true)
            protected String id;

            @XmlElement(name = "Nation", required = true)
            protected String nation;

            @XmlElement(name = "Name", required = false)
            protected String name;

            @XmlElement(name = "Phone", required = false)
            protected String phone;

            @XmlElement(name = "Address", required = false)
            protected String address;

            public String getIdType() {
                return idType;
            }

            public void setIdType(String idType) {
                this.idType = idType;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getNation() {
                return nation;
            }

            public void setNation(String nation) {
                this.nation = nation;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

        }
    }
}