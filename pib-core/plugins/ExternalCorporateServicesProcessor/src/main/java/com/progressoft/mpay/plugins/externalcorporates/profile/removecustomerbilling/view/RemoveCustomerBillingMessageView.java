package com.progressoft.mpay.plugins.externalcorporates.profile.removecustomerbilling.view;

public interface RemoveCustomerBillingMessageView {

	String getPspId();

	String getMessageDate();

	String getBillerCode();

	String getBillingCode();

	String getServiceType();

	String getEfawtercomProfileId();

}