package com.progressoft.mpay.plugins.externalcorporates.payment;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.*;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class PaymentMessageValidator {
    private static final Logger logger = LoggerFactory.getLogger(PaymentMessageValidator.class);

    private PaymentMessageValidator() {

    }

    public static ValidationResult validate(MessageProcessingContext context) {
        logger.debug("Inside Validate ...");
        if (context == null)
            throw new NullArgumentException("context");

        ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
                context.getOperation().getMessageType());
        if (!result.isValid())
            return result;

        result = LanguageValidator.validate(context.getLanguage());
        if (!result.isValid())
            return result;

        PaymentMessage request = (PaymentMessage) context.getRequest();

        if (request.getSenderType().equals(ReceiverInfoType.MOBILE)) {
            result = validateSenderMobile(context);
            if (!result.isValid())
                return result;
        } else if (request.getSenderType().equals(ReceiverInfoType.CORPORATE)) {
            result = validateSenderService(context);
            if (!result.isValid())
                return result;
        }

        result = TransactionConfigValidator.validate(context.getTransactionConfig(),
                BigDecimal.valueOf(Double.valueOf(request.getPaidAmt())));
        if (!result.isValid())
            return result;

        result = AmountValidator.validate(context);
        if (!result.isValid())
            return result;

        return LimitsValidator.validate(context);
    }

    private static ValidationResult validateSenderService(MessageProcessingContext context) {
        ValidationResult result = ServiceValidator.validate(context.getSender().getService(), true);
        if (!result.isValid())
            return result;

        result = CorporateValidator.validate(context.getSender().getCorporate(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getServiceAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(),
                context.getRequest().getDeviceId(), context.getSender().getService().getId()));
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getService(),
                context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        return PinCodeValidator.validate(context, context.getSender().getService(), context.getRequest().getPin());
    }

    private static ValidationResult validateSenderMobile(MessageProcessingContext context) {
        ValidationResult result = MobileValidator.validate(context.getSender().getMobile(), true);
        if (!result.isValid())
            return result;

        result = CustomerValidator.validate(context.getSender().getCustomer(), true);
        if (!result.isValid())
            return result;

        result = AccountValidator.validate(context.getSender().getMobileAccount(), true, context.getMessage().getMessageType().getIsFinancial());
        if (!result.isValid())
            return result;

        result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(),
                context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
        if (!result.isValid())
            return result;

        result = SignInValidator.validate(context, context.getSender().getMobile(), context.getRequest().getDeviceId());
        if (!result.isValid())
            return result;

        return PinCodeValidator.validate(context, context.getSender().getMobile(), context.getRequest().getPin());
    }
}
