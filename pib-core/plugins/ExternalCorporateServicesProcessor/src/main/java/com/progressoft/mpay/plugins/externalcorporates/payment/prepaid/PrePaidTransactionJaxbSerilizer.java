package com.progressoft.mpay.plugins.externalcorporates.payment.prepaid;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

public class PrePaidTransactionJaxbSerilizer implements PrePaidTransactionSerilizer {

	private JAXBContext context = null;

	@Override
	public String serilieze(TrxInf prePaidTransaction) throws JAXBException {
		if(context == null)
			context = JAXBContext.newInstance(TrxInf.class);
		StringWriter writer = new StringWriter();
		Marshaller marshaller = context.createMarshaller();
		marshaller.marshal(prePaidTransaction, writer);
		return writer.toString();
	}

}
