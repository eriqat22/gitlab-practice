package com.progressoft.mpay.plugins.externalcorporates.profile.accountupload.exception;

public class AccountUploadMessageException extends RuntimeException {

	public AccountUploadMessageException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
