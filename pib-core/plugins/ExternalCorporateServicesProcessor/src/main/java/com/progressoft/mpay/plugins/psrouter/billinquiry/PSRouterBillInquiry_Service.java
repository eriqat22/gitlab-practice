package com.progressoft.mpay.plugins.psrouter.billinquiry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import java.net.URL;

@WebServiceClient(name = "PSRouterBillInquiry", wsdlLocation = "PSRouterBillInquiry.wsdl", targetNamespace = "http://www.progressoft.com/PSRouterBillInquiry/")
public class PSRouterBillInquiry_Service extends Service {
	public final static QName SERVICE = new QName("http://www.progressoft.com/PSRouterBillInquiry/",
			"PSRouterBillInquiry");
	public final static QName PSRouterBillInquirySOAP = new QName("http://www.progressoft.com/PSRouterBillInquiry/",
			"PSRouterBillInquirySOAP");
    private static final Logger logger = LoggerFactory.getLogger(PSRouterBillInquiry_Service.class);
    public static URL WSDL_LOCATION;

	static {

		// TODO: change location to be in properties file.
		WSDL_LOCATION = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try {
			URL url = classLoader.getResource("/config/app/wsdl/PSRouterBillInquiry.wsdl");
			if (url == null) {
				java.util.logging.Logger.getLogger(PSRouterBillInquiry_Service.class.getName()).log(
						java.util.logging.Level.INFO, "Can not initialize the default wsdl from {0}",
						"PSRouterBillInquiry.wsdl");
			}
			WSDL_LOCATION = url;
		} catch (Exception e) {
			logger.error("error... change me later", e);
		}

	}

	public PSRouterBillInquiry_Service(URL wsdlLocation) {
		super(wsdlLocation, SERVICE);
	}

	public PSRouterBillInquiry_Service(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public PSRouterBillInquiry_Service() {
		super(WSDL_LOCATION, SERVICE);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterBillInquiry_Service(WebServiceFeature... features) {
		super(WSDL_LOCATION, SERVICE, features);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterBillInquiry_Service(URL wsdlLocation, WebServiceFeature... features) {
		super(wsdlLocation, SERVICE, features);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterBillInquiry_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	/**
	 *
	 * @return returns PSRouterBillInquiry
	 */
	@WebEndpoint(name = "PSRouterBillInquirySOAP")
	public PSRouterBillInquiry getPSRouterBillInquirySOAP() {
		return super.getPort(PSRouterBillInquirySOAP, PSRouterBillInquiry.class);
	}

	/**
	 *
	 * @param features
	 *            A list of {@link WebServiceFeature} to configure
	 *            on the proxy. Supported features not in the
	 *            <code>features</code> parameter will have their default
	 *            values.
	 * @return returns PSRouterBillInquiry
	 */
	@WebEndpoint(name = "PSRouterBillInquirySOAP")
	public PSRouterBillInquiry getPSRouterBillInquirySOAP(WebServiceFeature... features) {
		return super.getPort(PSRouterBillInquirySOAP, PSRouterBillInquiry.class, features);
	}

}
