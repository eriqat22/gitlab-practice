package com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry.processor.exception;

public class AccountInquiryParseMessageException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AccountInquiryParseMessageException(String message) {
		super(message);
	}
}
