package com.progressoft.mpay.plugins.externalcorporates.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EfawateercomGregorianTimeUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(EfawateercomGregorianTimeUtil.class);

    public static XMLGregorianCalendar generateGregorianFormatDate(Date date) {
        GregorianCalendar calendar = perepareGregorianCalender(date);
        return prepareXMLGregorianCalender(calendar);
    }

    private static GregorianCalendar perepareGregorianCalender(Date date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;
    }

    private static XMLGregorianCalendar prepareXMLGregorianCalender(GregorianCalendar calendar) {
        XMLGregorianCalendar gregorianCalendar = null;
        try {
            gregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            gregorianCalendar.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
            gregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        } catch (DatatypeConfigurationException e) {
            LOGGER.error("An Error while parsing gregorian calendar");
        }
        return gregorianCalendar;
    }

}
