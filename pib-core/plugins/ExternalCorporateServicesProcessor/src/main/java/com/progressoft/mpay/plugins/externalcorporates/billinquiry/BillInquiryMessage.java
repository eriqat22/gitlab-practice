package com.progressoft.mpay.plugins.externalcorporates.billinquiry;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

import java.sql.Timestamp;

public class BillInquiryMessage extends MPayRequest {
	public static final String billingNoKey = "billingNo";
	public static final String billerCodeKey = "billerCode";
	public static final String billNoKey = "billNo";
	public static final String serviceTypeKey = "serviceType";
	public static final String incPaymentsKey = "incPayments";
	public static final String incPaidBillsKey = "incPaidBills";
	public static final String startDateKey = "startDate";
	public static final String endDateKey = "endDate";
    private static final String dateFormat = "dd-MM-yyyy hh:mm:ss";
    private String billingNo;
	private String billerCode;
	private String billNo;
	private String serviceType;
	private String incPayments;
	private String incPaidBills;
	private Timestamp startDate;
	private Timestamp endDate;

	public BillInquiryMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static BillInquiryMessage ParseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		BillInquiryMessage message = new BillInquiryMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);

		if (message.getSenderType() == null || (!(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
				&& !(message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

		message.setBillingNo(message.getValue(billingNoKey));
		if (message.getBillingNo() == null || message.getBillingNo().trim().length() == 0)
			throw new MessageParsingException(billingNoKey);

		message.setBillerCode(message.getValue(billerCodeKey));
		if (message.getBillerCode() == null || message.getBillerCode().trim().length() == 0)
			throw new MessageParsingException(billerCodeKey);

		message.setBillNo(message.getValue(billNoKey));
		if (message.getBillNo() == null || message.getBillNo().trim().length() == 0)
			throw new MessageParsingException(billNoKey);

		message.setServiceType(message.getValue(serviceTypeKey));
		if (message.getServiceType() == null || message.getServiceType().trim().length() == 0)
			throw new MessageParsingException(serviceTypeKey);

		message.setIncPayments(message.getValue(incPaymentsKey));
		if (message.getIncPayments() == null)
			throw new MessageParsingException(incPaymentsKey);

		message.setIncPaidBills(message.getValue(incPaidBillsKey));
		if (message.getIncPaidBills() == null)
			throw new MessageParsingException(incPaidBillsKey);

		try {
			if (message.getValue(startDateKey) != null)
				message.setStartDate(
						new Timestamp(SystemHelper.parseDate(message.getValue(startDateKey), dateFormat).getTime()));
		} catch (Exception ex) {
			throw new MessageParsingException(startDateKey);
		}
		try {
			if (message.getValue(endDateKey) != null)
				message.setEndDate(
						new Timestamp(SystemHelper.parseDate(message.getValue(endDateKey), dateFormat).getTime()));
		} catch (Exception ex) {
			throw new MessageParsingException(endDateKey);
		}

		return message;
    }

    public String getBillingNo() {
        return billingNo;
    }

    public void setBillingNo(String billingNo) {
        this.billingNo = billingNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getIncPayments() {
        return incPayments;
    }

    public void setIncPayments(String incPayments) {
        this.incPayments = incPayments;
    }

    public String getIncPaidBills() {
        return incPaidBills;
    }

    public void setIncPaidBills(String incPaidBills) {
        this.incPaidBills = incPaidBills;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }
}
