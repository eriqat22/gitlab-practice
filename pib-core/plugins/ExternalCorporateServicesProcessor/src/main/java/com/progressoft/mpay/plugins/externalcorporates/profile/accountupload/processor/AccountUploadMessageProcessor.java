package com.progressoft.mpay.plugins.externalcorporates.profile.accountupload.processor;

import com.google.gson.Gson;
import com.progressoft.accountinquriy.model.CustIdTypeModel.IdType;
import com.progressoft.accountupload.client.AccountUploadClient;
import com.progressoft.accountupload.converter.AccountUploadConverter;
import com.progressoft.accountupload.model.AccountUploadRequestModel;
import com.progressoft.accountupload.model.AcctTypeModel;
import com.progressoft.accountupload.model.AcctsTypeModel;
import com.progressoft.accountupload.model.MessageBodyTypeModel;
import com.progressoft.accountupload.request.AccountUploadRequest;
import com.progressoft.accountupload.request.AcctType;
import com.progressoft.accountupload.request.AcctsType;
import com.progressoft.accountupload.request.MessageBodyType;
import com.progressoft.accountupload.response.PSRouterResponse;
import com.progressoft.accountupload.response.PSRouterResponse.ResBody.MsgBody.Accts;
import com.progressoft.accountupload.response.PSRouterResponse.ResBody.MsgBody.Accts.Acct;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.externalcorporates.common.Constants;
import com.progressoft.mpay.plugins.externalcorporates.common.ExternalCorporatesValidator;
import com.progressoft.mpay.plugins.externalcorporates.profile.accountupload.AccountUploadMessage;
import com.progressoft.mpay.plugins.externalcorporates.profile.accountupload.processor.exception.AccountUploadParseMessageException;
import org.apache.commons.lang.NullArgumentException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AccountUploadMessageProcessor implements MessageProcessor {

    private static final String MESSAGE_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String SUCCESS_CODE = "0";

    private static JAXBContext JAXB_CONTEXT_INSTANCE;

    private XMLGregorianCalendar generateGregorienDate = null;

    private static XMLGregorianCalendar generateGregorienDate() {
        try {
            SimpleDateFormat gregorianFormat = new SimpleDateFormat(MESSAGE_DATE_TIME_FORMAT);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianFormat.format(new Date()));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        try {
            preProcessMessage(context);
            return getResultAccordingToValidation(context, ExternalCorporatesValidator.validate(context));
        } catch (Exception exception) {
            return rejectMpayMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, exception.getMessage());
        }
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        if (context == null)
            throw new NullArgumentException("messagaccountUploadRequestModeleContext");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }

    private MessageProcessingResult getResultAccordingToValidation(MessageProcessingContext context,
                                                                   ValidationResult validationResult) {
        return validationResult.isValid() ? acceptMessage(context)
                : rejectMpayMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        AccountUploadRequestModel accountUploadRequestModel = prepareRequestModel(context);
        AccountUploadRequest uploadAccountRequest = AccountUploadConverter.getInstance()
                .toUploadAccountRequest(accountUploadRequestModel);
        MPAY_ExternalIntegMessage integrationMessage = createExternalIntegrationMessage(uploadAccountRequest, context);

        String accUploadResponse = AccountUploadClient.sendAccountUploadRequest(accountUploadRequestModel)
                .getAccUploadResponse();
        if (accUploadResponse.contains("MPAY-"))
            return rejectMpayMessage(context, ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE,
                    "Service Currently Unavilable");
        PSRouterResponse response = PSRouterResponse
                .toObject(accUploadResponse.replaceAll("&lt;", "<").replaceAll("&gt;", ">"));
        return reflectStatusToExternalMessage(context, integrationMessage, response, accUploadResponse);
    }

    private MessageProcessingResult reflectStatusToExternalMessage(MessageProcessingContext context,
                                                                   MPAY_ExternalIntegMessage integrationMessage, PSRouterResponse response, String responseContent) {
        integrationMessage.setResponseContent(responseContent);

        if (SUCCESS_CODE.equals(response.getResCode())) {
            if (!isVerifiedRouterSignature(context, generateSigningValues(response), response.getResSignature())) {
                integrationMessage
                        .setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED));
                context.getDataProvider().updateExternalIntegrationMessage(integrationMessage);
                return rejectMpayMessage(context, ReasonCodes.INVALID_TOKEN, "Invalid Signature from router");
            }
            updateIntegrationMessage(context, integrationMessage, ProcessingStatusCodes.ACCEPTED);
            updateCustomerData(context, response);
            return acceptMpayMessage(context, response);
        } else {
            updateIntegrationMessage(context, integrationMessage, ProcessingStatusCodes.REJECTED);
            return rejectMpayMessage(context, ReasonCodes.SERVICE_INTEGRATION_REJECTION, response.getResDesc());
        }
    }

    private void updateCustomerData(MessageProcessingContext context, PSRouterResponse response) {
        Acct acct = response.getResBody().getMsgBody().getAccts().getAcct().get(0);
        MPAY_Customer customer = context.getDataProvider()
                .getCustomer(IdType.getIdTypeByRouterCode(acct.getIdType()).description(), acct.getId());
        customer.setClientRef(acct.getJOEBPPSNo());
        context.getDataProvider().updateCustomer(customer);
    }

    private void updateIntegrationMessage(MessageProcessingContext context,
                                          MPAY_ExternalIntegMessage integrationMessage, String processingStatusCodes) {
        integrationMessage.setRefStatus(context.getLookupsLoader().getProcessingStatus(processingStatusCodes));
        context.getDataProvider().updateExternalIntegrationMessage(integrationMessage);
    }

    private MessageProcessingResult rejectMpayMessage(MessageProcessingContext context, String reasonCode,
                                                      String reasonDescription) {
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
                ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult acceptMpayMessage(MessageProcessingContext context, PSRouterResponse response) {
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, new Gson().toJson(response),
                ProcessingStatusCodes.ACCEPTED);
        result.setResponse(generateResponse(context, true));
        return result;
    }

    private String generateSigningValues(PSRouterResponse response) {
        return response.getOriginaleMsgId() + response.getResDateTime().toString() + response.getResCode()
                + response.getResDesc() + generateAccountsData(response.getResBody().getMsgBody().getAccts());
    }

    private String generateAccountsData(Accts accts) {
        StringBuilder buffer = new StringBuilder();
        for (Acct account : accts.getAcct()) {
            buffer.append(account.getIdType()).append(account.getId()).append(account.getNation())
                    .append(account.getResult().getErrorCode()).append(account.getResult().getErrorDesc())
                    .append(account.getResult().getSeverity()).append(account.getJOEBPPSNo());
        }
        return buffer.toString();
    }

    private boolean isVerifiedRouterSignature(MessageProcessingContext context, String accUploadResponse,
                                              String resSignature) {
        return context.getCryptographer().verifyToken(context.getSystemParameters(), accUploadResponse, resSignature,
                null, IntegMessagesSource.E_FAWATERCOM);
    }

    private MPAY_ExternalIntegMessage createExternalIntegrationMessage(AccountUploadRequest request,
                                                                       MessageProcessingContext context) {
        MPAY_ExternalIntegMessage integrationMessage = new MPAY_ExternalIntegMessage();
        integrationMessage.setRequestContent(createXmlContent(request));
        integrationMessage.setRequestDate(SystemHelper.getSystemTimestamp());
        integrationMessage.setRequestToken(request.getPspSignature());
        integrationMessage.setRequestID(request.getMessageId());
        integrationMessage.setReqField1(request.getPspId());
        integrationMessage.setReqField2(Constants.PS_ROUTER_NAME);
        integrationMessage.setRefMessage(context.getMessage());
        integrationMessage.setRefService(context.getReceiver().getService());
        integrationMessage.setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
        context.getDataProvider().persistExternalIntegrationMessage(integrationMessage);
        return integrationMessage;
    }

    private String createXmlContent(AccountUploadRequest request) {
        try {
            if (JAXB_CONTEXT_INSTANCE == null)
                JAXB_CONTEXT_INSTANCE = JAXBContext.newInstance(AccountUploadRequest.class, MessageBodyType.class,
                        AcctsType.class, AcctType.class);
            StringWriter writer = new StringWriter();
            JAXB_CONTEXT_INSTANCE.createMarshaller().marshal(request, writer);
            return writer.toString();

        } catch (JAXBException e) {
            throw new AccountUploadParseMessageException("Error while persisting the external message");
        }
    }

    private AccountUploadRequestModel prepareRequestModel(MessageProcessingContext context) {
        return new AccountUploadRequestModel.Builder().messageId(context.getRequest().getMsgId())
                .pspId(((AccountUploadMessage) context.getRequest()).getPspId()).messageDateTime(generateGregorienDate)
                .messageBodyTypeModel(generateMessageBodyType(context)).pspSignature(genratePspSignature(context))
                .build();
    }

    private String genratePspSignature(MessageProcessingContext context) {
        StringBuilder builder = new StringBuilder();
        AccountUploadMessage accountUploadMessage = (AccountUploadMessage) context.getRequest();
        builder.append(accountUploadMessage.getPspId()).append(accountUploadMessage.getMsgId())
                .append(generateGregorienDate)
                .append(IdType.getRouterCodeByIdType(context.getSender().getCustomer().getIdType().getCode()))
                .append(context.getSender().getMobile().getRefCustomer().getIdNum())
                .append(context.getSender().getMobile().getRefCustomer().getNationality().getDescription())
                .append(accountUploadMessage.getName()).append(accountUploadMessage.getPhone())
                .append(accountUploadMessage.getAddress()).append(accountUploadMessage.getEmail());
        return context.getCryptographer().generateToken(context.getSystemParameters(), builder.toString(), "",
                IntegMessagesSource.SYSTEM);
    }

    private MessageBodyTypeModel generateMessageBodyType(MessageProcessingContext context) {
        return new MessageBodyTypeModel.Builder().acctsTypeModel(generateAccountsTypeModel(context)).build();
    }

    private AcctsTypeModel generateAccountsTypeModel(MessageProcessingContext context) {
        return new AcctsTypeModel.Builder().acctTypeModels(generateListOfAccountTypeModel(context)).build();
    }

    private List<AcctTypeModel> generateListOfAccountTypeModel(MessageProcessingContext context) {
        List<AcctTypeModel> accountsTypeModel = new ArrayList<>();
        accountsTypeModel.add(new AcctTypeModel.Builder().id(context.getSender().getCustomer().getIdNum())
                .idType(IdType.getRouterCodeByIdType(context.getSender().getCustomer().getIdType().getCode()).name())
                .nation(context.getSender().getCustomer().getNationality().getDescription()).build());

        return accountsTypeModel;
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                String reasonDescription, String processingStatus) {
        MPAY_ProcessingStatus status = prepareMessageContext(context, reasonCode, reasonDescription, processingStatus);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private MPAY_ProcessingStatus prepareMessageContext(MessageProcessingContext context, String reasonCode,
                                                        String reasonDescription, String processingStatus) {
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        return status;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    SystemParameters.getInstance().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        return response.toString();
    }

    private void preProcessMessage(MessageProcessingContext context) {
        generateGregorienDate = generateGregorienDate();
        AccountUploadMessage accountUploadMessage = AccountUploadMessage.parseMessage(context.getRequest());
        context.setRequest(accountUploadMessage);
        ProcessingContextSide sender = new ProcessingContextSide();

        String senderType = accountUploadMessage.getSenderType();

        if (senderType.equals(ReceiverInfoType.MOBILE)) {
            sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
            if (sender.getMobile() == null)
                return;
            context.setSender(sender);
            context.setReceiver(generateReciverData(context));
            sender.setCustomer(sender.getMobile().getRefCustomer());
            sender.setMobileAccount(
                    SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
            if (sender.getMobileAccount() == null)
                return;
            sender.setProfile(sender.getMobileAccount().getRefProfile());
        } else if (senderType.equals(ReceiverInfoType.CORPORATE)) {

            sender.setService(context.getDataProvider().getCorporateService(accountUploadMessage.getSender(),
                    accountUploadMessage.getSenderType()));
            if (sender.getService() == null)
                return;
            context.setSender(sender);
            sender.setCorporate(sender.getService().getRefCorporate());
            sender.setServiceAccount(
                    SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
            if (sender.getServiceAccount() == null)
                return;
            sender.setProfile(sender.getServiceAccount().getRefProfile());
        }
    }

    private ProcessingContextSide generateReciverData(MessageProcessingContext context) {
        return new ProcessingContextSide();
    }
}