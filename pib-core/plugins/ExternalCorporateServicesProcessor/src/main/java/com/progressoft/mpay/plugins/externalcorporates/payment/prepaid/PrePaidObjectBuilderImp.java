package com.progressoft.mpay.plugins.externalcorporates.payment.prepaid;

import com.opensymphony.util.GUID;
import com.progressoft.mpay.plugins.externalcorporates.common.EfawateercomGregorianTimeUtil;
import com.progressoft.mpay.plugins.externalcorporates.payment.PaymentMessage;
import com.progressoft.mpay.plugins.externalcorporates.payment.prepaid.TrxInf.AcctInfo;
import com.progressoft.mpay.plugins.externalcorporates.payment.prepaid.TrxInf.PayerInfo;
import com.progressoft.mpay.plugins.externalcorporates.payment.prepaid.TrxInf.ServiceTypeDetails;

public class PrePaidObjectBuilderImp implements PrePaidObjectBuilder {

	private static final String ACCESS_CHANNEL = "PSP";
	private static final String PAYMENT_STATUS = "PmtNew";
	private static final String PAYMENT_METHOD = "ACTDEB";

	@Override
	public TrxInf buildPostPaidTransaction(PaymentMessage message) {
		return preparePrePaidTransactionObject(message);
	}

	private TrxInf preparePrePaidTransactionObject(PaymentMessage message) {
		TrxInf transactionInformation = new TrxInf();
		transactionInformation.setAcctInfo(prepareAccountInfo(message));
		transactionInformation.setAccessChannel(ACCESS_CHANNEL);
		transactionInformation.setPmtStatus(PAYMENT_STATUS);
		transactionInformation.setBankTrxID(GUID.generateGUID());
		transactionInformation.setDueAmt(message.getDueAmt());
		transactionInformation.setPaidAmt(message.getPaidAmt());
		transactionInformation.setValidationCode(message.getValidationCode());
		transactionInformation.setPaymentMethod(PAYMENT_METHOD);
		transactionInformation.setProcessDate(EfawateercomGregorianTimeUtil.generateGregorianFormatDate(message.getProcessDate()));
		transactionInformation.setServiceTypeDetails(prepareSreviceTypeDetails(message));
		transactionInformation.setPayerInfo(generatePayerInfo(message));
		return transactionInformation;
	}

	private AcctInfo prepareAccountInfo(PaymentMessage message) {
		AcctInfo accountInfo = new AcctInfo();
		accountInfo.setBillerCode(message.getBillerCode());
		accountInfo.setBillingNo(message.getBillingNo());
		return accountInfo;
	}

	private ServiceTypeDetails prepareSreviceTypeDetails(PaymentMessage message) {
		ServiceTypeDetails serviceTypeDetails = new ServiceTypeDetails();
		serviceTypeDetails.setServiceType(message.getServiceType());
		serviceTypeDetails.setPrepaidCat(message.getPrepaidCat());
		return serviceTypeDetails;
	}

	private PayerInfo generatePayerInfo(PaymentMessage message) {
		PayerInfo info = new PayerInfo();
		info.setId(message.getIdNUmber());
		info.setIdType(message.getIdType());
		info.setNation(message.getNation());
		return info;
	}
}