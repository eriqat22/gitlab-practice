package com.progressoft.mpay.plugins.externalcorporates.payment.postpaid;

import com.progressoft.mpay.plugins.externalcorporates.payment.PaymentMessage;

public interface PostPaidObjectBuilder {
	public Transactions buildPostPaidTransaction(PaymentMessage message);
}
