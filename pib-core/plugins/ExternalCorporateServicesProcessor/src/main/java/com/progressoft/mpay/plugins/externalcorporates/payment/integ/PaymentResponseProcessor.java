package com.progressoft.mpay.plugins.externalcorporates.payment.integ;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.externalcorporates.payment.NotificationProcessor;
import com.progressoft.mpay.plugins.externalcorporates.payment.PaymentMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class PaymentResponseProcessor extends IntegrationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(PaymentResponseProcessor.class);

    @Override
    public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
        try {
            logger.debug("Inside ProcessIntegration ...");
            preProcessIntegration(context);
            if (MPClearReasons.RECEIPT_CONFIRMATION.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return null;
            if (MPClearReasons.SUCCESS.equals(context.getMpClearIsoMessage().getField(44).getValue()))
                return accept(context);
            else
                return reject(context);
        } catch (Exception ex) {
            logger.error("Error in ProcessIntegration", ex);
            return reject(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
        logger.debug("Inside PreProcessIntegration ...");
        if (context.getMessage() != null)
            context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        PaymentMessage request = null;
        try {
            request = PaymentMessage.ParseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
        } catch (MessageParsingException ex) {
            logger.error("Error while parsing message", ex);
            throw new WorkflowException(ex);
        }
        receiver.setInfo(request.getServiceType());
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setAmount(BigDecimal.valueOf(Double.valueOf(request.getPaidAmt())));
        if (request.getSenderType().equalsIgnoreCase(ReceiverInfoType.MOBILE)) {
            sender.setMobile(context.getTransaction().getSenderMobile());
            sender.setCustomer(sender.getMobile().getRefCustomer());
            sender.setMobileAccount(context.getTransaction().getSenderMobileAccount());
            sender.setAccount(sender.getMobileAccount().getRefAccount());
            sender.setBanked(sender.getMobile().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
            sender.setInfo(sender.getMobile().getMobileNumber());
            sender.setProfile(sender.getMobileAccount().getRefProfile());

        } else if (request.getSenderType().equalsIgnoreCase(ReceiverInfoType.CORPORATE)) {
            sender.setService(context.getTransaction().getSenderService());
            sender.setCorporate(sender.getService().getRefCorporate());
            sender.setServiceAccount(context.getTransaction().getSenderServiceAccount());
            sender.setAccount(sender.getServiceAccount().getRefAccount());
            sender.setBanked(sender.getService().getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
            sender.setInfo(sender.getService().getName());
            sender.setProfile(sender.getServiceAccount().getRefProfile());
        }
        sender.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(), sender.getAccount().getAccNumber()));
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));


        receiver.setService(context.getTransaction().getReceiverService());
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();

        if (receiver.getService() == null) {
            receiver.setInfo(request.getServiceType());
            context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
                    sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
                    context.getOperation().getMessageType().getId(), context.getTransactionNature(),
                    context.getOperation().getId()));
            return;
        }
        receiver.setServiceAccount(context.getTransaction().getReceiverServiceAccount());
        receiver.setAccount(receiver.getServiceAccount().getRefAccount());
        receiver.setCorporate(receiver.getService().getRefCorporate());
        receiver.setBank(receiver.getService().getBank());
        receiver.setBanked(receiver.getService().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
        receiver.setCharge(TransactionHelper.getAccountDeductedChargeValue(context.getTransaction(),
                receiver.getAccount().getAccNumber()));
        receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));

        context.setDirection(context.getTransaction().getDirection().getId());
        context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
                sender.isBanked(), receiver.getCorporate().getClientType().getId(), receiver.isBanked(),
                context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT));

    }

    private IntegrationProcessingResult accept(IntegrationProcessingContext context) {
        logger.debug("Inside Accept ...");
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED,
                ReasonCodes.VALID, null, null, false);
        result.setNotifications(NotificationProcessor.CreateAcceptanceNotificationMessages(context));
        return result;
    }

    private IntegrationProcessingResult reject(IntegrationProcessingContext context) {
        logger.debug("Inside Reject ...");
        BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
        if (integrationResult != null)
            TransactionHelper.reverseTransaction(context);
        String reasonDescription = null;
        if (context.getMpClearIsoMessage().getField(47) != null)
            reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED,
                ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
                context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
        result.setNotifications(NotificationProcessor.CreateRejectionNotificationMessages(context));
        return result;
    }

    private IntegrationProcessingResult reject(IntegrationProcessingContext context, String processingCode,
                                               String reasonCode, String reasonDescription) {
        logger.debug("Inside Reject ...");
        BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
        if (integrationResult != null)
            TransactionHelper.reverseTransaction(context);
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, processingCode, reasonCode,
                reasonDescription, null, false);
        result.setNotifications(NotificationProcessor.CreateRejectionNotificationMessages(context));
        return result;
    }
}
