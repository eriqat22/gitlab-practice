package com.progressoft.mpay.plugins.externalcorporates.profile.addcustomerbilling.processor.exception;

public class AddCustomerBillingParseMessageException extends RuntimeException{

	public AddCustomerBillingParseMessageException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
