package com.progressoft.mpay.plugins.externalcorporates.profile.accountupload;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.plugins.externalcorporates.profile.accountupload.view.AccountUploadMessageView;

public class AccountUploadMessage extends MPayRequest implements AccountUploadMessageView {

	private String messageDate;
	private String idType;
	private String id;
	private String nation;
	private String phone;
	private String address;
	private String email;
	private String name;
	private String pspId;

	public AccountUploadMessage(MPayRequest request) {
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setTenant(request.getTenant());
		setExtraData(request.getExtraData());
	}

	@Override
	public String getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}

	@Override
	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	@Override
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getPspId() {
		return pspId;
	}

	public void setPspId(String pspId) {
		this.pspId = pspId;
	}

	public static AccountUploadMessage parseMessage(MPayRequest request) {
		AccountUploadMessage accountMessage = new AccountUploadMessage(request);
		accountMessage.setPspId(LookupsLoader.getInstance().getPSP().getNationalID());
		return accountMessage;

	}
}