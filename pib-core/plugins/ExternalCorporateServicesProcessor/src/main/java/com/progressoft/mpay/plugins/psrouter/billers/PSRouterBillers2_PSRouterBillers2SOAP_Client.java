package com.progressoft.mpay.plugins.psrouter.billers;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import com.progressoft.jfw.model.service.utils.AppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import java.io.IOException;
import java.net.URL;

/**
 * This class was generated by Apache CXF 2.7.3 2016-06-13T14:37:47.207+03:00
 * Generated source version: 2.7.3
 *
 */
public final class PSRouterBillers2_PSRouterBillers2SOAP_Client {
	private static final QName SERVICE_NAME = new QName("http://www.progressoft.com/PSRouterBillers2/", "PSRouterBillers2");
	private static final Logger logger = LoggerFactory
			.getLogger(PSRouterBillers2_PSRouterBillers2SOAP_Client.class);
    PSRouterBillers2 port;

	public PSRouterBillers2_PSRouterBillers2SOAP_Client() {
	}

	public PSRouterBillers2 InitClient() {
		URL wsdlURL = PSRouterBillers2_Service.WSDL_LOCATION;
		logger.info("PSRouterBillers2_Service WSDL location ",
				PSRouterBillers2_Service.WSDL_LOCATION.toString());

		try {
			wsdlURL = AppContext
					.getApplicationContext()
					.getResource(
							"classpath:config/app/wsdl/PSRouterBillers2.wsdl")
					.getURL();
			PSRouterBillers2_Service ss = new PSRouterBillers2_Service(wsdlURL,
					SERVICE_NAME);
			port = ss.getPSRouterBillers2SOAP();
			return port;
			// PSRouterBillers2 port = ss.getPSRouterBillers2SOAP();
		} catch (IOException e) {
			logger.error("Error while initiation PSRouterBillers Client ", e);
		}
		return null;

	}

//	 public static void main(String args[]) throws java.lang.Exception {
//	 URL wsdlURL = PSRouterBillers2_Service.WSDL_LOCATION;
//	 if (args.length > 0 && args[0] != null && !"".equals(args[0])) {
//	 File wsdlFile = new File(args[0]);
//	 try {
//	 if (wsdlFile.exists()) {
//	 wsdlURL = wsdlFile.toURI().toURL();
//	 } else {
//	 wsdlURL = new URL(args[0]);
//	 }
//	 } catch (MalformedURLException e) {
//	 e.printStackTrace();
//	 }
//	 }
//
//	 PSRouterBillers2_Service ss = new PSRouterBillers2_Service(wsdlURL,
//	 SERVICE_NAME);
//	 PSRouterBillers2 port = ss.getPSRouterBillers2SOAP();
//
//	 {
//	 System.out.println("Invoking getBillers...");
//	 java.lang.String _getBillers_pspId = "";
//	 java.lang.String _getBillers_pspSignature = "";
//	 java.lang.String _getBillers_messageId = "";
//	 javax.xml.datatype.XMLGregorianCalendar _getBillers_messageDateTime =
//	 null;
//	 com.progressoft.psrouterbillers2.MessageBodyType _getBillers_messageBody
//	 = null;
//	 java.lang.String _getBillers__return = port.getBillers(_getBillers_pspId,
//	 _getBillers_pspSignature, _getBillers_messageId,
//	 _getBillers_messageDateTime, _getBillers_messageBody);
//	 System.out.println("getBillers.result=" + _getBillers__return);
//
//
//	 }
//
//	 System.exit(0);
//	 }

}
