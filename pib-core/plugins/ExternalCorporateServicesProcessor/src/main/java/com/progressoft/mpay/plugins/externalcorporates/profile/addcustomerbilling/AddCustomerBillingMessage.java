package com.progressoft.mpay.plugins.externalcorporates.profile.addcustomerbilling;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.plugins.externalcorporates.profile.addcustomerbilling.view.AddCustomerBillingMessageView;

public class AddCustomerBillingMessage extends MPayRequest implements AddCustomerBillingMessageView {

	private String pspId;
	private String messageDate;
	private String billerCode;
	private String billingCode;
	private String serviceType;
	private String efawtercomProfileId;

	public AddCustomerBillingMessage(MPayRequest request) {
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setTenant(request.getTenant());
		setExtraData(request.getExtraData());
	}

	@Override
	public String getPspId() {
		return pspId;
	}

	public void setPspId(String pspId) {
		this.pspId = pspId;
	}

	@Override
	public String getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(String messageDate) {
		this.messageDate = messageDate;
	}

	@Override
	public String getBillerCode() {
		return billerCode;
	}

	public void setBillerCode(String billerCode) {
		this.billerCode = billerCode;
	}

	@Override
	public String getBillingCode() {
		return billingCode;
	}

	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}

	@Override
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	@Override
	public String getEfawtercomProfileId() {
		return efawtercomProfileId;
	}

	public void setEfawtercomProfileId(String efawtercomProfileId) {
		this.efawtercomProfileId = efawtercomProfileId;
	}

	public static AddCustomerBillingMessage parseMessage(MPayRequest request) {
		AddCustomerBillingMessage message = new AddCustomerBillingMessage(request);
		message.setEfawtercomProfileId("0");
		message.setPspId(LookupsLoader.getInstance().getPSP().getNationalID());
		message.setBillerCode(message.getValue("billerCode"));
		message.setBillingCode(message.getValue("billingNum"));
		message.setServiceType(message.getValue("serviceType"));
		return message;
	}

}
