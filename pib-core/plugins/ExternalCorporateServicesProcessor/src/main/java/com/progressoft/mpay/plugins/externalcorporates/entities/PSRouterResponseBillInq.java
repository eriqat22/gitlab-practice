package com.progressoft.mpay.plugins.externalcorporates.entities;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"originaleMsgId", "resDateTime", "resCode", "resDesc", "resBody", "resSignature"})
@XmlRootElement(name = "PSRouterResponse")
public class PSRouterResponseBillInq {

    @XmlElement(required = true)
    protected String originaleMsgId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar resDateTime;
    protected String resCode;
    @XmlElement(required = true)
    protected String resDesc;
    @XmlElement(required = true)
    protected PSRouterResponseBillInq.ResBody resBody;
    @XmlElement(required = true)
    protected String resSignature;

    public String getOriginaleMsgId() {
        return originaleMsgId;
    }

    public void setOriginaleMsgId(String value) {
        this.originaleMsgId = value;
    }

    public XMLGregorianCalendar getResDateTime() {
        return resDateTime;
    }

    public void setResDateTime(XMLGregorianCalendar value) {
        this.resDateTime = value;
    }

    public String getResCode() {
        return resCode;
    }

    public void setResCode(String value) {
        this.resCode = value;
    }

    public String getResDesc() {
        return resDesc;
    }

    public void setResDesc(String value) {
        this.resDesc = value;
    }

    public PSRouterResponseBillInq.ResBody getResBody() {
        return resBody;
    }

    public void setResBody(PSRouterResponseBillInq.ResBody value) {
        this.resBody = value;
    }

    public String getResSignature() {
        return resSignature;
    }

    public void setResSignature(String value) {
        this.resSignature = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"msgBody"})
    public static class ResBody {

        @XmlElement(name = "MsgBody", required = true)
        protected PSRouterResponseBillInq.ResBody.MsgBody msgBody;

        public PSRouterResponseBillInq.ResBody.MsgBody getMsgBody() {
            return msgBody;
        }

        public void setMsgBody(PSRouterResponseBillInq.ResBody.MsgBody value) {
            this.msgBody = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {"recCount", "billsRec"})
        public static class MsgBody {

            @XmlElement(name = "RecCount")
            protected byte recCount;
            @XmlElement(name = "BillsRec", required = true)
            protected PSRouterResponseBillInq.ResBody.MsgBody.BillsRec billsRec;

            public byte getRecCount() {
                return recCount;
            }

            public void setRecCount(byte value) {
                this.recCount = value;
            }

            public PSRouterResponseBillInq.ResBody.MsgBody.BillsRec getBillsRec() {
                return billsRec;
            }

            public void setBillsRec(PSRouterResponseBillInq.ResBody.MsgBody.BillsRec value) {
                this.billsRec = value;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {"billRec"})
            public static class BillsRec {

                @XmlElement(name = "BillRec", required = true)
                protected PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec billRec;

                public PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec getBillRec() {
                    return billRec;
                }

                public void setBillRec(PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec value) {
                    this.billRec = value;
                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {"acctInfo", "billStatus", "dueAmount", "feesAmt", "issueDate",
                        "dueDate", "serviceType", "pmtConst"})
                public static class BillRec {

                    @XmlElement(name = "AcctInfo", required = true)
                    protected PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec.AcctInfo acctInfo;
                    @XmlElement(name = "BillStatus", required = true)
                    protected String billStatus;
                    @XmlElement(name = "DueAmount")
                    protected float dueAmount;
                    @XmlElement(name = "FeesAmt")
                    protected float feesAmt;
                    @XmlElement(name = "IssueDate", required = true)
                    @XmlSchemaType(name = "dateTime")
                    protected XMLGregorianCalendar issueDate;
                    @XmlElement(name = "DueDate", required = true)
                    @XmlSchemaType(name = "dateTime")
                    protected XMLGregorianCalendar dueDate;
                    @XmlElement(name = "ServiceType", required = true)
                    protected String serviceType;
                    @XmlElement(name = "PmtConst", required = true)
                    protected PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec.PmtConst pmtConst;

                    public PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec.AcctInfo getAcctInfo() {
                        return acctInfo;
                    }

                    public void setAcctInfo(PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec.AcctInfo value) {
                        this.acctInfo = value;
                    }

                    public String getBillStatus() {
                        return billStatus;
                    }

                    public void setBillStatus(String value) {
                        this.billStatus = value;
                    }

                    public float getDueAmount() {
                        return dueAmount;
                    }

                    public void setDueAmount(float value) {
                        this.dueAmount = value;
                    }

                    public float getFeesAmt() {
                        return feesAmt;
                    }

                    public void setFeesAmt(float value) {
                        this.feesAmt = value;
                    }

                    public XMLGregorianCalendar getIssueDate() {
                        return issueDate;
                    }

                    public void setIssueDate(XMLGregorianCalendar value) {
                        this.issueDate = value;
                    }

                    public XMLGregorianCalendar getDueDate() {
                        return dueDate;
                    }

                    public void setDueDate(XMLGregorianCalendar value) {
                        this.dueDate = value;
                    }

                    public String getServiceType() {
                        return serviceType;
                    }

                    public void setServiceType(String value) {
                        this.serviceType = value;
                    }

                    public PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec.PmtConst getPmtConst() {
                        return pmtConst;
                    }

                    public void setPmtConst(PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec.PmtConst value) {
                        this.pmtConst = value;
                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {"billingNo", "billNo", "billerCode"})
                    public static class AcctInfo {

                        @XmlElement(name = "BillingNo")
                        protected String billingNo;
                        @XmlElement(name = "BillNo")
                        protected String billNo;
                        @XmlElement(name = "BillerCode")
                        protected byte billerCode;

                        public String getBillingNo() {
                            return billingNo;
                        }

                        public void setBillingNo(String value) {
                            this.billingNo = value;
                        }

                        public String getBillNo() {
                            return billNo;
                        }

                        public void setBillNo(String value) {
                            this.billNo = value;
                        }

                        public byte getBillerCode() {
                            return billerCode;
                        }

                        public void setBillerCode(byte value) {
                            this.billerCode = value;
                        }

                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {"allowPart", "lower", "upper"})
                    public static class PmtConst {

                        @XmlElement(name = "AllowPart", required = true)
                        protected String allowPart;
                        @XmlElement(name = "Lower")
                        protected float lower;
                        @XmlElement(name = "Upper")
                        protected float upper;

                        public String getAllowPart() {
                            return allowPart;
                        }

                        public void setAllowPart(String value) {
                            this.allowPart = value;
                        }

                        public float getLower() {
                            return lower;
                        }

                        public void setLower(float value) {
                            this.lower = value;
                        }

                        public float getUpper() {
                            return upper;
                        }

                        public void setUpper(float value) {
                            this.upper = value;
                        }
                    }
                }
            }
        }
    }
}