
package com.progressoft.mpay.plugins.psrouter.prepaidvalidation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for messageBodyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="messageBodyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingInfo" type="{http://www.progressoft.com/PSRouterPrepaidValidation/}BillingInfoType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "messageBodyType", propOrder = {
    "billingInfo"
})
public class MessageBodyType {

    @XmlElement(name = "BillingInfo", required = true)
    protected BillingInfoType billingInfo;

    /**
     * Gets the value of the billingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BillingInfoType }
     *     
     */
    public BillingInfoType getBillingInfo() {
        return billingInfo;
    }

    /**
     * Sets the value of the billingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingInfoType }
     *     
     */
    public void setBillingInfo(BillingInfoType value) {
        this.billingInfo = value;
    }

}
