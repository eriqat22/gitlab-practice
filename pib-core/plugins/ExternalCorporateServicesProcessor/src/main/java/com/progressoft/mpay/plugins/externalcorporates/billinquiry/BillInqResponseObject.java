package com.progressoft.mpay.plugins.externalcorporates.billinquiry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class BillInqResponseObject {
	private String billingNo;
	private String billNo;
	private String billerCode;
	private String billStatus;
	private String dueAmnt;
	private String feesAmnt;
	private String issueDate;
	private String dueDate;
	private String serviceType;
	private Boolean allowPart;
	private String lower;
	private String upper;
    private ArrayList<BillPmt> billPmts;

	public String getBillingNo() {
		return billingNo;
	}

	public void setBillingNo(String billingNo) {
		this.billingNo = billingNo;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getBillerCode() {
		return billerCode;
	}

	public void setBillerCode(String billerCode) {
		this.billerCode = billerCode;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public String getDueAmnt() {
		return dueAmnt;
	}

	public void setDueAmnt(String dueAmnt) {
		this.dueAmnt = dueAmnt;
	}

	public String getFeesAmnt() {
		return feesAmnt;
	}

	public void setFeesAmnt(String feesAmnt) {
		this.feesAmnt = feesAmnt;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Boolean getAllowPart() {
		return allowPart;
	}

	public void setAllowPart(Boolean allowPart) {
		this.allowPart = allowPart;
	}

	public String getLower() {
		return lower;
	}

	public void setLower(String lower) {
		this.lower = lower;
	}

	public String getUpper() {
		return upper;
	}

	public void setUpper(String upper) {
		this.upper = upper;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
        String json = gson.toJson(this);
        return json;
    }

    public ArrayList<BillPmt> getBillPmts() {
        return billPmts;
	}

    public void setBillPmts(ArrayList<BillPmt> billPmts) {
        this.billPmts = billPmts;
	}
}
