package com.progressoft.mpay.plugins.externalcorporates.payment.prepaid;

public class PrePaidTransactionObjectBuilderFactory {

	public static PrePaidObjectBuilder getObjectBuilder() {
		return new PrePaidObjectBuilderImp();
	}
}