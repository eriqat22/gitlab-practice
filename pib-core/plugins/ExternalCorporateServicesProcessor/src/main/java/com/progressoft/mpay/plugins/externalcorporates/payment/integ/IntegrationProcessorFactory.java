package com.progressoft.mpay.plugins.externalcorporates.payment.integ;

import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.NotSupportedException;

public class IntegrationProcessorFactory {
	private static final Logger logger = LoggerFactory.getLogger(IntegrationProcessorFactory.class);

    public static IntegrationProcessor CreateProcessor(int messageType) throws NotSupportedException {
        logger.debug("Inside CreateProcessor ...");
		logger.debug("messageType: " + messageType);
		if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST)
			return new PaymentProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_RESPONSE)
			return new PaymentResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_RESPONSE)
			return new PaymentOfflineResponseProcessor();
		else if (messageType == MPClearIntegrationMessageTypes.REVERSAL_ADVISE_REQUEST)
			return new PaymentReversalAdviseProcessor();
		else
			throw new NotSupportedException("messageType == " + messageType);
	}
}
