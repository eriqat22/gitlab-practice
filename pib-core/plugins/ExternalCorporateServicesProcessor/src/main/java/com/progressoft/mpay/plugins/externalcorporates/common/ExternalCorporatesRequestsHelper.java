package com.progressoft.mpay.plugins.externalcorporates.common;

import com.opensymphony.util.GUID;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ExternalIntegMessage;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.externalcorporates.billinquiry.BillInqResponseObject;
import com.progressoft.mpay.plugins.externalcorporates.billinquiry.BillInquiryMessage;
import com.progressoft.mpay.plugins.externalcorporates.entities.PSRouterResponseBillInq;
import com.progressoft.mpay.plugins.psrouter.billinquiry.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.StringReader;
import java.io.StringWriter;

public class ExternalCorporatesRequestsHelper {
	private static final Logger logger = LoggerFactory.getLogger(ExternalCorporatesRequestsHelper.class);

	public static MPAY_ExternalIntegMessage GenerateBillInquiryRequest(MessageProcessingContext context) {
		logger.debug("Inside GenerateBillInquiryRequest ...");
		try {
			BillInquiryMessage request = (BillInquiryMessage) context.getRequest();
			MPAY_MPayMessage mpayMessage = context.getMessage();

			BodyType messageBody = creaetMessageBody(request);

			BillInquiryRequest billInqRequest = new BillInquiryRequest();
			RequestMessageType messageTypeRequest = createBillInquriyRequest(context, messageBody);

			String requestValues = messageTypeRequest.toString();
			String token = context.getCryptographer().generateToken(context.getSystemParameters(), requestValues, "",
					IntegMessagesSource.SYSTEM);
			messageTypeRequest.setPspSignature(token);
			billInqRequest.setRequestMessage(messageTypeRequest);

			context.getExtraData().put(Constants.BillInquiryRequestKey, billInqRequest);
			String messageContent = createXML(billInqRequest);

			return messageContent == null ? null
					: generateExternalIntegMessage(context, mpayMessage, messageTypeRequest, messageContent);
		} catch (Exception ex) {
			logger.error("Error when GenerateBillInquiryRequest in ExternalCorporatesRequestsHelper", ex);
			return null;
		}
	}

	private static MPAY_ExternalIntegMessage generateExternalIntegMessage(MessageProcessingContext context,
																		  MPAY_MPayMessage mpayMessage, RequestMessageType messageTypeRequest, String messageContent) {
		MPAY_ExternalIntegMessage integMessage = new MPAY_ExternalIntegMessage();
		integMessage.setRequestContent(messageContent);
		integMessage.setRequestDate(SystemHelper.getSystemTimestamp());
		integMessage.setRequestToken(messageTypeRequest.getPspSignature());
		integMessage.setRequestID(messageTypeRequest.getMessageId());
		integMessage.setReqField1(messageTypeRequest.getPspId());
		integMessage.setReqField2(Constants.PS_ROUTER_NAME);
		integMessage.setRefMessage(mpayMessage);
		integMessage.setRefService(context.getReceiver().getService());
		integMessage.setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
		return integMessage;
	}

	private static RequestMessageType createBillInquriyRequest(MessageProcessingContext context, BodyType messageBody) {
		RequestMessageType messageTypeRequest = new RequestMessageType();
		messageTypeRequest.setPspId(context.getLookupsLoader().getPSP().getNationalID());
		messageTypeRequest.setMessageId(GUID.generateGUID());
		messageTypeRequest.setMessageDateTime(SystemHelper.generateXmlDate());
		messageTypeRequest.setMessageBody(messageBody);
		return messageTypeRequest;
	}

	private static BodyType creaetMessageBody(BillInquiryMessage request) {
		BodyType messageBody = new BodyType();
		messageBody.setAcctInfo(createAccountInfoType(request));
		if (request.getStartDate() != null && request.getEndDate() != null)
			messageBody.setDateRange(createDateRange(request));
		messageBody.setServiceType(request.getServiceType());
		messageBody.setIncPaidBills(Boolean.parseBoolean(request.getIncPaidBills()));
		messageBody.setIncPayments(Boolean.parseBoolean(request.getIncPayments()));
		return messageBody;
	}

	private static AcctInfoType createAccountInfoType(BillInquiryMessage request) {
		AcctInfoType accountInfo = new AcctInfoType();
		accountInfo.setBillingNo(request.getBillingNo());
		accountInfo.setBillerCode(request.getBillerCode());
		accountInfo.setBillNo(request.getBillNo());
		return accountInfo;
	}

	private static DateRangeType createDateRange(BillInquiryMessage request) {
		DateRangeType dateRange = new DateRangeType();
		dateRange.setEndDt(EfawateercomGregorianTimeUtil.generateGregorianFormatDate(request.getEndDate()));
		dateRange.setStartDt(EfawateercomGregorianTimeUtil.generateGregorianFormatDate(request.getStartDate()));
		return dateRange;
	}

	public static MPAY_ExternalIntegMessage SendBillInquiryRequest(MPAY_ExternalIntegMessage integMessage,
																   MessageProcessingContext context) {
		logger.debug("Inside SendBillInquiryRequest ...");
		MPAY_MPayMessage mpayMessage = context.getMessage();

		PSRouterResponseBillInq billersResponse = null;
		BillInquiryResponse response = null;

		String errorDesc = null;
		String errorCode = null;
		String responseString = null;
		String fullMessage = null;
		try {
			PSRouterBillInquiry port = createBillInquiryService();
			BillInquiryRequest billInquiryMessage = (BillInquiryRequest) context.getExtraData()
					.get(Constants.BillInquiryRequestKey);

			response = port.doBillInquiry(billInquiryMessage);
			System.out.println(response);
			responseString = response.getResponseMessage();
			System.out.println(responseString);
			responseString = responseString.replace("&gt;", ">").replace("&lt;", "<");

			billersResponse = (PSRouterResponseBillInq) unmarshallResponse(responseString);

			if (billersResponse == null)
				return GenerateResponse(context, integMessage, mpayMessage, "", responseString,
						ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, Constants.Failedconnect,
						"Invalid response received");

			errorCode = billersResponse.getResCode();
			errorDesc = billersResponse.getResDesc();

			if (!isValidMessage(errorCode))
				return GenerateResponse(context, integMessage, mpayMessage, billersResponse.getResSignature(),
						responseString, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION,
						String.valueOf(errorCode), errorDesc);

			String responseBody;
			responseString = getMsgBodyTextOnly(responseString);
			responseBody = Helper.collectXMLElements(responseString);

			fullMessage = billersResponse.getOriginaleMsgId() + billersResponse.getResDateTime()
					+ billersResponse.getResCode() + billersResponse.getResDesc() + responseBody;

			if (!isSignatureVerified(context, fullMessage, billersResponse.getResSignature()))
				return GenerateResponse(context, integMessage, mpayMessage, billersResponse.getResSignature(),
						responseString, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION,
						Constants.InvalidMessageSignature, ReasonCodes.INVALID_TOKEN + " - "
								+ context.getLookupsLoader().getReason(ReasonCodes.INVALID_TOKEN).getDescription());
			else {
				context.getExtraData().put(Constants.BillInquiryResponseKey, billersResponse);
				return GenerateResponse(context, integMessage, mpayMessage, billersResponse.getResSignature(),
						responseString, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, Constants.Valid, null);
			}

		} catch (Exception e) {
			logger.error("Error when SendBillInquiryRequest in GetExternalCorporatesRequestsHelper", e);
			return GenerateResponse(context, integMessage, mpayMessage, billersResponse.getResSignature(),
					responseString, ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE,
					Constants.Failedconnect, e.getMessage());
		}
	}

	private static PSRouterBillInquiry createBillInquiryService() {
		PSRouterBillInquiry_Service service = new PSRouterBillInquiry_Service();
		PSRouterBillInquiry port = service.getPSRouterBillInquirySOAP();
		return port;
	}

	public static BillInqResponseObject CreateBillInquiryResponse(MessageProcessingContext context) {
		logger.debug("Inside CreateBillInquiryResponse ...");

		PSRouterResponseBillInq psRouterResponse = null;
		if (context.getExtraData().containsKey(Constants.BillInquiryResponseKey))
			psRouterResponse = (PSRouterResponseBillInq) context.getExtraData().get(Constants.BillInquiryResponseKey);

		if (psRouterResponse == null || psRouterResponse.getResBody().getMsgBody().getBillsRec() == null)
			return null;

		PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec billRecord = psRouterResponse.getResBody().getMsgBody().getBillsRec().getBillRec();
		return createBillInqResponse(billRecord);
	}

	private static BillInqResponseObject createBillInqResponse(PSRouterResponseBillInq.ResBody.MsgBody.BillsRec.BillRec billRecord) {
		BillInqResponseObject billInquiryResponse = new BillInqResponseObject();

		billInquiryResponse.setBillingNo(String.valueOf(billRecord.getAcctInfo().getBillingNo()));
		billInquiryResponse.setBillNo(String.valueOf(billRecord.getAcctInfo().getBillNo()));
		billInquiryResponse.setBillerCode(String.valueOf(billRecord.getAcctInfo().getBillerCode()));
		billInquiryResponse.setBillStatus(billRecord.getBillStatus());
		billInquiryResponse.setServiceType(billRecord.getServiceType());
		billInquiryResponse.setDueAmnt(String.valueOf(billRecord.getDueAmount()));
		billInquiryResponse.setFeesAmnt(String.valueOf(billRecord.getFeesAmt()));
		billInquiryResponse.setDueDate(billRecord.getDueDate().toString());
		billInquiryResponse.setIssueDate(billRecord.getIssueDate().toString());
		billInquiryResponse.setAllowPart(Boolean.parseBoolean(billRecord.getPmtConst().getAllowPart()));
		billInquiryResponse.setLower(String.valueOf(billRecord.getPmtConst().getLower()));
		billInquiryResponse.setUpper(String.valueOf(billRecord.getPmtConst().getUpper()));

		return billInquiryResponse;
	}

	private static MPAY_ExternalIntegMessage GenerateResponse(ProcessingContext context,
															  MPAY_ExternalIntegMessage integMessage, MPAY_MPayMessage mpayMessage, String psRouterResponseSignature,
															  String response, String processingStatusCode, String reasonCode, String status, String reasonDescription) {
		logger.debug("Inside GenerateResponse ...");
		MPAY_ProcessingStatus processingStatus = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);

		integMessage.setReasonCode(status);
		integMessage.setReasonDescription(reasonDescription);
		integMessage.setRefStatus(processingStatus);
		integMessage.setResponseDate(SystemHelper.getSystemTimestamp());
		if (response != null) {
			integMessage.setResponseContent(response);
			integMessage.setResponseToken(psRouterResponseSignature);
		}
		if (!processingStatusCode.equals(ProcessingStatusCodes.ACCEPTED)) {
			mpayMessage.setReason(reason);
			mpayMessage.setProcessingStatus(processingStatus);
			mpayMessage.setReasonDesc(reasonDescription);
		}
		return integMessage;
	}

	private static String getMsgBodyTextOnly(String responseText) {

		int firstIndex = responseText.indexOf("<resBody>");
		int lastIndex = responseText.indexOf("<resSignature>");

		return responseText.substring(firstIndex, lastIndex);
	}

	private static boolean isValidMessage(String errorCode) {
		return errorCode.equals("0");
	}

	public static boolean isSignatureVerified(ProcessingContext context, String message, String signature) {
		logger.debug("Inside verifySigniture ...");
		try {
			return context.getCryptographer().verifyToken(context.getSystemParameters(), message, signature, null,
					IntegMessagesSource.E_FAWATERCOM);
		} catch (Exception ex) {
			logger.error("Error when verifySigniture in GetExternalCorporatesRequestsHelper", ex);
			return false;
		}
	}

	public static String createXML(Object object) throws JAXBException {
		StringWriter writer = new StringWriter();
		JAXBContext JAXB_CONTEXT = JAXBContext.newInstance(BillInquiryRequest.class);
		JAXB_CONTEXT.createMarshaller().marshal(object, writer);
		return writer.toString();
	}

	public static Object unmarshallResponse(String response) throws JAXBException {
		JAXBContext JAXB_CONTEXT = JAXBContext.newInstance(PSRouterResponseBillInq.class);
		return JAXB_CONTEXT.createUnmarshaller().unmarshal(new StringReader(response));
	}
}
