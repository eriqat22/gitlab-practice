package com.progressoft.mpay.plugins.externalcorporates.payment;

public class AcctInf {

	private String BillingNo;
	private String BillNo;
	private String BillerCode;

	public String getBillingNo() {
		return BillingNo;
	}

	public void setBillingNo(String billingNo) {
		BillingNo = billingNo;
	}

	public String getBillNo() {
		return BillNo;
	}

	public void setBillNo(String billNo) {
		BillNo = billNo;
	}

	public String getBillerCode() {
		return BillerCode;
	}

	public void setBillerCode(String billerCode) {
		BillerCode = billerCode;
	}

}
