package com.progressoft.mpay.plugins.externalcorporates.payment.filed124;

import com.progressoft.mpay.plugins.externalcorporates.payment.postpaid.PostpaidFIled124Builder;
import com.progressoft.mpay.plugins.externalcorporates.payment.prepaid.PrepaidField124Builder;

public class Field124Factory {

	private static final String PREPAID = "Prepaid";
	private static final String POSTPAID = "Postpaid";

	public static Field124Builder getFiled124BuilderByType(String paidType) {
		if(paidType.equals(POSTPAID)) {
			return new PostpaidFIled124Builder();
		} else if (paidType.equals(PREPAID)){
			return new PrepaidField124Builder();
		} else {
			throw new IllegalArgumentException("The payment " + paidType + " dose not support");
		}
	}
}