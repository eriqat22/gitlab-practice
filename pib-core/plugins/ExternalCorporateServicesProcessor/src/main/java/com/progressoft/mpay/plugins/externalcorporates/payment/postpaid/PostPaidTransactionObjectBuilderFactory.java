package com.progressoft.mpay.plugins.externalcorporates.payment.postpaid;

public class PostPaidTransactionObjectBuilderFactory {

	public static PostPaidObjectBuilder getObjectBuilder() {
		return new PostPaidObjectBuilderImp();
	}

}
