package com.progressoft.mpay.plugins.externalcorporates.billinquiry;

public class BillPmt {
	private String efawTrxId;
	private String bankCode;
	private String dueAmt;
	private String paidAmt;
	private String pmtStatus;
	private String processDate;

	public String getEfawTrxId() {
		return efawTrxId;
	}

	public void setEfawTrxId(String efawTrxId) {
		this.efawTrxId = efawTrxId;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getDueAmt() {
		return dueAmt;
	}

	public void setDueAmt(String dueAmt) {
		this.dueAmt = dueAmt;
	}

	public String getPaidAmt() {
		return paidAmt;
	}

	public void setPaidAmt(String paidAmt) {
		this.paidAmt = paidAmt;
	}

	public String getPmtStatus() {
		return pmtStatus;
	}

	public void setPmtStatus(String pmtStatus) {
		this.pmtStatus = pmtStatus;
	}

	public String getProcessDate() {
		return processDate;
	}

	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}

}
