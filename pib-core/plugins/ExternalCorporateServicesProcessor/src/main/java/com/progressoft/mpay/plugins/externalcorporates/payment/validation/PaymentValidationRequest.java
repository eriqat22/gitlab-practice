package com.progressoft.mpay.plugins.externalcorporates.payment.validation;

import com.opensymphony.util.GUID;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ExternalIntegMessage;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.externalcorporates.common.Constants;
import com.progressoft.mpay.plugins.externalcorporates.common.Helper;
import com.progressoft.mpay.plugins.externalcorporates.entities.PSRouterResponsePrepaidValidation;
import com.progressoft.mpay.plugins.psrouter.prepaidvalidation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;

public class PaymentValidationRequest {
    private static final Logger logger = LoggerFactory.getLogger(PaymentValidationRequest.class);

    public static MPAY_ExternalIntegMessage GeneratePrePaidValidationRequest(MessageProcessingContext context) {
        logger.debug("Inside GeneratePrePaidValidationRequest ...");
        try {
            PaymentValidationMessage message = (PaymentValidationMessage) context.getRequest();
            PrepaidValidation prepaidValidation = new PrepaidValidation();
            RequestMessageType messageType = new RequestMessageType();

            messageType.setPspId(context.getLookupsLoader().getPSP().getNationalID());
            messageType.setMessageId(GUID.generateGUID());
            messageType.setMessageDateTime(SystemHelper.generateXmlDate());

            MessageBodyType messageBody = new MessageBodyType();
            BillingInfoType billingInfo = new BillingInfoType();

            AcctInfoType accountInfo = new AcctInfoType();
            accountInfo.setBillingNo(message.getBillingNo());
            accountInfo.setBillerCode(Integer.parseInt(message.getBillerCode()));
            billingInfo.setAcctInfo(accountInfo);

            if (message.getDueAmt() != null)
                billingInfo.setDueAmt(BigDecimal.valueOf(message.getDueAmt()));

            ServiceTypeDetailsType serviceTypeDetails = new ServiceTypeDetailsType();
            serviceTypeDetails.setServiceType(message.getServiceType());
            if (message.getPrepaidCat() != null)
                serviceTypeDetails.setPrepaidCat(message.getPrepaidCat());
            billingInfo.setServiceTypeDetails(serviceTypeDetails);

            messageBody.setBillingInfo(billingInfo);
            messageType.setMessageBody(messageBody);

            String requestValues = messageType.toString();
            logger.info("---------------------------------  " + requestValues);
            String token = context.getCryptographer().generateToken(context.getSystemParameters(), requestValues, "",
                    IntegMessagesSource.SYSTEM);
            messageType.setPspSignature(token);
            prepaidValidation.setRequestMessage(messageType);

            context.getExtraData().put(Constants.PrePaidValidationRequestKey, prepaidValidation);
            String messageContent = createXML(prepaidValidation);

            return messageContent == null ? null
                    : generateExternalIntegMessage(context, prepaidValidation, messageContent);
        } catch (Exception ex) {
            logger.error("Error when GeneratePrePaidValidationRequest in PaymentValidationRequest", ex);
            return null;
        }
    }

    private static MPAY_ExternalIntegMessage generateExternalIntegMessage(MessageProcessingContext context,
                                                                          PrepaidValidation prepaidValidation, String messageContent) {
        MPAY_ExternalIntegMessage integMessage = new MPAY_ExternalIntegMessage();
        integMessage.setRequestContent(messageContent);
        integMessage.setRequestDate(SystemHelper.getSystemTimestamp());
        integMessage.setRequestToken(prepaidValidation.getRequestMessage().getPspSignature());
        integMessage.setRequestID(prepaidValidation.getRequestMessage().getMessageId());
        integMessage.setReqField1(prepaidValidation.getRequestMessage().getPspId());
        integMessage.setReqField2(Constants.PS_ROUTER_NAME);
        integMessage.setRefMessage(context.getMessage());
        integMessage.setRefService(context.getReceiver().getService());
        integMessage.setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
        return integMessage;
    }

    public static MPAY_ExternalIntegMessage SendPrePaidValidationRequest(MPAY_ExternalIntegMessage integMessage,
                                                                         MessageProcessingContext context) {
        logger.debug("Inside SendPrePaidValidationRequest ...");
        MPAY_MPayMessage mpayMessage = context.getMessage();

        PSRouterResponsePrepaidValidation validationResponse = null;
        String response = null;
        String errorDesc = null;
        String errorCode = null;
        String fullMessage = null;
        try {
            PSRouterPrepaidValidation port = createPrepaidValidationService();
            PrepaidValidation prepaidValidationMessage = (PrepaidValidation) context.getExtraData()
                    .get(Constants.PrePaidValidationRequestKey);

            response = port.prepaidValidation(prepaidValidationMessage.getRequestMessage());
            response = response.replace("&gt;", ">").replace("&lt;", "<");

            validationResponse = (PSRouterResponsePrepaidValidation) unmarshallResponse(response);

            if (validationResponse == null)
                return GenerateResponse(context, integMessage, mpayMessage, "", response, ProcessingStatusCodes.FAILED,
                        ReasonCodes.SERVICE_INTEGRATION_FAILURE, Constants.Failedconnect, "Invalid response received");

            errorCode = validationResponse.getResCode();
            errorDesc = validationResponse.getResDesc();

            if (!isValidMessage(errorCode))
                return GenerateResponse(context, integMessage, mpayMessage, validationResponse.getResSignature(),
                        response, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION,
                        errorCode, errorDesc);

            String responseBody;
            response = getMsgBodyTextOnly(response);
            responseBody = Helper.collectXMLElements(response);

            fullMessage = validationResponse.getOriginaleMsgId() + validationResponse.getResDateTime()
                    + validationResponse.getResCode() + validationResponse.getResDesc() + responseBody;

            if (!isSignatureVerified(context, fullMessage, validationResponse.getResSignature()))
                return GenerateResponse(context, integMessage, mpayMessage, validationResponse.getResSignature(),
                        response, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION,
                        Constants.InvalidMessageSignature, ReasonCodes.INVALID_TOKEN + " - "
                                + context.getLookupsLoader().getReason(ReasonCodes.INVALID_TOKEN).getDescription());
            else {
                context.getExtraData().put(Constants.PrePaidValidationResponseKey, validationResponse);
                return GenerateResponse(context, integMessage, mpayMessage, validationResponse.getResSignature(),
                        response, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, Constants.Valid, null);
            }

        } catch (Exception e) {
            logger.error("Error when SendPrePaidValidationRequest in PaymentValidationRequest", e);
            return GenerateResponse(context, integMessage, mpayMessage, validationResponse.getResSignature(), response,
                    ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, Constants.Failedconnect,
                    e.getMessage());
        }
    }

    private static MPAY_ExternalIntegMessage GenerateResponse(ProcessingContext context,
                                                              MPAY_ExternalIntegMessage integMessage, MPAY_MPayMessage mpayMessage, String psRouterResponseSignature,
                                                              String response, String processingStatusCode, String reasonCode, String status, String reasonDescription) {
        logger.debug("Inside GenerateResponse ...");
        MPAY_ProcessingStatus processingStatus = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);

        integMessage.setReasonCode(status);
        integMessage.setReasonDescription(reasonDescription);
        integMessage.setRefStatus(processingStatus);
        integMessage.setResponseDate(SystemHelper.getSystemTimestamp());
        if (response != null) {
            integMessage.setResponseContent(response);
            integMessage.setResponseToken(psRouterResponseSignature);
        }
        if (!processingStatusCode.equals(ProcessingStatusCodes.ACCEPTED)) {
            mpayMessage.setReason(reason);
            mpayMessage.setProcessingStatus(processingStatus);
            mpayMessage.setReasonDesc(reasonDescription);
        }
        return integMessage;
    }

    public static PaymentValidationResponse CreatePrepaidValidationResponse(MessageProcessingContext context) {
        logger.debug("Inside CreatePrepaidValidationResponse ...");

        PSRouterResponsePrepaidValidation psRouterResponse = null;
        if (context.getExtraData().containsKey(Constants.PrePaidValidationResponseKey))
            psRouterResponse = (PSRouterResponsePrepaidValidation) context.getExtraData()
                    .get(Constants.PrePaidValidationResponseKey);

        if (psRouterResponse == null)
            return null;

        PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo billingInfo = psRouterResponse.getResBody().getMsgBody().getBillingInfo();
        PaymentValidationResponse response = new PaymentValidationResponse();
        PSRouterResponsePrepaidValidation.ResBody.MsgBody.BillingInfo.AcctInfo acctInfo = billingInfo.getAcctInfo();
        response.setDueAmt(billingInfo.getDueAmt());
        response.setFeesAmt(billingInfo.getFeesAmt());
        response.setValidationCode(billingInfo.getValidationCode());
        response.setMsgLabel(billingInfo.getMsgLabel());
        if (acctInfo != null)
            response.setBillerCode(acctInfo.getBillerCode());
        if (acctInfo != null)
            response.setBillingNo(acctInfo.getBillingNo());
        PaymentValidationMessage message = (PaymentValidationMessage) context.getRequest();
        response.setServiceType(message.getServiceType());
        response.setPrepaidCat(message.getPrepaidCat());

        return response;
    }

    public static String createXML(Object object) throws JAXBException {
        StringWriter writer = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(PrepaidValidation.class);
        jaxbContext.createMarshaller().marshal(object, writer);
        return writer.toString();
    }

    public static Object unmarshallResponse(String response) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(PSRouterResponsePrepaidValidation.class);
        return jaxbContext.createUnmarshaller().unmarshal(new StringReader(response));
    }

    private static PSRouterPrepaidValidation createPrepaidValidationService() {
        PSRouterPrepaidValidation_Service service = new PSRouterPrepaidValidation_Service();
        return service.getPSRouterPrepaidValidationSOAP();

    }

    private static String getMsgBodyTextOnly(String responseText) {
        int firstIndex = responseText.indexOf("<resBody>");
        int lastIndex = responseText.indexOf("<resSignature>");

        return responseText.substring(firstIndex, lastIndex);
    }

    private static boolean isValidMessage(String errorCode) {
        return errorCode.equals("0");
    }

    public static boolean isSignatureVerified(ProcessingContext context, String message, String signature) {
        logger.debug("Inside verifySigniture ...");
        try {
            return context.getCryptographer().verifyToken(context.getSystemParameters(), message, signature, null,
                    IntegMessagesSource.E_FAWATERCOM);
        } catch (Exception ex) {
            logger.error("Error when verifySigniture in PaymentValidationRequest", ex);
            return false;
        }
    }

}
