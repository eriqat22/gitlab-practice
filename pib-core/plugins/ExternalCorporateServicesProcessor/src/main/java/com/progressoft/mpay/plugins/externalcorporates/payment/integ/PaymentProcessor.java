package com.progressoft.mpay.plugins.externalcorporates.payment.integ;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.externalcorporates.payment.NotificationProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.SQLException;

public class PaymentProcessor extends IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(PaymentProcessor.class);

	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		try {
			logger.debug("Inside ProcessIntegration ...");
            PreProcessIntegration(context);
            if (context.getMpClearIsoMessage().getField(44).getValue().equals("99"))
                return null;
            if (context.getMpClearIsoMessage().getField(44).getValue().equals("00"))
                return Accept(context);
            else
                return Reject(context);
        } catch (Exception ex) {
			logger.error("Error in ProcessIntegration", ex);
            return Reject(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
	}

    private void PreProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
        logger.debug("Inside PreProcessIntegration ...");
		if (context.getMessage() != null)
            context.setTransaction(DataProvider.instance().getTransactionByMessageId(context.getMessage().getId()));
        int registrationIdLength = SystemParameters.getInstance().getMobileAccountSelectorLength() + SystemParameters.getInstance().getRoutingCodeLength();
		String mpClearReceiver = MPClearHelper.getEncodedString(context.getMpClearIsoMessage(), 63);
		String senderInfo = MPClearHelper.getEncodedString(context.getMpClearIsoMessage(), 62).substring(registrationIdLength + 1);
		String receiverInfo = MPClearHelper.getEncodedString(context.getMpClearIsoMessage(), 63).substring(registrationIdLength);
		String receiverType = receiverInfo.substring(0, 1);
		receiverInfo = receiverInfo.substring(1);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setAmount(MPClearHelper.getAmount(context.getMpClearIsoMessage()));
		context.setSender(sender);
		context.setReceiver(receiver);
		sender.setInfo(senderInfo);
        MPAY_CorpoarteService receiverService = DataProvider.instance().getCorporateService(receiverInfo, receiverType);
        if (receiverService == null)
			return;
		receiver.setService(receiverService);
		receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), receiverService, mpClearReceiver.substring(0, registrationIdLength)));
		if (receiver.getServiceAccount() == null)
			return;
		receiver.setAccount(receiver.getServiceAccount().getRefAccount());
		receiver.setCorporate(receiverService.getRefCorporate());
		receiver.setBank(receiverService.getBank());
		receiver.setBanked(receiverService.getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
		receiver.setProfile(receiverService.getRefProfile());
		receiver.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), receiver.getAccount().getId()));
		context.setDirection(TransactionDirection.INWARD);
        ChargesCalculator.calculate(context);
    }

    private IntegrationProcessingResult Accept(IntegrationProcessingContext context) throws SQLException {
        logger.debug("Inside Accept ...");
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, null, null, false);
		result.setNotifications(NotificationProcessor.CreateAcceptanceNotificationMessages(context));
		return result;
	}

    private IntegrationProcessingResult Reject(IntegrationProcessingContext context) throws SQLException {
        logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		String reasonDescription = null;
		if (context.getMpClearIsoMessage().getField(47) != null)
			reasonDescription = context.getMpClearIsoMessage().getField(47).getValue().toString();
        IntegrationProcessingResult result = IntegrationProcessingResult.create(context, ProcessingStatusCodes.REJECTED, ReasonCodes.REJECTED_BY_NATIONAL_SWITCH,
                context.getMpClearIsoMessage().getField(44).getValue() + " " + reasonDescription, null, false);
        context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(),
                BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
        result.setNotifications(NotificationProcessor.CreateRejectionNotificationMessages(context));
		return result;
	}

    private IntegrationProcessingResult Reject(IntegrationProcessingContext context, String processingCode, String reasonCode, String reasonDescription) {
        logger.debug("Inside Reject ...");
		BankIntegrationResult integrationResult = BankIntegrationHandler.reverse(context);
		if (integrationResult != null)
			TransactionHelper.reverseTransaction(context);
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, processingCode, reasonCode, reasonDescription, null, false);
		try {
            context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(), context.getTransactionConfig().getMessageType().getId(),
                    BigDecimal.ZERO.subtract(context.getTransaction().getTotalAmount()), -1, SystemHelper.getCurrentDateWithoutTime());
        } catch (SQLException e) {
			logger.error("Failed when trying to reverse limits", e);
		}
		result.setNotifications(NotificationProcessor.CreateRejectionNotificationMessages(context));
		return result;
	}
}
