package com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry.view.AccountInquiryView;

public class AccountInquiryMessage extends MPayRequest implements AccountInquiryView {

	private String efawatercomProfileId;
	private String pspId;
	private String pspSignature;

	private AccountInquiryMessage(MPayRequest request) {
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public void setEfawatercomCustomerId(String efawatercomCustomerId) {
		this.efawatercomProfileId = efawatercomCustomerId;
	}

	public void setPspId(String pspId) {
		this.pspId = pspId;
	}

	public void setPspSignature(String pspSignature) {
		this.pspSignature = pspSignature;
	}

	@Override
	public String getEfawatercomCustomerId() {
		return efawatercomProfileId;
	}

	@Override
	public String getPspId() {
		return pspId;
	}

	@Override
	public String getPspSignature() {
		return pspSignature;
	}

	public static AccountInquiryMessage parseMessage(MPayRequest request) {
		AccountInquiryMessage accountInquiryMessage = new AccountInquiryMessage(request);
		//because router bug
		accountInquiryMessage.setEfawatercomCustomerId("0");
		accountInquiryMessage.setPspId(LookupsLoader.getInstance().getPSP().getNationalID());
		return accountInquiryMessage;
	}
}