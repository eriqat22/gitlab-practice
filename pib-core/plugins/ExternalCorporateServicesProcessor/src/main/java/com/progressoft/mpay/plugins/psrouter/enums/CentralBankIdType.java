package com.progressoft.mpay.plugins.psrouter.enums;

public class CentralBankIdType {

    private static String NATIONAL_ID = "NAT";
    private static String PASSPORT = "PAS";
    private static String IQAMEH = "IQA";
    //TODO im not sure if its (BIS,PSP) or somthing else //prince
    private static String National_institution_ID = "BIS";
    private static String SERVICES_PROVIDER = "PSP";

    public static String getCentralBankCodeByMpay(String mpayCode) {
        switch (mpayCode) {
            case "777":
                return NATIONAL_ID;
            case "888":
                return PASSPORT;
            case "999":
                return National_institution_ID;
            case "3":
                return SERVICES_PROVIDER;
            default:
                return IQAMEH;

        }
    }
}