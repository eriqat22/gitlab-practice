package com.progressoft.mpay.plugins.externalcorporates.payment;

import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.commissions.CommissionDirections;
import com.progressoft.mpay.commissions.CommissionProcessorFactory;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.*;
import com.progressoft.mpay.messages.*;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclearinteg.ISO8583Parser;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.mpclearinteg.MPClearIntegrationMessageTypes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.externalcorporates.payment.filed124.Field124Factory;
import com.progressoft.mpay.plugins.externalcorporates.payment.integ.IntegrationProcessor;
import com.progressoft.mpay.plugins.externalcorporates.payment.integ.IntegrationProcessorFactory;
import com.progressoft.mpay.plugins.psrouter.enums.CentralBankIdType;
import com.progressoft.mpay.tax.TaxCalculator;
import com.progressoft.mpay.transactions.AccountingHelper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.SQLException;

public class PaymentProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(PaymentProcessor.class);
    private static final String Postpaid = "Postpaid";
    private static final int XML_HEADER_LENGHT = 55;

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");
        try {
            preProcessMessage(context);
            ValidationResult validationResult = PaymentMessageValidator.validate(context);
            if (validationResult.isValid())
                return AcceptMessage(context);
            else
                return RejectMessage(context, ProcessingStatusCodes.REJECTED, validationResult.getReasonCode(),
                        validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            return RejectMessage(context, ProcessingStatusCodes.REJECTED, ReasonCodes.FAILED_TO_PARSE_MESSAGE,
                    e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage", ex);
            if (context.getTransaction() != null)
                TransactionHelper.reverseTransaction(context);
            return RejectMessage(context, ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR,
                    ex.getMessage());
        }
    }

    private MessageProcessingResult AcceptMessage(MessageProcessingContext context) throws SQLException {
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = new MessageProcessingResult();
        MPAY_ProcessingStatus status;
        MPAY_Reason reason = context.getLookupsLoader().getReason(ReasonCodes.VALID);
        String reasonDescription = null;

        context.getMessage().setReason(reason);
        MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT,
                BigDecimal.ZERO, null);
        result.setMessage(context.getMessage());
        context.setTransaction(transaction);
        result.setTransaction(transaction);
        context.getDataProvider().persistProcessingResult(result);
        JVPostingResult postingResult = AccountingHelper.postAccounts(context.getDataProvider(),
                context.getLookupsLoader(), transaction);
        if (postingResult.isSuccess()) {
            BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
            if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
                Integer mpClearIntegType;
                if (SystemParameters.getInstance().getOfflineModeEnabled() && context.getDirection() == TransactionDirection.ONUS) {
                    mpClearIntegType = MPClearIntegrationMessageTypes.OFFLINE_FINANCIAL_REQUEST;
                    status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED);
                    result.setHandleMPClearOffline(true);
                } else {
                    mpClearIntegType = MPClearIntegrationMessageTypes.ONLINE_FINANCIAL_REQUEST;
                    status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PARTIALLY_ACCEPTED);
                }
                boolean isSenderAlias = false;
                if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE)) {
                    isSenderAlias = context.getSender().getMobile().getNotificationShowType()
                            .equals(NotificationShowTypeCodes.ALIAS) && context.getSender().getMobile().getAlias() != null
                            && context.getSender().getMobile().getAlias().length() > 0;
                }
                IsoMessage mpClearIsoMessage = CreateMPClearRequest(context, mpClearIntegType, isSenderAlias);
                MPAY_MpClearIntegMsgLog messageLog = MPClearHelper.createMPClearMessageLog(context, mpClearIsoMessage,
                        Integer.toHexString(mpClearIntegType));
                messageLog.setRefMessage(context.getMessage());
                result.setMpClearMessage(messageLog);
                context.getDataProvider().updateAccountLimit(context.getSender().getAccount().getId(),
                        context.getMessage().getRefOperation().getMessageType().getId(), transaction.getTotalAmount(),
                        1, SystemHelper.getCurrentDateWithoutTime());
            } else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
                reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
                reasonDescription = integResult.getReasonDescription();
                TransactionHelper.reverseTransaction(context);
            } else {
                status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.FAILED);
                reason = context.getLookupsLoader().getReason(integResult.getReasonCode());
                reasonDescription = integResult.getReasonDescription();
                TransactionHelper.reverseTransaction(context);
            }
        } else {
            if (!postingResult.getReason().equals(ReasonCodes.INSUFFICIENT_FUNDS)) {
                reason = context.getLookupsLoader().getReason(ReasonCodes.INTERNAL_SYSTEM_ERROR);
                context.getMessage().setReasonDesc(postingResult.getReason());
            } else
                reason = context.getLookupsLoader().getReason(ReasonCodes.INSUFFICIENT_FUNDS);
            status = context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED);
        }
        context.getMessage().setProcessingStatus(status);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        transaction.setProcessingStatus(context.getMessage().getProcessingStatus());
        transaction.setReason(context.getMessage().getReason());
        transaction.setReasonDesc(context.getMessage().getReasonDesc());
        result.setStatus(status);
        result.setStatusDescription(context.getMessage().getReasonDesc());
        if (status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            result.setNotifications(NotificationProcessor.CreateAcceptanceNotificationMessages(context));
        else if (status.getCode().equals(ProcessingStatusCodes.REJECTED))
            result.setNotifications(NotificationProcessor.CreateRejectionNotificationMessages(context));
        handleCommissions(context, status);
        return result;
    }


    private void handleCommissions(ProcessingContext context, MPAY_ProcessingStatus status) {
        if (!status.getCode().equals(ProcessingStatusCodes.ACCEPTED))
            return;
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.OUTWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getSender().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getSender(),
                    CommissionDirections.SENDER);
        if (context.getDirection() == TransactionDirection.ONUS || context.getDirection() == TransactionDirection.INWARD)
            CommissionProcessorFactory.getInstance().getProcessor(context.getReceiver().getProfile().getCommissionsScheme().getProcessor()).process(context, context.getReceiver(),
                    CommissionDirections.RECEIVER);
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, SQLException {
        logger.debug("Inside PreProcessMessage ...");
        PaymentMessage message = PaymentMessage.ParseMessage(context.getRequest());

        context.setRequest(message);
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        receiver.setInfo(message.getServiceType());
        context.setAmount(BigDecimal.valueOf(Double.valueOf(message.getPaidAmt())));
        context.getExtraData().put(PluginsCommonConstants.OTP_KEY,
                context.getDataProvider().getOtpClientByCode(message.getPin()));
        boolean isLoaded = false;
        context.setTransactionNature(TransactionTypeCodes.DIRECT_CREDIT);
        if (message.getSenderType().equals(ReceiverInfoType.MOBILE))
            isLoaded = loadSenderCustomer(context, sender);
        else if (message.getSenderType().equals(ReceiverInfoType.CORPORATE))
            isLoaded = loadSenderCorporate(context, sender);
        sender.setCommission(context.getDataProvider().getClientCommission(context.getOperation().getMessageType().getId(), sender.getAccount().getId()));
        if (!isLoaded)
            return;

        if (context.getTransactionConfig() != null)
            context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());


        context.getReceiver().setService(context.getLookupsLoader().getPSP().getPspMPClearService());
        context.getReceiver().setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(),
                context.getReceiver().getService(), null));
        if (context.getReceiver().getServiceAccount() != null)
            context.getReceiver().setBank(context.getReceiver().getServiceAccount().getBank());

        if (context.getTransactionConfig() != null)
            context.getMessage().setMessageType(context.getTransactionConfig().getMessageType());

        ChargesCalculator.calculateSenderCharge(context.getOperation().getMessageType().getCode(), context,
                context.getSender().getProfile());
        TaxCalculator.claculate(context);


    }


    private boolean loadSenderCustomer(MessageProcessingContext context, ProcessingContextSide sender)
            throws MessageParsingException, SQLException {
        PaymentMessage message = PaymentMessage.ParseMessage(context.getRequest());
        sender.setInfo(message.getSender());
        sender.setMobile(DataProvider.instance().getCustomerMobile(message.getSender()));
        if (sender.getMobile() == null)
            return false;
        sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(),
                message.getSenderAccount()));
        if (sender.getMobileAccount() == null)
            return false;
        sender.setAccount(sender.getMobileAccount().getRefAccount());
        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setBank(sender.getMobile().getBank());
        sender.setProfile(sender.getMobileAccount().getRefProfile());
        sender.setBanked(sender.getMobile().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
                context.getOperation().getMessageType().getId()));
        context.setDirection(TransactionDirection.OUTWARD);
        long personClientType = context.getLookupsLoader().getCustomerClientType().getId();
        context.setTransactionConfig(context.getLookupsLoader().getTransactionConfig(personClientType,
                sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
                context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT,
                context.getOperation().getId()));
        return true;
    }

    private boolean loadSenderCorporate(MessageProcessingContext context, ProcessingContextSide sender)
            throws MessageParsingException, SQLException {
        PaymentMessage message = PaymentMessage.ParseMessage(context.getRequest());
        sender.setInfo(message.getSender());
        sender.setService(DataProvider.instance().getCorporateService(message.getSender(), message.getSenderType()));
        if (sender.getService() == null)
            return false;
        sender.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(),
                message.getSenderAccount()));
        if (sender.getServiceAccount() == null)
            return false;
        sender.setAccount(sender.getServiceAccount().getRefAccount());
        sender.setCorporate(sender.getService().getRefCorporate());
        sender.setBank(sender.getService().getBank());
        sender.setProfile(sender.getServiceAccount().getRefProfile());
        sender.setBanked(sender.getService().getBankedUnbanked().equalsIgnoreCase(BankedUnbankedFlag.BANKED));
        sender.setLimits(context.getDataProvider().getAccountLimits(sender.getAccount().getId(),
                context.getOperation().getMessageType().getId()));

        context.setDirection(TransactionDirection.OUTWARD);
        context.setTransactionConfig(
                context.getLookupsLoader().getTransactionConfig(sender.getCorporate().getClientType().getId(),
                        sender.isBanked(), context.getLookupsLoader().getMPClear().getClientType(), false,
                        context.getOperation().getMessageType().getId(), TransactionTypeCodes.DIRECT_CREDIT,
                        context.getOperation().getId()));
        return true;
    }


    private MessageProcessingResult RejectMessage(MessageProcessingContext context, String processingStatusCode,
                                                  String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        if (context.getTransaction() != null) {
            BankIntegrationResult result = BankIntegrationHandler.reverse(context);
            if (result != null)
                TransactionHelper.reverseTransaction(context);
        }
        return MessageProcessingResult.create(context, status, reason, reasonDescription);
    }

    private IsoMessage CreateMPClearRequest(MessageProcessingContext context, Integer type, boolean isSenderAlias) {
        logger.debug("Inside CreateMPClearRequest ...");
        try {
            PaymentMessage request = PaymentMessage.ParseMessage(context.getRequest());

            MPAY_CustomerMobile customerMobile;
            MPAY_CorpoarteService service;
            if (request.getSenderType().equalsIgnoreCase(ReceiverInfoType.MOBILE)) {
                customerMobile = context.getDataProvider().getCustomerMobile(request.getSender(), request.getSenderType());
                MPAY_Customer refCustomer = customerMobile.getRefCustomer();
                request.setIdNUmber(refCustomer.getIdNum());
                request.setNation(refCustomer.getNationality().getDescription());
                request.setIdType(getIdTypeAccordingToRequest(refCustomer.getIdType().getCode()));
            } else if (request.getSenderType().equalsIgnoreCase(ReceiverInfoType.CORPORATE)) {
                service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
                MPAY_Corporate refCorporate = service.getRefCorporate();
                request.setIdNUmber(refCorporate.getRegistrationId());
                request.setNation(refCorporate.getRefCountry().getDescription());
                request.setIdType(getIdTypeAccordingToRequest(refCorporate.getIdType().getCode()));
            }


            BigDecimal amount = context.getTransaction().getTotalAmount();
            amount = amount.subtract(context.getSender().getCharge()).subtract(context.getReceiver().getCharge());
            String messageID = context.getDataProvider().getNextMPClearMessageId();
            IsoMessage message = ISO8583Parser.getInstance(context.getSystemParameters().getISO8583ConfigFilePath())
                    .newMessage(type);
            message.setBinary(false);
            String formatedAmount = SystemHelper.formatISOAmount(context.getSystemParameters(), amount,
                    AmountType.AMOUNT);
            message.setField(3, new IsoValue<String>(IsoType.NUMERIC, "200000", 6));
            message.setField(4, new IsoValue<String>(IsoType.NUMERIC, formatedAmount, 12));
            MPClearHelper.setMessageEncoded(message);
            message.setField(7, new IsoValue<String>(IsoType.DATE10,
                    MPClearHelper.formatIsoDate(SystemHelper.getCalendar().getTime())));
            message.setField(46, new IsoValue<String>(IsoType.LLLVAR, "250", "250".length()));
            message.setField(49,
                    new IsoValue<String>(IsoType.ALPHA, context.getTransaction().getCurrency().getStringISOCode(), 3));
            message.setField(59, new IsoValue<String>(IsoType.LLLVAR, "1", "1".length()));
            message.setField(114, new IsoValue<String>(IsoType.LLLVAR, "0", "0".length()));
            logger.info("All fileds filled ......................................................................");

            String account1 = null;
            if (request.getSenderType().equalsIgnoreCase(ReceiverInfoType.MOBILE)) {
                account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getMobile(),
                        context.getSender().getMobileAccount(), isSenderAlias);
            } else if (request.getSenderType().equalsIgnoreCase(ReceiverInfoType.CORPORATE)) {
                account1 = MPClearHelper.getAccount(context.getSystemParameters(), context.getSender().getService(),
                        context.getSender().getServiceAccount(), isSenderAlias);
            }


            MPClearHelper.setEncodedString(message, 62, IsoType.LLLVAR, account1);

            String account2 = MPClearHelper.getAccount(context.getSystemParameters(), request.getServiceType(),
                    ReceiverInfoType.CORPORATE);
            MPClearHelper.setEncodedString(message, 63, IsoType.LLLVAR, account2);

            String filed124 = Field124Factory.getFiled124BuilderByType(request.getPaymentType()).createFiled124Xml(request);
            String resultWithoutXmlHeader = filed124.substring(XML_HEADER_LENGHT);
            String field124Encoded = Base64
                    .encodeBase64String(resultWithoutXmlHeader.getBytes(Charset.forName("UTF-8")));
            message.setField(124, new IsoValue<String>(IsoType.LLLVAR, field124Encoded, field124Encoded.length()));

            String pmtTypeNo = (request.getPaymentType().equals(Postpaid)) ? "1" : "2";
            message.setField(125, new IsoValue<String>(IsoType.LLLVAR, pmtTypeNo, pmtTypeNo.length()));

            String messageTypeCode = context.getMessage().getMessageType().getMpClearCode();
            message.setField(104, new IsoValue<String>(IsoType.LLLVAR, messageTypeCode, messageTypeCode.length()));
            message.setField(115, new IsoValue<String>(IsoType.LLLVAR, messageID, messageID.length()));

            return message;
        } catch (Exception e) {
            logger.error("Error when CreateMPClearRequest", e);
            return null;
        }
    }

    private String getIdTypeAccordingToRequest(String idType) {
        return CentralBankIdType.getCentralBankCodeByMpay(idType);
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside ProcessIntegration ...");
        IsoMessage mpClearIsoMessage = context.getMpClearIsoMessage();
        try {
            IntegrationProcessor processor = IntegrationProcessorFactory.CreateProcessor(mpClearIsoMessage.getType());
            IntegrationProcessingResult result = processor.ProcessIntegration(context);
            handleCommissions(context, result.getProcessingStatus());
            return result;
        } catch (Exception e) {
            logger.error("Error when ProcessIntegration", e);
            IntegrationProcessingResult result = IntegrationProcessingResult.create(context,
                    ProcessingStatusCodes.FAILED, ReasonCodes.INTERNAL_SYSTEM_ERROR, e.getMessage(), null,
                    context.isRequest());
            if (context.isRequest())
                result.setMpClearOutMessage(MPClearHelper.createMPClearReversalResponse(context,
                        mpClearIsoMessage.getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(),
                        MPClearHelper.getResponseType(mpClearIsoMessage.getType()), result.getReason().getCode(),
                        result.getReasonDescription()));
            return result;
        }
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        logger.debug("Inside Reverse ...");
        try {
            JVPostingResult result = TransactionHelper.reverseTransaction(context);
            DataProvider.instance().mergeTransaction(context.getTransaction());
            if (result.isSuccess())
                return CreateResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

            return CreateResult(context, result.getReason(), null, ProcessingStatusCodes.REJECTED);
        } catch (Exception ex) {
            logger.error("Failed to reverse transaction with id = " + context.getTransaction().getId(), ex);
            return CreateResult(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage(),
                    ProcessingStatusCodes.FAILED);
        }
    }

    public MessageProcessingResult CreateResult(MessageProcessingContext context, String reasonCode,
                                                String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        if (context.getTransaction() != null) {
            context.getTransaction().setReason(reason);
            context.getTransaction().setReasonDesc(reasonDescription);
            context.getTransaction().setProcessingStatus(status);
        }
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setTransaction(context.getTransaction());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }
}