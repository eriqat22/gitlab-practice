package com.progressoft.mpay.plugins.externalcorporates.profile.removecustomerbilling.processor.exception;

public class RemoveCusotmerBillingParseMessageException extends RuntimeException {

	public RemoveCusotmerBillingParseMessageException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
