package com.progressoft.mpay.plugins.externalcorporates.getbillers;

import com.opensymphony.util.GUID;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_ExternalIntegMessage;
import com.progressoft.mpay.entities.MPAY_MPayMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ProcessingContext;
import com.progressoft.mpay.plugins.externalcorporates.common.Constants;
import com.progressoft.mpay.plugins.externalcorporates.common.Helper;
import com.progressoft.mpay.plugins.externalcorporates.entities.PSRouterResponse;
import com.progressoft.mpay.plugins.psrouter.billers.GetBillers;
import com.progressoft.mpay.plugins.psrouter.billers.MessageBodyType;
import com.progressoft.mpay.plugins.psrouter.billers.PSRouterBillers2;
import com.progressoft.mpay.plugins.psrouter.billers.PSRouterBillers2_PSRouterBillers2SOAP_Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class GetBillersHelper {
    private static final Logger logger = LoggerFactory.getLogger(GetBillersHelper.class);

    public static MPAY_ExternalIntegMessage GenerateGetBillersRequest(MessageProcessingContext context) {
        logger.debug("Inside GenerateGetBillersRequest ...");
        try {
            GetBillersMessage request = (GetBillersMessage) context.getRequest();
            GetBillers getBiller = generateGetBillersObject(context, request);

            String requestValues = getBiller.toString();
            String token = context.getCryptographer().generateToken(context.getSystemParameters(), requestValues, "",
                    IntegMessagesSource.SYSTEM);
            getBiller.setPspSignature(token);

            context.getExtraData().put(Constants.GetBillersRequestKey, getBiller);
            String messageContent = createXML(getBiller);

            return messageContent == null ? null : generateExternalIntegMessage(context, getBiller, messageContent);
        } catch (Exception ex) {
            logger.error("Error when GenerateGetBillersRequest in ExternalCorporatesRequestsHelper", ex);
            return null;
        }
    }

    private static MPAY_ExternalIntegMessage generateExternalIntegMessage(MessageProcessingContext context,
                                                                          GetBillers getBiller, String messageContent) {
        MPAY_ExternalIntegMessage integMessage = new MPAY_ExternalIntegMessage();
        integMessage.setRequestContent(messageContent);
        integMessage.setRequestDate(SystemHelper.getSystemTimestamp());
        integMessage.setRequestToken(getBiller.getPspSignature());
        integMessage.setRequestID(getBiller.getMessageId());
        integMessage.setReqField1(getBiller.getPspId());
        integMessage.setReqField2(Constants.PS_ROUTER_NAME);
        integMessage.setRefMessage(context.getMessage());
        integMessage.setRefService(context.getReceiver().getService());
        integMessage.setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
        return integMessage;
    }

    private static GetBillers generateGetBillersObject(MessageProcessingContext context, GetBillersMessage request) {
        GetBillers getBiller = new GetBillers();
        getBiller.setPspId(context.getLookupsLoader().getPSP().getNationalID());
        getBiller.setMessageId(GUID.generateGUID());
        getBiller.setMessageDateTime(SystemHelper.generateXmlDate());
        getBiller.setMessageBody(getMessageBody(request));
        return getBiller;
    }

    private static MessageBodyType getMessageBody(GetBillersMessage request) {
        MessageBodyType messageBody = new MessageBodyType();
        messageBody.setCategoryCode(request.getCategoryCode() != null ? request.getCategoryCode() : "");
        return messageBody;
    }

    public static MPAY_ExternalIntegMessage SendGetBillersRequest(MPAY_ExternalIntegMessage integMessage,
                                                                  MessageProcessingContext context) {
        logger.debug("Inside SendGetBillersRequest ...");
        String errorCode = null;
        String response = null;
        String errorDesc = null;

        String fullMessage = null;
        String responseBody = null;
        String originalResponse = null;
        PSRouterResponse billersResponse = null;

        MPAY_MPayMessage mpayMessage = context.getMessage();
        PSRouterBillers2 port = sendGetBillersRequest();
        try {
            response = getResponse(context, port);
            originalResponse = response;
            billersResponse = (PSRouterResponse) unmarshallResponse(response);

            if (billersResponse == null)
                return GenerateResponse(context, integMessage, mpayMessage, "", originalResponse,
                        ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, Constants.Failedconnect,
                        "Invalid response received");

            errorCode = billersResponse.getResCode();
            errorDesc = billersResponse.getResDesc();

            if (!isValidMessage(errorCode))
                return GenerateResponse(context, integMessage, mpayMessage, billersResponse.getResSignature(),
                        originalResponse, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION,
                        String.valueOf(errorCode), errorDesc);

            response = getMsgBodyTextOnly(response);
            responseBody = Helper.collectXMLElements(response);

            fullMessage = billersResponse.getOriginaleMsgId() + billersResponse.getResDateTime()
                    + billersResponse.getResCode() + billersResponse.getResDesc() + responseBody;

            if (!isSignatureVerified(context, fullMessage, billersResponse.getResSignature()))
                return GenerateResponse(context, integMessage, mpayMessage, billersResponse.getResSignature(),
                        originalResponse, ProcessingStatusCodes.REJECTED, ReasonCodes.SERVICE_INTEGRATION_REJECTION,
                        Constants.InvalidMessageSignature, ReasonCodes.INVALID_TOKEN + " - "
                                + context.getLookupsLoader().getReason(ReasonCodes.INVALID_TOKEN).getDescription());
            else {
                context.getExtraData().put(Constants.GetBillersResponseKey, billersResponse);
                return GenerateResponse(context, integMessage, mpayMessage, billersResponse.getResSignature(),
                        originalResponse, ProcessingStatusCodes.ACCEPTED, ReasonCodes.VALID, Constants.Valid, null);
            }
        } catch (Exception e) {
            logger.error("Error when SendGetBillersRequest in GetExternalCorporatesRequestsHelper", e);
            return GenerateResponse(context, integMessage, mpayMessage, billersResponse.getResSignature(), response,
                    ProcessingStatusCodes.FAILED, ReasonCodes.SERVICE_INTEGRATION_FAILURE, Constants.Failedconnect,
                    e.getMessage());
        }
    }

    private static String getMsgBodyTextOnly(String responseText) {
        int firstIndex = responseText.indexOf("<resBody>");
        int lastIndex = responseText.indexOf("<resSignature>");

        return responseText.substring(firstIndex, lastIndex);
    }

    private static boolean isValidMessage(String errorCode) {
        return errorCode.equals(Constants.Valid);
    }

    private static String getResponse(MessageProcessingContext context, PSRouterBillers2 port) {
        String response;
        GetBillers getBillersRequest = (GetBillers) context.getExtraData().get(Constants.GetBillersRequestKey);
        response = port.getBillers(getBillersRequest.getPspId(), getBillersRequest.getPspSignature(),
                getBillersRequest.getMessageId(), getBillersRequest.getMessageDateTime(),
                getBillersRequest.getMessageBody());
        response = response.replace("&gt;", ">").replace("&lt;", "<");
        return response;
    }

    private static PSRouterBillers2 sendGetBillersRequest() {
        PSRouterBillers2_PSRouterBillers2SOAP_Client client = new PSRouterBillers2_PSRouterBillers2SOAP_Client();
        PSRouterBillers2 port = client.InitClient();
        return port;
    }

    public static boolean isSignatureVerified(ProcessingContext context, String message, String signature) {
        logger.debug("Inside verifySigniture ...");
        try {
            return context.getCryptographer().verifyToken(context.getSystemParameters(), message, signature, null,
                    IntegMessagesSource.E_FAWATERCOM);
        } catch (Exception ex) {
            logger.error("Error when verifySigniture in GetExternalCorporatesRequestsHelper", ex);
            return false;
        }
    }

    public static String CreateGetBillersResponse(MessageProcessingContext context) {
        logger.debug("Inside CreateGetBillersResponse ...");
        PSRouterResponse getBillersResponse = null;

        if (context.getExtraData().containsKey(Constants.GetBillersResponseKey))
            getBillersResponse = (PSRouterResponse) context.getExtraData().get(Constants.GetBillersResponseKey);

        if (getBillersResponse == null)
            return null;

        List<PSRouterResponse.ResBody.BillersList.Biller> billersList = getBillersResponse.getResBody().getBillersList().getBiller();
        return createListOfBillersResponse(billersList);
    }

    private static String createListOfBillersResponse(List<PSRouterResponse.ResBody.BillersList.Biller> billersList) {
        List<String> listOfBillersResponse = new ArrayList<String>();

        for (PSRouterResponse.ResBody.BillersList.Biller biller : billersList) {
            GetBillersResponse response = new GetBillersResponse();
            List<BillerService> services = new ArrayList<BillerService>();

            createGetBillersResponse(biller, response);
            createServices(biller, services);

            response.setServices(services);
            listOfBillersResponse.add(response.toString());
        }

        return listOfBillersResponse.toString();
    }

    @SuppressWarnings("rawtypes")
    private static void createServices(PSRouterResponse.ResBody.BillersList.Biller biller, List<BillerService> services) {
        for (PSRouterResponse.ResBody.BillersList.Biller.Services.Service billerService : biller.getServices().getService()) {
            BillerService service = new BillerService();
            createService(service, billerService);
            if (isContainPrepaidCategories(billerService)) {
                service.setContainsPrePaidCats(isContainPrepaidCategories(billerService));
                List<PrePaidCategories> prepaidCategories = new ArrayList<PrePaidCategories>();

                for (Serializable prepaidCat : billerService.getPrepaidCats().getContent()) {
                    PrePaidCategories prePaidCategory = createPrePaidCategories(
                            (PSRouterResponse.ResBody.BillersList.Biller.Services.Service.PrepaidCats.PrepaidCat) ((JAXBElement) prepaidCat).getValue());
                    prepaidCategories.add(prePaidCategory);
                    service.setPrepaidCats(prepaidCategories);
                }
            }
            services.add(service);
        }
    }

    private static boolean isContainPrepaidCategories(PSRouterResponse.ResBody.BillersList.Biller.Services.Service billerService) {
        return Boolean.valueOf(billerService.getContainsPrepaidCats());
    }

    private static PrePaidCategories createPrePaidCategories(PSRouterResponse.ResBody.BillersList.Biller.Services.Service.PrepaidCats.PrepaidCat prepaidCat) {
        PrePaidCategories prePaidCategory = new PrePaidCategories();
        prePaidCategory.setArDesc(prepaidCat.getPrepaidArDesc());
        prePaidCategory.setEngDesc(prepaidCat.getPrepaidEnDesc());
        prePaidCategory.setName(prepaidCat.getPrepaidCatName());
        return prePaidCategory;
    }

    private static void createService(BillerService service, PSRouterResponse.ResBody.BillersList.Biller.Services.Service billerService) {
        service.setArDesc(billerService.getServiceArDesc());
        service.setEnDesc(billerService.getServiceEnDesc());
        service.setPaymentType(billerService.getPaymentType());
        service.setServiceType(billerService.getServiceType());
        service.setBillingNoRequired(Boolean.parseBoolean(billerService.getBillingNoRequired()));
    }

    private static void createGetBillersResponse(PSRouterResponse.ResBody.BillersList.Biller biller, GetBillersResponse response) {
        response.setBillerCode(biller.getBillerCode());
        response.setArName(biller.getBillerArName());
        response.setEngName(biller.getBillerEnName());
        response.setCategory(biller.getBillerCategory());
        response.setEmail(biller.getBillerEmail());
        response.setPhone(String.valueOf(biller.getBillerPhoneNumber()));
        response.setWebsite(biller.getBillersWebsite());
        response.setStmtBankCode(biller.getBillerSettlementBank());
    }

    public static String createXML(Object object) throws JAXBException {
        StringWriter writer = new StringWriter();
        JAXBContext JAXB_CONTEXT = JAXBContext.newInstance(GetBillers.class);
        JAXB_CONTEXT.createMarshaller().marshal(object, writer);
        return writer.toString();
    }

    public static Object unmarshallResponse(String response) throws JAXBException {
        JAXBContext JAXB_CONTEXT = JAXBContext.newInstance(PSRouterResponse.class);
        return JAXB_CONTEXT.createUnmarshaller().unmarshal(new StringReader(response));
    }

    private static MPAY_ExternalIntegMessage GenerateResponse(ProcessingContext context,
                                                              MPAY_ExternalIntegMessage integMessage, MPAY_MPayMessage mpayMessage, String psRouterResponseSignature,
                                                              String response, String processingStatusCode, String reasonCode, String status, String reasonDescription) {
        logger.debug("Inside GenerateResponse ...");
        MPAY_ProcessingStatus processingStatus = context.getLookupsLoader().getProcessingStatus(processingStatusCode);
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);

        integMessage.setReasonCode(status);
        integMessage.setReasonDescription(reasonDescription);
        integMessage.setRefStatus(processingStatus);
        integMessage.setResponseDate(SystemHelper.getSystemTimestamp());
        if (response != null) {
            integMessage.setResponseContent(response);
            integMessage.setResponseToken(psRouterResponseSignature);
        }
        if (!processingStatusCode.equals(ProcessingStatusCodes.ACCEPTED)) {
            mpayMessage.setReason(reason);
            mpayMessage.setProcessingStatus(processingStatus);
            mpayMessage.setReasonDesc(reasonDescription);
        }
        return integMessage;
    }
}
