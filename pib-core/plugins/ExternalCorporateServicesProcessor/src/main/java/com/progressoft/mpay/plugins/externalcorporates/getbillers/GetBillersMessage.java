package com.progressoft.mpay.plugins.externalcorporates.getbillers;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

public class GetBillersMessage extends MPayRequest {
	public static final String categoryCodeKey = "categoryCode";

	private String categoryCode;

	public GetBillersMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static GetBillersMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		GetBillersMessage message = new GetBillersMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);

		if (message.getSenderType() == null || (!(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
				&& !(message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

		message.setCategoryCode(message.getValue(categoryCodeKey));
		return message;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }
}
