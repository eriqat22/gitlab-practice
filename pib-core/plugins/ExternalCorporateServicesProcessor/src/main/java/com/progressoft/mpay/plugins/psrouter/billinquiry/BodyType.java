
package com.progressoft.mpay.plugins.psrouter.billinquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bodyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bodyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctInfo" type="{http://www.progressoft.com/PSRouterBillInquiry/}AcctInfoType"/>
 *         &lt;element name="ServiceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DateRange" type="{http://www.progressoft.com/PSRouterBillInquiry/}DateRangeType"/>
 *         &lt;element name="IncPayments" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IncPaidBills" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bodyType", propOrder = {
    "acctInfo",
    "serviceType",
    "dateRange",
    "incPayments",
    "incPaidBills"
})
public class BodyType {

    @XmlElement(name = "AcctInfo", required = true)
    protected AcctInfoType acctInfo;
    @XmlElement(name = "ServiceType", required = true)
    protected String serviceType;
    @XmlElement(name = "DateRange", required = true)
    protected DateRangeType dateRange;
    @XmlElement(name = "IncPayments")
    protected boolean incPayments;
    @XmlElement(name = "IncPaidBills")
    protected boolean incPaidBills;

    /**
     * Gets the value of the acctInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AcctInfoType }
     *     
     */
    public AcctInfoType getAcctInfo() {
        return acctInfo;
    }

    /**
     * Sets the value of the acctInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcctInfoType }
     *     
     */
    public void setAcctInfo(AcctInfoType value) {
        this.acctInfo = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the dateRange property.
     * 
     * @return
     *     possible object is
     *     {@link DateRangeType }
     *     
     */
    public DateRangeType getDateRange() {
        return dateRange;
    }

    /**
     * Sets the value of the dateRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateRangeType }
     *     
     */
    public void setDateRange(DateRangeType value) {
        this.dateRange = value;
    }

    /**
     * Gets the value of the incPayments property.
     * 
     */
    public boolean isIncPayments() {
        return incPayments;
    }

    /**
     * Sets the value of the incPayments property.
     * 
     */
    public void setIncPayments(boolean value) {
        this.incPayments = value;
    }

    /**
     * Gets the value of the incPaidBills property.
     * 
     */
    public boolean isIncPaidBills() {
        return incPaidBills;
    }

    /**
     * Sets the value of the incPaidBills property.
     * 
     */
    public void setIncPaidBills(boolean value) {
        this.incPaidBills = value;
    }

}
