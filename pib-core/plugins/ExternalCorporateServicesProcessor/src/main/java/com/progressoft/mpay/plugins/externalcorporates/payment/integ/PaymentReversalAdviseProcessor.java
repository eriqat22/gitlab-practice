package com.progressoft.mpay.plugins.externalcorporates.payment.integ;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaymentReversalAdviseProcessor extends IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(PaymentReversalAdviseProcessor.class);
	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}
}
