
package com.progressoft.mpay.plugins.psrouter.prepaidvalidation;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.progressoft.psrouterprepaidvalidation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.progressoft.psrouterprepaidvalidation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PrepaidValidationResponse }
     * 
     */
    public PrepaidValidationResponse createPrepaidValidationResponse() {
        return new PrepaidValidationResponse();
    }

    /**
     * Create an instance of {@link PrepaidValidation }
     * 
     */
    public PrepaidValidation createPrepaidValidation() {
        return new PrepaidValidation();
    }

    /**
     * Create an instance of {@link RequestMessageType }
     * 
     */
    public RequestMessageType createRequestMessageType() {
        return new RequestMessageType();
    }

    /**
     * Create an instance of {@link MessageBodyType }
     * 
     */
    public MessageBodyType createMessageBodyType() {
        return new MessageBodyType();
    }

    /**
     * Create an instance of {@link AcctInfoType }
     * 
     */
    public AcctInfoType createAcctInfoType() {
        return new AcctInfoType();
    }

    /**
     * Create an instance of {@link BillingInfoType }
     * 
     */
    public BillingInfoType createBillingInfoType() {
        return new BillingInfoType();
    }

    /**
     * Create an instance of {@link ServiceTypeDetailsType }
     * 
     */
    public ServiceTypeDetailsType createServiceTypeDetailsType() {
        return new ServiceTypeDetailsType();
    }

}
