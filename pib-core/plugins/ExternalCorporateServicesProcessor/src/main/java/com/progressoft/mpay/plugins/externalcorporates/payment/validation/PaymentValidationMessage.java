package com.progressoft.mpay.plugins.externalcorporates.payment.validation;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

public class PaymentValidationMessage extends MPayRequest {

	public static final String billingNoKey = "billingNo";
	public static final String billerCodeKey = "billerCode";
	public static final String DueAmtKey = "DueAmt";
	public static final String serviceTypeKey = "serviceType";
	public static final String prepaidCatKey = "prepaidCat";

	private String BillingNo;
	private String BillerCode;
	private Double DueAmt;
	private String ServiceType;
	private String prepaidCat;

	public PaymentValidationMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

    public static PaymentValidationMessage ParseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        PaymentValidationMessage message = new PaymentValidationMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || (!(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
                && !(message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        message.setBillerCode(message.getValue(billerCodeKey));
        if (message.getBillerCode() == null || message.getBillerCode().trim().length() == 0)
            throw new MessageParsingException(billerCodeKey);

        message.setServiceType(message.getValue(serviceTypeKey));
        if (message.getServiceType() == null || message.getServiceType().trim().length() == 0)
            throw new MessageParsingException(serviceTypeKey);

        message.setBillingNo(message.getValue(billingNoKey));
        if (message.getValue(DueAmtKey) != null)
            message.setDueAmt(Double.valueOf(message.getValue(DueAmtKey)));
        if (message.getValue(prepaidCatKey) != null)
            message.setPrepaidCat(message.getValue(prepaidCatKey));

        return message;
    }

	public String getBillingNo() {
		return BillingNo;
	}

	public void setBillingNo(String billingNo) {
		BillingNo = billingNo;
	}

	public String getBillerCode() {
		return BillerCode;
	}

	public void setBillerCode(String billerCode) {
		BillerCode = billerCode;
	}

	public Double getDueAmt() {
		return DueAmt;
	}

	public void setDueAmt(Double dueAmt) {
		DueAmt = dueAmt;
	}

	public String getServiceType() {
		return ServiceType;
	}

	public void setServiceType(String serviceType) {
		ServiceType = serviceType;
	}

	public String getPrepaidCat() {
		return prepaidCat;
	}

	public void setPrepaidCat(String prepaidCat) {
		this.prepaidCat = prepaidCat;
	}
}
