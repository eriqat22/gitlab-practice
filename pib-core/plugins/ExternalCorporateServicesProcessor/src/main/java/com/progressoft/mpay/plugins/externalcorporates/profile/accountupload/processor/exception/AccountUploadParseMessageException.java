package com.progressoft.mpay.plugins.externalcorporates.profile.accountupload.processor.exception;

public class AccountUploadParseMessageException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	private String fieldName;

	public AccountUploadParseMessageException(String message) {
		super(message);
	}

	public void setFiledName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFiledName() {
		return fieldName;
	}
}