package com.progressoft.mpay.plugins.externalcorporates.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.validators.AccountValidator;
import com.progressoft.mpay.plugins.validators.CorporateValidator;
import com.progressoft.mpay.plugins.validators.CustomerValidator;
import com.progressoft.mpay.plugins.validators.DeviceValidator;
import com.progressoft.mpay.plugins.validators.LanguageValidator;
import com.progressoft.mpay.plugins.validators.LimitsValidator;
import com.progressoft.mpay.plugins.validators.MobileValidator;
import com.progressoft.mpay.plugins.validators.RequestTypeValidator;
import com.progressoft.mpay.plugins.validators.ServiceValidator;

public class ExternalCorporatesValidator {
	private static final Logger logger = LoggerFactory.getLogger(ExternalCorporatesValidator.class);

	private ExternalCorporatesValidator() {

	}

	public static ValidationResult validate(MessageProcessingContext context) {
		logger.debug("Inside Validate ...");

		ValidationResult result = RequestTypeValidator.validate(context.getSender().getProfile(),
				context.getOperation().getMessageType());
		if (!result.isValid())
			return result;

		result = LanguageValidator.validate(context.getLanguage());
		if (!result.isValid())
			return result;

		if (context.getRequest().getSenderType().equals(ReceiverInfoType.CORPORATE))
			return validateSenderService(context);
		else if (context.getRequest().getSenderType().equals(ReceiverInfoType.MOBILE))
			return validateSenderMobile(context);

		return new ValidationResult(ReasonCodes.FAILED_TO_PARSE_MESSAGE, null, false);
	}

	private static ValidationResult validateSenderMobile(MessageProcessingContext context) {
		ValidationResult result;
		result = MobileValidator.validate(context.getSender().getMobile(), true);
		if (!result.isValid())
			return result;

		result = CustomerValidator.validate(context.getSender().getCustomer(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getMobileAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCustomerDevice(context.getDataProvider(),
				context.getRequest().getDeviceId(), context.getSender().getMobile().getId()));
		if (!result.isValid())
			return result;

		return LimitsValidator.validate(context);
	}

	private static ValidationResult validateSenderService(MessageProcessingContext context) {
		ValidationResult result;
		result = ServiceValidator.validate(context.getSender().getService(), true);
		if (!result.isValid())
			return result;

		result = CorporateValidator.validate(context.getSender().getCorporate(), true);
		if (!result.isValid())
			return result;

		result = AccountValidator.validate(context.getSender().getServiceAccount(), true,context.getMessage().getMessageType().getIsFinancial());
		if (!result.isValid())
			return result;

		result = DeviceValidator.validate(MPayHelper.getCorporateDevice(context.getDataProvider(),
				context.getRequest().getDeviceId(), context.getSender().getService().getId()));
		if (!result.isValid())
			return result;

		return LimitsValidator.validate(context);
	}
}
