package com.progressoft.mpay.plugins.externalcorporates.payment.postpaid;

import com.progressoft.mpay.plugins.externalcorporates.payment.PaymentMessage;
import com.progressoft.mpay.plugins.externalcorporates.payment.filed124.Field124Builder;

public class PostpaidFIled124Builder implements Field124Builder {

	@Override
	public String createFiled124Xml(PaymentMessage message) throws Exception {
		Transactions postPaidTransaction = PostPaidTransactionObjectBuilderFactory.getObjectBuilder().buildPostPaidTransaction(message);
		return PostPaidTransactionSerilizerFactory.getSeriliezer().serilieze(postPaidTransaction);
	}
}