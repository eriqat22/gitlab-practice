package com.progressoft.mpay.plugins.externalcorporates.payment;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;
import org.apache.commons.lang.NullArgumentException;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

public class PaymentMessage extends MPayRequest {
    private static final String billingNoKey = "billingNo";
    private static final String billerCodeKey = "billerCode";
    private static final String billNoKey = "billNo";
    private static final String serviceTypeKey = "serviceType";
    private static final String dueAmtKey = "dueAmt";
    private static final String paidAmtKey = "paidAmt";
    private static final String processDateKey = "processDate";
    private static final String paymentTypeKey = "paymentType";
    private static final String currencyKey = "currency";
    private static final String validationCodeKey = "validationCode";
    private static final String prepaidCatKey = "prepaidCat";
    private static final String senderAccountKey = "senderAccount";
    private static final String receiverAccountKey = "senderAccount";

    private String billingNo;
    private String billNo;
    private String billerCode;
    private String dueAmt;
    private String paidAmt;
    private Timestamp processDate;
    private String paymentType;
    private String currency;
    private String serviceType;
    private String senderAccount;
    private String receiverAccount;
    private String validationCode;
    private String prepaidCat;
    private String idType;
    private String idNUmber;
    private String nation;

    public PaymentMessage(MPayRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static PaymentMessage ParseMessage(MPayRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        PaymentMessage message = new PaymentMessage(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(MPayRequest.SENDER_KEY);
        if (message.getSenderType() == null || (!(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
                && !(message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE))))
            throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);

        message.setBillerCode(message.getValue(billerCodeKey));
        if (message.getBillerCode() == null)
            throw new MessageParsingException(billerCodeKey);

        message.setServiceType(message.getValue(serviceTypeKey));
        if (message.getServiceType() == null || message.getServiceType().trim().length() == 0)
            throw new MessageParsingException(serviceTypeKey);

        message.setCurrency(message.getValue(currencyKey));
        if (message.getCurrency() == null)
            throw new MessageParsingException(currencyKey);

        try {
            message.setProcessDate(getCurrentTimestampWihtEfawateercomFormat(message));
        } catch (Exception ex) {
            throw new MessageParsingException(processDateKey);
        }

        String dueAmount = message.getValue(dueAmtKey);
        if (dueAmount == null)
            throw new MessageParsingException(dueAmtKey);
        try {
            message.setDueAmt(dueAmount);
        } catch (NumberFormatException ex) {
            throw new MessageParsingException(dueAmtKey);
        }

        String paidAmount = message.getValue(paidAmtKey);
        if (paidAmount == null)
            throw new MessageParsingException(paidAmtKey);
        try {
            message.setPaidAmt(paidAmount);
        } catch (NumberFormatException ex) {
            throw new MessageParsingException(paidAmtKey);
        }

        message.setPaymentType(message.getValue(paymentTypeKey));
        if (message.getPaymentType() == null)
            throw new MessageParsingException(paymentTypeKey);

        if (message.getValue(paymentTypeKey).equals("Prepaid")) {
            if (message.getValue(prepaidCatKey) != null)
                message.setPrepaidCat(message.getValue(prepaidCatKey));

            message.setValidationCode(message.getValue(validationCodeKey));
            if (message.getValidationCode() == null)
                throw new MessageParsingException(validationCodeKey);
        }

        message.setBillNo(message.getValue(billNoKey));
        message.setBillingNo(message.getValue(billingNoKey));

        message.setSenderAccount(message.getValue(senderAccountKey));
        message.setReceiverAccount(message.getValue(receiverAccountKey));

        return message;
    }

    private static Timestamp getCurrentTimestampWihtEfawateercomFormat(PaymentMessage message) throws ParseException {
        // TODO fix the ro83ah
        Date minus = new Date(System.currentTimeMillis() - 5 * 60 * 1000);
        return new Timestamp(minus.getTime());
    }

    public String getBillingNo() {
        return billingNo;
    }

    public void setBillingNo(String billingNo) {
        this.billingNo = billingNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public String getPaidAmt() {
        return paidAmt;
    }

    public void setPaidAmt(String paidAmt) {
        this.paidAmt = paidAmt;
    }

    public String getDueAmt() {
        return dueAmt;
    }

    public void setDueAmt(String dueAmt) {
        this.dueAmt = dueAmt;
    }

    public Timestamp getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Timestamp processDate) {
        this.processDate = processDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    public String getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    public String getPrepaidCat() {
        return prepaidCat;
    }

    public void setPrepaidCat(String prepaidCat) {
        this.prepaidCat = prepaidCat;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNUmber() {
        return idNUmber;
    }

    public void setIdNUmber(String idNUmber) {
        this.idNUmber = idNUmber;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }
}
