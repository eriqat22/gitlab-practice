package com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry.processor.exception;

public class AccountInquirySigningException extends RuntimeException {

	private static final long serialVersionUID = 1L;
}
