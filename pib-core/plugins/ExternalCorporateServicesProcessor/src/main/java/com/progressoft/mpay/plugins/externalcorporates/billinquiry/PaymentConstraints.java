package com.progressoft.mpay.plugins.externalcorporates.billinquiry;

import java.math.BigDecimal;

public class PaymentConstraints {
	private Boolean allowPart;
	private BigDecimal lower;
	private BigDecimal upper;

	public Boolean getAllowPart() {
		return allowPart;
	}

	public void setAllowPart(Boolean allowPart) {
		this.allowPart = allowPart;
	}

	public BigDecimal getLower() {
		return lower;
	}

	public void setLower(BigDecimal lower) {
		this.lower = lower;
	}

	public BigDecimal getUpper() {
		return upper;
	}

	public void setUpper(BigDecimal upper) {
		this.upper = upper;
	}
}
