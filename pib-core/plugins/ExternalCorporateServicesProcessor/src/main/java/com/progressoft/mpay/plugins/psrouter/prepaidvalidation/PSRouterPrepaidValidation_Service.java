package com.progressoft.mpay.plugins.psrouter.prepaidvalidation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import java.net.URL;

@WebServiceClient(name = "PSRouterPrepaidValidation", wsdlLocation = "PSRouterPrepaidValidation.wsdl", targetNamespace = "http://www.progressoft.com/PSRouterPrepaidValidation/")
public class PSRouterPrepaidValidation_Service extends Service {
	public final static QName SERVICE = new QName("http://www.progressoft.com/PSRouterPrepaidValidation/",
			"PSRouterPrepaidValidation");
	public final static QName PSRouterPrepaidValidationSOAP = new QName(
			"http://www.progressoft.com/PSRouterPrepaidValidation/", "PSRouterPrepaidValidationSOAP");
    private static final Logger logger = LoggerFactory.getLogger(PSRouterPrepaidValidation_Service.class);
    public static URL WSDL_LOCATION;

	static {

		// TODO: change location to be in properties file.
		WSDL_LOCATION = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try {
			URL url = classLoader.getResource("/config/app/wsdl/PSRouterPrepaidValidation.wsdl");
			if (url == null) {
				java.util.logging.Logger.getLogger(PSRouterPrepaidValidation_Service.class.getName()).log(
						java.util.logging.Level.INFO, "Can not initialize the default wsdl from {0}",
						"PSRouterPrepaidValidation.wsdl");
			}
			WSDL_LOCATION = url;
		} catch (Exception e) {
			logger.error("error... change me later", e);
		}

	}

	public PSRouterPrepaidValidation_Service(URL wsdlLocation) {
		super(wsdlLocation, SERVICE);
	}

	public PSRouterPrepaidValidation_Service(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public PSRouterPrepaidValidation_Service() {
		super(WSDL_LOCATION, SERVICE);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterPrepaidValidation_Service(WebServiceFeature... features) {
		super(WSDL_LOCATION, SERVICE, features);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterPrepaidValidation_Service(URL wsdlLocation, WebServiceFeature... features) {
		super(wsdlLocation, SERVICE, features);
	}

	// This constructor requires JAX-WS API 2.2. You will need to endorse the
	// 2.2
	// API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS
	// 2.1
	// compliant code instead.
	public PSRouterPrepaidValidation_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	/**
	 *
	 * @return returns PSRouterPrepaidValidation
	 */
	@WebEndpoint(name = "PSRouterPrepaidValidationSOAP")
	public PSRouterPrepaidValidation getPSRouterPrepaidValidationSOAP() {
		return super.getPort(PSRouterPrepaidValidationSOAP, PSRouterPrepaidValidation.class);
	}

	/**
	 *
	 * @param features
	 *            A list of {@link WebServiceFeature} to configure
	 *            on the proxy. Supported features not in the
	 *            <code>features</code> parameter will have their default
	 *            values.
	 * @return returns PSRouterPrepaidValidation
	 */
	@WebEndpoint(name = "PSRouterPrepaidValidationSOAP")
	public PSRouterPrepaidValidation getPSRouterPrepaidValidationSOAP(WebServiceFeature... features) {
		return super.getPort(PSRouterPrepaidValidationSOAP, PSRouterPrepaidValidation.class, features);
	}

}
