package com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry.processor;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.progressoft.accountinquriy.client.AccountInquiryClient;
import com.progressoft.accountinquriy.converter.AccountInquriyConverter;
import com.progressoft.accountinquriy.model.AccountInquriyRequestModel;
import com.progressoft.accountinquriy.model.AcctTypeModel;
import com.progressoft.accountinquriy.model.CustIdTypeModel;
import com.progressoft.accountinquriy.model.CustIdTypeModel.IdType;
import com.progressoft.accountinquriy.model.MessageBodyTypeModel;
import com.progressoft.accountinquriy.model.nation.Nationalities;
import com.progressoft.accountinquriy.request.AccountInquriyRequest;
import com.progressoft.accountinquriy.request.AcctType;
import com.progressoft.accountinquriy.request.MessageBodyType;
import com.progressoft.accountinquriy.response.PSRouterResponse;
import com.progressoft.accountinquriy.response.PSRouterResponse.ResBody.MsgBody.Acct.BillingsRec.BillingRec;
import com.progressoft.accountinquriy.response.PSRouterResponse.ResBody.MsgBody.Acct.CustId;
import com.progressoft.accountinquriy.response.PSRouterResponse.ResBody.MsgBody.Acct.Result;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ExternalIntegMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.IntegMessagesSource;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.externalcorporates.common.Constants;
import com.progressoft.mpay.plugins.externalcorporates.common.ExternalCorporatesValidator;
import com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry.AccountInquiryMessage;
import com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry.processor.exception.AccountInquiryParseMessageException;
import com.progressoft.mpay.plugins.externalcorporates.profile.accountinquiry.processor.exception.AccountInquirySigningException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AccountInquiryProcessor implements MessageProcessor {

    private static final String MESSAGE_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String SUCCESS_CODE = "0";
    private static final String BANK_JOEBPPSNO_NOT_FOUND = "307";
    private static final String CUSTOMER_ID_OR_JOEBPPSNO_NOT_EXSIST = "310";
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountInquriyRequest.class);
    private static JAXBContext jaxbContext;
    private PSRouterResponse response;
    private XMLGregorianCalendar generateGregorienDate = null;

    private static String createXmlContent(AccountInquriyRequest request) {
        try {
            if (jaxbContext == null)
                jaxbContext = JAXBContext.newInstance(AccountInquriyRequest.class, MessageBodyType.class,
                        AcctType.class);
            StringWriter writer = new StringWriter();
            jaxbContext.createMarshaller().marshal(request, writer);
            return writer.toString();
        } catch (JAXBException ex) {
            LOGGER.error("createXmlContent", ex);
            throw new AccountInquiryParseMessageException("Error while Create Xml Content");
        }
    }

    private static XMLGregorianCalendar generateGregorienDate() {
        try {
            SimpleDateFormat gregorianFormat = new SimpleDateFormat(MESSAGE_DATE_TIME_FORMAT);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianFormat.format(new Date()));
        } catch (Exception ex) {
            LOGGER.error("Exception in generateGregorienDate", ex);
            return null;
        }
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        try {
            preProcessMessage(context);
            return getResultAccordingToValidation(context, ExternalCorporatesValidator.validate(context));
        } catch (Exception ex) {
            LOGGER.error("Error when ProcessMessage", ex);
            return rejectMpayMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private void preProcessMessage(MessageProcessingContext context) {
        generateGregorienDate = generateGregorienDate();
        AccountInquiryMessage inquiryMessage = AccountInquiryMessage.parseMessage(context.getRequest());
        context.setRequest(inquiryMessage);
        ProcessingContextSide sender = new ProcessingContextSide();
        String senderType = inquiryMessage.getSenderType();
        if (senderType.equals(ReceiverInfoType.MOBILE)) {
            sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
            if (sender.getMobile() == null)
                return;
            context.setSender(sender);
            sender.setCustomer(sender.getMobile().getRefCustomer());
            sender.setMobileAccount(
                    SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
            if (sender.getMobileAccount() == null)
                return;
            sender.setProfile(sender.getMobileAccount().getRefProfile());
        } else if (senderType.equals(ReceiverInfoType.CORPORATE)) {

            sender.setService(context.getDataProvider().getCorporateService(inquiryMessage.getSender(),
                    inquiryMessage.getSenderType()));
            if (sender.getService() == null)
                return;
            context.setSender(sender);
            sender.setCorporate(sender.getService().getRefCorporate());
            sender.setServiceAccount(
                    SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
            if (sender.getServiceAccount() == null)
                return;
            sender.setProfile(sender.getServiceAccount().getRefProfile());
        }
    }

    private MessageProcessingResult getResultAccordingToValidation(MessageProcessingContext context,
                                                                   ValidationResult validationResult) {
        return validationResult.isValid() ? acceptMessage(context)
                : rejectMpayMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        AccountInquriyRequestModel accountInquriyRequestModel = prepareModelRequest(context);
        AccountInquriyRequest request = AccountInquriyConverter.getInstance()
                .toAccountInquriyRequest(accountInquriyRequestModel);

        MPAY_ExternalIntegMessage integMessage = createExternalIntegrationMessage(request, context);
        String accInquriyResponse = AccountInquiryClient.sendAccountUploadRequest(accountInquriyRequestModel)
                .getAccInquriyResponse();
        if (accInquriyResponse.contains("MPAY-"))
            return rejectMpayMessage(context, ReasonCodes.SERVICE_CURRENTLY_UNAVAILABLE,
                    "Service Currently Unavilable");
        response = PSRouterResponse.toObject(accInquriyResponse.replaceAll("&lt;", "<").replace("&gt;", ">"));

        return reflectStatusToExternalMessageAccordingToResponse(context, integMessage, response, accInquriyResponse);
    }

    private MessageProcessingResult reflectStatusToExternalMessageAccordingToResponse(MessageProcessingContext context,
                                                                                      MPAY_ExternalIntegMessage integrationMessage, PSRouterResponse response, String accInquiryResponse) {
        integrationMessage.setResponseContent(accInquiryResponse);
        if (SUCCESS_CODE.equals(response.getResCode())) {
            if (!isVerifiedRouterSignature(context, generateResponseSigetureData(response),
                    response.getResSignature())) {
                integrationMessage
                        .setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED));
                context.getDataProvider().updateExternalIntegrationMessage(integrationMessage);
                return rejectMpayMessage(context, ReasonCodes.INVALID_TOKEN, "Invalid Signature from router");
            }
            integrationMessage
                    .setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.ACCEPTED));
            context.getDataProvider().updateExternalIntegrationMessage(integrationMessage);
            return acceptMpayMessage(context, response);
        } else {
            integrationMessage
                    .setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.REJECTED));
            context.getDataProvider().updateExternalIntegrationMessage(integrationMessage);
            if (CUSTOMER_ID_OR_JOEBPPSNO_NOT_EXSIST.equals(response.getResCode())
                    || BANK_JOEBPPSNO_NOT_FOUND.equals(response.getResCode()))
                return rejectMpayMessage(context, ReasonCodes.NO_PROFILE_EXSIST, response.getResDesc());
            return rejectMpayMessage(context, ReasonCodes.SERVICE_INTEGRATION_REJECTION, response.getResDesc());
        }
    }

    private boolean isVerifiedRouterSignature(MessageProcessingContext context, String accUploadResponse,
                                              String resSignature) {
        return context.getCryptographer().verifyToken(context.getSystemParameters(), accUploadResponse, resSignature,
                null, IntegMessagesSource.E_FAWATERCOM);
    }

    private MessageProcessingResult acceptMpayMessage(MessageProcessingContext context, PSRouterResponse response) {
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, new Gson().toJson(response),
                ProcessingStatusCodes.ACCEPTED);
        result.setResponse(generateResponse(context, true));
        return result;
    }

    private String generateResponseSigetureData(PSRouterResponse response) {
        String message = response.getOriginaleMsgId() + response.getResDateTime().toString() + response.getResCode()
                + response.getResDesc() + collectMessageData(response);
        LOGGER.info("The values to sign is ", message);
        return message;
    }

    private String collectMessageData(PSRouterResponse response) {

        try {
            Result result = response.getResBody().getMsgBody().getAcct().getResult();
            CustId custId = response.getResBody().getMsgBody().getAcct().getCustId();
            String joebppsNo = response.getResBody().getMsgBody().getAcct().getJOEBPPSNo();
            String recCount = response.getResBody().getMsgBody().getAcct().getRecCount();
            return result.getErrorCode() + result.getErrorDesc() + result.getSeverity() + custId.getIdType()
                    + custId.getId() + custId.getNation() + setOptionalDataIfExist(custId) + joebppsNo + recCount
                    + generateBellerDataSignature(response);
        } catch (Exception ex) {
            LOGGER.error("Exception in collectMessageData", ex);
            throw new AccountInquirySigningException();
        }
    }

    private String setOptionalDataIfExist(CustId custId) {
        StringBuilder builder = new StringBuilder();
        if (custId.getName() != null && custId.getName().length() > 0)
            builder.append(custId.getName());
        if (custId.getPhone() != null && custId.getPhone().length() > 0)
            builder.append(custId.getPhone());
        if (custId.getAddress() != null && custId.getAddress().length() > 0)
            builder.append(custId.getAddress());
        if (custId.getEmail() != null && custId.getEmail().length() > 0)
            builder.append(custId.getEmail());
        return builder.toString();
    }

    private String generateBellerDataSignature(PSRouterResponse response) {
        String message = "";
        List<BillingRec> billingRecs = response.getResBody().getMsgBody().getAcct().getBillingsRec().getBillingRec();
        for (BillingRec record : billingRecs) {
            message = record.getBillerCode() + record.getBillingNo() + record.getServiceType()
                    + record.getBillingStatus();
        }
        return message;
    }

    private MPAY_ExternalIntegMessage createExternalIntegrationMessage(AccountInquriyRequest request,
                                                                       MessageProcessingContext context) {
        MPAY_ExternalIntegMessage integrationMessage = new MPAY_ExternalIntegMessage();
        integrationMessage.setRequestContent(createXmlContent(request));
        integrationMessage.setRequestDate(SystemHelper.getSystemTimestamp());
        integrationMessage.setRequestToken(request.getPspSignature());
        integrationMessage.setRequestID(request.getMessageId());
        integrationMessage.setReqField1(request.getPspId());
        integrationMessage.setReqField2(Constants.PS_ROUTER_NAME);
        integrationMessage.setRefMessage(context.getMessage());
        integrationMessage.setRefService(context.getReceiver().getService());
        integrationMessage.setRefStatus(context.getLookupsLoader().getProcessingStatus(ProcessingStatusCodes.PENDING));
        context.getDataProvider().persistExternalIntegrationMessage(integrationMessage);
        return integrationMessage;
    }

    private AccountInquriyRequestModel prepareModelRequest(MessageProcessingContext context) {
        return new AccountInquriyRequestModel.Builder().messageId(context.getRequest().getMsgId())
                .pspId(((AccountInquiryMessage) context.getRequest()).getPspId()).messageDateTime(generateGregorienDate)
                .messageBodyTypeModel(generateMessageBodyType(context)).pspSignature(generatePspSignature(context))
                .build();
    }

    private String generatePspSignature(MessageProcessingContext context) {
        StringBuilder builder = new StringBuilder();
        AccountInquiryMessage accountInquiryMessage = (AccountInquiryMessage) context.getRequest();

        builder.append(accountInquiryMessage.getPspId()).append(accountInquiryMessage.getMsgId())
                .append(generateGregorienDate)
                .append(IdType.getRouterCodeByIdType(context.getSender().getCustomer().getIdType().getCode()).name())
                .append(context.getSender().getMobile().getRefCustomer().getIdNum())
                .append(context.getSender().getMobile().getRefCustomer().getNationality().getDescription())
                .append(accountInquiryMessage.getEfawatercomCustomerId());

        LOGGER.info("The values to sign is " + builder.toString());

        return context.getCryptographer().generateToken(context.getSystemParameters(), builder.toString(), "",
                IntegMessagesSource.SYSTEM);
    }

    private MessageBodyTypeModel generateMessageBodyType(MessageProcessingContext context) {
        return new MessageBodyTypeModel.Builder().acctTypeModel(generateAccTypeModel(context)).build();
    }

    private AcctTypeModel generateAccTypeModel(MessageProcessingContext context) {
        return new AcctTypeModel.Builder().custIdType(generateCustomerType(context)).build();
    }

    private CustIdTypeModel generateCustomerType(MessageProcessingContext context) {
        return new CustIdTypeModel.Builder().id(context.getSender().getCustomer().getIdNum())
                .idType(IdType.getRouterCodeByIdType(context.getSender().getCustomer().getIdType().getCode()))
                .nation(Nationalities.getCodeByDescription(context.getSender().getCustomer().getNationality().getDescription()))
                .build();
    }

    private MessageProcessingResult rejectMpayMessage(MessageProcessingContext context, String reasonCode,
                                                      String reasonDescription) {
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
                ProcessingStatusCodes.REJECTED);
        String rejectionResponse = generateResponse(context, false);
        result.getMessage().setResponseContent(rejectionResponse);
        result.setResponse(rejectionResponse);
        return result;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        String description;
        MPayResponse mpayResponse = new MPayResponse();
        mpayResponse.setErrorCd(context.getMessage().getReason().getCode());
        mpayResponse.setRef(Long.parseLong(context.getMessage().getReference()));
        checkIsContextLanguageIsNull(context);
        description = prepareMpayReasonsNls(context);
        mpayResponse.setDesc(description);
        mpayResponse.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        ifIsAccepted(isAccepted, mpayResponse);
        return mpayResponse.toString();
    }

    private void ifIsAccepted(boolean isAccepted, MPayResponse mpayResponse) {
        if (isAccepted) {
            mpayResponse.getExtraData()
                    .add(new ExtraData("joebppsNo", response.getResBody().getMsgBody().getAcct().getJOEBPPSNo()));
            mpayResponse.getExtraData().add(new ExtraData("records", convertBillerListToJson(
                    response.getResBody().getMsgBody().getAcct().getBillingsRec().getBillingRec())));
        }
    }

    private String convertBillerListToJson(List<BillingRec> billingRec) {
        return new Gson().toJson(billingRec, new TypeToken<List<BillingRec>>() {
        }.getType());
    }

    private void checkIsContextLanguageIsNull(MessageProcessingContext context) {
        if (context.getLanguage() == null)
            context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
    }

    private String prepareMpayReasonsNls(MessageProcessingContext context) {
        String description;
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
                    SystemParameters.getInstance().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        return description;
    }

    private MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
                                                 String reasonDescription, String processingStatus) {
        MPAY_ProcessingStatus status = prepareMessageContext(context, reasonCode, reasonDescription, processingStatus);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private MPAY_ProcessingStatus prepareMessageContext(MessageProcessingContext context, String reasonCode,
                                                        String reasonDescription, String processingStatus) {
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        return status;
    }

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext context) {
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext context) {
        return null;
    }
}