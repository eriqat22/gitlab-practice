package com.progressoft.mpay.plugins.externalcorporates.payment.validation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PaymentValidationResponse {

	private String billingNo;
	private String billerCode;
	private double dueAmt;
	private double feesAmt;
	private String validationCode;
	private String msgLabel;
	private String serviceType;
	private String prepaidCat;

	public double getDueAmt() {
		return dueAmt;
	}

	public void setDueAmt(double dueAmt) {
		this.dueAmt = dueAmt;
	}

	public double getFeesAmt() {
		return feesAmt;
	}

	public void setFeesAmt(double feesAmt) {
		this.feesAmt = feesAmt;
	}

	public String getValidationCode() {
		return validationCode;
	}

	public void setValidationCode(String validationCode) {
		this.validationCode = validationCode;
	}

	public String getMsgLabel() {
		return msgLabel;
	}

	public void setMsgLabel(String msgLabel) {
		this.msgLabel = msgLabel;
	}

	public String getBillerCode() {
		return billerCode;
	}

	public void setBillerCode(String billerCode) {
		this.billerCode = billerCode;
	}

	public String getBillingNo() {
		return billingNo;
	}

	public void setBillingNo(String billingNo) {
		this.billingNo = billingNo;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getPrepaidCat() {
		return prepaidCat;
	}

	public void setPrepaidCat(String prepaidCat) {
		this.prepaidCat = prepaidCat;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(this);
		return json;
	}
}
