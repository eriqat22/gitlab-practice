package com.progressoft.mpay.plugins.externalcorporates.payment.validation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PaymentValidationResponseObject {
	private String billingNo;
	private int billerCode;
	private String dueAmt;
	private String feesAmnt;
	private String validationCode;
	private String msgLabel;
	private String serviceType;
	private String prepaidCat;

	public String getBillingNo() {
		return billingNo;
	}

	public void setBillingNo(String billingNo) {
		this.billingNo = billingNo;
	}

	public int getBillerCode() {
		return billerCode;
	}

	public void setBillerCode(int billerCode) {
		this.billerCode = billerCode;
	}

	public String getDueAmt() {
		return dueAmt;
	}

	public void setDueAmt(String dueAmt) {
		this.dueAmt = dueAmt;
	}

	public String getFeesAmnt() {
		return feesAmnt;
	}

	public void setFeesAmnt(String feesAmnt) {
		this.feesAmnt = feesAmnt;
	}

	public String getValidationCode() {
		return validationCode;
	}

	public void setValidationCode(String validationCode) {
		this.validationCode = validationCode;
	}

	public String getMsgLabel() {
		return msgLabel;
	}

	public void setMsgLabel(String msgLabel) {
		this.msgLabel = msgLabel;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getPrepaidCat() {
		return prepaidCat;
	}

	public void setPrepaidCat(String prepaidCat) {
		this.prepaidCat = prepaidCat;
	}

	@Override
	public String toString() {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(this);
		return json;
	}
}
