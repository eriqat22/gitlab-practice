package com.progressoft.mpay.plugins.externalcorporates.payment;

import com.progressoft.mpay.*;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.BankedUnbankedFlag;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class NotificationProcessor {
    private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

    public static List<MPAY_Notification> CreateAcceptanceNotificationMessages(ProcessingContext context) {
        logger.debug("Inside CreateAcceptanceNotificationMessages ...");
        if (context.getSender().getMobile() != null)
            return CreateAcceptanceCustomerNotificationMessages(context);
        if (context.getSender().getService() != null)
            return CreateAcceptanceCorporateNotificationMessages(context);
        return new ArrayList<>();
    }

    private static List<MPAY_Notification> CreateAcceptanceCorporateNotificationMessages(ProcessingContext context) {
        try {
            String sender;
            String reference;
            long senderLanguageId = 0;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            MPAY_CorpoarteService senderService = context.getSender().getService();
            MPAY_EndPointOperation operation = context.getOperation();

            PaymentNotificationContext notificationContext = new PaymentNotificationContext();
            notificationContext
                    .setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setOperation(operation.getOperation());

            notificationContext.setReference(reference);
            notificationContext.setReceiver(context.getReceiver().getInfo());

            if (senderService != null) {
                sender = senderService.getName();
                DataProvider.instance().refreshEntity(context.getSender().getAccount());
                notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
                        context.getSender().getAccount().getBalance()));
                notificationContext
                        .setSenderBanked(senderService.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setSenderCharges(
                        SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
                if (context.getSender().getCorporate() != null)
                    senderLanguageId = context.getSender().getCorporate().getPrefLang().getId();
                else
                    senderLanguageId = SystemParameters.getInstance().getSystemLanguage().getId();
            } else
                sender = context.getSender().getInfo();

            if (context.getReceiver().getNotes() != null) {
                notificationContext.setNotes(context.getReceiver().getNotes());
                notificationContext.setHasNotes(true);
            }

            notificationContext.setSender(sender);
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (senderService != null) {
                notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(),
                        notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
                        senderService.getNotificationReceiver(), senderService.getNotificationChannel().getCode()));
            }

            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateAcceptanceNotificationMessages", e);
            return null;
        }
    }

    private static List<MPAY_Notification> CreateAcceptanceCustomerNotificationMessages(ProcessingContext context) {
        try {
            String sender = null;
            String reference = null;
            long senderLanguageId = 0;
            if (context.getMessage() == null)
                reference = context.getTransaction().getReference();
            else
                reference = context.getMessage().getReference();
            MPAY_CustomerMobile senderMobile = context.getSender().getMobile();
            MPAY_EndPointOperation operation = context.getOperation();

            PaymentNotificationContext notificationContext = new PaymentNotificationContext();
            notificationContext
                    .setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
            notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setOperation(operation.getOperation());
            notificationContext.setReference(reference);

            notificationContext.setReceiver(context.getReceiver().getInfo());

            if (senderMobile != null) {
                sender = senderMobile.getMobileNumber();
                DataProvider.instance().refreshEntity(context.getSender().getAccount());
                notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(),
                        context.getSender().getAccount().getBalance()));
                notificationContext.setSenderBanked(senderMobile.getBankedUnbanked().equals(BankedUnbankedFlag.BANKED));
                notificationContext.setSenderCharges(
                        SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
                if (context.getSender().getCustomer() != null)
                    senderLanguageId = context.getSender().getCustomer().getPrefLang().getId();
                else
                    senderLanguageId = SystemParameters.getInstance().getSystemLanguage().getId();
                if (senderMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS)
                        && senderMobile.getAlias() != null && !senderMobile.getAlias().isEmpty())
                    sender = senderMobile.getAlias();
            } else
                sender = context.getSender().getInfo();

            if (context.getReceiver().getNotes() != null) {
                notificationContext.setNotes(context.getReceiver().getNotes());
                notificationContext.setHasNotes(true);
            }

            notificationContext.setSender(sender);
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (senderMobile != null) {
                notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(),
                        notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId,
                        senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
            }

            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateAcceptanceNotificationMessages", e);
            return null;
        }
    }

    public static List<MPAY_Notification> CreateRejectionNotificationMessages(ProcessingContext context) {
        if (context.getSender().getMobile() != null)
            return CreateRejectionCustomerNotificationMessages(context);
        if (context.getSender().getService() != null)
            return CreateRejectionCorporateNotificationMessages(context);
        return new ArrayList<>();
    }

    public static List<MPAY_Notification> CreateRejectionCustomerNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            PaymentMessage request = PaymentMessage.ParseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            String senderMobile = request.getSender();
            MPAY_CustomerMobile mobile = DataProvider.instance().getCustomerMobile(request.getSender());
            MPAY_Language language = context.getSystemParameters().getSystemLanguage();
            if(mobile != null) {
                senderMobile = mobile.getMobileNumber();
                language = mobile.getRefCustomer().getPrefLang();
            }
            PaymentNotificationContext notificationContext = new PaymentNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            if (context.getTransaction() == null)
                notificationContext.setCurrency(SystemParameters.getInstance().getDefaultCurrency().getStringISOCode());
            else
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(),
                    BigDecimal.valueOf(Double.valueOf(request.getPaidAmt()))));
            notificationContext.setReceiver(context.getTransaction().getReceiverInfo());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(
                    MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
            notifications
                    .add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext,
                            NotificationType.REJECTED_SENDER, context.getMessage().getRefOperation().getId(), language.getId(),
                            senderMobile, NotificationChannelsCode.SMS));
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateRejectionNotificationMessages", e);
            return null;
        }
    }

    public static List<MPAY_Notification> CreateRejectionCorporateNotificationMessages(ProcessingContext context) {
        try {
            logger.debug("Inside CreateRejectionNotificationMessages ...");
            List<MPAY_Notification> notifications = new ArrayList<>();
            if (context.getMessage() == null)
                return notifications;
            if (!context.getMessage().getReason().getSmsEnabled())
                return notifications;
            PaymentMessage request = PaymentMessage
                    .ParseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
            MPAY_CorpoarteService service = context.getSender().getService();
            PaymentNotificationContext notificationContext = new PaymentNotificationContext();
            notificationContext.setReference(context.getMessage().getReference());
            notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
            if (context.getTransaction() == null)
                notificationContext.setCurrency(SystemParameters.getInstance().getDefaultCurrency().getStringISOCode());
            else
                notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
            notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(),
                    BigDecimal.valueOf(Double.valueOf(request.getPaidAmt()))));
            notificationContext.setReceiver(context.getReceiver().getInfo());
            notificationContext.setReasonCode(context.getMessage().getReason().getCode());
            notificationContext.setReasonDescription(
                    MPayHelper.getReasonNLS(context.getMessage().getReason(), service.getRefCorporate().getPrefLang()).getDescription());
            notifications
                    .add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext,
                            NotificationType.REJECTED_SENDER, context.getMessage().getRefOperation().getId(), service.getRefCorporate().getPrefLang().getId(),
                            service.getNotificationReceiver(), service.getNotificationChannel().getCode()));
            return notifications;
        } catch (Exception e) {
            logger.error("Error when CreateRejectionNotificationMessages", e);
            return null;
        }
    }
}
