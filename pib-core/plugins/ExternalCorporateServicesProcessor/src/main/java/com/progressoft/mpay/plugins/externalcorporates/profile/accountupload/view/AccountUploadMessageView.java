package com.progressoft.mpay.plugins.externalcorporates.profile.accountupload.view;

public interface AccountUploadMessageView {

	String getMessageDate();
	String getIdType();
	String getId();
	String getNation();
	String getPhone();
	String getAddress();
	String getEmail();
	String getName();
	String getPspId();
}