package com.progressoft.mpay.plugins.externalcorporates.profile.addcustomerbilling.processor.exception;

public class AddCustomerBillingSigningException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public AddCustomerBillingSigningException(Exception e) {
		super(e);
	}

}
