package com.progressoft.mpay.plugins.externalcorporates.payment.postpaid;

import com.opensymphony.util.GUID;
import com.progressoft.mpay.plugins.externalcorporates.common.EfawateercomGregorianTimeUtil;
import com.progressoft.mpay.plugins.externalcorporates.payment.PaymentMessage;
import com.progressoft.mpay.plugins.externalcorporates.payment.postpaid.Transactions.TrxInf;
import com.progressoft.mpay.plugins.externalcorporates.payment.postpaid.Transactions.TrxInf.AcctInfo;
import com.progressoft.mpay.plugins.externalcorporates.payment.postpaid.Transactions.TrxInf.PayerInfo;
import com.progressoft.mpay.plugins.externalcorporates.payment.postpaid.Transactions.TrxInf.ServiceTypeDetails;

public class PostPaidObjectBuilderImp implements PostPaidObjectBuilder {

    private static final String ACCESS_CHANNEL = "PSP";
    private static final String PAYMENT_STATUS = "PmtNew";
    private static final String PAYMENT_METHOD = "ACTDEB";

    @Override
    public Transactions buildPostPaidTransaction(PaymentMessage message) {
        return preparePostPaidTransactionObject(message);
    }

    private Transactions preparePostPaidTransactionObject(PaymentMessage message) {
        Transactions postPaidTransaction = new Transactions();
        postPaidTransaction.setTrxInf(createTransactionInformation(message));
        return postPaidTransaction;
    }

    private TrxInf createTransactionInformation(PaymentMessage message) {
        TrxInf transactionInformation = new TrxInf();
        transactionInformation.setAcctInfo(prepareAccountInfo(message));
        transactionInformation.setAccessChannel(ACCESS_CHANNEL);
        transactionInformation.setPmtStatus(PAYMENT_STATUS);
        transactionInformation.setBankTrxID(GUID.generateGUID());
        transactionInformation.setDueAmt(message.getDueAmt());
        transactionInformation.setPaidAmt(message.getPaidAmt());
        transactionInformation.setPaymentMethod(PAYMENT_METHOD);
        transactionInformation.setProcessDate(EfawateercomGregorianTimeUtil.generateGregorianFormatDate(message.getProcessDate()));
        transactionInformation.setServiceTypeDetails(prepareSreviceTypeDetails(message));
        transactionInformation.setPayerInfo(generatePayerInfo(message));
        return transactionInformation;
    }

    private PayerInfo generatePayerInfo(PaymentMessage message) {
        PayerInfo info = new PayerInfo();
        info.setId(message.getIdNUmber());
        info.setIdType(message.getIdType());
        info.setNation(message.getNation());
        return info;
    }

    private AcctInfo prepareAccountInfo(PaymentMessage message) {
        AcctInfo accountInfo = new AcctInfo();
        accountInfo.setBillerCode(message.getBillerCode());
        accountInfo.setBillingNo(message.getBillingNo());
        accountInfo.setBillNo(message.getBillNo());
        return accountInfo;
    }

    private ServiceTypeDetails prepareSreviceTypeDetails(PaymentMessage message) {
        ServiceTypeDetails serviceTypeDetails = new ServiceTypeDetails();
        serviceTypeDetails.setServiceType(message.getServiceType());
        return serviceTypeDetails;
    }
}