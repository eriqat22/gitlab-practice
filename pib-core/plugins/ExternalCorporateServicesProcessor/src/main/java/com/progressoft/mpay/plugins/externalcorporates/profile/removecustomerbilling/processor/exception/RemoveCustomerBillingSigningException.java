package com.progressoft.mpay.plugins.externalcorporates.profile.removecustomerbilling.processor.exception;

public class RemoveCustomerBillingSigningException extends RuntimeException {

	public RemoveCustomerBillingSigningException(Exception e) {
		super(e);
	}

	private static final long serialVersionUID = 1L;

}
