package com.progressoft.mpay.plugins.externalcorporates.billinquiry;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_ExternalIntegMessage;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.externalcorporates.common.Constants;
import com.progressoft.mpay.plugins.externalcorporates.common.ExternalCorporatesRequestsHelper;
import com.progressoft.mpay.plugins.externalcorporates.common.ExternalCorporatesValidator;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BillInquiryProcessor implements MessageProcessor {

	private static final Logger logger = LoggerFactory.getLogger(BillInquiryProcessor.class);

	@Override
	public IntegrationProcessingResult processIntegration(
			IntegrationProcessingContext paramIntegrationProcessingContext) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext messageContext) {
		logger.debug("Inside ProcessMessage ...");
		if (messageContext == null)
			throw new NullArgumentException("messageContext");

		try {
			preProcessMessage(messageContext);
			ValidationResult validationResult = ExternalCorporatesValidator.validate(messageContext);
			if (validationResult.isValid())
				return AcceptMessage(messageContext);
			else
				return RejectMessage(messageContext, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			return RejectMessage(messageContext, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in BillInquiryProcessor", ex);
			return RejectMessage(messageContext, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
		logger.debug("Inside PreProcessMessage ...");
		BillInquiryMessage message = BillInquiryMessage.ParseMessage(context.getRequest());
		context.setRequest(message);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);

		String senderType = message.getSenderType();

		if (senderType.equals(ReceiverInfoType.MOBILE)) {
			sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
			if (sender.getMobile() == null)
				return;
			sender.setCustomer(sender.getMobile().getRefCustomer());
			sender.setMobileAccount(
					SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
			if (sender.getMobileAccount() == null)
				return;
			sender.setProfile(sender.getMobileAccount().getRefProfile());
		} else if (senderType.equals(ReceiverInfoType.CORPORATE)) {
			sender.setService(context.getDataProvider().getCorporateService(message.getSender(), senderType));
			if (sender.getService() == null)
				return;
			sender.setCorporate(sender.getService().getRefCorporate());
			sender.setServiceAccount(
					SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
			if (sender.getServiceAccount() == null)
				return;
			sender.setProfile(sender.getServiceAccount().getRefProfile());
		}
		OTPHanlder.loadOTP(context, message.getPin());
	}

	private MessageProcessingResult RejectMessage(MessageProcessingContext context, String reasonCode,
                                                  String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = CreateResult(context, reasonCode, reasonDescription,
				ProcessingStatusCodes.REJECTED);
		String response = GenerateResponse(context, null, false);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult AcceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		MessageProcessingResult result = CreateResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);

		MPAY_ExternalIntegMessage integMessage = ExternalCorporatesRequestsHelper.GenerateBillInquiryRequest(context);
		if (integMessage == null)
			return RejectMessage(context, ReasonCodes.SERVICE_INTEGRATION_FAILURE,
					"Cannot generate integration message");

		DataProvider.instance().persistExternalIntegrationMessage(integMessage);
		integMessage = ExternalCorporatesRequestsHelper.SendBillInquiryRequest(integMessage, context);
		DataProvider.instance().updateExternalIntegrationMessage(integMessage);

		if (!integMessage.getRefStatus().getCode().equals(ProcessingStatusCodes.ACCEPTED))
			return RejectMessage(context, context.getMessage().getReason().getCode(),
					context.getMessage().getReasonDesc());

		if (ExternalCorporatesRequestsHelper.CreateBillInquiryResponse(context) == null) {
			return RejectMessage(context, ReasonCodes.BILL_NOT_FOUND, null);
		}

		String response = GenerateResponse(context, integMessage, true);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private String GenerateResponse(MessageProcessingContext context, MPAY_ExternalIntegMessage integMessage,
                                    boolean isAccepted) {
		logger.debug("Inside GenerateResponse ...");
		logger.debug("isAccepted: " + isAccepted);
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description = null;
		if (context.getLanguage() == null)
			context.setLanguage(SystemParameters.getInstance().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
				context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
					SystemParameters.getInstance().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());

		if (isAccepted) {
			BillInqResponseObject createBillInquiryResponse = ExternalCorporatesRequestsHelper
					.CreateBillInquiryResponse(context);
			response.getExtraData()
					.add(new ExtraData(Constants.billingNoKey, createBillInquiryResponse.getBillingNo()));
			response.getExtraData().add(new ExtraData(Constants.billNoKey, createBillInquiryResponse.getBillNo()));
			response.getExtraData()
					.add(new ExtraData(Constants.billStatusKey, createBillInquiryResponse.getBillStatus()));
			response.getExtraData().add(new ExtraData(Constants.dueAmntKey, createBillInquiryResponse.getDueAmnt()));
			response.getExtraData().add(new ExtraData(Constants.feesAmntKey, createBillInquiryResponse.getFeesAmnt()));
			response.getExtraData()
					.add(new ExtraData(Constants.serviceTypeKey, createBillInquiryResponse.getServiceType()));
			response.getExtraData().add(
					new ExtraData(Constants.allowPartKey, String.valueOf(createBillInquiryResponse.getAllowPart())));
			response.getExtraData().add(new ExtraData(Constants.upperKey, createBillInquiryResponse.getUpper()));
			response.getExtraData().add(new ExtraData(Constants.lowerKey, createBillInquiryResponse.getLower()));
			response.getExtraData().add(
					new ExtraData(Constants.issueDateKey, String.valueOf(createBillInquiryResponse.getIssueDate())));
			String dueDate = createBillInquiryResponse.getDueDate();
			response.getExtraData()
					.add(new ExtraData(Constants.dueDateKey, dueDate == null ? "" : String.valueOf(dueDate)));
			// response.getExtraData().add(new ExtraData(Constants.billPmtsKey,
			// ExternalCorporatesRequestsHelper.CreateBillInquiryResponse(context).getBillPmts().toString()));
		}

		return response.toString();
	}

	public MessageProcessingResult CreateResult(MessageProcessingContext context, String reasonCode,
                                                String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext paramMessageProcessingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext paramMessageProcessingContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext paramMessageProcessingContext) {
		// TODO Auto-generated method stub
		return null;
	}

}
