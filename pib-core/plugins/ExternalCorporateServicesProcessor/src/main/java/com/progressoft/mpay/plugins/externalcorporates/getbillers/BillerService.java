package com.progressoft.mpay.plugins.externalcorporates.getbillers;

import java.util.List;

public class BillerService {
	private String serviceType;
	private String enDesc;
	private String arDesc;
	private String paymentType;
	private Boolean billingNoRequired;
	private Boolean containsPrePaidCats;
	private List<PrePaidCategories> prepaidCats;

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getEnDesc() {
		return enDesc;
	}

	public void setEnDesc(String enDesc) {
		this.enDesc = enDesc;
	}

	public String getArDesc() {
		return arDesc;
	}

	public void setArDesc(String arDesc) {
		this.arDesc = arDesc;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Boolean isBillingNoRequired() {
		return billingNoRequired;
	}

	public void setBillingNoRequired(Boolean billingNoRequired) {
		this.billingNoRequired = billingNoRequired;
	}

	public Boolean isContainsPrePaidCats() {
		return containsPrePaidCats;
	}

	public void setContainsPrePaidCats(Boolean containsPrePaidCats) {
		this.containsPrePaidCats = containsPrePaidCats;
	}

	public List<PrePaidCategories> getPrepaidCats() {
		return prepaidCats;
	}

	public void setPrepaidCats(List<PrePaidCategories> prepaidCats) {
		this.prepaidCats = prepaidCats;
	}
}
