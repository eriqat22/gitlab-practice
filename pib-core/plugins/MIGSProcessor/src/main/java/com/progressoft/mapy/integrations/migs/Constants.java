package com.progressoft.mapy.integrations.migs;

public class Constants {
	public static final String AuthorizeIdKey = "vpc_AuthorizeId";
	public static final String ReferenceKey = "vpc_Reference";
}
