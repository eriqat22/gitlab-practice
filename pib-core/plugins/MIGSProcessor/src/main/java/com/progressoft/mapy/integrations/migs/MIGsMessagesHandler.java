package com.progressoft.mapy.integrations.migs;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.migs.request.MigsRequest;
import com.progressoft.mpay.migs.response.MigsResponse;

public class MIGsMessagesHandler {
	private static final Logger logger = LoggerFactory.getLogger(MIGsMessagesHandler.class);
	private JAXBContext jaxbContext = null;
	private static MIGsMessagesHandler _instance;

	private MIGsMessagesHandler() throws JAXBException {
		jaxbContext = JAXBContext.newInstance(MigsRequest.class, MigsResponse.class);
	}

	public static MIGsMessagesHandler Instance() throws JAXBException {
		if (_instance == null)
			_instance = new MIGsMessagesHandler();

		return _instance;
	}

	public String marshallMigsObject(Object object) throws JAXBException, IOException {
		logger.debug("marshallMigsObject");
		StringWriter writer = null;
		try {
			Marshaller marshaller = jaxbContext.createMarshaller();
			writer = new StringWriter();
			marshaller.marshal(object, writer);
			return writer.toString();
		} catch (Exception ex) {
			return null;
		} finally {
			if (writer != null)
				writer.close();
		}
	}

	public MigsRequest unmarshallMigsRequest(String request) throws JAXBException, IOException {
		logger.debug("unmarshallMigsRequest");
		logger.debug("request:");
		logger.debug(request);

		StringReader reader = null;
		try {
			Unmarshaller marshaller = jaxbContext.createUnmarshaller();
			reader = new StringReader(request);
			return (MigsRequest) marshaller.unmarshal(reader);
		} catch (Exception ex) {
			logger.error("Error while unmarshaling MigsRequest", ex);
			return null;
		} finally {
			if (reader != null)
				reader.close();
		}
	}

	public MigsResponse unmarshallMigsResponse(String response) throws JAXBException, IOException {
		logger.debug("unmarshallMigsResponse");
		logger.debug("response:");
		logger.debug(response);

		StringReader reader = null;
		try {
			Unmarshaller marshaller = jaxbContext.createUnmarshaller();
			reader = new StringReader(response);
			return (MigsResponse) marshaller.unmarshal(reader);
		} catch (Exception ex) {
			logger.error("Error while unmarshaling MigsResponse", ex);
			return null;
		} finally {
			if (reader != null)
				reader.close();
		}
	}
}
