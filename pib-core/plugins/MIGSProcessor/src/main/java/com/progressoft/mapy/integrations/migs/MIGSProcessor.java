package com.progressoft.mapy.integrations.migs;

import org.apache.commons.lang.NullArgumentException;

import com.ibm.icu.math.BigDecimal;
import com.progressoft.mpay.banksintegration.BankIntegrationContext;
import com.progressoft.mpay.banksintegration.BankIntegrationMethod;
import com.progressoft.mpay.banksintegration.BankIntegrationProcessor;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.MPAY_BankIntegMessage;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.migs.request.MigsRequest;
import com.progressoft.mpay.migs.response.MigsResponse;

public  class MIGSProcessor implements BankIntegrationProcessor {

	@Override
	public MPAY_BankIntegMessage createChargeMessage(BankIntegrationContext context) {
		return null;
	}

	@Override
	public MPAY_BankIntegMessage createMessage(BankIntegrationContext context) {
		if (context.getMethod().equals(BankIntegrationMethod.ACCOUNT_DEPOSIT))
			return new MPAY_BankIntegMessage();
		MPayRequest request = MPayRequest.fromJson(context.getProcessingContext().getMessage().getRequestContent());
		return context.getDataProvider().getBankIntegrationMessage(request.getValue(Constants.ReferenceKey));
	}

	@Override
	public MPAY_BankIntegMessage createReversalMessage(BankIntegrationContext context) {
		return null;
	}

	@Override
	public BankIntegrationResult processMessage(BankIntegrationContext context) {
		if (context == null)
			throw new NullArgumentException("context");
		if (context.getMethod() == BankIntegrationMethod.ACCOUNT_DEPOSIT)
			return createResult(null, ProcessingStatusCodes.ACCEPTED, null, ReasonCodes.VALID);
		MPAY_BankIntegMessage integMessage = context.getIntegMessage();
		MPayRequest request = MPayRequest.fromJson(context.getProcessingContext().getMessage().getRequestContent());
		try {
			MigsRequest integRequest = MIGsMessagesHandler.Instance().unmarshallMigsRequest(integMessage.getRequestContent());
			if (integRequest == null)
				return createResult(integMessage, BankIntegrationStatus.FAILED, "Failed to transform integration request", ReasonCodes.BANK_INTEGRATION_FAILURE);
			if (integMessage.getResponseContent() == null || integMessage.getResponseContent().trim().length() == 0)
				return createResult(integMessage, BankIntegrationStatus.REJECTED, "Response not received from MIGs", ReasonCodes.BANK_INTEGRATION_REJECTION);
			if (integMessage.getRefStatus().getCode().equals(BankIntegrationStatus.ACCEPTED))
				return createResult(integMessage, BankIntegrationStatus.REJECTED, "Bank Request already processed", ReasonCodes.BANK_INTEGRATION_REJECTION);
			MigsResponse integResponse = MIGsMessagesHandler.Instance().unmarshallMigsResponse(integMessage.getResponseContent());
			if (integResponse == null)
				return createResult(integMessage, BankIntegrationStatus.FAILED, "Failed to transform integration response", ReasonCodes.BANK_INTEGRATION_FAILURE);
			String reference = request.getValue(Constants.ReferenceKey);
			if (!context.getProcessingContext().getSender().getMobile().getMobileNumber().equals(integRequest.getMobileNumber()))
				return createResult(integMessage, BankIntegrationStatus.REJECTED, "Invalid Sender Mobile", ReasonCodes.BANK_INTEGRATION_REJECTION);
			if (reference == null || reference.trim().length() == 0 || !reference.equals(integRequest.getVpcMerchTxnRef()))
				return createResult(integMessage, BankIntegrationStatus.REJECTED, "Invalid Reference ID", ReasonCodes.BANK_INTEGRATION_REJECTION);
			if (!integRequest.getMobileNumber().equals(context.getProcessingContext().getSender().getMobile().getMobileNumber()))
				return createResult(integMessage, BankIntegrationStatus.REJECTED, "Invalid Mobile Number", ReasonCodes.BANK_INTEGRATION_REJECTION);
			String authorizeId = request.getValue(Constants.AuthorizeIdKey);
			if (authorizeId == null || !authorizeId.equals(integResponse.getVpcAuthorizeId()))
				return createResult(integMessage, BankIntegrationStatus.REJECTED, "Invalid Authorize ID", ReasonCodes.BANK_INTEGRATION_REJECTION);
			if (integMessage.getRefStatus().getCode().equals(BankIntegrationStatus.REJECTED))
				return createResult(integMessage, BankIntegrationStatus.REJECTED, "Request Rejected, reason: " + integResponse.getVpcTxnResponseCode() + " - " + integResponse.getVpcMessage(),
						ReasonCodes.BANK_INTEGRATION_REJECTION);
			if (integMessage.getRefStatus().getCode().equals(BankIntegrationStatus.FAILED))
				return createResult(integMessage, BankIntegrationStatus.FAILED, integMessage.getStatusDescription(), ReasonCodes.BANK_INTEGRATION_FAILURE);
			if (context.getProcessingContext().getAmount().equals(getAmount(context, integRequest.getVpcAmount())))
				return createResult(integMessage, BankIntegrationStatus.REJECTED, "Invalid Amount", ReasonCodes.BANK_INTEGRATION_REJECTION);
			integMessage.setRefStatus(context.getLookupsLoader().getBankIntegStatus(BankIntegrationStatus.ACCEPTED));
			integMessage.setRefTransaction(context.getTransaction());
			return createResult(integMessage, BankIntegrationStatus.ACCEPTED, null, ReasonCodes.VALID);

		} catch (Exception e) {
			return createResult(integMessage, BankIntegrationStatus.FAILED, e.getMessage(), ReasonCodes.BANK_INTEGRATION_FAILURE);
		}
	}

	private BigDecimal getAmount(BankIntegrationContext context, String value) {
		int decimalCount = context.getSystemParameters().getDecimalCount();
		double amount = Double.parseDouble(value);
		for (int i = 0; i < decimalCount; i++)
			amount = amount / 10;
		return BigDecimal.valueOf(amount);
	}

	@Override
	public BankIntegrationResult reverseMessage(BankIntegrationContext context) {
		return null;
	}

	private BankIntegrationResult createResult(MPAY_BankIntegMessage integMessage, String integStatusCode, String integStatusDescription, String reasonCode) {
		BankIntegrationResult result = new BankIntegrationResult();
		result.setReasonCode(reasonCode);
		result.setReasonDescription(integStatusDescription);
		result.setIntegMessage(integMessage);
		result.setStatus(integStatusCode);
		return result;
	}

	@Override
	public BankIntegrationResult processBalanceInquiryMessage(BankIntegrationContext context) {
		return null;
	}

	@Override
	public MPAY_BankIntegMessage createBalanceInquiryMessage(BankIntegrationContext context) {
		return null;
	}
}
