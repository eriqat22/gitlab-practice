package com.progressoft.mpay.plugins.mepspay.integrations;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.workflow.WorkflowException;
import com.progressoft.mpay.ChargesCalculator;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.banksintegration.BankIntegrationHandler;
import com.progressoft.mpay.banksintegration.BankIntegrationResult;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.MPAY_CorpoarteService;
import com.progressoft.mpay.entities.MPAY_CustomerMobile;
import com.progressoft.mpay.entities.MPAY_MpClearIntegMsgLog;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Transaction;
import com.progressoft.mpay.entity.MPClearReasons;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.messages.TransactionHelper;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.mpclear.MPClearCommonFields;
import com.progressoft.mpay.mpclearinteg.MPClearHelper;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.TransactionDirection;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.mepspay.NotificationProcessor;
import com.progressoft.mpay.transactions.AccountingHelper;

public class MEPSPayInwardProcessor extends IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MEPSPayInwardProcessor.class);

	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		try {
			preProcessIntegration(context);
			ValidationResult result = MEPSPayIntegrationValidator.validate(context);
			if (result.isValid())
				return accept(context);
			else
				return reject(context, ProcessingStatusCodes.REJECTED, result.getReasonCode(), result.getReasonDescription());
		} catch (Exception e) {
			logger.error("Error in ProcessIntegration", e);
		}
		return null;
	}

	private void preProcessIntegration(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside PreProcessIntegration ...");
		if (context.getMessage() != null)
			context.setTransaction(context.getDataProvider().getTransactionByMessageId(context.getMessage().getId()));
		int registrationIdLength = context.getSystemParameters().getMobileAccountSelectorLength() + context.getSystemParameters().getRoutingCodeLength();
		String mpClearReceiver = MPClearHelper.getEncodedString(context.getMpClearIsoMessage(), 63);
		String senderInfo = MPClearHelper.getEncodedString(context.getMpClearIsoMessage(), 62).substring(registrationIdLength + 1);
		String receiverInfo = MPClearHelper.getEncodedString(context.getMpClearIsoMessage(), 63).substring(registrationIdLength);
		String receiverType = receiverInfo.substring(0, 1);
		receiverInfo = receiverInfo.substring(1);
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		if (context.getTransactionNature() == null)
			return;
		context.setAmount(MPClearHelper.getAmount(context.getMpClearIsoMessage()));
		sender.setInfo(senderInfo);
		receiver.setInfo(receiverInfo);
		if (context.getTransactionNature().equals(TransactionTypeCodes.DIRECT_CREDIT)) {
			MPAY_CustomerMobile receiverMobile = context.getDataProvider().getCustomerMobile(receiverInfo, receiverType);
			if (receiverMobile == null)
				return;
			receiver.setMobile(receiverMobile);
			receiver.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), receiverMobile, mpClearReceiver.substring(0, registrationIdLength)));
			if (receiver.getMobileAccount() == null)
				return;
			receiver.setAccount(receiver.getMobileAccount().getRefAccount());
			receiver.setCustomer(receiverMobile.getRefCustomer());
			receiver.setBank(receiver.getMobileAccount().getBank());
			receiver.setBanked(receiver.getAccount().getIsBanked());
			receiver.setProfile(receiver.getMobileAccount().getRefProfile());
		} else {
			MPAY_CorpoarteService receiverService = context.getDataProvider().getCorporateService(receiverInfo, receiverType);
			if (receiverService == null)
				return;
			receiver.setService(receiverService);
			receiver.setServiceAccount(SystemHelper.getServiceAccount(context.getSystemParameters(), receiverService, mpClearReceiver.substring(0, registrationIdLength)));
			if (receiver.getServiceAccount() == null)
				return;
			receiver.setAccount(receiver.getServiceAccount().getRefAccount());
			receiver.setCorporate(receiverService.getRefCorporate());
			receiver.setBank(receiver.getServiceAccount().getBank());
			receiver.setBanked(receiver.getAccount().getIsBanked());
			receiver.setProfile(receiver.getServiceAccount().getRefProfile());
		}

		context.setDirection(TransactionDirection.INWARD);
		ChargesCalculator.calculateReceiverCharge(context.getTransactionConfig().getMessageType().getCode(), context, receiver.getProfile());
	}

	private IntegrationProcessingResult accept(IntegrationProcessingContext context) throws WorkflowException {
		logger.debug("Inside Accept ...");
		String processingStatus;
		String reason;
		String reasonDescription = null;
		String note = null;
		if (context.getMpClearIsoMessage().getField(124) != null)
			note = context.getMpClearIsoMessage().getField(124).getValue().toString();
		MPAY_Transaction transaction = TransactionHelper.createTransaction(context, TransactionTypeCodes.DIRECT_CREDIT, BigDecimal.ZERO, note);
		context.getDataProvider().persistTransaction(transaction);
		context.setTransaction(transaction);
		AccountingHelper.postAccounts(context.getDataProvider(), context.getLookupsLoader(), transaction);
		BankIntegrationResult integResult = BankIntegrationHandler.handleIntegrations(context);
		if (integResult.getStatus().equals(BankIntegrationStatus.ACCEPTED)) {
			processingStatus = ProcessingStatusCodes.ACCEPTED;
			reason = ReasonCodes.VALID;
		} else if (integResult.getStatus().equals(BankIntegrationStatus.REJECTED)) {
			processingStatus = ProcessingStatusCodes.REJECTED;
			reason = integResult.getReasonCode();
			reasonDescription = integResult.getReasonDescription();
		} else {
			processingStatus = ProcessingStatusCodes.FAILED;
			reason = integResult.getReasonCode();
			reasonDescription = integResult.getReasonDescription();
		}
		MPAY_MpClearIntegMsgLog mpClearResponse = MPClearHelper.createMPClearResponse(context, context.getMpClearIsoMessage().getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(),
				MPClearHelper.getResponseType(context.getMpClearIsoMessage().getType()), reason, reasonDescription, context.getMpClearIsoMessage().getField(MPClearCommonFields.PROCESSING_CODE)
						.getValue().toString());
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, processingStatus, reason, reasonDescription, mpClearResponse, context.isRequest());
		result.setNotifications(NotificationProcessor.createAcceptanceNotificationMessages(context));
		return result;
	}

	private IntegrationProcessingResult reject(IntegrationProcessingContext context, String processingStatus, String reasonCode, String reasonDescription) {
		logger.debug("Inside Reject ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_MpClearIntegMsgLog mpClearResponse = MPClearHelper.createMPClearResponse(context, context.getMpClearIsoMessage().getField(MPClearCommonFields.MESSAGE_ID).getValue().toString(),
				MPClearHelper.getResponseType(context.getMpClearIsoMessage().getType()), MPClearReasons.REJECTED_BY_RECEIVER_PSP, reason.getCode() + " - " + reason.getDescription(), context
						.getMpClearIsoMessage().getField(MPClearCommonFields.PROCESSING_CODE).getValue().toString());
		IntegrationProcessingResult result = IntegrationProcessingResult.create(context, processingStatus, reasonCode, reasonDescription, null, context.isRequest());
		result.setMpClearOutMessage(mpClearResponse);
		context.getDataProvider().mergeIntegrationResult(result);
		return result;
	}
}
