package com.progressoft.mpay.plugins.mepspay;

import java.util.ArrayList;
import java.util.List;

import com.progressoft.mpay.MPayHelper;
import com.progressoft.mpay.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.NotificationChannelsCode;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entity.NotificationShowTypeCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.NotificationType;
import com.progressoft.mpay.messages.TransactionTypeCodes;
import com.progressoft.mpay.notifications.NotificationHelper;
import com.progressoft.mpay.plugins.ProcessingContext;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(NotificationProcessor.class);

	private NotificationProcessor() {

	}

	public static List<MPAY_Notification> createAcceptanceNotificationMessages(ProcessingContext context) {
		logger.debug("Inside CreateAcceptanceNotificationMessages ...");
		try {
			String receiver;
			String sender;
			String reference;
			long senderLanguageId;
			long receiverLanguageId;
			if (context.getMessage() == null)
				reference = context.getTransaction().getReference();
			else
				reference = context.getMessage().getReference();

			MPAY_CustomerMobile senderMobile;
			MPAY_CorpoarteService receiverService;
			MPAY_Account senderAccount;
			MPAY_Account receiverAccount;

			if (context.getTransaction().getRefType().getCode().equals(TransactionTypeCodes.DIRECT_CREDIT)) {
				senderMobile = context.getSender().getMobile();
				senderAccount = context.getSender().getMobileAccount().getRefAccount();
				receiverService = context.getReceiver().getService();
				receiverAccount = context.getReceiver().getServiceAccount().getRefAccount();
			} else {
				senderMobile = context.getReceiver().getMobile();
				senderAccount = context.getReceiver().getMobileAccount().getRefAccount();
				receiverService = context.getSender().getService();
				receiverAccount = context.getSender().getServiceAccount().getRefAccount();
			}
			context.getDataProvider().refreshEntity(senderAccount);
			context.getDataProvider().refreshEntity(receiverAccount);
			MPAY_EndPointOperation operation = context.getTransactionConfig().getRefOperation();

			MEPSPayNotificationContext notificationContext = new MEPSPayNotificationContext();
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), context.getAmount()));
			notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setOperation(operation.getOperation());
			notificationContext.setReference(reference);
			if (senderMobile != null) {
				sender = senderMobile.getMobileNumber();

				notificationContext.setSenderBalance(SystemHelper.formatAmount(context.getSystemParameters(), senderAccount.getBalance()));
				notificationContext.setSenderBanked(senderAccount.getIsBanked());
				notificationContext.setSenderCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getSender().getCharge()));
				senderLanguageId = senderMobile.getRefCustomer().getPrefLang().getId();
				if (senderMobile.getNotificationShowType().equalsIgnoreCase(NotificationShowTypeCodes.ALIAS) && senderMobile.getAlias() != null && !senderMobile.getAlias().isEmpty())
					sender = senderMobile.getAlias();
			} else {
				sender = context.getSender().getInfo();
				senderLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			if (receiverService != null) {
				receiver = receiverService.getName();

				notificationContext.setReceiverBalance(SystemHelper.formatAmount(context.getSystemParameters(), receiverAccount.getBalance()));
				notificationContext.setReceiverBanked(receiverAccount.getIsBanked());
				notificationContext.setReceiverCharges(SystemHelper.formatAmount(context.getSystemParameters(), context.getReceiver().getCharge()));
				receiverLanguageId = receiverService.getRefCorporate().getPrefLang().getId();
			} else {
				receiver = context.getReceiver().getInfo();
				receiverLanguageId = context.getSystemParameters().getSystemLanguage().getId();
			}
			notificationContext.setReceiver(receiver);
			notificationContext.setSender(sender);
			return generateNotifications(context, senderLanguageId, receiverLanguageId, senderMobile, receiverService, operation, notificationContext);

		} catch (Exception e) {
			logger.error("Error when CreateAcceptanceNotificationMessages", e);
			return new ArrayList<>();
		}
	}

	private static List<MPAY_Notification> generateNotifications(ProcessingContext context, long senderLanguageId, long receiverLanguageId, MPAY_CustomerMobile senderMobile, MPAY_CorpoarteService receiverService, MPAY_EndPointOperation operation, MEPSPayNotificationContext notificationContext) {
		List<MPAY_Notification> notifications = new ArrayList<>();
		if (senderMobile != null) {
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_SENDER, operation.getId(), senderLanguageId, senderMobile.getMobileNumber(), NotificationChannelsCode.SMS));
		}
		if (receiverService != null) {
			if (receiverService.getNotificationChannel().getCode().equals(NotificationChannelsCode.PUSH_NOTIFICATION)) {
				handlePushNotifications(notificationContext, context, operation, receiverService, receiverLanguageId, notifications);
			} else {
				notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId, receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
			}
		}
		return notifications;
	}

	@SuppressWarnings("unchecked")
	private static void handlePushNotifications(MEPSPayNotificationContext notificationContext, ProcessingContext context, MPAY_EndPointOperation operation, MPAY_CorpoarteService receiverService, long receiverLanguageId, List<MPAY_Notification> notifications) {
		List<String> tokens = (List<String>) context.getExtraData().get(MEPSPayProcessor.NOTIFICATION_TOKEN);
		if (tokens == null)
			return;
		for (String token : tokens) {
			notificationContext.setExtraData1(token);
			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.ACCEPTED_RECEIVER, operation.getId(), receiverLanguageId, receiverService.getNotificationReceiver(), receiverService.getNotificationChannel().getCode()));
		}
	}

	@SuppressWarnings("unchecked")
	public static List<MPAY_Notification> createRejectionNotificationMessages(ProcessingContext context) {
		try {
			logger.debug("Inside CreateRejectionNotificationMessages ...");
			List<MPAY_Notification> notifications = new ArrayList<>();
			if (context.getMessage() == null)
				return notifications;
			if (!context.getMessage().getReason().getSmsEnabled())
				return notifications;
			String notificationChannel = NotificationChannelsCode.SMS;
			MEPSPayMessage request = MEPSPayMessage.parseMessage(MPayRequest.fromJson(context.getMessage().getRequestContent()));
			String sender = request.getSender();
			MPAY_Language language = context.getSystemParameters().getSystemLanguage();
			if (request.getSenderType().equals(ReceiverInfoType.MOBILE)) {
				MPAY_CustomerMobile mobile = context.getDataProvider().getCustomerMobile(request.getSender());
				if (mobile != null) {
					language = mobile.getRefCustomer().getPrefLang();
				}
			} else {
				notificationChannel = NotificationChannelsCode.EMAIL;
				MPAY_CorpoarteService service = context.getDataProvider().getCorporateService(request.getSender(), request.getSenderType());
				if (service != null) {
					language = service.getRefCorporate().getPrefLang();
				}
			}

			MEPSPayNotificationContext notificationContext = new MEPSPayNotificationContext();
			notificationContext.setReference(context.getMessage().getReference());
			notificationContext.setOperation(context.getMessage().getRefOperation().getOperation());
			if (context.getTransaction() == null)
				notificationContext.setCurrency(context.getSystemParameters().getDefaultCurrency().getStringISOCode());
			else
				notificationContext.setCurrency(context.getTransaction().getCurrency().getStringISOCode());
			notificationContext.setAmount(SystemHelper.formatAmount(context.getSystemParameters(), request.getAmount()));
			notificationContext.setReceiver(request.getReceiverInfo());
			notificationContext.setReasonCode(context.getMessage().getReason().getCode());
			notificationContext.setReasonDescription(MPayHelper.getReasonNLS(context.getMessage().getReason(), language).getDescription());
			List<String> tokens = (List<String>) context.getExtraData().get(MEPSPayProcessor.NOTIFICATION_TOKEN);
			if (tokens != null && !tokens.isEmpty())
				notificationContext.setExtraData1(tokens.get(0));

			notifications.add(NotificationHelper.getInstance().createNotification(context.getMessage(), notificationContext, NotificationType.REJECTED_SENDER, context.getMessage().getRefOperation().getId(), language.getId(), sender, notificationChannel));
			return notifications;
		} catch (Exception e) {
			logger.error("Error when CreateRejectionNotificationMessages", e);
			return new ArrayList<>();
		}
	}
}
