package com.progressoft.mpay.plugins.mepspay.integrations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;

public class MEPSPayReversalAdviseProcessor extends IntegrationProcessor {
	private static final Logger logger = LoggerFactory.getLogger(MEPSPayReversalAdviseProcessor.class);
	@Override
	public IntegrationProcessingResult ProcessIntegration(IntegrationProcessingContext context) {
		logger.debug("Inside ProcessIntegration ...");
		return null;
	}

}
