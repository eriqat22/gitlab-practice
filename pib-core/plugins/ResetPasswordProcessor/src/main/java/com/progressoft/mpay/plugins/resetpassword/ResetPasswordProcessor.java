package com.progressoft.mpay.plugins.resetpassword;

import org.apache.commons.lang.NullArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.MPAY_CorporateDevice;
import com.progressoft.mpay.entities.MPAY_CustomerDevice;
import com.progressoft.mpay.entities.MPAY_ProcessingStatus;
import com.progressoft.mpay.entities.MPAY_Reason;
import com.progressoft.mpay.entities.MPAY_Reasons_NLS;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.IntegrationProcessingContext;
import com.progressoft.mpay.plugins.IntegrationProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessingContext;
import com.progressoft.mpay.plugins.MessageProcessingResult;
import com.progressoft.mpay.plugins.MessageProcessor;
import com.progressoft.mpay.plugins.ProcessingContextSide;
import com.progressoft.mpay.plugins.ValidationResult;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;

public class ResetPasswordProcessor implements MessageProcessor {
	private static final Logger logger = LoggerFactory.getLogger(ResetPasswordProcessor.class);

	@Override
	public MessageProcessingResult processMessage(MessageProcessingContext context) {
		logger.debug("Inside ProcessMessage ...");
		if (context == null)
			throw new NullArgumentException("context");

		try {
			preProcessMessage(context);
			ValidationResult validationResult = ResetPasswordValidator.validate(context);
			if (validationResult.isValid())
				return acceptMessage(context);
			else
				return rejectMessage(context, validationResult.getReasonCode(),
						validationResult.getReasonDescription());
		} catch (MessageParsingException e) {
			logger.error("Error while parsing message", e);
			return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
		} catch (Exception ex) {
			logger.error("Error when ProcessMessage in BalanceInquiryProcessor", ex);
			return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
		}
	}

	private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode,
			String reasonDescription) {
		logger.debug("Inside RejectMessage ...");
		MessageProcessingResult result = createResult(context, reasonCode, reasonDescription,
				ProcessingStatusCodes.REJECTED);
		String response = generateResponse(context);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		return result;
	}

	private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
		logger.debug("Inside AcceptMessage ...");
		resetDevicePassword(context);
		MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
		String response = generateResponse(context);
		result.getMessage().setResponseContent(response);
		result.setResponse(response);
		OTPHanlder.removeOTP(context);
		return result;
	}

	private void resetDevicePassword(MessageProcessingContext context) {
		ResetPasswordMessage message = (ResetPasswordMessage) context.getRequest();
		if (ReceiverInfoType.MOBILE.equals(message.getSenderType()))
			resetCustomerDevicePassword(message, context);
		else if (ReceiverInfoType.CORPORATE.equals(message.getSenderType()))
			resetCorporateDevicePassword(message, context);
		else
			throw new NotSupportedException("senderType: " + message.getSenderType() + " not supported");
	}

	private void resetCustomerDevicePassword(ResetPasswordMessage message, MessageProcessingContext context) {
		MPAY_CustomerDevice device = (MPAY_CustomerDevice) context.getExtraData().get(Constants.DEVICE_KEY);
		device.setPassword(message.getPassword());
		context.getDataProvider().mergeCustomerDevice(device);
	}

	private void resetCorporateDevicePassword(ResetPasswordMessage message, MessageProcessingContext context) {
		MPAY_CorporateDevice device = (MPAY_CorporateDevice) context.getExtraData().get(Constants.DEVICE_KEY);
		device.setPassword(message.getPassword());
		context.getDataProvider().mergeCorporateDevice(device);
	}

	private String generateResponse(MessageProcessingContext context) {
		logger.debug("Inside GenerateResponse ...");
		MPayResponse response = new MPayResponse();
		response.setErrorCd(context.getMessage().getReason().getCode());
		response.setRef(Long.parseLong(context.getMessage().getReference()));
		String description;
		if (context.getLanguage() == null)
			context.setLanguage(context.getSystemParameters().getSystemLanguage());
		MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
				context.getLanguage().getCode());
		if (nls == null)
			description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(),
					context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
		else
			description = nls.getDescription();
		response.setDesc(description);
		response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
		return response.toString();
	}

	public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode,
			String reasonDescription, String processingStatus) {
		logger.debug("Inside CreateResult ...");
		MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
		MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
		context.getMessage().setReason(reason);
		context.getMessage().setReasonDesc(reasonDescription);
		context.getMessage().setProcessingStatus(status);
		MessageProcessingResult result = new MessageProcessingResult();
		result.setMessage(context.getMessage());
		result.setReasonCode(reasonCode);
		result.setStatus(status);
		result.setStatusDescription(reasonDescription);
		return result;
	}

	private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException {
		logger.debug("Inside PreProcessMessage ...");
		ResetPasswordMessage message = ResetPasswordMessage.parseMessage(context.getRequest());
		ProcessingContextSide sender = new ProcessingContextSide();
		ProcessingContextSide receiver = new ProcessingContextSide();
		context.setSender(sender);
		context.setReceiver(receiver);
		context.setRequest(message);
		if (message.getSenderType().equals(ReceiverInfoType.MOBILE)) {
			sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
			if (sender.getMobile() == null)
				return;
			sender.setCustomer(sender.getMobile().getRefCustomer());
			sender.setMobileAccount(
					SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
			if (sender.getMobileAccount() == null)
				return;
			sender.setProfile(sender.getMobileAccount().getRefProfile());

			preapreCustomerDevice(context, message, sender);


		} else {
			sender.setService(
					context.getDataProvider().getCorporateService(message.getSender(), message.getSenderType()));
			if (sender.getService() == null)
				return;
			sender.setCorporate(sender.getService().getRefCorporate());
			sender.setServiceAccount(
					SystemHelper.getServiceAccount(context.getSystemParameters(), sender.getService(), null));
			if (sender.getServiceAccount() == null)
				return;
			sender.setProfile(sender.getServiceAccount().getRefProfile());
			prepareCorporateDevice(context, message, sender);
		}

		context.setResetPassword(true);
		OTPHanlder.loadOTP(context, message.getPin());
	}

	private void prepareCorporateDevice(MessageProcessingContext context, ResetPasswordMessage message,
			ProcessingContextSide sender) {
		MPAY_CorporateDevice corporateDevice = context.getDataProvider().getCorporateDevice(message.getDeviceId());
		if(corporateDevice!=null) {
			if(corporateDevice.getRefCorporateService().getId()==sender.getService().getId())
				context.getExtraData().put(Constants.DEVICE_KEY, corporateDevice);
			else
				context.getExtraData().put(Constants.DEVICE_KEY, null);
		}else
			context.getExtraData().put(Constants.DEVICE_KEY, null);
	}

	private void preapreCustomerDevice(MessageProcessingContext context, ResetPasswordMessage message,
			ProcessingContextSide sender) {
		MPAY_CustomerDevice customerDevice = context.getDataProvider().getCustomerDevice(message.getDeviceId());
		if(customerDevice!=null)
		{
			if (customerDevice.getRefCustomerMobile().getId() == sender.getMobile().getId())
				context.getExtraData().put(Constants.DEVICE_KEY, customerDevice);
			else
				context.getExtraData().put(Constants.DEVICE_KEY, null);
		}
		else
			context.getExtraData().put(Constants.DEVICE_KEY, null);
	}

	@Override
	public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reverse(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult accept(MessageProcessingContext context) {
		return null;
	}

	@Override
	public MessageProcessingResult reject(MessageProcessingContext context) {
		return null;
	}

}
