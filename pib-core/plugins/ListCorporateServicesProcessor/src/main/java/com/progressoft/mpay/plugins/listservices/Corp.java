package com.progressoft.mpay.plugins.listservices;

import java.util.List;

public class Corp {
	private long id;
	private String name;
	private String clientRef;
	private List<SrvsObject> srvs;

	long getId() {
		return id;
	}

	void setId(long id) {
		this.id = id;
	}

	String getName() {
		return name;
	}

	void setName(String name) {
		this.name = name;
	}

	public List<SrvsObject> getSrvs() {
		return srvs;
	}

	public void setSrvs(List<SrvsObject> srvs) {
		this.srvs = srvs;
	}

	public String getClientRef() {
		return clientRef;
	}

	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}
}
