package com.progressoft.mpay.plugins.listservices;

import java.util.List;

import com.progressoft.mpay.messages.MPayResponse;

public class ListServicesResponse extends MPayResponse {
	List<Corp> corps;

	public List<Corp> getCorps() {
		return corps;
	}

	public void setCorps(List<Corp> corps) {
		this.corps = corps;
	}
}
