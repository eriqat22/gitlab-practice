package com.progressoft.mpay.plugins.listservices;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class ListServicesMessage extends MPayRequest {
	public ListServicesMessage() {
		//
	}

	public ListServicesMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public static ListServicesMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		ListServicesMessage message = new ListServicesMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !message.getSenderType().trim().equals(ReceiverInfoType.MOBILE))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		return message;
	}
}
