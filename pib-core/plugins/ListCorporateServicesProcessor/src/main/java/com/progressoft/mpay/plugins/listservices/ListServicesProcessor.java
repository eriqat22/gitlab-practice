package com.progressoft.mpay.plugins.listservices;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opensymphony.workflow.WorkflowException;
import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.mpay.SysConfigKeys;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.ReasonCodes;
import com.progressoft.mpay.messages.ExtraData;
import com.progressoft.mpay.messages.MPayResponse;
import com.progressoft.mpay.messages.MessageParsingException;
import com.progressoft.mpay.messages.ProcessingStatusCodes;
import com.progressoft.mpay.plugins.*;
import com.progressoft.mpay.plugins.helpers.OTPHanlder;
import com.progressoft.mpay.registration.corporates.CorporateServiceWorkflowStatuses;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListServicesProcessor implements MessageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(ListServicesProcessor.class);

    @Override
    public IntegrationProcessingResult processIntegration(IntegrationProcessingContext context) {
        logger.debug("Inside ProcessIntegration ...");
        return null;
    }

    @Override
    public MessageProcessingResult processMessage(MessageProcessingContext context) {
        logger.debug("Inside ProcessMessage ...");
        if (context == null)
            throw new NullArgumentException("context");

        try {
            preProcessMessage(context);
            ValidationResult validationResult = ListServicesValidator.validate(context);
            if (validationResult.isValid())
                return acceptMessage(context);
            else
                return rejectMessage(context, validationResult.getReasonCode(), validationResult.getReasonDescription());
        } catch (MessageParsingException e) {
            logger.debug("Failed to parse message", e);
            return rejectMessage(context, ReasonCodes.FAILED_TO_PARSE_MESSAGE, e.getField() + " Not Provided");
        } catch (Exception ex) {
            logger.error("Error when ProcessMessage in ListServicesProcessor", ex);
            return rejectMessage(context, ReasonCodes.INTERNAL_SYSTEM_ERROR, ex.getMessage());
        }
    }

    private MessageProcessingResult rejectMessage(MessageProcessingContext context, String reasonCode, String reasonDescription) {
        logger.debug("Inside RejectMessage ...");
        MessageProcessingResult result = createResult(context, reasonCode, reasonDescription, ProcessingStatusCodes.REJECTED);
        String response = generateResponse(context, false);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        return result;
    }

    private MessageProcessingResult acceptMessage(MessageProcessingContext context) {
        logger.debug("Inside AcceptMessage ...");
        MessageProcessingResult result = createResult(context, ReasonCodes.VALID, null, ProcessingStatusCodes.ACCEPTED);
        String response = generateResponse(context, true);
        result.getMessage().setResponseContent(response);
        result.setResponse(response);
        OTPHanlder.removeOTP(context);
        return result;
    }

    private String generateResponse(MessageProcessingContext context, boolean isAccepted) {
        logger.debug("Inside AcceptMessage ...");
        logger.debug("isAccepted: " + isAccepted);
        MPayResponse response = new MPayResponse();
        response.setErrorCd(context.getMessage().getReason().getCode());
        response.setRef(Long.parseLong(context.getMessage().getReference()));
        String description;
        if (context.getLanguage() == null)
            context.setLanguage(context.getSystemParameters().getSystemLanguage());
        MPAY_Reasons_NLS nls = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getLanguage().getCode());
        if (nls == null)
            description = context.getLookupsLoader().getReasonNLS(context.getMessage().getReason().getId(), context.getSystemParameters().getSystemLanguage().getCode()).getDescription();
        else
            description = nls.getDescription();
        response.setDesc(description);
        response.setStatusCode(context.getMessage().getProcessingStatus().getCode());
        response.getExtraData().add(new ExtraData(Constants.ServicesResponseKey, prepareCorpListToMob(context, isAccepted)));
        return response.toString();
    }

    private void preProcessMessage(MessageProcessingContext context) throws MessageParsingException, WorkflowException {
        logger.debug("Inside PreProcessMessage ...");
        ListServicesMessage message = ListServicesMessage.parseMessage(context.getRequest());
        ProcessingContextSide sender = new ProcessingContextSide();
        ProcessingContextSide receiver = new ProcessingContextSide();
        context.setSender(sender);
        context.setReceiver(receiver);
        context.setRequest(message);
        sender.setMobile(context.getDataProvider().getCustomerMobile(context.getRequest().getSender()));
        if (sender.getMobile() == null)
            return;
        sender.setCustomer(sender.getMobile().getRefCustomer());
        sender.setMobileAccount(SystemHelper.getMobileAccount(context.getSystemParameters(), sender.getMobile(), null));
        if (sender.getMobileAccount() == null)
            return;
        sender.setProfile(sender.getMobileAccount().getRefProfile());
        OTPHanlder.loadOTP(context, message.getPin());
    }

    public MessageProcessingResult createResult(MessageProcessingContext context, String reasonCode, String reasonDescription, String processingStatus) {
        logger.debug("Inside CreateResult ...");
        MPAY_Reason reason = context.getLookupsLoader().getReason(reasonCode);
        MPAY_ProcessingStatus status = context.getLookupsLoader().getProcessingStatus(processingStatus);
        context.getMessage().setReason(reason);
        context.getMessage().setReasonDesc(reasonDescription);
        context.getMessage().setProcessingStatus(status);
        MessageProcessingResult result = new MessageProcessingResult();
        result.setMessage(context.getMessage());
        result.setReasonCode(reasonCode);
        result.setStatus(status);
        result.setStatusDescription(reasonDescription);
        return result;
    }

    private String prepareCorpListToMob(ProcessingContext context, Boolean isAccepted) {
        logger.debug("Inside PrepareCorpListToMob ...");
        logger.debug("isAccepted: " + isAccepted);
        Gson gson = new GsonBuilder().create();
        if (!isAccepted) {
            return gson.toJson(new ArrayList<Corp>());
        }
        List<MPAY_Corporate> mpayCorps = context.getDataProvider().listActiveCorporates();
        String[] exposedCorpCd = (context.getLookupsLoader().getSystemConfigurations(SysConfigKeys.EXPOSED_CORPORATES_CODES).getConfigValue()).split(";");
        List<Corp> corpsToMob = new ArrayList<>();

        for (MPAY_Corporate mpayCorp : mpayCorps) {
            MPAY_ClientType clientType = mpayCorp.getClientType();
            if (clientType != null) {
                if (exposedCorpCd != null && !(Arrays.asList(exposedCorpCd).contains(clientType.getCode())))
                    continue;

                Corp corpToMob = prepareCorpToMob(mpayCorp);
                corpsToMob.add(corpToMob);
            }
        }

        return gson.toJson(corpsToMob);
    }

    private Corp prepareCorpToMob(MPAY_Corporate mpayCorp) {
        logger.debug("Inside PrepareCorpToMob ...");
        Corp corp = new Corp();

        corp.setId(mpayCorp.getId());
        corp.setName(mpayCorp.getName());
        corp.setClientRef(mpayCorp.getClientRef());

        for (MPAY_CorpoarteService service : mpayCorp.getRefCorporateCorpoarteServices()) {

            MPAY_MessageType paymentType = service.getPaymentType();
            WorkflowStatus statusId = service.getStatusId();
            if (paymentType == null || statusId == null)
                return corp;
            if (StringUtils.equals(Constants.PaymentType, paymentType.getCode()) && service.getIsActive() && service.getIsRegistered() && StringUtils.equals(CorporateServiceWorkflowStatuses.APPROVED.toString(), statusId.getCode())) {
                if (corp.getSrvs() == null) {
                    corp.setSrvs(new ArrayList<SrvsObject>());
                    corp.getSrvs().add(prepareCorporateService(service));
                } else {
                    corp.getSrvs().add(prepareCorporateService(service));
                }
            }
        }
        return corp;
    }

    private SrvsObject prepareCorporateService(MPAY_CorpoarteService service) {
        logger.debug("Inside PrepareCorporateService ...");
        SrvsObject corpSrv = new SrvsObject();

        corpSrv.setId(service.getId());
        corpSrv.setName(service.getName());
        corpSrv.setAlias(service.getAlias());
        corpSrv.setDescription(service.getDescription());
        corpSrv.setServiceCategoryCode(service.getServiceCategory() != null ? service.getServiceCategory().getCode() : null);
        corpSrv.setServiceTypeCode(service.getServiceType() != null ? service.getServiceType().getCode() : null);
        return corpSrv;
    }

    @Override
    public MessageProcessingResult accept(MessageProcessingContext arg0) {
        logger.debug("Inside Accept ...");
        return null;
    }

    @Override
    public MessageProcessingResult reject(MessageProcessingContext arg0) {
        logger.debug("Inside Reject ...");
        return null;
    }

    @Override
    public MessageProcessingResult reverse(MessageProcessingContext arg0) {
        logger.debug("Inside Reverse ...");
        return null;
    }
}
