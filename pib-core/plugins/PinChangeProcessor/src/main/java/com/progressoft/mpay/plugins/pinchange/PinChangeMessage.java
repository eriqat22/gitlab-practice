package com.progressoft.mpay.plugins.pinchange;

import org.apache.commons.lang.NullArgumentException;

import com.progressoft.mpay.entity.ReceiverInfoType;
import com.progressoft.mpay.messages.MPayRequest;
import com.progressoft.mpay.messages.MessageParsingException;

public class PinChangeMessage extends MPayRequest {
	private static final String NEW_PIN_KEY = "nPin";

	private String newPin;

	public PinChangeMessage() {
		//
	}

	public PinChangeMessage(MPayRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		setOperation(request.getOperation());
		setSender(request.getSender());
		setSenderType(request.getSenderType());
		setDeviceId(request.getDeviceId());
		setLang(request.getLang());
		setMsgId(request.getMsgId());
		setPin(request.getPin());
		setExtraData(request.getExtraData());
	}

	public String getNewPin() {
		return newPin;
	}

	public void setNewPin(String nPin) {
		this.newPin = nPin;
	}

	public static PinChangeMessage parseMessage(MPayRequest request) throws MessageParsingException {
		if (request == null)
			return null;
		PinChangeMessage message = new PinChangeMessage(request);
		if (message.getSender() == null || message.getSender().trim().length() == 0)
			throw new MessageParsingException(MPayRequest.SENDER_KEY);
		if (message.getSenderType() == null || !(message.getSenderType().trim().equals(ReceiverInfoType.MOBILE) || message.getSenderType().trim().equals(ReceiverInfoType.CORPORATE)))
			throw new MessageParsingException(MPayRequest.SENDER_TYPE_KEY);
		message.setNewPin(message.getValue(NEW_PIN_KEY));
		if (message.getNewPin() == null || message.getNewPin().trim().length() == 0)
			throw new MessageParsingException(NEW_PIN_KEY);
		return message;
	}
}
