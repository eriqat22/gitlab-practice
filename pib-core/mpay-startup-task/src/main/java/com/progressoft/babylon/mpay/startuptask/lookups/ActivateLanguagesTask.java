package com.progressoft.babylon.mpay.startuptask.lookups;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Language;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

import static com.progressoft.mpay.Constants.GENERIC_ACTIVE;

@Component
public class ActivateLanguagesTask extends StartupTask {
    private static final Logger LOGGER = Logger.getLogger(ActivateLanguagesTask.class.getName());

    @Override
    public void execute(OptionsProvider optionsProvider){
        LOGGER.info("inside execute in ActivateLanguagesTask");
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();
        WorkflowStatus workflowStatus = dataProvider.getWorkflowStatus(GENERIC_ACTIVE);

        for (MPAY_Language language : this.itemDao.getItems(MPAY_Language.class)) {
            if (isAlreadyActive(language))
                LOGGER.info("Language is already active :" + language.getName());
            else {
                language.setStatusId(workflowStatus);
                dataProvider.updateLanguageStatus(language);
            }
        }
        LOGGER.info("Languages has been activated successfully");
    }

    private boolean isAlreadyActive(MPAY_Language language) {
        WorkflowStatus statusId = language.getStatusId();
        return statusId != null && statusId.getCode().equalsIgnoreCase(GENERIC_ACTIVE);
    }
}

