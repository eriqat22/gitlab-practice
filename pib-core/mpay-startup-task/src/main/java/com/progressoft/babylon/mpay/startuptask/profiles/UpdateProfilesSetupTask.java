package com.progressoft.babylon.mpay.startuptask.profiles;


import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.MPayContext;
import org.springframework.stereotype.Component;

@Component
public class UpdateProfilesSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {

        LOG.info("UpdateProfilesSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();
        executeQuery("UPDATE MPAY_PROFILES SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("101705").getId() + "'");
        LOG.info("UpdateProfilesSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
    }

    private void executeQuery(String query) {
        itemDao.getEntityManager().createNativeQuery(query).executeUpdate();
        itemDao.getEntityManager().createNativeQuery("COMMIT").executeUpdate();
    }
}
