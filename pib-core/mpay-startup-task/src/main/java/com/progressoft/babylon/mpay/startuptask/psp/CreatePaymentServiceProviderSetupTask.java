package com.progressoft.babylon.mpay.startuptask.psp;

import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Corporate;
import com.progressoft.mpay.entity.MPAYView;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class CreatePaymentServiceProviderSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        LOG.info("CreatePaymentServiceProviderSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");
        ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();
        try (ServiceUserEnabled ignored = jfw.enableServiceUser("SYSTEM")) {
            MPAY_Corporate psp = new MPAY_Corporate();
            psp.setRefCountry(lookupsLoader.getCountry("PSE"));
            psp.setClientType(lookupsLoader.getClientType("100"));
            psp.setIsActive(true);
            psp.setRegistrationDate(new Date());
            psp.setIsRegistered(true);
            psp.setCity(lookupsLoader.getCity("01"));
            psp.setDescription("PIB");
            psp.setName("PIB");
            psp.setEmail("pibadmin@pib.com");
            psp.setPrefLang(lookupsLoader.getLanguageByCode("en"));
            psp.setCategory(lookupsLoader.getAccountCategory(5));
            psp.setIdType(lookupsLoader.getIDType("999"));
            psp.setNationalID("999");
            psp.setPspNationalID("999");
            psp.setRegistrationId("999");
            jfw.createEntity(MPAYView.PSP.viewName,psp);

        }
        LOG.info("CreatePaymentServiceProviderSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
    }
}
