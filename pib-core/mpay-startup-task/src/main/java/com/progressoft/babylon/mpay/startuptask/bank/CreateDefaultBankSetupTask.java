package com.progressoft.babylon.mpay.startuptask.bank;

import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.WF_Banks;
import org.springframework.stereotype.Component;

@Component
public class CreateDefaultBankSetupTask extends StartupTask {


    private static final String DEFAULT_VALUES = "Default";
    private static final long DEFAULT_ID_VALUE = 1l;


    @Override
    public void execute(OptionsProvider optionsProvider) {
        LOG.info("CreateDefaultBankSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");

        try (ServiceUserEnabled ignored = jfw.enableServiceUser(optionsProvider.get("tenants.names"))) {
            ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();

            if (lookupsLoader.getBankByCode("105") != null) {
                LOG.info("all initial data is already created");
                return;
            }

            MPAY_Bank defaultBank = createDefaultBank();
            jfw.createEntity(defaultBank);

        }

        LOG.info("CreateDefaultBankSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
    }

    private MPAY_Bank createDefaultBank() {
        MPAY_Bank mpayBank = new MPAY_Bank();
        mpayBank.setName(DEFAULT_VALUES);
        mpayBank.setDescription(DEFAULT_VALUES);
        mpayBank.setIsActive(true);
        mpayBank.setCode("105");
        mpayBank.setSettlementParticipant("1");
        mpayBank.setTellerCode("Teller105");
        return mpayBank;
    }


}

