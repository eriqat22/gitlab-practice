package com.progressoft.babylon.mpay.startuptask.lookups;


import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_SysConfig;
import org.springframework.stereotype.Component;

import static java.lang.Boolean.*;
import static java.lang.String.valueOf;

@Component
public class AddSystemConfigurationsSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        Long defaultOrgId;
        String tenant = optionsProvider.get("tenants.names");

        try (ServiceUserEnabled ignored = jfw.enableServiceUser(tenant)) {
            MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "name", "Default");
            defaultOrgId = defaultBank.getOrgId();
        }

        try (ServiceUserEnabled ignored = jfw.enableServiceUser("SYSTEM")) {
            itemDao.persist(createNewSysConfig("orgId", valueOf(defaultOrgId), TRUE));
            itemDao.persist(createNewSysConfig("Jawwal Service Name", "JAWWAL", TRUE));
            itemDao.persist(createNewSysConfig("Roots Service Name", "ROOTS", TRUE));
        }

    }

    private MPAY_SysConfig createNewSysConfig(String configKey, String configValue,Boolean isDownloadable) {
        MPAY_SysConfig orgId = new MPAY_SysConfig();
        orgId.setConfigKey(configKey);
        orgId.setConfigValue(configValue);
        orgId.setSection("ALL");
        orgId.setIsDownloadable(isDownloadable);
        return orgId;
    }
}
