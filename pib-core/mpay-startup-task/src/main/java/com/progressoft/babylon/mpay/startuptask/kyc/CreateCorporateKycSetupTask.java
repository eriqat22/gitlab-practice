package com.progressoft.babylon.mpay.startuptask.kyc;


import com.progressoft.babylon.mpay.startuptask.lookups.SetupTasksConstants;
import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.entities.MPAY_CorporateKyc;
import com.progressoft.mpay.entities.MPAY_Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CreateCorporateKycSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        LOG.info("CreateCorporateKycSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");
        String tenant = optionsProvider.get("tenants.names");
        try (ServiceUserEnabled ignored = jfw.enableServiceUser(tenant)) {
            List<MPAY_Profile> allProfiles = itemDao.getItems(MPAY_Profile.class);
            MPAY_CorporateKyc fullKyc = new MPAY_CorporateKyc();
            fullKyc.setCode("FullKeyc");
            fullKyc.setName("FullKeyc");
            fullKyc.setProfiles(allProfiles.stream().filter(profile -> profile.getName().equals(SetupTasksConstants.DEFAULT_VALUES)).collect(Collectors.toList()));

            fullKyc.setExpectedActivitiesVisible(Boolean.FALSE);
            fullKyc.setClientRefVisible(Boolean.FALSE);
            fullKyc.setRegistrationAuthorityVisible(Boolean.FALSE);
            fullKyc.setRegistrationLocationVisible(Boolean.FALSE);
            fullKyc.setChamberIdVisible(Boolean.FALSE);
            fullKyc.setLegalEntityVisible(Boolean.FALSE);
            fullKyc.setOtherLegalEntityVisible(Boolean.FALSE);
            fullKyc.setPhoneOneVisible(Boolean.FALSE);
            fullKyc.setPhoneTwoVisible(Boolean.FALSE);
            fullKyc.setPoboxVisible(Boolean.FALSE);
            fullKyc.setZipCodeVisible(Boolean.FALSE);
            fullKyc.setBuildingNumVisible(Boolean.FALSE);
            fullKyc.setStreetNameVisible(Boolean.FALSE);
            fullKyc.setLocationVisible(Boolean.FALSE);
            fullKyc.setExpectedActivitiesEnabled(Boolean.FALSE);
            fullKyc.setClientRefEnabled(Boolean.FALSE);
            fullKyc.setRegistrationAuthorityEnabled(Boolean.FALSE);
            fullKyc.setRegistrationLocationEnabled(Boolean.FALSE);
            fullKyc.setChamberIdEnabled(Boolean.FALSE);
            fullKyc.setLegalEntityEnabled(Boolean.FALSE);
            fullKyc.setOtherLegalEntityEnabled(Boolean.FALSE);
            fullKyc.setPhoneOneEnabled(Boolean.FALSE);
            fullKyc.setPhoneTwoEnabled(Boolean.FALSE);
            fullKyc.setPoboxEnabled(Boolean.FALSE);
            fullKyc.setZipCodeEnabled(Boolean.FALSE);
            fullKyc.setBuildingNumEnabled(Boolean.FALSE);
            fullKyc.setStreetNameEnabled(Boolean.FALSE);
            fullKyc.setLocationEnabled(Boolean.FALSE);
            fullKyc.setExpectedActivitiesReq(Boolean.FALSE);
            fullKyc.setClientRefReq(Boolean.FALSE);
            fullKyc.setRegistrationAuthorityReq(Boolean.FALSE);
            fullKyc.setRegistrationLocationReq(Boolean.FALSE);
            fullKyc.setChamberIdReq(Boolean.FALSE);
            fullKyc.setLegalEntityReq(Boolean.FALSE);
            fullKyc.setOtherLegalEntityReq(Boolean.FALSE);
            fullKyc.setPhoneOneReq(Boolean.FALSE);
            fullKyc.setPhoneTwoReq(Boolean.FALSE);
            fullKyc.setPoboxReq(Boolean.FALSE);
            fullKyc.setZipCodeReq(Boolean.FALSE);
            fullKyc.setBuildingNumReq(Boolean.FALSE);
            fullKyc.setStreetNameReq(Boolean.FALSE);
            fullKyc.setLocationReq(Boolean.FALSE);
            jfw.createEntity(fullKyc);
        }
        LOG.info("CreateCorporateKycSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
    }
}
