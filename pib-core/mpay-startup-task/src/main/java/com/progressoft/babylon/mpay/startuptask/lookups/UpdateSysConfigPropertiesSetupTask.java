package com.progressoft.babylon.mpay.startuptask.lookups;


import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.entities.MPAY_Bank;
import com.progressoft.mpay.entities.MPAY_CustomerKyc;
import com.progressoft.mpay.entities.MPAY_Profile;
import org.springframework.stereotype.Component;

@Component
public class UpdateSysConfigPropertiesSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        LOG.info("UpdateSysConfigPropertiesSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");

        String tenant = optionsProvider.get("tenants.names");
        try (ServiceUserEnabled ignored = jfw.enableServiceUser(tenant)) {

            MPAY_Profile lightWalletProfile = itemDao.getItem(MPAY_Profile.class, "name", "LightWallet");
            MPAY_Profile autoRegistrationProfile = itemDao.getItem(MPAY_Profile.class, "name", "AutoReg");
            MPAY_Bank defaultBank = itemDao.getItem(MPAY_Bank.class, "name", "Default");
            MPAY_CustomerKyc lightKyc = itemDao.getItem(MPAY_CustomerKyc.class, "name", "LightKyc");
            itemDao.getEntityManager().createNativeQuery("UPDATE MPAY_SYSCONFIGS SET CONFIGVALUE='" + lightWalletProfile.getId() + "' WHERE CONFIGKEY='Light Wallet Profile Id'").executeUpdate();
            itemDao.getEntityManager().createNativeQuery("UPDATE MPAY_SYSCONFIGS SET CONFIGVALUE='" + defaultBank.getId() + "' WHERE CONFIGKEY='Default Bank ID'").executeUpdate();
            itemDao.getEntityManager().createNativeQuery("UPDATE MPAY_SYSCONFIGS SET CONFIGVALUE='" + defaultBank.getCode() + "' WHERE CONFIGKEY='Default Bank Code'").executeUpdate();
            itemDao.getEntityManager().createNativeQuery("UPDATE MPAY_SYSCONFIGS SET CONFIGVALUE='" + lightKyc.getId() + "' WHERE CONFIGKEY='Light Wallet KYC Template Id'").executeUpdate();
            itemDao.getEntityManager().createNativeQuery("UPDATE MPAY_SYSCONFIGS SET CONFIGVALUE='com.progressoft.psmpay.notifications.NotificationSenderUseCase' WHERE CONFIGKEY='Notifications Sender'").executeUpdate();

            itemDao.getEntityManager().createNativeQuery("COMMIT").executeUpdate();
        }

        System.out.println("|--------------------------------------------------------------------------------------------------------------|");
        System.out.println("|                                                                                                              |");
        System.out.println("|                                                                                                              |");
        System.out.println("|                                                                                                              |");
        System.out.println("|                                                                                                              |");
        System.out.println("|                                   APP SETUP TASKS DONE SUCCESSFULLY                                          |");
        System.out.println("|                                                                                                              |");
        System.out.println("|                                                                                                              |");
        System.out.println("|                                                                                                              |");
        System.out.println("|                                                                                                              |");
        System.out.println("|--------------------------------------------------------------------------------------------------------------|");
    }


}
