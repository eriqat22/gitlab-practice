package com.progressoft.babylon.mpay.startuptask.psp;

import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.MPayContext;
import org.springframework.stereotype.Component;


@Component
public class UpdatePaymentServiceProviderSetupTask extends StartupTask {

    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        LOG.info("UpdatePaymentServiceProviderSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");
        String tenant = optionsProvider.get("tenants.names");
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();
        executeQuery("UPDATE MPAY_CORPORATES SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("101505").getId() + "'");
        executeQuery("UPDATE MPAY_NetworkManagements SET z_org_id=(select SUPERORGID from JFW_TENANT where name='"+ tenant +"'), z_tenant_id='"+tenant+"'");
        LOG.info("UpdatePaymentServiceProviderSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");
    }

    private void executeQuery(String query) {
        itemDao.getEntityManager().createNativeQuery(query).executeUpdate();
        itemDao.getEntityManager().createNativeQuery("COMMIT").executeUpdate();
    }
}
