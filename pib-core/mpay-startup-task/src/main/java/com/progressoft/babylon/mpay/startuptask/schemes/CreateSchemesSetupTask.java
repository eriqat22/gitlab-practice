package com.progressoft.babylon.mpay.startuptask.schemes;

import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static com.progressoft.babylon.mpay.startuptask.lookups.SetupTasksConstants.DEFAULT_CURRENCY_CODE;
import static org.springframework.beans.factory.xml.BeanDefinitionParserDelegate.DEFAULT_VALUE;

@Component
public class CreateSchemesSetupTask extends StartupTask {


    private static final String COMMISSIONS_PROCESSOR = "com.progressoft.psmpay.commissions.CommissionProcessorImpl";



    @Override
    public void execute(OptionsProvider optionsProvider) throws InvalidActionException {
        LOG.info("CreateSchemesSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");

        ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();

        if (dataProvider.getTaxSchemeByCode(DEFAULT_VALUE) != null) {
            LOG.info("all CreateSchemesSetupTask is already created");
            return;
        }

        MPAY_LimitsScheme limitsScheme = new MPAY_LimitsScheme();
        limitsScheme.setIsActive(true);
        limitsScheme.setCode(DEFAULT_VALUE);
        limitsScheme.setName(DEFAULT_VALUE);
        limitsScheme.setPinlessAmount(new BigDecimal(0));
        limitsScheme.setWalletCap(new BigDecimal(0));
        limitsScheme.setCurrency(lookupsLoader.getCurrency(DEFAULT_CURRENCY_CODE));
        limitsScheme.setDescription(DEFAULT_VALUE);
        limitsScheme.setStatusId(dataProvider.getWorkflowStatus("102105"));


        MPAY_ChargesScheme chargesScheme = new MPAY_ChargesScheme();
        chargesScheme.setCode(DEFAULT_VALUE);
        chargesScheme.setName(DEFAULT_VALUE);
        chargesScheme.setDescription(DEFAULT_VALUE);
        chargesScheme.setIsActive(true);
        chargesScheme.setCurrency(lookupsLoader.getCurrency(DEFAULT_CURRENCY_CODE));
        chargesScheme.setStatusId(dataProvider.getWorkflowStatus("101805"));

        MPAY_RequestTypesScheme requestTypesScheme = new MPAY_RequestTypesScheme();
        requestTypesScheme.setCode(DEFAULT_VALUE);
        requestTypesScheme.setName(DEFAULT_VALUE);
        requestTypesScheme.setDescription(DEFAULT_VALUE);
        requestTypesScheme.setStatusId(dataProvider.getWorkflowStatus("904605"));


        MPAY_CommissionScheme commissionScheme = new MPAY_CommissionScheme();
        commissionScheme.setCode(DEFAULT_VALUE);
        commissionScheme.setName(DEFAULT_VALUE);
        commissionScheme.setProcessor(COMMISSIONS_PROCESSOR);
        commissionScheme.setIsActive(true);
        commissionScheme.setDescription(DEFAULT_VALUE);
        commissionScheme.setStatusId(dataProvider.getWorkflowStatus("211805"));

        MPAY_TaxScheme taxScheme = new MPAY_TaxScheme();
        taxScheme.setName(DEFAULT_VALUE);
        taxScheme.setCode(DEFAULT_VALUE);
        taxScheme.setIsActive(true);
        taxScheme.setCurrency(lookupsLoader.getCurrency(DEFAULT_CURRENCY_CODE));
        taxScheme.setDescription(DEFAULT_VALUE);
        taxScheme.setStatusId(dataProvider.getWorkflowStatus("311805"));
        taxScheme.setIsInludedInsideFees(true);

        try (ServiceUserEnabled ignored = jfw.enableServiceUser(optionsProvider.get("tenants.names"))) {
            JfwHelper.createEntity(MPAYView.LIMITSSCHEME, limitsScheme);
            JfwHelper.createEntity(MPAYView.CHARGESCHEME, chargesScheme);
            JfwHelper.createEntity(MPAYView.REQUESTTYPESCHEME, requestTypesScheme);
            JfwHelper.createEntity(MPAYView.COMMISIONSCHEME, commissionScheme);
            JfwHelper.createEntity(MPAYView.TAXSCHEME, taxScheme);


        }
        LOG.info("CreateSchemesSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");



    }




}

