package com.progressoft.babylon.mpay.startuptask.lookups;

import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import org.springframework.stereotype.Component;


@Component
public class UpdateEndpointsOperationsSetupTask extends StartupTask {

    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        String tenant = optionsProvider.get("tenants.names");
        try (ServiceUserEnabled ignored = jfw.enableServiceUser(tenant)) {
            executeQuery("update mpay_endpointoperations set processor='com.progressoft.mars.psmpay.backend.corporate.paymentsgateway.token.RequestOnlineTokenUseCaseFactory' where operation='onlineToken'");
            executeQuery("update mpay_endpointoperations set processor='com.progressoft.mars.psmpay.backend.corporate.paymentsgateway.otp.OnlineSendOTPUseCaseFactory' where operation='onlineOtp'");
            executeQuery("update mpay_endpointoperations set processor='com.progressoft.mars.psmpay.backend.corporate.paymentsgateway.verifyOtp.OnlineVerifyOtpUseCaseFactory' where operation='onlineVerifyOtp'");
            executeQuery("update mpay_endpointoperations set processor='com.progressoft.mars.psmpay.backend.corporate.paymentsgateway.payment.OnlinePaymentUseCaseFactory' where operation='onlinePayment'");
        }

    }

    private void executeQuery(String query) {
        itemDao.getEntityManager().createNativeQuery(query).executeUpdate();
        itemDao.getEntityManager().createNativeQuery("COMMIT").executeUpdate();
    }
}
