package com.progressoft.babylon.mpay.startuptask.kyc;

import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.MPayContext;
import org.springframework.stereotype.Component;

@Component
public class UpdateCorporateKycSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {

        LOG.info("UpdateCorporateKycSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();
        executeQuery("UPDATE mpay_corporatekyc SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("104605").getId() + "'");
        LOG.info("UpdateCorporateKycSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
    }


    private void executeQuery(String query) {
        itemDao.getEntityManager().createNativeQuery(query).executeUpdate();
        itemDao.getEntityManager().createNativeQuery("COMMIT").executeUpdate();
    }
}
