package com.progressoft.babylon.mpay.startuptask.kyc;

import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.entities.MPAY_CustomerKyc;
import com.progressoft.mpay.entities.MPAY_Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.progressoft.babylon.mpay.startuptask.lookups.SetupTasksConstants.*;


@Component
public class CreateCustomerKycSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {

        LOG.info("CreateCustomerKycSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");

        String tenant = optionsProvider.get("tenants.names");
        try (ServiceUserEnabled ignored = jfw.enableServiceUser(tenant)) {
            List<MPAY_Profile> allProfiles = itemDao.getItems(MPAY_Profile.class);


            jfw.createEntity(createCustomerKyc("1", "FullKyc",
                    allProfiles.stream().filter(profile -> profile.getName().equals(DEFAULT_VALUES)).collect(Collectors.toList())));

            jfw.createEntity(createCustomerKyc("2", "LightKyc",
                    allProfiles.stream().filter(profile -> profile.getName().equals(LIGHT_WALLET_VALUE)
                            || profile.getName().equals(AUTO_REG_PROFILE_VALUE))
                            .collect(Collectors.toList())));
        }

        LOG.info("CreateCustomerKycSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
    }

    private MPAY_CustomerKyc createCustomerKyc(String kycCode, String kycName, List<MPAY_Profile> kycProfiles) {
        MPAY_CustomerKyc lightKyc = new MPAY_CustomerKyc();
        lightKyc.setCode(kycCode);
        lightKyc.setName(kycName);
        lightKyc.setProfiles(kycProfiles);
        lightKyc.setEnglishFullNameVisible(Boolean.FALSE);
        lightKyc.setArabicFullNameVisible(Boolean.FALSE);
        lightKyc.setIdentificationReferenceVisible(Boolean.FALSE);
        lightKyc.setIdentificationCardVisible(Boolean.FALSE);
        lightKyc.setIdCardIssuanceDateVisible(Boolean.FALSE);
        lightKyc.setPassportIdVisible(Boolean.FALSE);
        lightKyc.setPassportIssuanceDateVisible(Boolean.FALSE);
        lightKyc.setPhoneOneVisible(Boolean.FALSE);
        lightKyc.setPhoneTwoVisible(Boolean.FALSE);
        lightKyc.setAreaVisible(Boolean.FALSE);
        lightKyc.setPoboxVisible(Boolean.FALSE);
        lightKyc.setZipCodeVisible(Boolean.FALSE);
        lightKyc.setBuildingNumVisible(Boolean.FALSE);
        lightKyc.setStreetNameVisible(Boolean.FALSE);
        lightKyc.setAddressVisible(Boolean.FALSE);
        lightKyc.setNoteVisible(Boolean.FALSE);
        lightKyc.setOccupationVisible(Boolean.FALSE);
        lightKyc.setIdExpiryDateVisible(Boolean.FALSE);
        lightKyc.setEnglishFullNameEnabled(Boolean.FALSE);
        lightKyc.setArabicFullNameEnabled(Boolean.FALSE);
        lightKyc.setIdentificationReferenceEnabled(Boolean.FALSE);
        lightKyc.setIdentificationCardEnabled(Boolean.FALSE);
        lightKyc.setIdCardIssuanceDateEnabled(Boolean.FALSE);
        lightKyc.setPassportIdEnabled(Boolean.FALSE);
        lightKyc.setPassportIssuanceDateEnabled(Boolean.FALSE);
        lightKyc.setPhoneOneEnabled(Boolean.FALSE);
        lightKyc.setPhoneTwoEnabled(Boolean.FALSE);
        lightKyc.setAreaEnabled(Boolean.FALSE);
        lightKyc.setPoboxEnabled(Boolean.FALSE);
        lightKyc.setZipCodeEnabled(Boolean.FALSE);
        lightKyc.setBuildingNumEnabled(Boolean.FALSE);
        lightKyc.setStreetNameEnabled(Boolean.FALSE);
        lightKyc.setAddressEnabled(Boolean.FALSE);
        lightKyc.setNoteEnabled(Boolean.FALSE);
        lightKyc.setOccupationEnabled(Boolean.FALSE);
        lightKyc.setIdExpiryDateEnabled(Boolean.FALSE);
        lightKyc.setEnglishFullNameReq(Boolean.FALSE);
        lightKyc.setIdentificationReferenceReq(Boolean.FALSE);
        lightKyc.setIdentificationCardReq(Boolean.FALSE);
        lightKyc.setIdCardIssuanceDateReq(Boolean.FALSE);
        lightKyc.setPassportIdReq(Boolean.FALSE);
        lightKyc.setPassportIssuanceDateReq(Boolean.FALSE);
        lightKyc.setPhoneOneReq(Boolean.FALSE);
        lightKyc.setPhoneTwoReq(Boolean.FALSE);
        lightKyc.setAreaReq(Boolean.FALSE);
        lightKyc.setPoboxReq(Boolean.FALSE);
        lightKyc.setZipCodeReq(Boolean.FALSE);
        lightKyc.setBuildingNumReq(Boolean.FALSE);
        lightKyc.setStreetNameReq(Boolean.FALSE);
        lightKyc.setAddressReq(Boolean.FALSE);
        lightKyc.setNoteReq(Boolean.FALSE);
        lightKyc.setOccupationReq(Boolean.FALSE);
        lightKyc.setIdExpiryDateReq(Boolean.FALSE);
        lightKyc.setPassportIdReq(Boolean.FALSE);
        return lightKyc;
    }
}
