package com.progressoft.babylon.mpay.startuptask.schemes;

import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.MPayContext;
import org.springframework.stereotype.Component;


@Component
public class UpdateSchemesSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {

        LOG.info("UpdateSchemesSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();
        executeQuery("UPDATE MPAY_LIMITSSCHEMES SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("102105").getId() + "'");
        executeQuery("UPDATE MPAY_CHARGESSCHEMES SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("101805").getId() + "'");
        executeQuery("UPDATE MPAY_REQUESTTYPESSCHEME SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("904605").getId() + "'");
        executeQuery("UPDATE MPAY_COMMISSIONSCHEMES SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("211805").getId() + "'");
        executeQuery("UPDATE MPAY_TAXSCHEMES SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("311805").getId() + "'");

        executeQuery("UPDATE MPAY_LIMITSDETAILS SET Z_DRAFT_STATUS='COMMITED',Z_DRAFT_ID=NULL,Z_STATUS_ID='" + dataProvider.getWorkflowStatus("101902").getId() + "'");
        executeQuery("UPDATE MPAY_CHARGESSCHEMEDETAILS SET Z_DRAFT_STATUS='COMMITED',Z_DRAFT_ID=NULL,Z_STATUS_ID='" + dataProvider.getWorkflowStatus("101902").getId() + "'");
        executeQuery("UPDATE MPAY_REQUESTTYPESDETAILS SET Z_DRAFT_STATUS='COMMITED',Z_DRAFT_ID=NULL,Z_STATUS_ID='" + dataProvider.getWorkflowStatus("901902").getId() + "'");
        executeQuery("UPDATE MPAY_COMMISSIONS SET Z_DRAFT_STATUS='COMMITED',Z_DRAFT_ID=NULL,Z_STATUS_ID='" + dataProvider.getWorkflowStatus("211902").getId() + "'");
        executeQuery("UPDATE MPAY_TAXSCHEMEDETAILS SET Z_DRAFT_STATUS='COMMITED',Z_DRAFT_ID=NULL,Z_STATUS_ID='" + dataProvider.getWorkflowStatus("311902").getId() + "'");
        LOG.info("UpdateSchemesSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
    }

    private void executeQuery(String query) {
        itemDao.getEntityManager().createNativeQuery(query).executeUpdate();
        itemDao.getEntityManager().createNativeQuery("COMMIT").executeUpdate();
    }

}

