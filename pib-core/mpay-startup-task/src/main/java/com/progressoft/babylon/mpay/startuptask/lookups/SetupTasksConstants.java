package com.progressoft.babylon.mpay.startuptask.lookups;

public class SetupTasksConstants {
    public static final String DEFAULT_VALUES = "Default";
    public static final String LIGHT_WALLET_VALUE = "LightWallet";
    public static final String AUTO_REG_PROFILE_VALUE = "AutoReg";
    public static final String DEFAULT_CURRENCY_CODE = "ILS";
}
