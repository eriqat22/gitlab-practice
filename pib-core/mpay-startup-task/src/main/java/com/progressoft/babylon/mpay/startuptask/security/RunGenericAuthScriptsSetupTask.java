package com.progressoft.babylon.mpay.startuptask.security;


import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class RunGenericAuthScriptsSetupTask extends StartupTask {
    @Override
    @Transactional
    public void execute(OptionsProvider optionsProvider) throws Exception {
        LOG.info("+++++++++++++++++++++++++++ RunGenericAuthScriptsSetupTask +++++++++++++++++++++++++++");
        String tenant = optionsProvider.get("tenants.names");
        for (String genericAuthorityName : optionsProvider.getValues("genericAuthorityNames")) {
            LOG.info("Adding generic authority: ".concat(genericAuthorityName));
            itemDao.getEntityManager().createNativeQuery(getQuery(genericAuthorityName, tenant)).executeUpdate();
        }
    }

    private String getQuery(String genericAuthorityName, String tenant) {
        return "insert into JFW_BROLES_GENERIC_AUTH (BROLE_ID,AUTHORITY_ID) values ((select id from JFW_BUSINESS_ROLES where name='"+tenant+"MAKER'),(select id from JFW_GENERIC_AUTHORITIES where name ='" + genericAuthorityName + "'))";
    }
}
