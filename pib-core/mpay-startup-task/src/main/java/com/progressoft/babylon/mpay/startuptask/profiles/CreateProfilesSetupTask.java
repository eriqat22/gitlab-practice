package com.progressoft.babylon.mpay.startuptask.profiles;


import com.progressoft.babylon.mpay.startuptask.lookups.SetupTasksConstants;
import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.model.bussinessobject.core.JFWCurrency;
import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.*;
import org.springframework.stereotype.Component;

import static com.progressoft.babylon.mpay.startuptask.lookups.SetupTasksConstants.*;

@Component
public class CreateProfilesSetupTask extends StartupTask {

    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        LOG.info("CreateProfilesSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");

        String tenant = optionsProvider.get("tenants.names");
        try (ServiceUserEnabled ignored = jfw.enableServiceUser(tenant)) {
            ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();
            IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();

            if (lookupsLoader.getBankByCode("kha") != null) {
                LOG.info("all initial data is already created");
                return;
            }

            MPAY_LimitsScheme limitsScheme = dataProvider.getLimitSchemeByCode(DEFAULT_VALUES);
            MPAY_ChargesScheme chargesScheme = dataProvider.getChargeSchemeByCode(DEFAULT_VALUES);
            MPAY_CommissionScheme commissionScheme = dataProvider.getCommissionScheneByCode(DEFAULT_VALUES);
            MPAY_TaxScheme taxScheme = dataProvider.getTaxSchemeByCode(DEFAULT_VALUES);
            MPAY_RequestTypesScheme requestTypesScheme = dataProvider.getRequestTypeScheme(DEFAULT_VALUES);
            WorkflowStatus profileStatus = dataProvider.getWorkflowStatus("101705");
            JFWCurrency currency = lookupsLoader.getCurrency(SetupTasksConstants.DEFAULT_CURRENCY_CODE);

            jfw.createEntity(createProfile(limitsScheme, chargesScheme, commissionScheme, taxScheme, requestTypesScheme, profileStatus, currency, DEFAULT_VALUES));
            jfw.createEntity(createProfile(limitsScheme, chargesScheme, commissionScheme, taxScheme, requestTypesScheme, profileStatus, currency, LIGHT_WALLET_VALUE));
            LOG.info("CreateProfilesSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
        }
    }

    private MPAY_Profile createProfile(MPAY_LimitsScheme limitsScheme, MPAY_ChargesScheme chargesScheme, MPAY_CommissionScheme commissionScheme, MPAY_TaxScheme taxScheme, MPAY_RequestTypesScheme requestTypesScheme, WorkflowStatus profileStatus, JFWCurrency currency, String autoRegProfileValue) {
        MPAY_Profile autoRegistrationProfile = new MPAY_Profile();
        autoRegistrationProfile.setName(autoRegProfileValue);
        autoRegistrationProfile.setCode(autoRegProfileValue);
        autoRegistrationProfile.setLimitsScheme(limitsScheme);
        autoRegistrationProfile.setChargesScheme(chargesScheme);
        autoRegistrationProfile.setCommissionsScheme(commissionScheme);
        autoRegistrationProfile.setTaxScheme(taxScheme);
        autoRegistrationProfile.setRequestTypesScheme(requestTypesScheme);
        autoRegistrationProfile.setDescription(autoRegProfileValue);
        autoRegistrationProfile.setCurrency(currency);
        autoRegistrationProfile.setStatusId(profileStatus);
        return autoRegistrationProfile;
    }
}
