package com.progressoft.babylon.mpay.startuptask.merchants;

import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.MPayContext;
import org.springframework.stereotype.Component;


@Component
public class UpdateInitialMerchantsSetupTask extends StartupTask {
    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        LOG.info("UpdateInitialMerchantsSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();
        executeQuery("UPDATE MPAY_Corporates SET Z_DRAFT_STATUS='COMMITED', Z_DRAFT_ID=NULL, Z_STATUS_ID='" + dataProvider.getWorkflowStatus("200005").getId() + " " +
                "where name='JAWWAL'");
        LOG.info("UpdateInitialMerchantsSetupTask finished +++++++++++++++++++++++++++++++++++++++++++++++");
    }

    private void executeQuery(String query) {
        itemDao.getEntityManager().createNativeQuery(query).executeUpdate();
        itemDao.getEntityManager().createNativeQuery("COMMIT").executeUpdate();
    }
}
