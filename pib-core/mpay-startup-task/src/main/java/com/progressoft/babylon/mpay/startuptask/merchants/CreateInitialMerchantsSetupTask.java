package com.progressoft.babylon.mpay.startuptask.merchants;

import com.progressoft.jfw.ServiceUserEnabled;
import com.progressoft.jfw.model.bussinessobject.core.JfwCountry;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.Date;

import static com.google.common.io.ByteStreams.toByteArray;
import static java.util.Objects.requireNonNull;


@Component
public class CreateInitialMerchantsSetupTask extends StartupTask {


    @Override
    public void execute(OptionsProvider optionsProvider) throws Exception {
        ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();
        LOG.info("CreatePaymentServiceProviderSetupTask started +++++++++++++++++++++++++++++++++++++++++++++++");

        byte[] logoBytes = toByteArray(new FileInputStream(new File(requireNonNull(this.getClass().getClassLoader().getResource("logo.png")).getPath())));

        try (ServiceUserEnabled ignored = jfw.enableServiceUser(optionsProvider.get("tenants.names"))) {

            MPAY_Bank defaultBank = lookupsLoader.getBank("Default");
            MPAY_MessageType p2PPaymentType = lookupsLoader.getMessageTypes("P2P");
            JfwCountry country = lookupsLoader.getCountry("PSE");
            MPAY_ClientType clientType = lookupsLoader.getClientType("650");
            MPAY_City city = lookupsLoader.getCity("01");
            MPAY_CityArea areaByCode = lookupsLoader.getAreaByCode("1");
            MPAY_Language en = lookupsLoader.getLanguageByCode("en");
            MPAY_AccountCategory accountCategory = lookupsLoader.getAccountCategory(2);
            MPAY_IDType idType = lookupsLoader.getIDType("999");
            MPAY_Profile aDefault = lookupsLoader.getProfile("Default");
            MPAY_ServicesCategory serviceCategory = lookupsLoader.getServiceCategory("0");
            MPAY_ServiceType serviceType = lookupsLoader.getServiceType("0");

            MPAY_CorporateKyc fullKeyc = lookupsLoader.getCorporateKycByCode("FullKeyc");
            LOG.info(fullKeyc.getCode().concat("++++++++++++++++++++++++++++++++++++++++++++++++"));
            MPAY_Corporate merchantJawwal = createMerchant(defaultBank, p2PPaymentType, "JAWWAL", "jawwal@pib.com", "8888888888", country, clientType, city, areaByCode, en, accountCategory, idType, aDefault, serviceCategory, serviceType, fullKeyc);
            jfw.createEntity(MPAYView.CORPORATE.viewName, merchantJawwal);

            MPAY_Corporate merchantRoots = createMerchant(defaultBank, p2PPaymentType, "ROOTS", "roots@pib.com", "7777777777", country, clientType, city, areaByCode, en, accountCategory, idType, aDefault, serviceCategory, serviceType, fullKeyc);
            jfw.createEntity(MPAYView.CORPORATE.viewName, merchantRoots);

            MPAY_CorporateAtt jawwalLogo = getMpay_corporateAtt(logoBytes, merchantJawwal);
            itemDao.persist(jawwalLogo);

            MPAY_CorporateAtt rootsLogo = getMpay_corporateAtt(logoBytes, merchantRoots);
            itemDao.persist(rootsLogo);

            attachMerchantServiceLogo(logoBytes, merchantJawwal);
            attachMerchantServiceLogo(logoBytes, merchantRoots);

            Long merchantJawwalServiceId = merchantJawwal.getRefCorporateCorpoarteServices().get(0).getId();
            Long merchantRootsServiceId = merchantRoots.getRefCorporateCorpoarteServices().get(0).getId();

            itemDao.getEntityManager().createNativeQuery("UPDATE MPAY_SYSCONFIGS SET CONFIGVALUE='" + merchantJawwalServiceId + "' WHERE CONFIGKEY='Jawwal Service Id'").executeUpdate();
            itemDao.getEntityManager().createNativeQuery("UPDATE MPAY_SYSCONFIGS SET CONFIGVALUE='" + merchantRootsServiceId + "' WHERE CONFIGKEY='Roots Service Id'").executeUpdate();

            itemDao.getEntityManager().createNativeQuery("commit").executeUpdate();
        }
    }

    private void attachMerchantServiceLogo(byte[] logoBytes, MPAY_Corporate merchantJawwal) {
        MPAY_CorpoarteServiceAtt serviceJawwalLogo = new MPAY_CorpoarteServiceAtt();
        serviceJawwalLogo.setAttFile(logoBytes);
        serviceJawwalLogo.setContentType("png");
        serviceJawwalLogo.setName("logo");
        serviceJawwalLogo.setEntityId(merchantJawwal.getRefCorporateCorpoarteServices().get(0).getClass().getName());
        serviceJawwalLogo.setRecordId(merchantJawwal.getRefCorporateCorpoarteServices().get(0).getId().toString());
        serviceJawwalLogo.setSize(BigDecimal.valueOf(logoBytes.length));
        itemDao.persist(serviceJawwalLogo);
    }

    private MPAY_Corporate createMerchant(MPAY_Bank defaultBank, MPAY_MessageType p2PPaymentType, String merchantName, String merchantEmail, String merchantId, JfwCountry country, MPAY_ClientType clientType, MPAY_City city, MPAY_CityArea areaByCode, MPAY_Language en, MPAY_AccountCategory accountCategory, MPAY_IDType idType, MPAY_Profile aDefault, MPAY_ServicesCategory serviceCategory, MPAY_ServiceType serviceType, MPAY_CorporateKyc kycTemplate) {
        MPAY_Corporate merchant = new MPAY_Corporate();
        merchant.setRefCountry(country);
        merchant.setClientType(clientType);
        merchant.setIsActive(true);
        merchant.setRegistrationDate(new Date());
        merchant.setIsRegistered(true);
        merchant.setCity(city);
        merchant.setArea(areaByCode);
        merchant.setDescription(merchantName);
        merchant.setName(merchantName);
        merchant.setEmail(merchantEmail);
        merchant.setPrefLang(en);
        merchant.setCategory(accountCategory);
        merchant.setIdType(idType);
        merchant.setNationalID(merchantId);
        merchant.setRegistrationId(merchantId);
        merchant.setServiceName(merchantName);
        merchant.setServiceDescription(merchantName);
        merchant.setBank(defaultBank);
        merchant.setPaymentType(p2PPaymentType);
        merchant.setRefProfile(aDefault);
        merchant.setServiceCategory(serviceCategory);
        merchant.setServiceType(serviceType);
        merchant.setMaxNumberOfDevices(0L);
        merchant.setMas("1");
        merchant.setIban("IBAN000000000000001");
        merchant.setBankedUnbanked("1");
        merchant.setKycTemplate(kycTemplate);
        return merchant;
    }

    private MPAY_CorporateAtt getMpay_corporateAtt(byte[] logoBytes, MPAY_Corporate merchantJawwal) {
        MPAY_CorporateAtt corporateLogo = new MPAY_CorporateAtt();
        corporateLogo.setAttFile(logoBytes);
        corporateLogo.setContentType("png");
        corporateLogo.setName("logo");
        corporateLogo.setEntityId(merchantJawwal.getClass().getName());
        corporateLogo.setRecordId(merchantJawwal.getId().toString());
        corporateLogo.setSize(BigDecimal.valueOf(logoBytes.length));
        return corporateLogo;
    }
}
