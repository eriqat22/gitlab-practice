FROM progressoft/tomcat-sql5:alpine8-latest
ARG  PSCI_VERSION=1.0
ENV  VERSION=${PSCI_VERSION}
USER root
# this is another note for the new branch
# this is note to be able to update the branch
RUN  printf "║ %-25s │ %-25s ║\n" "core" ${VERSION} >> /VERSION_INFO; \
     chown -R nobody:nobody /VERSION_INFO

COPY --chown=nobody:nobody artifacts/core-*.war            /usr/local/tomcat/webapps/core.war
COPY --chown=nobody:nobody cicd/docker/core-entrypoint.sh  /usr/local/tomcat/
RUN unzip -q /usr/local/tomcat/webapps/core.war -d         /usr/local/tomcat/webapps/ROOT \
    && rm /usr/local/tomcat/webapps/core.war \
    && tar -xzvf /usr/local/tomcat/webapps/ROOT/WEB-INF/backup/app/core.tar.gz -C /usr/local/tomcat/webapps/ROOT/WEB-INF/backup/app/ \
    && chown -R nobody:nobody /usr/local/tomcat \
    && chmod +x /usr/local/tomcat/*.sh \
    && cp -r /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/liquibase/scripts /liquibase/scripts

WORKDIR /usr/local/tomcat/bin
CMD   ../core-entrypoint.sh
USER  nobody
