#!/bin/bash

if ! dbtool  $db_engine \
    --user   $db_user \
    --pass   $db_password \
    --host   $db_host \
    --port   $db_port \
    --inst   $db_instance \
    --test   MPAY_ePayStatusCodes \
    --external $db_external \
    --rebuild true \
    --command "imp system/oracle@$db_host:$db_port/$db_instance fromuser=MARSPIBHYBRIDMASTERPIBH touser=$db_user file=/usr/local/tomcat/webapps/ROOT/WEB-INF/backup/app/core.dmp ignore=yes statistics=none"

then
    echo "[JFW_ENTRY] Failed to dbtool"
    exit 1
fi
echo "+-------------------------------------------------------+"
echo "|       Executing database migration scripts            |"
echo "+-------------------------------------------------------+"
cat /liquibase/scripts/liquibase-master.xml | grep include
cd /liquibase; ./liquibase --driver=$db_driver --url=$db_url --username=$db_user --password=$db_password --changeLogFile=./scripts/liquibase-master.xml update
if [ "$?" != "0" ]; then echo "Executing migration scripts failed"; exit 1; fi;
echo "+-------------------------------------------------------+"
echo "|       Done executing migration scripts                |"
echo "+-------------------------------------------------------+"


sed -i 's/SYSTEM/PIB/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/routes/MPClearIntegration/*
sed -i 's/tenantId=.*"/tenantId=PIB"/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/routes/MPClearIntegration/mpayAutoLogin.xml
sed -i 's/default.tenant=SYSTEM/default.tenant=PIB/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/core/system-generic.properties
sed -i 's|7777/mpay|7070|g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/app-camel-settings.properties

cd /usr/local/tomcat/bin || exit
catalina.sh jpda run

